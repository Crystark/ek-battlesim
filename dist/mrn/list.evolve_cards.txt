Abhorrent Gargoyle       | 2 x Giant Mud Larva          | material to: Behemoth (x3)
Abyss Observer           | 2 x Infernal Emperor         | material to: Blazing Dragon (x2)
Aegis Dragon             | 2 x Malevolent Dryad         | material to: Virgo (x2)
Aeneas                   | 2 x Shadow Assassin          | material to: Taiga General (x2)
Alanine                  | 3 x Night Kirin              | material to: Vine Horror (x3)
Ancient Garuda           | 3 x Hephaestus               | material to: Mephisto (x2)
Ancient One              | 3 x Sea Piercer              | material to: Gilgamesh (x3)
Ancient Oni              | 2 x Zombie Dragon            | material to: Zombie Dragon (x2)
Angelia the Pure         | 2 x Gilgamesh                | material to: Tsunami Razer (x2)
Angra Mainyu             | 3 x Skydweller               | material to: Demon Soul (x2)
Ankylo                   | 3 x Undead Warrior           | material to: Taurus Guard (x3)
Anubis                   | 3 x Capricorn                | material to: Senior Alchemist (x2)
Aranyani                 | 2 x Christmas Treant         | material to: Night Elf Ranger (x3)
Arbiter of War           | 2 x Celestial Touchstone     | material to: Celestial Touchstone (x2)
Archfiend Striker        | 3 x Kraken Defender          | material to: Psionic Envoy (x1)
Arctic Cephalid          | 3 x Taiga General            | material to: Ice Dragon (x2)
Arctic Drake             | 3 x Frostree-Folk            | material to: Arctic Terror (x3)
Arctic Lich              | 2 x Lady Venom               | material to: Lady Venom (x2)
Arctic Overlord          | 3 x Whitewing Pope           | material to: Meteor Mage (x2)
Arctic Sea Dragon        | 3 x Polar Bearborn           | material to: Taiga Cleric (x3)
Arctic Terror            | 3 x Arctic Drake             | material to: Snow Ogre (x3)
Arctic Whale             | 2 x Giant Starfish           | material to: Tundra (x2), Frost Goliath (x2)
Aries                    | 2 x Phantom Guardian         | material to: Pisces (x3)
Armored Sumatran         | 2 x Fire Kirin               | material to: Fire Kirin (x2), Exiled Angel (x3), Hecate (x3), Nocturne (x4)
Armored Turkey           | 2 x God of Pain              | material to: Frostfire Maiden (x2)
Arrogant Queen           | 2 x Cryosurge                | material to: 
Asteria                  | 2 x Tundra Rider             | material to: 
Aurora Master            | 2 x The Rising Dead          | material to: 
Avenger                  | 2 x Mudslide Arachnoid       | material to: 
Azriel                   | 2 x Soul Thief               | material to: Mistress Gloom (x2), Goblin Scout (x3), Swamp Rider (x3)
Azula                    | 2 x Dire Snapping Turtle     | material to: Fire Demon (x2)
Balrog Sentry            | 3 x Fire Gorilla             | material to: Steel Executioner (x2)
Basaltic Guard           | 2 x Giant Frost Toad         | material to: 
Bastet                   | 2 x Evil Mantis              | material to: Dhul-Kifl (x2)
Bat Wizard               | 3 x Psionic Envoy            | material to: Skeleton Marshal (x3)
Beetlelisk               | 2 x Dragon Summoner          | material to: Malevolent Dryad (x2)
Behemoth                 | 3 x Abhorrent Gargoyle       | material to: Serpent Shamanka (x2), Gothmog (x3)
Belial                   | 3 x Crazy Train              | material to: Soulstorm (x3), Reverse Alchemist (x2)
Black Thorn              | 3 x Manscorpion              | material to: 
Blackfire Assassin       | 2 x Mephisto                 | material to: Horde General (x2)
Blackstone               | 3 x Latite Golem             | material to: Magma Frostblade (x3)
Blazing Dragon           | 2 x Abyss Observer           | material to: Dragon Slayer (x2)
Blazing Lion             | 3 x Magma Frostblade         | material to: Demonic Imp (x3)
Blood Lotus Nymph        | 2 x Vampire Bat              | material to: Inferno Treant (x3)
Blood Warrior            | 3 x Taurus Guard             | material to: Swamp Golem (x3)
Bloody Host              | 3 x Vampire Bat Demon        | material to: Ennui (x2), Suchos (x3)
Bones Devourer           | 3 x The Don                  | material to: Evil Treant (x2)
Bow of the Ages          | 2 x Mudslide Arachnoid       | material to: Emerald Dragon (x2)
Bronze Dragon            | 3 x Kitsune                  | material to: Water Elemental (x3)
Bronze Dragon Rider      | 3 x Flame Protector          | material to: 
Cacti Guardian           | 3 x Leprechaun               | material to: Rose Nymph (x3), ThornyLizard (x3)
Cancer                   | 2 x Plasma Dragon            | material to: Plasma Dragon (x2)
Cancer[internal testing] | 2 x Plasma Dragon            | material to: 
Capricorn                | 3 x Keeper of the light      | material to: Anubis (x3), Keeper of the light (x3)
Capricornus Priest       | 4 x Skeleton King            | material to: 
Caster                   | 2 x God of Pain              | material to: 
Cavern Brutalizer        | 2 x Ennui                    | material to: 
Celestial Peacock        | 2 x Tendril Dragon           | material to: Justitia (x3)
Celestial Touchstone     | 2 x Arbiter of War           | material to: Arbiter of War (x2)
Centaur Warrior          | 2 x Infernal Jester          | material to: Frostwolf Chieftain (x3), The Fury (x2)
Centaurian Hoplite       | 2 x Kindred Conjurer         | material to: Kindred Conjurer (x2)
Cerato                   | 3 x Spiderdemon              | material to: Undead Warrior (x3)
Chaos Demon              | 2 x Swamp Predator           | material to: Swamp Predator (x2)
Chimera                  | 3 x Light of Hydra           | material to: Dragon Summoner (x2)
Christmas Treant         | 2 x Goddess of Order         | material to: Aranyani (x2)
Cloris                   | 3 x Soul Stomper             | material to: 
Coiled Dragon            | 3 x Statue of Light          | material to: 
Colossus                 | 3 x Infernal Emperor         | material to: Venus Mermaid (x2)
Combat Master            | 2 x God of War               | material to: 
Coral Princess           | 2 x Royal Guard              | material to: Royal Guard (x2)
Corrupt Dunkleosteus     | 2 x Storm Spirit             | material to: Cryosurge (x2)
Crazy Train              | 3 x The Horned King          | material to: The Horned King (x3), Belial (x3), Kotodama (x2)
Crescent Rider           | 2 x Goddess of Order         | material to: Galaxy Explorer (x2)
Crimson Phoenix          | 3 x Lava Destroyer           | material to: Poseidon (x2)
Cryosurge                | 2 x Corrupt Dunkleosteus     | material to: Arrogant Queen (x2), Flurries (x2)
Crystal Emperor          | 2 x Jack Frost               | material to: Eclipse Beast (x3), Sea Piercer (x2)
Crystal Enchantress      | 3 x Rogue Knight             | material to: Ice Golem (x3), Voyager (x2)
Cthulhu                  | 2 x UnknownCardId[9016]      | material to: 
Cunning Fiend            | 3 x Light of Hydra           | material to: 
Daemon Scorpion          | 2 x Void Blademaster         | material to: Thunder Warhawk (x2)
Dark Heart               | 2 x Poseidon's Wrath         | material to: 
Dark Ranger              | 2 x The Butcher              | material to: 
Dawning Horn             | 3 x Venomripper              | material to: 
Death Knight             | 2 x Toxic Fungi Lord         | material to: The Butcher (x2)
Death Knight Lord        | 3 x Exiled Angel             | material to: 
Death Smile              | 2 x Hanging Gardens          | material to: 
Demon Soul               | 2 x Angra Mainyu             | material to: 
Demon's Heart            | 3 x Infernal Blade           | material to: Infernal Blade (x3), Red Lotus Witch (x2)
Demonic Imp              | 3 x Blazing Lion             | material to: Draconian Shaman (x3)
Dharmanian               | 3 x Ice Dragon               | material to: Winter Samurai (x2), Taiga Archon (x2)
Dhul-Kifl                | 2 x Bastet                   | material to: 
Dire Snapping Turtle     | 2 x Fire Demon               | material to: Azula (x2)
Divine Enforcer          | 3 x Keeper of the light      | material to: The Flying Dutchman (x3), League Knight (x2)
Draconian Elder          | 3 x Draconian Shaman         | material to: Vampire Bat (x3)
Draconian Shaman         | 3 x Demonic Imp              | material to: Draconian Elder (x3)
Dragon Commander         | 4 x Red Banded Dragon        | material to: 
Dragon Guardian          | 2 x Vulcan                   | material to: 
Dragon Knight            | 1 x Lava Destroyer           | material to: Queen of the Dead (x2)
Dragon Slayer            | 2 x Blazing Dragon           | material to: Winged Cyanocinctus (x2), The Four Horsemen (x3)
Dragon Summoner          | 2 x Chimera                  | material to: Beetlelisk (x2)
DreamGoddess             | 2 x Silver Swordmaster       | material to: 
Eagle Fighter            | 2 x Forest Rider             | material to: Forest Rider (x2)
Eclipse Beast            | 3 x Crystal Emperor          | material to: 
Emerald Dragon           | 2 x Bow of the Ages          | material to: NO 17 (x2)
Empyrean Priestess       | 3 x Omniscient Dragon        | material to: Moon Druid (x2)
Ennui                    | 2 x Bloody Host              | material to: Cavern Brutalizer (x2)
Enticing Alpaca          | 3 x Void Blademaster         | material to: Foxtail Nymph (x3)
Eos                      | 2 x Flamereaper              | material to: 
Everglade Mermaid        | 3 x Serpent Shamanka         | material to: 
Evil Empress             | 2 x Serpent Shamanka         | material to: Venus Soultrap (x2), Venomripper (x3), Orc Teacher (x3)
Evil Mantis              | 2 x Goblin Chieftain         | material to: Strange Visitor (x2), Bastet (x2)
Evil Treant              | 2 x Bones Devourer           | material to: Goblin Knight (x2)
Exiled Angel             | 3 x Armored Sumatran         | material to: Gale The Knight (x2), Princess Sakura (x2), Death Knight Lord (x3), Forest (x2)
Fairy Lake               | 3 x Moor Ripper              | material to: 
Fallen Knight            | 2 x The Fury                 | material to: 
Feral Stalker            | 2 x Forest Bender            | material to: 
Fire Berserker           | 2 x Winged Exile             | material to: God of Pain (x2)
Fire Demon               | 2 x Azula                    | material to: Spirit Hunter (x2), Dire Snapping Turtle (x2)
Fire Dragon Turtle       | 2 x Flame Empress            | material to: Flame Empress (x2)
Fire Gorilla             | 3 x Red Banded Dragon        | material to: Balrog Sentry (x3), Skeleton King (x2)
Fire Kirin               | 2 x Armored Sumatran         | material to: Armored Sumatran (x2)
Firebrand Warrior        | 2 x Skeleton Marshal         | material to: 
Flame Empress            | 2 x Fire Dragon Turtle       | material to: Fire Dragon Turtle (x2)
Flame Protector          | 3 x Giant Mollusc            | material to: Latite Golem (x3), Bronze Dragon Rider (x3)
Flamereaper              | 2 x Infernal Simian          | material to: Volcano Guard (x2), Mountain (x2), Eos (x2)
Flurries                 | 2 x Cryosurge                | material to: 
Forest                   | 2 x Exiled Angel             | material to: 
Forest Bender            | 1 x Forest Bender SP         | material to: Forest Bender SP (x1), Feral Stalker (x2), Glacial Interceptor (x3), Reindeer Warrior (x1)
Forest Bender SP         | 1 x Forest Bender            | material to: Forest Bender (x1), Leprechaun (x3)
Forest Giant             | 3 x Water Elemental          | material to: Void Blademaster (x4)
Forest Rider             | 2 x Eagle Fighter            | material to: Eagle Fighter (x2)
Foxtail Nymph            | 3 x Enticing Alpaca          | material to: 
Freyja                   | 4 x God of War               | material to: 
Frost Goliath            | 2 x Arctic Whale             | material to: Storm Spirit (x3)
Frost Pumpkin            | 2 x Lightning Pumpkin        | material to: Molten Pumpkin (x2)
Frostfire Maiden         | 2 x Armored Turkey           | material to: Varian Wrynn (x3), Pharaoh (x2)
Frostree-Folk            | 3 x Snow Crusader            | material to: Arctic Drake (x3)
Frostwolf Chieftain      | 3 x Centaur Warrior          | material to: 
Galaxy Explorer          | 2 x Crescent Rider           | material to: 
Gale The Knight          | 2 x Exiled Angel             | material to: 
Gemini                   | 2 x Serpent's Quin           | material to: Little Coward (x2)
Ghost Ranger             | 2 x Typhon                   | material to: 
Giant Crustacean         | 3 x Venus Soultrap           | material to: 
Giant Frost Toad         | 2 x Magma Frostblade         | material to: Basaltic Guard (x2)
Giant Mollusc            | 3 x Necromancer              | material to: Flame Protector (x3)
Giant Mud Larva          | 2 x Goblin Engineer          | material to: Abhorrent Gargoyle (x2), Typhon (x3)
Giant Starfish           | 2 x Tsunami Razer            | material to: Arctic Whale (x2)
Gilgamesh                | 3 x Ancient One              | material to: Angelia the Pure (x2)
Glacial Interceptor      | 3 x Forest Bender            | material to: Snowman Warrior (x1), Kraken Defender (x3)
Goblin Chieftain         | 2 x Soul Stomper             | material to: Evil Mantis (x2)
Goblin Elder             | 3 x Swamp Golem              | material to: 
Goblin Engineer          | 2 x Moor Ripper              | material to: Giant Mud Larva (x2)
Goblin Knight            | 2 x Evil Treant              | material to: 
Goblin Scout             | 3 x Azriel                   | material to: 
Goblin Warrior           | 3 x Murk Monstrosity         | material to: 
God of Pain              | 2 x Fire Berserker           | material to: Armored Turkey (x2), Winged Exile (x2), Caster (x2)
God of Salvation         | 3 x Santa                    | material to: 
God of War               | 2 x Venus Soultrap           | material to: Freyja (x4), Combat Master (x2), Marsh Devourer (x2)
Goddess of Flowers       | 3 x Venus Soultrap           | material to: 
Goddess of Order         | 2 x Omniscient Dragon        | material to: Crescent Rider (x2), Christmas Treant (x2), Snow Elsie (x3)
Gothmog                  | 3 x Behemoth                 | material to: Shadow Guard A (x2)
Griffon Dancer           | 2 x Sea Piercer              | material to: Infinity King (x2)
Guard Sakura             | 2 x Strange Visitor          | material to: 
Hanging Gardens          | 3 x The Horned King          | material to: Idomeneo (x3), Death Smile (x2)
Hecate                   | 3 x Armored Sumatran         | material to: 
Hedgehog Sprite          | 3 x Vampire Bat              | material to: Hell Khazra (x3)
Hell Khazra              | 3 x Hedgehog Sprite          | material to: 
Hephaestus               | 2 x Pharaoh                  | material to: Ancient Garuda (x3)
Horde General            | 2 x Blackfire Assassin       | material to: Infernal Simian (x2)
Ice Dancer               | 2 x Ice Hide                 | material to: 
Ice Dragon               | 2 x Arctic Cephalid          | material to: Shadow Assassin (x2), Steel Dragon (x3), Dharmanian (x3)
Ice Golem                | 3 x Crystal Enchantress      | material to: Snow Crusader (x3)
Ice Knight               | 2 x Taiga Archon             | material to: Santa (x2)
Idomeneo                 | 3 x Hanging Gardens          | material to: Mystery Ichthyosaur (x2), Tendril Dragon (x2)
Infernal Blade           | 3 x Demon's Heart            | material to: Demon's Heart (x3), The Gate of Hell (x3), White Tiger Master (x2)
Infernal Emperor         | 3 x The Gate of Hell         | material to: Abyss Observer (x2), Colossus (x3)
Infernal Jester          | 2 x Infernal Simian          | material to: Centaur Warrior (x2)
Infernal Simian          | 2 x Horde General            | material to: Vulcan (x3), Infernal Jester (x2), Flamereaper (x2)
Infernal Tormentor       | 2 x Kindred Conjurer         | material to: 
Inferno Treant           | 3 x Blood Lotus Nymph        | material to: 
Infinity King            | 2 x Griffon Dancer           | material to: 
Jack Frost               | 2 x Light Paladin            | material to: Crystal Emperor (x2)
Justitia                 | 3 x Celestial Peacock        | material to: Mudslide Arachnoid (x2), Robin Test (x2)
Keeper of the light      | 3 x Capricorn                | material to: Divine Enforcer (x3), Capricorn (x3)
Kindred Conjurer         | 2 x Centaurian Hoplite       | material to: Infernal Tormentor (x2), Centaurian Hoplite (x2)
Kitsune                  | 3 x Stone Taurus             | material to: Bronze Dragon (x3)
Kotodama                 | 2 x Crazy Train              | material to: 
Kraken                   | 2 x Poseidon's Wrath         | material to: Shadow Guard B (x2), Sapphire Dragon (x2)
Kraken Defender          | 3 x Glacial Interceptor      | material to: Archfiend Striker (x3), Soul Thief (x1)
Lady Venom               | 2 x Arctic Lich              | material to: Arctic Lich (x2)
Latite Golem             | 3 x Flame Protector          | material to: Blackstone (x3)
Lava Destroyer           | 3 x Nuriel                   | material to: Crimson Phoenix (x3), Yeti (x3), Dragon Knight (x1)
League Knight            | 2 x Divine Enforcer          | material to: 
Leprechaun               | 3 x Forest Bender SP         | material to: Cacti Guardian (x3)
Light Paladin            | 2 x Santa                    | material to: Water Bender (x3), Jack Frost (x2)
Light Swordsman          | 2 x Nightingale              | material to: Nightingale (x2)
Light of Hydra           | 3 x Moss Dragon              | material to: Cunning Fiend (x3), Prince of the Forest (x2), Chimera (x3)
Lightning Pumpkin        | 2 x Toxic Pumpkin            | material to: Frost Pumpkin (x2)
Little Coward            | 2 x Gemini                   | material to: 
Lord Brook               | 2 x Whitewing Pope           | material to: 
Lord of the Mire         | 3 x Vampire Bat Demon        | material to: Undead Felldrake (x3)
Magma Frostblade         | 3 x Blackstone               | material to: Giant Frost Toad (x2), Blazing Lion (x3)
Malevolent Dryad         | 2 x Beetlelisk               | material to: Aegis Dragon (x2)
Malicious Treant         | 2 x Suchos                   | material to: 
Manscorpion              | 3 x Undead Felldrake         | material to: Therapod (x3), Black Thorn (x3)
Marsh Devourer           | 2 x God of War               | material to: Moor Ripper (x2)
Medusa                   | 2 x Vampire Bat              | material to: 
Meow Maid                | 2 x The Gate of Hell         | material to: 
Mephisto                 | 2 x Ancient Garuda           | material to: Blackfire Assassin (x2)
Mermaid Princess         | 3 x Santa's helper           | material to: Rogue Knight (x3)
Meteor Mage              | 2 x Arctic Overlord          | material to: 
Mistress Gloom           | 2 x Azriel                   | material to: Murk Monstrosity (x2)
Molten Pumpkin           | 2 x Frost Pumpkin            | material to: Toxic Pumpkin (x2)
Moon Druid               | 2 x Empyrean Priestess       | material to: 
Moon Ranger              | 2 x Water Elemental          | material to: 
Moor Ripper              | 2 x Marsh Devourer           | material to: Goblin Engineer (x2), Fairy Lake (x3)
Moss Dragon              | 2 x Thunder Dragon           | material to: Light of Hydra (x3)
Mountain                 | 2 x Flamereaper              | material to: 
Mountain King            | 3 x Steel Executioner        | material to: 
Mudslide Arachnoid       | 2 x Justitia                 | material to: Avenger (x2), Bow of the Ages (x2)
Murk Monstrosity         | 2 x Mistress Gloom           | material to: Goblin Warrior (x3)
Music Memoria            | 3 x Phantom Guardian         | material to: 
Mystery Ichthyosaur      | 2 x Idomeneo                 | material to: 
NO 17                    | 2 x Emerald Dragon           | material to: 
Navigator                | 2 x Necroflayer              | material to: 
Necroflayer              | 2 x Venomripper              | material to: Navigator (x2)
Necromancer              | 3 x Skeleton Marshal         | material to: Giant Mollusc (x3)
Night Elf Ranger         | 3 x Aranyani                 | material to: Winged Tiger (x2)
Night Kirin              | 3 x Rose Nymph               | material to: Alanine (x3)
Nightingale              | 2 x Light Swordsman          | material to: Light Swordsman (x2)
Nocturne                 | 4 x Armored Sumatran         | material to: 
North Sea Kraken         | 2 x Suchos                   | material to: 
Nuriel                   | 2 x Queen of the Dead        | material to: Lava Destroyer (x3)
Omniscient Dragon        | 2 x Prince of the Forest     | material to: Goddess of Order (x2), Predictor (x3), Empyrean Priestess (x3)
Ophiuchus                | 2 x Storm Spirit             | material to: 
Orc Teacher              | 3 x Evil Empress             | material to: Voodoo Scarecrow (x3)
Paraselene               | 2 x Serpent's Quin           | material to: 
Phantom Guardian         | 2 x Tsunami Razer            | material to: Music Memoria (x3), Aries (x2)
Pharaoh                  | 2 x Frostfire Maiden         | material to: Hephaestus (x2)
Pisces                   | 3 x Aries                    | material to: 
Plasma Dragon            | 2 x Cancer                   | material to: Cancer[internal testing] (x2), Cancer (x2)
Polar Bearborn           | 3 x Snow Ogre                | material to: Arctic Sea Dragon (x3)
Poseidon                 | 2 x Crimson Phoenix          | material to: 
Poseidon's Wrath         | 2 x Stardust Dragon          | material to: Kraken (x2), Dark Heart (x2)
Predictor                | 3 x Omniscient Dragon        | material to: 
Prince of the Forest     | 2 x Light of Hydra           | material to: Warrior of the Forest (x2), Omniscient Dragon (x2)
Princess Sakura          | 2 x Exiled Angel             | material to: 
Prophet                  | 2 x Snow Crusader            | material to: 
Psionic Envoy            | 1 x Archfiend Striker        | material to: Bat Wizard (x3)
Queen of the Dead        | 2 x Dragon Knight            | material to: Nuriel (x2)
Red Banded Dragon        | 2 x Skeleton King            | material to: Fire Gorilla (x3), Dragon Commander (x4)
Red Lotus Witch          | 2 x Demon's Heart            | material to: 
Reindeer Warrior         | 1 x Forest Bender            | material to: 
Reverse Alchemist        | 2 x Belial                   | material to: 
Robin Test               | 2 x Justitia                 | material to: 
Rogue Knight             | 3 x Mermaid Princess         | material to: Crystal Enchantress (x3)
Rose Knight              | 2 x Winged Tiger             | material to: 
Rose Nymph               | 3 x Cacti Guardian           | material to: Night Kirin (x3)
Royal Guard              | 2 x Coral Princess           | material to: Coral Princess (x2)
Sagittarius              | 2 x Strange Visitor          | material to: 
Santa                    | 2 x Ice Knight               | material to: God of Salvation (x3), Light Paladin (x2)
Santa's helper           | 3 x Snowman Warrior          | material to: Mermaid Princess (x3)
Sapphire Dragon          | 2 x Kraken                   | material to: 
Scarlet Ranger           | 3 x Vampire Bat              | material to: 
Scavenger Griffin        | 2 x Viscid Ooze              | material to: 
Sea Piercer              | 2 x Crystal Emperor          | material to: Ancient One (x3), Griffon Dancer (x2)
Senior Alchemist         | 2 x Anubis                   | material to: 
Serpent Shamanka         | 2 x Behemoth                 | material to: Evil Empress (x2), Everglade Mermaid (x3)
Serpent's Quin           | 3 x Soul Stomper             | material to: Paraselene (x2), Gemini (x2)
Shadow Assassin          | 2 x Ice Dragon               | material to: Aeneas (x2), Tundra Rider (x2)
Shadow Guard A           | 2 x Gothmog                  | material to: 
Shadow Guard B           | 2 x Kraken                   | material to: 
Silver Swordmaster       | 3 x Steel Dragon             | material to: DreamGoddess (x2)
Skeleton King            | 2 x Fire Gorilla             | material to: Capricornus Priest (x4), Red Banded Dragon (x2)
Skeleton Marshal         | 3 x Bat Wizard               | material to: Necromancer (x3), Firebrand Warrior (x2)
Skydweller               | 2 x The Butcher              | material to: Angra Mainyu (x3)
Snow Crusader            | 3 x Ice Golem                | material to: Prophet (x2), Frostree-Folk (x3)
Snow Elsie               | 3 x Goddess of Order         | material to: 
Snow Ogre                | 3 x Arctic Terror            | material to: Polar Bearborn (x3)
Snowman Warrior          | 1 x Glacial Interceptor      | material to: Santa's helper (x3)
Sophistry Counsellor     | 2 x Stardust Dragon          | material to: 
Soul Stomper             | 2 x Venomripper              | material to: Goblin Chieftain (x2), Cloris (x3), Serpent's Quin (x3)
Soul Thief               | 1 x Kraken Defender          | material to: Azriel (x2), Wild Duron (x3)
Soulstorm                | 3 x Belial                   | material to: Taurus (x3), The Rising Dead (x3)
Spiderdemon              | 3 x Therapod                 | material to: Cerato (x3)
Spirit Hunter            | 2 x Fire Demon               | material to: 
Stardust Dragon          | 3 x The Flying Dutchman      | material to: Sophistry Counsellor (x2), Poseidon's Wrath (x2)
Statue of Light          | 3 x Terra Cotta Warrior      | material to: Coiled Dragon (x3)
Steel Dragon             | 3 x Ice Dragon               | material to: Silver Swordmaster (x3)
Steel Executioner        | 2 x Balrog Sentry            | material to: Mountain King (x3)
Stone Taurus             | 3 x Treant Healer            | material to: Kitsune (x3)
Storm Spirit             | 3 x Frost Goliath            | material to: Ophiuchus (x2), Corrupt Dunkleosteus (x2)
Strange Visitor          | 2 x Evil Mantis              | material to: Sagittarius (x2), Guard Sakura (x2)
Suchos                   | 3 x Bloody Host              | material to: North Sea Kraken (x2), Malicious Treant (x2)
Swamp Golem              | 3 x Blood Warrior            | material to: Goblin Elder (x3)
Swamp Predator           | 2 x Chaos Demon              | material to: Chaos Demon (x2)
Swamp Rider              | 3 x Azriel                   | material to: 
Taiga Archon             | 2 x Dharmanian               | material to: Ice Knight (x2)
Taiga Cleric             | 3 x Arctic Sea Dragon        | material to: Terra Cotta Warrior (x3)
Taiga General            | 2 x Aeneas                   | material to: Arctic Cephalid (x3)
Taurus                   | 3 x Soulstorm                | material to: 
Taurus Guard             | 3 x Ankylo                   | material to: Blood Warrior (x3)
Tendril Dragon           | 2 x Idomeneo                 | material to: Thalia (x2), Celestial Peacock (x2)
Terra Cotta Warrior      | 3 x Taiga Cleric             | material to: Statue of Light (x3)
Thalia                   | 2 x Tendril Dragon           | material to: 
The Butcher              | 2 x Death Knight             | material to: Dark Ranger (x2), Skydweller (x2)
The Don                  | 3 x Voodoo Scarecrow         | material to: Bones Devourer (x3)
The Flying Dutchman      | 3 x Divine Enforcer          | material to: X Ninja (x2), Stardust Dragon (x3)
The Four Horsemen        | 3 x Dragon Slayer            | material to: Wind Musician (x2)
The Fury                 | 2 x Centaur Warrior          | material to: Fallen Knight (x2)
The Gate of Hell         | 3 x Infernal Blade           | material to: Infernal Emperor (x3), Meow Maid (x2)
The Horned King          | 3 x Crazy Train              | material to: Hanging Gardens (x3), Crazy Train (x3)
The Rising Dead          | 3 x Soulstorm                | material to: Aurora Master (x2)
The Seer(God)            | 1 x The Seer(God)            | material to: The Seer(God) (x1)
Therapod                 | 3 x Manscorpion              | material to: Spiderdemon (x3)
ThornyLizard             | 3 x Cacti Guardian           | material to: 
Thunder Dragon           | 2 x Winged Tiger             | material to: Moss Dragon (x2)
Thunder Warhawk          | 2 x Daemon Scorpion          | material to: 
Toxic Fungi Lord         | 3 x Voodoo Scarecrow         | material to: Death Knight (x2)
Toxic Pumpkin            | 2 x Molten Pumpkin           | material to: Lightning Pumpkin (x2)
Treant Healer            | 3 x White Peryton            | material to: Stone Taurus (x3)
Tsunami Razer            | 2 x Angelia the Pure         | material to: Giant Starfish (x2), Phantom Guardian (x2)
Tundra                   | 2 x Arctic Whale             | material to: 
Tundra Rider             | 2 x Shadow Assassin          | material to: Asteria (x2)
Typhon                   | 3 x Giant Mud Larva          | material to: Ghost Ranger (x2)
Undead Felldrake         | 3 x Lord of the Mire         | material to: Manscorpion (x3)
Undead Warrior           | 3 x Cerato                   | material to: Ankylo (x3)
Vampire Bat              | 3 x Draconian Elder          | material to: Scarlet Ranger (x3), Medusa (x2), Hedgehog Sprite (x3), Blood Lotus Nymph (x2)
Vampire Bat Demon        | 3 x Wild Duron               | material to: Lord of the Mire (x3), Bloody Host (x3)
Varian Wrynn             | 3 x Frostfire Maiden         | material to: 
Venomripper              | 3 x Evil Empress             | material to: Dawning Horn (x3), Soul Stomper (x2), Necroflayer (x2)
Venus Mermaid            | 2 x Colossus                 | material to: 
Venus Soultrap           | 2 x Evil Empress             | material to: Giant Crustacean (x3), God of War (x2), Goddess of Flowers (x3)
Vine Horror              | 3 x Alanine                  | material to: White Peryton (x3)
Virgo                    | 2 x Aegis Dragon             | material to: Virgo Shadow (x2)
Virgo Shadow             | 2 x Virgo                    | material to: 
Void Blademaster         | 4 x Forest Giant             | material to: Enticing Alpaca (x3), Daemon Scorpion (x2)
Volcano Guard            | 2 x Flamereaper              | material to: 
Voodoo Scarecrow         | 3 x Orc Teacher              | material to: Toxic Fungi Lord (x3), The Don (x3)
Voyager                  | 2 x Crystal Enchantress      | material to: 
Vulcan                   | 3 x Infernal Simian          | material to: Dragon Guardian (x2)
Vulture Soul             | 2 x Warrior of the Forest    | material to: 
Warrior of the Forest    | 2 x Prince of the Forest     | material to: Vulture Soul (x2)
Water Bender             | 3 x Light Paladin            | material to: 
Water Elemental          | 3 x Bronze Dragon            | material to: Forest Giant (x3), Moon Ranger (x2)
White Peryton            | 3 x Vine Horror              | material to: Treant Healer (x3)
White Tiger Master       | 2 x Infernal Blade           | material to: 
Whitewing Pope           | 2 x Winter Samurai           | material to: Arctic Overlord (x3), Lord Brook (x2)
Wild Duron               | 3 x Soul Thief               | material to: Vampire Bat Demon (x3)
Wind Musician            | 2 x The Four Horsemen        | material to: 
Winged Cyanocinctus      | 2 x Dragon Slayer            | material to: 
Winged Exile             | 2 x God of Pain              | material to: Fire Berserker (x2)
Winged Tiger             | 2 x Night Elf Ranger         | material to: Thunder Dragon (x2), Rose Knight (x2)
Winter Samurai           | 2 x Dharmanian               | material to: Whitewing Pope (x2)
X Ninja                  | 2 x The Flying Dutchman      | material to: 
Yeti                     | 3 x Lava Destroyer           | material to: 
Zombie Dragon            | 2 x Ancient Oni              | material to: Ancient Oni (x2)
duohun[internal testing] | 1 x duohun[internal testing] | material to: duohun[internal testing] (x1)
taopao[internal testing] | 1 x taopao[internal testing] | material to: taopao[internal testing] (x1)
