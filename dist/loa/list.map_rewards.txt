Map 1-1   1☆ | 
Map 1-1   2☆ | 
Map 1-1   3☆ | Shaylee (1*)
Map 1-2   1☆ | 
Map 1-2   2☆ | 
Map 1-2   3☆ | Tarantula (1*)
Map 1-3   1☆ | Imp Guardian (2*)
Map 1-3   2☆ | 
Map 1-3   3☆ | Pikeman (1*)
Map 1-4   1☆ | Amazon (2*)
Map 1-4   2☆ | 
Map 1-4   3☆ | Magic Clown (1*)
Map 1-5   1☆ | 
Map 1-5   2☆ | 
Map 1-5   3☆ | Night Bat (1*)
Map 1-6   1☆ | Battlemage (2*)
Map 1-6   2☆ | Night Bat (1*)
Map 1-6   3☆ | Werehyena (2*)
Map 2-1   1☆ | 
Map 2-1   2☆ | 
Map 2-1   3☆ | Troll (1*)
Map 2-2   1☆ | 
Map 2-2   2☆ | 
Map 2-2   3☆ | Cleric (2*)
Map 2-3   1☆ | Elf Wizard (3*)
Map 2-3   2☆ | 
Map 2-3   3☆ | Elf Sniper (2*)
Map 2-4   1☆ | 
Map 2-4   2☆ | 
Map 2-4   3☆ | Satyr (2*)
Map 2-5   1☆ | 
Map 2-5   2☆ | 
Map 2-5   3☆ | GoblinSapper (1*)
Map 2-6   1☆ | Oracle (2*)
Map 2-6   2☆ | Guard (1*)
Map 2-6   3☆ | Mage (3*)
Map 3-1   1☆ | 
Map 3-1   2☆ | 
Map 3-1   3☆ | Centauride (2*)
Map 3-2   1☆ | Harpy (2*)
Map 3-2   2☆ | 
Map 3-2   3☆ | Shieldman (2*)
Map 3-3   1☆ | 
Map 3-3   2☆ | 
Map 3-3   3☆ | Lightning
Map 3-4   1☆ | 
Map 3-4   2☆ | 
Map 3-4   3☆ | Cleric (2*)
Map 3-5   1☆ | Din's Servant (2*)
Map 3-5   2☆ | 
Map 3-5   3☆ | Torch Treant (2*)
Map 3-6   1☆ | 
Map 3-6   2☆ | 
Map 3-6   3☆ | Wasteland
Map 3-7   1☆ | Thunderbolt
Map 3-7   2☆ | Gargoyle (2*)
Map 3-7   3☆ | Cold Wave
Map 3-8   1☆ | Knight (3*)
Map 3-8   2☆ | Faun Shaman (3*)
Map 3-8   3☆ | Mage (3*)
Map 4-1   1☆ | Mummy (3*)
Map 4-1   2☆ | 
Map 4-1   3☆ | CentaurGuard (2*)
Map 4-2   1☆ | 
Map 4-2   2☆ | 
Map 4-2   3☆ | Gargoyle (2*)
Map 4-3   1☆ | 
Map 4-3   2☆ | 
Map 4-3   3☆ | Fire Fist
Map 4-4   1☆ | 
Map 4-4   2☆ | 
Map 4-4   3☆ | Unicorn (3*)
Map 4-5   1☆ | 
Map 4-5   2☆ | 
Map 4-5   3☆ | Amazon (2*)
Map 4-6   1☆ | 
Map 4-6   2☆ | 
Map 4-6   3☆ | Werehyena (2*)
Map 4-7   1☆ | Elf Priest (3*)
Map 4-7   2☆ | Hellhound (2*)
Map 4-7   3☆ | Fire Offering
Map 4-8   1☆ | Cerberus (3*)
Map 4-8   2☆ | Obsidian (3*)
Map 4-8   3☆ | Lich (3*)
Map 5-1   1☆ | Griffin (3*)
Map 5-1   2☆ | 
Map 5-1   3☆ | Vampire Lord (2*)
Map 5-2   1☆ | 
Map 5-2   2☆ | 
Map 5-2   3☆ | Cleric (2*)
Map 5-3   1☆ | 
Map 5-3   2☆ | 
Map 5-3   3☆ | Red Lotus
Map 5-4   1☆ | 
Map 5-4   2☆ | 
Map 5-4   3☆ | Valkyrie (3*)
Map 5-5   1☆ | 
Map 5-5   2☆ | 
Map 5-5   3☆ | vortex
Map 5-6   1☆ | 
Map 5-6   2☆ | 
Map 5-6   3☆ | Lamia (2*)
Map 5-7   1☆ | 
Map 5-7   2☆ | 
Map 5-7   3☆ | Vampire Lord (2*)
Map 5-8   1☆ | DarkElf Mage (3*)
Map 5-8   2☆ | DarkElf (3*)
Map 5-8   3☆ | Torch
Map 5-9   1☆ | Nosferatu (3*)
Map 5-9   2☆ | OgreWarlock (3*)
Map 5-9   3☆ | Nightmare (3*)
Map 6-1   1☆ | Tree-Guard (3*)
Map 6-1   2☆ | 
Map 6-1   3☆ | Flower Elf (3*)
Map 6-2   1☆ | 
Map 6-2   2☆ | 
Map 6-2   3☆ | PegasusRider (3*)
Map 6-3   1☆ | 
Map 6-3   2☆ | 
Map 6-3   3☆ | Knight (3*)
Map 6-4   1☆ | 
Map 6-4   2☆ | 
Map 6-4   3☆ | Mage (3*)
Map 6-5   1☆ | 
Map 6-5   2☆ | 
Map 6-5   3☆ | Swamp
Map 6-6   1☆ | 
Map 6-6   2☆ | 
Map 6-6   3☆ | Rhino Knight (3*)
Map 6-7   1☆ | 
Map 6-7   2☆ | 
Map 6-7   3☆ | Icicle
Map 6-8   1☆ | Gulf
Map 6-8   2☆ | Nightmare (3*)
Map 6-8   3☆ | Fire Forge
Map 6-9   1☆ | Senior Elf (3*)
Map 6-9   2☆ | Mummy (3*)
Map 6-9   3☆ | Djinn (3*)
Map 7-1   1☆ | PegasusRider (3*)
Map 7-1   2☆ | 
Map 7-1   3☆ | Black Widow (3*)
Map 7-2   1☆ | 
Map 7-2   2☆ | 
Map 7-2   3☆ | vortex
Map 7-3   1☆ | 
Map 7-3   2☆ | 
Map 7-3   3☆ | Senior Elf (3*)
Map 7-4   1☆ | 
Map 7-4   2☆ | 
Map 7-4   3☆ | Murder Doll (2*)
Map 7-5   1☆ | 
Map 7-5   2☆ | 
Map 7-5   3☆ | Snow Wizard (3*)
Map 7-6   1☆ | 
Map 7-6   2☆ | 
Map 7-6   3☆ | OgreWarlock (3*)
Map 7-7   1☆ | 
Map 7-7   2☆ | 
Map 7-7   3☆ | Elf Priest (3*)
Map 7-8   1☆ | 
Map 7-8   2☆ | 
Map 7-8   3☆ | Mineral
Map 7-9   1☆ | OgreWarlock (3*)
Map 7-9   2☆ | Mammoth (4*)
Map 7-9   3☆ | Fire Flow
Map 7-10  1☆ | Durahan (4*)
Map 7-10  2☆ | Dryad (4*)
Map 7-10  3☆ | Lampade (4*)
Map 8-1   1☆ | Chariot (3*)
Map 8-1   2☆ | 
Map 8-1   3☆ | Chief Gorloc (3*)
Map 8-2   1☆ | 
Map 8-2   2☆ | 
Map 8-2   3☆ | Cerberus (3*)
Map 8-3   1☆ | 
Map 8-3   2☆ | 
Map 8-3   3☆ | Clear Springs
Map 8-4   1☆ | 
Map 8-4   2☆ | 
Map 8-4   3☆ | Fire Crow (3*)
Map 8-5   1☆ | Titan (4*)
Map 8-5   2☆ | 
Map 8-5   3☆ | Nosferatu (3*)
Map 8-6   1☆ | 
Map 8-6   2☆ | 
Map 8-6   3☆ | Berserker (3*)
Map 8-7   1☆ | 
Map 8-7   2☆ | 
Map 8-7   3☆ | Archbishop (4*)
Map 8-8   1☆ | 
Map 8-8   2☆ | 
Map 8-8   3☆ | Manticore (3*)
Map 8-9   1☆ | 
Map 8-9   2☆ | 
Map 8-9   3☆ | Lilith (4*)
Map 8-10  1☆ | Black Widow (3*)
Map 8-10  2☆ | Explosion
Map 8-10  3☆ | Lava Armor
Map 8-11  1☆ | Fenrir (4*)
Map 8-11  2☆ | Orc General (4*)
Map 8-11  3☆ | Durahan (4*)
Map 9-1   1☆ | Succubus (3*)
Map 9-1   2☆ | 
Map 9-1   3☆ | Monk (3*)
Map 9-2   1☆ | 
Map 9-2   2☆ | 
Map 9-2   3☆ | Tree-Guard (3*)
Map 9-3   1☆ | 
Map 9-3   2☆ | 
Map 9-3   3☆ | Rhino Knight (3*)
Map 9-4   1☆ | 
Map 9-4   2☆ | 
Map 9-4   3☆ | WildFishman (3*)
Map 9-5   1☆ | 
Map 9-5   2☆ | 
Map 9-5   3☆ | TantorGuard (3*)
Map 9-6   1☆ | 
Map 9-6   2☆ | 
Map 9-6   3☆ | PegasusRider (3*)
Map 9-7   1☆ | 
Map 9-7   2☆ | 
Map 9-7   3☆ | Djinn (3*)
Map 9-8   1☆ | 
Map 9-8   2☆ | 
Map 9-8   3☆ | OgreWarlock (3*)
Map 9-9   1☆ | 
Map 9-9   2☆ | 
Map 9-9   3☆ | Snow Wizard (3*)
Map 9-10  1☆ | Frost-Treant (3*)
Map 9-10  2☆ | Archdemon (4*)
Map 9-10  3☆ | Blademaster (4*)
Map 9-11  1☆ | Bone Dragon (4*)
Map 9-11  2☆ | Pleiade (4*)
Map 9-11  3☆ | Red Dragon (4*)
Map 10-1  1☆ | Royal Lion (3*)
Map 10-1  2☆ | 
Map 10-1  3☆ | Chief Pirate (3*)
Map 10-2  1☆ | 
Map 10-2  2☆ | 
Map 10-2  3☆ | Valkyrie (3*)
Map 10-3  1☆ | 
Map 10-3  2☆ | 
Map 10-3  3☆ | Manticore (3*)
Map 10-4  1☆ | 
Map 10-4  2☆ | 
Map 10-4  3☆ | Nightmare (3*)
Map 10-5  1☆ | 
Map 10-5  2☆ | 
Map 10-5  3☆ | Dwarf Sniper (3*)
Map 10-6  1☆ | 
Map 10-6  2☆ | 
Map 10-6  3☆ | Sandworm (3*)
Map 10-7  1☆ | 
Map 10-7  2☆ | 
Map 10-7  3☆ | FireShaman (3*)
Map 10-8  1☆ | 
Map 10-8  2☆ | 
Map 10-8  3☆ | Electra-Eel (3*)
Map 10-9  1☆ | 
Map 10-9  2☆ | 
Map 10-9  3☆ | Treant Ghost (3*)
Map 10-10 1☆ | Faun Priest (3*)
Map 10-10 2☆ | Giant (4*)
Map 10-10 3☆ | Wyvern (4*)
Map 10-11 1☆ | Naiad (4*)
Map 10-11 2☆ | Tinuth (4*)
Map 10-11 3☆ | Demon King (4*)
Map 11-1  1☆ | 
Map 11-1  2☆ | 
Map 11-1  3☆ | Black Widow (3*)
Map 11-2  1☆ | 
Map 11-2  2☆ | 
Map 11-2  3☆ | DarkElf (3*)
Map 11-3  1☆ | 
Map 11-3  2☆ | 
Map 11-3  3☆ | Ganesha (4*)
Map 11-4  1☆ | 
Map 11-4  2☆ | 
Map 11-4  3☆ | FireShaman (3*)
Map 11-5  1☆ | 
Map 11-5  2☆ | 
Map 11-5  3☆ | Monk (3*)
Map 11-6  1☆ | 
Map 11-6  2☆ | 
Map 11-6  3☆ | Charlatan (3*)
Map 11-7  1☆ | 
Map 11-7  2☆ | 
Map 11-7  3☆ | Royal Lion (3*)
Map 11-8  1☆ | 
Map 11-8  2☆ | 
Map 11-8  3☆ | Dwarf Sniper (3*)
Map 11-9  1☆ | 
Map 11-9  2☆ | 
Map 11-9  3☆ | Hedge Hog (3*)
Map 11-10 1☆ | 
Map 11-10 2☆ | 
Map 11-10 3☆ | Electreant (3*)
Map 11-11 1☆ | Phantasms (3*)
Map 11-11 2☆ | Unicorn King (4*)
Map 11-11 3☆ | Guath (4*)
Map 11-12 1☆ | Mammoth (4*)
Map 11-12 2☆ | Chaotic Bear (4*)
Map 11-12 3☆ | Phoenix (5*)
Map 12-1  1☆ | 
Map 12-1  2☆ | 
Map 12-1  3☆ | Hunter (3*)
Map 12-2  1☆ | 
Map 12-2  2☆ | 
Map 12-2  3☆ | Dark Magus (3*)
Map 12-3  1☆ | 
Map 12-3  2☆ | 
Map 12-3  3☆ | Djinn (3*)
Map 12-4  1☆ | 
Map 12-4  2☆ | 
Map 12-4  3☆ | Elf Assassin (3*)
Map 12-5  1☆ | 
Map 12-5  2☆ | 
Map 12-5  3☆ | Chief Pirate (3*)
Map 12-6  1☆ | 
Map 12-6  2☆ | 
Map 12-6  3☆ | Hunter (3*)
Map 12-7  1☆ | 
Map 12-7  2☆ | 
Map 12-7  3☆ | Hedge Hog (3*)
Map 12-8  1☆ | 
Map 12-8  2☆ | 
Map 12-8  3☆ | Frost-Treant (3*)
Map 12-9  1☆ | 
Map 12-9  2☆ | 
Map 12-9  3☆ | Phantasms (3*)
Map 12-10 1☆ | 
Map 12-10 2☆ | 
Map 12-10 3☆ | Faun Priest (3*)
Map 12-11 1☆ | Treant priest (3*)
Map 12-11 2☆ | Tinuth (4*)
Map 12-11 3☆ | BearWarrior (4*)
Map 12-12 1☆ | Faun Elder (4*)
Map 12-12 2☆ | Medusa (4*)
Map 12-12 3☆ | Kirin (5*)
Map 13-1  1☆ | 
Map 13-1  2☆ | 
Map 13-1  3☆ | Merman Soldier (3*)
Map 13-2  1☆ | 
Map 13-2  2☆ | 
Map 13-2  3☆ | Mecha (4*)
Map 13-3  1☆ | 
Map 13-3  2☆ | 
Map 13-3  3☆ | Imp Artisan (3*)
Map 13-4  1☆ | 
Map 13-4  2☆ | 
Map 13-4  3☆ | Priest (4*)
Map 13-5  1☆ | 
Map 13-5  2☆ | 
Map 13-5  3☆ | Dream Cleric (3*)
Map 13-6  1☆ | 
Map 13-6  2☆ | 
Map 13-6  3☆ | Armed Rose (4*)
Map 13-7  1☆ | 
Map 13-7  2☆ | 
Map 13-7  3☆ | Royal Dancer (3*)
Map 13-8  1☆ | 
Map 13-8  2☆ | 
Map 13-8  3☆ | Ghost Eye (4*)
Map 13-9  1☆ | 
Map 13-9  2☆ | 
Map 13-9  3☆ | Treant Ghost (3*)
Map 13-10 1☆ | 
Map 13-10 2☆ | 
Map 13-10 3☆ | Chaotic Bear (4*)
Map 13-11 1☆ | Spellsword (3*)
Map 13-11 2☆ | Mermaid (4*)
Map 13-11 3☆ | Rain Summoner (5*)
Map 13-12 1☆ | Demon Spirit (4*)
Map 13-12 2☆ | Scarlet Assassin (4*)
Map 13-12 3☆ | Lich Lord (5*)
Map 14-1  1☆ | 
Map 14-1  2☆ | 
Map 14-1  3☆ | Unicorn King (4*)
Map 14-2  1☆ | 
Map 14-2  2☆ | 
Map 14-2  3☆ | Tinuth (4*)
Map 14-3  1☆ | 
Map 14-3  2☆ | 
Map 14-3  3☆ | Guath (4*)
Map 14-4  1☆ | 
Map 14-4  2☆ | 
Map 14-4  3☆ | DragonRider (4*)
Map 14-5  1☆ | 
Map 14-5  2☆ | 
Map 14-5  3☆ | Winged Princess (4*)
Map 14-6  1☆ | 
Map 14-6  2☆ | 
Map 14-6  3☆ | Turtle Cub (4*)
Map 14-7  1☆ | 
Map 14-7  2☆ | 
Map 14-7  3☆ | Snow Giant (4*)
Map 14-8  1☆ | 
Map 14-8  2☆ | 
Map 14-8  3☆ | Magical Archer (4*)
Map 14-9  1☆ | 
Map 14-9  2☆ | 
Map 14-9  3☆ | Aranyani (4*)
Map 14-10 1☆ | 
Map 14-10 2☆ | 
Map 14-10 3☆ | Dark Punisher (4*)
Map 14-11 1☆ | Dandelion Elf (3*)
Map 14-11 2☆ | Magic Titan (4*)
Map 14-11 3☆ | DreamGoddess (5*)
Map 14-12 1☆ | Snow Giant (4*)
Map 14-12 2☆ | UmpirageTitan (4*)
Map 14-12 3☆ | Gaia (5*)
Map 15-1  1☆ | 
Map 15-1  2☆ | 
Map 15-1  3☆ | SeismicTurtle (4*)
Map 15-2  1☆ | 
Map 15-2  2☆ | 
Map 15-2  3☆ | BearWarrior (4*)
Map 15-3  1☆ | 
Map 15-3  2☆ | 
Map 15-3  3☆ | Voi (4*)
Map 15-4  1☆ | 
Map 15-4  2☆ | 
Map 15-4  3☆ | Orient Mesmer (4*)
Map 15-5  1☆ | 
Map 15-5  2☆ | 
Map 15-5  3☆ | Winged Princess (4*)
Map 15-6  1☆ | 
Map 15-6  2☆ | 
Map 15-6  3☆ | Ursine Shaman (4*)
Map 15-7  1☆ | 
Map 15-7  2☆ | 
Map 15-7  3☆ | Dancing Queen (4*)
Map 15-8  1☆ | 
Map 15-8  2☆ | 
Map 15-8  3☆ | High NightElf (4*)
Map 15-9  1☆ | 
Map 15-9  2☆ | 
Map 15-9  3☆ | Ghost Eye (4*)
Map 15-10 1☆ | 
Map 15-10 2☆ | 
Map 15-10 3☆ | Rhino Rider (4*)
Map 15-11 1☆ | Reindeer (3*)
Map 15-11 2☆ | Soul Reaper (5*)
Map 15-11 3☆ | Horn General (5*)
Map 15-12 1☆ | Demon Spirit (4*)
Map 15-12 2☆ | Azara (5*)
Map 15-12 3☆ | Moirai (5*)
Map 16-1  1☆ | 
Map 16-1  2☆ | 
Map 16-1  3☆ | Hydra Swordsman (4*)
Map 16-2  1☆ | 
Map 16-2  2☆ | 
Map 16-2  3☆ | Oni Slayer (4*)
Map 16-3  1☆ | 
Map 16-3  2☆ | 
Map 16-3  3☆ | Duke Locust (5*)
Map 16-4  1☆ | 
Map 16-4  2☆ | 
Map 16-4  3☆ | Spring Goddess (5*)
Map 16-5  1☆ | 
Map 16-5  2☆ | 
Map 16-5  3☆ | Dragon Master (5*)
Map 16-6  1☆ | 
Map 16-6  2☆ | 
Map 16-6  3☆ | Twlight Guardian (5*)
Map 16-7  1☆ | 
Map 16-7  2☆ | 
Map 16-7  3☆ | Iron Soldier (5*)
Map 16-8  1☆ | 
Map 16-8  2☆ | 
Map 16-8  3☆ | Solar Wolf (5*)
Map 16-9  1☆ | 
Map 16-9  2☆ | 
Map 16-9  3☆ | Lunar Wolf (5*)
Map 16-10 1☆ | 
Map 16-10 2☆ | 
Map 16-10 3☆ | Capricornus Priest (5*)
Map 16-11 1☆ | Tengu (5*)
Map 16-11 2☆ | Trial of Thor (5*)
Map 16-11 3☆ | Heaven Keeper (5*)
Map 16-12 1☆ | Ultra-Doll (5*)
Map 16-12 2☆ | Dream Guardian (5*)
Map 16-12 3☆ | Doom Diviner (5*)
Map 17-1  1☆ | 
Map 17-1  2☆ | 
Map 17-1  3☆ | Fallen Sister (4*)
Map 17-2  1☆ | 
Map 17-2  2☆ | 
Map 17-2  3☆ | Demeter (4*)
Map 17-3  1☆ | 
Map 17-3  2☆ | 
Map 17-3  3☆ | Ghost of Elves King (5*)
Map 17-4  1☆ | 
Map 17-4  2☆ | 
Map 17-4  3☆ | Guards of Sacred City (5*)
Map 17-5  1☆ | 
Map 17-5  2☆ | 
Map 17-5  3☆ | Goddess of Flowers (5*)
Map 17-6  1☆ | 
Map 17-6  2☆ | 
Map 17-6  3☆ | Shadow of Rainbow (5*)
Map 17-7  1☆ | 
Map 17-7  2☆ | 
Map 17-7  3☆ | Curlew (5*)
Map 17-8  1☆ | 
Map 17-8  2☆ | 
Map 17-8  3☆ | The Sirius (5*)
Map 17-9  1☆ | 
Map 17-9  2☆ | 
Map 17-9  3☆ | Red Fox (5*)
Map 17-10 1☆ | 
Map 17-10 2☆ | 
Map 17-10 3☆ | Forest Dragon (5*)
Map 17-11 1☆ | Chaos Dragon (5*)
Map 17-11 2☆ | Vulture Soul (5*)
Map 17-11 3☆ | Battle Terminator (5*)
Map 17-12 1☆ | Feathered Serpent (5*)
Map 17-12 2☆ | The Holy Elf Priest (5*)
Map 17-12 3☆ | Universal 5 Star Card (5*)
Map 18-1  1☆ | 
Map 18-1  2☆ | 
Map 18-1  3☆ | Ice Elementalist (4*)
Map 18-2  1☆ | 
Map 18-2  2☆ | 
Map 18-2  3☆ | Elf Elementalist (4*)
Map 18-3  1☆ | 
Map 18-3  2☆ | 
Map 18-3  3☆ | Elf Chanter (4*)
Map 18-4  1☆ | 
Map 18-4  2☆ | 
Map 18-4  3☆ | Dream Guardian (5*)
Map 18-5  1☆ | 
Map 18-5  2☆ | 
Map 18-5  3☆ | Holy Knight (5*)
Map 18-6  1☆ | 
Map 18-6  2☆ | 
Map 18-6  3☆ | Frigg (5*)
Map 18-7  1☆ | 
Map 18-7  2☆ | 
Map 18-7  3☆ | Freyja (5*)
Map 18-8  1☆ | 
Map 18-8  2☆ | 
Map 18-8  3☆ | Deviant Moon (5*)
Map 18-9  1☆ | 
Map 18-9  2☆ | 
Map 18-9  3☆ | Gluttony (5*)
Map 18-10 1☆ | 
Map 18-10 2☆ | 
Map 18-10 3☆ | Dawning Horn (5*)
Map 18-11 1☆ | Miss Fortune (5*)
Map 18-11 2☆ | Red Lotus Witch (5*)
Map 18-11 3☆ | Cait Sith (5*)
Map 18-12 1☆ | Love Messenger (5*)
Map 18-12 2☆ | Vampire Shinso (5*)
Map 18-12 3☆ | Universal 5 Star Card (5*)
Map 18-13 1☆ | Princess Sakura (5*)
Map 18-13 2☆ | Cunning Fiend (5*)
Map 18-13 3☆ | Universal 5 Star Card (5*), Basaker
Map 19-1  1☆ | 
Map 19-1  2☆ | 
Map 19-1  3☆ | Princess Night (5*)
Map 19-2  1☆ | 
Map 19-2  2☆ | 
Map 19-2  3☆ | Ghost Hunter (5*)
Map 19-3  1☆ | 
Map 19-3  2☆ | 
Map 19-3  3☆ | Sword Goddess (5*)
Map 19-4  1☆ | 
Map 19-4  2☆ | 
Map 19-4  3☆ | Dragon Summoner (5*)
Map 19-5  1☆ | 
Map 19-5  2☆ | 
Map 19-5  3☆ | Saber (5*)
Map 19-6  1☆ | 
Map 19-6  2☆ | 
Map 19-6  3☆ | Kamikaze Kengoo (5*)
Map 19-7  1☆ | 
Map 19-7  2☆ | 
Map 19-7  3☆ | Drow (5*)
Map 19-8  1☆ | 
Map 19-8  2☆ | 
Map 19-8  3☆ | Lord Brook (5*)
Map 19-9  1☆ | 
Map 19-9  2☆ | 
Map 19-9  3☆ | Lance (5*)
Map 19-10 1☆ | 
Map 19-10 2☆ | 
Map 19-10 3☆ | Fire Witch (5*)
Map 19-11 1☆ | Mecha Fiend (5*)
Map 19-11 2☆ | Janice Moon (5*)
Map 19-11 3☆ | Diviner (5*)
Map 19-12 1☆ | Mechanical KOF (5*)
Map 19-12 2☆ | Elf King (5*)
Map 19-12 3☆ | Universal 5 Star Card (5*)
Map 20-1  1☆ | 
Map 20-1  2☆ | 
Map 20-1  3☆ | Flower Warlock (4*)
Map 20-2  1☆ | 
Map 20-2  2☆ | 
Map 20-2  3☆ | Caster (5*)
Map 20-3  1☆ | 
Map 20-3  2☆ | 
Map 20-3  3☆ | Dexter (5*)
Map 20-4  1☆ | 
Map 20-4  2☆ | 
Map 20-4  3☆ | Fire Blade (5*)
Map 20-5  1☆ | 
Map 20-5  2☆ | 
Map 20-5  3☆ | Music Memoria (5*)
Map 20-6  1☆ | 
Map 20-6  2☆ | 
Map 20-6  3☆ | Starry Queen (5*)
Map 20-7  1☆ | 
Map 20-7  2☆ | 
Map 20-7  3☆ | Ghost Ranger (5*)
Map 20-8  1☆ | 
Map 20-8  2☆ | 
Map 20-8  3☆ | Cloris (5*)
Map 20-9  1☆ | 
Map 20-9  2☆ | 
Map 20-9  3☆ | Lord Brook (5*)
Map 20-10 1☆ | 
Map 20-10 2☆ | 
Map 20-10 3☆ | Kamikaze Kengoo (5*)
Map 20-11 1☆ | Dark Witch (5*)
Map 20-11 2☆ | Frostwolf Chieftain (5*)
Map 20-11 3☆ | The Last shepherd (5*), Hawkeye
Map 20-12 1☆ | Galaxy Swordman (5*)
Map 20-12 2☆ | Avenger (5*)
Map 20-12 3☆ | Universal 5 Star Card (5*)
Map 21-1  1☆ | 
Map 21-1  2☆ | 
Map 21-1  3☆ | Martyr (4*)
Map 21-2  1☆ | 
Map 21-2  2☆ | 
Map 21-2  3☆ | Ghost Falcon (4*)
Map 21-3  1☆ | 
Map 21-3  2☆ | 
Map 21-3  3☆ | Terrifying cactus (5*)
Map 21-4  1☆ | 
Map 21-4  2☆ | 
Map 21-4  3☆ | Nirvana Angel (5*)
Map 21-5  1☆ | 
Map 21-5  2☆ | 
Map 21-5  3☆ | Plague doctor (4*)
Map 21-6  1☆ | 
Map 21-6  2☆ | 
Map 21-6  3☆ | Gorgons (5*)
Map 21-7  1☆ | 
Map 21-7  2☆ | 
Map 21-7  3☆ | Fairy Lake (5*)
Map 21-8  1☆ | 
Map 21-8  2☆ | 
Map 21-8  3☆ | Persephone (5*)
Map 21-9  1☆ | 
Map 21-9  2☆ | 
Map 21-9  3☆ | Snow Elsie (5*)
Map 21-10 1☆ | 
Map 21-10 2☆ | 
Map 21-10 3☆ | Nidhogg (5*)
Map 21-11 1☆ | Music Memoria (5*)
Map 21-11 2☆ | Pneuma Master (5*)
Map 21-11 3☆ | Flora (5*), Rock
Map 21-12 1☆ | Magic Dragon (5*)
Map 21-12 2☆ | Ladon (5*)
Map 21-12 3☆ | Universal 5 Star Card (5*)
