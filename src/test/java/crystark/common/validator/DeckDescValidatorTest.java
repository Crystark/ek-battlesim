package crystark.common.validator;

import crystark.common.api.CardDesc;
import crystark.common.api.DeckDesc;
import crystark.common.format.CardDescFormat;
import crystark.ek.battlesim.task.TaskException;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class DeckDescValidatorTest {

	private static List<CardDesc> cards(String... names) {
		return Arrays.asList(names).stream()
			.map(CardDescFormat::parse)
			.collect(Collectors.toList());
	}

	@Test
	public void test_validate_OK() {
		DeckDesc dd;

		dd = new DeckDesc("deck", "player", 100, 100, cards("Scarab"), Collections.emptyList(), 0, false, false);
		DeckDescValidator.validate(dd);

		String customCancer = "Cancer, 1, 1, 1, 1, Tundra";
		dd = new DeckDesc("deck", "player", 100, 100, cards(customCancer, customCancer), Collections.emptyList(), 0, false, false);
		DeckDescValidator.validate(dd);
	}

	@Test(expected = TaskException.class)
	public void test_validate_KO_conflict() {
		DeckDesc dd = new DeckDesc("deck", "player", 100, 100, cards("Aries", "Virgo"), Collections.emptyList(), 0, false, false);

		DeckDescValidator.validate(dd);
	}

	@Test(expected = TaskException.class)
	public void test_validate_KO_limit() {
		DeckDesc dd = new DeckDesc("deck", "player", 100, 100, cards("Celestial Touchstone", "Celestial Touchstone"), Collections.emptyList(), 0, false, false);

		DeckDescValidator.validate(dd);
	}
}
