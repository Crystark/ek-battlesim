package crystark.common.format;

import org.junit.Assert;
import org.junit.Test;

import crystark.common.api.CardDesc;

/**
 *
 */
public class BuffParserTest {

    @Test
    public void test() {
        final CardDesc cardForest = CardDescFormat.parse("Random Forest, 0, 0, 1000, 2000, forest");
        final CardDesc cardMnt = CardDescFormat.parse("Random Mnt, 0, 00, 1000, 2000, mtn");
        final CardDesc cardSwamp = CardDescFormat.parse("Random Swamp, 0, 0, 1000, 2000, swamp");

        BuffParser buffParser = new BuffParser();
        buffParser
            .parseHealth("forest 200%;swamp 3")
            .parseAttack("mtn  0.5 ; swamp   200 % ; ");

        final CardDesc cardForestNew = buffParser.apply(cardForest);
        final CardDesc cardMntNew = buffParser.apply(cardMnt);
        final CardDesc cardSwampNew = buffParser.apply(cardSwamp);

        Assert.assertEquals(cardForest.health * 2, cardForestNew.health);
        Assert.assertEquals(cardMnt.health, cardMntNew.health);
        Assert.assertEquals(cardSwamp.health * 3, cardSwampNew.health);

        Assert.assertEquals(cardForest.attack, cardForestNew.attack);
        Assert.assertEquals((int) (cardMnt.attack * 0.5), cardMntNew.attack);
        Assert.assertEquals(cardSwamp.attack * 2, cardSwampNew.attack);
    }

    @Test(expected = IllegalArgumentException.class)
    public void incorrect1() {
        BuffParser buffParser = new BuffParser();
        buffParser.parseHealth("forest200%; swamp 3");
    }

    @Test(expected = IllegalArgumentException.class)
    public void incorrect2() {
        BuffParser buffParser = new BuffParser();
        buffParser.parseHealth("forest 200 swamp 3");
    }

    //@Test(expected = IllegalArgumentException.class)
}