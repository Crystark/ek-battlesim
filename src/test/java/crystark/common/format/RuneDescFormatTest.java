package crystark.common.format;

import org.junit.Assert;
import org.junit.Test;

import crystark.common.api.RuneDesc;

public class RuneDescFormatTest {
	@Test
	public void test_parse() {
		RuneDesc runeDesc;

		runeDesc = RuneDescFormat.parse("Thunder Shield");
		Assert.assertNotNull(runeDesc);
		Assert.assertEquals("Thunder Shield", runeDesc.name);
		Assert.assertEquals(4, runeDesc.times);
		Assert.assertEquals(200, runeDesc.value);

		runeDesc = RuneDescFormat.parse("  Thunder Shield  ");
		Assert.assertNotNull(runeDesc);
		Assert.assertEquals("Thunder Shield", runeDesc.name);
		Assert.assertEquals(4, runeDesc.times);
		Assert.assertEquals(200, runeDesc.value);

		runeDesc = RuneDescFormat.parse("Thunder Shield:300");
		Assert.assertNotNull(runeDesc);
		Assert.assertEquals("Thunder Shield", runeDesc.name);
		Assert.assertEquals(4, runeDesc.times);
		Assert.assertEquals(300, runeDesc.value);

		runeDesc = RuneDescFormat.parse("Thunder Shield:L2");
		Assert.assertNotNull(runeDesc);
		Assert.assertEquals("Thunder Shield", runeDesc.name);
		Assert.assertEquals(4, runeDesc.times);
		Assert.assertEquals(160, runeDesc.value);
	}

	@Test
	public void test_parse_addon_runes() {
		RuneDesc runeDesc;

		runeDesc = RuneDescFormat.parse("am envol:200");
		Assert.assertNotNull(runeDesc);
		Assert.assertEquals("AM Envol", runeDesc.name);
		Assert.assertEquals(4, runeDesc.times);
		Assert.assertEquals(200, runeDesc.value);
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_parse_KO_require_value() {
		RuneDescFormat.parse("am envol");
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_parse_KO_require_value2() {
		RuneDescFormat.parse("am envol:L4");
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_parse_KO_typo() {
		RuneDescFormat.parse("Thunder Shiel");
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_parse_KO_level() {
		RuneDescFormat.parse("Thunder Shield:L5");
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_parse_KO_value() {
		RuneDescFormat.parse("Thunder Shield:R5");
	}
}
