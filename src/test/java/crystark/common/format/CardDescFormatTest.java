package crystark.common.format;

import crystark.common.api.CardDesc;
import crystark.common.api.CardTokenType;
import crystark.common.api.ConflictType;
import crystark.common.db.Database;
import crystark.common.events.Warnings;
import crystark.ek.battlesim.battle.Card;
import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.atomic.AtomicBoolean;

public class CardDescFormatTest {
	@Test
	public void test_parseSimple() {
		CardDesc cardDesc;

		cardDesc = CardDescFormat.parseSimple("Fire Demon");
		Assert.assertNotNull(cardDesc);
		Assert.assertEquals("Fire Demon", cardDesc.name);
		Assert.assertEquals(10, cardDesc.level);
		Assert.assertEquals(14, cardDesc.cost);
		Assert.assertEquals(0, cardDesc.evolution);
		Assert.assertEquals(3, cardDesc.abilities.size());
		Assert.assertEquals(670, cardDesc.attack);
		Assert.assertEquals(1710, cardDesc.health);

		cardDesc = CardDescFormat.parseSimple("E:Fire Demon");
		Assert.assertNotNull(cardDesc);
		Assert.assertEquals("Fire Demon", cardDesc.name);
		Assert.assertEquals(10, cardDesc.level);
		Assert.assertEquals(14, cardDesc.cost);
		Assert.assertEquals(0, cardDesc.evolution);
		Assert.assertEquals(3, cardDesc.abilities.size());
		Assert.assertEquals(CardTokenType.event, cardDesc.tokens.get(0).type);
		Assert.assertEquals(2010, cardDesc.attack);
		Assert.assertEquals(5130, cardDesc.health);

		cardDesc = CardDescFormat.parseSimple("Fire Demon, 13");
		Assert.assertNotNull(cardDesc);
		Assert.assertEquals("Fire Demon", cardDesc.name);
		Assert.assertEquals(13, cardDesc.level);
		Assert.assertEquals(16, cardDesc.cost);
		Assert.assertEquals(3, cardDesc.evolution);
		Assert.assertEquals(3, cardDesc.abilities.size());

		cardDesc = CardDescFormat.parseSimple("Fire Demon, 6");
		Assert.assertNotNull(cardDesc);
		Assert.assertEquals("Fire Demon", cardDesc.name);
		Assert.assertEquals(6, cardDesc.level);
		Assert.assertEquals(14, cardDesc.cost);
		Assert.assertEquals(0, cardDesc.evolution);
		Assert.assertEquals(2, cardDesc.abilities.size());

		cardDesc = CardDescFormat.parseSimple("Fire Demon, 13+5");
		Assert.assertNotNull(cardDesc);
		Assert.assertEquals("Fire Demon", cardDesc.name);
		Assert.assertEquals(13, cardDesc.level);
		Assert.assertEquals(17, cardDesc.cost);
		Assert.assertEquals(5, cardDesc.evolution);
		Assert.assertEquals(3, cardDesc.abilities.size());

		cardDesc = CardDescFormat.parseSimple("Fire Demon, 15, Bloodsucker:90, Resistance");
		Assert.assertNotNull(cardDesc);
		Assert.assertEquals("Fire Demon", cardDesc.name);
		Assert.assertEquals(15, cardDesc.level);
		Assert.assertEquals(17, cardDesc.cost);
		Assert.assertEquals(5, cardDesc.evolution);
		Assert.assertEquals(5, cardDesc.abilities.size());

		cardDesc = CardDescFormat.parseSimple("  E :  Fire Demon  , 13  +  5  ");
		Assert.assertNotNull(cardDesc);
		Assert.assertEquals("Fire Demon", cardDesc.name);
		Assert.assertEquals(13, cardDesc.level);
		Assert.assertEquals(17, cardDesc.cost);
		Assert.assertEquals(5, cardDesc.evolution);
		Assert.assertEquals(3, cardDesc.abilities.size());
		Assert.assertEquals(CardTokenType.event, cardDesc.tokens.get(0).type);
	}

	@Test
	public void test_legendary() {
		CardDesc cardDesc;

		cardDesc = CardDescFormat.parseSimple("LG:Aranyani (Legendary), 15");
		Assert.assertNotNull(cardDesc);
		Assert.assertEquals("Aranyani (Legendary)", cardDesc.name);
		Assert.assertEquals(15, cardDesc.level);
		Assert.assertEquals(CardTokenType.legendary, cardDesc.tokens.get(0).type);
		Assert.assertEquals(1650, cardDesc.attack);
		Assert.assertEquals(124227000, cardDesc.health);

		cardDesc = CardDescFormat.parseSimple("LG:Aranyani");
		Assert.assertNotNull(cardDesc);
		Assert.assertEquals("Aranyani", cardDesc.name);
		Assert.assertEquals(10, cardDesc.level);
		Assert.assertEquals(CardTokenType.legendary, cardDesc.tokens.get(0).type);
		Assert.assertEquals(68, cardDesc.attack);
		Assert.assertEquals(171, cardDesc.health);

		cardDesc = CardDescFormat.parse("Giant Mud Larva (Legendary), 19, 20, 1650, 23015000, hydra, Destroy, Devil's Blade:2000, Evasion, Toxic Clouds:200, Resistance");
		Assert.assertNotNull(cardDesc);
		Assert.assertEquals("Giant Mud Larva (Legendary)", cardDesc.name);
		Assert.assertEquals(10, cardDesc.level);
		Assert.assertTrue(cardDesc.tokens.isEmpty());
		Assert.assertEquals(1650, cardDesc.attack);
		Assert.assertEquals(23015000, cardDesc.health);
	}

	@Test
	public void test_attackMultiplierToken() {
		CardDesc cardDesc;

		cardDesc = CardDescFormat.parseSimple("ATK#2.5:Voodoo Scarecrow");
		Assert.assertEquals(CardTokenType.attackMultiplier, cardDesc.tokens.get(0).type);
		Assert.assertEquals(1750, cardDesc.attack);
		Assert.assertEquals(1770, cardDesc.health);

		cardDesc = CardDescFormat.parseSimple("ATK#2:Voodoo Scarecrow");
		Assert.assertEquals(CardTokenType.attackMultiplier, cardDesc.tokens.get(0).type);
		Assert.assertEquals(1400, cardDesc.attack);
		Assert.assertEquals(1770, cardDesc.health);
	}

	@Test
	public void test_healthMultiplierToken() {
		CardDesc cardDesc;

		cardDesc = CardDescFormat.parseSimple("HP#2.5:Demonic Imp");
		Assert.assertEquals(CardTokenType.healthMultiplier, cardDesc.tokens.get(0).type);
		Assert.assertEquals(510, cardDesc.attack);
		Assert.assertEquals(2500, cardDesc.health);

		cardDesc = CardDescFormat.parseSimple("HP#2:Demonic Imp");
		Assert.assertEquals(CardTokenType.healthMultiplier, cardDesc.tokens.get(0).type);
		Assert.assertEquals(510, cardDesc.attack);
		Assert.assertEquals(2000, cardDesc.health);
	}

	@Test
	public void test_conflictToken() {
		CardDesc cardDesc;

		cardDesc = CardDescFormat.parse("CF#ds:Scarab");
		Assert.assertEquals(CardTokenType.conflict, cardDesc.tokens.get(0).type);
		Assert.assertEquals(ConflictType.ds, cardDesc.conflictType);

		cardDesc = CardDescFormat.parse("CF#Z1:Scarab");
		Assert.assertEquals(CardTokenType.conflict, cardDesc.tokens.get(0).type);
		Assert.assertEquals(ConflictType.z1, cardDesc.conflictType);

		cardDesc = CardDescFormat.parse("CF#z3:Fully Described, 1, 1, 1, 1, Special");
		Assert.assertEquals(CardTokenType.conflict, cardDesc.tokens.get(0).type);
		Assert.assertEquals(ConflictType.z3, cardDesc.conflictType);
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_parseSimple_KO_name() {
		CardDescFormat.parseSimple("Fire Demo");
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_parseSimple_KO_level() {
		CardDescFormat.parseSimple("Fire Demon, 1E");
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_parseSimple_KO_level2() {
		CardDescFormat.parseSimple("Fire Demon, 20");
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_parseSimple_KO_evo() {
		CardDescFormat.parseSimple("Fire Demon, 13+9");
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_parseSimple_KO_token() {
		CardDescFormat.parseSimple("WRONG:Fire Demon, 13+9");
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_parseSimple_KO_evo2() {
		CardDescFormat.parseSimple("Fire Demon, 13+A");
	}

	// TODO
	//	@Test(expected = IllegalArgumentException.class)
	//	public void test_parseSimple_KO_wrong_format() {
	//		SimpleCardDescFormat.parseSimple("Fire Demon, 13, 6, 830, 1890, mtn, Concentration:120, Laceration, Immunity, Bloodsucker:90");
	//	}

	@Test
	public void test_allCardDescription() {
		AtomicBoolean failed = new AtomicBoolean();

		Database.get().cards().all()
			.forEach(card -> {
				try {
					CardDesc cd = CardDescFormat.parseFull(CardDescFormat.toFullDescription(card.createCardDesc()));
					Assert.assertNotNull(cd);
					new Card(cd, null);
				}
				catch (Exception e) {
					failed.set(true);
					System.out.println("Card parsing failed for " + card.name() + " because of \"" + e.getMessage() + "\"");
				}
			});

		Warnings.getAndReset().subscribe(System.out::println);
		Assert.assertFalse("Some cards can't be used", failed.get());
	}

	@Test
	public void test_parseFull() {
		CardDesc cardDesc;

		cardDesc = CardDescFormat.parseFull("Fire Demon, 17, 6, 830, 1890, mtn, Concentration:120, Laceration, Immunity, Bloodsucker:90");
		Assert.assertNotNull(cardDesc);

		cardDesc = CardDescFormat.parseFull("Fire Demon, 17, 6, 830, 1890, mtn");
		Assert.assertNotNull(cardDesc);

		cardDesc = CardDescFormat.parseFull("DI:Fire Demon, 17, 6, 830, 1890, mtn");
		Assert.assertNotNull(cardDesc);
		Assert.assertEquals(CardTokenType.diMerit, cardDesc.tokens.get(0).type);

		cardDesc = CardDescFormat.parseFull("  Fire Demon  , 17  , 6  , 830  , 1890  , mtn  ");
		Assert.assertNotNull(cardDesc);
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_parseFull_KO_cost() {
		CardDescFormat.parseFull("Fire Demon, WRONG, 6, 830, 1890, mtn, Concentration:120, Laceration, Immunity, Bloodsucker:90");
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_parseFull_KO_timer() {
		CardDescFormat.parseFull("Fire Demon, 17, WRONG, 830, 1890, mtn, Concentration:120, Laceration, Immunity, Bloodsucker:90");
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_parseFull_KO_atk() {
		CardDescFormat.parseFull("Fire Demon, 17, 6, WRONG, 1890, mtn, Concentration:120, Laceration, Immunity, Bloodsucker:90");
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_parseFull_KO_health() {
		CardDescFormat.parseFull("Fire Demon, 17, 6, 830, WRONG, mtn, Concentration:120, Laceration, Immunity, Bloodsucker:90");
	}

	@Test(expected = FormatException.class)
	public void test_parseFull_KO_type() {
		CardDescFormat.parseFull("Fire Demon, 17, 6, 830, 1890, WRONG, Concentration:120, Laceration, Immunity, Bloodsucker:90");
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_parseFull_KO_token() {
		CardDescFormat.parseFull("WRONG:Fire Demon, 17, 6, 830, 1890, mtn, Concentration:120, Laceration, Immunity, Bloodsucker:90");
	}

	@Test(expected = FormatException.class)
	public void test_parseFull_KO_missing_value2() {
		CardDescFormat.parseFull("Keeper of the light, 18, 2, 675, 1680, tundra, Resurrection:55, Frost Shock:140, QS_Teleportation");
	}

	@Test(expected = FormatException.class)
	public void test_parse_KO_missing_value2() {
		CardDescFormat.parse("Keeper of the light, 18, 2, 675, 1680, tundra, Resurrection:55, Frost Shock:140, QS_Teleportation");
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_parseFull_KO_wrong_format() {
		CardDescFormat.parseFull("Fire Demon");
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_parseFull_KO_wrong_format2() {
		CardDescFormat.parseFull("Fire Demon, 10");
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_parseFull_KO_wrong_format3() {
		CardDescFormat.parseFull("Fire Demon, 10, Concentration:120, Laceration, Immunity, Bloodsucker:90");
	}
}
