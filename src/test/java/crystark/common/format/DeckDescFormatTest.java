package crystark.common.format;

import crystark.common.api.DeckDesc;
import crystark.ek.battlesim.battle.Player;
import org.junit.Assert;
import org.junit.Test;

import java.util.stream.Stream;

import static crystark.tools.Tools.file;

public class DeckDescFormatTest {
	@Test
	public void test_parseFile() {
		DeckDesc deckDesc = DeckDescFormat.parseFile(file("_test_simple_deck_desc_format_.txt"));
		Assert.assertEquals("Crystark", deckDesc.playerName);
		Assert.assertEquals(103, (int) deckDesc.playerLevel);
		Assert.assertTrue(deckDesc.alwaysStart);
		Assert.assertTrue(deckDesc.skipShuffle == Integer.MAX_VALUE);
		Assert.assertEquals(5, deckDesc.cardsDescs.size());
		Assert.assertEquals(3, deckDesc.runesDescs.size());

		Player.from(deckDesc);
	}

	@Test
	public void test_toDeckString() {
		DeckDesc deckDescOrigin = DeckDescFormat.parseFile(file("_test_simple_deck_desc_format_.txt"));

		String string = DeckDescFormat.toDeckString(deckDescOrigin, "Unit Testing");

		DeckDesc deckDesc = DeckDescFormat.parseString("test", string);

		Assert.assertEquals(deckDescOrigin.deckName, deckDesc.deckName);
		Assert.assertEquals(deckDescOrigin.playerName, deckDesc.playerName);
		Assert.assertEquals(deckDescOrigin.playerLevel, deckDesc.playerLevel);
		Assert.assertEquals(deckDescOrigin.cardsDescs.size(), deckDesc.cardsDescs.size());
		Assert.assertEquals(deckDescOrigin.runesDescs.size(), deckDesc.runesDescs.size());
		Assert.assertEquals(deckDescOrigin.skipShuffle, deckDesc.skipShuffle);
		Assert.assertEquals(deckDescOrigin.alwaysStart, deckDesc.alwaysStart);
		Assert.assertEquals(deckDescOrigin.infiniteHp, deckDesc.infiniteHp);
	}

	@Test
	public void test_parseBOM() {
		DeckDescFormat.parseLines(null, Stream.of('\ufeff' + "# Nothing"));
	}
}
