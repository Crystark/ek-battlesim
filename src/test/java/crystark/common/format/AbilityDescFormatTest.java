package crystark.common.format;

import org.junit.Assert;
import org.junit.Test;

import crystark.common.api.AbilityDesc;
import crystark.common.api.LaunchType;

public class AbilityDescFormatTest {
	@Test
	public void test_parse() {
		AbilityDesc abilityDesc;

		abilityDesc = AbilityDescFormat.parse("Exile");
		Assert.assertNotNull(abilityDesc);
		Assert.assertEquals("Exile", abilityDesc.name);
		Assert.assertEquals(LaunchType.normal, abilityDesc.launchType);
		Assert.assertNull(abilityDesc.value);
		Assert.assertFalse(abilityDesc.isUseless);

		abilityDesc = AbilityDescFormat.parse("QS_Exile");
		Assert.assertNotNull(abilityDesc);
		Assert.assertEquals("Exile", abilityDesc.name);
		Assert.assertEquals(LaunchType.quickstrike, abilityDesc.launchType);
		Assert.assertNull(abilityDesc.value);
		Assert.assertFalse(abilityDesc.isUseless);

		abilityDesc = AbilityDescFormat.parse("Quick Strike: Exile");
		Assert.assertNotNull(abilityDesc);
		Assert.assertEquals("Exile", abilityDesc.name);
		Assert.assertEquals(LaunchType.quickstrike, abilityDesc.launchType);
		Assert.assertNull(abilityDesc.value);
		Assert.assertFalse(abilityDesc.isUseless);

		abilityDesc = AbilityDescFormat.parse("PS_Regeneration 10");
		Assert.assertNotNull(abilityDesc);
		Assert.assertEquals("Regeneration", abilityDesc.name);
		Assert.assertEquals(LaunchType.preemptiveStrike, abilityDesc.launchType);
		Assert.assertEquals(250, abilityDesc.value);
		Assert.assertFalse(abilityDesc.isUseless);

		abilityDesc = AbilityDescFormat.parse("Preemptive Strike: Regeneration 10");
		Assert.assertNotNull(abilityDesc);
		Assert.assertEquals("Regeneration", abilityDesc.name);
		Assert.assertEquals(LaunchType.preemptiveStrike, abilityDesc.launchType);
		Assert.assertEquals(250, abilityDesc.value);
		Assert.assertFalse(abilityDesc.isUseless);

		abilityDesc = AbilityDescFormat.parse("DW_Regeneration 10");
		Assert.assertNotNull(abilityDesc);
		Assert.assertEquals("Regeneration", abilityDesc.name);
		Assert.assertEquals(LaunchType.deathWhisper, abilityDesc.launchType);
		Assert.assertEquals(250, abilityDesc.value);
		Assert.assertFalse(abilityDesc.isUseless);

		abilityDesc = AbilityDescFormat.parse("Death Whisper: Regeneration 10");
		Assert.assertNotNull(abilityDesc);
		Assert.assertEquals("Regeneration", abilityDesc.name);
		Assert.assertEquals(LaunchType.deathWhisper, abilityDesc.launchType);
		Assert.assertEquals(250, abilityDesc.value);
		Assert.assertFalse(abilityDesc.isUseless);

		abilityDesc = AbilityDescFormat.parse("Blizzard:300");
		Assert.assertNotNull(abilityDesc);
		Assert.assertEquals("Blizzard", abilityDesc.name);
		Assert.assertEquals(LaunchType.normal, abilityDesc.launchType);
		Assert.assertEquals(300, abilityDesc.value);
		Assert.assertFalse(abilityDesc.isUseless);

		abilityDesc = AbilityDescFormat.parse("  Blizzard  :  300  ");
		Assert.assertNotNull(abilityDesc);
		Assert.assertEquals("Blizzard", abilityDesc.name);
		Assert.assertEquals(LaunchType.normal, abilityDesc.launchType);
		Assert.assertEquals(300, abilityDesc.value);
		Assert.assertFalse(abilityDesc.isUseless);

		abilityDesc = AbilityDescFormat.parse("Blizzard 10");
		Assert.assertNotNull(abilityDesc);
		Assert.assertEquals("Blizzard", abilityDesc.name);
		Assert.assertEquals(LaunchType.normal, abilityDesc.launchType);
		Assert.assertEquals(200, abilityDesc.value);
		Assert.assertFalse(abilityDesc.isUseless);

		abilityDesc = AbilityDescFormat.parse("D_Blizzard 10");
		Assert.assertNotNull(abilityDesc);
		Assert.assertEquals("Blizzard", abilityDesc.name);
		Assert.assertEquals(LaunchType.desperation, abilityDesc.launchType);
		Assert.assertEquals(200, abilityDesc.value);
		Assert.assertFalse(abilityDesc.isUseless);

		abilityDesc = AbilityDescFormat.parse("Desperation:  Blizzard 10");
		Assert.assertNotNull(abilityDesc);
		Assert.assertEquals("Blizzard", abilityDesc.name);
		Assert.assertEquals(LaunchType.desperation, abilityDesc.launchType);
		Assert.assertEquals(200, abilityDesc.value);
		Assert.assertFalse(abilityDesc.isUseless);

		abilityDesc = AbilityDescFormat.parse("  Blizzard 10  ");
		Assert.assertNotNull(abilityDesc);
		Assert.assertEquals("Blizzard", abilityDesc.name);
		Assert.assertEquals(LaunchType.normal, abilityDesc.launchType);
		Assert.assertEquals(200, abilityDesc.value);
		Assert.assertFalse(abilityDesc.isUseless);

		abilityDesc = AbilityDescFormat.parse("Reanimation");
		Assert.assertNotNull(abilityDesc);
		Assert.assertEquals("Reanimation", abilityDesc.name);
		Assert.assertEquals(LaunchType.normal, abilityDesc.launchType);
		Assert.assertNull(abilityDesc.value);
		Assert.assertFalse(abilityDesc.isUseless);

		abilityDesc = AbilityDescFormat.parse("D_Reanimation");
		Assert.assertNotNull(abilityDesc);
		Assert.assertEquals("Reanimation", abilityDesc.name);
		Assert.assertEquals(LaunchType.desperation, abilityDesc.launchType);
		Assert.assertNull(abilityDesc.value);
		Assert.assertFalse(abilityDesc.isUseless);

		abilityDesc = AbilityDescFormat.parse("Lavish Dinner");
		Assert.assertNotNull(abilityDesc);
		Assert.assertEquals("Lavish Dinner", abilityDesc.name);
		Assert.assertEquals(LaunchType.normal, abilityDesc.launchType);
		Assert.assertNull(abilityDesc.value);
		Assert.assertTrue(abilityDesc.isUseless);

		abilityDesc = AbilityDescFormat.parse("Summon\\: Treant Guardian:Elf Guard & Elf Captain");
		Assert.assertNotNull(abilityDesc);
		Assert.assertEquals("Summon: Treant Guardian", abilityDesc.name);
		Assert.assertEquals(LaunchType.normal, abilityDesc.launchType);
		Assert.assertEquals("Elf Guard & Elf Captain", abilityDesc.value);
		Assert.assertFalse(abilityDesc.isUseless);

		abilityDesc = AbilityDescFormat.parse("Exile & Teleport:Quick Strike\\: Exile & Teleportation");
		Assert.assertNotNull(abilityDesc);
		Assert.assertEquals("Exile & Teleport", abilityDesc.name);
		Assert.assertEquals(LaunchType.normal, abilityDesc.launchType);
		Assert.assertEquals("Quick Strike: Exile & Teleportation", abilityDesc.value);
		Assert.assertFalse(abilityDesc.isUseless);
	}

	@Test(expected = FormatException.class)
	public void test_parse_KO_require_value() {
		AbilityDescFormat.parse("Summon");
	}

	@Test(expected = FormatException.class)
	public void test_parse_KO_missing_value() {
		AbilityDescFormat.parse("Snipe");
	}

	@Test(expected = FormatException.class)
	public void test_parse_KO_typo() {
		AbilityDescFormat.parse("Resistanc");
	}
}
