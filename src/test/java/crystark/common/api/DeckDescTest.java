package crystark.common.api;

import crystark.common.db.model.Deck;
import crystark.common.format.DeckDescFormat;
import org.junit.Assert;
import org.junit.Test;

import static crystark.tools.Tools.file;

public class DeckDescTest {
	@Test
	public void test_calcCost() {
		Assert.assertEquals(13, DeckDesc.calcCost(1));
		Assert.assertEquals(40, DeckDesc.calcCost(10));
		Assert.assertEquals(70, DeckDesc.calcCost(20));
		Assert.assertEquals(90, DeckDesc.calcCost(30));
		Assert.assertEquals(110, DeckDesc.calcCost(40));
		Assert.assertEquals(130, DeckDesc.calcCost(50));
		Assert.assertEquals(140, DeckDesc.calcCost(60));
		Assert.assertEquals(180, DeckDesc.calcCost(100));
	}

	// TODO regroup methods
	@Test
	public void test_calcHp() {
		Assert.assertEquals(1000, Deck.calcHp(1));
		Assert.assertEquals(4900, Deck.calcHp(40));
		Assert.assertEquals(14350, Deck.calcHp(90));
		Assert.assertEquals(15400, Deck.calcHp(91));
		Assert.assertEquals(17160, Deck.calcHp(102));
		Assert.assertEquals(23240, Deck.calcHp(140));
		Assert.assertEquals(23540, Deck.calcHp(141));
		Assert.assertEquals(26240, Deck.calcHp(150));
	}

	@Test
	public void test_getTotalPower() {
		Assert.assertEquals(28531, DeckDescFormat.parseFile(file("decks/kw/fd_56.txt")).getTotalPower());
	}
}
