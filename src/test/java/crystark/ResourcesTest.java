package crystark;

import com.google.gson.Gson;
import crystark.common.api.CardDesc;
import crystark.common.api.CardType;
import crystark.common.api.Game;
import crystark.common.api.LaunchType;
import crystark.common.api.Server;
import crystark.common.db.Database;
import crystark.common.db.model.BaseCard;
import crystark.common.db.model.BaseMap;
import crystark.common.db.model.BaseRune;
import crystark.common.db.model.BaseSkill;
import crystark.common.events.Warnings;
import crystark.common.format.CardDescFormat;
import crystark.ek.Constants;
import crystark.ek.DataFetcher;
import crystark.ek.DataFetcherFactory;
import crystark.ek.EkbsConfig;
import crystark.ek.MrDataFetcher;
import crystark.ek.battlesim.ability.AbilityCondition;
import crystark.ek.battlesim.ability.AbilityFactory;
import crystark.ek.battlesim.ability.addons.AbilityProvider;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.rune.RuneFactory;
import crystark.tools.Tools;
import org.junit.After;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;

import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintStream;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
@RunWith(Parameterized.class)
public class ResourcesTest {
	private static final Logger L = LoggerFactory.getLogger(ResourcesTest.class);

	@Parameterized.Parameters(name = "{0}")
	public static List<Game> parameters(){
		return Arrays.asList(Game.values());
	}
	@Parameterized.Parameter
	public Game testGame;

	@After
	public void switchToDefaultGame(){
		EkbsConfig.current().switchGame(Game.mr);
	}

	@Test
	public void rebuild_resources() {
		boolean isCacharsGood = true;

		RuntimeException firstException = null;


		try {
			System.out.println("================ RESOURCES UPDATE of '" + testGame + "' ======================");
			isCacharsGood = rebuild_GameResources(testGame);
		}
		catch (RuntimeException e) {
			L.error(testGame + " resource update failure", e);
			if (firstException == null) firstException = e;
		}

		if (firstException != null) throw firstException;
		Assert.assertTrue("Some user files has wrong chars. Check log for details.", isCacharsGood);
	}

	private boolean rebuild_GameResources(Game game) {
		boolean isCacharsGood = true;

		final EkbsConfig ekbsConfig = EkbsConfig.current();
		ekbsConfig.switchGame(game);
		String RES_ROOT = System.getProperty("user.dir") + "/dist/" + ekbsConfig.game() + '/';

		DataFetcher df = DataFetcherFactory.get(ekbsConfig.defaultServer());

		try {
			Database.writeDb(ekbsConfig.game(), df.getCardsData(), df.getSkillsData(), df.getRunesData(), df.getOrderedMapsData());
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}

		String FILE_CARDS_SUPPORTED = RES_ROOT + "supported.cards.txt";
		String FILE_SKILLS_SUPPORTED = RES_ROOT + "supported.skills.txt";
		String FILE_RUNES_SUPPORTED = RES_ROOT + "supported.runes.txt";
		String FILE_EVOLVE_REQ_LIST = RES_ROOT + "list.evolve_cards.txt";
		String FILE_EVOLVES_LIST = RES_ROOT + "list.evolve_skills.txt";
		String FILE_SKILLS = RES_ROOT + "list.skills.txt";
		String FILE_RUNES = RES_ROOT + "list.runes.txt";
		String FILE_CARDS = RES_ROOT + "list.cards.csv";
		String FILE_COMPOSITE_SKILLS = RES_ROOT + "list.composite_skills.txt";
		String FILE_SUMMON_SKILLS = RES_ROOT + "list.summon_skills.txt";
		String FILE_JUDGEMENT_SKILLS = RES_ROOT + "list.judgement_skills.txt";
		String FILE_MAP_REWARDS = RES_ROOT + "list.map_rewards.txt";

		updateSupportedCards(FILE_CARDS_SUPPORTED);
		isCacharsGood &= checkChars(FILE_CARDS_SUPPORTED);

		updateSupportedSkills(FILE_SKILLS_SUPPORTED);
		isCacharsGood &= checkChars(FILE_SKILLS_SUPPORTED);

		updateSupportedRunes(FILE_RUNES_SUPPORTED);
		isCacharsGood &= checkChars(FILE_RUNES_SUPPORTED);

		if (game == Game.mr || game == Game.mrn) {
			updateEvolveReqList(FILE_EVOLVE_REQ_LIST);
			isCacharsGood &= checkChars(FILE_EVOLVE_REQ_LIST);

			updateEvolvesList(FILE_EVOLVES_LIST);
			isCacharsGood &= checkChars(FILE_EVOLVES_LIST);
		}

		updateSkillsList(FILE_SKILLS);
		isCacharsGood &= checkChars(FILE_SKILLS);

		updateRunesList(FILE_RUNES);
		isCacharsGood &= checkChars(FILE_RUNES);

		updateCardsList(game, FILE_CARDS);
		//isCacharsGood &= checkChars(FILE_CARDS);

		updateCompositeSkillsList(FILE_COMPOSITE_SKILLS);
		isCacharsGood &= checkChars(FILE_COMPOSITE_SKILLS);

		updateSummonSkillsList(FILE_SUMMON_SKILLS);
		isCacharsGood &= checkChars(FILE_SUMMON_SKILLS);

		updateJudgementSkillsList(FILE_JUDGEMENT_SKILLS);
		isCacharsGood &= checkChars(FILE_JUDGEMENT_SKILLS);

		updateMapRewardsList(FILE_MAP_REWARDS);
		//isCacharsGood &= checkChars(FILE_MAP_REWARDS);


		return isCacharsGood;
	}

	public boolean checkChars(String fileName){
		//String s = "：',3`";
		final CharsetEncoder asciiEnc = StandardCharsets.US_ASCII.newEncoder();
		final LineNumberReader fileReader;
		final boolean[] result = { true };
		try {
			fileReader = new LineNumberReader(new FileReader(fileName));
			fileReader.lines()
				.filter(l -> !asciiEnc.canEncode(l))
				.forEach(l -> {
					result[0] = false;
					int pos = -1;
					for(int i = 0; i < l.length(); i++){
						if (!asciiEnc.canEncode(l.charAt(i))) {
							pos = i + 1;
							break;
						}
					}
					System.out.println("Wrong chars in file '" + fileName + "' line " + fileReader.getLineNumber() + " column " + pos);
					System.out.println(l);
				}
			);
			fileReader.close();
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}
		return result[0];
	}

	@Test
	@Ignore
	public void changeNickname() {
		MrDataFetcher mdf = new MrDataFetcher(Server.apollo, "Coconutella", "");
		try {
			System.out.println(mdf.changeNick("Coconutella"));
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void updateMapRewardsList(String FILE_MAP_REWARDS) {
		String format = "Map %-5s %d☆ | %s";
		try (PrintStream ps = Tools.filePrintStream(FILE_MAP_REWARDS)) {
			Database.get().maps().all()
				.filter(BaseMap::hasLevels)
				.flatMap(m -> Observable.from(m.getLevels())
				.map(l -> String.format(format, m.number(), l.id(), l.firstWinReward().toString())))
				.toBlocking()
				.forEach(ps::println);
		}
	}

	@Test
	@Ignore
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void print_ew_data() {
		Pattern p = Pattern.compile("^\\[?EK] ?");

		String s = "";
		Gson g = new Gson();
		Map<String, Map<String, List<Map>>> map = g.fromJson(s, Map.class);
		List<Map> hits = map.get("data").get("ranks")
			.stream()
			.sorted((a, b) -> ((String) a.get("name")).compareToIgnoreCase((String) b.get("name")))
			.collect(Collectors.toList());
		List<Map> damage = map.get("data").get("hurtHpRanks")
			.stream()
			.sorted((a, b) -> ((String) a.get("name")).compareToIgnoreCase((String) b.get("name")))
			.collect(Collectors.toList());
		//			.forEach(e -> System.out.println(e.create("name") + "\t" + e.create("num")));

		Observable.from(hits)
			.zipWith(Observable.from(damage), (h, d) -> new String[] {
				(String) h.get("num"),
				(String) d.get("num"),
				(String) h.get("name") })
			.doOnNext(o -> o[2] = p.matcher(o[2]).replaceAll(""))
			.toSortedList((a, b) -> a[2].compareToIgnoreCase(b[2]))
			.flatMap(Observable::from)
			.toBlocking()
			.subscribe(n -> {
				System.out.println(n[0] + '\t' + n[1] + '\t' + n[2]);
			});
	}

	@Test
	@Ignore
	public void print_all_cards() {
		PrintStream ps = System.out;
		String T = "\t";
		Database.get().cards().all()
			.filter(card -> card.stars() == 5 && !card.type().equals(CardType.demon) && !card.type().equals(CardType.hydra))
			.forEach(c -> {
				ps.println(c.id() + T + c.name() + T + c.attackAt(10) + T + c.hpAt(10) + T + c.attackAt(15) + T + c.hpAt(15));
			});
	}

	private static void updateEvolveReqList(String FILE_EVOLVE_REQ_LIST) {
		final AtomicInteger longuestName = new AtomicInteger();
		Map<String, EvoData> evos = Database.get().cards()
			.all()
			.filter(BaseCard::canEvo)
			.map((card) -> {
				if (card.name().length() > longuestName.get()) {
					longuestName.set(card.name().length());
				}
				String secondaryName = card.secondaryEvolveCard() == null ? "UnknownCardId[" + card.id() + "]" : card.secondaryEvolveCard().name();
				return new EvoData(card.name(), secondaryName, card.secondaryEvolveCount());
			})
			.distinct(e -> e.cardName)
			.toMap(e -> e.cardName)
			.toBlocking()
			.first();

		for (Entry<String, EvoData> entry : evos.entrySet()) {
			EvoData evoData = entry.getValue();
			EvoData material = evos.get(evoData.evolvedWith);
			if (material != null) {
				material.addMaterialTo(evoData);
			}
			else {
				L.warn("Card " + evoData.evolvedWith + " cannot be evolved but is used as a material for " + evoData.cardName);
			}
		}

		List<EvoData> list = Observable
			.from(evos.values())
			.toSortedList((t1, t2) -> t1.cardName.compareTo(t2.cardName))
			.toBlocking()
			.first();

		String pattern = "%-" + longuestName.get() + "s | %d x %-" + longuestName.get() + "s | material to: ";
		String patternMaterial = "%s (x%d)";

		try (PrintStream ps = Tools.filePrintStream(FILE_EVOLVE_REQ_LIST)) {
			for (EvoData evoData : list) {
				StringBuilder sb = new StringBuilder(String.format(pattern, evoData.cardName, evoData.evovleCount, evoData.evolvedWith));
				if (!evoData.materialTo.isEmpty()) {
					for (EvoData materialTo : evoData.materialTo) {
						sb
							.append(String.format(patternMaterial, materialTo.cardName, materialTo.evovleCount))
							.append(", ");
					}
					sb.delete(sb.length() - 2, sb.length());
				}
				ps.println(sb.toString());
			}
		}
	}

	private static void updateEvolvesList(String FILE_EVOLVES_LIST) {
		List<BaseSkill> skills =
			Database.get().skills()
				.all()
				.filter(BaseSkill::isEvolvePossibility)
				.toSortedList((s1, s2) -> {
					Integer compareEvoRank = Integer.compare(s1.evoRank(), s2.evoRank());
					if (compareEvoRank != 0) {
						return compareEvoRank;
					}
					return s1.name().compareTo(s2.name());
				})
				.toBlocking()
				.first();

		int lastEvoRank = 0;
		try (PrintStream ps = Tools.filePrintStream(FILE_EVOLVES_LIST)) {
			for (BaseSkill baseSkill : skills) {
				if (baseSkill.evoRank() != lastEvoRank) {
					if (lastEvoRank != 0) {
						ps.println();
					}
					lastEvoRank = baseSkill.evoRank();
					ps.println("# Rank " + lastEvoRank + " evolutions");
				}
				ps.println(baseSkill.name());
			}
		}
	}

	private static void updateSkillsList(String FILE_SKILLS) {
		final AtomicInteger longuestName = new AtomicInteger();
		List<BaseSkill> skills =
			Database.get().skills()
				.all()
				.filter(BaseSkill::isBase)
				.filter(s -> !s.isUseless())
				.doOnNext(skill -> {
					if (skill.name().length() > longuestName.get()) {
						longuestName.set(skill.name().length());
					}
				})
				.toSortedList()
				.toBlocking()
				.first();

		String pattern = "%-" + longuestName.get() + "s | %s";

		try (PrintStream ps = Tools.filePrintStream(FILE_SKILLS)) {
			for (BaseSkill baseSkill : skills) {
				ps.println(String.format(pattern, baseSkill.name(), baseSkill.desc()));
			}
		}
	}

	private static void updateRunesList(String FILE_RUNES) {
		final AtomicInteger longuestName = new AtomicInteger();
		final AtomicInteger longuestCond = new AtomicInteger();
		final AtomicInteger longuestSkill = new AtomicInteger();
		final AtomicInteger longuestSkillDesc = new AtomicInteger();
		List<BaseRune> runes;

		runes =
			Database.get().runes()
				.all()
				.filter(BaseRune::isBase)
				.doOnNext(rune -> {
					if (rune.name().length() > longuestName.get()) {
						longuestName.set(rune.name().length());
					}
					if (rune.condition().length() > longuestCond.get()) {
						longuestCond.set(rune.condition().length());
					}
					BaseSkill skill4 = rune.skillsAt(4);
					if (skill4 == null)
						return;
					if (skill4.name().length() > longuestSkill.get()) {
						longuestSkill.set(skill4.name().length());
					}
					if (skill4.desc().length() > longuestSkillDesc.get()) {
						longuestSkillDesc.set(skill4.desc().length());
					}
				})
				.toSortedList()
				.toBlocking()
				.first();


		String pattern = "%-" + longuestName.get() + "s | %s | %-" + longuestCond.get() + "s | %-" + longuestSkill.get() + "s | %-" + longuestSkillDesc.get() + 's';

		try (PrintStream ps = Tools.filePrintStream(FILE_RUNES)) {
			String header = String.format(pattern, "Rune name", "T", "Condition", "Skill name", "Skill desc");
			ps.println(header.trim());
			ps.println(String.join("", Collections.nCopies(header.length(), "-")));
			for (BaseRune baseRune : runes) {
				BaseSkill skill4 = baseRune.skillsAt(4);
				if (skill4 == null) {
					ps.println(String.format(pattern, baseRune.name(), baseRune.times(), baseRune.condition(), "<Unknown skill>", "<Unknown skill>").trim());
				}
				else {
					ps.println(String.format(pattern, baseRune.name(), baseRune.times(), baseRune.condition(), skill4.name(), skill4.desc()).trim());
				}
			}
		}
	}

	private static void updateCompositeSkillsList(String FILE_COMPOSITE_SKILLS) {
		final AtomicInteger longuestName = new AtomicInteger();
		List<BaseSkill> skills =
			Database.get().skills()
				.all()
				.filter(BaseSkill::isComposite)
				.doOnNext(skill -> {
					if (skill.name().length() > longuestName.get()) {
						longuestName.set(skill.name().length());
					}
				})
				.toSortedList()
				.toBlocking()
				.first();

		String pattern = "%-" + longuestName.get() + "s | %s";

		try (PrintStream ps = Tools.filePrintStream(FILE_COMPOSITE_SKILLS)) {
			for (BaseSkill baseSkill : skills) {
				ps.println(String.format(pattern, baseSkill.name(), baseSkill.value()));
			}
		}
	}

	private static void updateSummonSkillsList(String FILE_SUMMON_SKILLS) {
		final AtomicInteger longuestName = new AtomicInteger();
		List<BaseSkill> skills =
			Database.get().skills()
				.all()
				.filter(BaseSkill::isSummon)
				.doOnNext(skill -> {
					if (skill.name().length() > longuestName.get()) {
						longuestName.set(skill.name().length());
					}
				})
				.toSortedList()
				.toBlocking()
				.first();

		String pattern = "%-" + longuestName.get() + "s | %-2s | %s";

		try (PrintStream ps = Tools.filePrintStream(FILE_SUMMON_SKILLS)) {
			for (BaseSkill baseSkill : skills) {
				ps.println(String.format(pattern, baseSkill.name(), Objects.toString(baseSkill.launchType().prefix(), "-"), baseSkill.value()));
			}
		}
	}

	private static void updateJudgementSkillsList(String FILE_JUDGEMENT_SKILLS) {
		final AtomicInteger longuestName = new AtomicInteger();
		final AtomicInteger longuestSkill = new AtomicInteger();
		List<BaseSkill> skills =
			Database.get().skills()
				.all()
				.filter(BaseSkill::isJudgement)
				.doOnNext(skill -> {
					try {

						if (skill.name().length() > longuestName.get()) {
							longuestName.set(skill.name().length());
						}
						int len = ((String) skill.value2()).length();
						if (len > longuestSkill.get()) {
							longuestSkill.set(len);
						}
					}
					catch (NullPointerException e) {
						throw new RuntimeException("Error for skill " + skill.name());
					}
				})
				.toSortedList()
				.toBlocking()
				.first();

		String pattern = "%-" + longuestName.get() + "s | %-" + longuestSkill.get() + "s | %s";

		try (PrintStream ps = Tools.filePrintStream(FILE_JUDGEMENT_SKILLS)) {

			ps.format(pattern + Constants.NL, "Composite skill name", "Skills in composite", "Condition for first skill");
			ps.print("------------------------------------------------------------------------------");
			ps.println(new String(new char[longuestName.get() + longuestSkill.get()]).replace("\0", "-"));

			for (BaseSkill baseSkill : skills) {
				AbilityCondition parser = new AbilityCondition((String) baseSkill.value());
				String condStr = parser.getDescription();
				ps.println(String.format(pattern, baseSkill.name(), baseSkill.value2(), condStr));
			}
		}
	}

	private static void updateSupportedSkills(String FILE_SKILLS_SUPPORTED) {
		try (PrintStream ps = Tools.filePrintStream(FILE_SKILLS_SUPPORTED)) {
			HashSet<String> ignore = new HashSet<String>();
			ignore.add(AbilityProvider.NAME);

			ps.println("# Supported skills");
			ps.println("#");
			ps.println("# This is the list of skills supported by the sim. Most skills require either a level or values.");
			ps.println("# You can reference a skill either by their full in game name (e.g. Prayer 7)");
			ps.println("# or you can reference them wit ha custom value (e.g. Prayer:280)");
			ps.println("#");
			ps.println("# To mark a skill as being a quick strike or a depseration you can prefix them with the following modifiers:");
			ps.println("# QuickStrike prefix: QS_ ");
			ps.println("# Desperation prefix: D_");
			ps.println("# For instance you can use QS_Prayer 7 or D_Prayer:280");
			ps.println("#");
			ps.println("# For more information on deck format, please check out the wiki page at");
			ps.println("# https://bitbucket.org/Crystark/ek-battlesim/wiki/Advanced%20Deck%20building");
			ps.println("");

			AbilityFactory.supportedSkills(true)
				.map(BaseSkill::cleanName)
				.filter(s -> !ignore.contains(s))
				.forEach(ps::println);
		}
	}

	private static void updateSupportedCards(String FILE_CARDS_SUPPORTED) {
		final int[] sizes = new int[11];
		final List<String> ignore = Arrays
			.<String>asList(
				"Hydra II",
				"Hydra III",
				"Hydra IV",
				"Hydra V");

		AtomicReference<String> pattern = new AtomicReference<>();
		AtomicReference<String> type = new AtomicReference<>();

		try (PrintStream ps = Tools.filePrintStream(FILE_CARDS_SUPPORTED)) {
			Database.get().cards().all()
				.filter(card -> card.hpAt(10) > 1 && !ignore.contains(card.name()))
				.filter(c -> {
						// Check that all skills are supported
						try {
							CardDesc cd = CardDescFormat.parseSimple(c.name());
							new Card(cd, null);
							return true;
						}
						catch (IllegalArgumentException e) {
						}
						return false;
					}
				)
				.map(SupportedCard::new)
				.doOnNext(c -> {
					List<String> card = c.toStringList();
					// Store highest sizes
					for (int i = 0; i < card.size(); i++) {
						String part = card.get(i);
						if (part.length() > sizes[i])
							sizes[i] = part.length();
					}
				})
				.toSortedList()
				.doOnNext(t -> {
					StringBuilder patternBuilder = new StringBuilder(256);
					for (int i = 0; i < sizes.length; i++) {
						patternBuilder.append('%').append(i + 1).append("$-").append(sizes[i] + 2).append('s');
					}
					pattern.set(patternBuilder.toString());
				})
				.concatMap(Observable::from)
				.toBlocking()
				.forEach(c -> {
					List<String> card = c.toStringList();
					Object[] v = new Object[sizes.length];
					Arrays.fill(v, "");
					int i, s;
					for (i = 0, s = card.size() - 1; i < s; i++) {
						v[i] = card.get(i) + ", ";
					}
					v[i] = card.get(i);

					String typeName = c.getTypeName();
					String prev = type.get();
					if (!typeName.equals(prev)) {
						type.set(typeName);
						if (prev != null) {
							ps.println();
						}
						ps.println("# " + typeName);
					}
					ps.println(String.format(pattern.get(), v).trim());
				});

			Warnings.getAndReset();
		}
	}

	private static String s(Object s) {
		return s == null ? "" : s.toString();
	}

	private static void updateCardsList(Game game, String FILE_CARDS) {
		switch (game){
			case mr:
			case mrn:
				updateMrCardsList(FILE_CARDS);
				break;
			case loa:
			case hf:
				updateLoaCardsList(FILE_CARDS);
		}
	}
	private static void updateMrCardsList(String FILE_CARDS) {
		String[] headers = new String[] {
			"Name",
			"Stars",
			"Type",
			"Cost",
			"Wait",
			"Limit",
			"Attack @ 10",
			"Attack @ 15",
			"Attack / lvl",
			"HP @ 10",
			"HP @ 15",
			"HP / lvl",
			"Frags",
			"Sell price",
			"XP value",
			"Gold / XP",
			"Maze",
			"Maze Frag",
			"Thief",
			"Thief Frag",
			"DI Reward",
			"KW Store",
			"FoH Store",
			"FT Pack"
		};

		try (PrintStream ps = Tools.filePrintStream(FILE_CARDS)) {
			ps.println(String.join(",", headers));
			Database.get().cards().all()
				.map(SupportedCard::new)
				.toSortedList()
				.concatMap(Observable::from)
				.map(cs -> {
					BaseCard c = cs.card;
					List<String> cardData = new ArrayList<>(42);

					Collections.addAll(cardData,
						c.name(),
						s(c.stars()),
						c.type().name(),
						s(c.costAt(0)),
						s(c.timer()),
						s(c.limit() == 0 ? "" : c.limit()),
						s(c.attackAt(10)),
						s(c.attackAt(15)),
						s(c.attackAt(11) - c.attackAt(10)),
						s(c.hpAt(10)),
						s(c.hpAt(15)),
						s(c.hpAt(11) - c.hpAt(10)),
						s(c.frags() == 0 ? "" : c.frags()),
						s(c.sellPrice()),
						s(c.xpValue()),
						c.xpValue() == 0 ? "∞" : Tools.float2String(c.sellPrice() / (float) c.xpValue(), 2),
						c.isMazeDrop() ? "Y" : "",
						c.isMazeFragDrop() ? "Y" : "",
						c.isThiefDrop() ? "Y" : "",
						c.isThiefFragDrop() ? "Y" : "",
						c.isDIReward() ? "Y" : "",
						c.inKwStore() ? s(c.kwStorePrice()) : "",
						c.inFOHStore() ? s(c.fohStorePrice()) : "",
						c.inFTPack() ? "Y" : "");

					return cardData;
				})
				.toBlocking()
				.forEach(card -> ps.println(String.join(",", card)));
		}
	}

	private static void updateLoaCardsList(String FILE_CARDS) {
		String[] headers = new String[] {
			"Name",
			"Stars",
			"Type",
			"Cost",
			"Wait",
			"Limit",
			"Attack @ 10",
			"Attack @ 15",
			"Attack / lvl",
			"HP @ 10",
			"HP @ 15",
			"HP / lvl",
			"Frags",
			"Sell price",
			"XP value",
			"Gold / XP",
			/*"Maze",
			"Maze Frag",
			"Thief",
			"DI Reward",
			"KW Store",
			"FoH Store",
			"FT Pack"*/
			"Thief Frag",
			"RacePack", //RacePacket
			"Dung",
			"Dung Frag",
		};

		try (PrintStream ps = Tools.filePrintStream(FILE_CARDS)) {
			ps.println(String.join(",", headers));
			Database.get().cards().all()
				.map(SupportedCard::new)
				.toSortedList()
				.concatMap(Observable::from)
				.map(cs -> {
					BaseCard c = cs.card;
					List<String> cardData = new ArrayList<>(42);

					Collections.addAll(cardData,
						c.name(),
						s(c.stars()),
						c.type().name(),
						s(c.costAt(0)),
						s(c.timer()),
						s(c.limit() == 0 ? "" : c.limit()),
						s(c.attackAt(10)),
						s(c.attackAt(15)),
						s(c.attackAt(11) - c.attackAt(10)),
						s(c.hpAt(10)),
						s(c.hpAt(15)),
						s(c.hpAt(11) - c.hpAt(10)),
						s(c.frags() == 0 ? "" : c.frags()),
						s(c.sellPrice()),
						s(c.xpValue()),
						c.xpValue() == 0 ? "∞" : Tools.float2String(c.sellPrice() / (float) c.xpValue(), 2),
						c.isThiefFragDrop() ? "Y" : "",
					/*	c.isMazeFragDrop() ? "Y" : "",
						c.isThiefDrop() ? "Y" : "",
						c.isThiefFragDrop() ? "Y" : "",
						c.isDIReward() ? "Y" : "",
						c.inKwStore() ? s(c.kwStorePrice()) : "",
						c.inFOHStore() ? s(c.fohStorePrice()) : "",
						c.inFTPack() ? "Y" : "")*/
						c.isRaceGemPackDrop() ? "Y" : "",
						c.isDungeonsDrop() ?  "Y" : "",
						c.isDungeonsFragDrop() ?  "Y" : ""
					);

					return cardData;
				})
				.toBlocking()
				.forEach(card -> ps.println(String.join(",", card)));
		}
	}

	private static void updateSupportedRunes(String FILE_RUNES_SUPPORTED) {
		try (PrintStream ps = Tools.filePrintStream(FILE_RUNES_SUPPORTED)) {
			RuneFactory.printNames(ps);
		}
	}


	static class DropData implements Comparable<DropData> {
		String  dropGroup;
		Integer stars;
		String  name;

		DropData(String dropGroup, Integer stars, String name) {
			super();
			this.dropGroup = dropGroup;
			this.stars = stars;
			this.name = name;
		}

		@Override
		public int compareTo(DropData o) {
			int compareGroup = dropGroup.compareTo(o.dropGroup);
			if (compareGroup == 0) {
				int compareStars = stars.compareTo(o.stars) * -1;
				if (compareStars == 0) {
					return name.compareTo(o.name);
				}
				return compareStars;
			}
			return compareGroup;
		}
	}

	static class EvoData {
		String        cardName;
		String        evolvedWith;
		int           evovleCount;
		List<EvoData> materialTo;

		EvoData(String cardName, String evolvedWith, int evovleCount) {
			super();
			this.cardName = cardName;
			this.evolvedWith = evolvedWith;
			this.evovleCount = evovleCount;
			this.materialTo = new ArrayList<>();
		}

		void addMaterialTo(EvoData d) {
			materialTo.add(d);
		}
	}

	static class SupportedCard implements Comparable<SupportedCard> {
		BaseCard     card;
		List<String> asList;

		public SupportedCard(BaseCard c) {
			this.card = c;
		}

		@Override
		public int compareTo(SupportedCard t) {
			int compare = this.getTypeForCompare().compareTo(t.getTypeForCompare());
			if (compare != 0)
				return compare;

			return card.name().compareTo(t.card.name());
		}

		private Integer getTypeForCompare() {
			switch (card.type()) {
				case forest:
				case mtn:
				case swamp:
				case tundra:
					if (card.name().contains("[Elite]")) {
						return 0;
					}
					return -card.stars();
				case demonist:
					return 1;
				case demon:
					if (card.costAt(0) != 99) {
						return -card.stars();
					}
					return 2;
				case hydra:
					if (card.id() == 494) { // Hell Pumpkin
						return -card.stars();
					}
					return card.name().endsWith("(Legendary)") && card.skillsAt(15).size() > 3 ? 3 : card.name().startsWith("Hydra") || card.name().endsWith("Lilith") ? 4 : 5;
				case special:
					return 6;
				default:
					return 7;
			}
		}

		public String getTypeName() {
			switch (card.type()) {
				case forest:
				case mtn:
				case swamp:
				case tundra:
					if (card.name().contains("[Elite]")) {
						return "Elite Cards";
					}
					return card.stars() + " star cards";
				case demon:
					if (card.costAt(0) != 99) {
						return card.stars() + " star cards";
					}
					return "Demons";
				case demonist:
					return "Demonist";
				case hydra:
					if (card.id() == 494) { // Hell Pumpkin
						return card.stars() + " star cards";
					}
					return card.name().endsWith("(Legendary)") && card.skillsAt(15).size() > 3 ? "EW Bosses"
						: card.name().startsWith("Hydra") ? "Hydras"
						: card.name().endsWith("Lilith") ? "Liliths"
						: "Hydra kind";
				case special:
					return "Special";
				default:
					return "Other";
			}
		}

		public List<String> toStringList() {
			if (asList == null) {
				ArrayList<String> stringList = new ArrayList<>();

				int baseLevel = (CardType.hydra.equals(card.type()) || CardType.demon.equals(card.type())) ? 15 : 10;

				Collections
					.addAll(stringList,
						card.name(),
						String.valueOf(card.costAt(baseLevel - 10)),
						String.valueOf(card.timer()),
						String.valueOf(card.attackAt(baseLevel)),
						String.valueOf(card.hpAt(baseLevel)),
						card.type().name());

				for (BaseSkill ad : card.skillsAt(baseLevel)) {
					stringList.add(ad.name()
						.replace("Desperate: ", LaunchType.desperation.prefix.toUpperCase() + "_")
						.replace("Desperation: ", LaunchType.desperation.prefix.toUpperCase() + "_")
						.replace("Quick Strike: ", LaunchType.quickstrike.prefix.toUpperCase() + "_")
						.replace("Preemptive Strike: ", LaunchType.preemptiveStrike.prefix.toUpperCase() + "_")
						.replace("Death Whisper: ", LaunchType.deathWhisper.prefix.toUpperCase() + "_")
						.replace("[Quick Strike]", LaunchType.quickstrike.prefix.toUpperCase() + "_")
						.replace("[Desperation]", LaunchType.desperation.prefix.toUpperCase() + "_")
						.replaceAll("_ +", "_")
						.trim());
				}
				return stringList;
			}
			return asList;
		}
	}
}
