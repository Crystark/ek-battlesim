package crystark.ek.battlesim;

import org.junit.Assert;
import org.junit.Test;

import crystark.common.events.Warnings;

public class AppUpdateCheckerTest {
	@Test
	public void test_extractVersion() {
		Assert.assertEquals("0.6.12", AppUpdateChecker.extractVersion("n\\n### [Download latest version (0.6.12 - 2015-09-22)](https://bitbu"));
		Assert.assertEquals("0.6.12A", AppUpdateChecker.extractVersion("n\\n### [Download latest version (0.6.12A - 2015-09-22)](https://bitbu"));
	}

	@Test
	public void test_run() throws InterruptedException {
		AppUpdateChecker updateChecker = AppUpdateChecker.run();
		String version = updateChecker.request.toBlocking().first();

		updateChecker.cdl.await();
		Assert.assertNotNull(version);
		Assert.assertEquals(1, (int) Warnings.getAndReset().count().toBlocking().first());
	}
}
