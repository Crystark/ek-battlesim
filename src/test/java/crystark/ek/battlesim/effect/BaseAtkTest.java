package crystark.ek.battlesim.effect;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Cards;

public class BaseAtkTest {

	@Test
	public void test_apply() {
		Card card = Cards.from("Sea King", null);
		int v = 5;
		BonusBaseAtk ba = new BonusBaseAtk(v);

		int baseAtk = card.desc.attack;
		int currentBaseAtk = card.getCurrentBaseAtk();
		int currentAtk = card.getCurrentAtk();

		ba.applyTo(card);

		Assert.assertEquals("Original base atk should not have changed", baseAtk, card.desc.attack);
		Assert.assertEquals("Current base atk should be increased by " + v, currentBaseAtk + v, card.getCurrentBaseAtk());
		Assert.assertEquals("Current atk should be increased by " + v, currentAtk + v, card.getCurrentAtk());
	}

}
