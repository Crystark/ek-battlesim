package crystark.ek.battlesim.rune;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import org.junit.Assert;
import org.junit.Test;

public class RuneTest {
	static class TestRune extends Rune {
		public TestRune() {
			super(new RuneDesc("TestRune", 5, null), new IOnRuneTurn() {
				@Override
				public String getName() {
					return "TestRuneAble";
				}

				@Override
				public boolean canBeLaunched(Card owner) {
					return true;
				}

				@Override
				public void onRuneTurn(Player player) {
					// NOOP
				}

			});
		}

		@Override
		protected boolean conditionsAreMet(int round, Player player) {
			return true;
		}
	}

	@Test
	public void test_charge() {
		TestRune rune = new TestRune();
		int times = rune.remaining();
		rune.charge();

		Assert.assertEquals(times + 1, rune.remaining());
	}
}
