package crystark.ek.battlesim.rune;

import crystark.common.api.NamesConverter;
import crystark.common.api.RuneDesc;
import crystark.common.db.Database;
import crystark.common.db.model.BaseRune;
import crystark.ek.EkbsConfig;
import crystark.ek.battlesim.task.TaskException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.reflections.Reflections;
import rx.exceptions.OnErrorThrowable;

import java.util.AbstractMap.SimpleEntry;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class RuneFactoryTest {
	private static Map<String, Class<? extends Rune>> runes;

	@BeforeClass
	public static void beforeClass() {

		final NamesConverter converter = EkbsConfig.current().namesConverter();

		runes = new Reflections(Rune.class.getPackage().getName()).getSubTypesOf(Rune.class).stream()
			.filter(RuneFactory::isBaseRune)
			.map(clazz -> {
				try {
					String name = clazz.getField("NAME").get(null).toString();
					name = converter.runeToAlias(name);
					return new SimpleEntry<String, Class<? extends Rune>>(name, clazz);
				}
				catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
					throw OnErrorThrowable.from(e);
				}
			})
			.collect(Collectors.toMap(SimpleEntry::getKey, SimpleEntry::getValue, (t, t2) -> t, TreeMap::new));
	}

	@Test
	public void test_allRunesInFactory() {
		try {
			for (String name : runes.keySet()) {
				Rune instance = RuneFactory.get(new RuneDesc(name, 1, 1));
				Assert.assertNotNull(name + " not returned by factory", instance);
			}
		}
		catch (TaskException e) {
			if (e.getMessage().startsWith("Unknown rune")) {
				for (Class<? extends Rune> r : runes.values()) {
					String name = r.getSimpleName();
					System.out.println("MAPPING.put(" + name + ".NAME, " + name + "::new);");
				}
				throw e;
			} else {
				System.out.println(e.getMessage());
			}
		}
	}

	@Test
	public void test_missingRunes() {
		StringBuilder sb = new StringBuilder();
		RuneFactory.supportedRunes(false)
			.toBlocking()
			.forEach(rune -> {
				sb.append(rune.name() + '\n');
			});

		Assert.assertTrue("Some runes aren't implemented: \n" + sb, sb.length() <= 0);
	}

	@Test
	public void test_allRunesInDb() {
		StringBuilder sb = new StringBuilder();
		for (String name : runes.keySet()) {
			BaseRune rune = Database.get().runes().get(name);
			//Assert.assertNotNull(name + " not found in DB", rune);
			if (rune == null) sb.append(name + "\n");
		}
		Assert.assertTrue("Runes not found in DB:\n" + sb, sb.length() <= 0);
	}
}
