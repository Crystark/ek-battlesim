package crystark.ek.battlesim.task;

import org.junit.Test;

import crystark.common.api.Server;

public class ExtractRMDecksTest {

	private ExtractRMDecksTaskOptions prepare(Server server, String outputDir) {
		return Tasks
			.prepareArenaExtract(server, outputDir)
			.printOutput(false);
	}

	@Test
	public void test_run() {
		this
			.prepare(
				Server.apollo,
				System.getProperty("java.io.tmpdir"))
			.from(48)
			.count(2)
			.run();
	}
}
