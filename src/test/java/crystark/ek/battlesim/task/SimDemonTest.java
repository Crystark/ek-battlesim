package crystark.ek.battlesim.task;

import crystark.common.format.DeckDescFormat;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static crystark.tools.Tools.file;

/**
 * @see SimDemon
 */
public class SimDemonTest {

	@Before
	public void before() {
		//		Tools.activateDebug();
	}

	@After
	public void after() {

		//Constants.IS_DEBUG = false;
	}

	private SimDemonTaskOptions prepare(long iter, String deck, String demon) {
		return Tasks
			.prepareDemonInvasionSim(DeckDescFormat.parseFile(file(deck)), demon)
			.printOutput(false)
			.iterations(iter);
		//			.iterations(1L);
	}

	private SimDemon run(long iter, String deck, String demon) {
		return (SimDemon) prepare(iter, deck, demon).run();
	}

	private SimDemon run(String deck, String demon) {
		return run(5000, deck, demon);
	}

	public void assertNearby(long expected, long actual) {
		double expect = expected;
		Assert.assertEquals(expect, actual, Math.min(expect * 0.5, 4000));
	}

	@Test
	public void test_plagueogryn() {
		SimDemon sim = this.run(20000, "_test_deck_po_.txt", "Plague Ogryn");

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
		assertNearby(382612, sim.meritPerMinute());
	}

	@Test
	@Ignore
	public void test_bahamut() {
		SimDemon sim = this.run("_test_deck_bahamut_.txt", "Bahamut");

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
		//		Assert.assertThat(sim.meritPerMinute(), Matchers.both(Matchers.greaterThan(13900L)).and(Matchers.lessThan(14100L)));
	}

	@Test
	@Ignore
	public void test_azathoth() {
		SimDemon sim = this.run("_test_deck_azathoth_.txt", "Azathoth");

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
		assertNearby(227759, sim.meritPerMinute());
	}

	@Test
	@Ignore
	public void test_azathoth_mob() {
		SimDemon sim = this.run("_test_deck_azathoth_mob_.txt", file("_test_deck_azathoth_mob_def_.txt"));

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
	}

	@Test
	public void test_cthulhu() {
		SimDemon sim = this.run(20000, "_test_deck_cthulhu_.txt", "Cthulhu");

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
		assertNearby(164528, sim.meritPerMinute());
	}

	@Test
	public void test_mars() {
		SimDemon sim = this.run(20000, "_test_deck_mars_.txt", "Mars");

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
		assertNearby(86177, sim.meritPerMinute());
	}

	@Test
	public void test_seaking() {
		SimDemon sim = this.run(20000, "_test_deck_sk_.txt", "Sea King");

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
		assertNearby(525657, sim.meritPerMinute());
	}

	@Test
	public void test_pandarus() {
		SimDemon sim = this.run(20000, "_test_deck_pandarus_.txt", "Pandarus");

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
		assertNearby(84959, sim.meritPerMinute());
	}

	@Test
	public void test_darktitan() {
		SimDemon sim = this.run(20000, "_test_deck_dt_.txt", "Dark Titan");

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
		assertNearby(383617, sim.meritPerMinute());
	}

	@Test
	public void test_darktitan2() {
		SimDemon sim = this.run(20000, "_test_deck_dt2_.txt", "Dark Titan,  102, 4, 1, 2100000000, demon,Devil's Mark,Damnation 10, Immunity");

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
		assertNearby(456000, sim.meritPerMinute());
	}

	@Test
	public void test_deucalion() {
		SimDemon sim = this.run(20000, "_test_deck_deucalion_.txt", "Deucalion");

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
		assertNearby(159133, sim.meritPerMinute());
	}

	@Test
	public void test_pazuzu() {
		SimDemon sim = this.run("_test_deck_pazuzu_.txt", "Pazuzu");

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
		assertNearby(206762, sim.meritPerMinute());
	}
	@Test
	public void test_pazuzu_limited() {
		long MAX_DAMAGE = 200_000;
		final SimDemonTaskOptions opts = Tasks
			.prepareDemonInvasionSim(DeckDescFormat.parseFile(file("_test_deck_pazuzu_.txt")), "Pazuzu")
			.printOutput(false)
			.iterations(5000L)
			.maxDamage(MAX_DAMAGE);

		SimDemon sim = (SimDemon) opts.run();
		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
		assertNearby(MAX_DAMAGE, (long) sim.metricMerit.getMean());
		Assert.assertTrue(sim.metricMerit.max.get() <= MAX_DAMAGE);
	}

	@SuppressWarnings("unused")
	private void getMinMax(String file, String demon) {
		int min = Integer.MAX_VALUE;
		int max = 0;
		for (int i = 0; i < 50; i++) {
			SimDemon sim = run(file, demon);
			long mpm = sim.meritPerMinute();
			min = (int) Math.min(min, mpm);
			max = (int) Math.max(max, mpm);
		}

		System.out.println(min + "-" + max + " for " + demon + " with " + file);
	}
}
