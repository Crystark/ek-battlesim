package crystark.ek.battlesim.task;

import crystark.common.format.DeckDescFormat;
import crystark.ek.Constants;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static crystark.tools.Tools.file;

/**
 * @see SimElementalWar
 */
public class SimElementalWarTest {

	@Before
	public void before() {
		//		Constants.IS_DEBUG = true;
		//		org.apache.log4j.LogManager.getRootLogger().setLevel(org.apache.log4j.Level.DEBUG);
	}

	@After
	public void after() {
		Constants.IS_DEBUG = false;
	}

	private SimElementalWarTaskOptions prepare(String deck, String lgDeck) {
		return Tasks
			.prepareElementalWarSim(DeckDescFormat.parseFile(file(deck)), file(lgDeck))
			.printOutput(false)
			.iterations(5000L);
		//			.iterations(1L);
	}

	private SimElementalWarTaskOptions prepareWithBossName(String deck, String lgDeck) {
		return Tasks
			.prepareElementalWarSim(DeckDescFormat.parseFile(file(deck)), lgDeck)
			.printOutput(false)
			.iterations(5000L);
		//			.iterations(1L);
	}

	private SimElementalWar run(String deck, String lgDeck) {
		return (SimElementalWar) prepare(deck, lgDeck).run();
	}

	@Test
	public void test_ew() {
		SimElementalWar sim = run("_test_deck_ew_atk_.txt", "_test_deck_ew_def_.txt");

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
	}

	@Test
	public void test_high_health() {
		SimElementalWar sim = run("_test_deck_ew_atk_.txt", "_test_deck_ew_high_hp_.txt");

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
		Assert.assertThat(sim.metricDamage.getMin(), Matchers.greaterThanOrEqualTo(0L));
	}

	@Test
	public void test_ew2() {
		final SimElementalWarTaskOptions options = prepareWithBossName("_test_deck_ew_atk2_.txt", "Thunder Dragon");
		final SimElementalWar sim = (SimElementalWar) options.run();

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
		Assert.assertThat(sim.metricDamage.getMean(), Matchers.allOf(Matchers.greaterThan(4_620_000F), Matchers.lessThan(4_680_000F)));
	}
}
