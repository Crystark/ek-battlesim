package crystark.ek.battlesim.task;

import org.junit.Assert;
import org.junit.Test;

public class SimMetricTest {
	@Test
	public void test_metrics() {
		SimMetric m = new SimMetric();
		m.update(2);
		m.update(3);
		m.update(1);
		m.update(10);
		m.update(2);
		m.update(6);

		Assert.assertEquals(1, m.getMin());
		Assert.assertEquals(4f, m.getMean(), 0f);
		Assert.assertEquals(24, m.getTotal());
	}

	@Test(expected = IllegalStateException.class)
	public void test_ko() {
		new SimMetric().getMin();
	}

	@Test
	public void test_hystogram() {
		SimMetric m = new SimMetric(10000);

		for (int i = 0; i < 10000; i++) {
			m.update((long) (Math.random() * 300));
		}

		for (int i = 1; i <= 10; i++) {
			try {
				m.getHystogramAsMap(i);
			}
			catch (Exception e) {
				e.printStackTrace();
				Assert.fail("Failed for " + i + " bars");
			}
		}
	}
}
