package crystark.ek.battlesim.task;

import static crystark.tools.Tools.file;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import crystark.common.format.DeckDescFormat;
import crystark.ek.Constants;

/**
 * @see SimKingdomWar
 */
public class SimKingdomWarTest {

	@Before
	public void before() {
		//		Tools.activateDebug();
	}

	@After
	public void after() {
		Constants.IS_DEBUG = false;
	}

	private SimKingdomWarTaskOptions prepare(String deck, Collection<String> guards) {
		return Tasks
			.prepareKingdomWarSim(DeckDescFormat.parseFile(file(deck)), guards)
			.printOutput(false)
			.iterations(1000L);
		//			.iterations(1L);
	}

	private SimKingdomWar run(String deck, Collection<String> guards) {
		return (SimKingdomWar) prepare(deck, guards).run();
	}

	@Test
	public void test_honorPerWinMin() {
		Assert.assertEquals(116, SimKingdomWar.honorPerWinMin(8418, 44610));
	}

	@Test
	public void test_kw_all() {
		SimKingdomWar sim = run("_test_deck_kw_.txt", Collections.<String> emptyList());

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
		Assert.assertThat(sim.fights.getMean(), Matchers.both(Matchers.greaterThan(45f)).and(Matchers.lessThan(60f)));
	}

	@Test
	public void test_kw_some() {
		SimKingdomWar sim = run("_test_deck_kw_.txt", Arrays.asList("fd_56", "id_53"));

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
		Assert.assertThat(sim.fights.getMean(), Matchers.both(Matchers.greaterThan(70f)).and(Matchers.lessThan(80f)));
	}
}
