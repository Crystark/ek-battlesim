package crystark.ek.battlesim.task;

import org.junit.Assert;
import org.junit.Test;

public class SimElementalWarTaskOptionsTest {
	@Test
	public void test_getEwDeck_name() {
		Assert.assertNotNull(SimElementalWarTaskOptions.getEwDeck("Goddess of Order"));
		Assert.assertNotNull(SimElementalWarTaskOptions.getEwDeck("goddess of order"));
		Assert.assertNotNull(SimElementalWarTaskOptions.getEwDeck("goddess of order legendary"));
		Assert.assertNotNull(SimElementalWarTaskOptions.getEwDeck("Goddess of Order (Legendary)"));
		Assert.assertNotNull(SimElementalWarTaskOptions.getEwDeck(" Goddess of Order (Legendary) "));
	}

	@Test(expected = TaskException.class)
	public void test_getEwDeck_KO() {
		SimElementalWarTaskOptions.getEwDeck("I do not exist");
	}
}
