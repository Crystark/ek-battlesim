package crystark.ek.battlesim.task;

import crystark.common.api.CardDesc;
import crystark.common.api.CardType;
import crystark.common.api.DeckDesc;
import crystark.common.api.Game;
import crystark.common.format.DeckDescFormat;
import crystark.ek.Constants;
import crystark.ek.EkbsConfig;
import crystark.ek.battlesim.battle.Cards;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static crystark.tools.Tools.file;

/**
 * @see SimHydra
 */
public class SimHydraTest {

	@Before
	public void before() {
		//		Constants.IS_DEBUG = true;
		//		org.apache.log4j.LogManager.getRootLogger().setLevel(org.apache.log4j.Level.DEBUG);
	}

	@After
	public void after() {
		Constants.IS_DEBUG = false;
	}

	private SimHydraTaskOptions prepare(String deck, List<String> hydras, boolean hydraOnly) {
		return Tasks
			.prepareHydraSim(DeckDescFormat.parseFile(file(deck)), hydras, hydraOnly)
			.printOutput(false)
			.iterations(1000L);
		//			.iterations(1L);
	}

	private SimHydra run(String deck, List<String> hydras, boolean hydraOnly) {
		return (SimHydra) prepare(deck, hydras, hydraOnly).run();
	}

	@Test
	public void test_hydra_all() {
		SimHydra sim = run("_test_deck_hydra_.txt", Collections.<String> emptyList(), false);

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
		Assert.assertEquals("Should sim all hydras", 25, sim.defenders.length);
	}

	@Test
	public void test_hydra_all_altDeck() {
		SimHydra sim = (SimHydra) prepare("_test_deck_hydra_.txt", Collections.<String> emptyList(), false)
			.withIsolatedHydraDeck(DeckDescFormat.parseFile(file("_test_deck_hydra_alt_.txt")))
			.run();

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
	}

	@Test
	public void test_hydra_all_altDeck_hp() {
		SimHydra sim = (SimHydra) prepare("_test_deck_hydra_.txt", Arrays.asList("hydra v guard"), false)
			.withIsolatedHydraDeck(DeckDescFormat.parseFile(file("_test_deck_hydra_alt_.txt")))
			.withDeckSwitchAtHp(270000)
			.run();

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
	}

	@Test
	public void test_lilith() {
		EkbsConfig.current().switchGame(Game.loa);
		List<String> hydras = Arrays.asList("l");
		SimHydra sim = run("_test_deck_lilith_.txt", hydras, false);
		EkbsConfig.current().switchGame(Game.mr);
		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
		Assert.assertEquals("Should only sim 25 hydras", 25, sim.defenders.length);
	}

	@Test
	public void test_hydra() {
		List<String> hydras = Arrays
			.asList("h1 fa",
				"hydra ii curse",
				"hydra ii freeze",
				"hydra ii reflect",
				"hydra ii trap",
				"hydra iii freeze",
				"hydra iii poison",
				"hydra iii reflect",
				"hydra iii trap");
		SimHydra sim = run("_test_deck_hydra_.txt", hydras, false);

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
		Assert.assertEquals("Should only sim 9 hydras", 9, sim.defenders.length);
		Assert.assertThat(sim.winRatioGlobal(0), Matchers.both(Matchers.greaterThan(39f)).and(Matchers.lessThan(41f)));
	}

	@Test
	public void test_hydraOnly_all() {
		SimHydra sim = run("_test_deck_hydra_.txt", Collections.<String> emptyList(), true);

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
		Assert.assertEquals("Should sim all hydras", 25, sim.defenders.length);
	}

	@Test
	public void test_hydraOnly() {
		List<String> hydras = Arrays
			.asList("h1 fa",
				"hydra ii curse",
				"hydra ii freeze",
				"hydra ii reflect",
				"hydra ii trap",
				"hydra iii freeze",
				"hydra iii poison",
				"hydra iii reflect",
				"hydra iii trap");
		SimHydra sim = run("_test_deck_hydra_.txt", hydras, true);

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
		Assert.assertEquals("Should only sim 9 hydras", 9, sim.defenders.length);
		Assert.assertThat(sim.winRatioGlobal(0), Matchers.both(Matchers.greaterThan(74f)).and(Matchers.lessThan(76f)));
	}

	@Test
	public void test_hydraDecksAreConsistent() {
		DeckDesc[] defenders = SimHydraTaskOptions.getHydras(Collections.<String> emptyList(), false);

		for (DeckDesc hydra : defenders) {
			String romanNumber = hydra.playerName.split(" ")[1].toLowerCase();
			float hpFactor;
			float atkFactor;
			int expectedRunes = 4;
			switch (romanNumber) {
				case "i":
					hpFactor = 1;
					atkFactor = 1;
					expectedRunes = 3;
					break;
				case "ii":
					hpFactor = 2;
					atkFactor = 1;
					break;
				case "iii":
					hpFactor = 3;
					atkFactor = 1.2f;
					break;
				case "iv":
					hpFactor = 4;
					atkFactor = 1.5f;
					break;
				case "v":
					hpFactor = 5;
					atkFactor = 2;
					break;
				default:
					throw new RuntimeException("Unhandled or illegal roman number " + romanNumber);
			}

			Assert.assertEquals(hydra + " is missing some runes.", expectedRunes, hydra.runesDescs.size());

			List<CardDesc> deck = hydra.cardsDescs;
			for (CardDesc card : deck) {
				String name = card.name;
				CardDesc original = Cards.getCardDesc(card.name);

				int expectedHp = (int) (original.health * hpFactor);
				int expectedAtk = (int) (original.attack * atkFactor);

				if (CardType.hydra.equals(card.type)) {
					Assert.assertEquals("Hydra name conflict", hydra.playerName.toLowerCase(), card.name.toLowerCase());
				}

				Assert.assertEquals(hydra + "'s card " + name + " doesn't have expected HP", expectedHp, card.health);
				Assert.assertEquals(hydra + "'s card " + name + " doesn't have expected ATK", expectedAtk, card.attack);
			}
		}
	}

	@Test
	public void test_hydraOnlyDecksAreConsistent() {
		DeckDesc[] defenders = SimHydraTaskOptions.getHydras(Collections.<String> emptyList(), true);

		for (DeckDesc hydra : defenders) {
			String romanNumber = hydra.playerName.split(" ")[1].toLowerCase();
			float hpFactor;
			float atkFactor;
			switch (romanNumber) {
				case "i":
					hpFactor = 1;
					atkFactor = 1;
					break;
				case "ii":
					hpFactor = 2;
					atkFactor = 1;
					break;
				case "iii":
					hpFactor = 3;
					atkFactor = 1.2f;
					break;
				case "iv":
					hpFactor = 4;
					atkFactor = 1.5f;
					break;
				case "v":
					hpFactor = 5;
					atkFactor = 2;
					break;
				default:
					throw new RuntimeException("Unhandled or illegal roman number " + romanNumber);
			}

			List<CardDesc> deck = hydra.cardsDescs;
			for (CardDesc card : deck) {
				String name = card.name;
				CardDesc original = Cards.getCardDesc(card.name);

				int expectedHp = (int) (original.health * hpFactor);
				int expectedAtk = (int) (original.attack * atkFactor);

				if (CardType.hydra.equals(card.type)) {
					Assert.assertEquals("Hydra name conflict", hydra.playerName.toLowerCase(), card.name.toLowerCase());
				}

				Assert.assertEquals(hydra + "'s card " + name + " doesn't have expected HP", expectedHp, card.health);
				Assert.assertEquals(hydra + "'s card " + name + " doesn't have expected ATK", expectedAtk, card.attack);
			}
		}
	}
}
