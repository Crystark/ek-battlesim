package crystark.ek.battlesim.task;

import org.junit.Test;

import crystark.common.api.Server;

public class ExtractLeagueDecksTest {

	private ExtractLeagueDecksTaskOptions prepare(Server server, String outputDir) {
		return Tasks
			.prepareFieldOfHonorExtract(server, outputDir)
			.printOutput(false);
	}

	@Test
	public void test_run() {
		this
			.prepare(
				Server.apollo,
				System.getProperty("java.io.tmpdir"))
			.run();
	}
}
