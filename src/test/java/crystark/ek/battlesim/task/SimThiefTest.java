package crystark.ek.battlesim.task;

import crystark.common.format.DeckDescFormat;
import crystark.ek.Constants;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static crystark.tools.Tools.file;

/**
 * @see SimThief
 */
public class SimThiefTest {

	@Before
	public void before() {
		//		Constants.IS_DEBUG = true;
		//		org.apache.log4j.LogManager.getRootLogger().setLevel(org.apache.log4j.Level.DEBUG);
	}

	@After
	public void after() {
		Constants.IS_DEBUG = false;
	}

	private SimThiefTaskOptions prepare(String deck) {
		return Tasks
			.prepareThiefSim(DeckDescFormat.parseFile(file(deck)))
			.printOutput(false)
			.iterations(10000L);
		//			.iterations(1L);
	}

	private SimThief run(String deck) {
		return (SimThief) prepare(deck).run();
	}

	@Test
	public void test_thief() {
		SimThief sim = run("_test_deck_thief_.txt");

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
		//		Assert.assertThat(sim.winRatioGlobal(0), Matchers.both(Matchers.greaterThan(12f)).and(Matchers.lessThan(14f))); // before dodge PRNG
		Assert.assertThat(sim.winRatioGlobal(0), Matchers.both(Matchers.greaterThan(8.6f)).and(Matchers.lessThan(10.6f)));
	}
}
