package crystark.ek.battlesim.task;

import crystark.common.api.DeckDesc;
import crystark.common.format.DeckDescFormat;
import crystark.ek.Constants;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static crystark.tools.Tools.file;

/**
 * @see SimVersus
 */
public class SimVersusTest {

	@Before
	public void before() {
		//		Tools.activateDebug();
	}

	@After
	public void after() {
		Constants.IS_DEBUG = false;
	}

	private SimVersusTaskOptions prepare(long iters, String deck, String... vsDecks) {
		List<DeckDesc> allVs = new ArrayList<>();
		for (int i = 0; i < vsDecks.length; i++) {
			Collections.addAll(allVs, DeckDescFormat.parseFiles(file(vsDecks[i])));
		}
		return Tasks
			.prepareVersusSim(DeckDescFormat.parseFile(file(deck)), allVs.toArray(new DeckDesc[allVs.size()]))
			.printOutput(false)
			.iterations(iters);
		//			.iterations(1L);
	}

	private SimVersus run(String deck, String... vsDecks) {
		return run(5000, deck, vsDecks);
	}

	private SimVersus run(long iters, String deck, String... vsDecks) {
		return (SimVersus) prepare(iters, deck, vsDecks).run();
	}

	@Test
	public void test_versus() {
		SimVersus sim = run(50000, "_test_deck_vs_atk_.txt", "_test_deck_vs_def_.txt");

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
		Assert.assertThat(sim.winRatioGlobal(0), Matchers.both(Matchers.greaterThan(96.1f)).and(Matchers.lessThan(98.1f)));
	}

	@Test
	public void test_versus_2_defenders() {
		SimVersus sim = run(50000, "_test_deck_vs_atk_.txt", "_test_deck_vs_def_.txt", "_test_deck_vs_def2_.txt");

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
		Assert.assertThat(sim.winRatioForDefender(0, 0), Matchers.both(Matchers.greaterThan(96f)).and(Matchers.lessThan(98f)));
		Assert.assertThat(sim.winRatioForDefender(1, 0), Matchers.both(Matchers.greaterThan(85.5f)).and(Matchers.lessThan(87.5f)));
		Assert.assertThat(sim.winRatioGlobal(0), Matchers.both(Matchers.greaterThan(91.2f)).and(Matchers.lessThan(93.5f)));
	}

	@Test
	public void test_versus_50_rounds() {
		SimVersus sim = run(50000, "_test_deck_vs_50r_.txt", "_test_deck_vs_50r_.txt");

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
		Assert.assertThat(sim.winRatioGlobal(0), Matchers.both(Matchers.greaterThan(49.3f)).and(Matchers.lessThan(50.7f)));
	}

	@Test
	public void test_versus_counter_bug() {
		SimVersus sim = run("_test_deck_bug_counter_and_desperation_.txt", "_test_deck_bug_counter_and_desperation_.txt");

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
	}

	@Test
	public void test_issue_14() {
		SimVersus sim = run("_test_deck_issue_14_.txt", "_test_deck_issue_14_.txt");

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
	}

	@Test
	public void test_issue_19() {
		SimVersus sim = run("_test_deck_issue_19_.txt", "_test_deck_issue_19_.txt");

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
	}

	@Test
	public void test_issue_55() {
		SimVersus sim = run("_test_deck_issue_55_.txt", "_test_deck_issue_55_.txt");

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
	}

	@Test
	public void test_complex_skills() {
		SimVersus sim = run("_test_complex_skills_.txt", "_test_complex_skills_.txt");

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
	}

	@Test
	@Ignore
	public void test_issue_123() {
		SimVersus sim = run("_test_deck_issue_123_.txt", "_test_deck_issue_123_.txt");

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
	}

	@Test
	public void test_preemptive_strike() {
		SimVersus sim = run("_test_preemptive_strike_.txt", "_test_preemptive_strike_.txt");

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
	}

	@Test
	public void test_storm_spirit_vs_ld() {
		SimVersus sim = run("_test_card_storm_spirit_.txt", "_test_card_ld_.txt");

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
	}

	@Test
	public void test_multi_ps_and_md() {
		SimVersus sim = run("_test_multi_ps_and_md_.txt", "_test_multi_ps_and_md_.txt");

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
	}

	@Test
	public void test_sq() {
		SimVersus sim = run("_test_sq_atk_.txt", "_test_sq_def_.txt");

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
	}

	@Test
	public void test_ExileAll() {
		SimVersus sim = run(300,"_test_deck_vs_atk3_.txt", "_test_deck_vs_def3_.txt");

		Assert.assertEquals("Should not have errors", 0, sim.getErrorCount());
	}

}
