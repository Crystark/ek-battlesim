package crystark.ek.battlesim.task;

import org.junit.Test;

import crystark.tools.Tools;

public class ConvertJsonDecksTest {

	private ConvertJsonDecksTaskOptions prepare(String[] inputFiles, String outputDir) {
		return Tasks
			.prepareConvertJsonDecks(inputFiles, outputDir)
			.printOutput(false);
	}

	@Test
	public void test_run() {
		this
			.prepare(
				new String[] { Tools.file("_test_json_to_deck_.txt") },
				System.getProperty("java.io.tmpdir"))
			.run();
	}
}
