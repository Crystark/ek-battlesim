package crystark.ek.battlesim.battle;

import crystark.common.api.DeckDesc;
import crystark.common.format.DeckDescFormat;
import org.junit.Assert;
import org.junit.Test;

public class BattleTest {
	@Test
	public void test_getExhaustionDamage() {
		Assert.assertEquals(200, Battle.getExhaustionDamage(56));
	}

	@Test
	public void testShuffleN(){
		final DeckDesc deckDesc = DeckDescFormat.parseString("deck1",
			"--skip-shuffle=2 \n"
				+ "Player level: 85 \n"
				+ "Guy1, 1, 1, 1, 1, Special \n"
				+ "Guy2, 1, 1, 1, 1, Special \n"
				+ "Guy3, 1, 1, 1, 1, Special \n"
				+ "Guy4, 1, 1, 1, 1, Special \n");

		boolean wasShuffled = false;
		for(int i = 0; i < 10; i++) {
			final Player player = new Player(deckDesc);
			Card card;
			card = player.drawCardFromDeck();
			Assert.assertTrue(card.getName().equals("Guy1"));
			card = player.drawCardFromDeck();
			Assert.assertTrue(card.getName().equals("Guy2"));

			card = player.drawCardFromDeck();
			wasShuffled = wasShuffled || !card.getName().equals("Guy3");
		}
		Assert.assertTrue("3rd card has to be shuffled", wasShuffled);
	}

	@Test
	public void testShuffleAll(){
		final DeckDesc deckDesc = DeckDescFormat.parseString("deck1",
			"--skip-shuffle = 0 \n"
				+ "Player level: 85 \n"
				+ "Guy1, 1, 1, 1, 1, Special \n"
				+ "Guy2, 1, 1, 1, 1, Special \n"
				+ "Guy3, 1, 1, 1, 1, Special \n"
				+ "Guy4, 1, 1, 1, 1, Special \n");

		boolean wasShuffled = false;
		for(int i = 0; i < 10; i++) {
			final Player player = new Player(deckDesc);
			Card card;
			card = player.drawCardFromDeck();
			wasShuffled = wasShuffled || !card.getName().equals("Guy1");
		}
		Assert.assertTrue("3rd card has to be shuffled", wasShuffled);
	}

	@Test
	public void testShuffleNone(){
		final DeckDesc deckDesc = DeckDescFormat.parseString("deck1",
			"--skip-shuffle=3 \n"
				+ "Player level: 85 \n"
				+ "Guy1, 1, 1, 1, 1, Special \n"
				+ "Guy2, 1, 1, 1, 1, Special \n"
				+ "Guy3, 1, 1, 1, 1, Special \n"
				+ "Guy4, 1, 1, 1, 1, Special \n");

		for(int i = 0; i < 10; i++) {
			final Player player = new Player(deckDesc);
			Card card;
			card = player.drawCardFromDeck();
			Assert.assertTrue(card.getName().equals("Guy1"));
			card = player.drawCardFromDeck();
			Assert.assertTrue(card.getName().equals("Guy2"));
			card = player.drawCardFromDeck();
			Assert.assertTrue(card.getName().equals("Guy3"));
			card = player.drawCardFromDeck();
			Assert.assertTrue(card.getName().equals("Guy4"));
		}
	}

}
