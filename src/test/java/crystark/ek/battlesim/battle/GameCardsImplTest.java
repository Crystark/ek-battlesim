package crystark.ek.battlesim.battle;

import crystark.common.api.CardDesc;
import crystark.common.api.Game;
import crystark.common.db.CardDb;
import crystark.common.db.Database;
import crystark.common.events.Warnings;
import crystark.common.format.CardDescFormat;
import crystark.ek.EkbsConfig;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import rx.Observable;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@RunWith(Parameterized.class)
public class GameCardsImplTest {

	@Parameterized.Parameters(name = "{0}")
	public static List<Game> parameters(){
		return Arrays.asList(Game.values());
	};

	@Parameterized.Parameter
	public Game testGame;

	@After
	public void switchToDefaultGame(){
		EkbsConfig.current().switchGame(Game.mr);
	}

	@Test
	public void allCardsCanBeUsed() {

		final EkbsConfig ekbsConfig = EkbsConfig.current();
		ekbsConfig.switchGame(testGame);

		AtomicBoolean failed = new AtomicBoolean();
		final CardDb cards = Database.get().cards();
		Warnings.getAndReset();
		cards.all()
			.forEach(card -> {
				try {
					CardDesc cd = CardDescFormat.parseSimple(card.name());
					Assert.assertNotNull(cd);
					new Card(cd, null);

					final Observable<String> messages = Warnings.getAndReset();
					messages.forEach((msg)->{
							failed.set(true);
							System.out.println("Card parsing failed for '" + card.name() + "' because of \"" + msg +"\"; ");
						}
					);
				}
				catch (Exception e) {
					failed.set(true);
					Warnings.getAndReset();
					System.out.println("Card parsing error for '" + card.name() + "' because of \"" + e.getMessage() + "\"");
				}
			});

		//	Warnings.getAndReset().toBlocking().forEach(System.out::println);
		Assert.assertFalse("Some cards can't be used", failed.get());
	}
}
