package crystark.ek.battlesim.battle;

import org.junit.Assert;
import org.junit.Test;

public class CardInFieldIteratorTest {
	@Test
	public void test_iterator() {
		Player player = PlayerWithAccess
			.withCardsOnField(
				"Fire Demon",
				"Demonic Imp",
				"Ice Dragon");

		CardInFieldIterator cifIterator = new CardInFieldIterator(player.field, CardPredicates.atIndexes(0, 2));

		Assert.assertTrue(cifIterator.hasNext());
		Assert.assertEquals("Fire Demon", cifIterator.next().desc.name);
		Assert.assertTrue(cifIterator.hasNext());
		Assert.assertEquals("Ice Dragon", cifIterator.next().desc.name);
		Assert.assertFalse(cifIterator.hasNext());
	}
}
