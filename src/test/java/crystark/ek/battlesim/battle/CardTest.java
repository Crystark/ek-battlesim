package crystark.ek.battlesim.battle;

import crystark.common.api.CardDesc;
import crystark.ek.battlesim.ability.impl.Infiltrator;
import crystark.ek.battlesim.ability.impl.TerrorRoar;
import crystark.ek.battlesim.status.StatusEffect;
import crystark.ek.battlesim.status.StatusEffectType;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class CardTest {

	@Test
	public void test_cardStillReferencesUselessSkills() {
		Card card = Cards.from("Cancer, 18, 4, 1750, 25000, tundra, Cancer", null);

		Assert.assertEquals(1, card.desc.abilities.size());
	}

	@Test
	public void test_coldBloodRemoveHp() {
		Card card = Cards.from("Gonna-bleed Guy, 1, 1, 1, 1000, Special", null);

		Assert.assertEquals(1000, card.getCurrentHp());
		Assert.assertEquals(1000, card.getCurrentBaseHp());

		// Apply 2 auras
		card.addBonusHp(100);
		card.addBonusHp(100);
		Assert.assertEquals(1200, card.getCurrentHp());
		Assert.assertEquals(1200, card.getCurrentBaseHp());

		// Cold Blood
		card.coldBloodRemoveHp(150);
		Assert.assertEquals(1050, card.getCurrentHp());
		Assert.assertEquals(1050, card.getCurrentBaseHp());

		// Remove one auras
		card.removeBonusHp(100);
		Assert.assertEquals("Should not change anything", 1050, card.getCurrentHp());
		Assert.assertEquals("Should not change anything", 1050, card.getCurrentBaseHp());

		// Remove last auras
		card.removeBonusHp(100);
		Assert.assertEquals("Should remove the last 50 bonus", 1000, card.getCurrentHp());
		Assert.assertEquals("Should remove the last 50 bonus", 1000, card.getCurrentBaseHp());
	}

	@Test
	public void test_coldBloodAndLastChanceCombined() {
		PlayerWithAccess p = PlayerWithAccess
			.withCardsOnField("Last Chance Guy, 1, 1, 1, 1000, Special, Last Chance");

		Card card = p.atIndexOnField(0);

		Assert.assertEquals(1000, card.getCurrentHp());
		Assert.assertEquals(1000, card.getCurrentBaseHp());

		// Cold Blood
		card.coldBloodRemoveHp(1000);
		Assert.assertEquals(1, card.getCurrentHp());
		Assert.assertEquals(1, card.getCurrentBaseHp());
	}

	@Test
	public void test_wickedLeechRemoveAtk() {
		// Card base
		// (base:1000, bonus: 0, craze:0, currBase:1000)
		// +Aura => +200
		// (base:1000, bonus: 200, craze:0, currBase:1200)
		// WL => -150
		// (base:1000, bonus: 200, craze:0, currBase:1050)
		// -Aura => -100
		// (base:1000, bonus: 100, craze:0, currBase:1050)
		// -Aura => -100
		// (base:1000, bonus: 0, craze:0, currBase:1000)

		Card card = Cards.from("Gonna-be-leeched Guy, 10, 2, 1000, 1000, Special", null);

		Assert.assertEquals(1000, card.getCurrentAtk());
		Assert.assertEquals(1000, card.getCurrentBaseAtk());

		// Apply 2 auras
		card.addBonusAtk(100);
		card.addBonusAtk(100);
		Assert.assertEquals(1200, card.getCurrentAtk());
		Assert.assertEquals(1200, card.getCurrentBaseAtk());

		// Wicked leech "bug" test
		card.wickedLeechRemoveAtk(150);
		Assert.assertEquals(1050, card.getCurrentAtk());
		Assert.assertEquals(1050, card.getCurrentBaseAtk());

		// Remove one auras
		card.removeBonusAtk(100);
		Assert.assertEquals("Should not change anything", 1050, card.getCurrentAtk());
		Assert.assertEquals("Should not change anything", 1050, card.getCurrentBaseAtk());

		// Remove last auras
		card.removeBonusAtk(100);
		Assert.assertEquals("Should remove the last 50 bonus", 1000, card.getCurrentAtk());
		Assert.assertEquals("Should remove the last 50 bonus", 1000, card.getCurrentBaseAtk());
	}

	@Test
	public void test_wickedLeechAndCrazeCombined() {
		// Card base
		// (base:1000, bonus: 0, craze:0, currBase:1000)
		// +Aura => +500
		// (base:1000, bonus: 500, craze:0, currBase:1500)
		// WL => -150
		// (base:1000, bonus: 500, craze:0, currBase:1350)
		// Craze => +50
		// (base:1000, bonus: 500, craze:50, currBase:1400)
		// -Aura => -150 (absorbed by WL)
		// (base:1000, bonus: 350, craze:50, currBase:1400)
		// -Aura => -150
		// (base:1000, bonus: 200, craze:50, currBase:1300)
		// -Aura => -200
		// (base:1000, bonus: 0, craze:50, currBase:1100)

		Card card = Cards.from("Gonna-be-leeched crazy Guy, 10, 2, 1000, 1000, Special", null);

		Assert.assertEquals(1000, card.getCurrentAtk());
		Assert.assertEquals(1000, card.getCurrentBaseAtk());

		// Apply auras
		card.addBonusAtk(150);
		card.addBonusAtk(150);
		card.addBonusAtk(200);
		Assert.assertEquals(1500, card.getCurrentAtk());
		Assert.assertEquals(1500, card.getCurrentBaseAtk());

		// Wicked leech "bug" test
		card.wickedLeechRemoveAtk(150);
		Assert.assertEquals(1350, card.getCurrentAtk());
		Assert.assertEquals(1350, card.getCurrentBaseAtk());

		// Craze
		card.crazeAddAtk(50);
		Assert.assertEquals(1400, card.getCurrentAtk());
		Assert.assertEquals(1400, card.getCurrentBaseAtk());

		// Remove part of auras
		card.removeBonusAtk(150);
		Assert.assertEquals("Should not change anything", 1400, card.getCurrentAtk());
		Assert.assertEquals("Should not change anything", 1400, card.getCurrentBaseAtk());

		// Remove some more auras
		card.removeBonusAtk(150);
		Assert.assertEquals("Should remove only 100", 1300, card.getCurrentAtk());
		Assert.assertEquals("Should remove only 100", 1300, card.getCurrentBaseAtk());

		// Remove all remaining auras
		card.removeBonusAtk(200);
		Assert.assertEquals(1100, card.getCurrentAtk());
		Assert.assertEquals(1100, card.getCurrentBaseAtk());
	}

	@Test
	public void test_wickedLeechCrazeAndBloodthirstyCombined() {
		// Card base
		// (base:1000, bonus: 0, craze:0, currBase:1000)
		// +Aura => +500
		// (base:1000, bonus: 500, craze:0, currBase:1500)
		// Bloodthirsty => +50
		// (base:1050, bonus: 500, craze:0, currBase:1550)
		// WL => -150
		// (base:1050, bonus: 500, craze:0, currBase:1400)
		// Craze => +50
		// (base:1050, bonus: 500, craze:50, currBase:1450)
		// -Aura => -150 (absorbed by WL)
		// (base:1050, bonus: 350, craze:50, currBase:1450)
		// -Aura => -150
		// (base:1050, bonus: 200, craze:50, currBase:1350)
		// -Aura => -200
		// (base:1050, bonus: 0, craze:50, currBase:1150)

		Card card = Cards.from("Gonna-be-leeched crazy Guy, 10, 2, 1000, 1000, Special", null);

		Assert.assertEquals(1000, card.getCurrentAtk());
		Assert.assertEquals(1000, card.getCurrentBaseAtk());

		// Apply auras
		card.addBonusAtk(150);
		card.addBonusAtk(150);
		card.addBonusAtk(200);
		Assert.assertEquals(1500, card.getCurrentAtk());
		Assert.assertEquals(1500, card.getCurrentBaseAtk());

		// BloodThirsty
		card.addBaseAtk(50);
		Assert.assertEquals(1550, card.getCurrentAtk());
		Assert.assertEquals(1550, card.getCurrentBaseAtk());

		// Wicked leech "bug" test
		card.wickedLeechRemoveAtk(150);
		Assert.assertEquals(1400, card.getCurrentAtk());
		Assert.assertEquals(1400, card.getCurrentBaseAtk());

		// Craze
		card.crazeAddAtk(50);
		Assert.assertEquals(1450, card.getCurrentAtk());
		Assert.assertEquals(1450, card.getCurrentBaseAtk());

		// Remove part of auras
		card.removeBonusAtk(150);
		Assert.assertEquals("Should not change anything", 1450, card.getCurrentAtk());
		Assert.assertEquals("Should not change anything", 1450, card.getCurrentBaseAtk());

		// Remove some more auras
		card.removeBonusAtk(150);
		Assert.assertEquals("Should remove only 100", 1350, card.getCurrentAtk());
		Assert.assertEquals("Should remove only 100", 1350, card.getCurrentBaseAtk());

		// Remove all remaining auras
		card.removeBonusAtk(200);
		Assert.assertEquals(1150, card.getCurrentAtk());
		Assert.assertEquals(1150, card.getCurrentBaseAtk());
	}

	@Test
	public void test_wickedLeech_underBase() {
		// Card base
		// (base:1000, bonus: 0, craze:0, currBase:1000)
		// +Aura => +500
		// (base:1000, bonus: 500, craze:0, currBase:1500)
		// WL => -600
		// (base:1000, bonus: 500, craze:0, currBase:900)
		// -Aura => -500
		// (base:1000, bonus: 0, craze:0, currBase:900)
		// +Aura => +150
		// (base:1000, bonus: 100, craze:0, currBase:1050)
		// -Aura => -150
		// (base:1000, bonus: 0, craze:0, currBase:1000)

		Card card = Cards.from("Gonna-be-leeched crazy Guy, 10, 2, 1000, 1000, Special", null);

		Assert.assertEquals(1000, card.getCurrentAtk());
		Assert.assertEquals(1000, card.getCurrentBaseAtk());

		// Apply auras
		card.addBonusAtk(150);
		card.addBonusAtk(150);
		card.addBonusAtk(200);
		Assert.assertEquals(1500, card.getCurrentAtk());
		Assert.assertEquals(1500, card.getCurrentBaseAtk());

		// Wicked leech "bug" test
		card.wickedLeechRemoveAtk(600);
		Assert.assertEquals(900, card.getCurrentAtk());
		Assert.assertEquals(900, card.getCurrentBaseAtk());

		// Remove auras
		card.removeBonusAtk(150);
		card.removeBonusAtk(150);
		card.removeBonusAtk(200);
		Assert.assertEquals("Should not change anything", 900, card.getCurrentAtk());
		Assert.assertEquals("Should not change anything", 900, card.getCurrentBaseAtk());

		//Re-add aura
		card.addBonusAtk(150);
		Assert.assertEquals(1050, card.getCurrentAtk());
		Assert.assertEquals(1050, card.getCurrentBaseAtk());

		//Remove aura
		card.removeBonusAtk(150);
		Assert.assertEquals(1000, card.getCurrentAtk());
		Assert.assertEquals(1000, card.getCurrentBaseAtk());
	}

	@Test
	public void test_crazeAddAtk_afterAura() {
		// Card base
		// (base:1000, bonus: 0, craze:0, currBase:1000)
		// +Aura => +100
		// (base:1000, bonus: 100, craze:0, currBase:1100)
		// Craze => +50
		// (base:1000, bonus: 100, craze:50, currBase:1150)
		// -Aura => -100
		// (base:1000, bonus: 0, craze:50, currBase:1100)

		Card card = Cards.from("Crazy Guy, 10, 2, 1000, 1000, Special", null);

		Assert.assertEquals(1000, card.getCurrentAtk());
		Assert.assertEquals(1000, card.getCurrentBaseAtk());

		// Add one aura
		card.addBonusAtk(100);
		Assert.assertEquals(1100, card.getCurrentAtk());
		Assert.assertEquals(1100, card.getCurrentBaseAtk());

		// Craze "bug" test
		card.crazeAddAtk(50);
		Assert.assertEquals(1150, card.getCurrentAtk());
		Assert.assertEquals(1150, card.getCurrentBaseAtk());

		// Remove the aura aura
		card.removeBonusAtk(100);
		Assert.assertEquals("Should only remove 50", 1100, card.getCurrentAtk());
		Assert.assertEquals("Should only remove 50", 1100, card.getCurrentBaseAtk());
	}

	@Test
	public void test_crazeAddAtk_beforeAura() {
		// Card base
		// (base:1000, bonus: 0, craze:0, currBase:1000)
		// Craze => +50
		// (base:1000, bonus: 0, craze:50, currBase:1050)
		// +Aura => +100
		// (base:1000, bonus: 100, craze:50, currBase:1150)
		// -Aura => -100
		// (base:1000, bonus: 0, craze:50, currBase:1100)

		Card card = Cards.from("Crazy Guy, 10, 2, 1000, 1000, Special", null);

		Assert.assertEquals(1000, card.getCurrentAtk());
		Assert.assertEquals(1000, card.getCurrentBaseAtk());

		// Craze "bug" test
		card.crazeAddAtk(50);
		Assert.assertEquals(1050, card.getCurrentAtk());
		Assert.assertEquals(1050, card.getCurrentBaseAtk());

		// Add one aura
		card.addBonusAtk(100);
		Assert.assertEquals(1150, card.getCurrentAtk());
		Assert.assertEquals(1150, card.getCurrentBaseAtk());

		// Remove the aura
		card.removeBonusAtk(100);
		Assert.assertEquals("Should only remove 50", 1100, card.getCurrentAtk());
		Assert.assertEquals("Should only remove 50", 1100, card.getCurrentBaseAtk());

		// PlagueOgryn
		// Cerato + Goblin Chemist
		//		595 Base
		//		670 Aura +75
		//		595 Death removes aura
		//		670 Rez > A
		//		595 D
		//		670 RA
		//		595 D
		//		670 RA
		//		595 D
		//		705 Craze
		//		780 RA
		//		780 D
		//		890 Craze
		//		965 RA
		//		965 D

		// SeaKing
		// Reindeer + RN 175 + RN 300 + FS
		//		680 Base
		//		740 Craze
		//		815 FS rez
		//		800 FS dead
		//		860 Craze
		//		1035 RN 175
		//		1110 FS rez
		//		1095 FS Dead
		//		1155 Craze
		//		1455 RN 300
		//		1530 FS rez
		//		1515 FS dead
	}

	@Test
	public void test_crazeAddAtk_higherThanAura() {
		// Card base
		// (base:1000, bonus: 0, craze:0, currBase:1000)
		// +Aura => +50
		// (base:1000, bonus: 50, craze:0, currBase:1050)
		// Craze => +100
		// (base:1000, bonus: 50, craze:100, currBase:1150)
		// -Aura => -50
		// (base:1000, bonus: 0, craze:100, currBase:1150)

		Card card = Cards.from("Crazy Guy, 10, 2, 1000, 1000, Special", null);

		Assert.assertEquals(1000, card.getCurrentAtk());
		Assert.assertEquals(1000, card.getCurrentBaseAtk());

		// Add one aura
		card.addBonusAtk(50);
		Assert.assertEquals(1050, card.getCurrentAtk());
		Assert.assertEquals(1050, card.getCurrentBaseAtk());

		// Craze "bug" test
		card.crazeAddAtk(100);
		Assert.assertEquals(1150, card.getCurrentAtk());
		Assert.assertEquals(1150, card.getCurrentBaseAtk());

		// Remove the aura
		card.removeBonusAtk(50);
		Assert.assertEquals("Should not remove anything", 1150, card.getCurrentAtk());
		Assert.assertEquals("Should not remove anything", 1150, card.getCurrentBaseAtk());
	}

	@Test
	public void test_crazeAddAtk_multiAura() {
		// RW base
		// (base:680, bonus: 0, craze:0, currBase:680)
		// Aura from 2xRN + TH
		// (base:680, bonus: 800, craze:0, currBase:1480)
		// TH Sniped => -150
		// (base:680, bonus: 650, craze:0, currBase:1330)
		// RW craze => +60
		// (base:680, bonus: 650, craze:60, currBase:1390)
		// TH rea => +150
		// (base:680, bonus: 800, craze:60, currBase:1540)
		// TH Sniped => -90
		// (base:680, bonus: 650, craze:60, currBase:1450)

		Card card = Cards.from("Crazy Guy, 10, 2, 680, 1000, Special", null);

		Assert.assertEquals(680, card.getCurrentAtk());
		Assert.assertEquals(680, card.getCurrentBaseAtk());

		// Add 3 auras
		card.addBonusAtk(325);
		card.addBonusAtk(325);
		card.addBonusAtk(150);
		Assert.assertEquals(1480, card.getCurrentAtk());
		Assert.assertEquals(1480, card.getCurrentBaseAtk());

		// Remove one aura
		card.removeBonusAtk(150);
		Assert.assertEquals(1330, card.getCurrentAtk());
		Assert.assertEquals(1330, card.getCurrentBaseAtk());

		// Craze!
		card.crazeAddAtk(60);
		Assert.assertEquals(1390, card.getCurrentAtk());
		Assert.assertEquals(1390, card.getCurrentBaseAtk());

		// Add back aura
		card.addBonusAtk(150);
		Assert.assertEquals(1540, card.getCurrentAtk());
		Assert.assertEquals(1540, card.getCurrentBaseAtk());

		// Remove aura gain
		card.removeBonusAtk(150);
		Assert.assertEquals(1450, card.getCurrentAtk());
		Assert.assertEquals(1450, card.getCurrentBaseAtk());
	}

	@Test
	public void test_snapshot() {
		CardDesc newCard;
		Card card = Cards.from("Truncated Guy, 10, 2, 1000, 1000, Special, Resistance", null);
		card.damaged(null, 100, DamageType.unavoidable);

		newCard = card.snapshot();
		Assert.assertEquals(900, newCard.health);
		Assert.assertSame(card.desc.abilities.get(0), newCard.abilities.get(0));
	}

	@Test
	public void test_boost() {
		Card card = Cards.from("Boosted Guy, 10, 2, 1000, 1000, Special", null);

		// Add boost
		card.modifyAtk(100);
		Assert.assertEquals("Should add 100", 1100, card.getCurrentAtk());
		Assert.assertEquals("Should not change anything", 1000, card.getCurrentBaseAtk());
		Assert.assertEquals("Should not change anything", 0, card.currentBonusAtk);
		// Remove boost
		card.modifyAtk(-100);
		Assert.assertEquals("Should remove 100", 1000, card.getCurrentAtk());
		Assert.assertEquals("Should not change anything", 1000, card.getCurrentBaseAtk());
		Assert.assertEquals("Should not change anything", 0, card.currentBonusAtk);
	}

	@Test
	public void test_lastChance() {
		Player player = PlayerWithAccess.withCardsOnField("Last Chance Guy, 10, 2, 1000, 1000, Special, D_Prayer:50, Last Chance, D_Prayer:100");

		Card card = player.atIndexOnField(0);
		player.damaged(300, false);

		// Preconditions
		Assert.assertEquals(player.desc.health - 300, (int) player.getCurrentHp());
		Assert.assertEquals(1000, card.getCurrentHp());
		Assert.assertFalse(card.isLastChance);

		// Try to kill
		card.damaged(null, (int) card.getCurrentHp(), DamageType.unavoidable);

		Assert.assertEquals("Should survive with 1 HP", 1, card.getCurrentHp());
		Assert.assertTrue("Last chance should now be active", card.isLastChance);
		Assert.assertEquals("Should recover 50 HP from first desperation skill", player.desc.health - 250, (int) player.getCurrentHp());

		// Try more damage
		card.damaged(null, 1000, null);
		Assert.assertEquals("Card shoudln't be affected by damage", 1, card.getCurrentHp());
		Assert.assertEquals(player.desc.health - 250, (int) player.getCurrentHp());

		// Simulate next turn and damage
		card.onTurnStart();
		card.damaged(null, 1000, null);
		Assert.assertEquals("Czard should die", 0, card.getCurrentHp());
		Assert.assertEquals("Should recover 150 HP from both desperation skills", player.desc.health - 100, (int) player.getCurrentHp());
	}

	@Test
	public void test_purify() {
		// TODO add more purification tests
		Player player = PlayerWithAccess.withCardsOnField("Corrupted Guy, 10, 2, 1000, 1000, Special");

		Card corrupted = player.atIndexOnField(0);
		ArrayList<StatusEffect> debuffs = corrupted.pickStatusList(StatusEffectType.debuff);

		Assert.assertEquals(0, debuffs.size());

		new TerrorRoar().terrorRoar(player);
		Assert.assertEquals(1, debuffs.size());

		corrupted.purify();
		Assert.assertEquals(0, debuffs.size());
	}

	@Test
	public void test_blind() {
		//		Player player = PlayerWithAccess.withCardsOnField("Blinding Guy, 1, 1, 0, 1000, Special, Blind:100");
		//		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Random Guy, 1, 1, 1, 1000, Special");
		//		Player.salute(player, opponent);
		//
		//		player.playCardsFromField();
		//		Assert.assertTrue(opponent.atIndexOnField(0).isBlind());
		//
		//		opponent.playCardsFromField();

		Infiltrator infiltrator = new Infiltrator();
		Card card1 = Cards.from("Random Guy, 1, 1, 1, 1000, Special", null);
		Card card2 = Cards.from("Random Guy, 1, 1, 1, 1000, Special", null);
		card1.markOnField(true);
		card2.markOnField(true);

		card1.markBlind(100);
		card1.addAbility(1L, infiltrator);
		card1.attack(card2, 10);
		Assert.assertEquals("Infiltrator shouldn't care about blindness", 990, card2.getCurrentHp());

		card1.removeAbility(infiltrator);
		card1.attack(card2, 10);
		Assert.assertEquals("100% blind should miss each time", 990, card2.getCurrentHp());

		card1.markBlind(0);
		card1.attack(card2, 10);
		Assert.assertEquals("0% blind should hit each time", 980, card2.getCurrentHp());
	}

	@Test
	public void test_silenceCard() {
		Player player = PlayerWithAccess.withCardsOnField(
			"Silenced guy, 1, 1, 100, 10000, Swamp, Dexterity:100, Sensitive:100, Reflection:100, Dodge:100, Immunity, Resistance, Guard, Rejuvenation:25, Bloodsucker:100",
			"Random Guy, 1, 1, 1, 10000, Swamp, Acrobatics, Immunity, Resistance");
		Player opponent = PlayerWithAccess.withCardsOnField(
			"Silencing Guy guy, 1, 1, 100, 1000, Special, Exile, Salvo:10:99, Feast of Blood:15, Puncture:110, Counterattack:120");
		Player.salute(player, opponent);

		opponent.playCardsFromField();
		final Card card = player.atIndexOnField(0);
		long expectedHealth = card.desc.health;
		Assert.assertEquals(expectedHealth, card.getCurrentHp());

		card.markSilenced();
/*
		card.allAbilities.forEach(a -> Assert.assertTrue(a + " should be silenced", !a.canBeLaunched(card)));
		card.purify();
		card.allAbilities.forEach(a -> Assert.assertTrue(a + " should still be silenced after purification", !a.canBeLaunched(card)));
*/

		opponent.playCardsFromField();
		final Card card2 = player.atIndexOnField(0);
		Assert.assertNotNull("Silenced card has to survive", card2);
		expectedHealth -= 100; // attack
		expectedHealth -= 10; // salvo
		expectedHealth -= 15; // Feast of Blood
		expectedHealth -= 110; // puncture + guard
		Assert.assertEquals(expectedHealth, card2.getCurrentHp());

		player.playCardsFromField();
		card2.allAbilities.forEach(a -> Assert.assertTrue(a + " should not be silenced anymore", a.canBeLaunched(card2)));
		expectedHealth -= 120; // Counterattack (Dexterity Fails)
		//expectedHealth += 25; // Rejuvenation doesn't works
		Assert.assertEquals(expectedHealth, card2.getCurrentHp());

		opponent.playCardsFromField();
		Assert.assertEquals(expectedHealth, card2.getCurrentHp());

	}
}
