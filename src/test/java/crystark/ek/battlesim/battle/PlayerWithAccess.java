package crystark.ek.battlesim.battle;

import crystark.common.api.DeckDesc;
import crystark.common.db.model.Deck;
import crystark.common.format.CardDescFormat;
import crystark.common.format.RuneDescFormat;
import crystark.ek.battlesim.rune.RuneFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PlayerWithAccess extends Player {
	public PlayerWithAccess(DeckDesc desc) {
		super(desc);
	}


	public PlayerWithAccess allCardsOnField() {
		Card card;
		while ((card = drawCardFromDeck()) != null) {
			this.addCardToField(card);
		}

		cleanup();

		return this;
	}

	public PlayerWithAccess withCardsInCemetery(String... cards) {
		for (String card : cards) {
			this.addCardToCemetery(Cards.from(card, this));
		}
		return this;
	}

	public PlayerWithAccess withCardsInHand(String... cards) {
		for (String card : cards) {
			this.addCardToHand(Cards.from(card, this));
		}
		return this;
	}

	public PlayerWithAccess withCardsInDeck(String... cards) {
		for (String card : cards) {
			this.addCardToDeck(Cards.from(card, this), 0);
		}
		return this;
	}

	public static PlayerWithAccess withoutCards() {
		PlayerWithAccess player =
			new PlayerWithAccess(
				new DeckDesc(
					"PlayerWithAccess No cards",
					"PlayerWithAccess No cards",
					85,
					Deck.calcHp(85),
					new ArrayList<>(),
					new ArrayList<>(4),
					Integer.MAX_VALUE,
					false,
					false));
		return player;
	}

	public static PlayerWithAccess withoutCards(int level, int skipShuffle, boolean alwaysStart, boolean infiniteHp) {
		PlayerWithAccess player =
			new PlayerWithAccess(
				new DeckDesc(
					"PlayerWithAccess No cards",
					"PlayerWithAccess No cards",
					level,
					Deck.calcHp(level),
					new ArrayList<>(),
					new ArrayList<>(4),
					skipShuffle,
					alwaysStart,
					infiniteHp));
		return player;
	}

	public PlayerWithAccess addCardsToField(String... descriptions) {
		final List<Card> cards = Arrays
			.stream(descriptions)
			.map(CardDescFormat::parse)
			.map(d -> {
				Card c = new Card(d, this);
				c.markOnField(true);
				return c;
			})
			.collect(Collectors.toList());
		this.field.addAll(cards);
		return this;
	}
	public static PlayerWithAccess withCardsOnField(String... descriptions) {
		// Build a player
		String name = descriptions.length > 0 ? descriptions[0].split(",")[0] : "No cards";
		PlayerWithAccess player =
			new PlayerWithAccess(
				new DeckDesc(
					"PlayerWithAccess " + name,
					"PlayerWithAccess " + name,
					85,
					Deck.calcHp(85),
					Arrays
						.stream(descriptions)
						.map(CardDescFormat::parse)
						.collect(Collectors.toList()),
					new ArrayList<>(4),
					Integer.MAX_VALUE,
					false,
					false));
		player.allCardsOnField();

		return player;
	}

	public PlayerWithAccess withRunes(String... runes) {
		this.runes.addAll(Arrays.stream(runes).map(RuneDescFormat::parse).map(RuneFactory::get).collect(Collectors.toList()));
		return this;
	}
}
