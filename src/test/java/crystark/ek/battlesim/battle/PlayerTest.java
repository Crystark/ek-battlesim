package crystark.ek.battlesim.battle;

import java.util.List;
import java.util.stream.Collectors;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

public class PlayerTest {

	@Test
	public void test_applyPurification() {
		// Test add and apply
		PlayerWithAccess p = PlayerWithAccess
			.withCardsOnField("Electric Eel", "Aries", "Scarab", "Angelia the Pure");

		Card cardBefore = p.field.get(0);
		Card self = p.field.get(1);
		Card cardAfter = p.field.get(2);

		Assert.assertEquals(1, p.beforeRoundCards.size());
		Assert.assertEquals(self, p.beforeRoundCards.get(0));

		cardBefore.markFrozen();
		self.markStunned();
		cardAfter.markConfused();

		Assert.assertTrue(cardBefore.isFrozen());
		Assert.assertTrue(self.isStunned());
		Assert.assertTrue(cardAfter.isConfused());

		p.applyBeforeRound();

		Assert.assertFalse(cardBefore.isFrozen());
		Assert.assertFalse(self.isStunned());
		Assert.assertFalse(cardAfter.isConfused());

		// Test remove
		p.removeCardFromField(self);
		Assert.assertEquals(0, p.beforeRoundCards.size());

		cardBefore.markFrozen();
		cardAfter.markConfused();

		Assert.assertTrue(cardBefore.isFrozen());
		Assert.assertTrue(cardAfter.isConfused());

		p.applyBeforeRound();

		Assert.assertTrue(cardBefore.isFrozen());
		Assert.assertTrue(cardAfter.isConfused());
	}

	@Test
	public void test_deadCardCantAttack() {
		Player player = PlayerWithAccess.withCardsOnField("Dies Before Strike, 1, 1, 1000, 200, Special, Electric Shock 10");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Reflection Guy, 1, 1, 1, 1000, Special, Reflection:200");
		Player.salute(player, opponent);

		player.playCardsFromField();
		Assert.assertEquals(1000, (int) opponent.atIndexOnField(0).getCurrentHp());
		Assert.assertEquals((int) opponent.desc.health, opponent.getCurrentHp());
	}

	@Test
	public void test_pickLowestHpCards() {
		Player player = PlayerWithAccess
			.withCardsOnField(
				"Health1, 1, 1, 1, 1, Special",
				"Health4, 1, 1, 1, 4, Special",
				"Health3b, 1, 1, 1, 3, Special",
				"Health2, 1, 1, 1, 2, Special",
				"Health6, 1, 1, 1, 6, Special",
				"Health3a, 1, 1, 1, 3, Special",
				"Health5, 1, 1, 1, 5, Special");

		List<Card> cards = player.pickLowestHpCards(3).stream().map(c -> c.card).collect(Collectors.toList());
		Assert.assertEquals(3, cards.size());
		Assert.assertEquals("Health1", cards.get(0).desc.name);
		Assert.assertEquals("Health2", cards.get(1).desc.name);
		Assert.assertThat(cards.get(2).desc.name, Matchers.anyOf(Matchers.equalTo("Health3a"), Matchers.equalTo("Health3b")));
	}
}
