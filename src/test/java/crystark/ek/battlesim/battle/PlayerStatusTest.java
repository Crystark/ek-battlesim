package crystark.ek.battlesim.battle;

import org.junit.Assert;
import org.junit.Test;

public class PlayerStatusTest {

	@Test
	public void test_hasSuppressing() {
		PlayerStatus s = new PlayerStatus();
		Card c = Cards.from("Suppresisng Guy, 1, 1, 1, 1, special, Suppressing", null);

		Assert.assertFalse(s.hasSuppressing());

		s.cardAddedToField(c);
		Assert.assertTrue(s.hasSuppressing());

		c.markSilenced();
		Assert.assertFalse(s.hasSuppressing());

		c.removeSilence();
		Assert.assertTrue(s.hasSuppressing());

		s.cardRemovedFromField(c);
		Assert.assertFalse(s.hasSuppressing());
	}
}
