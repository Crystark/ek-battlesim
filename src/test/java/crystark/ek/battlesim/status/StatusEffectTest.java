package crystark.ek.battlesim.status;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Cards;
import crystark.ek.battlesim.effect.BonusBaseAtk;

public class StatusEffectTest {
	@Test
	public void test_apply() {
		Card card = Cards.from("Sea King", null);
		int v = 5;
		StatusEffect bba = StatusEffect.builder()
			.setEffect(new BonusBaseAtk(v))
			.setType(StatusEffectType.aura)
			.build();

		int baseAtk = card.desc.attack;
		int currentBaseAtk = card.getCurrentBaseAtk();
		int currentAtk = card.getCurrentAtk();

		Assert.assertFalse("Status should not yet be registered on the card", card.hasStatus(bba));

		bba.applyTo(card);

		Assert.assertEquals("Original base atk should not have changed", baseAtk, card.desc.attack);
		Assert.assertEquals("Current base atk should be increased by " + v, currentBaseAtk + v, card.getCurrentBaseAtk());
		Assert.assertEquals("Current atk should be increased by " + v, currentAtk + v, card.getCurrentAtk());
		Assert.assertTrue("Status should be registered on the card", card.hasStatus(bba));

		bba.removeFrom(card);

		Assert.assertEquals("Original base atk should not have changed", baseAtk, card.desc.attack);
		Assert.assertEquals("Current base atk should be back to normal", currentBaseAtk, card.getCurrentBaseAtk());
		Assert.assertEquals("Current atk should be back to normal", currentAtk, card.getCurrentAtk());
		Assert.assertFalse("Status should not be registered on the card anymore", card.hasStatus(bba));
	}
}
