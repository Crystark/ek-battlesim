package crystark.ek.battlesim.ability.addons;

import crystark.common.api.Game;
import crystark.ek.EkbsConfig;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

public class SuperMagicErosionTest {
	@Test
	public void test_apply() {
		EkbsConfig.current().switchGame(Game.loa);
		Player player = PlayerWithAccess.withCardsOnField("Super Magic Erosion Guy, 0, 0, 0, 1, Special, Super Magic Erosion:150");
		PlayerWithAccess opponent = PlayerWithAccess
			.withCardsOnField(
				"Immune Guy, 1, 1, 1, 1000, Special, Immunity",
				"Reflection Guy, 1, 1, 1, 1000, Special, Reflection:150",
				"Random Guy, 1, 1, 1, 1000, Special",
				"Random Guy, 1, 1, 1, 1000, Special");
		Player.salute(player, opponent);

		player.atIndexOnField(0).applyPowerAbilities();

		EkbsConfig.current().switchGame(Game.mr);

		Assert.assertEquals(550, (int) opponent.atIndexOnField(0).getCurrentHp());
		Assert.assertEquals(550, (int) opponent.atIndexOnField(1).getCurrentHp());
		Assert.assertEquals(850, (int) opponent.atIndexOnField(2).getCurrentHp());
		Assert.assertEquals(850, (int) opponent.atIndexOnField(2).getCurrentHp());
	}
}
