package crystark.ek.battlesim.ability.addons;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.PlayerWithAccess;

public class PsychicMasterHandTest {
	@Test
	public void test_apply() {
		PlayerWithAccess player = PlayerWithAccess
			.withCardsOnField("The Psychic Master, 1, 1, 1, 1, Special, Psychic Master Hand")
			.withCardsInHand(
				"T2 Guy,   1, 2, 1, 1, Special",
				"T6 Guy 1, 1, 6, 1, 1, Special",
				"T4 Guy,   1, 4, 1, 1, Special",
				"T6 Guy 2, 1, 6, 1, 1, Special",
				"T4 Guy,   1, 4, 1, 1, Special");

		player.atIndexOnField(0).applyPowerAbilities();

		Assert.assertEquals("T6 Guy 1", player.atIndexOnField(1).desc.name);
	}
}
