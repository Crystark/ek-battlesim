package crystark.ek.battlesim.ability.addons;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class AbilityProviderTest {

	@Test
	public void test_apply() {
		Player player = PlayerWithAccess
			.withCardsOnField(
				"Snipe Provider Guy, 1, 0, 1, 1, Special, Ability Provider:Reflection 10",
				"Random Guy, 1, 0, 1, 1, Special");
		Player opponent = PlayerWithAccess.withoutCards();
		Player.salute(player, opponent);

		// Test base state: should not have Reflection
		Card testCard = player.atIndexOnField(0);
		Card otherCard = player.atIndexOnField(0);
		Assert.assertFalse(testCard.hasReflection());
		Assert.assertFalse(otherCard.hasReflection());

		testCard.applyPowerAbilities();
		Assert.assertTrue(testCard.hasReflection());
		Assert.assertTrue(otherCard.hasReflection());

		player.removeBuffs();

		Assert.assertFalse(testCard.hasReflection());
		Assert.assertFalse(otherCard.hasReflection());
	}

	@Test
	@Ignore
	public void test_existing_skill() {
		Player player = PlayerWithAccess
			.withCardsOnField("Group Snipe Guy, 1, 0, 1, 1, Special, Group Reflection 10");
		Player opponent = PlayerWithAccess.withoutCards();
		Player.salute(player, opponent);

		// Test base state: should not have Reflection
		Card testCard = player.atIndexOnField(0);
		Card otherCard = player.atIndexOnField(0);
		Assert.assertFalse(testCard.hasReflection());
		Assert.assertFalse(otherCard.hasReflection());

		testCard.applyPowerAbilities();
		Assert.assertTrue(testCard.hasReflection());
		Assert.assertTrue(otherCard.hasReflection());

		player.removeBuffs();

		Assert.assertFalse(testCard.hasReflection());
		Assert.assertFalse(otherCard.hasReflection());
	}
}
