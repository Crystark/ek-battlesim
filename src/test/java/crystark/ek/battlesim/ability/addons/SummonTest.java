package crystark.ek.battlesim.ability.addons;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

/**
 * @see SecondWind
 */
public class SummonTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess
			.withCardsOnField("Summon scarab Guy, 1, 0, 1, 1, Special, Summon: Scarab, Resurrection:100");
		Player opponent = PlayerWithAccess.withoutCards();
		Player.salute(player, opponent);

		Card testCard = player.atIndexOnField(0);

		Summon ab = (Summon) testCard.getPowerAbilities().get(0);

		Assert.assertEquals(1, player.countCardsInField());
		ab.apply(testCard); // Try summoning the scarab
		Assert.assertEquals(2, player.countCardsInField());
		ab.apply(testCard); // Try summoning the scarab
		Assert.assertEquals(2, player.countCardsInField());

		Card scarab = player.atIndexOnField(1);
		Assert.assertEquals("Scarab", scarab.desc.name);

		scarab.damaged(null, (int) scarab.desc.health, DamageType.unavoidable); // Kill the scarab
		Assert.assertEquals("The scarab should be dead", 1, player.countCardsInField());
		Assert.assertEquals("The scarab should not be in the cemetery", 0, player.countCardsInCemetery());

		ab.apply(testCard); // Try summoning the scarab
		Assert.assertEquals("The summon skill should resummon the scarab", 2, player.countCardsInField());

		testCard.damaged(null, 1, DamageType.unavoidable); // Kill the summoner
		player.playCardsFromHand(); // Put the summoner back on the field
		ab.apply(testCard); // Try summoning the scarab
		Assert.assertEquals("The scarab should not be summoned again", 2, player.countCardsInField());
	}

	@Test
	public void test_complex() {
		Player player = PlayerWithAccess
			.withCardsOnField("Summon scarab Guy, 1, 1, 1, 1, Special, Summon: Thunder Dragon\\, 15\\, Blizzard\\:100");

		Card testCard = player.atIndexOnField(0);
		testCard.getPowerAbilities().get(0).apply(testCard);

		Card summonedCard = player.atIndexOnField(1);
		Assert.assertEquals("Thunder Dragon", summonedCard.desc.name);
		Assert.assertEquals(15, summonedCard.desc.level);
		Assert.assertEquals("Blizzard", summonedCard.desc.abilities.get(3).name);
		Assert.assertEquals(100, summonedCard.desc.abilities.get(3).value);
	}

	@Test
	public void test_qs_complex() {
		Player player = PlayerWithAccess
			.withCardsOnField("Scarab, 10, QS_Summon: Thunder Dragon\\, 15\\, Blizzard\\:100");

		Card testCard = player.atIndexOnField(0);
		testCard.getQuickstrikeAbilities().get(0).apply(testCard);

		Card summonedCard = player.atIndexOnField(1);
		Assert.assertEquals("Thunder Dragon", summonedCard.desc.name);
		Assert.assertEquals(15, summonedCard.desc.level);
		Assert.assertEquals("Blizzard", summonedCard.desc.abilities.get(3).name);
		Assert.assertEquals(100, summonedCard.desc.abilities.get(3).value);
	}

	@Test
	public void test_multi_complex() {
		Player player = PlayerWithAccess
			.withCardsOnField("Summon Guy, 1, 1, 1, 1, Special, Summon: Moss Dragon\\,13 & Thunder Dragon\\, 15\\, Blizzard\\:100");

		Card testCard = player.atIndexOnField(0);
		testCard.getPowerAbilities().get(0).apply(testCard);

		Card summonedCard = player.atIndexOnField(1);
		Assert.assertEquals("Moss Dragon", summonedCard.desc.name);
		Assert.assertEquals(13, summonedCard.desc.level);

		summonedCard = player.atIndexOnField(2);
		Assert.assertEquals("Thunder Dragon", summonedCard.desc.name);
		Assert.assertEquals(15, summonedCard.desc.level);
		Assert.assertEquals("Blizzard", summonedCard.desc.abilities.get(3).name);
		Assert.assertEquals(100, summonedCard.desc.abilities.get(3).value);
	}

	@Test
	public void test_multi() {
		Player player = PlayerWithAccess
			.withCardsOnField("Summon Guy, 1, 1, 1, 1, Special, Summon: Moss Dragon\\,13 & Thunder Dragon\\, 15:1");

		Card testCard = player.atIndexOnField(0);
		testCard.getPowerAbilities().get(0).apply(testCard);

		Assert.assertEquals(2, player.countCardsInField());
		testCard.getPowerAbilities().get(0).apply(testCard);
		Assert.assertEquals(3, player.countCardsInField());
		testCard.getPowerAbilities().get(0).apply(testCard);
		Assert.assertEquals(3, player.countCardsInField());
	}
}
