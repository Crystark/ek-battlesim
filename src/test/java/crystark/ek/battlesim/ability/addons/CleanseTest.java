package crystark.ek.battlesim.ability.addons;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class CleanseTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess
			.withCardsOnField(
				"Too Far guy, 1, 1, 1, 1000, Special",
				"Random guy, 1, 1, 1, 1000, Special",
				"Cleanse guy, 1, 1, 1, 1000, Special, Cleanse",
				"Random guy, 1, 1, 1, 1000, Special");

		Card toFar = player.atIndexOnField(0);
		Card left = player.atIndexOnField(1);
		Card self = player.atIndexOnField(2);
		Card right = player.atIndexOnField(3);

		player.consumeCardsField(c -> c.markFrozen());

		Assert.assertTrue(toFar.isFrozen());
		Assert.assertTrue(left.isFrozen());
		Assert.assertTrue(self.isFrozen());
		Assert.assertTrue(right.isFrozen());

		self.onBeforeRound();

		Assert.assertTrue(toFar.isFrozen());
		Assert.assertFalse(left.isFrozen());
		Assert.assertFalse("Should purify self", self.isFrozen());
		Assert.assertFalse(right.isFrozen());
	}
}
