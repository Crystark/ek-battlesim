package crystark.ek.battlesim.ability.addons;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Cards;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

public class CardboundTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess
			.withCardsOnField("Scarab Bound Guy, 1, 0, 1, 1, Special, Cardbound:Reflection 5:Scarab, Resurrection:100");
		Player opponent = PlayerWithAccess.withoutCards();
		Player.salute(player, opponent);

		// Test base state: should not have Reflection
		Card testCard = player.atIndexOnField(0);
		testCard.onTurnStart();
		Assert.assertFalse(testCard.hasReflection());

		// Adding a scarab should allow reflection to be added
		Cards.from("Scarab", player).addToField();
		testCard.onTurnStart();
		Assert.assertTrue(testCard.hasReflection());

		// Kill the card and get back in the field
		testCard.damaged(null, 1, DamageType.unavoidable);
		player.playCardsFromHand();
		Assert.assertFalse(testCard.hasReflection());

		// Scarab is still there so it should trigger again
		testCard.onTurnStart();
		Assert.assertTrue(testCard.hasReflection());
	}

	@Test
	public void test_apply_complex() {
		Player player = PlayerWithAccess
			.withCardsOnField("Scarab and Bastet Bound Guy, 1, 0, 1, 1, Special, Cardbound:Reflection\\:100 & Resistance:Scarab & Bastet, Resurrection:100");
		Player opponent = PlayerWithAccess.withoutCards();
		Player.salute(player, opponent);

		// Test base state: should not have Reflection
		Card testCard = player.atIndexOnField(0);
		testCard.onTurnStart();
		Assert.assertFalse(testCard.hasReflection());

		// Adding a scarab should not be enough
		Cards.from("Scarab", player).addToField();
		testCard.onTurnStart();
		Assert.assertFalse(testCard.hasReflection());

		// Adding a bastet should trigger the skill
		Cards.from("Bastet", player).addToField();
		testCard.onTurnStart();
		Assert.assertTrue(testCard.hasReflection());
		Assert.assertTrue(testCard.hasResistance());
	}
}
