package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class BanishTest {
	@Test
	public void test_apply() {
		PlayerWithAccess player = PlayerWithAccess
			.withCardsOnField("Banish Guy, 1, 1, 1, 1, Special, Banish");

		PlayerWithAccess opponent = PlayerWithAccess
			.withCardsOnField("Summoner Guy, 1, 1, 1, 1000, Special, Summon Dragon, Gang Up!");

		Player.salute(player, opponent);

		Assert.assertEquals(4, opponent.countCardsInField());

		player.playCardsFromField();

		Assert.assertEquals(1, opponent.countCardsInField());
	}
}
