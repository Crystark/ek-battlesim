package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class DivineShieldTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Divine Shield Guy, 1, 1, 1, 1000, Special, Divine Shield");
		Card card = player.atIndexOnField(0);

		Assert.assertEquals(1000, (int) card.getCurrentHp());

		card.damaged(null, 100, DamageType.blood);
		Assert.assertEquals(900, (int) card.getCurrentHp());

		card.damaged(null, 100, DamageType.attack);
		Assert.assertEquals(900, (int) card.getCurrentHp());

		card.damaged(null, 100, DamageType.attack);
		Assert.assertEquals(800, (int) card.getCurrentHp());
	}
}
