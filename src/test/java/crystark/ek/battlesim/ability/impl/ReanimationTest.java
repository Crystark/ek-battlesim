package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Cards;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

public class ReanimationTest {

	@Test
	public void test_apply() {
		PlayerWithAccess player = PlayerWithAccess
			.withCardsOnField("Reanimation Guy, 1, 1, 1, 10, Special, Reanimation")
			.withCardsInCemetery(
				"No Skill Guy, 1, 1, 1, 1, Special",
				"Reanimation Guy, 1, 1, 1, 1, Special, Reanimation");

		Player opponent = PlayerWithAccess
			.withoutCards()
			.withCardsInHand("Soul Imprison Guy, 1, 0, 1, 1, Special, Soul Imprison");

		Player.salute(player, opponent);

		Card card = player.atIndexOnField(0);
		card.applyPowerAbilities(); // Try reanimation
		Assert.assertEquals(1, player.countCardsInField());

		Cards.from("Random Guy, 1, 1, 1, 10, Special, Immunity", player, true).addToCemetery();
		card.applyPowerAbilities(); // Try reanimation

		Assert.assertEquals(2, player.countCardsInField());
		Assert.assertEquals("Random Guy", player.atIndexOnField(1).desc.name);

		Cards.from("Random Guy 2, 1, 1, 1, 1, Special", player, true).addToCemetery();
		opponent.playCardsFromHand(); // Add Soul Imprison Card to field
		card.applyPowerAbilities(); // Try reanimation
		Assert.assertEquals("Reanimation should fail", 2, player.countCardsInField());
	}
}
