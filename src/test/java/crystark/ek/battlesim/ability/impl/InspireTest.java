package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class InspireTest {

	@Test
	public void test_apply() {
		Player player = PlayerWithAccess
			.withCardsOnField(
				"Scarab",
				"Inspired Guy, 1, 1, 1, 1, Special, Inspire:100",
				"Scarab");

		Card testCard = player.atIndexOnField(1);

		Assert.assertEquals(1, testCard.getCurrentAtk());

		testCard.applyBoosts();

		Assert.assertEquals(301, testCard.getCurrentAtk());
	}
}
