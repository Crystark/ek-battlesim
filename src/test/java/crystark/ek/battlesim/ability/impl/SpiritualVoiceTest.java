package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class SpiritualVoiceTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Spiritual Voice Guy, 1, 1, 1, 1, Special, Spiritual Voice");

		Assert.assertTrue(player.atIndexOnField(0).hasSpiritualVoice());
	}
}
