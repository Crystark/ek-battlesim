package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

/**
 * @see SummonDragon
 */
public class SummonDragonTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess
			.withCardsOnField("Summon Dragon Guy, 1, 1, 1, 1, Special, Summon Dragon");

		Assert.assertEquals(2, player.countCardsInField());
	}

	@Test
	public void test_2_abilities() {
		Player player = PlayerWithAccess
			.withCardsOnField("Summon Dragon Guy, 1, 1, 1, 1, Special, Summon Dragon, Summon Dragon");

		Assert.assertEquals(3, player.countCardsInField());
	}
}
