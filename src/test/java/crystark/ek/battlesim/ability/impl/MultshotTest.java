package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

public class MultshotTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Multshot Guy, 10, 2, 600, 1500, Special, Multshot:100:3");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Dark Titan");
		Player.salute(player, opponent);

		Card dsg = player.atIndexOnField(0);
		dsg.applyPowerAbilities();

		Card opponentCard = opponent.pickCardInField();
		Assert.assertEquals(opponentCard.desc.health - 100, (int) opponentCard.getCurrentHp());
	}
}
