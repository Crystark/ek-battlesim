package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

/**
 * @see DyingStrike
 */
public class DyingStrikeTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Dying strike guy, 1, 1, 1, 1, Special, Dying Strike");
		Player opponent = PlayerWithAccess.withCardsOnField("Don't hurt me, 1, 1, 1, 10, Special");
		Player.salute(player, opponent);

		Card ccCard = player.atIndexOnField(0);
		Card opCard = opponent.atIndexOnField(0);

		Assert.assertEquals(10, opCard.getCurrentHp());

		// Kill the card
		ccCard.damaged(null, 1, DamageType.unavoidable);

		Assert.assertEquals(8, opCard.getCurrentHp());
	}
}
