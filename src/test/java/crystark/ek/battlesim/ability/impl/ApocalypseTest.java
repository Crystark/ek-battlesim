package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class ApocalypseTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Apocalypse Guy, 1, 1, 0, 1000, Special, Apocalypse");
		PlayerWithAccess opponent = PlayerWithAccess
			.withCardsOnField(
				"Immune Guy, 1, 1, 1, 1000, Special, Immunity",
				"Reflection Guy, 1, 1, 1, 1000, Special, Reflection:150",
				"Random Guy, 1, 1, 1, 1000, Special");
		Player.salute(player, opponent);

		Card card = player.atIndexOnField(0);
		Card opImmune = opponent.atIndexOnField(0);
		Card opReflect = opponent.atIndexOnField(1);
		Card opRandom = opponent.atIndexOnField(2);

		Assert.assertEquals(1000, (int) card.getCurrentHp());
		Assert.assertEquals(1000, (int) opImmune.getCurrentHp());
		Assert.assertEquals(1000, (int) opReflect.getCurrentHp());
		Assert.assertEquals(1000, (int) opRandom.getCurrentHp());

		player.playCardsFromField();

		// Test the Fire God part
		Assert.assertFalse("Immune card shouldn't be burning", opImmune.isBurning());
		Assert.assertTrue("Reflection card should be burning", opReflect.isBurning());
		Assert.assertTrue("Random card should be burning", opRandom.isBurning());

		// Test the Asura's Flame part
		Assert.assertEquals(1000, (int) opImmune.getCurrentHp());
		Assert.assertEquals(1000, (int) opReflect.getCurrentHp());
		Assert.assertEquals(670, (int) opRandom.getCurrentHp());

	}
}
