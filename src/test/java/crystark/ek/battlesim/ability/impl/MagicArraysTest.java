package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class MagicArraysTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess
			.withCardsOnField(
				"Spell marked guy, 1, 1, 1, 1000, Special",
				"Spell marked guy, 1, 1, 1, 1000, Special");
		Player opponent = PlayerWithAccess.withCardsOnField("Magic Arrays guy, 1, 1, 1, 1000, Special, Magic Arrays:50");
		Player.salute(player, opponent);

		Card c1 = player.atIndexOnField(0);
		Card c2 = player.atIndexOnField(1);

		opponent.atIndexOnField(0).applyPowerAbilities();

		Assert.assertEquals(1000, (int) c1.getCurrentHp());
		Assert.assertEquals(1000, (int) c2.getCurrentHp());

		c1.damaged(null, 200, DamageType.burn);
		c2.damaged(null, 200, DamageType.fire);

		Assert.assertEquals(800, (int) c1.getCurrentHp());
		Assert.assertEquals(700, (int) c2.getCurrentHp());

		c1.damaged(null, 200, DamageType.ice);
		Assert.assertEquals(500, (int) c1.getCurrentHp());
	}
}
