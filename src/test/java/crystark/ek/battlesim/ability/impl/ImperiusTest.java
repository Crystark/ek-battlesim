package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Cards;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

public class ImperiusTest {

	@Test
	public void test_apply() {
		PlayerWithAccess player = PlayerWithAccess
			.withCardsOnField("Imperius Guy, 1, 1, 1, 10, Special, Imperius");

		Player opponent = PlayerWithAccess
			.withoutCards()
			.withCardsInHand("Soul Imprison Guy, 1, 0, 1, 1, Special, Soul Imprison");

		Player.salute(player, opponent);

		// Make sure Imperius doesn't crash on an empty cemetery
		Card card = player.atIndexOnField(0);
		card.applyPowerAbilities(); // Try imperius

		Assert.assertEquals(1, player.countCardsInField());

		// Test imperius getting the right card
		Card immunity = Cards.from("Immunity Guy, 1, 1, 1, 1, Special, Immunity", opponent, true).addToCemetery();
		Cards.from("Reanimation Guy, 1, 1, 1, 1, Special, Reanimation", opponent, true).addToCemetery();

		Assert.assertEquals(2, opponent.countCardsInCemetery());
		Assert.assertEquals(1, player.countCardsInField());

		card.applyPowerAbilities(); // Try imperius

		Assert.assertEquals(1, opponent.countCardsInCemetery());
		Assert.assertEquals(2, player.countCardsInField());
		Assert.assertEquals(player, immunity.owner());
		Assert.assertEquals("Immunity Guy", player.atIndexOnField(1).desc.name);

		// Test imperius blocked by Soul Imprison
		Cards.from("Random Guy 2, 1, 1, 1, 1, Special", player, true).addToCemetery();
		opponent.playCardsFromHand(); // Add Soul Imprison Card to field
		card.applyPowerAbilities(); // Try imperius

		Assert.assertEquals("Imperius should fail", 2, player.countCardsInField());

		// Make sure the card goes back to the opponent cemetery
		int baseCount = opponent.countCardsInCemetery();
		immunity.damaged(null, 1, DamageType.unavoidable);
		Assert.assertEquals(baseCount + 1, opponent.countCardsInCemetery());
		Assert.assertEquals(opponent, immunity.owner());
	}

	@Test
	public void test_with_exile() {
		PlayerWithAccess player = PlayerWithAccess
			.withCardsOnField("Imperius Guy, 1, 1, 0, 1, Special, Imperius");

		Player opponent = PlayerWithAccess
			.withCardsOnField(
				"Random Guy 1, 1, 1, 1, 1, Special",
				"Exile Guy 1, 1, 1, 1, 1000, Special, Exile")
			.withCardsInCemetery("Resurrection Guy, 1, 1, 1, 1, Special, Resurrection:100");

		Player.salute(player, opponent);

		Card card = player.atIndexOnField(0);
		Card random = opponent.pickCardInCemetery();
		card.applyPowerAbilities(); // Make opponent card ours

		Assert.assertEquals(2, player.countCardsInField()); // make sure it worked
		Assert.assertEquals(player, random.owner());

		// Activate exile
		opponent.atIndexOnField(1).applyPowerAbilities();

		Assert.assertEquals(1, player.countCardsInField()); // make sure it worked
		Assert.assertEquals(opponent, random.owner());
		Assert.assertEquals(random, opponent.pickLastHighestTimerCardInDeck(false)); // just using what's available here
	}

	@Test
	public void test_with_resurrection() {
		PlayerWithAccess player = PlayerWithAccess
			.withCardsOnField("Imperius Guy, 1, 1, 0, 1, Special, Imperius");

		Player opponent = PlayerWithAccess
			.withCardsOnField(
				"Random Guy 1, 1, 1, 1, 1, Special",
				"Exile Guy 1, 1, 1, 1, 1000, Special, Exile")
			.withCardsInCemetery("Resurrection Guy, 1, 1, 1, 1, Special, Resurrection:100");

		Player.salute(player, opponent);

		Card card = player.atIndexOnField(0);
		Card random = opponent.pickCardInCemetery();
		card.applyPowerAbilities(); // Make opponent card ours

		Assert.assertEquals(2, player.countCardsInField()); // make sure it worked
		Assert.assertEquals(player, random.owner());

		// Activate exile
		random.damaged(null, 1, DamageType.unavoidable);

		Assert.assertEquals(1, player.countCardsInField()); // make sure it worked
		Assert.assertEquals(opponent, random.owner());
		Assert.assertEquals(random, opponent.pickFirstHighestTimerCardInHand(false)); // just using what's available here
	}
}
