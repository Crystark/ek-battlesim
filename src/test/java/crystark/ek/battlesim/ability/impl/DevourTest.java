package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class DevourTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Devourer, 1, 1, 1000, 2000, Special, Devour:50");
		PlayerWithAccess opponent = PlayerWithAccess
			.withCardsOnField(
				"Immune Guy, 1, 1, 1, 1000, Special, Immunity",
				"Random Guy, 1, 1, 1, 1000, Special");
		Player.salute(player, opponent);
		Card devourCard = player.atIndexOnField(0);

		Assert.assertEquals(1000, devourCard.getCurrentBaseAtk());
		Assert.assertEquals(2000, devourCard.getCurrentBaseHp());

		player.playCardsFromField();

		Assert.assertEquals(1500, devourCard.getCurrentBaseAtk());
		Assert.assertEquals(3000, devourCard.getCurrentBaseHp());

		opponent.cleanup();
		player.playCardsFromField();

		Assert.assertEquals(2250, devourCard.getCurrentBaseAtk());
		Assert.assertEquals(4500, devourCard.getCurrentBaseHp());

		opponent.cleanup();
		player.playCardsFromField();
		// no more card to attack
		Assert.assertEquals(2250, devourCard.getCurrentBaseAtk());
		Assert.assertEquals(4500, devourCard.getCurrentBaseHp());
	}
}
