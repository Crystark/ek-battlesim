package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class FieldOfSilenceTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Silencer, 1, 1, 0, 1, Special, Field of Silence");
		PlayerWithAccess opponent = PlayerWithAccess
			.withCardsOnField(
				"Immune Guy, 1, 1, 1, 1000, Special, Immunity",
				"Random Guy, 1, 1, 1, 1000, Special",
				"Random Guy, 1, 1, 1, 1000, Special");
		Player.salute(player, opponent);

		player.playCardsFromField();

		Assert.assertTrue(opponent.atIndexOnField(0).isSilenced());
		Assert.assertTrue(opponent.atIndexOnField(1).isSilenced());
		Assert.assertTrue(opponent.atIndexOnField(2).isSilenced());
	}
}
