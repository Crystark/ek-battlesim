package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

public class ShadowTest {

	@Test
	public void summonedCardDoesntSummons() {
		Player player = PlayerWithAccess
			.withCardsOnField("X Ninja, 1, 1, 1, 1, Special, D_Shadow:[self]");
		Player opponent = PlayerWithAccess
			.withCardsOnField("Random Gay, 1, 1, 10000, 1, Special");

		Player.salute(player, opponent);
		opponent.playCardsFromField();

		player.cleanup();
		Assert.assertEquals(1, player.countCardsInField());
		Card clone = player.atIndexOnField(0);
		Assert.assertTrue(clone.isSummoned());

		opponent.playCardsFromField();
		player.cleanup();
		Assert.assertEquals("Summoned card must not summon",0, player.countCardsInField());
	}
}
