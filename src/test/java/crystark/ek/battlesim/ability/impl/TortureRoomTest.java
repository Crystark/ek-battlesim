package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class TortureRoomTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Tormentor, 1, 1, 0, 1, Special, Torture Room");
		PlayerWithAccess opponent = PlayerWithAccess
			.withCardsOnField(
				"Immune Guy, 1, 1, 1, 1000, Special, Immunity",
				"Random Guy, 1, 1, 1, 1000, Special",
				"Random Guy, 1, 1, 1, 1000, Special");
		Player.salute(player, opponent);

		player.playCardsFromField();

		Assert.assertTrue(opponent.atIndexOnField(0).isLacerated());
		Assert.assertTrue(opponent.atIndexOnField(1).isLacerated());
		Assert.assertTrue(opponent.atIndexOnField(2).isLacerated());
	}
}
