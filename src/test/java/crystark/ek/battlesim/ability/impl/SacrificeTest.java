package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class SacrificeTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess
			.withCardsOnField(
				"Sacrificed Guy, 1, 1, 1, 1, Special, Origins Guard:100, Power Source:100",
				"Sacrificed Guy, 1, 1, 1, 1, Special, Origins Guard:100, Power Source:100",
				"Sacrificing Guy, 1, 1, 1000, 1000, Special, Sacrifice:100");

		Card buffingCard = player.atIndexOnField(0);
		Card sacrificingCard = player.atIndexOnField(1);

		Assert.assertEquals(2200, sacrificingCard.getCurrentBaseAtk());
		Assert.assertEquals(2200, sacrificingCard.getCurrentBaseHp());

		player.destroyCard(buffingCard);

		Assert.assertEquals(2100, sacrificingCard.getCurrentBaseAtk());
		Assert.assertEquals(2100, sacrificingCard.getCurrentBaseHp());
	}
}
