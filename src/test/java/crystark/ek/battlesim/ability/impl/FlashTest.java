package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class FlashTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Flash Grenade, 1, 1, 0, 1, Special, Flash:100");
		PlayerWithAccess opponent = PlayerWithAccess
			.withCardsOnField(
				"Immune Guy, 1, 1, 1, 1000, Special, Immunity",
				"Random Guy, 1, 1, 1, 1000, Special",
				"Random Guy, 1, 1, 1, 1000, Special");
		Player.salute(player, opponent);

		player.playCardsFromField();

		Assert.assertFalse(opponent.atIndexOnField(0).isBlind());
		Assert.assertTrue(opponent.atIndexOnField(1).isBlind());
		Assert.assertTrue(opponent.atIndexOnField(2).isBlind());
	}
}
