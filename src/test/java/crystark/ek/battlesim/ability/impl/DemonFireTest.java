package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

public class DemonFireTest {
	@Test
	public void test_apply() {

		/**
		 * Deal random 1 enemy card 500-1000 damage, the enemy will lose 500 HP every time after its action.
		 */
		//Cerberus | Deal 120 damage to 3 random enemy cards.
		// If they have Immunity or Reflecting, deal 300% damage.
		// Reduce 30% physical when receiving physcal attack.

		Player player = PlayerWithAccess.withCardsOnField("Cerberus Guy, 1, 1, 0, 2000, Special, Demon Fire");
		PlayerWithAccess opponent = PlayerWithAccess
			.withCardsOnField(
				"Random Guy1, 1, 1, 1, 2000, Special",
				"Random Guy2, 1, 1, 1, 2000, Special");
		Player.salute(player, opponent);

		Card card = player.atIndexOnField(0);
		Card opRandom1 = opponent.atIndexOnField(0);
		Card opRandom2 = opponent.atIndexOnField(1);

		Assert.assertEquals(2_000L, card.getCurrentHp());
		Assert.assertEquals(2_000L, opRandom1.getCurrentHp());
		Assert.assertEquals(2_000L, opRandom2.getCurrentHp());

		// Test the Burn part
		player.playCardsFromField();
		Assert.assertTrue(opRandom1.getCurrentHp() < 2_000L ^ opRandom2.getCurrentHp() < 2_000L);

		Assert.assertTrue(opRandom1.isBurning() ^ opRandom2.isBurning());
	}
}
