package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class ElectricShockTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Electric Shock Guy, 1, 1, 1, 1000, Special, Electric Shock:100");
		PlayerWithAccess opponent = PlayerWithAccess
			.withCardsOnField(
				"Immune Guy, 1, 1, 1, 1000, Special, Immunity",
				"Reflection Guy, 1, 1, 1, 1000, Special, Reflection:150",
				"Immune-with-reflection Guy, 1, 1, 1, 1000, Special, Immunity, Reflection:150",
				"Random Guy, 1, 1, 1, 1000, Special");
		Player.salute(player, opponent);

		Card card = player.atIndexOnField(0);
		card.getPowerAbilities().get(0).apply(card);

		Assert.assertEquals(1000, (int) opponent.atIndexOnField(0).getCurrentHp());
		Assert.assertEquals(1000, (int) opponent.atIndexOnField(1).getCurrentHp());
		Assert.assertEquals(1000, (int) opponent.atIndexOnField(2).getCurrentHp());
		Assert.assertEquals(900, (int) opponent.atIndexOnField(3).getCurrentHp());

		Assert.assertEquals(700, (int) card.getCurrentHp()); // 300 lost from both Reflection
	}
}
