package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class WaterShieldTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Water Shield Guy, 1, 1, 1, 10000, Special, Water Shield:200:1000");

		Card card = player.atIndexOnField(0);
		player.damaged(3000, false);
		Assert.assertEquals(10000, (int) card.getCurrentHp());
		Assert.assertEquals(player.desc.health - 3000, (int) player.getCurrentHp());

		card.damaged(null, 2000, DamageType.unavoidable);
		Assert.assertEquals(8000, (int) card.getCurrentHp());
		Assert.assertEquals(player.desc.health - 3000, (int) player.getCurrentHp());

		card.damaged(null, 2000, DamageType.attack);
		Assert.assertEquals("Should only take 200 damage", 7800, (int) card.getCurrentHp());
		Assert.assertEquals("Should heal 1000 HP", player.desc.health - 2000, (int) player.getCurrentHp());

		card.damaged(null, 800, DamageType.attack);
		Assert.assertEquals("Should only take 200 damage", 7600, (int) card.getCurrentHp());
		Assert.assertEquals("Should heal 600 HP", player.desc.health - 1400, (int) player.getCurrentHp());

		card.damaged(null, 100, DamageType.attack);
		Assert.assertEquals("Should only take 100 damage", 7500, (int) card.getCurrentHp());
		Assert.assertEquals("Should not be healed more", player.desc.health - 1400, (int) player.getCurrentHp());
	}
}
