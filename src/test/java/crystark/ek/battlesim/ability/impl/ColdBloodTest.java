package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class ColdBloodTest {

	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Cold Blood Guy, 1, 1, 100, 1, Special, Cold Blood");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Random Guy, 1, 1, 1, 3000, Special");
		Player.salute(player, opponent);

		Card opponentCard = opponent.atIndexOnField(0);

		Assert.assertEquals(3000, opponentCard.getCurrentHp());
		Assert.assertEquals(3000, opponentCard.getCurrentBaseHp());

		player.playCardsFromField();
		Assert.assertEquals(2900, opponentCard.getCurrentHp());
		Assert.assertEquals(2900, opponentCard.getCurrentBaseHp());

		opponentCard.damaged(null, 100, DamageType.attack);
		Assert.assertEquals(2800, opponentCard.getCurrentHp());
		Assert.assertEquals(2900, opponentCard.getCurrentBaseHp());
	}
}
