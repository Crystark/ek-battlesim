package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class ManaBreakTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Mana Break Guy, 0, 0, 0, 1, Special, Mana Break 8");
		PlayerWithAccess opponent = PlayerWithAccess
			.withCardsOnField(
				"Immune Guy, 1, 1, 1, 10000, Special, Immunity",
				"Reflection Guy, 1, 1, 1, 10000, Special, Reflection:150",
				"Random Guy, 1, 1, 1, 10000, Special");
		Player.salute(player, opponent);

		player.atIndexOnField(0).applyPowerAbilities();

		Assert.assertEquals(8400, (int) opponent.atIndexOnField(0).getCurrentHp());
		Assert.assertEquals(8400, (int) opponent.atIndexOnField(1).getCurrentHp());
		Assert.assertEquals(9840, (int) opponent.atIndexOnField(2).getCurrentHp());
	}
}
