package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

public class ResurrectionTest {

	@Test
	public void test_apply() {
		PlayerWithAccess player = PlayerWithAccess
			.withCardsOnField("Resurection Guy, 1, 0, 1, 1, Special, Resurrection:100");
		PlayerWithAccess opponent = PlayerWithAccess
			.withoutCards()
			.withCardsInHand("Suppressing Guy, 1, 0, 1, 1, Special, Suppressing");

		Player.salute(player, opponent);

		Assert.assertEquals(0, player.countCardsInHand());

		Card card = player.atIndexOnField(0);
		card.damaged(null, 1, DamageType.unavoidable);
		Assert.assertEquals(1, player.countCardsInHand());

		player.playCardsFromHand(); // But the resurrected card back in the field
		opponent.playCardsFromHand(); // Add the suppressing card on the field
		card.damaged(null, 1, DamageType.unavoidable);
		Assert.assertEquals("Resurrection shouldn't have worked", 1, player.countCardsInCemetery());
	}
}
