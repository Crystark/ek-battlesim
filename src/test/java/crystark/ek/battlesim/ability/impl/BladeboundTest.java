package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class BladeboundTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess
			.withCardsOnField("Bladebound Guy, 0, 0, 0, 1, Special, Bladebound")
			.withCardsInHand("Infernal Blade, 0, 0, 0, 1, Special");
		PlayerWithAccess opponent = PlayerWithAccess
			.withCardsOnField("Random Guy, 1, 1, 1, 10000, Special");
		Player.salute(player, opponent);

		player.atIndexOnField(0).onTurnStart();
		player.atIndexOnField(0).applyPowerAbilities();
		Assert.assertEquals(10000, (int) opponent.atIndexOnField(0).getCurrentHp());

		player.playCardsFromHand();
		Assert.assertEquals(2, player.countCardsInField());
		player.atIndexOnField(0).onTurnStart();
		player.atIndexOnField(0).applyPowerAbilities();
		Assert.assertEquals(8000, (int) opponent.atIndexOnField(0).getCurrentHp());

		player.atIndexOnField(1).damaged(null, 1, DamageType.unavoidable);
		Assert.assertEquals(1, player.countCardsInField());
		player.atIndexOnField(0).onTurnStart();
		player.atIndexOnField(0).applyPowerAbilities();
		Assert.assertEquals(6000, (int) opponent.atIndexOnField(0).getCurrentHp());
	}
}
