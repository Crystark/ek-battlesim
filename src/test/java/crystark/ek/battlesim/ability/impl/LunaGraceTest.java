package crystark.ek.battlesim.ability.impl;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import crystark.common.api.Game;
import crystark.ek.EkbsConfig;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

/**
 *
 */
public class LunaGraceTest {
	Game game;

	@Before
	public void before() {
		game = EkbsConfig.current().game();
		EkbsConfig.current().switchGame(Game.hf);
	}

	@After
	public void after() {
		EkbsConfig.current().switchGame(game);
	}


	@Test
	public void test_apply() {

		Player player = PlayerWithAccess.withCardsOnField(
			"Random Guy, 10, 0, 0, 1000, Special",
			"Luna Grace Guy, 10, 0, 0, 1000, Special, Luna Grace"
		);

		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField(
			"Random Guy, 10, 0, 200, 1000, Special",
			"Random Guy, 10, 0, 500, 1000, Special, Double Attack"
		);
		Player.salute(player, opponent);


		final Card graceCard1 = player.atIndexOnField(0);
		final Card graceCard2 = player.atIndexOnField(1);

		player.removeBuffs();
		player.playCardsFromField();
		opponent.playCardsFromField();
		Assert.assertEquals(1000, graceCard1.getCurrentHp());
		Assert.assertEquals(500, graceCard2.getCurrentHp());

		player.removeBuffs();
		player.playCardsFromField();
		opponent.playCardsFromField();
		Assert.assertEquals(1000, graceCard1.getCurrentHp());
		Assert.assertEquals(0, graceCard2.getCurrentHp());
	}

	@Test
	public void testAffectsNextRound() {

		Player player = PlayerWithAccess.withCardsOnField(
			"Random Guy, 10, 0, 0, 1000, Special",
			"Luna Grace Guy, 10, 0, 0, 500, Special, Luna Grace"
		);

		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField(
			"Random Guy, 10, 0, 200, 1000, Special, Snipe:500"
		);
		Player.salute(player, opponent);


		final Card graceCard1 = player.atIndexOnField(0);
		final Card graceCard2 = player.atIndexOnField(1);

		player.removeBuffs();
		player.playCardsFromField();
		opponent.playCardsFromField();
		Assert.assertEquals("Snipe has to kills Luna Grace provider card",0, graceCard2.getCurrentHp());
		Assert.assertEquals("A card before Luna Grace has to avoid physic attack",1000, graceCard1.getCurrentHp());

		player.removeBuffs();
		player.playCardsFromField();
		opponent.playCardsFromField();
		Assert.assertEquals(300, graceCard1.getCurrentHp());
	}

	@Test
	public void test_Silence() {

		Player player = PlayerWithAccess.withoutCards()
			.withCardsInHand(
				"Luna Grace Guy, 10, 0, 0, 1000, Special, Luna Grace"
			);

		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField(
			"Random Guy, 10, 0, 1000, 1000, Special, Silence"
		);
		Player.salute(player, opponent);
		//player.playCardsFromHand();

		final Card oppositeCard = opponent.atIndexOnField(0);

		player.playCardsFromHand();
		final Card graceCard = player.atIndexOnField(0);
		player.playCardsFromField();

		opponent.playCardsFromField();
		Assert.assertEquals(0, graceCard.getCurrentHp());

	}
}
