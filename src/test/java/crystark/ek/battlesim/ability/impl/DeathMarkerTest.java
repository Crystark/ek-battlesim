package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

/**
 * @see DeathMarker
 */
public class DeathMarkerTest {

	@Test
	public void test_create() {
		DeathMarker ab = new DeathMarker(100);

		Assert.assertEquals(Integer.valueOf(100), ab.getValue());
	}

	@Test
	public void test_deathMark() {
		DeathMarker ab = new DeathMarker(100);
		Player player = PlayerWithAccess
			.withCardsOnField(
				"Guy to the left, 1, 2, 1, 1500, Special",
				"Death marked guy, 10, 2, 600, 1500, Special",
				"Guy to the right, 1, 2, 1, 1500, Special");

		Card left = player.atIndexOnField(0);
		Card deathMarked = player.atIndexOnField(1);
		Card right = player.atIndexOnField(2);

		// Mark the card
		ab.deathMarker(deathMarked);

		Assert.assertEquals(1500, left.getCurrentHp());
		Assert.assertEquals(1500, right.getCurrentHp());

		// Kill the card
		deathMarked.markSilenced(); // even if silenced it should work
		deathMarked.damaged(null, 1500, DamageType.unavoidable);

		Assert.assertEquals(1400, left.getCurrentHp());
		Assert.assertEquals(1400, right.getCurrentHp());
	}
}
