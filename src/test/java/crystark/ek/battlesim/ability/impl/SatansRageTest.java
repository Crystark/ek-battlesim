package crystark.ek.battlesim.ability.impl;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.ability.impl.alias.Hellfire;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class SatansRageTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Satan's Rage Guy, 1, 1, 0, 1000, Special, Satan's Rage 10");

		Card card = player.atIndexOnField(0);

		Assert.assertThat(card.getPowerAbilities().get(0), Matchers.instanceOf(Hellfire.class));
		Assert.assertThat(card.getPowerAbilities().get(1), Matchers.instanceOf(Firestorm.class));
	}
}
