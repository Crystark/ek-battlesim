package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Cards;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class ManaPunctureTest {
	@Test
	public void test_apply_10() {
		Player player = PlayerWithAccess.withCardsOnField("Plague Ogryn");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Dark Titan");
		Player.salute(player, opponent);

		Card mcg = Cards.from("Mana Puncture Guy, 10, 2, 600, 1500, Special, Mana Puncture 10", player);
		ManaPuncture mc = (ManaPuncture) mcg.getPowerAbilities().get(0);
		mc.apply(mcg);

		Card opponentCard = opponent.pickCardInField();
		Assert.assertEquals(2000, (int) opponentCard.desc.health - opponentCard.getCurrentHp());
	}

	@Test
	public void test_apply_11() {
		Player player = PlayerWithAccess.withCardsOnField("Plague Ogryn");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Dark Titan");
		Player.salute(player, opponent);

		Card mcg = Cards.from("Mana Puncture Guy, 10, 2, 600, 1500, Special, Mana Puncture 11", player);
		ManaPuncture mc = (ManaPuncture) mcg.getPowerAbilities().get(0);
		mc.apply(mcg);

		Card opponentCard = opponent.pickCardInField();
		Assert.assertEquals(5000, (int) opponentCard.desc.health - opponentCard.getCurrentHp());
	}

	@Test
	public void test_apply_custom() {
		Player player = PlayerWithAccess.withCardsOnField("Plague Ogryn");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Dark Titan");
		Player.salute(player, opponent);

		Card mcg = Cards.from("Mana Puncture Guy, 10, 2, 600, 1500, Special, Mana Puncture:160:1000", player);
		ManaPuncture mc = (ManaPuncture) mcg.getPowerAbilities().get(0);
		mc.apply(mcg);

		Card opponentCard = opponent.pickCardInField();
		Assert.assertEquals(1600, (int) opponentCard.desc.health - opponentCard.getCurrentHp());
	}
}
