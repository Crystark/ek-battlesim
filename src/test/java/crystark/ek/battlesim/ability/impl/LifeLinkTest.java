package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class LifeLinkTest {
	@Test
	public void test_apply_full() {
		Player player = PlayerWithAccess.withCardsOnField("Life Link Guy, 1, 1, 0, 1000, Special, Life Link");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Random Guy, 1, 1, 300, 1, Special");
		Player.salute(player, opponent);

		Card card = player.atIndexOnField(0);

		Assert.assertEquals(1000, card.getCurrentHp());

		opponent.playCardsFromField();
		Assert.assertEquals(700, card.getCurrentHp());
	}

	@Test
	public void test_apply_split2() {
		Player player = PlayerWithAccess
			.withCardsOnField(
				"Life Link Guy, 1, 1, 0, 1000, Special, Life Link",
				"Random Guy, 1, 1, 0, 1000, Special");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Random Guy, 1, 1, 300, 1, Special");
		Player.salute(player, opponent);

		Card card = player.atIndexOnField(0);
		Card right = player.atIndexOnField(1);

		Assert.assertEquals(1000, card.getCurrentHp());
		Assert.assertEquals(1000, right.getCurrentHp());

		opponent.playCardsFromField();
		Assert.assertEquals(850, card.getCurrentHp());
		Assert.assertEquals(850, right.getCurrentHp());
	}

	@Test
	public void test_apply_split3() {
		Player player = PlayerWithAccess
			.withCardsOnField(
				"Random Guy, 1, 1, 0, 1000, Special",
				"Life Link Guy, 1, 1, 0, 1000, Special, Life Link",
				"Random Guy, 1, 1, 0, 1000, Special",
				"Random Guy, 1, 1, 0, 1000, Special");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField(
			"Place Holder Guy, 1, 1, 0, 1, Special",
			"Random Guy, 1, 1, 300, 1, Special");
		Player.salute(player, opponent);

		Card left = player.atIndexOnField(0);
		Card card = player.atIndexOnField(1);
		Card right = player.atIndexOnField(2);
		Card outOfRange = player.atIndexOnField(3);

		Assert.assertEquals(1000, left.getCurrentHp());
		Assert.assertEquals(1000, card.getCurrentHp());
		Assert.assertEquals(1000, right.getCurrentHp());
		Assert.assertEquals(1000, outOfRange.getCurrentHp());

		opponent.playCardsFromField();
		Assert.assertEquals(900, left.getCurrentHp());
		Assert.assertEquals(900, card.getCurrentHp());
		Assert.assertEquals(900, right.getCurrentHp());
		Assert.assertEquals(1000, outOfRange.getCurrentHp());
	}

	@Test
	public void test_apply_split3_partial() {
		Player player = PlayerWithAccess
			.withCardsOnField(
				"Random Guy, 1, 1, 0, 200, Special",
				"Life Link Guy, 1, 1, 0, 1000, Special, Life Link",
				"Random Guy, 1, 1, 0, 1000, Special");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField(
			"Place Holder Guy, 1, 1, 0, 1, Special",
			"Random Guy, 1, 1, 1500, 1, Special");
		Player.salute(player, opponent);

		Card left = player.atIndexOnField(0);
		Card card = player.atIndexOnField(1);
		Card right = player.atIndexOnField(2);

		Assert.assertTrue(left.isAlive());
		Assert.assertEquals(1000, card.getCurrentHp());
		Assert.assertEquals(1000, right.getCurrentHp());

		opponent.playCardsFromField();
		Assert.assertFalse(left.isAlive());
		Assert.assertEquals(200, card.getCurrentHp());
		Assert.assertEquals(500, right.getCurrentHp());
	}
}
