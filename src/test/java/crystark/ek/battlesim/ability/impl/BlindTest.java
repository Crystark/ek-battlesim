package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class BlindTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Blinding Guy, 1, 1, 0, 1000, Special, Blind:100");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Random Guy, 1, 1, 1, 1000, Special");
		Player.salute(player, opponent);

		player.playCardsFromField();
		Assert.assertTrue(opponent.atIndexOnField(0).isBlind());
	}
}
