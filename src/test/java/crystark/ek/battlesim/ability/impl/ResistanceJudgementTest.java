package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

/**
 * Resistance Judgement | Resistance-less & Mana Burn 10    | Card owner and opponent both have 3 or more cards with 'Resistance' on battlefield
 * Resistance-less = Teleport all enemy cards with skill of Resistance back to the enemy's deck.
 */
public class ResistanceJudgementTest {

	@Test
	public void testExileResistance() {
		Player player = PlayerWithAccess
			.withCardsOnField("ResistanceLess Guy, 0, 0, 0, 1, Special, Resistance Judgement");

		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField(
			"Resistance Guy1, 0, 0, 1, 2000, Special, Resistance",
			"NonResistance Guy1, 0, 0, 1, 3000, Special",
			"Resistance Guy2, 0, 0, 1, 2000, Special, Resistance",
			"Resistance Guy3, 0, 0, 1, 2000, Special, Resistance"
			);
		Player.salute(player, opponent);

		Card opponentCard = opponent.atIndexOnField(0);
		Card nonResistCard = opponent.atIndexOnField(1);
		Assert.assertTrue(opponentCard.isAlive());
		Assert.assertTrue(nonResistCard.isAlive());


		player.playCardsFromField();
		Assert.assertEquals(1, opponent.countCardsInField());
		Assert.assertEquals(3, opponent.countCardsInDeck());
		Assert.assertFalse(opponentCard.isAlive());
		Assert.assertTrue(nonResistCard.isAlive());
		//Mana Burn dmg: 3000-200=2800
		Assert.assertEquals(2800, nonResistCard.getCurrentHp());

		player.cleanup();
		opponent.cleanup();
		player.playCardsFromField();
		nonResistCard = opponent.atIndexOnField(0);
		//2800-200-1=2600
		Assert.assertEquals(2600, nonResistCard.getCurrentHp());
	}

	@Test
	public void testExileResistanceDoNotTriggeredWith2ResistCards() {
		Player player = PlayerWithAccess
			.withCardsOnField("ResistanceLess Guy, 0, 0, 0, 1, Special, Resistance Judgement");

		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField(
			"Resistance Guy1, 0, 0, 1, 2000, Special, Resistance",
			"NonResistance Guy1, 0, 0, 1, 3000, Special",
			"Resistance Guy2, 0, 0, 1, 2000, Special, Resistance"
		);
		Player.salute(player, opponent);

		Card opponentCard = opponent.atIndexOnField(0);
		Card nonResistCard = opponent.atIndexOnField(1);
		Assert.assertTrue(opponentCard.isAlive());
		Assert.assertTrue(nonResistCard.isAlive());


		player.playCardsFromField();
		Assert.assertEquals(3, opponent.countCardsInField());
		Assert.assertEquals(0, opponent.countCardsInDeck());
		Assert.assertTrue(opponentCard.isAlive());
		Assert.assertTrue(nonResistCard.isAlive());
		//Mana Burn dmg: 3000-200=2800
		Assert.assertEquals(2800, nonResistCard.getCurrentHp());
	}

	@Test
	public void testManaBurn10() {
		Player player = PlayerWithAccess.withCardsOnField("Mana Burn Guy, 10, 2, 0, 1500, Special, Resistance Judgement");
		PlayerWithAccess opponent = PlayerWithAccess
			.withCardsOnField(
				"Immune Guy, 1, 1, 1, 1000, Special, Immunity",
				"Reflection Guy, 1, 1, 1, 1000, Special, Reflection:150",
				"Random Guy, 1, 1, 1, 1000, Special");
		Player.salute(player, opponent);

		player.playCardsFromField();

		Assert.assertEquals(400, (int) opponent.atIndexOnField(0).getCurrentHp());
		Assert.assertEquals(400, (int) opponent.atIndexOnField(1).getCurrentHp());
		Assert.assertEquals(800, (int) opponent.atIndexOnField(2).getCurrentHp());
	}


/*	@Test
	public void testTmp() {

		Player player = PlayerWithAccess.withCardsOnField("Dhul-Kifl");
		Player opponent = PlayerWithAccess.withCardsOnField("Dhul-Kifl");

		DeckDesc playerDeckDesc = player.snapshotHpAndCards();
		DeckDesc oppDeckDesc = opponent.snapshotHpAndCards();
		Battle.run(playerDeckDesc, oppDeckDesc);
	}*/
}
