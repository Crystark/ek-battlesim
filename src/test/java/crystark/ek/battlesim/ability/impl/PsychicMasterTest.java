package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.PlayerWithAccess;

public class PsychicMasterTest {
	@Test
	public void test_apply() {
		PlayerWithAccess player = PlayerWithAccess
			.withCardsOnField("The Psychic Master, 1, 1, 1, 1, Special, Psychic Master")
			.withCardsInDeck(
				"T2 Guy,   1, 2, 1, 1, Special",
				"T6 Guy 1, 1, 6, 1, 1, Special",
				"T4 Guy,   1, 4, 1, 1, Special",
				"T6 Guy 2, 1, 6, 1, 1, Special",
				"T4 Guy,   1, 4, 1, 1, Special");

		Assert.assertEquals(5, player.countCardsInDeck());

		player.atIndexOnField(0).applyPowerAbilities();
		Assert.assertEquals("T6 Guy 2", player.atIndexOnField(1).desc.name);
		Assert.assertEquals(4, player.countCardsInDeck());
	}

	@Test
	public void test_qs_alias() {
		PlayerWithAccess player = PlayerWithAccess
			.withCardsOnField("The Psychic Master, 1, 1, 1, 1, Special, Conjurer")
			.withCardsInDeck(
				"T2 Guy,   1, 2, 1, 1, Special",
				"T6 Guy 1, 1, 6, 1, 1, Special",
				"T4 Guy,   1, 4, 1, 1, Special",
				"T6 Guy 2, 1, 6, 1, 1, Special",
				"T4 Guy,   1, 4, 1, 1, Special");

		Assert.assertEquals(5, player.countCardsInDeck());

		player.atIndexOnField(0).applyQuickStrikes();
		Assert.assertEquals("T6 Guy 2", player.atIndexOnField(1).desc.name);
		Assert.assertEquals(4, player.countCardsInDeck());
	}
}
