package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

public class GuardTest {
	@Test
	public void test_apply() {
		//# Name, Cost, Timer, Attack, Health, Type, ability_1[:value], ..., ability_N[:value]
		Player player = PlayerWithAccess.withCardsOnField("Guard Guy, 0, 0, 0, 100, Special, Guard",
			"Guard Guy, 0, 0, 0, 100, Special, Guard");
		int baseHealth = player.desc.health;
		player.damaged(199, false);
		Assert.assertEquals(baseHealth, player.getCurrentHp());
		Card card1 = player.atIndexOnField(0);
		Assert.assertNull(card1);
		Card card2 = player.atIndexOnField(1);
		Assert.assertNotNull(card2);
		Assert.assertEquals(1, card2.getCurrentHp());

	}
}
