package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

public class SacredSpringTest {

	@Test
	public void sacredSpringIsSilenced() {
		Player player = PlayerWithAccess
			.withCardsOnField("Sacred Spring, 0, 0, 0, 1000, Special, Sacred Spring");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField(
			"Silence Guy, 0, 0, 1, 2000, Special, Silence"
			);
		Player.salute(player, opponent);

		Card sacredSpringCard = player.atIndexOnField(0);
		Card silenceCard = opponent.atIndexOnField(0);

		sacredSpringCard.markSilenced();
		sacredSpringCard.damaged(silenceCard,200, DamageType.attack);
		Assert.assertEquals(800, sacredSpringCard.getCurrentHp());

		player.playCardsFromField();
		Assert.assertEquals(800, sacredSpringCard.getCurrentHp());
	}
}
