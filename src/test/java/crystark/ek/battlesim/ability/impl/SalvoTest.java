package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class SalvoTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Salvo Guy, 1, 1, 1, 1, Special, Salvo:100");
		PlayerWithAccess opponent = PlayerWithAccess
			.withCardsOnField(
				"Immune Guy, 1, 1, 1, 1000, Special, Immunity",
				"Reflection Guy, 1, 1, 1, 1000, Special, Reflection:150",
				"Random Guy, 1, 1, 1, 1000, Special");
		Player.salute(player, opponent);

		player.atIndexOnField(0).applyPowerAbilities();

		Assert.assertEquals(900, (int) opponent.atIndexOnField(0).getCurrentHp());
		Assert.assertEquals(900, (int) opponent.atIndexOnField(1).getCurrentHp());
		Assert.assertEquals(900, (int) opponent.atIndexOnField(2).getCurrentHp());
	}
}
