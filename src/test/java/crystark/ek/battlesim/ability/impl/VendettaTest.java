package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

/**
 * @see Vendetta
 */
public class VendettaTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess
			.withCardsOnField("Vendetta Guy, 1, 1, 1, 1, Special, Vendetta:100")
			.withCardsInCemetery("scarab", "scarab", "scarab");

		Card testCard = player.atIndexOnField(0);

		Assert.assertEquals(1, testCard.getCurrentAtk());

		Vendetta ab = (Vendetta) testCard.getBoostAbilities().get(0);
		ab.onBeforeAttack(testCard);

		Assert.assertEquals(301, testCard.getCurrentAtk());

	}
}
