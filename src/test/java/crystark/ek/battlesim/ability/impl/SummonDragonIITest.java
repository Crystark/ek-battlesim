package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class SummonDragonIITest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess
			.withCardsOnField("Summon Dragon II Guy, 1, 1, 1, 1, Special, Summon Dragon II");

		Assert.assertEquals(2, player.countCardsInField());
	}
}
