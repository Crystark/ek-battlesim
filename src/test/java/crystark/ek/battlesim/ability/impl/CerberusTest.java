package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class CerberusTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Cerberus Guy, 1, 1, 0, 1000, Special, Cerberus");
		PlayerWithAccess opponent = PlayerWithAccess
			.withCardsOnField(
				"Immune Guy, 1, 1, 1, 1000, Special, Immunity",
				"Reflection Guy, 1, 1, 1, 1000, Special, Reflection:150",
				"Random Guy, 1, 1, 1, 1000, Special");
		Player.salute(player, opponent);

		Card card = player.atIndexOnField(0);
		Card opImmune = opponent.atIndexOnField(0);
		Card opReflect = opponent.atIndexOnField(1);
		Card opRandom = opponent.atIndexOnField(2);

		Assert.assertEquals(1000, (int) card.getCurrentHp());
		Assert.assertEquals(1000, (int) opImmune.getCurrentHp());
		Assert.assertEquals(1000, (int) opReflect.getCurrentHp());
		Assert.assertEquals(1000, (int) opRandom.getCurrentHp());

		// Test the Block part
		card.damaged(null, 100, DamageType.attack);
		Assert.assertEquals(930, (int) card.getCurrentHp());

		// Test the ManBurn part
		player.playCardsFromField();
		Assert.assertEquals(640, (int) opImmune.getCurrentHp());
		Assert.assertEquals(640, (int) opReflect.getCurrentHp());
		Assert.assertEquals(880, (int) opRandom.getCurrentHp());

	}
}
