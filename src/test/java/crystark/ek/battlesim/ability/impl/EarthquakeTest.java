package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class EarthquakeTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Earthquake Guy, 10, 2, 600, 1500, Special, Earthquake:100:200");
		PlayerWithAccess opponent = PlayerWithAccess
			.withCardsOnField(
				"Immune Guy, 1, 1, 1, 1000, Special, Immunity",
				"Resistance Guy, 1, 1, 1, 1000, Special, Resistance",
				"Demon Guy, 1, 1, 1, 1000, Demon",
				"Random Guy, 1, 1, 1, 1000, Special");
		Player.salute(player, opponent);

		player.atIndexOnField(0).applyPowerAbilities();

		Assert.assertEquals(900, (int) opponent.atIndexOnField(0).getCurrentHp());
		Assert.assertEquals(800, (int) opponent.atIndexOnField(1).getCurrentHp());
		Assert.assertEquals(900, (int) opponent.atIndexOnField(2).getCurrentHp());
		Assert.assertEquals(900, (int) opponent.atIndexOnField(3).getCurrentHp());
	}
}
