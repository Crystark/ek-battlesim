package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

public class ResistanceLessTest {

	@Test
	public void testExileResistance() {
		Player player = PlayerWithAccess
			.withCardsOnField("ResistanceLess Guy, 0, 0, 1, 1, Special, Resistance-less");

		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField(
			"Resistance Guy1, 0, 0, 1, 3000, Special, Resistance",
			"NonResistance Guy1, 0, 0, 1, 3000, Special",
			"Resistance Guy2, 0, 0, 1, 3000, Special, Resistance");
		Player.salute(player, opponent);

		Card opponentCard = opponent.atIndexOnField(0);
		Assert.assertTrue(opponentCard.isAlive());


		player.playCardsFromField();
		Assert.assertFalse(opponentCard.isAlive());
		Assert.assertEquals(2, opponent.countCardsInDeck());
		Assert.assertEquals(1, opponent.countCardsInField());

	}
}
