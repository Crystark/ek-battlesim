package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.hamcrest.Matchers;
import org.junit.Test;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class PhysicalBuffTest {

	@Test
	public void testPhysicalBuff() {
		Player player = PlayerWithAccess.withCardsOnField(
			"Random Guy1, 0, 0, 100, 1000, Special",
			"Physical buff Guy, 0, 0, 100, 1000, Special, Physical Buff",
			"Random Guy2, 0, 0, 100, 1000, Special");

		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Rival Guy, 0, 0, 100, 5000, Special");
		Player.salute(player, opponent);

		final Card buffedCard1 = player.atIndexOnField(0);
		final Card buffCard = player.atIndexOnField(1);
		final Card buffedCard2 = player.atIndexOnField(2);

		final Card rivalCard = opponent.atIndexOnField(0);

		int currentAtk1 = buffedCard1.getCurrentAtk();
		int currentAtk = buffCard.getCurrentAtk();
		int currentAtk2 = buffedCard2.getCurrentAtk();

		assertEquals(100, currentAtk1);
		assertEquals(100, currentAtk);
		assertEquals(100, currentAtk2);

		player.playCardsFromField();

		//5000-100-700 = 4200
		//5000-100-2000 = 2900
		assertThat(rivalCard.getCurrentHp(), Matchers.allOf(greaterThanOrEqualTo(2900L), lessThanOrEqualTo(4200L)));

		//4200-100-700 = 3400
		//2900-100-2000 = 800
		player.playCardsFromField();

		assertThat(rivalCard.getCurrentHp(), Matchers.allOf(greaterThanOrEqualTo(800L), lessThanOrEqualTo(3400L)));

	}

}