package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Cards;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

public class TundraSilenceTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess
			.withCardsOnField("Tundra Bound Guy, 1, 0, 1, 1, Special, Tundra Silence, Resurrection:100");
		Player opponent = PlayerWithAccess.withoutCards();
		Player.salute(player, opponent);

		// Test base state: should not have Silence
		Card testCard = player.atIndexOnField(0);
		testCard.onTurnStart();
		Assert.assertEquals(0, testCard.getPowerAbilities().stream().filter(a -> a instanceof Silence).count());

		// Adding a Freyja should allow Silence to be added
		Cards.from("Freyja", player).addToField();
		testCard.onTurnStart();
		Assert.assertEquals(1, testCard.getPowerAbilities().stream().filter(a -> a instanceof Silence).count());

		// Kill the card and get back in the field
		testCard.damaged(null, 1, DamageType.unavoidable);
		player.playCardsFromHand();
		Assert.assertEquals(0, testCard.getPowerAbilities().stream().filter(a -> a instanceof Silence).count());

		// Freyja is still there so it should trigger again
		testCard.onTurnStart();
		Assert.assertEquals(1, testCard.getPowerAbilities().stream().filter(a -> a instanceof Silence).count());
	}
}
