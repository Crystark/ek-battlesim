package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class DemonSkinTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Demon skin Guy, 1, 1, 1, 1000, Special, Demon Skin");
		Card card = player.atIndexOnField(0);

		Assert.assertEquals(1000, (int) card.getCurrentHp());

		card.damaged(null, 100, DamageType.blood);
		Assert.assertEquals(1000, (int) card.getCurrentHp());

		card.damaged(null, 100, DamageType.attack);
		Assert.assertEquals(1000, (int) card.getCurrentHp());

		card.damaged(null, 300, DamageType.attack);
		Assert.assertEquals(900, (int) card.getCurrentHp());

		ManaCorruption mc = new ManaCorruption(100);
		mc.manaCorruption(card, null);
		Assert.assertEquals(600, (int) card.getCurrentHp());

	}
}
