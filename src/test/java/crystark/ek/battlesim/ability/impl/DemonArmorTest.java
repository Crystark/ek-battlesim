package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

public class DemonArmorTest {
	@Test
	public void test_apply() {
		//When the card receive damage, it will receive total damage minus 650 damage.
		Player player = PlayerWithAccess.withCardsOnField("Demon armor Guy, 1, 1, 1, 1000, Special, Demon Armor:650, Immunity");
		Card card = player.atIndexOnField(0);

		Assert.assertEquals(1000, (int) card.getCurrentHp());

		card.damaged(null, 100, DamageType.blood);
		Assert.assertEquals(1000, (int) card.getCurrentHp());

		card.damaged(null, 100, DamageType.attack);
		Assert.assertEquals(1000, (int) card.getCurrentHp());

		card.damaged(null, 650, DamageType.attack);
		Assert.assertEquals(1000, (int) card.getCurrentHp());

		ManaCorruption mc = new ManaCorruption(100);
		mc.manaCorruption(card, null);
		Assert.assertEquals(1000, (int) card.getCurrentHp());

		card.damaged(null, 700, DamageType.attack);
		Assert.assertEquals(950, (int) card.getCurrentHp());

		mc = new ManaCorruption(300);
		mc.manaCorruption(card, null);
		//950-(300*3 - 650)= 700
		Assert.assertEquals(700, (int) card.getCurrentHp());


	}
}
