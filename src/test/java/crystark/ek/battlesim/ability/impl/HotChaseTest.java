package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

/**
 * @see HotChase
 */
public class HotChaseTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess
			.withCardsOnField("Hot Chase Guy, 1, 1, 1, 1, Special, Hot Chase:100");

		PlayerWithAccess opponent = PlayerWithAccess
			.withCardsOnField("Dark Titan")
			.withCardsInCemetery("Scarab", "Scarab", "Scarab");
		Player.salute(player, opponent);

		Card testCard = player.atIndexOnField(0);

		Assert.assertEquals(1, testCard.getCurrentAtk());

		HotChase ab = (HotChase) testCard.getBoostAbilities().get(0);
		ab.onBeforeAttack(testCard);

		Assert.assertEquals(301, testCard.getCurrentAtk());

	}
}
