package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class BlockTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Block Guy, 1, 1, 1, 1000, Special, Block:50");

		Card card = player.atIndexOnField(0);

		card.damaged(null, 100, DamageType.unavoidable);
		Assert.assertEquals(900, (int) card.getCurrentHp());

		card.damaged(null, 100, DamageType.attack);
		Assert.assertEquals(850, (int) card.getCurrentHp());
	}
}
