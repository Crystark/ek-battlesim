package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

public class FatalPoisonTest {

	@Test
	public void test_apply() {
		Player player = PlayerWithAccess
			.withoutCards()
			.withCardsInHand("Fatal Poison Guy, 0, 0, 1, 1, Special, Fatal Poison, QS_Toxic Clouds 1");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Random Guy, 0, 0, 1, 3000, Special, Resurrection:100");
		Player.salute(player, opponent);

		Card opponentCard = opponent.atIndexOnField(0);
		Assert.assertTrue(opponentCard.isAlive());

		player.playCardsFromHand();
		player.playCardsFromField();
		Assert.assertFalse(opponentCard.isAlive());

		opponent.playCardsFromHand();
		player.playCardsFromField();
		Assert.assertTrue(opponentCard.isAlive());
	}

	@Test
	public void poisonKillsAfterDie() {
		Player player = PlayerWithAccess
			.withoutCards()
			.withCardsInHand(
				"Random Guy, 0, 0, 1, 201, Special",
				"Fatal Poison Guy, 0, 0, 10, 101, Special, Fatal Poison, QS_Toxic Clouds 1, Clean Sweep");//Toxic Clouds 10
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField(
			"Counter Guy, 0, 0, 1, 3000, Special, Retaliation:100",
			"Random Guy, 0, 0, 1, 3000, Special",
			"Lucky Guy, 0, 0, 1, 3000, Special");

		Player.salute(player, opponent);

		player.playCardsFromHand();
		Card poisonCard = player.atIndexOnField(1);
		Card luckyCard = opponent.atIndexOnField(2);
		player.playCardsFromField();

		Assert.assertEquals(1, player.countCardsInField());
		Assert.assertFalse("Card with Fatal Poison has to die after Retaliation", poisonCard.isAlive());

		Assert.assertEquals("Fatal Poison + Clean Sweep has to kill 2 card before die",1, opponent.countCardsInField());
		Assert.assertTrue(luckyCard.isAlive());
		Assert.assertTrue(luckyCard.isPoisonned());
	}
}
