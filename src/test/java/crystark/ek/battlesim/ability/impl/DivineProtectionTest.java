package crystark.ek.battlesim.ability.impl;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import crystark.ek.battlesim.effect.BonusHp;
import crystark.ek.battlesim.status.StatusEffectType;

/**
 * @see DivineProtection
 */
public class DivineProtectionTest {

	@Test
	public void test_create() {
		DivineProtection ab = new DivineProtection(100);

		Assert.assertEquals(Integer.valueOf(100), ab.getValue());
		Assert.assertEquals(StatusEffectType.aura, ab.getStatusType());
		Assert.assertThat(ab.getEffect(null), CoreMatchers.instanceOf(BonusHp.class));
	}

	@Test
	public void test_apply() {
		Player player = PlayerWithAccess
			.withCardsOnField(
				"Kitten, 1, 2, 1, 1, Special",
				"Divine Protection Guy, 10, 2, 600, 1500, Special, Divine Protection:100",
				"Puppy, 1, 2, 1, 1, Special");

		// As we're dealing with an aura, it has been applied when cards were set on field

		Card kitten = player.atIndexOnField(0);
		Card testCard = player.atIndexOnField(1);
		Card puppy = player.atIndexOnField(2);

		Assert.assertEquals(101, kitten.getCurrentHp());
		Assert.assertEquals(1600, testCard.getCurrentHp());
		Assert.assertEquals(101, puppy.getCurrentHp());
	}
}
