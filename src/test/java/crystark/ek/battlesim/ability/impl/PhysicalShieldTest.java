package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PhysicalShieldTest {

	@Test
	public void testExileResistance() {
		Player player = PlayerWithAccess.withCardsOnField("Physical Shield Guy, 0, 0, 1, 1000, Special, Physical Shield");

		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Double Attack Guy, 0, 0, 100, 3000, Special, Double Attack");
		Player.salute(player, opponent);
		final Card shieldCard = player.atIndexOnField(0);
		opponent.playCardsFromField();
		assertEquals(900, shieldCard.getCurrentHp());

		player.removeBuffs();

		opponent.playCardsFromField();
		assertEquals(800, shieldCard.getCurrentHp());
		opponent.playCardsFromField();
		assertEquals(600, shieldCard.getCurrentHp());


	}

}