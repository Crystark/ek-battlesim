package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class DoubleWingsTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess
			.withCardsOnField("Double Wings Guy, 1, 1, 1, 1, Special, Double Wings");

		Assert.assertEquals(3, player.countCardsInField());
	}
}
