package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

public class DoubleAttackTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Salvo Guy, 1, 1, 100, 1000, Special, Double Attack, Bloodthirsty:50");
		PlayerWithAccess opponent = PlayerWithAccess
			.withCardsOnField("Counterattack Guy, 1, 1, 1, 1000, Special, Counterattack:150, Parry:10");
		Player.salute(player, opponent);

		player.playCardsFromField();

		Assert.assertEquals("Should have damaged the card twice", 820, (int) opponent.atIndexOnField(0).getCurrentHp());
		Assert.assertEquals("Should have been countered twice", 700, (int) player.atIndexOnField(0).getCurrentHp());
		Assert.assertEquals("Should have increased attack twice", 200, player.atIndexOnField(0).getCurrentAtk());
	}

	@Test
	public void test_vs_player() {
		Player player = PlayerWithAccess.withCardsOnField("Salvo Guy, 1, 1, 100, 1000, Special, Double Attack");
		PlayerWithAccess opponent = PlayerWithAccess
			.withoutCards()
			.withCardsInHand("Counterattack Guy, 1, 1, 1, 1000, Special, Counterattack:150");
		Player.salute(player, opponent);

		player.playCardsFromField();

		Assert.assertEquals("Should have damaged the player once", opponent.desc.health - 100, (int) opponent.getCurrentHp());
	}

	@Test
	public void test_vs_card_dead_from_first_attack() {
		Player player = PlayerWithAccess.withCardsOnField("Salvo Guy, 1, 1, 100, 1000, Special, Double Attack");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Dead in one hit Guy, 1, 1, 1, 1, Special");
		Player.salute(player, opponent);

		player.playCardsFromField();

		Assert.assertEquals("Should not have damaged the player", (int) opponent.desc.health, opponent.getCurrentHp());
	}
}
