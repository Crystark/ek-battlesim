package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class PlagueBlastTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Plague Blast Guy, 1, 1, 0, 1000, Special, Plague Blast");
		PlayerWithAccess opponent = PlayerWithAccess
			.withCardsOnField(
				"Immune Guy, 1, 1, 100, 1000, Special, Immunity",
				"Reflection Guy, 1, 1, 100, 1000, Special, Reflection:150",
				"Random Guy, 1, 1, 100, 1000, Special");
		Player.salute(player, opponent);

		Card opImmune = opponent.atIndexOnField(0);
		Card opReflect = opponent.atIndexOnField(1);
		Card opRandom = opponent.atIndexOnField(2);

		Assert.assertEquals(1000, (int) opImmune.getCurrentHp());
		Assert.assertEquals(1000, (int) opReflect.getCurrentHp());
		Assert.assertEquals(1000, (int) opRandom.getCurrentHp());

		Assert.assertEquals(100, opImmune.getCurrentAtk());
		Assert.assertEquals(100, opReflect.getCurrentAtk());
		Assert.assertEquals(100, opRandom.getCurrentAtk());

		player.playCardsFromField();

		Assert.assertEquals(1000, (int) opImmune.getCurrentHp());
		Assert.assertEquals(810, (int) opReflect.getCurrentHp());
		Assert.assertEquals(810, (int) opRandom.getCurrentHp());

		Assert.assertEquals(100, opImmune.getCurrentAtk());
		Assert.assertEquals(50, opReflect.getCurrentAtk());
		Assert.assertEquals(50, opRandom.getCurrentAtk());

		Assert.assertFalse("Immune should not be poisonned", opImmune.isPoisonned());
		Assert.assertTrue("Reflect should be poisonned", opReflect.isPoisonned());
		Assert.assertTrue("Random should be poisonned", opRandom.isPoisonned());

	}
}
