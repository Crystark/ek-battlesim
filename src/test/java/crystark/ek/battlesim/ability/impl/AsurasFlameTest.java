package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class AsurasFlameTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Asura's Flame Guy, 1, 1, 1, 1, Special, Asura's Flame:150:60");
		PlayerWithAccess opponent = PlayerWithAccess
			.withCardsOnField(
				"Immune Guy, 1, 1, 1, 1000, Special, Immunity",
				"Reflection Guy, 1, 1, 1, 1000, Special, Reflection:150",
				"Random Guy, 1, 1, 1, 1000, Special");
		Player.salute(player, opponent);

		Card card = player.atIndexOnField(0);
		card.getPowerAbilities().get(0).apply(card);

		Assert.assertEquals(1000, (int) opponent.atIndexOnField(0).getCurrentHp());
		Assert.assertEquals(1000, (int) opponent.atIndexOnField(1).getCurrentHp());
		Assert.assertEquals(670, (int) opponent.atIndexOnField(2).getCurrentHp());
	}
}
