package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

public class BreakIceTest {

	@Test
	public void test_apply_on_ice_shield() {
		Player player = PlayerWithAccess.withCardsOnField("Break Ice Guy, 1, 1, 1, 1, Special, Break Ice");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Ice Shield Guy, 1, 1, 1, 3000, Special, Ice Shield 10");
		Player.salute(player, opponent);

		Card opponentCard = opponent.atIndexOnField(0);
		Assert.assertTrue(opponentCard.isAlive());

		player.playCardsFromField();
		Assert.assertFalse(opponentCard.isAlive());
	}

	@Test
	public void test_apply_on_frozen() {
		Player player = PlayerWithAccess.withCardsOnField("Break Ice Guy, 1, 1, 1, 1, Special, Break Ice");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Random Guy, 1, 1, 1, 3000, Special");
		Player.salute(player, opponent);

		Card opponentCard = opponent.atIndexOnField(0);
		Assert.assertTrue(opponentCard.isAlive());

		player.playCardsFromField();
		Assert.assertTrue(opponentCard.isAlive());

		opponentCard.markFrozen();
		player.playCardsFromField();
		Assert.assertFalse(opponentCard.isAlive());
	}
}
