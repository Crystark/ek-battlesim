package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class RetreatTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Retreat Guy, 0, 0, 1, 1500, Special, Retreat");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Attacking Guy, 0, 0, 1000, 1000, Special");
		Player.salute(player, opponent);

		Assert.assertEquals(1, player.countCardsInField());
		Assert.assertEquals(0, player.countCardsInHand());
		Assert.assertEquals(0, player.countCardsInDeck());
		Assert.assertEquals(0, player.countCardsInCemetery());

		// Retreat to hand
		opponent.playCardsFromField();
		Assert.assertEquals(0, player.countCardsInField());
		Assert.assertEquals(1, player.countCardsInHand());
		Assert.assertEquals(0, player.countCardsInDeck());
		Assert.assertEquals(0, player.countCardsInCemetery());

		// If dead go to cemetery
		player.cleanup();
		player.playCardsFromHand();
		player.atIndexOnField(0).damaged(null, 500, DamageType.unavoidable);
		opponent.playCardsFromField();
		Assert.assertEquals(0, player.countCardsInField());
		Assert.assertEquals(0, player.countCardsInHand());
		Assert.assertEquals(0, player.countCardsInDeck());
		Assert.assertEquals(1, player.countCardsInCemetery());
	}

	@Test
	public void test_apply_hand_full() {
		Player player = PlayerWithAccess
			.withCardsOnField("Retreat Guy, 0, 0, 1, 1500, Special, Retreat")
			.withCardsInHand("Scarab", "Scarab", "Scarab", "Scarab", "Scarab")
			.withCardsInDeck("Scarab");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Attacking Guy, 0, 0, 1000, 1000, Special");
		Player.salute(player, opponent);

		Assert.assertEquals(1, player.countCardsInField());
		Assert.assertEquals(5, player.countCardsInHand());
		Assert.assertEquals(1, player.countCardsInDeck());
		Assert.assertEquals(0, player.countCardsInCemetery());

		opponent.playCardsFromField();
		Assert.assertEquals(0, player.countCardsInField());
		Assert.assertEquals(5, player.countCardsInHand());
		Assert.assertEquals(2, player.countCardsInDeck());
		Assert.assertEquals(0, player.countCardsInCemetery());
	}
}
