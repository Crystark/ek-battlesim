package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Test;

import static org.junit.Assert.*;

public class TauntTest {
	@Test
	public void TauntCatchDamageLargerHp() {
		Player player = PlayerWithAccess.withCardsOnField(
			"Random Guy, 1, 1, 1, 1500, Special",
			"Taunt Guy, 1, 1, 1, 1500, Special, Taunt, Parry:100, Counterattack:50");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Attacking Guy, 1, 1, 1000, 1000, Special");
		Player.salute(player, opponent);

		opponent.playCardsFromField();

		final Card tauntCard = player.atIndexOnField(1);
		final Card randomCard = player.atIndexOnField(0);
		assertEquals(1500, randomCard.getCurrentHp());
		assertEquals(600, tauntCard.getCurrentHp());//Damage reducer works for Taunt card
		assertEquals(950, opponent.atIndexOnField(0).getCurrentHp()); // Counterattack works

		opponent.playCardsFromField();

		assertEquals(1500, randomCard.getCurrentHp());
		assertFalse(tauntCard.isAlive());

	}

	@Test
	public void TauntIsNotSilenced() {
		Player player = PlayerWithAccess.withCardsOnField(
			"Random Guy, 1, 1, 1, 1500, Special",
			"Taunt Guy, 1, 1, 1, 1500, Special, Taunt, Parry:100");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Attacking Guy, 1, 1, 1000, 1000, Special, Field of Silence");
		Player.salute(player, opponent);

		opponent.playCardsFromField();

		final Card tauntCard = player.atIndexOnField(1);
		final Card randomCard = player.atIndexOnField(0);
		assertTrue(tauntCard.isSilenced());
		assertEquals(1500, randomCard.getCurrentHp()); // Taunt is not silenced
		assertEquals(500, tauntCard.getCurrentHp()); //Parry is silenced

		opponent.playCardsFromField();

		assertEquals(1500, randomCard.getCurrentHp());
		assertFalse(tauntCard.isAlive());
	}

	@Test
	public void TauntIsActiveVsArmorLess() {
		Player player = PlayerWithAccess.withCardsOnField(
			"Random Guy, 1, 1, 1, 1500, Special",
			"Taunt Guy, 1, 1, 1, 1500, Special, Taunt");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("ArmorLess Guy, 1, 1, 1000, 1000, Special, Armor-less:100");
		Player.salute(player, opponent);

		opponent.playCardsFromField();

		final Card tauntCard = player.atIndexOnField(1);
		final Card randomCard = player.atIndexOnField(0);
		assertEquals(1500, randomCard.getCurrentHp());
		assertEquals(500, tauntCard.getCurrentHp());

		opponent.playCardsFromField();

		assertEquals(1500, randomCard.getCurrentHp());
		assertFalse(tauntCard.isAlive());
	}

	@Test
	public void DamageToTauntCardIsNotCatchedByAnother() {
		Player player = PlayerWithAccess.withCardsOnField(
			"Taunt Guy1, 1, 1, 1, 1500, Special, Taunt", // Btw, check Taunt to Taunt indefinite recursion is broken
			"Taunt Guy2, 1, 1, 1, 1500, Special, Taunt");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Attacking Guy, 1, 1, 1000, 1000, Special");
		Player.salute(player, opponent);

		opponent.playCardsFromField();

		final Card tauntCard1 = player.atIndexOnField(0);
		final Card tauntCard2 = player.atIndexOnField(1);
		assertEquals(500, tauntCard1.getCurrentHp());
		assertEquals(1500, tauntCard2.getCurrentHp());

	}

	@Test
	public void SecondAttackIsNotIntercepted() {
		Player player = PlayerWithAccess.withCardsOnField(
			"Random Guy1, 1, 1, 1, 1500, Special",
			"Taunt Guy1, 1, 1, 1, 2500, Special, Taunt"
			);
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Attacking Guy, 1, 1, 1000, 1000, Special, Double Attack");
		Player.salute(player, opponent);

		opponent.playCardsFromField();

		final Card randomCard = player.atIndexOnField(0);
		final Card tauntCard = player.atIndexOnField(1);
		assertEquals(1500, tauntCard.getCurrentHp());
		assertEquals(500, randomCard.getCurrentHp());

	}

	@Test
	public void CleanSweepIsInercepted() {
		//Tools.activateDebug();
		Player player = PlayerWithAccess.withCardsOnField(
			"Random Guy1, 1, 1, 1, 1500, Special",
			"Taunt Guy1, 1, 1, 1, 2500, Special, Taunt",
			"Random Guy2, 1, 1, 1, 1500, Special");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Attacking Guy, 1, 1, 1000, 1000, Special, Clean Sweep");
		Player.salute(player, opponent);

		opponent.playCardsFromField();

		final Card randomCard = player.atIndexOnField(0);
		final Card tauntCard = player.atIndexOnField(1);
		final Card randomCard2 = player.atIndexOnField(2);
		assertEquals(500, tauntCard.getCurrentHp());
		assertEquals(1500, randomCard.getCurrentHp());
		assertEquals(1500, randomCard2.getCurrentHp());

	}
}
