package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class SacredFlameTest {
	@Test
	public void test_apply() {
		PlayerWithAccess player = PlayerWithAccess
			.withCardsOnField("Sacred Flame Guy, 1, 1, 1, 1, Special, Sacred Flame");
		Player opponent = PlayerWithAccess
			.withCardsOnField("Scarab")
			.withCardsInHand("Soul Imprison Guy, 1, 0, 1, 1, Special, Soul Imprison")
			.withCardsInCemetery(
				"Random Guy,   1, 2, 1, 1, Special, Immunity, Resistance",
				"Random Guy,   1, 6, 1, 1, Special, Immunity, Resistance",
				"Random Guy,   1, 4, 1, 1, Special, Immunity, Resistance");
		Player.salute(player, opponent);

		Assert.assertEquals(3, opponent.countCardsInCemetery());

		player.atIndexOnField(0).applyPowerAbilities();
		Assert.assertEquals(2, opponent.countCardsInCemetery());

		opponent.playCardsFromHand();
		player.atIndexOnField(0).applyPowerAbilities();
		Assert.assertEquals(2, opponent.countCardsInCemetery());
	}
}
