package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class SpellReductionTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Spell Reduction Guy, 1, 1, 1, 1000, Special, Spell Reduction:50");
		Card card = player.atIndexOnField(0);

		Assert.assertEquals(1000, card.getCurrentHp());

		card.damaged(null, 100, DamageType.blood);
		Assert.assertEquals(950, card.getCurrentHp());

		card.damaged(null, 100, DamageType.attack);
		Assert.assertEquals(850, card.getCurrentHp());

		card.damaged(null, 100, DamageType.ice);
		Assert.assertEquals(800, card.getCurrentHp());
	}
}
