package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

/**
 * 'Frost Shock 20' was renamed in the Sim to 'Lighting Cut 20'
 * Deal 800 damage to all enemy cards. The cards receiving damage will have a 75% chance to get frozen and
 * can't attack or use skills in the next turn.If any of the card has Immunity or Reflection, deal 300% damage.
 */
public class LightingCutTest {
	@Test
	public void testLightingCut() {
		PlayerWithAccess player = PlayerWithAccess.withoutCards();
		PlayerWithAccess opponent = PlayerWithAccess
			.withCardsOnField(
				"Immune Guy, 1, 1, 1, 3000, Special, Immunity",
				"Reflection Guy, 1, 1, 1, 3000, Special, Reflection:150",
				"Random Guy, 1, 1, 1, 3000, Special");
		Player.salute(player, opponent);
		player.addCardsToField("Frost Shock Guy, 1, 1, 0, 1000, Special, Lighting Cut 20");

		Card card = player.atIndexOnField(0);
		Card opImmune = opponent.atIndexOnField(0);
		Card opReflect = opponent.atIndexOnField(1);
		Card opRandom = opponent.atIndexOnField(2);

		Assert.assertEquals(1000, (int) card.getCurrentHp());
		player.playCardsFromField();
		
		Assert.assertEquals(1400, (int) opImmune.getCurrentHp());
		Assert.assertEquals(2200, (int) opRandom.getCurrentHp());
		Assert.assertEquals("Lighting Cut (FrostShock 20) damage has not to be reflected", 1400, (int) opReflect.getCurrentHp());

	}


	@Test
	public void testLightingCutFullDescription() {
		
		PlayerWithAccess player = PlayerWithAccess.withoutCards();
		PlayerWithAccess opponent = PlayerWithAccess
			.withCardsOnField(
				"Immune Guy, 1, 1, 1, 3000, special, Immunity",
				"Reflection Guy, 1, 1, 1, 3000, special, Reflection:150",
				"Random Guy, 1, 1, 1, 3000, special",
				"Sensitive Guy, 1, 1, 1, 3000, special, Sensitive:100"
				);
		Player.salute(player, opponent);

		/*
		 * Deal 200 damage to all enemy cards. The cards receiving damage will have a 100% chance to get frozen and
 		 * can't attack or use skills in the next turn.If any of the card has Immunity or Reflection, deal 300% damage.
		 */
		player.addCardsToField("Frost Shock Guy, 1, 1, 0, 1000, Special, Lighting Cut:50_100:200_300");

		Card opImmune = opponent.atIndexOnField(0);
		Card opReflect = opponent.atIndexOnField(1);
		Card opRandom = opponent.atIndexOnField(2);
		Card opSensitive = opponent.atIndexOnField(3);

		player.playCardsFromField();

		Assert.assertEquals(2400, (int) opImmune.getCurrentHp());
		Assert.assertFalse(opImmune.isShocked());
		Assert.assertEquals(2400, (int) opReflect.getCurrentHp());
		Assert.assertFalse(opReflect.isShocked());
		Assert.assertEquals(2800, (int) opRandom.getCurrentHp());
		Assert.assertTrue(opRandom.isShocked());
		Assert.assertEquals(3000, (int) opSensitive.getCurrentHp());
		Assert.assertTrue(opSensitive.isShocked());

	}
}
