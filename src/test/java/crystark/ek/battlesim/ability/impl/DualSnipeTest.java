package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.ability.impl.alias.DualSnipe;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

public class DualSnipeTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Dual Snipe Guy, 10, 2, 600, 1500, Special, Dual Snipe:100");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Dark Titan");
		Player.salute(player, opponent);

		Card dsg = player.atIndexOnField(0);
		DualSnipe ds = (DualSnipe) dsg.getPowerAbilities().get(0);
		ds.apply(dsg);

		Card opponentCard = opponent.pickCardInField();
		Assert.assertEquals(opponentCard.desc.health - 100, (int) opponentCard.getCurrentHp());
	}
}
