package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class FieldOfChaosTest {

	@Test
	public void test_apply() {
		FieldOfChaos ab = new FieldOfChaos(100);
		Player player = PlayerWithAccess
			.withCardsOnField(
				"Some guy, 1, 1, 1, 1, Special",
				"Some guy, 1, 1, 1, 1, Special",
				"Some guy, 1, 1, 1, 1, Special",
				"Some guy, 1, 1, 1, 1, Special",
				"Some guy, 1, 1, 1, 1, Special");

		ab.fieldOfChaos(player);

		Assert.assertEquals(3, player.countCardsInField(Card::isConfused));
	}
}
