package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class DianasTouchTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess
			.withCardsOnField(
				"Immunity guy, 1, 1, 1, 1000, Special, Immunity, Diana's Touch:10",
				"Some guy, 1, 1, 1, 1000, Special",
				"Some guy, 1, 1, 1, 1000, Special");

		Card immune = player.atIndexOnField(0);
		Card some1 = player.atIndexOnField(1);
		Card some2 = player.atIndexOnField(2);

		immune.damaged(null, 200, DamageType.unavoidable);
		some2.damaged(null, 250, DamageType.unavoidable);

		Assert.assertEquals(800, immune.getCurrentHp());
		Assert.assertEquals(1000, some1.getCurrentHp());
		Assert.assertEquals(750, some2.getCurrentHp());

		immune.applyPowerAbilities();

		Assert.assertEquals(800, immune.getCurrentHp());
		Assert.assertEquals(1000, some1.getCurrentHp());
		Assert.assertEquals(850, some2.getCurrentHp());

		immune.applyPowerAbilities();

		Assert.assertEquals(900, immune.getCurrentHp());
		Assert.assertEquals(1000, some1.getCurrentHp());
		Assert.assertEquals(850, some2.getCurrentHp());
	}
}
