package crystark.ek.battlesim.ability.impl;

import crystark.common.format.CardDescFormat;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Cards;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Test;

/*
TODO Develop syntax for recursive skills composition like this:
Soul Bound:Composit['Summon:Cannon Fodder':Summon[NO 17 & NO 17] & Magic Arrays:80]
Soul Bound(Cannon Fodder(NO 17 & NO 17) & Magic Arrays(80))
Current:
Soul Bound:Summon\\:Cannon Fodder\:NO 17 & NO 17 & Magic Arrays\:80
 */
public class Paraselene {
	@Test
	public void test_apply() {
		//PlayerWithAccess player = PlayerWithAccess.withCardsOnField("Immune Guy, 1, 1, 1, 3000, Special, Soul Bound");
		PlayerWithAccess player = PlayerWithAccess.withoutCards();
		final Card card = Cards.from("Immune Guy, 1, 1, 1, 3000, Special, Soul Bound", player);
		String s = CardDescFormat.toFullDescription(card.desc);

//		PlayerWithAccess player2 = PlayerWithAccess.withCardsOnField("Paraselene");

	}
}
