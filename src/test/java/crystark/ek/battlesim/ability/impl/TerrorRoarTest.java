package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

/**
 * @see TerrorRoar
 */
public class TerrorRoarTest {
	@Test
	public void test_terrorRoar() {
		TerrorRoar ab = new TerrorRoar();
		TerrorRoar ab2 = new TerrorRoar();
		Player player = PlayerWithAccess
			.withCardsOnField("Terrorised guy, 10, 2, 600, 1500, Special");

		Card terrorised = player.atIndexOnField(0);
		Assert.assertEquals(600, terrorised.getCurrentAtk());

		// Apply first terrorRoar
		ab.terrorRoar(player);
		terrorised.applyBoosts();
		Assert.assertEquals(300, terrorised.getCurrentAtk());
		terrorised.removeBoosts();
		Assert.assertEquals(600, terrorised.getCurrentAtk());

		// Apply first terrorRoar a 2nd time: should not change anything
		ab.terrorRoar(player);
		terrorised.applyBoosts();
		Assert.assertEquals(300, terrorised.getCurrentAtk());
		terrorised.removeBoosts();
		Assert.assertEquals(600, terrorised.getCurrentAtk());

		// Apply second terrorRoar
		ab2.terrorRoar(player);
		terrorised.applyBoosts();
		Assert.assertEquals(150, terrorised.getCurrentAtk());
		terrorised.removeBoosts();
		Assert.assertEquals(600, terrorised.getCurrentAtk());
	}
}
