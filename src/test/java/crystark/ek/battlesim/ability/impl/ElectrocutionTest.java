package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class ElectrocutionTest {

	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Electrocution Guy, 1, 1, 1, 1, Special, Electrocution");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Random Guy, 1, 1, 1, 3000, Special");
		Player.salute(player, opponent);

		Card opponentCard = opponent.atIndexOnField(0);
		Assert.assertTrue(opponentCard.isAlive());

		player.playCardsFromField();
		Assert.assertTrue(opponentCard.isAlive());

		opponentCard.markShocked();
		player.playCardsFromField();
		Assert.assertFalse(opponentCard.isAlive());
	}
}
