package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class MachinegunTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Machinegun Guy, 1, 1, 1, 1, Special, Machinegun");
		PlayerWithAccess opponent = PlayerWithAccess
			.withCardsOnField(
				"Immune Guy, 1, 1, 1, 2000, Special, Immunity",
				"Reflection Guy, 1, 1, 1, 1000, Special, Reflection:150",
				"Random Guy, 1, 1, 1, 3000, Special");
		Player.salute(player, opponent);

		player.atIndexOnField(0).applyPowerAbilities();

		Assert.assertEquals(1620, opponent.atIndexOnField(0).getCurrentHp());
		Assert.assertEquals(380, opponent.atIndexOnField(1).getCurrentHp());
		Assert.assertEquals(2820, opponent.atIndexOnField(2).getCurrentHp());

	}
}
