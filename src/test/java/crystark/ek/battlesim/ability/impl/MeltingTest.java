package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

public class MeltingTest {

	@Test
	public void test_apply() {
		Player player = PlayerWithAccess
			.withoutCards()
			.withCardsInHand("Melting Guy, 0, 0, 1, 1, Special, Melting, QS_Fire God 1");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Random Guy, 0, 0, 1, 3000, Special, Resurrection:100");
		Player.salute(player, opponent);

		Card opponentCard = opponent.atIndexOnField(0);
		Assert.assertTrue(opponentCard.isAlive());

		player.playCardsFromHand();
		player.playCardsFromField();
		Assert.assertFalse(opponentCard.isAlive());

		opponent.playCardsFromHand();
		player.playCardsFromField();
		Assert.assertTrue(opponentCard.isAlive());
	}
}
