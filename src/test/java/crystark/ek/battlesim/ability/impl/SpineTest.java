package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class SpineTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Spine Guy, 1, 1, 1, 1500, Special, Spine:40");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Attacking Guy, 1, 1, 1000, 1000, Special");
		Player.salute(player, opponent);

		opponent.playCardsFromField();

		Assert.assertEquals(600, opponent.atIndexOnField(0).getCurrentHp());

		opponent.playCardsFromField();

		Assert.assertEquals(200, opponent.atIndexOnField(0).getCurrentHp());
	}
}
