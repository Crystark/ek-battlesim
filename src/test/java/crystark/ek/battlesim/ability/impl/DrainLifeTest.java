package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class DrainLifeTest {

	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Drain Life Guy, 1, 1, 100, 1000, Special, Drain Life:10");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Random Guy, 1, 1, 100, 3000, Special");
		Player.salute(player, opponent);

		Card card = player.atIndexOnField(0);
		Card opponentCard = opponent.atIndexOnField(0);

		Assert.assertEquals(1000, card.getCurrentHp());
		Assert.assertEquals(1000, card.getCurrentBaseHp());
		Assert.assertEquals(3000, opponentCard.getCurrentHp());
		Assert.assertEquals(3000, opponentCard.getCurrentBaseHp());

		opponent.playCardsFromField();
		Assert.assertEquals(1000, card.getCurrentHp()); // Hit for 100 but steal 300
		Assert.assertEquals(1000, card.getCurrentBaseHp()); // Does it increase base HP ?
		Assert.assertEquals(2700, opponentCard.getCurrentHp());
		Assert.assertEquals(2700, opponentCard.getCurrentBaseHp());
	}
}
