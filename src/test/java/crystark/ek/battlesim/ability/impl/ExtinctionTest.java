package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class ExtinctionTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Extinguisher, 1, 1, 1, 2000, Special, Extinction");
		PlayerWithAccess opponent = PlayerWithAccess
			.withCardsOnField(
				"Immune Guy, 1, 1, 1, 1, Special, Immunity",
				"Last Chance Guy, 1, 1, 1, 1, Special, Last Chance, Resurrection:100");
		Player.salute(player, opponent);

		Assert.assertEquals(0, opponent.countCardsInCemetery());
		Assert.assertEquals(2, opponent.countCardsInField());

		player.playCardsFromField();
		Assert.assertEquals(0, opponent.countCardsInCemetery());
		Assert.assertEquals(1, opponent.countCardsInField());

		opponent.cleanup();
		player.playCardsFromField();
		Assert.assertEquals(0, opponent.countCardsInCemetery());
		Assert.assertEquals(1, opponent.countCardsInField());

		opponent.playCardsFromField();
		player.playCardsFromField();
		Assert.assertEquals(0, opponent.countCardsInCemetery());
		Assert.assertEquals(0, opponent.countCardsInField());
	}
}
