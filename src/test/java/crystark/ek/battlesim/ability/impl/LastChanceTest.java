package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class LastChanceTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess
			.withCardsOnField(
				"Last Chance guy, 1, 1, 1, 10, Special, Last Chance",
				"Last Chance guy, 1, 1, 1, 10, Special, Last Chance");
		Card cardOne = player.atIndexOnField(0);
		Card cardTwo = player.atIndexOnField(1);

		cardOne.damaged(null, 10, DamageType.fire);
		Assert.assertTrue(cardOne.isLastChance());
		Assert.assertTrue(cardOne.isAlive());
		Assert.assertEquals(1, (int) cardOne.getCurrentHp());

		player.destroyCard(cardTwo);
		Assert.assertFalse(cardTwo.isLastChance());
		Assert.assertFalse(cardTwo.isAlive());
	}
}
