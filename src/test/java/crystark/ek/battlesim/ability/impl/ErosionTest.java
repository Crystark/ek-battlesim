package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

public class ErosionTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess
			.withoutCards()
			.withCardsInHand("Sacrificing Guy, 1, 0, 1000, 1000, Special, Erosion 1");

		Player opponent = PlayerWithAccess
			.withCardsOnField(
				"Sacrificed Guy, 1, 1, 1, 1, Special"
				);

		Player.salute(player, opponent);

		Card sacrificedCard = opponent.atIndexOnField(0);
		Assert.assertTrue(sacrificedCard.isAlive());

		player.playCardsFromHand();
		Card sacrificingCard = player.atIndexOnField(0);

		Assert.assertFalse(sacrificedCard.isAlive());
		Assert.assertEquals(1, opponent.countCardsInCemetery());

		Assert.assertEquals(2000, sacrificingCard.getCurrentBaseAtk());
		Assert.assertEquals(2000, sacrificingCard.getCurrentBaseHp());

	}
}
