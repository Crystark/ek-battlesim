package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

public class TwinsTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess
			.withCardsOnField("Scarab, 1, 1, 1, 1, Special, Twins");

		Assert.assertEquals(2, player.countCardsInField());
		Assert.assertEquals("Scarab", player.atIndexOnField(1).desc.name);
		Assert.assertEquals(250, player.atIndexOnField(1).desc.attack);
	}

	@Test
	public void test_2_abilities() {
		Player player = PlayerWithAccess
			.withCardsOnField("Scarab, 1, 1, 1, 1, Special, Twins, Twins");

		Assert.assertEquals(3, player.countCardsInField());
		Assert.assertEquals("Scarab", player.atIndexOnField(1).desc.name);
		Assert.assertEquals("Scarab", player.atIndexOnField(2).desc.name);
		Assert.assertEquals(250, player.atIndexOnField(2).desc.attack);
	}
}
