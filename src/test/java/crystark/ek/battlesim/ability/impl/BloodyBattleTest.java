package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

/**
 * @see BloodyBattle
 */
public class BloodyBattleTest {

	@Test
	public void test_create() {
		BloodyBattle ab = new BloodyBattle(100);

		Assert.assertEquals(Integer.valueOf(100), ab.getValue());
	}

	@Test
	public void test_apply() {
		Player player = PlayerWithAccess
			.withCardsOnField("Bloody Battle Guy, 1, 1, 1, 1000, Special, Bloody Battle:100");

		Card testCard = player.atIndexOnField(0);

		Assert.assertEquals(1, testCard.getCurrentAtk());

		BloodyBattle ab = (BloodyBattle) testCard.getBoostAbilities().get(0);
		ab.onBeforeAttack(testCard);
		Assert.assertEquals(1, testCard.getCurrentAtk());

		testCard.removeBoosts();
		Assert.assertEquals(1, testCard.getCurrentAtk());

		// Damage card > 700 / 1000
		testCard.damaged(null, 300, DamageType.unavoidable);

		ab.onBeforeAttack(testCard);
		Assert.assertEquals(301, testCard.getCurrentAtk());

		testCard.removeBoosts();
		Assert.assertEquals(1, testCard.getCurrentAtk());

		// Bonus HP card > 1100 / 1400 (base 1000)
		testCard.addBonusHp(400);

		ab.onBeforeAttack(testCard);
		Assert.assertEquals(1, testCard.getCurrentAtk());
		testCard.removeBoosts();

		// Damage card > 800 / 1400 (base 1000)
		testCard.damaged(null, 300, DamageType.unavoidable);

		ab.onBeforeAttack(testCard);
		Assert.assertEquals(201, testCard.getCurrentAtk());
		testCard.removeBoosts();
		Assert.assertEquals(1, testCard.getCurrentAtk());
	}
}
