package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

/**
 * @see Weaken
 */
public class WeakenTest {
	@Test
	public void test_weaken() {
		Weaken ab = new Weaken(30);
		Player player = PlayerWithAccess.withCardsOnField("Weakened guy, 10, 2, 50, 1500, Special");

		Card weakened = player.atIndexOnField(0);
		Assert.assertEquals(50, weakened.getCurrentAtk());

		ab.onAfterHit(null, weakened, 9999);
		Assert.assertEquals(20, weakened.getCurrentAtk());
		ab.onAfterHit(null, weakened, 9999);
		Assert.assertEquals(0, weakened.getCurrentAtk());
	}

	@Test
	public void test_weaken_with_aura() {
		Weaken ab = new Weaken(30);
		Player player = PlayerWithAccess
			.withCardsOnField(
				"Weakened guy, 10, 2, 50, 1500, Special",
				"Buffing guy, 10, 2, 1, 1, Special, Power Source:20");

		Card weakened = player.atIndexOnField(0);
		Card powerSource = player.atIndexOnField(1);
		Assert.assertEquals(70, weakened.getCurrentAtk());

		ab.onAfterHit(null, weakened, 9999);
		Assert.assertEquals(40, weakened.getCurrentAtk());
		ab.onAfterHit(null, weakened, 9999);
		Assert.assertEquals(10, weakened.getCurrentAtk());
		weakened.removeAurasWithSource(powerSource);
		Assert.assertEquals(0, weakened.getCurrentAtk());
	}
}
