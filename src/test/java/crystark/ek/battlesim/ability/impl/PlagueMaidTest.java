package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class PlagueMaidTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess
			.withCardsOnField("Plague Maid Guy, 1, 1, 1, 1, Special, Plague Maid");

		Assert.assertEquals(3, player.countCardsInField());
	}
}
