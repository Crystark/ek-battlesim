package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class BlessTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Bless Guy, 1, 1, 1, 1, Special, Bless 5");
		Card card = player.atIndexOnField(0);

		int baseHealth = player.desc.health;
		int tenPercent = (int) (baseHealth * 0.1);
		int twentyPercent = tenPercent * 2;
		player.damaged(twentyPercent, false);

		Assert.assertEquals(baseHealth - twentyPercent, (int) player.getCurrentHp());

		card.applyPowerAbilities();

		Assert.assertEquals(baseHealth - tenPercent, (int) player.getCurrentHp());
	}
}
