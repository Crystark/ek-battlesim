package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.ability.impl.Dodge.PRNG;
import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class DodgePRNGTest {
	@Test
	public void test_prng() {
		int iters = 50_000;

		double rate;
		for (Integer key : Dodge.PRNG.successAfterFail.keySet()) {
			rate = simSuccessRate(key, iters);
			//System.out.println(key + " vs " + rate);
			Assert.assertEquals(key / 100d, rate, 0.01);
		}

		rate = simSuccessRate(85, iters);
		//			System.out.println(85 + " vs " + rate);
		Assert.assertEquals(0.85, rate, 0.01);

		rate = simSuccessRate(45, iters);
		//			System.out.println(45 + " vs " + rate);
		Assert.assertEquals(0.45, rate, 0.01);

		rate = simSuccessRate(60, iters);
		//			System.out.println(60 + " vs " + rate);
		Assert.assertEquals(0.60, rate, 0.01);

		rate = simSuccessRate(70, iters);
		Assert.assertEquals(0.70, rate, 0.01);

		rate = simSuccessRate(100, iters);
		Assert.assertEquals(1.0, rate, 0.01);

		rate = simSuccessRate(0, iters);
		Assert.assertEquals(0.0, rate, 0.01);

		rate = simSuccessRate(65, iters);
		Assert.assertNotEquals(0.67, rate, 0.01);
	}

	private static double simSuccessRate(Integer key, int iters){
		int success = 0;
		PRNG prng = new Dodge.PRNG(key);
		for (int i = 0; i < iters; i++) {
			if (prng.test()) success++;
		}
		return (double) success / iters;
	}


	@Test
	public void test_prng_firstHit() {

		int iters = 10_000;
		for (Map.Entry<Integer, Double> entry : PRNG.successAfterSuccess.entrySet()) {
			Integer key = entry.getKey();
			int succ = 0;
			for (int i = 0; i < iters; i++) {
				Dodge.PRNG prng = new Dodge.PRNG(key);
				if (prng.test())
					succ++;
			}
			double rate = (double) succ / iters;
			//System.out.println("key=" + entry.getKey() +": " + entry.getValue() + " vs " + rate);
			Assert.assertEquals(entry.getValue() , rate, 0.01);
		}
	}
}
