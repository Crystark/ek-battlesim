package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class SoulImprisonTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Soul Imprison Guy, 1, 1, 1, 1, Special, Soul Imprison");
		Player opponent = PlayerWithAccess.withCardsOnField("Scarab");
		Player.salute(player, opponent);

		Assert.assertTrue(player.isCemeteryLocked());
		Assert.assertTrue(opponent.isCemeteryLocked());

		player.atIndexOnField(0).damaged(null, 1, DamageType.unavoidable);

		Assert.assertFalse(player.isCemeteryLocked());
		Assert.assertFalse(opponent.isCemeteryLocked());
	}
}
