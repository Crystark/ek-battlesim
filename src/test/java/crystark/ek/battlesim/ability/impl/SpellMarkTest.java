package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class SpellMarkTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Spell marked guy, 1, 1, 1, 1000, Special");
		Player opponent = PlayerWithAccess.withCardsOnField("Spell Mark guy, 1, 1, 1, 1000, Special, Spell Mark:50");
		Player.salute(player, opponent);

		Card c1 = player.atIndexOnField(0);

		opponent.atIndexOnField(0).applyPowerAbilities();

		Assert.assertEquals(1000, (int) c1.getCurrentHp());

		c1.damaged(null, 200, DamageType.burn);
		Assert.assertEquals(800, (int) c1.getCurrentHp());

		c1.damaged(null, 200, DamageType.ice);
		Assert.assertEquals(500, (int) c1.getCurrentHp());
	}
}
