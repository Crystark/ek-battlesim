package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class WordOfWitheringTest {

	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Word of Withering Guy, 1, 1, 100, 1000, Special, Word of Withering:10");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Random Guy, 1, 1, 1000, 3000, Special");
		Player.salute(player, opponent);

		Card card = player.atIndexOnField(0);
		Card opponentCard = opponent.atIndexOnField(0);

		Assert.assertEquals(1000, opponentCard.getCurrentAtk());
		Assert.assertEquals(1000, opponentCard.getCurrentBaseAtk());
		Assert.assertEquals(3000, opponentCard.getCurrentHp());
		Assert.assertEquals(3000, opponentCard.getCurrentBaseHp());

		card.applyPowerAbilities();

		Assert.assertEquals(900, opponentCard.getCurrentAtk());
		Assert.assertEquals(900, opponentCard.getCurrentBaseAtk());
		Assert.assertEquals(2700, opponentCard.getCurrentHp());
		Assert.assertEquals(2700, opponentCard.getCurrentBaseHp());
	}
}
