package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class DianasProtectionTest {
	@Test
	public void test_apply() {
		DianasProtection ab = new DianasProtection(40);
		Player player = PlayerWithAccess
			.withCardsOnField(
				"Immunity guy, 1, 1, 1, 1000, Special, Immunity",
				"Some guy, 1, 1, 1, 1000, Special",
				"Some guy, 1, 1, 1, 1000, Special",
				"Some guy, 1, 1, 1, 1000, Special");

		player.consumeCardsField(c -> c.damaged(null, 600, DamageType.unavoidable));
		ab.dianasProtection(player);

		Assert.assertEquals(800, player.atIndexOnField(0).getCurrentHp());
		Assert.assertEquals(800, player.atIndexOnField(1).getCurrentHp());
		Assert.assertEquals(800, player.atIndexOnField(2).getCurrentHp());
		Assert.assertEquals(800, player.atIndexOnField(3).getCurrentHp());
	}
}
