package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Cards;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class ManaCorruptionTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Plague Ogryn");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Dark Titan");
		Player.salute(player, opponent);

		Card mcg = Cards.from("Mana corruption Guy, 10, 2, 600, 1500, Special, Mana corruption:160", player);
		ManaCorruption mc = (ManaCorruption) mcg.getPowerAbilities().get(0);
		mc.apply(mcg);

		Card opponentCard = opponent.pickCardInField();
		Assert.assertEquals(opponentCard.desc.health - 480, (int) opponentCard.getCurrentHp());
	}
}
