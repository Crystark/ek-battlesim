package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class CaesarsStrikeTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess
			.withCardsOnField(
				"Legionary, 1, 1, 10, 1, Special",
				"Ceasar, 1, 1, 1, 1, Special, Caesar's Strike:50",
				"Centurion, 1, 1, 100, 1, Special",
				"Roman God, 1, 1, 1000, 1, Special");

		Card card = player.atIndexOnField(1);
		Assert.assertEquals(1, card.getCurrentAtk());

		card.applyBoosts();
		Assert.assertEquals(56, card.getCurrentAtk());
	}

	@Test
	public void test_apply_alone() {
		Player player = PlayerWithAccess.withCardsOnField("Ceasar, 1, 1, 1, 1, Special, Caesar's Strike:50");

		Card card = player.atIndexOnField(0);
		Assert.assertEquals(1, card.getCurrentAtk());

		card.applyBoosts();
		Assert.assertEquals(1, card.getCurrentAtk());
	}
}
