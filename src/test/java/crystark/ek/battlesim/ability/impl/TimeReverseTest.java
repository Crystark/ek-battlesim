package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Cards;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

/**
 * @see TimeReverse
 */
public class TimeReverseTest {
	@Test
	public void test_apply() {
		PlayerWithAccess player = PlayerWithAccess
			.withCardsOnField(
				"Immune Guy, 1, 1, 1, 1, Special, Immunity",
				"Resistance Guy, 1, 1, 1, 1, Special, Resistance",
				"Random Guy, 1, 1, 1, 1, Special")
			.withCardsInHand(
				"Time Reverse Guy, 1, 0, 1, 1, Special, Time Reverse, Resurrection:100",
				"Immune Hand Guy, 1, 1, 1, 1, Special, Immunity");

		PlayerWithAccess opponent = PlayerWithAccess
			.withCardsOnField(
				"Immune Guy, 1, 1, 1, 1, Special, Immunity",
				"Resistance Guy, 1, 1, 1, 1, Special, Resistance",
				"Random Guy, 1, 1, 1, 1, Special")
			.withCardsInHand("Immune Hand Guy, 1, 1, 1, 1, Special, Immunity");

		Player.salute(player, opponent);

		Assert.assertEquals(3, player.countCardsInField());
		Assert.assertEquals(3, opponent.countCardsInField());
		Assert.assertEquals(2, player.countCardsInHand());
		Assert.assertEquals(1, opponent.countCardsInHand());

		player.playCardsFromHand();

		Assert.assertEquals(1, player.countCardsInField());
		Assert.assertEquals(0, opponent.countCardsInField());
		Assert.assertEquals(0, player.countCardsInHand());
		Assert.assertEquals(0, opponent.countCardsInHand());

		player.cleanup();
		player.destroyCard(player.atIndexOnField(0));
		Cards.from("Random Guy, 1, 1, 1, 1, Special", player).addToField();
		player.playCardsFromHand();

		Assert.assertEquals(2, player.countCardsInField());
		Assert.assertEquals(0, opponent.countCardsInField());
		Assert.assertEquals(0, player.countCardsInHand());
		Assert.assertEquals(0, opponent.countCardsInHand());
	}
}
