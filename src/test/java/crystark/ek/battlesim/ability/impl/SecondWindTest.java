package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.ability.addons.Summon;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

/**
 * @see SecondWind
 */
public class SecondWindTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess
			.withCardsOnField("Second wind Guy, 1, 1, 1, 1, Special, Second Wind");

		Card testCard = player.atIndexOnField(0);

		Summon ab = (Summon) testCard.getDesperationAbilities().get(0);

		Assert.assertEquals(1, player.countCardsInField());
		ab.apply(testCard);
		Assert.assertEquals(2, player.countCardsInField());
		ab.apply(testCard);
		Assert.assertEquals(2, player.countCardsInField());
	}
}
