package crystark.ek.battlesim.ability.impl;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import crystark.common.api.AbilityDesc;
import crystark.common.format.AbilityDescFormat;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class ViolentStormTest {

	@Test
	public void test_parse() {
		AbilityDesc ad;
		ad = AbilityDescFormat.parse("Violent Storm");

		Assert.assertEquals(700, ad.value);
		Assert.assertEquals(2000, ad.value2);

		ad = AbilityDescFormat.parse("Violent Storm:300:1300");

		Assert.assertEquals(300, ad.value);
		Assert.assertEquals(1300, ad.value2);

		try {
			AbilityDescFormat.parse("Violent Storm:300");
			Assert.fail("Should not be parsed");
		}
		catch (Throwable t) {
			// success
		}
	}

	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Gilgamesh, 1, 1, 100, 1, Special, Violent Storm");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Random Guy, 1, 1, 1, 3000, Special");
		Player.salute(player, opponent);

		Card testCard = player.atIndexOnField(0);

		Assert.assertEquals(100, testCard.getCurrentAtk());
		testCard.applyBoosts();

		Assert.assertThat(testCard.getCurrentAtk(), Matchers.both(Matchers.greaterThanOrEqualTo(800)).and(Matchers.lessThanOrEqualTo(2100)));
	}
}
