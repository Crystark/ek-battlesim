package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class ManaBurnTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Mana Burn Guy, 10, 2, 600, 1500, Special, Mana Burn:160");
		PlayerWithAccess opponent = PlayerWithAccess
			.withCardsOnField(
				"Immune Guy, 1, 1, 1, 1000, Special, Immunity",
				"Reflection Guy, 1, 1, 1, 1000, Special, Reflection:150",
				"Random Guy, 1, 1, 1, 1000, Special");
		Player.salute(player, opponent);

		Card mcg = player.atIndexOnField(0);
		mcg.getPowerAbilities().get(0).apply(mcg);

		Assert.assertEquals(520, (int) opponent.atIndexOnField(0).getCurrentHp());
		Assert.assertEquals(520, (int) opponent.atIndexOnField(1).getCurrentHp());
		Assert.assertEquals(840, (int) opponent.atIndexOnField(2).getCurrentHp());
	}
}
