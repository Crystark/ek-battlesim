package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

public class ChargingTest {
	@Test
	public void test_apply() {
		PlayerWithAccess player = PlayerWithAccess
			.withoutCards()
			.withCardsInHand("Charging Guy, 1, 0, 0, 1, Special, Charging")
			.withRunes("Leaf");

		PlayerWithAccess opponent = PlayerWithAccess
			.withCardsOnField("Snipable Guy, 1, 1, 1, 2000, Special");

		Player.salute(player, opponent);

		Card sniped = opponent.atIndexOnField(0);

		// Activate the rune 4 times
		player.handleRunes(20);
		player.handleRunes(20);
		player.handleRunes(20);
		player.handleRunes(20);
		Assert.assertEquals("Should have hit 4 times", 1040, sniped.getCurrentHp());

		player.handleRunes(20);
		Assert.assertEquals("Should not work", 1040, sniped.getCurrentHp());

		player.playCardsFromHand();
		player.playCardsFromField(); // Should activate Charging
		player.handleRunes(20);
		Assert.assertEquals(800, sniped.getCurrentHp());
	}
}
