package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Cards;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

/**
 * @see PowerSource
 */
public class PowerSourceTest {
	@Test
	public void test_apply() {
		PlayerWithAccess player = PlayerWithAccess
			.withCardsOnField("Scarab");

		Card psg = Cards.from("Power Source Guy, 10, 2, 600, 1500, Special, power source:160", player);
		PowerSource ps = (PowerSource) psg.getAuraAbilities().get(0);
		Card playerCard = player.pickCardInField();
		ps.onEnterField(psg);

		Assert.assertEquals(playerCard.desc.attack + 160, playerCard.getCurrentBaseAtk());
	}
}
