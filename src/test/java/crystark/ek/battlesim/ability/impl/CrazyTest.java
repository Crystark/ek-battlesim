package crystark.ek.battlesim.ability.impl;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;
import org.junit.Assert;
import org.junit.Test;

public class CrazyTest {

	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Crazy Guy, 10, 2, 600, 1500, Special, Crazy");
		PlayerWithAccess opponent = PlayerWithAccess
			.withCardsOnField(
				"Some Guy, 1, 1, 100, 1000, Special",
				"Some Guy, 1, 1, 100, 1000, Special, Dodge:100",
				"Some Guy, 1, 1, 100, 1000, Special, Ice Shield:10");
		Player.salute(player, opponent);

		Crazy crazy = (Crazy) player.atIndexOnField(0).getPowerAbilities().get(0);

		Card first = opponent.atIndexOnField(0);
		Card second = opponent.atIndexOnField(1);
		Card third = opponent.atIndexOnField(2);

		Crazy.crazy(first, 100);
		Assert.assertEquals(1000, (int) first.getCurrentHp());
		Assert.assertEquals(900, (int) second.getCurrentHp());
		Assert.assertEquals(1000, (int) third.getCurrentHp());

		Crazy.crazy(second, 100);
		Assert.assertEquals(900, (int) first.getCurrentHp());
		Assert.assertEquals(900, (int) second.getCurrentHp());
		Assert.assertEquals(900, (int) third.getCurrentHp());

		Crazy.crazy(third, 100);
		Assert.assertEquals(900, (int) first.getCurrentHp());
		Assert.assertEquals(800, (int) second.getCurrentHp());
		Assert.assertEquals(900, (int) third.getCurrentHp());
	}
}
