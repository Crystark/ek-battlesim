package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

/**
 * @see DreadRoar
 */
public class DreadRoarTest {

	@Test
	public void test_dreadRoar_withWarpathAndAura() {
		DreadRoar ab = new DreadRoar();
		Player player = PlayerWithAccess
			.withCardsOnField(
				"Terrorised guy, 10, 2, 600, 1500, Special, Warpath:100",
				"Buffing guy, 10, 2, 1, 1, Special, Power Source:200");
		Player opponent = PlayerWithAccess.withCardsOnField("Plague Ogryn");
		Player.salute(player, opponent);

		Card terrorised = player.atIndexOnField(0);
		Assert.assertEquals(800, terrorised.getCurrentAtk());

		ab.dreadRoar(player);
		terrorised.applyBoosts(); // Warpath + Terrorized
		Assert.assertEquals(1200, terrorised.getCurrentAtk());
		terrorised.removeBoosts();
		Assert.assertEquals(800, terrorised.getCurrentAtk());

		terrorised.markSilenced(); // should work even if silenced
		terrorised.applyBoosts(); // only Terrorized
		Assert.assertEquals(400, terrorised.getCurrentAtk());
		terrorised.removeBoosts();
		Assert.assertEquals(800, terrorised.getCurrentAtk());
	}

	@Test
	public void test_dreadRoar_0ATK() {
		Player player = PlayerWithAccess.withCardsOnField("Dread Roar Guy, 0, 0, 1, 1, Special, Dread Roar");
		Player opponent = PlayerWithAccess.withCardsOnField("Terrorised guy, 0, 0, 0, 1, Special");
		Player.salute(player, opponent);

		Card card = player.atIndexOnField(0);
		Card terrorised = opponent.atIndexOnField(0);
		Assert.assertEquals(0, terrorised.getCurrentAtk());

		card.applyPowerAbilities();
		Assert.assertEquals(0, terrorised.getCurrentAtk());

		terrorised.applyBoosts();
		Assert.assertEquals(0, terrorised.getCurrentAtk());
	}

	@Test
	public void test_dreadRoar_1ATK() {
		Player player = PlayerWithAccess.withCardsOnField("Dread Roar Guy, 0, 0, 1, 1, Special, Dread Roar");
		Player opponent = PlayerWithAccess.withCardsOnField("Terrorised guy, 0, 0, 1, 1, Special");
		Player.salute(player, opponent);

		Card card = player.atIndexOnField(0);
		Card terrorised = opponent.atIndexOnField(0);
		Assert.assertEquals(1, terrorised.getCurrentAtk());

		card.applyPowerAbilities();
		Assert.assertEquals(1, terrorised.getCurrentAtk());

		terrorised.applyBoosts();
		Assert.assertEquals(0, terrorised.getCurrentAtk());
	}
}
