package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class ExecutionTest {

	@Test
	public void test_apply() {
		Player player = PlayerWithAccess.withCardsOnField("Execution Guy, 1, 1, 100, 1, Special, Execution");
		PlayerWithAccess opponent = PlayerWithAccess.withCardsOnField("Random Guy, 1, 1, 1, 1000, Special");
		Player.salute(player, opponent);

		Card opponentCard = opponent.atIndexOnField(0);
		opponentCard.damaged(null, 650, DamageType.unavoidable);
		Assert.assertEquals(350, opponentCard.getCurrentHp());
		Assert.assertTrue(opponentCard.isAlive());

		player.playCardsFromField();
		Assert.assertTrue(opponentCard.isAlive());

		player.playCardsFromField();
		Assert.assertFalse(opponentCard.isAlive());
	}
}
