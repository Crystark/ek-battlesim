package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class NeverEndingTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess
			.withCardsOnField("Never Ending Guy, 1, 1, 1, 1, Special, Never Ending");

		Assert.assertEquals(1, player.countCardsInField());
		player.atIndexOnField(0).damaged(null, 1, DamageType.unavoidable);
		Assert.assertEquals(1, player.countCardsInCemetery());
		Assert.assertEquals(1, player.countCardsInField());
	}
}
