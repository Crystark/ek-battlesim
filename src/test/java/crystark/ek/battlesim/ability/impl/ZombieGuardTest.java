package crystark.ek.battlesim.ability.impl;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class ZombieGuardTest {
	@Test
	public void test_apply() {
		Player player = PlayerWithAccess
			.withCardsOnField("Zombie Guard Guy, 1, 1, 1, 1, Special, Zombie Guard");

		Assert.assertEquals(2, player.countCardsInField());
	}
}
