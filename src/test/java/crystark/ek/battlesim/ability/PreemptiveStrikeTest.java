package crystark.ek.battlesim.ability;

import org.junit.Assert;
import org.junit.Test;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.PlayerWithAccess;

public class PreemptiveStrikeTest {
	@Test
	public void test() {
		Player player = PlayerWithAccess
			.withCardsOnField("Injured Guy, 1, 1, 0, 1000, Special")
			.withCardsInHand("PS Regen Guy, 1, 1, 0, 1000, Special, PS_Regeneration:250");

		Card card = player.atIndexOnField(0);
		card.damaged(null, 500, DamageType.unavoidable);

		Assert.assertEquals(500, card.getCurrentHp());

		player.preemptiveStrikes();

		Assert.assertEquals(750, card.getCurrentHp());
	}
}
