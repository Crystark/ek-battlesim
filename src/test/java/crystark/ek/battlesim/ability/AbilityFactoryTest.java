package crystark.ek.battlesim.ability;

import crystark.common.api.AbilityDesc;
import crystark.common.api.CardType;
import crystark.common.api.Game;
import crystark.common.api.LaunchType;
import crystark.common.api.NamesConverter;
import crystark.common.db.Database;
import crystark.common.db.model.BaseSkill;
import crystark.common.format.AbilityDescFormat;
import crystark.ek.EkbsConfig;
import crystark.ek.battlesim.ability.addons.AbilityProvider;
import crystark.ek.battlesim.ability.addons.Judgement;
import crystark.ek.battlesim.ability.addons.Summon;
import crystark.ek.battlesim.ability.impl.DemonFire;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.reflections.Reflections;
import rx.exceptions.OnErrorThrowable;

import java.util.AbstractMap.SimpleEntry;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class AbilityFactoryTest {
	private static Map<String, Class<? extends IAbility>> abilities;

	private AbilityDesc makeAbilityDesc(String name, int valueType) {
		Object value = 1;
		Object value2 = null;
		if (name.equals(Summon.NAME) || valueType == 1) {
			value = "scarab";
		}
		if (name.equals(AbilityProvider.NAME) || valueType == 2) {
			value = "Snipe:10";
		}
		if (valueType == 3) {
			value = "Snipe:10";
			value2 = "scarab";
		}
		else if (valueType == 4) {
			value2 = 1;
		}
		else if (valueType == 5) {
			value = CardType.tundra;
			value2 = "Snipe:10";
		}
		else if (valueType == 6) {
			value = 2;
			value2 = "500_1000_200";
		}
		else if (valueType == 7) {
			value = "1001_1_1";
			//value2 = "276_176"; //276 - Snipe 1; 176 - Fire Wall 1
			value2 = "Snipe 1 & Fire Wall 1";
		}
		return new AbilityDesc(name, name, name, LaunchType.normal, value, value2, false,0);
	}

	@BeforeClass
	public static void beforeClass() {
		final NamesConverter converter = EkbsConfig.current().namesConverter();
		abilities = new Reflections(IAbility.class.getPackage().getName()).getSubTypesOf(IAbility.class).stream()
			.filter(AbilityFactory::isBaseAbility)
			.map(clazz -> {
				try {
					String name = clazz.getField("NAME").get(null).toString();
					name = converter.skillToAlias(name);
					return new SimpleEntry<String, Class<? extends IAbility>>(name, clazz);
				}
				catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
					throw OnErrorThrowable.from(e);
				}
			})
			.collect(Collectors.toMap(SimpleEntry::getKey, SimpleEntry::getValue, (t, t2) -> t, TreeMap::new));
	}

	@Test
	public void test_allAbilitiesInFactory() {
		try {
			for (Entry<String, Class<? extends IAbility>> entry : abilities.entrySet()) {

				AbilityDesc abilityDesc = null;

				String name = entry.getKey();
				final Class<? extends IAbility> ability = entry.getValue();

				int valueType = 0;

				// Judgement is a CompositeSkill so has to been checked first
				if (Judgement.class.isAssignableFrom(ability)) {
					valueType = 7;
				}
				else if (CompositeSkill.class.isAssignableFrom(ability)) {
					valueType = 2;
				}
				else if (CardboundAbility.class.isAssignableFrom(ability)) {
					valueType = 3;
				}
				else if (AwakingAbility.class.isAssignableFrom(ability)) {
					valueType = 5;
				}
				else if (DemonFire.class.isAssignableFrom(ability)) {
					valueType = 6;
				}
				else if (!name.equals(AbilityProvider.NAME) && !name.equals(Summon.NAME) ){
					final List<BaseSkill> skills = Database.get().skills().getByAbilityName(name);
					if (skills == null && EkbsConfig.current().game() != Game.mr) continue; // in LoA mode some skill absent in db
					Assert.assertNotNull(name + " is absent in database", skills);
					BaseSkill skill = skills.get(0);
					abilityDesc = skill.createAbilityDesc();
				}

				if (abilityDesc == null) makeAbilityDesc(name, valueType);
			}
		}
		catch (IllegalArgumentException e) {
			for (Class<? extends IAbility> ab : abilities.values()) {
				String name = ab.getSimpleName();
				System.out.println("MAPPING.put(" + name + ".NAME, " + name + "::new);");
			}
			throw e;
		}
	}

	@Test
	public void test_missingAbilities() {
		AtomicBoolean failed = new AtomicBoolean();

		AbilityFactory.supportedSkills(false)
			.filter(skill -> !skill.isUseless() && !skill.isSkillProvider())
			.forEach(skill -> {
				failed.set(true);
				System.out.println("Unimplemented ability: " + skill.cleanName() + " - " + skill.desc());
			});

		// Assert.assertFalse("Some abilities aren't implemented", failed.create());
	}

	@Test
	public void test_allAbilitiesInDb() {
		Set<String> ignored = new HashSet<>();
		ignored.add(AbilityProvider.NAME);
		StringBuilder sb = new StringBuilder();

		for (String name : abilities.keySet()) {
			if (ignored.contains(name)) {
				continue;
			}
			List<BaseSkill> ability = Database.get().skills().getByAbilityName(name);
			if (ability == null) sb.append(name + "\n");
			//Assert.assertNotNull(name + " not found in DB", ability);
		}
		Assert.assertTrue("Skills not found in DB:\n" + sb, sb.length() <= 0);
	}

	@Test
	public void test_compositeResurrection() {
		final Supplier<IAbility> supplier = AbilityFactory.getSupplier(AbilityDescFormat.parse("Resurrection X:Resurrection 8 & Dexterity 5"));
		IAbility a = supplier.get();
		Assert.assertTrue(a instanceof CompositeSkill);
	}
}
