package crystark.ek.cli;

import java.util.Arrays;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

public class BootOptionsTest {
	@Test
	public void test_npb_after_hydra() {
		String[] args = { "-dk", "deck", "-hy", "-npb" };
		BootOptions bo = new BootOptions(args);

		Assert.assertFalse(bo.showProgessBar());

		String[] hydras = bo.getHydras();
		Assert.assertEquals(0, hydras.length);
	}

	@Test
	public void test_hydra_all() {
		String[] args = { "-dk", "deck", "-hy" };
		BootOptions bo = new BootOptions(args);

		String[] hydras = bo.getHydras();
		Assert.assertEquals(0, hydras.length);
	}

	@Test
	public void test_hydra_some() {
		String[] args = { "-dk", "deck", "-hy", "hydra i", "-hy", "hydra ii curse" };
		BootOptions bo = new BootOptions(args);

		String[] hydras = bo.getHydras();
		Assert.assertEquals(2, hydras.length);
		Assert.assertThat(Arrays.asList(hydras), Matchers.contains("hydra i", "hydra ii curse"));
	}

	@Test
	public void test_kw_some() {
		String[] args = { "-dk", "deck", "-kw", "guard1", "-kw", "guard2" };
		BootOptions bo = new BootOptions(args);

		String[] guards = bo.getKWGuards();
		Assert.assertEquals(2, guards.length);
		Assert.assertThat(Arrays.asList(guards), Matchers.contains("guard1", "guard2"));
	}

	@Test
	public void test_versus() {
		String[] args = { "-dk", "deck", "-vs", "file1", "-vs", "file2" };
		BootOptions bo = new BootOptions(args);

		String[] versus = bo.getVersusFiles();
		Assert.assertEquals(2, versus.length);
		Assert.assertThat(Arrays.asList(versus), Matchers.contains("file1", "file2"));
	}
}
