package crystark.tools;

import crystark.common.api.Game;
import crystark.common.db.Database;
import crystark.common.db.model.BaseSkill;
import crystark.common.db.model.ek.EkSkill;
import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class GameSkillsAliasHelper {

	@Test
	public void loaToMrSkills() {

		final Database mrDb = Database.create(Game.mr, true);
		final Database loaDb = Database.create(Game.hf, true);
		final LinkedHashMap<String, String> notFound = new LinkedHashMap<>();
		loaDb.skills().allByAbilityName()
			.map(entry -> entry.getValue().get(0))
			.filter(skill -> !skill.isUseless() && !skill.isSkillProvider() && !skill.isGeneric())
			.forEach(skill -> {
				BaseSkill equalSkill = null;
				int maxIdx = 0;

				final List<BaseSkill> mrSkills = mrDb.skills().getByAbilityAffectType(skill.AffectType());
				for (BaseSkill mrSkill : mrSkills) {
					final int idx = closeIndex(skill, mrSkill);
					if (idx > maxIdx) {
						maxIdx = idx;
						equalSkill = mrSkill;
					}
				}
				//if (equalSkill == null && mrSkills.size() > 0) equalSkill = mrSkills.get(0);

				if (equalSkill != null && !skill.abilityName().equalsIgnoreCase(equalSkill.abilityName())) {
					System.out.println(skill.abilityName() + " = " + equalSkill.abilityName() + " | " + skill.desc() + " | " + equalSkill.desc());
				}
				else if (equalSkill == null) {
					//System.out.println(skill.cleanName() + " | " + skill.desc());
					notFound.put(skill.name(), skill.desc());
				}

			});
		System.out.println();
		System.out.println("======== Equal skill not found for: ================");
		for (Map.Entry<String, String> entry : notFound.entrySet()) {
			System.out.print(entry.getKey());
			System.out.print('|');
			System.out.println(entry.getValue());
		}

	}

	private int closeIndex(BaseSkill skill, BaseSkill mrSkill) {
		int idx = 100;
		/*if (skill.launchType() == mrSkill.launchType()) {
			idx += 10;
		}*/
		try {
			final EkSkill dbSkill = skill.getSkill();
			final EkSkill dbMrSkill = mrSkill.getSkill();

			if (skill.value() instanceof Integer && mrSkill.value() instanceof Integer){
				if ((int) skill.value() > 0 ^ (int) mrSkill.value() > 0) idx -= 5;
			} else if (dbSkill.AffectValue.equals(dbMrSkill.AffectValue)){
				idx += 10;
			}

			if (skill.value() instanceof Integer ^ mrSkill.value() instanceof Integer){
				idx -= 25;
			}

			if (skill.value2() instanceof Integer && mrSkill.value2() instanceof Integer){
				if ((int) skill.value2() > 0 ^ (int) mrSkill.value2() > 0) idx -= 5;
			} else if (dbSkill.AffectValue2.equals(dbMrSkill.AffectValue2)){
				idx += 10;
			}

			if (!dbSkill.AffectValue2.equals(skill.value()) && dbSkill.AffectValue2.equals(dbMrSkill.AffectValue2)){
				idx += 5;
			}

			if (skill.value2() instanceof Integer ^ mrSkill.value2() instanceof Integer){
				idx -= 25;
			}

			if (Objects.equals(skill.launchCondition(), mrSkill.launchCondition())) {
				idx += 10;
			}
			if (Objects.equals(skill.value(), mrSkill.value())) {
				idx += 20;
			}
			if (Objects.equals(skill.value2(), mrSkill.value2())) {
				idx += 20;
			}
		}
		catch (NumberFormatException e) {
		}
		return idx;
	}
}
