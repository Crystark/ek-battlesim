package crystark.tools;

import org.junit.Assert;
import org.junit.Test;

public class ToolsTest {
	@Test
	public void test_distinctRandoms() {

		Assert.assertEquals(0, Tools.distinctRandoms(0, 10).length);
		Assert.assertEquals(2, Tools.distinctRandoms(2, 10).length);
		Assert.assertEquals(5, Tools.distinctRandoms(5, 10).length);
		Assert.assertEquals(10, Tools.distinctRandoms(10, 10).length);
		Assert.assertEquals(10, Tools.distinctRandoms(15, 10).length);
	}

	@Test
	public void test_normalizeStringForFile() {
		Assert.assertEquals("_HB_", Tools.normalizeStringForFile(":HB:"));
	}
}
