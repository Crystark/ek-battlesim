package crystark.tools;

import java.util.function.Predicate;

import org.junit.Assert;
import org.junit.Test;

public class CustomPredicatesTest {

	@Test
	public void test_nthCallPredicate() {
		Predicate<?> predicate = CustomPredicates.nthCall(0, 1, 3);

		Assert.assertTrue(predicate.test(null));
		Assert.assertTrue(predicate.test(null));
		Assert.assertFalse(predicate.test(null));
		Assert.assertTrue(predicate.test(null));
		Assert.assertFalse(predicate.test(null));
		Assert.assertFalse(predicate.test(null));
	}
}
