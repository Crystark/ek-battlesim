package crystark;

import crystark.ek.battlesim.ability.AbilityFactoryTest;
import crystark.ek.battlesim.battle.CardTest;
import crystark.ek.battlesim.battle.GameCardsImplTest;
import crystark.ek.battlesim.rune.RuneFactoryTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ ResourcesTest.class, AbilityFactoryTest.class, RuneFactoryTest.class, GameCardsImplTest.class, CardTest.class })
public class MainTests {}
