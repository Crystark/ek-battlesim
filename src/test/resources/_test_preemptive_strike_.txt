deck name: Test PS
player name: Test PS
player level: 130

# Cards
Flamereaper, 21, 1, 1070, 1050, mtn, Dying Strike, Last Chance, Resurrection:70, Trap:2
Vulcan, 21, 4, 800, 2075, mtn, Cerberus, Apocalypse, Double Wings, Reflection:150
Cancer, 21, 4, 2250, 30000, tundra, Cancer, Firestorm:150
Flamereaper, 21, 1, 1070, 1050, mtn, Dying Strike, Last Chance, Resurrection:70, QS_Exile
Keeper of the light, 20, 2, 807, 1820, tundra, Resurrection:55, Frost Shock:140:55, QS_Teleportation, QS_Exile
The Fury, 20, 4, 264, 5420, mtn, Road of Ashes, Eruption, Land of Lava, Resurrection:55
Aries, 21, 4, 870, 2020, tundra, Ice Shield:110, Purification, Immunity, Trap:2
The Horned King, 18, 3, 875, 2426, forest, Trap:5, Life Link, Silence, QS_Exile
Death Knight, 21, 4, 740, 1875, swamp, Shield of Earth, Resurrection:60, Reanimation, Immunity
The Flying Dutchman, 21, 99, 850, 2450, Tundra, PS_Salvo 10, PS_Reincarnation 2, Resistance, Reflection 5

# Runes
Blight Stone
Blood Stone
Ghost Step
Burning Soul
