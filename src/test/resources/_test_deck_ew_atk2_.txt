Player name: EW Atk
Player level: 138

# Deck:
	Celestial Peacock, 12, Thunderbolt 4
	Celestial Touchstone, 12+3, Fire God 5
	Cloris
	Demon's Heart
	Dragon Summoner, 15, Summon Dragon II
	Evil Mantis, 15, Bloodthirsty 7
	Gemini, 15, Paradox
	Goddess of Order, 15, Prayer 10
	Moon Druid, 5
	Taiga Cleric, 15, Prayer 7
# Runes:
	Red Valley:90
	Thunder Shield:200
	Spring Breeze:240
	Leaf:240
