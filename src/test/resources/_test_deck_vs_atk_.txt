Player level: 110

Whitewing Pope,        18, 4, 714,  1830,      tundra, Blizzard:160,          Reincarnation:1,       Ice Shield:150,  Resistance
Balrog Sentry,         16, 2, 656,  1392,      mtn,    QS_Teleportation,      Evasion,               Resurrection:65, Electric Shock:150
Lava Destroyer,        20, 1, 780,  1730,      mtn,    Guard,                 Resurrection:60,       D_Reanimation,   Snipe:180
Ancient One,           18, 4, 745,  1980,      tundra, Clean Sweep,           Infiltrator,           Ice Shield:120,  Resistance
Ancient One,           18, 4, 745,  1980,      tundra, Clean Sweep,           Infiltrator,           Ice Shield:120,  Trap:2
Skeleton King,         15, 2, 855,  1395,      mtn,    Backstab:320,          Snipe:270,             Resurrection:65, Resistance
Arctic Lich,           16, 4, 765,  1860,      tundra, Mana Corruption:160,   Sacred Flame,          Resistance,      Firestorm:150
Nuriel,                16, 2, 820,  1890,      mtn,    Mountain Guard:400,    Magic Shield:80,       Impede:1, 	      Resurrection:55
Tundra Rider,          16, 3, 685,  1780,      tundra, Blizzard:160,          Mana Corruption:180,   Ice Shield:100,  Trap:2

Ghost Step
Blood Stone
Permafrost
Clear Spring
