package crystark.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.BiFunction;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import crystark.common.db.Database;
import crystark.ek.Constants;
import crystark.ek.battlesim.task.TaskException;

public class Tools {
	private static final Gson	G	= new GsonBuilder()
										.disableHtmlEscaping()
										.setPrettyPrinting()
										.create();
//	private static final Random	R	= new Random();
	private static final String	NL	= System.lineSeparator();

	public static String listPrettyPrint(List<? extends Object> l) {
		if (l == null || l.isEmpty())
			return "";

		StringBuilder sb = new StringBuilder();
		for (Object o : l) {
			sb.append('\t').append(o).append(NL);
		}
		sb.replace(sb.length() - NL.length(), sb.length(), ""); // remove trailing line separator
		return sb.toString();
	}

	public static String jsonPrettyPrint(String json) {
		JsonParser jp = new JsonParser();
		JsonElement je = jp.parse(json);
		return G.toJson(je);
	}

	public static String formatSecondsToMinutes(int durationInSeconds) {
		long remainingSeconds = Long.valueOf(durationInSeconds);

		long elapsedMinutes = remainingSeconds / 60;
		remainingSeconds = remainingSeconds % 60;

		return String.format("%02d:%02d", elapsedMinutes, remainingSeconds);
	}

	public static String float2String(Float f, int scale) {
		if (f == null) {
			return null;
		}
		return BigDecimal.valueOf(f).setScale(scale, RoundingMode.HALF_EVEN).toString();
	}

	//////////
	/// TOOLS
	//////////

	public static int[] distinctRandoms(int nb, int max) {
		if (nb == 1) {
			return new int[] { ThreadLocalRandom.current().nextInt(max) };
		}
		int[] all = new int[max];
		for (int i = 0; i < all.length; i++) {
			all[i] = i;
		}
		if (max <= nb) {
			return all;
		}
		int index;
		int[] result = new int[nb];

		for (int j = 0, k = all.length - 1; k > 0 && j < nb; k--, j++) {
			index = ThreadLocalRandom.current().nextInt(k + 1);
			result[j] = all[index]; // save element
			all[index] = all[k]; // overwrite chosen with last element
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static <T> ArrayList<T> newArrayList(T... elements) {
		ArrayList<T> al = new ArrayList<>();
		Collections.addAll(al, elements);
		return al;
	}

	public static <A, B, C> Stream<C> zip(Stream<A> streamA, Stream<B> streamB, BiFunction<A, B, C> zipper) {
		final Iterator<A> iteratorA = streamA.iterator();
		final Iterator<B> iteratorB = streamB.iterator();
		final Iterator<C> iteratorC = new Iterator<C>() {
			@Override
			public boolean hasNext() {
				return iteratorA.hasNext() && iteratorB.hasNext();
			}

			@Override
			public C next() {
				return zipper.apply(iteratorA.next(), iteratorB.next());
			}
		};
		final boolean parallel = streamA.isParallel() || streamB.isParallel();
		return iteratorToFiniteStream(iteratorC, parallel);
	}

	public static <T> Stream<T> iteratorToFiniteStream(Iterator<T> iterator, boolean parallel) {
		final Iterable<T> iterable = () -> iterator;
		return StreamSupport.stream(iterable.spliterator(), parallel);
	}

	public static void activateDebug() {
		Constants.IS_DEBUG = true;
		org.apache.log4j.LogManager.getRootLogger().setLevel(org.apache.log4j.Level.DEBUG);
	}

	public static <T> T compareToValue(int compareResult, T positive, T negative, T equals) {
		if (compareResult == 0) {
			return equals;
		}
		else if (compareResult > 0) {
			return positive;
		}
		else {
			return negative;
		}
	}

	public static int parseInt(final String s) {
		if (s == null || s.isEmpty())
			throw new NumberFormatException("Missing digits");

		// Check for a sign.
		int num = 0;
		int sign = -1;
		final int len = s.length();
		final char ch = s.charAt(0);
		if (ch == '-') {
			if (len == 1)
				throw new NumberFormatException("Missing digits:  " + s);
			sign = 1;
		}
		else {
			final int d = ch - '0';
			if (d < 0 || d > 9)
				throw new NumberFormatException("Malformed:  " + s);
			num = -d;
		}

		// Build the number.
		final int max = (sign == -1) ? -Integer.MAX_VALUE : Integer.MIN_VALUE;
		final int multmax = max / 10;
		int i = 1;
		while (i < len) {
			int d = s.charAt(i++) - '0';
			if (d < 0 || d > 9)
				throw new NumberFormatException("Malformed:  " + s);
			if (num < multmax)
				throw new NumberFormatException("Over/underflow:  " + s);
			num *= 10;
			if (num < (max + d))
				throw new NumberFormatException("Over/underflow:  " + s);
			num -= d;
		}

		return sign * num;
	}

	//////////
	/// FILES
	//////////

	public static String normalizeStringForFile(String str) {
		return str.replaceAll("[^a-zA-Z0-9.\\-\\[\\]() ]", "_");
	}

	public static Stream<File> listFiles(String... files) {
		return Stream
			.of(files)
			.map(File::new)
			.flatMap(Tools::listFiles);
	}

	public static Stream<File> listFiles(File f) {
		if (!f.exists()) {
			throw new TaskException("File not found: " + f.getPath());
		}
		if (f.isHidden()) {
			return Stream.empty();
		}
		if (f.isDirectory()) {
			File[] sub = f.listFiles();
			if (sub.length == 0) {
				throw new TaskException("Directory is empty: " + f.getPath());
			}
			return Stream.of(sub).flatMap(Tools::listFiles);
		}
		return Stream.of(f);
	}

	public static String file(String s) {
		final URL url = ClassLoader.getSystemResource(s);
		final String path;
		try {
			path = Paths.get(url.toURI()).toFile().getAbsolutePath();
			return path;
		}
		catch (URISyntaxException e) {
			throw new RuntimeException("Wrong file path: '" + s + '\'', e);
		}
	}

	/**
	 * InputStream to String
	 */
	public static String readStream(InputStream is) {
		try (Scanner scanner = new Scanner(is, StandardCharsets.UTF_8.name())) {
			Scanner delemited = scanner.useDelimiter("\\A");
			if (delemited.hasNext()) {
				return delemited.next();
			}
			return "";
		}
	}

	/**
	 * File to String
	 */
	public static String readResource(String path) {
		return readStream(ClassLoader.getSystemResourceAsStream(path));
	}

	/**
	 * File to String
	 */
	public static String readFile(File file) throws IOException {
		try (FileInputStream fis = new FileInputStream(file)) {
			return readStream(fis);
		}
	}

	/**
	 * File to String
	 */
	public static String readFile(String file) throws IOException {
		return readFile(new File(file));
	}

	/**
	 * File to reader
	 */
	public static Reader fileReader(File file) throws IOException {
		return new StringReader(readFile(file));
	}

	public static PrintStream filePrintStream(String filePath) {
		return filePrintStream(new File(filePath));
	}

	private static PrintStream filePrintStream(File file) {
		File parent = file.getParentFile();
		if (!parent.exists()) {
			parent.mkdirs();
		}
		try {
			return printStream(new FileOutputStream(file), false);
		}
		catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	private static PrintStream printStream(OutputStream os, boolean autoFlush) {
		try {
			return new PrintStream(os, autoFlush, "UTF-8");
		}
		catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * String to File
	 */
	public static void writeToFile(String path, String content) {
		writeToFile(new File(path), content);
	}

	/**
	 * String to File
	 */
	public static void writeToFile(File file, String content) {
		try (PrintStream ps = filePrintStream(file)) {
			ps.print(content.replaceAll("\\R", System.lineSeparator()));
		}
	}

	public static String getStartDir() {
		String exeDir = System.getProperty("l4j.exedir");
		if (exeDir != null) return exeDir; //exeDir = System.getProperty("user.dir");

		URL location = Database.class.getProtectionDomain().getCodeSource().getLocation();
		Path p;
		try {
			p = Paths.get(location.toURI());
			p = p.toRealPath();
		}
		catch (URISyntaxException | IOException e) {
			throw new RuntimeException(e);
		}
		File f;
		do {
			f = p.toFile();
			if (f.isDirectory() && Files.exists(p.resolve("db"))) {
				return f.getAbsolutePath();
			}
			p = p.getParent();
		} while (!f.isDirectory() && p != null);

		return "";
	}

	/**
	 * List directory contents for a resource folder. Not recursive.
	 * This is basically a brute-force implementation.
	 * Works for regular files and also JARs.
	 *
	 * @author Greg Briggs
	 * @param clazz Any java class that lives in the same place as the resources you want.
	 * @param path Should end with "/", but not start with one.
	 * @return Just the name of each member item, not the full paths.
	 * @throws URISyntaxException
	 * @throws IOException
	 */
	public static String[] getResourceListing(Class clazz, String path) {
		URL dirURL = clazz.getClassLoader().getResource(path);
		try {
			if (dirURL != null && dirURL.getProtocol().equals("file")) {
		/* A file path: easy enough */
				return new File(dirURL.toURI()).list();
			}

			if (dirURL == null) {
        /*
         * In case of a jar file, we can't actually find a directory.
         * Have to assume the same jar as clazz.
         */
				String me = clazz.getName().replace(".", "/") + ".class";
				dirURL = clazz.getClassLoader().getResource(me);
			}

			if (dirURL.getProtocol().equals("jar")) {
        /* A JAR path */
				String jarPath = dirURL.getPath().substring(5, dirURL.getPath().indexOf("!")); //strip out only the
				// JAR file

				JarFile jar = new JarFile(URLDecoder.decode(jarPath, "UTF-8"));
				Enumeration<JarEntry> entries = jar.entries(); //gives ALL entries in jar
				Set<String> result = new HashSet<String>(); //avoid duplicates in case it is a subdirectory
				while (entries.hasMoreElements()) {
					String name = entries.nextElement().getName();
					if (name.startsWith(path)) { //filter according to the path
						String entry = name.substring(path.length());
						int checkSubdir = entry.indexOf("/");
						if (checkSubdir >= 0) {
							// if it is a subdirectory, we just return the directory name
							entry = entry.substring(0, checkSubdir);
						}
						result.add(entry);
					}
				}
				return result.toArray(new String[result.size()]);
			}
		} catch (IOException | URISyntaxException e) {
			return new String[0];
		}
		throw new UnsupportedOperationException("Cannot list files for URL " + dirURL);
	}
}
