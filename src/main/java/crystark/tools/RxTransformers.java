package crystark.tools;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rx.Observable;
import rx.Observable.Transformer;

public class RxTransformers {
	private static final Logger L = LoggerFactory.getLogger(RxTransformers.class);

	public static <T> Transformer<T, T> induceLatency(long millis) {
		return (t) -> Observable.defer(() -> {
			try {
				L.warn("Induced latency of " + millis + "...");
				Thread.sleep(millis);
			}
			catch (Exception e) {
				throw new RuntimeException(e);
			}
			return t;
		});
	}

	public static <T> Transformer<T, T> pace(long delay, TimeUnit unit) {
		return o -> o
			.concatMap(i -> Observable.<T> empty()
				.delay(delay, unit)
				.startWith(i));
	}

	public static <T> Transformer<T, T> fullDebug(String mark) {
		String pattern = "[" + mark + "][%-15s]";
		return new Transformer<T, T>() {
			@Override
			public Observable<T> call(Observable<T> o) {
				return o
					.doOnSubscribe(() -> L.warn(String.format(pattern, "doOnSubscribe")))
					.doOnNext((t) -> L.warn(String.format(pattern, "donOnNext") + "- " + t))
					.doOnCompleted(() -> L.warn(String.format(pattern, "doOnCompleted")))
					.doOnError((t) -> L.warn(String.format(pattern, "doOnError") + "- ", t))
					.doOnUnsubscribe(() -> L.warn(String.format(pattern, "doOnUnsubscribe")));
			}
		};
	}
}
