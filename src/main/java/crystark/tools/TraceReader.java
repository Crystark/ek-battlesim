package crystark.tools;

import java.io.BufferedReader;
import java.io.File;
import java.util.stream.Stream;

import com.google.gson.Gson;

import crystark.common.db.model.ek.EkDto;

public class TraceReader {
	private static final Gson G = new Gson();

	public static void main(String[] args) {
		//		String path = "D:\\Google Drive\\PCSync\\ek-battlesim\\dodge70-new\\";
		String path = "E:\\Google Drive\\PCSync\\ek-battlesim\\cancer-dodge70-x2-trace.txt";

		Tools.listFiles(new File(path))
			//			.map(f -> {
			//				try {
			//					return Tools.inputStreamToString(Tools.fileInputStream(f));
			//				}
			//				catch (Exception e) {
			//					throw new RuntimeException(e);
			//				}
			//			})
			.flatMap(f -> {
				try {
					BufferedReader br = new BufferedReader(Tools.fileReader(f));
					return br.lines();
				}
				catch (Exception e) {
					System.out.println(e);
					return Stream.empty();
				}
			})
			//			.peek(t -> System.out.println(t))
			.map(s -> G.fromJson(s, EkDto.class))
			.forEach(dto -> {
				dto.data.Battle.stream()
					.filter(r -> !r.isAttack)
					.flatMap(r -> r.Opps.stream())
					.filter(o -> o.HP != null && o.Target.contains("atk_1"))
					.forEach(o -> {
					if (o.Value < 0) {
						System.out.print("hit\t");
					}
					else if (o.Value.equals(0)) {
						System.out.print("miss\t");
					}
					else {
						System.out.print("oth (" + o.Value + ")\t");
					}
				});
				System.out.println();
			});
		// to EkDto
	}
}
