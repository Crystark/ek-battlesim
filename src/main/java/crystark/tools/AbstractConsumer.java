package crystark.tools;

import java.util.function.Consumer;
import java.util.function.Predicate;

public abstract class AbstractConsumer<T> implements Consumer<T> {
	public Consumer<T> withPredicate(Predicate<T> predicate) {
		return c -> {if (predicate.test(c)) this.accept(c);};
	}
}
