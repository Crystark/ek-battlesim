package crystark.tools;

import rx.functions.Func1;

public class RxFilters {
	public static <T> Func1<T, Boolean> not(Func1<T, Boolean> filter) {
		return new RxNotFilter<T>(filter);
	}

	public static class RxNotFilter<T> implements Func1<T, Boolean> {
		private final Func1<T, Boolean> filter;

		RxNotFilter(Func1<T, Boolean> filter) {
			this.filter = filter;
		}

		@Override
		public Boolean call(T t) {
			return !this.filter.call(t);
		}
	}
}
