package crystark.tools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

public class CustomPredicates {


	@SuppressWarnings("unchecked")
	public static <T> Predicate<T> notNull() {
		//This leads threads to synch on simultaneous notNull call
		//private static final Predicate<?> NOT_NULL = Predicate.isEqual(null).negate();
		//return (Predicate<T>) NOT_NULL;

		return Objects::nonNull;
	}


	public static <T> Predicate<T> randomNAmongX(int n, int x) {
		return nthCall(Tools.distinctRandoms(n, x));
	}

	public static <T> Predicate<T> nthCall(int... calls) {
		return new NthCallPredicate(calls);
	}

	public static <T> Predicate<T> allOf(List<Predicate<T>> predicates) {
		return new AllOfPredicate<>(predicates);
	}


	private static class NthCallPredicate<T> implements Predicate<T> {
		private final int[]	calls;
		private int			callsIdx	= 0;
		private int			nCall		= 0;

		NthCallPredicate(int... calls) {
			this.calls = calls;
			Arrays.sort(this.calls);
		}

		@Override
		public boolean test(T input) {
			if (callsIdx < calls.length && calls[callsIdx] == nCall++) {
				callsIdx++;
				return true;
			}
			return false;
		}
	}

	public static class AllOfPredicate<T> implements Predicate<T> {
		private final List<Predicate<T>> predicates;

		AllOfPredicate(List<Predicate<T>> predicates) {
			this.predicates = new ArrayList<>(predicates);
		}

		@Override
		public boolean test(T input) {
			predicates.removeIf(o -> o.test(input));
			return predicates.isEmpty();
		}
	}
}
