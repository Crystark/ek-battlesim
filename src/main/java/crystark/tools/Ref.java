package crystark.tools;

/**
 *
 */
public class Ref<T> {
	private T referent;

	public Ref(T referent) {
		this.referent = referent;
	}

	public T get() {
		return this.referent;
	}

	public void set(T referent) {
		this.referent = referent;
	}

	public void clear() {
		this.referent = null;
	}


}
