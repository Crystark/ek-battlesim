package crystark.tools;

import java.util.function.Consumer;
import java.util.function.Predicate;

public class PredicateConsumer<T> implements Consumer<T> {
	private final java.util.function.Consumer<T>	delegate;
	private final Predicate<T>						predicate;

	public PredicateConsumer(java.util.function.Consumer<T> delegate, Predicate<T> predicate) {
		this.delegate = delegate;
		this.predicate = predicate;
	}

	/**
	 * Performs this operation on the given argument.
	 *
	 * @param t the input argument
	 */
	@Override
	public void accept(T t) {
		if (this.predicate.test(t)) {
			this.delegate.accept(t);
		}
	}
}