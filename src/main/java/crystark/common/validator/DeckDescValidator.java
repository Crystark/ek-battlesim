package crystark.common.validator;

import crystark.common.api.CardDesc;
import crystark.common.api.ConflictType;
import crystark.common.api.DeckDesc;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.task.TaskException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class DeckDescValidator implements IValidator<DeckDesc> {

	static final DeckDescValidator INSTANCE = new DeckDescValidator();

	public static void validate(DeckDesc desc) {
		INSTANCE.apply(desc);
	}

	@Override
	public void apply(DeckDesc desc) {
		// Strictly required
		if (desc.playerName == null) {
			throw new TaskException("Missing player name");
		}
		if (desc.playerLevel <= 0 && !desc.infiniteHp) {
			throw new TaskException("Missing player level");
		}
		if (desc.waitToPlay > 0 && (desc.waitToPlay < 2 || 5 < desc.waitToPlay)) {
			throw new TaskException("You can only wait to play from 2 to 5 cards at once.");
		}
		if (desc.cardsDescs.isEmpty()) {
			throw new TaskException("You must provide at least one card in your deck");
		}

		// Game-rules specifics
		if (!desc.bypassValidation) {
			if (desc.cardsDescs.size() > 10) {
				throw new TaskException("You cannot have more than 10 cards in your deck");
			}
			else {
				// Conflict
				List<String> conflictNames = desc.cardsDescs.stream()
					.filter(cd -> cd.conflictType != null && !ConflictType.none.equals(cd.conflictType))
					.collect(Collectors.groupingBy(cd -> cd.conflictType))
					.entrySet()
					.stream()
					.filter(e -> e.getValue().size() > 1)
					.limit(1)
					.flatMap(e -> e.getValue().stream())
					.map(cd -> cd.name)
					.collect(Collectors.toList());

				if (!conflictNames.isEmpty()) {
					throw new TaskException("Those cards conflict and cannot be played together: " + conflictNames);
				}

				// Limit
				Optional<CardDesc> limitedCard = desc.cardsDescs.stream()
					.filter(cd -> cd.limit != 0)
					.collect(Collectors.groupingBy(cd -> cd.name))
					.entrySet()
					.stream()
					.filter(e -> {
						List<CardDesc> v = e.getValue();
						return v.size() > v.get(0).limit;
					})
					.map(e -> e.getValue().get(0))
					.findFirst();

				if (limitedCard.isPresent()) {
					CardDesc card = limitedCard.get();
					throw new TaskException("You can only have " + card.limit + " " + card.name + " in your deck.");
				}
			}
			if (desc.runesDescs.size() > 4) {
				throw new TaskException("You cannot have more than 4 runes in your deck");
			}
			long distinctRunes = desc.runesDescs.stream()
				.map(r -> r.name)
				.distinct()
				.count();
			if (distinctRunes != desc.runesDescs.size()) {
				throw new TaskException("You cannot use the same rune twice");
			}
		}

		// TODO check runes and abilities beforehand
		Player.from(desc);
	}
}
