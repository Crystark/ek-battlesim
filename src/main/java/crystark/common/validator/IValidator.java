package crystark.common.validator;

import crystark.ek.battlesim.task.TaskException;

public interface IValidator<T> {
	public void apply(T t) throws TaskException;
}
