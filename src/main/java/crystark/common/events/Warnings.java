package crystark.common.events;

import rx.Observable;
import rx.subjects.ReplaySubject;
import rx.subjects.Subject;

public class Warnings {
	private static volatile Warnings			INSTANCE	= new Warnings();
	private Subject<String, String>	events = ReplaySubject.<String> createWithSize(100).toSerialized();

	public static Observable<String> getAndReset() {
		Observable<String> results = get().events.asObservable().onErrorResumeNext(t -> Observable.empty()).distinct();
		get().events.onCompleted();
		INSTANCE = new Warnings();
		return results;
	}

	public static void send(String warning) {
		get().events.onNext(warning);
	}
	
	private static Warnings get() {
		return INSTANCE;
	}
}
