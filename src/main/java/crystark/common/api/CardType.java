package crystark.common.api;

public enum CardType {
	tundra, mtn, swamp, forest, demonist, special, exp, gold, demon, hydra, unknown;

	public static final CardType fromRace(int race) {
		switch (race) {
			case 1:
				return tundra;
			case 2:
				return forest;
			case 3:
				return swamp;
			case 4:
				return mtn;
			case 5:
				return demonist;
			case 95:
				return gold;
			case 96:
				return exp;
			case 97:
				return hydra;
			case 99:
				return special;
			case 100:
				return demon;
			default:
//				throw new IllegalArgumentException("Unknown race " + race);
				return unknown;
		}
	}
}