package crystark.common.api;

public class CardToken {
	public final CardTokenType	type;
	public final Object		value;

	public CardToken(CardTokenType type) {
		this(type, null);
	}

	public CardToken(CardTokenType type, Object value) {
		super();
		this.type = type;
		this.value = value;
		if (value == null && type.requiresValue) {
			throw new IllegalArgumentException("Token " + type + " requires a value.");
		}
	}
}
