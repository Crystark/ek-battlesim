package crystark.common.api;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum CardTokenType {
	event("e"), diMerit("di"), legendary("lg"),
	attackMultiplier("atk", Float::valueOf), healthMultiplier("hp", Float::valueOf), conflict("cf", ConflictType::valueOf);

	public final String								token;
	public final boolean							requiresValue;
	public final Function<String, ?>				valueParser;

	private static final Map<String, CardTokenType>	mapping	= Stream
																.of(CardTokenType.values())
																.collect(Collectors.toMap(t -> t.token, t -> t));

	CardTokenType(String token) {
		this(token, null);
	}

	CardTokenType(String token, Function<String, ?> valueParser) {
		this.token = token;
		this.requiresValue = valueParser != null;
		this.valueParser = valueParser;
	}

	public static CardTokenType byToken(String token) {
		CardTokenType result = mapping.get(token);
		if (result == null) {
			throw new IllegalArgumentException("Unknown token " + token);
		}
		return result;
	}
}
