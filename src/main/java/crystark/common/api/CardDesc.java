package crystark.common.api;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

public class CardDesc {
	private static final ConcurrentMap<String, AtomicInteger>	NAMES	= new ConcurrentHashMap<>();

	public final String											uniqueName;
	public final String											name;
	public final int											cost;
	public final int											evolution;
	public final int											level;
	public final int											timer;
	public final int											attack;
	public final long											health;
	public final CardType										type;
	public final ConflictType									conflictType;
	public final int											limit;
	public final List<AbilityDesc>								abilities;
	public final List<CardToken>								tokens;

	public final int											baseCost;

	public CardDesc(String uniqueName, String name, int cost, int evolution, int level, int timer, int attack, long health, CardType type, ConflictType conflictType, int limit, List<AbilityDesc> abilities, List<CardToken> tokens) {
		super();
		this.uniqueName = uniqueName;
		this.name = name;
		this.cost = cost;
		this.evolution = evolution;
		this.level = level;
		this.timer = timer;
		this.attack = attack;
		this.health = health;
		this.type = type;
		this.conflictType = conflictType;
		this.abilities = abilities;
		this.tokens = tokens;
		this.limit = limit;

		this.baseCost = cost - (int) Math.ceil(evolution / 2.0);
	}

	public CardDesc(String name, int cost, int evolution, int level, int timer, int attack, long health, CardType type, ConflictType conflictType, Integer limit, List<AbilityDesc> abilities, List<CardToken> tokens) {
		this(newUniqueName(name), name, cost, evolution, level, timer, attack, health, type, conflictType, limit, abilities, tokens);
	}

	private static String newUniqueName(String name) {
		NAMES.putIfAbsent(name, new AtomicInteger(0));
		return name + '[' + NAMES.get(name).incrementAndGet() + ']';
	}

	public List<String> toStringList() {
		List<String> l = new ArrayList<>();
		Collections
			.addAll(l,
				name,
				String.valueOf(cost),
				String.valueOf(timer),
				String.valueOf(attack),
				String.valueOf(health),
				type.name());

		for (AbilityDesc ad : abilities) {
			l.add(ad.toString());
		}
		return l;
	}

	@Override
	public String toString() {
		return name + ", " + cost + ", " + timer + ", " + attack + ", " + health + ", " + type + ", " + abilities;
	}
}
