package crystark.common.api;

import crystark.tools.Tools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 *
 */
public class NamesConverterByFiles implements NamesConverter {
	private static final Logger L = LoggerFactory.getLogger(NamesConverterByFiles.class);

	private final String baseFolder;
	private static final String FILE_NAME_SKILLS = "alias-skills.txt";
	private static final String FILE_NAME_RUNES  = "alias-runes.txt";

	private Map<String, String> skillsFromAlias = new HashMap<>(512);
	private Map<String, String> skillsToAlias   = new HashMap<>(512);
	private Map<String, String> runesFromAlias  = new HashMap<>(128);
	private Map<String, String> runesToAlias    = new HashMap<>(128);

	public NamesConverterByFiles(Game game) {
		baseFolder = game.toString() + '/';
		loadMapping(baseFolder + FILE_NAME_SKILLS, skillsFromAlias, skillsToAlias);
		loadMapping(baseFolder + FILE_NAME_RUNES, runesFromAlias, runesToAlias);
	}

	private void loadMapping(String filePath, Map<String, String> alias2name, Map<String, String> name2alias) {
		try {
			InputStream stream = null;
			String fullFileName = Tools.getStartDir() + File.separator + filePath;
			File file = new File(fullFileName);
			if (file.exists()) {
				L.info("Aliases file '" + filePath + '\'');
				stream = new FileInputStream(file);
			}
			else {
				//L.info("Aliases file '" + filePath + "' not found. Use default aliases.");
				stream = new ByteArrayInputStream(new byte[0]);
			}
			InputStream cpStream = ClassLoader.getSystemResourceAsStream(filePath);

			try (BufferedReader br = new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8));
				BufferedReader cpbr = new BufferedReader(new InputStreamReader(cpStream, StandardCharsets.UTF_8))) {

				final Stream<String> lines = Stream.concat(br.lines(), cpbr.lines());
				lines
					.map(String::trim)
					.filter(s -> s.length() > 0 && !s.startsWith("#"))
					.forEach(s -> {
							final int i = s.indexOf('=');
							final String ign = s.substring(0, i).trim();
							final String mr = s.substring(i + 1).trim();

							//Verified skills are first in the file
							alias2name.putIfAbsent(ign.toLowerCase(), mr);
							name2alias.putIfAbsent(mr.toLowerCase(), ign);
						}
					);
				br.close();
			}
			catch (IOException e) {

			}
		}
		catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String skillFromAlias(String name) {
		return skillsFromAlias.getOrDefault(name.toLowerCase(), name);
	}

	@Override
	public String skillToAlias(String name) {
		return skillsToAlias.getOrDefault(name.toLowerCase(), name);
	}

	@Override
	public String runeFromAlias(String name) {
		return runesFromAlias.getOrDefault(name.toLowerCase(), name);
	}

	@Override
	public String runeToAlias(String name) {
		return runesToAlias.getOrDefault(name.toLowerCase(), name);
	}
}
