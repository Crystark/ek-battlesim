package crystark.common.api;

import java.util.Map;

public class BattleDesc {
	public final DeckDesc				attacker;
	public final DeckDesc				defender;
	public final Map<String, Object>	metas;

	public BattleDesc(DeckDesc attacker, DeckDesc defender) {
		this(attacker, defender, null);
	}

	public BattleDesc(DeckDesc attacker, DeckDesc defender, Map<String, Object> metas) {
		super();
		this.attacker = attacker;
		this.defender = defender;
		this.metas = metas;
	}

	public Object getMeta(String key) {
		return metas == null ? null : metas.get(key);
	}
}
