package crystark.common.api;

import crystark.ek.Helper;

import java.util.ArrayList;
import java.util.List;

import static crystark.common.api.Game.*;

public enum Server {
	//skorn(ek, "s1.ekru.ifreeteam.com"),
	apollo(mr, "s1.ekbb.ifreeteam.com", "CrystarkSim", "EKb5p4ssw0rd"),
	//daeneris(loa,"ea9.liesofastaroth.com", "CrystarkSim", "EKb5p4ssw0rd"),//Merget to Loki
	loki(loa,"ea2.liesofastaroth.com", "CrystarkSim", "EKb5p4ssw0rd"),
	mr_01(mrn, "s1.magicrealms.net", "CrystarkSim", "EKb5p4ssw0rd"),
	mirrorlake(hf, "s1.heroinesfantasy.com", "facebook_104303423725237", "")
	;
	//s1.amandrfr.ifreeteam
	//s2.amandrfr.ifreeteam
	//s3.amandrfr.ifreeteam
	//s0.siguozhanji.muhenet.com

	private final Game          game;
	private final String        host;
	private final Credentials[] users;

	Server(Game game, String host, String... logins) {
		this.game = game;
		this.host = host;

		final int count = logins.length / 2;
		users = new Credentials[count];
		for (int i = 0; i < count; i++) {
			Credentials c = new Credentials();
			c.user = logins[i*2];
			c.pass = logins[i*2 + 1];
			users[i] = c;
		}
	}

	public Game game() {
		return this.game;
	}

	public String host() {
		return this.host;
	}

	public Credentials user() {
		return users.length == 0 ? null : users[Helper.random(users.length)];
	}

	@Override
	public String toString() {
		return host();
	}

	public static final List<String> names() {
		ArrayList<String> list = new ArrayList<String>();
		for (Server server : values()) {
			list.add(server.name());
		}
		return list;
	}

	public static class Credentials {
		public String user;
		public String pass;
	}
}