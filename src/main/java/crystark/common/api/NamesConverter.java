package crystark.common.api;

/**
 *
 */
public interface NamesConverter {
	String skillFromAlias(String name);

	String skillToAlias(String name);

	String runeFromAlias(String name);

	String runeToAlias(String name);

	NamesConverter IDENTITY = new NamesConverter(){

		@Override
		public String skillFromAlias(String name) {
			return name;
		}

		@Override
		public String skillToAlias(String name) {
			return name;
		}

		@Override
		public String runeFromAlias(String name) {
			return name;
		}

		@Override
		public String runeToAlias(String name) {
			return name;
		}
	};
}
