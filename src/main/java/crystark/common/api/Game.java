package crystark.common.api;

public enum Game {
	//ek,
	mr("Magic Realms"),
	loa("Lies of Astaroth"),
	mrn("Magic Realms Nostalgia"),
	hf("Heroines Fantasy");

	private final String fullName;
	Game(String fullName){
		this.fullName = fullName;
	}

	public String getFullName() {
		return fullName;
	}
}