package crystark.common.api;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public enum LaunchType {
	normal(null),
	quickstrike("qs"),
	desperation("d"),
	preemptiveStrike("ps"),
	deathWhisper("dw");

	private static Map<String, LaunchType>	byPrefix	= Arrays.asList(LaunchType.values()).stream()
															.collect(Collectors.toMap(LaunchType::prefix, lc -> lc));
	public final String						prefix;

	LaunchType(String prefix) {
		this.prefix = prefix;
	}

	public String prefix() {
		return prefix;
	}

	public static LaunchType byPrefix(String prefix) {
		return byPrefix.get(prefix);
	}
}
