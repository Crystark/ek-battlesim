package crystark.common.api;

import java.util.List;
import java.util.Objects;

public class DeckDesc {
	public final String			deckName;
	public final String			playerName;
	public final int			playerLevel;
	public final int			health;
	public final List<CardDesc>	cardsDescs;
	public final List<RuneDesc>	runesDescs;

	public final int			skipShuffle;
	public final boolean		alwaysStart;
	public final boolean		infiniteHp;

	public final boolean		bypassValidation;

	public final int			waitToPlay;

	private int			cardsCost = -1;
	private int			cardsBaseCost = -1;
	private int			cardsHp = -1;
	private int			cardsAtk = -1;

	public DeckDesc(String deckName, String playerName, Integer playerLevel, Integer health, List<CardDesc> cardsDescriptions, List<RuneDesc> runesDescriptions, int skipShuffle, boolean alwaysStart, boolean infiniteHp) {
		this(deckName, playerName, playerLevel, health, cardsDescriptions, runesDescriptions, skipShuffle, alwaysStart, infiniteHp, null, false);
	}

	public DeckDesc(String deckName, String playerName, Integer playerLevel, Integer health, List<CardDesc> cardsDescriptions, List<RuneDesc> runesDescriptions, int skipShuffle, boolean alwaysStart, boolean infiniteHp, Integer waitToPlay, boolean bypassValidation) {
		super();
		this.deckName = deckName;
		this.playerName = playerName;
		this.playerLevel = playerLevel == null ? 0 : playerLevel;
		this.health = health == null ? 0 : health;
		this.cardsDescs = cardsDescriptions;
		this.runesDescs = runesDescriptions;
		this.skipShuffle = skipShuffle;
		this.alwaysStart = alwaysStart;
		this.infiniteHp = infiniteHp;
		this.waitToPlay = waitToPlay == null ? 0 : waitToPlay;
		this.bypassValidation = bypassValidation;
	}

	public int getCardsCost() {
		if (this.cardsCost < 0) {
			this.cardsCost = calcCardsCost(cardsDescs);
		}
		return this.cardsCost;
	}

	public int getCardsBaseCost() {
		if (this.cardsBaseCost < 0) {
			this.cardsBaseCost = calcCardsBaseCost(cardsDescs);
		}
		return this.cardsBaseCost;
	}

	public int getCardsHp() {
		if (this.cardsHp < 0) {
			this.cardsHp = calcCardsHp(cardsDescs);
		}
		return this.cardsHp;
	}

	public int getCardsAtk() {
		if (this.cardsAtk < 0) {
			this.cardsAtk = calcCardsAtk(cardsDescs);
		}
		return this.cardsAtk;
	}

	public int getMaxCost() {
		return calcCost(playerLevel);
	}

	public int getTotalPower() {
		if (infiniteHp) {
			return Integer.MAX_VALUE;
		}
		return health + getCardsHp() + getCardsAtk();
	}

	public int countTokens(CardTokenType token) {
		int c = 0;
		for (CardDesc cardDesc : cardsDescs) {
			if (cardDesc.tokens.stream().anyMatch(t -> t.type.equals(token))) {
				c++;
			}
		}
		return c;
	}

	@Override
	public String toString() {
		return Objects.toString(this.deckName, this.playerName);
	}

	static int calcCost(int level) {
		if (level <= 20) {
			return 10 + 3 * level;
		}
		else if (level <= 50) {
			return 70 + 2 * (level - 20);
		}
		else {
			return 130 + (level - 50);
		}
	}

	static final int calcCardsCost(List<? extends CardDesc> cards) {
		int totalCost = 0;
		for (CardDesc card : cards) {
			totalCost += card.cost;
		}
		return totalCost;
	}

	static final int calcCardsBaseCost(List<? extends CardDesc> cards) {
		int totalCost = 0;
		for (CardDesc card : cards) {
			totalCost += card.baseCost;
		}
		return totalCost;
	}

	static final int calcCardsHp(List<? extends CardDesc> cards) {
		int totalHp = 0;
		for (CardDesc card : cards) {
			totalHp += card.health;
		}
		return totalHp;
	}

	static final int calcCardsAtk(List<? extends CardDesc> cards) {
		int totalAtk = 0;
		for (CardDesc card : cards) {
			totalAtk += card.attack;
		}
		return totalAtk;
	}
}
