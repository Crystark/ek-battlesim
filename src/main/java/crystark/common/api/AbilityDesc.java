package crystark.common.api;

import crystark.common.db.Database;
import crystark.common.db.model.BaseSkill;

import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AbilityDesc {
	private static final Pattern COLON = Pattern.compile(":", Pattern.LITERAL);
	public static final String ESCAPE_COLON = Matcher.quoteReplacement("\\:");
	public final String name;
	public final String		abilityName;
	public final String     fullName;
	public final Object		value;
	public final Object		value2;
	public final LaunchType launchType;
	public final boolean    isUseless;
	public final int        launchCondition;
	private      String     toString;

	public AbilityDesc(String name, String abilityName, String fullName, LaunchType launchType, Object value, Object value2, boolean isUseless, int launchCondition) {
		super();
		this.name = name;
		this.abilityName = abilityName;
		this.fullName = fullName;
		this.launchType = launchType;
		this.value = value;
		this.value2 = value2;
		this.isUseless = isUseless;
		this.launchCondition = launchCondition;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, abilityName, value, value2) * 31 + launchType.hashCode();
	}

	@Override
	public boolean equals(Object o) {
		// self check
		if (this == o)
			return true;
		// null check, type check and cast
		if (o == null || getClass() != o.getClass())
			return false;
		AbilityDesc ad = (AbilityDesc) o;
		// field comparison
		return Objects.equals(name, ad.name)
			&& Objects.equals(abilityName, ad.abilityName)
			&& launchType == ad.launchType
			&& Objects.equals(value, ad.value)
			&& Objects.equals(value2, ad.value2)
			;
	}

	@Override
	public String toString() {
		if (toString == null) {
			List<BaseSkill> skill = Database.get().skills().getByAbilityName(abilityName);
			LaunchType defaultLaunchType = LaunchType.normal;
			boolean showValues = true;
			if (skill != null && !skill.isEmpty()) {
				BaseSkill first = skill.get(0);
				defaultLaunchType = first.launchType();
				if (skill.size() == 1 && first.isBase()) {
					Object origValue = first.value();
					Object origValue2 = first.value2();
					if ((origValue == null || origValue.equals(0)) && (origValue2 == null || origValue2.equals(0))) {
						// Don't show values for skills that don't have variations unless values are overrided
						showValues = false;
					}
				}
			}

			StringBuilder sb = new StringBuilder(COLON.matcher(abilityName).replaceAll(ESCAPE_COLON));
			if (!launchType.equals(defaultLaunchType) && launchType != LaunchType.normal) {
				sb.insert(0, launchType.prefix.toUpperCase() + '_');
			}
			if (showValues) {
				if (value != null) {
					String s = COLON.matcher(value.toString()).replaceAll(ESCAPE_COLON);
					sb.append(':').append(s);
				}
				if (value2 != null) {
					String s = COLON.matcher(value2.toString()).replaceAll(ESCAPE_COLON);
					sb.append(':').append(s);
				}
			}
			toString = sb.toString();
		}
		return toString;
	}
}
