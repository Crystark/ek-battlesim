package crystark.common.api;

public class RuneDesc {
	public final String	name;
	public final int	times;
	public final Object	value;

	public RuneDesc(String name, int times, Object value) {
		super();
		this.name = name;
		this.times = times;
		this.value = value;
	}

	@Override
	public String toString() {
		return name + (value == null ? "" : ":" + value);
	}
}
