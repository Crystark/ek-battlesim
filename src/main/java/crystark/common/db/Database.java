package crystark.common.db;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import com.google.gson.Gson;

import crystark.common.api.Game;
import crystark.common.db.model.ek.EkDto;
import crystark.common.db.model.ek.EkMapDto;
import crystark.common.events.Warnings;
import crystark.ek.EkbsConfig;
import crystark.tools.Tools;

public class Database {
	private static final Gson   G             = new Gson();
	private static final String RES_ROOT      = System.getProperty("user.dir") + "/src/main/resources/";
	private static final String ADDONS_CARDS  = "db/addons.cards.json";
	private static final String ADDONS_SKILLS = "db/addons.skills.json";
	private static final String ADDONS_RUNES  = "db/addons.runes.json";
	private static final String DB_CARDS      = ".cards.json";
	private static final String DB_SKILLS     = ".skills.json";
	private static final String DB_RUNES      = ".runes.json";
	private static final String DB_MAPS       = ".maps.json";

	private static Database DEFAULT_INSTANCE;
	private static Game currentGame;

	private final CardDb  cards;
	private final SkillDb skills;
	private final RuneDb  runes;
	private final MapDb   maps;

	private final int version;

	private Database(InputStream cardsData, InputStream skillsData, InputStream runesData, InputStream mapsData, boolean isUseAffectType) {

		final Reader cardsReader = new InputStreamReader(cardsData, StandardCharsets.UTF_8);
		final Reader skillsReader = new InputStreamReader(skillsData, StandardCharsets.UTF_8);
		final Reader runesReader = new InputStreamReader(runesData, StandardCharsets.UTF_8);
		final Reader mapsReader = new InputStreamReader(mapsData, StandardCharsets.UTF_8);


		EkDto cardsDataDto = G.fromJson(cardsReader, EkDto.class);
		version = Tools.parseInt(cardsDataDto.version.http);

		this.cards = new CardDb(this);
		this.skills = new SkillDb(this, isUseAffectType);
		this.runes = new RuneDb(this);
		this.maps = new MapDb(this);

		this.cards.addAll(cardsDataDto.data.Cards);
		Reader addonCardsReader = new InputStreamReader(ClassLoader.getSystemResourceAsStream(ADDONS_CARDS));
		this.cards.addAll(G.fromJson(addonCardsReader, EkDto.class).data.Cards);

		this.skills.addAll(G.fromJson(skillsReader, EkDto.class).data.Skills);
		Reader addonSkillsReader = new InputStreamReader(ClassLoader.getSystemResourceAsStream(ADDONS_SKILLS));
		this.skills.addAll(G.fromJson(addonSkillsReader, EkDto.class).data.Skills);

		this.runes.addAll(G.fromJson(runesReader, EkDto.class).data.Runes);
		Reader addonRunesReader = new InputStreamReader(ClassLoader.getSystemResourceAsStream(ADDONS_RUNES));
		this.runes.addAll(G.fromJson(addonRunesReader, EkDto.class).data.Runes);

		this.maps.addAll(G.fromJson(mapsReader, EkMapDto.class).data);
	}

	private Database(String cardsData, String skillsData, String runesData, String mapsData) {
		this(
			new ByteArrayInputStream(cardsData.getBytes(StandardCharsets.UTF_8)),
			new ByteArrayInputStream(skillsData.getBytes(StandardCharsets.UTF_8)),
			new ByteArrayInputStream(runesData.getBytes(StandardCharsets.UTF_8)),
			new ByteArrayInputStream(mapsData.getBytes(StandardCharsets.UTF_8)),
			false
		);
	}

	public int compareTo(Database db) {
		return Integer.valueOf(this.version).compareTo(db.version);
	}

	private static boolean isRunFromConsole() {
		return System.console() != null;
	}

	/**
	 * This rewrites the default db files. It also recreates the default instance using that data.
	 */
	public static void writeDb(Game game, String cardsData, String skillsData, String runesData, String mapsData) {
		String cardsPath = "db/" + game + DB_CARDS;
		String skillsPath = "db/" + game + DB_SKILLS;
		String runesPath = "db/" + game + DB_RUNES;
		String mapsPath = "db/" + game + DB_MAPS;

		if (!isRunFromConsole()) {
			cardsPath = RES_ROOT + cardsPath;
			skillsPath = RES_ROOT + skillsPath;
			runesPath = RES_ROOT + runesPath;
			mapsPath = RES_ROOT + mapsPath;
		}

		writeTo(cardsPath, cardsData);
		writeTo(skillsPath, skillsData);
		writeTo(runesPath, runesData);
		writeTo(mapsPath, mapsData);

	}

	private static void writeTo(String file, String json) {
		Tools.writeToFile(file, Tools.jsonPrettyPrint(json));
	}

	public static Database get() {
		if (DEFAULT_INSTANCE == null || currentGame != EkbsConfig.current().game()) {
			currentGame = EkbsConfig.current().game();
			DEFAULT_INSTANCE = create(currentGame);
		}
		return DEFAULT_INSTANCE;
	}

	public static Database create(Game game) {
		return create(game, false);
	}
	public static Database create(Game game, boolean isUseAffectType) {

		Database usedDb;

		String cardsPath = "db/" + game + DB_CARDS;
		String skillsPath = "db/" + game + DB_SKILLS;
		String runesPath = "db/" + game + DB_RUNES;
		String mapsPath = "db/" + game + DB_MAPS;

		try {
			// Try to load from file system
			final String dir = Tools.getStartDir() + File.separator;
			usedDb = new Database(
				new FileInputStream(dir + cardsPath),
				new FileInputStream(dir + skillsPath),
				new FileInputStream(dir + runesPath),
				new FileInputStream(dir + mapsPath),
				isUseAffectType
			);
			Warnings.send("You're using the local database at '" + dir + "db'");
		}
		catch (IOException e) {
			InputStream cardsIs = ClassLoader.getSystemResourceAsStream(cardsPath);
			InputStream skillsIs = ClassLoader.getSystemResourceAsStream(skillsPath);
			InputStream runesIs = ClassLoader.getSystemResourceAsStream(runesPath);
			InputStream mapsIs = ClassLoader.getSystemResourceAsStream(mapsPath);
			usedDb = new Database(cardsIs, skillsIs, runesIs, mapsIs, isUseAffectType);
		}

		return usedDb;
	}

	public static Database from(String cardsData, String skillsData, String runesData, String mapsData) {
		return new Database(cardsData, skillsData, runesData, mapsData);
	}

	public CardDb cards() {
		return cards;
	}

	public SkillDb skills() {
		return skills;
	}

	public RuneDb runes() {
		return runes;
	}

	public MapDb maps() {
		return maps;
	}

}
