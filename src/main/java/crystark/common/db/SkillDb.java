package crystark.common.db;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Stream;

import crystark.common.db.model.BaseSkill;
import crystark.common.db.model.ek.EkSkill;
import crystark.ek.EkbsConfig;
import rx.Observable;

public class SkillDb extends Db<BaseSkill> {
	final Map<String, List<BaseSkill>> dbDataByAbilityName = new TreeMap<>();
	final Map<Integer, List<BaseSkill>> dbDataByAffectType;
	final Set<String> hasDifferentLaunchType = new HashSet<>(64);
	final boolean isUseAffectType;

	public SkillDb(Database db, boolean isUseAffectType) {
		super(db);
		this.isUseAffectType = isUseAffectType;

		if (isUseAffectType) {
			dbDataByAffectType = new HashMap<>(256);
		} else {
			dbDataByAffectType = Collections.emptyMap();
		}
	}

	public SkillDb addAll(List<EkSkill> elements) {
		Observable<BaseSkill> skillObservable = Observable
			.from(elements)
			.filter(s -> !s.Desc.equals("jinengmiaoshu"))
			.map(s -> new BaseSkill(db, s))
			.doOnNext(
				s -> {
					String cleanName = s.cleanName().toLowerCase();
					computeEntry(s, cleanName);
				}
			);
		// getByAbilityAffectType is used in tests only, but dbDataByAffectType initialization is long.
		if (isUseAffectType) {

			skillObservable = skillObservable.doOnNext(
				s -> dbDataByAffectType.compute(s.AffectType(), (key, value) -> {
					List<BaseSkill> newValue = (value == null ? new ArrayList<>(16) : value);
					newValue.add(s);
					return newValue;
				})
			);
		}

		super.addAll(skillObservable);

		return this;
	}

	private void computeEntry(BaseSkill baseSkill, String cleanName) {
		dbDataByAbilityName.compute(cleanName, (key, value) -> {
				List<BaseSkill> newValue = (value == null ? new ArrayList<>(16) : value);
				if (newValue.size() > 0 && baseSkill.launchType() != newValue.get(newValue.size() - 1).launchType()){
					hasDifferentLaunchType.add(key);
				}
				newValue.add(baseSkill);
				return newValue;
			});
	}

	public List<BaseSkill> getByAbilityName(String name) {
		String aliasName = EkbsConfig.current().namesConverter().skillToAlias(name);
		List<BaseSkill> baseSkills = dbDataByAbilityName.get(aliasName.toLowerCase());
		if (!name.equals(aliasName)){
			final List<BaseSkill> baseSkillsOrig = dbDataByAbilityName.get(name.toLowerCase());
			if (baseSkills != null && baseSkillsOrig != null) {
				ArrayList<BaseSkill> tmpList = new ArrayList<>(baseSkills.size() + baseSkillsOrig.size());
				tmpList.addAll(baseSkills);
				tmpList.addAll(baseSkillsOrig);
				baseSkills = tmpList;
			} else if (baseSkillsOrig != null) {
				baseSkills = baseSkillsOrig;
			}
		}
		return baseSkills;
	}

	public List<BaseSkill> getByAbilityAffectType(int affectType) {
		if (!isUseAffectType) throw new IllegalStateException("getByAbilityAffectType method is disabled");
		final List<BaseSkill> baseSkills = dbDataByAffectType.get(affectType);
		return baseSkills == null ? Collections.EMPTY_LIST : baseSkills;
	}

	public boolean hasDifferentLaunchType(String name){
		return hasDifferentLaunchType.contains(name.toLowerCase());
	}

	public Stream<Entry<String, List<BaseSkill>>> allByAbilityName() {
		return dbDataByAbilityName.entrySet().stream();
	}
}
