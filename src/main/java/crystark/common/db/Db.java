package crystark.common.db;

import crystark.common.db.model.IBaseModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public abstract class Db<T extends IBaseModel> {
	private static final Logger		L	= LoggerFactory.getLogger(Db.class);
	protected final Database		db;
	protected final Map<Integer, T>	dbData;
	protected final Map<String, T>	dbDataByName;
	protected final Set<String>		nameDuplicates;

	Db(Database db) {
		this.db = db;
		this.dbData = new TreeMap<>();
		this.dbDataByName = new HashMap<>(2048);
		this.nameDuplicates = new HashSet<>(16);
	}

	public void addAll(Observable<T> elements) {
		elements.toBlocking().forEach(this::add);
	}

	public void add(T t) {
		dbData.put(t.id(), t);
		String name = t.name().trim().toLowerCase();
		dbDataByName.merge(name, t, (o, n) -> {
			nameDuplicates.add(name);
			L.warn(name + "'s name has duplicates (" + o.id() + " or " + n.id() + " ?).");
			return o; // keep first
		});
	}

	public T get(int id) {
		return dbData.get(id);
	}

	public T get(String name) {
		name = name.toLowerCase();
		//Single notification
		if (nameDuplicates.remove(name)) {
			L.warn(name + "'s name has duplicates. It's advised to use the full description or id to ensure the desired element is selected.");
		}
		return dbDataByName.get(name);
	}

	public boolean exists(String name) {
		return dbDataByName.containsKey(name.toLowerCase());
	}

	public Observable<T> all() {
		return Observable.from(dbData.values());
	}
}
