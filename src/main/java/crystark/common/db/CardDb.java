package crystark.common.db;

import crystark.common.db.model.BaseCard;
import crystark.common.db.model.ek.EkCard;
import rx.Observable;

import java.util.List;

public class CardDb extends Db<BaseCard> {
	public CardDb(Database db) {
		super(db);
	}

	public void addAll(List<EkCard> elements) {
		super.addAll(Observable
			.from(elements)
			.map(c -> new BaseCard(db, c)));
	}
}
