package crystark.common.db;

import java.util.List;

import crystark.common.db.model.BaseRune;
import crystark.common.db.model.ek.EkRune;
import rx.Observable;

public class RuneDb extends Db<BaseRune> {
	public RuneDb(Database db) {
		super(db);
	}

	public RuneDb addAll(List<EkRune> elements) {
		super.addAll(Observable
			.from(elements)
			.map(r -> new BaseRune(db, r)));

		return this;
	}
}
