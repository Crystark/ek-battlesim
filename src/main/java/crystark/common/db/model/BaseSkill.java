package crystark.common.db.model;

import java.util.Arrays;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import crystark.common.api.AbilityDesc;
import crystark.common.api.CardType;
import crystark.common.api.Game;
import crystark.common.api.LaunchType;
import crystark.common.db.CardDb;
import crystark.common.db.Database;
import crystark.common.db.SkillDb;
import crystark.common.db.model.ek.EkSkill;
import crystark.common.events.Warnings;
import crystark.ek.EkbsConfig;
import crystark.ek.battlesim.ability.AwakingAbility;
import crystark.ek.battlesim.ability.SummonAbility;
import crystark.ek.battlesim.ability.addons.Cardbound;
import crystark.ek.battlesim.ability.addons.Composite;
import crystark.ek.battlesim.ability.addons.Judgement;
import crystark.ek.battlesim.ability.addons.Summon;
import crystark.ek.battlesim.ability.impl.Cleanse;
import crystark.tools.Tools;

public class BaseSkill implements IBaseModel, Comparable<BaseSkill> {
	private static final Object								NO_VALUE	= new Object();
	private static final Collector<CharSequence, ?, String>	AMP_JOINER	= Collectors.joining("&");

	private static final Pattern LAUNCH_TYPE_ALIASES  = Pattern.compile(
		"Desperation:|Desperate:|\\[Desperation\\]|^Desperation|"
			+ "Quick Strike:|\\[Quick Strike\\]|First Strike:|\\[Strike\\]|"
			+ "Preemptive Strike:|"
			+ "Death Whisper:|Death Wisper:"
	);
	private static final Pattern NUMERIC_SUFFIX       = Pattern.compile(" [0-9#\\. ]+$");
	private static final Pattern BAD_QUOTE            = Pattern.compile("[‘’，]");
	private static final Pattern BAD_COLON            = Pattern.compile("：", Pattern.LITERAL);
	private static final Pattern BAD_COMMA            = Pattern.compile("、", Pattern.LITERAL);
	private static final Pattern BAD_DOUBLE_QUOTE     = Pattern.compile("“", Pattern.LITERAL);
	private static final Pattern BAD_DOT              = Pattern.compile(".", Pattern.LITERAL);
	private static final Pattern BAD_PARENTHESIS_OPEN = Pattern.compile("（", Pattern.LITERAL);
	private static final Pattern BAD_PARENTHESIS_CLOSE = Pattern.compile("）", Pattern.LITERAL);
	private static final Pattern BAD_EXCLAMATION = Pattern.compile("！", Pattern.LITERAL);
	private static final Pattern BAD_SPACE            = Pattern.compile("[ ·•。\\s]+");
	private static final Pattern SD_II                = Pattern.compile("nII", Pattern.LITERAL);

	private final EkSkill									skill;
	private final Database									db;

	private String											fixedName;
	private String											cleanName;
	private Object											value;
	private Object											value2;
	private LaunchType										launchType;

	public BaseSkill(Database db, EkSkill skill) {
		this.db = db;
		this.skill = skill;
	}

	@Override
	public int id() {
		return skill.SkillId;
	}

	private String baseName() {
		return skill.Name;
	}

	@Override
	public String name() {

		if (fixedName == null) {

			fixedName = baseName();

			final Game game = EkbsConfig.current().game();

			switch (skill.AffectType) {
				case 124:
					fixedName = Cleanse.NAME; //HF has 2 'Purify field' skills
			}

			if (game == Game.mr || game == Game.mrn) {
				switch (id()) {
					case 2173: //LoA Self-destruction
						fixedName = "Self-destruction X";
						break;
					case 3019: // Resurrection = Resurrection 8 + Dexterity
						fixedName = "Resurrection X";
						break;
					case 3030: //Puncture 1 [Rune]
					case 3031:
					case 3032:
					case 3033:
					case 3034:
					case 3035:
					case 3036:
					case 3037:
					case 3038:
					case 3039: //Puncture 10 [Rune]
						fixedName = "Group Puncture " + (skill.SkillId - 3029);
						break;
					case 3040:
					case 3041:
					case 3042:
						
					case 3043:
					case 3044:
					case 3045:
					case 3046:
					case 3047:
					case 3048:
					case 3049:
						fixedName = "Group Water Shield " + (skill.SkillId - 3039);
						break;
					case 3100: //Desperation: Exile
						fixedName = "Exile All";
						break;
					case 3116: //Armor-less 5
						fixedName = "Armor-less_5";
						break;
					case 3132: //Quick Strike:Exile
						fixedName = "Quick Strike: Resistance-less";
						break;
					case 3124: //Frost Shock 15
						fixedName = "Quick Strike: Lighting Cut 15";
						break;
					case 3167://Frost Shock 20
						fixedName = "Lighting Cut 20";
						break;
					case 3133:// Thunderstorm 15
						fixedName = "Thundershower 15";
						break;
					default:
						fixedName = BAD_SPACE.matcher(fixedName).replaceAll(" "); // remove when Titan's Blast is fixed
						fixedName = BAD_QUOTE.matcher(fixedName).replaceAll("'"); // remove when Titan's Blast is fixed
						fixedName = BAD_COLON.matcher(fixedName).replaceAll(":"); // HF "Awaken："
						fixedName = SD_II.matcher(fixedName).replaceAll("n II");// remove when Summon Dragon II is fixed

						fixedName = fixedName.trim();
						break;
				}
			} else {
				switch (id()) {
					case 2173: //LoA Self-destruction
						fixedName = "Self-destruction X";
						break;
					default:
						fixedName = BAD_SPACE.matcher(fixedName).replaceAll(" "); // remove when Titan's Blast is fixed
						fixedName = BAD_QUOTE.matcher(fixedName).replaceAll("'"); // remove when Titan's Blast is fixed
						fixedName = BAD_COLON.matcher(fixedName).replaceAll(":"); // HF "Awaken："
						fixedName = SD_II.matcher(fixedName).replaceAll("n II");// remove when Summon Dragon II is fixed

						fixedName = fixedName.trim();
						break;
				}
			}
		}
		return fixedName;
	}

	/**
	 * Returns the name cleaned of all modifiers (desperation, quick strike, level, ...)
	 */
	public String cleanName() {
		if (cleanName == null) {
			BaseSkill linkedSkill = linkedSkill();
			if (linkedSkill == null || LaunchType.normal.equals(launchType())) {
				cleanName = name();
				cleanName = LAUNCH_TYPE_ALIASES.matcher(cleanName).replaceAll("");
				cleanName = NUMERIC_SUFFIX.matcher(cleanName).replaceAll("");
				cleanName = cleanName.trim();
			}
			else {
				cleanName = linkedSkill.cleanName();
			}
		}
		return cleanName;
	}

	public String abilityName() {
		switch (skill.AffectType) {
			case 99:
				return AwakingAbility.NAME;
			case 101:
			case 154: // Twins (Summon base self)
			case 184: //Summon:Cannon Fodder
				return Summon.NAME;
			case 122:
				return Composite.NAME;
			case 146:
				return Cardbound.NAME;
			case 158:
				return Judgement.NAME;
			default:
				return cleanName();
		}
	}

	public Integer level() {
		String[] parts = this.name().split(" ");
		if (parts.length > 1) {
			try {
				int num = Tools.parseInt(parts[parts.length - 1]);
				if (1 <= num && num <= 10) {
					return num;
				}
			}
			catch (NumberFormatException e) {
				// do nothing
			}
		}
		return null;
	}

	public String desc() {
		String fixedName = skill.Desc;
		fixedName = BAD_SPACE.matcher(fixedName).replaceAll(" ");
		fixedName = BAD_QUOTE.matcher(fixedName).replaceAll("'"); // remove when Titan's Blast is fixed
		fixedName = BAD_DOUBLE_QUOTE.matcher(fixedName).replaceAll("\"");
		fixedName = BAD_DOT.matcher(fixedName).replaceAll(".");
		fixedName = BAD_COMMA.matcher(fixedName).replaceAll(",");
		fixedName = BAD_EXCLAMATION.matcher(fixedName).replaceAll("!");
		fixedName = BAD_PARENTHESIS_OPEN.matcher(fixedName).replaceAll("(");
		fixedName = BAD_PARENTHESIS_CLOSE.matcher(fixedName).replaceAll(")");
		fixedName = BAD_COLON.matcher(fixedName).replaceAll(":"); // HF "Awaken："
		fixedName = fixedName.replace("\n", "");
		return fixedName;
	}

	public int AffectType() {
		final BaseSkill linkedSkill = linkedSkill();
		return linkedSkill == null ? skill.AffectType : linkedSkill.AffectType();
	}

	public boolean isUseless() {
		return skill.Type.equals("99") || (skill.AffectType == 55 && skill.AffectValue.equals("0"));
	}

	public boolean isEvolvePossibility() {
		return evoRank() != 0;
	}

	public boolean isGeneric() {
		return skill.AffectType == 0;
	}

	public boolean isSkillProvider() {
		return skill.AffectType == 60;
	}

	public boolean isComposite() {
		return skill.AffectType == 122;
	}

	public boolean isSummon() {
		return skill.AffectType == 101 || skill.AffectType == 154;
	}

	public boolean isJudgement(){
		return skill.AffectType == 158;
	}

	public boolean isBase() {
		return id() < 100000;
	}

	public int evoRank() {
		return skill.EvoRank;
	}

	public int launchCondition(){
		return skill.LanchCondition;
	}

	public int launchConditionValue(){
		return skill.LanchConditionValue;
	}

	/**
	 * Returns the final value of this skill based on what is required by the sim or null if no value is required.
	 */
	public Object value() {
		if (value == null) {
			Object value = null;
			CardDb cardsDb;

			switch (skill.AffectType) {
				case 84: // Roars (AffectValue is number of affected cards)
				case 88: // Type-changing
				case 89: // Silence
				case 94: // Divine Shield
				//case 111: // Dying Strike
				case 169: // Resistance-less
					value = NO_VALUE;
					break;
				case 99: // Tundra Silence; Awaking
					final CardType race = CardType.fromRace(skill.LanchConditionValue);
					value = skill.AffectValue + '_' + race;
					break;
				case 101: // Summons
					cardsDb = db.cards();
					Stream<String> stream = Stream.of(skill.AffectValue.split("_"));

					//LoA. Summons AffectValue2 cards count among cards referenced by AffectValue
					if (!skill.AffectValue.contains("_")){
						final Stream<String> st2 = Stream.of(skill.AffectValue2.split("_"));
						stream = Stream.concat(stream, st2);
					}

					value = stream
						.map(Tools::parseInt)
						.filter(id -> id != 0)
						.map(id -> {
							BaseCard card = cardsDb.get(id);
							if (card == null) {
								Warnings.send("Missing referenced card with id " + id + " for skill " + name());
							}
							return card;
						})
						.filter(Objects::nonNull)
						.map(BaseCard::name)
						.collect(AMP_JOINER);
					break;
				case 154: // Twins (Summon base self)
					value = SummonAbility.SELF;
					break;
				case 122: // Composite skills
					value = skillIds2Names(skill.AffectValue, "_");
					break;
				case 146: // Card Bound
					value = skillIds2Names(skill.AffectValue2, ",");;
					break;
				case 53:
				case 59:
				case 60:
				case 141: // Death Wisper
				case 142:// Preemtive Strike
				case 173: //Physical buff
					// Redirect to other skill value
					final BaseSkill linkedSkill = linkedSkill();
					value = linkedSkill == null ? skill.AffectValue : linkedSkill.value();
					break;
				case 86: // Death Marker
					value = Tools.parseInt(skill.AffectValue2);
					break;
				case 158: //Judgement
				case 162: //Frost Shock 15
				case 165: //Lighting Cut
					value = skill.AffectValue;
					break;
				case 184: //Summon:Cannon Fodder
					value = cardIds2Names(skill.AffectValue, "_");
					break;
				case 187: //Quick Summon 2
					//TODO
					value = NO_VALUE;
					break;
				case 189:// LoA Withered Magic
					value = NO_VALUE;
					break;
				case 190:// LoA Laido
					value = skill.AffectValue;
					break;
				default:
					String val = skill.AffectValue.replace("%", "");
					value = Tools.parseInt(val);
					if (value.equals(0)) {
						val = skill.AffectValue2.replace("%", "");
						value = Tools.parseInt(val);
					}
					break;
			}
			if (value instanceof Integer) {
				if (value.equals(0)) {
					value = NO_VALUE;
				}
				else {
					value = Math.abs((int) value);
				}
			}
			this.value = value;
		}
		return value == NO_VALUE ? null : value;
	}

	/**
	 * Returns the secondary value of this skill based on what is required by the sim or null if no secondary value is required.
	 */
	public Object value2() {
		if (value2 == null) {
			Object value2 = null;
			switch (skill.AffectType) {
				case 6: // ThunderBolt
				case 7: // Chain Lightning
				case 8: // Electric Shock
				case 9: // Iceball
				case 10: // Nova Frost
				case 11: // Blizzard
				case 24: // Aura skills
				case 25: // Aura skills
				//case 69: // Field of Chaos / Confusion
				//case 72: // Mania
				case 78: // Mana Corruption
				case 86: // Death Marker
				case 154: // Twins
				case 109: // Magic Array / Spell Mark
				case 169: // Resistance-less
					value2 = NO_VALUE;
					break;
				case 22: // Snipes
					int v =  Tools.parseInt(skill.AffectValue2);
					value2 = v > 2 && v < 99 ? v : NO_VALUE; //Special names for 'Snipe', 'Dual Snipe', 'Savlo'. 'Multishot' has to have 2nd value for LoA
					break;
				case 101: // Summons
					value2 = NO_VALUE;
					//LoA
					if (skill.AffectValue.contains("_")){
						value2 = Integer.parseInt(skill.AffectValue2);
					}
					break;

				case 146: // Card Bound
					value2 = cardIds2Names(skill.AffectValue,",");
					break;
				case 99: // Tundra Silence; Awaking
					value2 = skillIds2Names(skill.AffectValue2, ",");
					break;
				case 53:
				case 59:
				case 60:
				case 141:
				case 142:
				case 173:
					// Redirect to other skill value
					final BaseSkill linkedSkill = linkedSkill();
					value2 = linkedSkill == null ? skill.AffectValue2 : linkedSkill.value2();
					break;
				case 158: //Judgement
					value2 = skillIds2Names(skill.AffectValue2, "_");
					break;
				case 162: //Frost Shock 15
				case 165: //Lighting Cut
				case 166: //Demon Fire
				case 178: //Thunderstorm 15
					value2 = skill.AffectValue2;
					break;
				case 184: //Summon:Cannon Fodder
					value2 = NO_VALUE;
					break;
				case 187: //Quick Summon 2
					//TODO
					value2 = NO_VALUE;
					break;
				case 189:// LoA Withered Magic
					value = NO_VALUE;
					break;
				case 190:// LoA Laido
					value = skill.AffectValue;
					break;
				default:
					if (value() != null && !"0".equals(skill.AffectValue)) {
						value2 = Tools.parseInt(skill.AffectValue2);
					}
					break;
			}
			if (value2 instanceof Integer) {
				if (value2.equals(0)) {
					value2 = NO_VALUE;
				}
				else {
					value2 = Math.abs((int) value2);
				}
			}
			this.value2 = value2;
		}
		return value2 == NO_VALUE ? null : value2;
	}

	public BaseSkill linkedSkill() {
		BaseSkill linkedSkill = null;
		switch (skill.AffectType) {
			case 53:
			case 59:
			case 60:
			case 141:
			case 142:
			case 173:
				linkedSkill = db.skills().get(Tools.parseInt(skill.AffectValue));
				return linkedSkill;
		}
		return null;
	}

	/**
	 * Returns the LaunchType of this skill based on what is required by the sim.
	 */
	public LaunchType launchType() {
		if (launchType == null) {
			switch (skill.LanchType) {
				case 9:
					launchType = LaunchType.desperation;
					break;
				case 10:
				case 16:
					launchType = LaunchType.quickstrike;
					break;
				default:
					switch (skill.AffectType) {
						case 141:
							launchType = LaunchType.deathWhisper;
							break;
						case 142:
							launchType = LaunchType.preemptiveStrike;
							break;
						default:
							launchType = LaunchType.normal;
							break;
					}
					break;
			}
		}
		return launchType;
	}

	public AbilityDesc createAbilityDesc(Object value, Object value2, LaunchType type) {
		try {

			return new AbilityDesc(
				cleanName(),
				abilityName(),
				name(),
				type == null ? launchType() : type,
				value == null ? value() : value,
				value2 == null ? value2() : value2,
				isUseless(),
				launchCondition()
				);
		} catch (NumberFormatException e){
			throw new NumberFormatException("Skill '" + skill.Name + "' " + e.getMessage());
		}
	}

	public AbilityDesc createAbilityDesc(Object value, Object value2) {
		return createAbilityDesc(value, value2, null);
	}

	public AbilityDesc createAbilityDesc(LaunchType type) {
		return createAbilityDesc(null, null, type);
	}

	public AbilityDesc createAbilityDesc() {
		return createAbilityDesc(null, null, null);
	}

	@Override
	public int compareTo(BaseSkill o) {
		int compareName = this.cleanName().compareTo(o.cleanName());
		if (compareName != 0) {
			return compareName;
		}

		int compareLaunchType = this.launchType().compareTo(o.launchType());
		if (compareLaunchType != 0) {
			return compareLaunchType;
		}

		Integer lvl = this.level();
		if (lvl != null) {
			Integer otherLevel = o.level();
			if (otherLevel != null) {
				return lvl.compareTo(o.level());
			}
		}
		return 0;
	}

	private Object cardIds2Names(String stringValue, String splitter) {
		CardDb cardsDb;
		Object value;
		cardsDb = db.cards();
		value = Stream.of(stringValue.split(splitter))
			.map(Tools::parseInt)
			.filter(id -> id != 0)
			.map(id -> {
				BaseCard card = cardsDb.get(id);
				if (card == null) {
					Warnings.send("Missing referenced card with id " + id + " for skill " + name());
				}
				return card;
			})
			.filter(Objects::nonNull)
			.map(BaseCard::name)
			.collect(AMP_JOINER);
		return value;
	}

	private String skillIds2Names(String ids, String splitter) {
		SkillDb skillsDb = db.skills();
		return Arrays
			.stream(ids.split(splitter))
			.map(Tools::parseInt)
			.map(skillsDb::get)
			.filter(c -> c != null)
			.map(BaseSkill::name)
			//.map(BaseSkill::createAbilityDesc)
			//.map(AbilityDesc::toString)
			.collect(AMP_JOINER);
	}

	public EkSkill getSkill() {
		return skill;
	}

	@Override
	public String toString() {
		return skill.toString();
	}
}