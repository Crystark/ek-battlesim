package crystark.common.db.model;

import crystark.common.db.Database;
import crystark.common.db.model.ek.EkDeckInfo;
import crystark.common.db.model.ek.EkLeagueDeck;
import crystark.common.db.model.ek.EkUserDeck;

public class LeagueDeck extends Deck {
	private final EkLeagueDeck leagueDeck;

	public LeagueDeck(Database db, EkLeagueDeck leagueDeck) {
		super(db, makeUser(leagueDeck.BattleInfo));

		this.leagueDeck = leagueDeck;
	}

	public Float odds() {
		return leagueDeck.BetOdds;
	}

	public Long bets() {
		return leagueDeck.BetTotal;
	}

	private static EkUserDeck makeUser(EkDeckInfo deckInfo) {
		EkUserDeck ekUser = new EkUserDeck();
		ekUser.NickName = deckInfo.User.NickName;
		ekUser.Level = deckInfo.User.Level;
		ekUser.Cards = deckInfo.Cards;
		ekUser.Runes = deckInfo.Runes;
		return ekUser;
	}
}
