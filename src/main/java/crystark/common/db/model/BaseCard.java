package crystark.common.db.model;

import crystark.common.api.AbilityDesc;
import crystark.common.api.CardDesc;
import crystark.common.api.CardToken;
import crystark.common.api.CardTokenType;
import crystark.common.api.CardType;
import crystark.common.api.ConflictType;
import crystark.common.api.Game;
import crystark.common.db.Database;
import crystark.common.db.model.ek.EkCard;
import crystark.common.events.Warnings;
import crystark.ek.EkbsConfig;
import crystark.tools.Tools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BaseCard implements IBaseModel {
	private static final Logger	L	= LoggerFactory.getLogger(BaseCard.class);
	private static final Pattern INVALID_CHARS = Pattern.compile("[^A-z \\d'\\-)(]");
	private static final Pattern SPACES = Pattern.compile("\\s+");

	private final EkCard		card;
	private final Database		db;

	private String				cleanName;
	private CardType			type;
	private ConflictType		conflictType;

	private BaseCard			secondaryEvolve;
	private int					secondaryEvolveCount;

	public BaseCard(Database db, EkCard card) {
		this.db = db;
		this.card = card;
	}

	@Override
	public int id() {
		return card.CardId;
	}

	@Override
	public String name() {
		if (cleanName == null) {
			//			switch (id()) {
			//				case 439: // Pisces
			//					cleanName = baseName() + " Legacy";
			//					break;
			//				case 7223: // Ligh Paladin (legendary)
			//					cleanName = baseName() + " Dup";
			//					break;
			//				case 8072: // Persephone
			//					cleanName = baseName() + " Special";
			//					break;
			//				default:
			//					cleanName = baseName();
			//			}



			cleanName = INVALID_CHARS.matcher(baseName()).replaceAll(" ");
			cleanName = SPACES.matcher(cleanName).replaceAll(" ").trim();

		}
		return cleanName;
	}

	public String baseName() {
		return card.CardName;
	}

	public int sellPrice() {
		return card.Price;
	}

	public int frags() {
		return card.Fragment;
	}

	/**
	 * LoA/HF. Card specific frags (not universal) requeres to compose
	 * @return
	 */
	public int fragsCanUse(){
		return card.FragmentCanUse;
	}

	public int xpValue() {
		return Tools.parseInt(card.BaseExp);
	}

	public int costAt(int evo) {
		if (evo < 0 || 5 < evo) {
			throw new IllegalArgumentException("Card evo must be between 0 and 5");
		}

		int cost;
		if (evo <= 0 || card.EvoCost == 0) {
			// MR
			cost = card.Cost + (int) Math.ceil(evo / 2.0);
		}
		else {
			//LoA/HF
			cost = card.EvoCost;
		}
		return cost;
	}

	public int timer() {
		return card.Wait;
	}

	public Integer attackAt(int level) {
		if (level < 0 || 15 < level) {
			throw new IllegalArgumentException("Card level must be between 0 and 15");
		}
		return card.AttackArray.get(level);
	}

	public Integer hpAt(int level) {
		return card.HpArray.get(level);
	}

	public CardType type() {
		if (type == null) {
			type = CardType.fromRace(card.Race);
		}
		return type;
	}

	public int stars() {
		return card.Color;
	}

	public int limit() {
		return card.MaxInDeck;
	}

	public ConflictType conflictType() {
		if (conflictType == null) {

			switch (card.Conflict) {
				case 0:
					conflictType = ConflictType.none;
					break;
				case 1:
					conflictType = ConflictType.ds;
					break;
				case 2:
					conflictType = ConflictType.z1;
					break;
				case 3:
					conflictType = ConflictType.z2;
					break;
				case 4:
					conflictType = ConflictType.z3;
					break;
			}

		}
		return conflictType;
	}

	public boolean canEvo() {
		return "1".equals(card.CanEvo);
	}

	public Boolean inKwStore() {
		return card.ForceFightExchange != 0;
	}

	public Boolean isThiefDrop() {
		return card.Robber != 0;
	}

	public Boolean isThiefFragDrop() {
		return card.ThieveChipWeight != 0 || card.FragRobber != 0;
	}

	public Boolean isMazeDrop() {
		return card.Maze != 0;
	}

	public Boolean isMazeFragDrop() {
		return card.MazeChipWeight != 0;
	}

	public Boolean isDIReward() {
		return card.Boss != 0;
	}

	public boolean inFTPack() {
		return card.MagicCard != 0;
	}

	public boolean inFOHStore() {
		return card.LeagueWeight != 0;
	}

	public Integer fohStorePrice() {
		return card.LeaguePoint * 5;
	}

	public Integer kwStorePrice() {
		return card.ForceExchangeInitPrice;
	}

	public boolean isRaceGemPackDrop() {
		return card.RacePacket != 0;
	}
	public boolean isDungeonsDrop() {
		return card.DungeonsCard != 0;
	}
	public boolean isDungeonsFragDrop() {
		return card.DungeonsFrag != 0;
	}

/*
	public int           FragMagicCard;
	public int           FragMasterPacket;
	public int           FragMaze;
	public int           FragNewYearPacket;
	public int           FragRacePacket;
	public int           FragRobber;
	public int           FragSeniorPacket;
 */

	public BaseCard secondaryEvolveCard() {
		if (secondaryEvolve == null) {
			splitSecondaryEvolve();
		}
		return secondaryEvolve;
	}

	public int secondaryEvolveCount() {
		if (secondaryEvolve == null) {
			splitSecondaryEvolve();
		}
		return secondaryEvolveCount;
	}

	private void splitSecondaryEvolve() {
		if (!"0".equals(card.KeyCard) && card.KeyCard != null) {
			String[] parts = card.KeyCard.split("_");
			final int id = Tools.parseInt(parts[0]);
			if (this.id() == id) {
				secondaryEvolve = this;
			} else {
				secondaryEvolve = db.cards().get(id);
			}
			secondaryEvolveCount = Tools.parseInt(parts[1]);
		}
	}

	public List<BaseSkill> skillsAt(int level) {
		List<BaseSkill> skills = new ArrayList<>();

		addSkills(skills, card.Skill);
		if (level > 4)
			addSkills(skills, card.LockSkill1);
		if (level > 9)
			addSkills(skills, card.LockSkill2);

		return skills;
	}

	private void addSkills(List<BaseSkill> skillList, String skillString) {
		if (!skillString.isEmpty()) {
			String[] skillSplit = skillString.split("_");
			for (String skill : skillSplit) {
				BaseSkill baseSkill = db.skills().get(Tools.parseInt(skill));
				if (baseSkill == null) {
					Warnings.send("Card " + cleanName + " has invalid skill ref. " + skill);
					//L.warn("Card " + cleanName + " has invalid skill ref. " + skill);
				}
				else {
					skillList.add(baseSkill);
				}
			}
		}
	}

	/**
	 * Create a CardDesc for a level 10 card
	 */
	public CardDesc createCardDesc() {
		return createCardDescFor(10);
	}

	public CardDesc createCardDescFor(int level) {
		return createCardDesc(level, this.attackAt(level), this.hpAt(level), null);
	}

	public CardDesc createCardDesc(int level, Integer attack, Integer health, Integer evolution, Integer... newSkillIds) {
		List<AbilityDesc> newSkills = new ArrayList<>(newSkillIds.length);
		for (int i = 0; i < newSkillIds.length; i++) {
			if (newSkillIds[i] != 0) {
				BaseSkill skill = db.skills().get(newSkillIds[i]);
				if (skill == null) {
					Warnings.send("Missing skill ID " + newSkillIds[i] + " on card " + name() + '.');
				}
				else {
					newSkills.add(skill.createAbilityDesc());
				}
			}
		}

		return createCardDesc(level, attack, health, evolution, this.conflictType(), null, newSkills);
	}

	public CardDesc createCardDesc(int level, Integer evolution, List<CardToken> tokens, Collection<AbilityDesc> newAbilities) {
		int atk = this.attackAt(level);
		int hp = this.hpAt(level);
		ConflictType ct = this.conflictType();

		if (tokens != null) {
			for (CardToken t : tokens) {
				if (CardTokenType.event.equals(t.type)) {
					float multiplier = 1;
					switch (stars()) {
						case 3:
							multiplier = 1.5f;
							break;
						case 4:
							multiplier = 2f;
							break;
						case 5:
							multiplier = 3f;
							break;
					}
					atk *= multiplier;
					hp *= multiplier;
				}
				if (CardTokenType.legendary == t.type) {
					if (name().toLowerCase().contains("legendary")) {
						hp *= 180;
					}
					else {
						atk *= 0.1;
						hp *= 0.1;
					}
				}
				if (CardTokenType.attackMultiplier == t.type) {
					atk *= (Float) t.value;
				}
				if (CardTokenType.healthMultiplier == t.type) {
					hp *= (Float) t.value;
				}
				if (CardTokenType.conflict == t.type) {
					ct = (ConflictType) t.value;
				}
			}
		}

		return createCardDesc(level, atk, hp, evolution, ct, tokens, newAbilities);
	}

	public CardDesc createCardDesc(int level, Integer attack, Integer health, Integer evolution, ConflictType ct, List<CardToken> tokens, Collection<AbilityDesc> newAbilities) {
		int evo = fixEvo(level, evolution, newAbilities != null && !newAbilities.isEmpty());

		List<AbilityDesc> abilities = Stream
			.concat(
				this.skillsAt(level).stream()
					.map(BaseSkill::createAbilityDesc),
				newAbilities.stream())
			.collect(Collectors.toList());

		return new CardDesc(
			this.name(),
			this.costAt(evo),
			evo,
			level,
			this.timer(),
			attack == null ? this.attackAt(level) : attack,
			health == null ? this.hpAt(level) : health,
			this.type(),
			ct,
			this.limit(),
			abilities,
			tokens == null ? Collections.emptyList() : tokens);
	}

	private static final int fixEvo(int level, Integer evo, boolean hasNewSkill) {
		final Game game = EkbsConfig.current().game();
		if ((level > 10 || hasNewSkill)&& (game ==  Game.hf || game ==  Game.loa) ) evo = 5;
		int minEvo = Math.max(0, level - 10);
		if (evo == null) {
			return minEvo;
		}

		int fixed = Math.max(minEvo, evo);
		if (fixed != evo) {
			L.warn("Invalid evo value of " + evo + " adjusted to " + fixed + " cause level is " + level);
		}
		return fixed;
	}

	@Override
	public String toString() {
		return card.toString();
	}
}
