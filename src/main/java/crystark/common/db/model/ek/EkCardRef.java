package crystark.common.db.model.ek;

public class EkCardRef {
	public String     UUID;
	public String     CardId;
	public Integer    UserCardId;
	public String     Level;
	public Float 	  Attack;
	public Float      HP;
	public String     SkillNew;
	public Integer    Evolution;
	public Integer    WashTime;
}
