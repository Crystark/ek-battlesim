package crystark.common.db.model.ek;

import com.google.gson.annotations.SerializedName;

public class EkSkill {
	public int    SkillId;
	public String Name;
	public String Desc;
	/**
	 * 60, 53, 59 : points to other skill
	 */
	public int    AffectType;
	public String AffectValue;
	public String AffectValue2;
	/**
	 * Desperation 9
	 * Quickstrike 10
	 */
	@SerializedName(value = "LanchType", alternate = { " LanchType" })
	public int    LanchType;
	public int    LanchCondition;
	public int    LanchConditionValue;
	public String Type;
	//public int    SkillCategory;
	public int    EvoRank;

	@Override
	public String toString() {
		return String.valueOf(SkillId) + ", " + Name;
	}
}