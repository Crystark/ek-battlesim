package crystark.common.db.model.ek;

import java.util.List;

public class EkMapStageDetail {
	public String					MapStageId;
	public String					MapStageDetailId;
	public int						Rank;
	public String					Name;
	public List<EkMapStageLevel>	Levels;
}
