package crystark.common.db.model.ek;

import java.util.List;

public class EkData {
	public List<EkCard>			Cards;
	public List<EkRune>			Runes;
	public List<EkSkill>		Skills;
	public List<EkCompetitor>	Competitors;
	public EkLeague				LeagueNow;
	public EkUserDeck			DefendPlayer;
	public EkUserDeck			AttackPlayer;
	public List<EkBattleRound>	Battle;
}
