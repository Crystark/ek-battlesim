package crystark.common.db.model.ek;

public class EkMapStageLevel {
	public String	Level;
	public String	CardList;		// 341_15_688,432_15_688
	public String	RuneList;		// 37_4,38_4,39_4,40_4
	public String	HeroLevel;
	public String	BonusWin;		// "Exp_160,Coins_240"
	public String	FirstBonusWin;	// Card_104 or Rune_2
	public String	BonusExplore;	// Exp_160,Coins_240
}
