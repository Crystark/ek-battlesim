package crystark.common.db.model.ek;

public class EkRune {
	public String	RuneId;
	public String	RuneName;
	public String	Condition;
	public Integer	SkillTimes;
	public Integer	LockSkill1;
	public Integer	LockSkill2;
	public Integer	LockSkill3;
	public Integer	LockSkill4;
	public Integer	LockSkill5;

	/*public int SkillConditionSlide;
	public int SkillConditionType;
	public int SkillConditionRace;
	public int SkillConditionColor;
	public int SkillConditionCompare;
	public int SkillConditionValue;*/
}
