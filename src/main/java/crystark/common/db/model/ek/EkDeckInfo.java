package crystark.common.db.model.ek;

import java.util.List;

public class EkDeckInfo {
	public EkUserDeck		User;
	public List<EkCardRef>	Cards;
	public List<EkRuneRef>	Runes;
}
