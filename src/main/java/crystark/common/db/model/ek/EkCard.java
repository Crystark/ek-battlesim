package crystark.common.db.model.ek;

import java.util.List;

public class EkCard {
	public int			CardId;
	public String			CardName;

	public int			Color;					// stars
	/**
	 * tundra 1
	 * forest 2
	 * swamp 3
	 * moutain 4
	 * gold 95
	 * food 96
	 * hydra 97
	 * special 99
	 * demon 100
	 */
	public int           Race;
	public int           Cost;
	public int           Wait;
	public String        Skill;
	public String        LockSkill1;
	public String        LockSkill2;
	public List<Integer> HpArray;
	public List<Integer> AttackArray;
	public String        CanEvo;
	public String        KeyCard;                // secondary evolve material
	public int           Conflict;
	public int           MaxInDeck;
	public int           ForceFightExchange;        // in KW Store
	public int           ForceExchangeInitPrice;    // KW store price
	public int           LeagueWeight;            // FoH store weight
	public int           LeaguePoint;            // FoH store price
	public int           Robber;                    // Thief Drop
	public int           ThieveChipWeight;        // Thief frag Drop
	public int           Maze;                    // Maze Drop
	public int           MazeChipWeight;            // Maze frag Drop
	public int           Boss;                    // DI reward
	public int           MagicCard;                // in FT Pack
	public int           Price;
	public String        BaseExp;
	public int           Fragment;    //Frags to compose

	//============== LoA/HF ===========================================
	public int           EvoCost;
	public int           FragmentCanUse; //Card specific frags (not universal) requeres to compose
	public int 			 RacePacket;
	public int		     RacePacketRoll;
	public int 			 DungeonsCard;
	public int			 DungeonsFrag;
	public int           FragMagicCard;
	public int           FragMasterPacket;
	public int           FragMaze;
	public int           FragNewYearPacket;
	public int           FragRacePacket;
	public int           FragRobber;
	public int           FragSeniorPacket;







	@Override
	public String toString() {
		return String.valueOf(CardId) + ", " + CardName;
	}
}
