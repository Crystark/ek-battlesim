package crystark.common.db.model.ek;

import java.util.List;

public class EkMapDto {
	public Integer		status;
	public String		message;
	public List<EkMap>	data;
	public EkVersion	version;
}
