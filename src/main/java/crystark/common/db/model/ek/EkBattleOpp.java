package crystark.common.db.model.ek;

import java.util.List;

public class EkBattleOpp {
	public String		UUID;
	public Float		HP;
	public Integer		Opp;
	public List<Object>	Target;
	public Float		Value;
}
