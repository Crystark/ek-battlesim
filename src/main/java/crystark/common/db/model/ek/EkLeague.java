package crystark.common.db.model.ek;

import java.util.List;

public class EkLeague {
	public Integer					LeagueId;
	public Integer					RoundNow;
	public List<List<EkLeagueDeck>>	RoundResult;
	public EkBattleCondition		Condition;
}
