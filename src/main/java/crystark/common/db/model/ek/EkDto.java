package crystark.common.db.model.ek;

public class EkDto {
	public Integer		status;
	public String		message;
	public EkData		data;
	public EkVersion	version;
}
