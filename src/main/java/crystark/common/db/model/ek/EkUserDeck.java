package crystark.common.db.model.ek;

import java.util.List;

public class EkUserDeck {
	public String			NickName;
	public int				Level;
	public List<EkCardRef>	Cards;
	public List<EkRuneRef>	Runes;
}
