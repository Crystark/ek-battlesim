package crystark.common.db.model;

import crystark.common.db.Database;
import crystark.common.db.model.ek.EkMapStageDetail;
import crystark.common.db.model.ek.EkMapStageLevel;
import crystark.tools.Tools;

import java.util.List;
import java.util.stream.Collectors;

public class BaseMap implements IBaseModel, Comparable<BaseMap> {
	private final EkMapStageDetail	map;
	private final Database			db;
	private final int				id;

	private List<BaseMapLevel>		levels;

	public BaseMap(Database db, EkMapStageDetail map) {
		this.db = db;
		this.map = map;
		this.id = Tools.parseInt(map.MapStageDetailId);
	}

	@Override
	public int id() {
		return id;
	}

	@Override
	public String name() {
		return map.Name;
	}

	@Override
	public int compareTo(BaseMap o) {
		return id() - o.id();
	}

	public Object number() {
		return map.MapStageId + '-' + (map.Rank + 1);
	}

	public boolean hasLevels() {
		return map.Levels != null;
	}

	public List<BaseMapLevel> getLevels() {
		if (levels == null && map.Levels != null) {
			levels = map.Levels.stream()
				.map(BaseMapLevel::new)
				.sorted()
				.collect(Collectors.toList());
		}
		return levels;
	}

	public class BaseMapLevel implements Comparable<BaseMapLevel> {
		private final EkMapStageLevel	level;
		private final int				id;

		private Reward					firstWinReward;

		BaseMapLevel(EkMapStageLevel level) {
			this.level = level;
			this.id = Tools.parseInt(level.Level);
		}

		public int id() {
			return id;
		}

		public Reward firstWinReward() {
			if (firstWinReward == null) {
				firstWinReward = new Reward(level.FirstBonusWin);
			}
			return firstWinReward;
		}

		@Override
		public int compareTo(BaseMapLevel o) {
			return id() - o.id();
		}

		public class Reward {
			String reward = "";

			Reward(String string) {
				if (string == null) return;

				final String[] rewards = string.split(",");
				for (String rewardStr : rewards) {
					if (reward.length() > 0) reward += ", ";
					String[] parts = rewardStr.split("_");
					switch (parts[0]) {
						case "Card":
							final BaseCard baseCard = db.cards().get(Tools.parseInt(parts[1]));
							reward = reward + baseCard.name() + " (" + baseCard.stars() + "*)";
							break;
						case "Rune":
							reward = reward + db.runes().get(Tools.parseInt(parts[1])).name();
							break;
						case "CardChip":
							reward = reward + "Frags: " + db.cards().get(Tools.parseInt(parts[1])).name() + " x " + parts[2];
							break;
						default:
							reward = string;
							break;
					}
				}
			}

			@Override
			public String toString() {
				if (reward != null) {
					return reward;
				}
				return "n/a";
			}
		}
	}
}
