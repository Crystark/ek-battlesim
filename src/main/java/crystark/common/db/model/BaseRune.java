package crystark.common.db.model;

import crystark.common.api.RuneDesc;
import crystark.common.db.Database;
import crystark.common.db.model.ek.EkRune;
import crystark.tools.Tools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Pattern;

public class BaseRune implements IBaseModel, Comparable<BaseRune> {
	private static final Logger  L             = LoggerFactory.getLogger(BaseRune.class);
	private static final Pattern BAD_APOSTRAPH = Pattern.compile("’", Pattern.LITERAL);
	private static final Pattern BAD_SPACE = Pattern.compile("[  ]");
	private final EkRune	rune;
	private final Database	db;
	private String fixedName;

	public BaseRune(Database db, EkRune rune) {
		this.db = db;
		this.rune = rune;
	}

	@Override
	public int id() {
		return Tools.parseInt(rune.RuneId);
	}

	@Override
	public String name() {
		if (fixedName == null) {
			String name = BAD_APOSTRAPH.matcher(rune.RuneName).replaceAll("'");
			name = BAD_SPACE.matcher(name).replaceAll(" ");
			fixedName = name.trim();
		}
		return fixedName;
	}

	public String condition() {

		String name = rune.Condition;
		name = BAD_APOSTRAPH.matcher(name).replaceAll("'");
		name = BAD_SPACE.matcher(name).replaceAll(" ");

		return name;
	}

	public int times() {
		return rune.SkillTimes;
	}

	public BaseSkill skillsAt(int level) {
		Integer skillId;
		switch (level) {
			case 0:
				skillId = rune.LockSkill1;
				break;
			case 1:
				skillId = rune.LockSkill2;
				break;
			case 2:
				skillId = rune.LockSkill3;
				break;
			case 3:
				skillId = rune.LockSkill4;
				break;
			case 4:
				skillId = rune.LockSkill5;
				break;
			default:
				throw new IllegalArgumentException("Rune level must be between 0 and 4");
		}
		if (skillId == null) {
			throw new IllegalArgumentException("Rune " + name() + " requires a custom value. e.g. " + name() + ":value");
		}
		final BaseSkill baseSkill = db.skills().get(skillId);
		if (baseSkill == null) {
			L.warn("Rune " + name() + " has invalid skill ref."+ skillId);
		}
		/*if (baseSkill == null) throw new IllegalArgumentException("Rune " + name() + " uses unreleased skill. Skill id="
			+ skillId);*/
		return baseSkill;
	}

	public Integer valueAt(int level) {
		// TODO Unsafe cast to be made safe
		return (Integer) skillsAt(level).value();
	}

	public boolean isBase() {
		return id() < 100000;
	}

	public RuneDesc createRuneDescFor(Integer level) {
		return createRuneDesc(valueAt(level));
	}

	public RuneDesc createRuneDesc() {
		return createRuneDescFor(4);
	}

	public RuneDesc createRuneDesc(Integer value) {
		return new RuneDesc(name(), times(), value);
	}

	@Override
	public int compareTo(BaseRune o) {
		return this.name().compareTo(o.name());
	}
}
