package crystark.common.db.model;

public interface IBaseModel {
	public int id();

	public String name();
}
