package crystark.common.db.model;

import crystark.common.api.CardDesc;
import crystark.common.api.DeckDesc;
import crystark.common.api.RuneDesc;
import crystark.common.db.Database;
import crystark.common.db.model.ek.EkCardRef;
import crystark.common.db.model.ek.EkRuneRef;
import crystark.common.db.model.ek.EkUserDeck;
import crystark.common.events.Warnings;
import crystark.ek.battlesim.task.TaskException;
import crystark.tools.Tools;

import java.util.ArrayList;
import java.util.List;

public class Deck {

	// HP table for lvl 1 - 149
	private static final int[]	HP_PER_LEVEL	= new int[] { 0,
		1000, 1070, 1140, 1210, 1280, 1350, 1420, 1490, 1560, 1630, //+0; +70
		1800, 1880, 1960, 2040, 2120, 2200, 2280, 2360, 2440, 2520, //+100; +80
		2800, 2890, 2980, 3070, 3160, 3250, 3340, 3430, 3520, 3610, //+200; +90
		4000, 4100, 4200, 4300, 4400, 4500, 4600, 4700, 4800, 4900, //+300; +100
		5400, 5510, 5620, 5730, 5840, 5950, 6060, 6170, 6280, 6390, //+400; +110
		7000, 7120, 7240, 7360, 7480, 7600, 7720, 7840, 7960, 8080, //+500; +120
		8800, 8930, 9060, 9190, 9320, 9450, 9580, 9710, 9840, 9970, //+600; +130
		10800, 10940, 11080, 11220, 11360, 11500, 11640, 11780, 11920, 12060, //+700; +140
		13000, 13150, 13300, 13450, 13600, 13750, 13900, 14050, 14200, 14350,  //+800; +150
		15400, 15560, 15720, 15880, 16040, 16200, 16360, 16520, 16680, 16840, // +900; +160
		17000, 17160, 17320, 17480, 17640, 17800, 17960, 18120, 18280, 18440, // +0; +160
		18600, 18760, 18920, 19080, 19240, 19400, 19560, 19720, 19880, 20040, // +0; +160
		20200, 20360, 20520, 20680, 20840, 21000, 21160, 21320, 21480, 21640, // +0; +160
		21800, 21960, 22120, 22280, 22440, 22600, 22760, 22920, 23080, 23240, // +0; +160
		23540, 23840, 24140, 24440, 24740, 25040, 25340, 25640, 25940, 26240 // +140; +300
	};

	protected final Database	db;
	protected final EkUserDeck	ekUser;

	public Deck(Database db, EkUserDeck ekUser) {
		this.db = db;
		this.ekUser = ekUser;
	}

	public int health() {
		return calcHp(ekUser.Level);
	}

	public DeckDesc toDeckDesc() {
		List<CardDesc> cards = new ArrayList<>();
		for (EkCardRef ekCardRef : ekUser.Cards) {
			if (ekCardRef.UserCardId < 110000000) {
				BaseCard baseCard = db.cards().get(Tools.parseInt(ekCardRef.CardId));
				if (baseCard == null) {
					throw new TaskException("Unknown card id " + ekCardRef.CardId + ". Make sure you're using the lastest version of the sim. If so please run `ek-battlesim -uldb` first.");
				}
				Integer evo = ekCardRef.Evolution;
				if (evo != null && evo > 5) {
					Warnings.send(ekUser.NickName + "'s card " + baseCard.baseName() + " with userCardId " + ekCardRef.UserCardId + " has an invalid evo of +" + evo);
					evo = 5;
				}
				Integer att = ekCardRef.Attack == null ? null : Math.round(ekCardRef.Attack);
				Integer hp = ekCardRef.HP == null ? null : Math.round(ekCardRef.HP);
				cards.add(baseCard.createCardDesc(Tools.parseInt(ekCardRef.Level), att , hp, evo, ekCardRef.SkillNew == null ? 0 : Tools.parseInt(ekCardRef.SkillNew)));
			}
		}

		List<RuneDesc> runes = new ArrayList<>(4);
		for (EkRuneRef ekRuneRef : ekUser.Runes) {
			runes.add(db.runes().get(Tools.parseInt(ekRuneRef.RuneId)).createRuneDescFor(Tools.parseInt(ekRuneRef.Level)));
		}

		return new DeckDesc(ekUser.NickName, ekUser.NickName, ekUser.Level, health(), cards, runes, 0, false, false);
	}

	public static final int calcHp(int level) {
		//lvl 141 - HP 23540
		//lvl 140 - HP 23240
		if (level < 140) {
			return HP_PER_LEVEL[level];
		} else {
			return 23540 + (level - 141) * 300;
		}
	}
}
