package crystark.common.db.model;

import crystark.common.api.BattleDesc;
import crystark.common.api.DeckDesc;
import crystark.common.db.Database;
import crystark.common.db.model.ek.EkLeague;
import crystark.common.db.model.ek.EkLeagueDeck;
import crystark.ek.battlesim.task.TaskException;
import rx.Observable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class League {
	private final Database					db;
	private final EkLeague					league;
	private Map<Integer, List<LeagueDeck>>	leagueDecks;

	public League(Database db, EkLeague league) {
		this.db = db;
		this.league = league;
	}

	public int season() {
		return league.LeagueId;
	}

	public String condition() {
		return league.Condition.Desc;
	}

	private List<LeagueDeck> leagueDecksForRound(int round) {
		int r = round - 1;
		if (leagueDecks == null) {
			leagueDecks = new HashMap<>();
		}

		if (!leagueDecks.containsKey(r)) {
			ArrayList<LeagueDeck> roundLeagueBattles = new ArrayList<>();
			for (EkLeagueDeck ekLeagueBattle : league.RoundResult.get(r)) {
				roundLeagueBattles.add(new LeagueDeck(db, ekLeagueBattle));
			}
			leagueDecks.put(r, roundLeagueBattles);
		}

		return leagueDecks.get(r);
	}

	public Observable<DeckDesc> deckDescsForRound(int round) {
		return Observable
			.from(leagueDecksForRound(round))
			.map(LeagueDeck::toDeckDesc);
	}

	public Observable<BattleDesc> battlesForRound(int round) {
		List<LeagueDeck> ekBattles = leagueDecksForRound(round);
		if (ekBattles.size() != (8 / Math.pow(2, round - 1))) {
			throw new TaskException("Incomplete EK data. Please retry in a few minutes.");
		}
		return Observable
			.from(ekBattles)
			.buffer(2)
			.map((List<LeagueDeck> pair) -> {
				LeagueDeck leagueDeckA = pair.get(0);
				LeagueDeck leagueDeckD = pair.get(1);

				HashMap<String, Object> meta = new HashMap<>();
				meta.put("season", season());
				meta.put("round", round);
				meta.put("aOdds", leagueDeckA.odds());
				meta.put("aBets", leagueDeckA.bets());
				meta.put("dOdds", leagueDeckD.odds());
				meta.put("dBets", leagueDeckD.bets());

				return new BattleDesc(leagueDeckA.toDeckDesc(), leagueDeckD.toDeckDesc(), meta);
			});
	}

	public Observable<BattleDesc> nextBattles() {
		return battlesForRound(Math.min(league.RoundNow, 3)); // RoundNow may be == 4 between 23h and 00h server time
	}
}
