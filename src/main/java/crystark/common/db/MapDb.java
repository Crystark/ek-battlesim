package crystark.common.db;

import java.util.List;

import crystark.common.db.model.BaseMap;
import crystark.common.db.model.ek.EkMap;
import rx.Observable;

public class MapDb extends Db<BaseMap> {
	public MapDb(Database db) {
		super(db);
	}

	public MapDb addAll(List<EkMap> elements) {
		super.addAll(Observable
			.from(elements)
			.flatMap(m -> Observable.from(m.MapStageDetails))
			.map(r -> new BaseMap(db, r)));

		return this;
	}
}
