package crystark.common.transform;

import java.util.function.Function;

public class DefaultValueTransformer<T> implements Function<T, T> {
	private T defaultValue;

	private DefaultValueTransformer(T defaultValue) {
		this.defaultValue = defaultValue;
	}

	public static <T> DefaultValueTransformer<T> create(T defaultValue) {
		return new DefaultValueTransformer<T>(defaultValue);
	}

	@Override
	public T apply(T t) {
		return t == null ? defaultValue : t;
	}
}
