package crystark.common.transform;

import crystark.common.api.AbilityDesc;
import crystark.common.api.CardDesc;
import crystark.common.api.CardToken;
import crystark.common.api.CardType;
import crystark.common.api.ConflictType;

import java.util.List;
import java.util.function.Function;

public class CardDescTransformer implements Function<CardDesc, CardDesc> {
	private boolean										anyTransformer		= false;

	private Function<List<CardToken>, List<CardToken>>	tokensTransformer	= Function.identity();
	private Function<Long, Long>						healthTransformer	= Function.identity();
	private Function<Integer, Integer>						attackTransformer	= Function.identity();

	private CardDescTransformer() {}

	public static CardDescTransformer create() {
		return new CardDescTransformer();
	}

	@Override
	public CardDesc apply(CardDesc dd) {
		if (anyTransformer) {
			String name = dd.name;
			int cost = dd.cost;
			int evolution = dd.evolution;
			int level = dd.level;
			int timer = dd.timer;
			int attack = attackTransformer.apply(dd.attack);
			long health = healthTransformer.apply(dd.health);
			CardType type = dd.type;
			ConflictType conflictType = dd.conflictType;
			Integer limit = dd.limit;
			List<AbilityDesc> abilities = dd.abilities;
			List<CardToken> tokens = tokensTransformer.apply(dd.tokens);

			return new CardDesc(name, cost, evolution, level, timer, attack, health, type, conflictType, limit, abilities, tokens);
		}
		return dd;
	}

	public CardDescTransformer withTokensTransformer(Function<List<CardToken>, List<CardToken>> tokensTransformer) {
		this.tokensTransformer = tokensTransformer;
		this.anyTransformer = true;
		return this;
	}

	public CardDescTransformer withHealthTransformer(Function<Long, Long> healthTransformer) {
		this.healthTransformer = healthTransformer;
		this.anyTransformer = true;
		return this;
	}

	public CardDescTransformer withAttackTransformer(Function<Integer, Integer> attackTransformer) {
		this.attackTransformer = attackTransformer;
		this.anyTransformer = true;
		return this;
	}

}
