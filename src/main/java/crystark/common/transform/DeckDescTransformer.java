package crystark.common.transform;

import crystark.common.api.CardDesc;
import crystark.common.api.DeckDesc;
import crystark.common.api.RuneDesc;

import java.util.List;
import java.util.function.Function;

public class DeckDescTransformer implements Function<DeckDesc, DeckDesc> {
	private Function<DeckDesc, String>			deckNameTransformer			= dd -> dd.deckName;
	private Function<DeckDesc, String>			playerNameTransformer		= dd -> dd.playerName;
	private Function<DeckDesc, Integer>			playerLevelTransformer		= dd -> dd.playerLevel;
	private Function<DeckDesc, Integer>			healthTransformer			= dd -> dd.health;
	private Function<DeckDesc, List<CardDesc>>	cardsTransformer			= dd -> dd.cardsDescs;
	private Function<DeckDesc, List<RuneDesc>>	runesTransformer			= dd -> dd.runesDescs;
	private Function<DeckDesc, Integer>			skipShuffleTransformer		= dd -> dd.skipShuffle;
	private Function<DeckDesc, Boolean>			alwaysStartTransformer		= dd -> dd.alwaysStart;
	private Function<DeckDesc, Boolean>			infiniteHpTransformer		= dd -> dd.infiniteHp;
	private Function<DeckDesc, Boolean>			bypassValidationTransformer	= dd -> dd.bypassValidation;

	private DeckDescTransformer() {}

	public static DeckDescTransformer create() {
		return new DeckDescTransformer();
	}

	@Override
	public DeckDesc apply(DeckDesc dd) {
		String deckName = deckNameTransformer.apply(dd);
		String playerName = playerNameTransformer.apply(dd);
		Integer playerLevel = playerLevelTransformer.apply(dd);
		Integer health = healthTransformer.apply(dd);
		List<CardDesc> cardsDescs = cardsTransformer.apply(dd);
		List<RuneDesc> runesDescs = runesTransformer.apply(dd);
		int skipShuffle = skipShuffleTransformer.apply(dd);
		boolean alwaysStart = alwaysStartTransformer.apply(dd);
		boolean infiniteHp = infiniteHpTransformer.apply(dd);
		boolean bypassValidation = bypassValidationTransformer.apply(dd);

		return new DeckDesc(deckName, playerName, playerLevel, health, cardsDescs, runesDescs, skipShuffle, alwaysStart, infiniteHp, dd.waitToPlay, bypassValidation);
	}

	public DeckDescTransformer withDeckNameTransformer(Function<DeckDesc, String> deckNameTransformer) {
		this.deckNameTransformer = deckNameTransformer;
		return this;
	}

	public DeckDescTransformer withPlayerNameTransformer(Function<DeckDesc, String> playerNameTransformer) {
		this.playerNameTransformer = playerNameTransformer;
		return this;
	}

	public DeckDescTransformer withPlayerLevelTransformer(Function<DeckDesc, Integer> playerLevelTransformer) {
		this.playerLevelTransformer = playerLevelTransformer;
		return this;
	}

	public DeckDescTransformer withHealthTransformer(Function<DeckDesc, Integer> healthTransformer) {
		this.healthTransformer = healthTransformer;
		return this;
	}

	public DeckDescTransformer withCardsTransformer(Function<DeckDesc, List<CardDesc>> cardsTransformer) {
		this.cardsTransformer = cardsTransformer;
		return this;
	}

	public DeckDescTransformer withRunesTransformer(Function<DeckDesc, List<RuneDesc>> runesTransformer) {
		this.runesTransformer = runesTransformer;
		return this;
	}

	public DeckDescTransformer withSkipShuffleTransformer(Function<DeckDesc, Integer> skipShuffleTransformer) {
		this.skipShuffleTransformer = skipShuffleTransformer;
		return this;
	}

	public DeckDescTransformer withAlwaysStartTransformer(Function<DeckDesc, Boolean> alwaysStartTransformer) {
		this.alwaysStartTransformer = alwaysStartTransformer;
		return this;
	}

	public DeckDescTransformer withInfiniteHpTransformer(Function<DeckDesc, Boolean> infiniteHpTransformer) {
		this.infiniteHpTransformer = infiniteHpTransformer;
		return this;
	}

	public DeckDescTransformer withBypassValidationTransformer(Function<DeckDesc, Boolean> bypassValidationTransformer) {
		this.bypassValidationTransformer = bypassValidationTransformer;
		return this;
	}

}
