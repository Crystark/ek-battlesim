package crystark.common.format;

import crystark.common.api.AbilityDesc;
import crystark.common.api.LaunchType;
import crystark.common.db.Database;
import crystark.common.db.SkillDb;
import crystark.common.db.model.BaseSkill;
import crystark.tools.Tools;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class AbilityDescFormat {

	private static final Pattern	PATTERN_SPLIT_COLON		= Pattern.compile("(?<![\\\\]):");
	private static final Pattern	PATTERN_UNESCAPE_COLON	= Pattern.compile("\\:", Pattern.LITERAL);
	private static final Pattern BAD_SPACE            = Pattern.compile("\\s+");

	private static String unescapeColons(String v) {
		return PATTERN_UNESCAPE_COLON.matcher(v).replaceAll(":").trim();
	}

	public static AbilityDesc parse(String description) {
		SkillDb skillsDb = Database.get().skills();

		LaunchType launchType = null;
		description = BAD_SPACE.matcher(description).replaceAll(" ");
		String toParse = description.trim();

		// launch prefix / name
		String[] launchTypeAndSkillName = toParse.split("_", 2); //skill values may separates by '_' too
		if (launchTypeAndSkillName.length > 1 && launchTypeAndSkillName[0].length() <= 2) {
			toParse = launchTypeAndSkillName[launchTypeAndSkillName.length - 1].trim();
			String lt = launchTypeAndSkillName[0].trim();
			launchType = LaunchType.byPrefix(lt.toLowerCase());
			if (launchType == null) {
				throw new FormatException("Invalid ability prefix '" + lt + "'. Allowed values: " + Arrays.toString(LaunchType.values()));
			}
		}

		// Check if it's an IGN description
		BaseSkill skill = skillsDb.get(toParse);
		if (skill != null) {
			return skill.createAbilityDesc(launchType);
		}


		// Split name part / value part
		String[] splitValues = PATTERN_SPLIT_COLON.split(toParse, 3);
		toParse = unescapeColons(splitValues[0]).trim();

		// Consider as custom description and try to find value.
		Object value = null;
		Object value2 = null;

		if (splitValues.length > 1) {
			value = splitValues[1].trim();
			try {
				value = Tools.parseInt(value.toString());
			}
			catch (NumberFormatException e) {
				// Not a number, keep it as a string
				value = unescapeColons(value.toString());
			}

			if (splitValues.length > 2) {
				value2 = splitValues[2].trim();
				try {
					value2 = Tools.parseInt(value2.toString());
				}
				catch (NumberFormatException e) {
					// Not a number, keep it as a string
					value2 = unescapeColons(value2.toString());
				}
			}
		}


		String name = toParse;
		List<BaseSkill> skillGroup = skillsDb.getByAbilityName(name);
		if (skillGroup == null) {
			throw new FormatException("Ability '" + name + "' does not exist.");
		}
		else {
			BaseSkill first = skillGroup.get(0);
			if (first.value() != null && value == null) {
				throw new FormatException("Ability '" + name + "' requires a value (i.e. '" + name + ":value').");
			}
			if (first.value2() != null && value2 == null) {
				throw new FormatException("Ability '" + name + "' requires a second value (i.e. '" + name + ":value:value2').");
			}


			// Frost Shock 20 and Lighting Cut launch type is 'normal' (NOT QS)
			// Frost Shock 15 (Lighting Cut 15) is QS and it goes first in skillGroup that makes custom Lighting Cut as QS_Lighting Cut
			// Sacrifice 1-9 are QS and custom description 'Sacrifice:100' has to make it as QS.
			// Let's take launch type from skillGroup if all skills in group has same launch type only,
			// overwise custom description has to specify launch type explicitly.
			if (launchType == null) {
				if (skillsDb.hasDifferentLaunchType(name)) {
					// Custom description has to specify launch type explicitly, assume 'normal' if not specified
					launchType = LaunchType.normal;
				} else {
					launchType = first.launchType();
				}
			}

			return new AbilityDesc(
				first.cleanName(),
				first.abilityName(),
				first.name(),
				launchType,
				value,
				value2,
				false, 0);
		}
	}
}
