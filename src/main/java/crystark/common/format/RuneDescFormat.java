package crystark.common.format;

import crystark.common.api.RuneDesc;
import crystark.common.db.Database;
import crystark.common.db.model.BaseRune;
import crystark.tools.Tools;

public class RuneDescFormat {
	public static RuneDesc parseSilent(String description) {
		try {
			return parse(description);
		}
		catch (IllegalArgumentException e) {
			return null;
		}
	}

	public static RuneDesc parse(String description) {
		String[] s = description.split(":");
		String name = s[0].trim();

		BaseRune rune = Database.get().runes().get(name);
		if (rune == null) {
			throw new IllegalArgumentException("Could not find a rune named " + name);
		}

		if (s.length > 1) {
			String levelOrValue = s[1].trim();
			if (levelOrValue.toLowerCase().startsWith("l")) {
				return rune.createRuneDescFor(Tools.parseInt(levelOrValue.substring(1)));
			}
			else {
				return rune.createRuneDesc(Tools.parseInt(levelOrValue));
			}
		}

		return rune.createRuneDesc();
	}

	public static String toFullDescription(RuneDesc desc) {
		return desc.toString();
	}
}
