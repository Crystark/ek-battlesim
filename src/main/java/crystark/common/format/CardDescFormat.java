package crystark.common.format;

import crystark.common.api.AbilityDesc;
import crystark.common.api.CardDesc;
import crystark.common.api.CardToken;
import crystark.common.api.CardTokenType;
import crystark.common.api.CardType;
import crystark.common.api.ConflictType;
import crystark.common.db.Database;
import crystark.common.db.model.BaseCard;
import crystark.common.events.Warnings;
import crystark.tools.Tools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * name,[level,[evo,[newSkill1,...,newSkillN]]]
 */
public class CardDescFormat {
	// name, cost, timer, atk, hp, type, [abilities]
	private static final Pattern	PATTERN_DESCRIPTION		= Pattern.compile("^([A-z][^,]*),\\s*([0-9]+)\\s*,\\s*([0-9]+)\\s*,\\s*([0-9]+)\\s*,\\s*([0-9]+)\\s*,\\s*([A-z]+)\\s*(?:,\\s*(.*))?$");
	private static final Pattern	PATTERN_SPLIT_COMMA		= Pattern.compile("(?<![\\\\]),");
	private static final Pattern	PATTERN_UNESCAPE_COMMA	= Pattern.compile("\\,", Pattern.LITERAL);

	public static CardDesc parse(String description) {
		CardDesc deckDesc = parseSilent(description);
		if (deckDesc == null) {
			throw new IllegalArgumentException("Not a valid description: " + description);
		}
		return deckDesc;
	}

	public static CardDesc parseSilent(String description) {
		CardDesc deckDesc = parseFullSilent(description);
		return deckDesc == null ? parseSimpleSilent(description) : deckDesc;
	}

	private static String unescapeCommas(String desc) {
		return PATTERN_UNESCAPE_COMMA.matcher(desc).replaceAll(",").trim();
	}

	public static CardDesc parseSimple(String description) {
		int level = 10;
		Integer evo = null;
		List<CardToken> tokens;
		List<AbilityDesc> newAbilities = new ArrayList<>();

		String[] s = PATTERN_SPLIT_COMMA.split(description);
		NameAndTokens nameAndTokens = NameAndTokens.parse(s[0]);
		String name = nameAndTokens.name;
		tokens = nameAndTokens.tokens;

		BaseCard card = Database.get().cards().get(name);
		if (card == null) {
			throw new IllegalArgumentException("Could not find a card named " + name);
		}

		if (s.length > 1) {
			String[] levelAndEvo = s[1].split("\\+");
			level = Tools.parseInt(levelAndEvo[0].trim());

			if (levelAndEvo.length > 1) {
				evo = Tools.parseInt(levelAndEvo[1].trim());
			}
			if (s.length > 2) {
				for (int i = 2; i < s.length; i++) {
					AbilityDesc desc = AbilityDescFormat.parse(unescapeCommas(s[i]));
					if (desc != null) {
						newAbilities.add(desc);
					}
				}
			}
		}

		return card.createCardDesc(level, evo, tokens, newAbilities);
	}

	public static CardDesc parseSimpleSilent(String description) {
		try {
			return parseSimple(description);
		}
		catch (IllegalArgumentException e) {
			//			L.debug("parseSimple failed", e);
			Warnings.getAndReset();
			return null;
		}
	}

	public static CardDesc parseFull(String description) {
		Matcher m = PATTERN_DESCRIPTION.matcher(description.trim());
		if (m.matches()) {
			List<String> parts = new ArrayList<String>(6);
			for (int i = 1; i <= m.groupCount(); i++) {
				String part = m.group(i);
				parts.add(part == null ? null : part.trim());
			}

			String abilities = parts.get(6);
			List<AbilityDesc> abilityDescs = new ArrayList<AbilityDesc>();
			if (abilities != null) {
				for (String ability : PATTERN_SPLIT_COMMA.split(abilities)) {
					AbilityDesc abilityDesc = AbilityDescFormat.parse(unescapeCommas(ability));
					if (abilityDesc != null) {
						abilityDescs.add(abilityDesc);
					}
				}
			}

			String typeAsString = parts.get(5);
			NameAndTokens nameAndTokens = NameAndTokens.parse(parts.get(0));
			CardType type;
			try {
				type = CardType.valueOf(typeAsString.toLowerCase());
			}
			catch (IllegalArgumentException ex) {
				throw new FormatException('\'' + typeAsString + "' is not a valid card type. Allowed values are: " + Arrays.toString(CardType.values()));
			}

			ConflictType conflictType = ConflictType.none;
			if (nameAndTokens.tokens != null) {
				for (CardToken t : nameAndTokens.tokens) {
					if (CardTokenType.conflict.equals(t.type)) {
						conflictType = (ConflictType) t.value;
						break;
					}
				}
			}

			try {
				return new CardDesc(
					nameAndTokens.name,
					Tools.parseInt(parts.get(1)),
					0, // TODO calc this
					10, // TODO calc this
					Tools.parseInt(parts.get(2)),
					Tools.parseInt(parts.get(3)),
					Tools.parseInt(parts.get(4)),
					type,
					conflictType,
					0,
					abilityDescs,
					nameAndTokens.tokens);
			}
			catch (NumberFormatException ex) {
				throw new FormatException("Max value for numbers is " + Integer.MAX_VALUE);
			}
		}
		else {
			throw new IllegalArgumentException(description + " is not a valid card description.");
		}
	}

	public static CardDesc parseFullSilent(String description) {
		try {
			return parseFull(description);
		}
		catch (IllegalArgumentException e) {
			//			System.out.println(e);
			return null;
		}
	}

	public static String toFullDescription(CardDesc t) {
		return String.join(", ", t.toStringList());
	}

	private static class NameAndTokens {
		String			name;
		List<CardToken>	tokens;

		NameAndTokens(String name, List<CardToken> tokens) {
			super();
			this.name = name;
			this.tokens = tokens;
		}

		static NameAndTokens parse(String str) {
			String[] tokensAndName = str.trim().split(":");

			String name = tokensAndName[tokensAndName.length - 1].trim();
			List<CardToken> tokens = Stream
				.of(tokensAndName)
				.limit(tokensAndName.length - 1)
				.map(t -> {
					String[] tokenAndValue = t.trim().toLowerCase().split("#");
					CardTokenType type = CardTokenType.byToken(tokenAndValue[0]);
					Object value = tokenAndValue.length > 1 ? type.valueParser.apply(tokenAndValue[1]) : null;
					return new CardToken(type, value);
				})
				.collect(Collectors.toList());

			return new NameAndTokens(name, tokens);
		}
	}

	public static String toFullDescription(String name, int level) {
		return toFullDescription(Database.get().cards().get(name).createCardDescFor(level));
	}
}
