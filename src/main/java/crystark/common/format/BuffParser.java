package crystark.common.format;

import crystark.common.api.AbilityDesc;
import crystark.common.api.CardDesc;
import crystark.common.api.CardToken;
import crystark.common.api.CardType;
import crystark.common.api.ConflictType;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.UnaryOperator;

/**
 * CardDesc newCd = BuffParser
 * .parseHealth("forest 200%;mnt 1.5")
 * .parseAttack("forest 150%;mnt 0.5")
 * .apply(CardDesc srcCd)
 */
public class BuffParser implements UnaryOperator<CardDesc> {

    Consumer<State> transformer = null;

    /**
     * forest 200%;mnt 1.5
     *
     * @param text
     */
    public BuffParser parseHealth(String text) {

        parse(text, (state, rate) -> state.health = (long) ((double) state.health * rate));
        return this;
    }

    public BuffParser parseAttack(String text) {

        parse(text, (state, rate) -> state.attack = (int) ((double) state.attack * rate));
        return this;
    }

    private void parse(String text, BiConsumer<State, Double> modificator) {

        final String[] split = text.split(";");
        for (String s : split) {
            s = s.trim().toLowerCase();
            if (s.isEmpty()) {
                continue;
            }

            final int i = s.indexOf(' ');
            if (i < 0) {
                throw new IllegalArgumentException("Buff value has to be prepended by space. " +
                    "E.g. \"forest 200%; mnt 1.5\"");
            }

            String typeStr = s.substring(0, i);
            CardType cardType;
            try {
                cardType = CardType.valueOf(typeStr);
            } catch (IllegalArgumentException ex) {
                throw new FormatException('\'' + typeStr + "' is not a valid card type. Allowed values are: " +
                    Arrays.toString(CardType.values()));
            }


            String rateStr = s.substring(i + 1).trim();
            double rate;
            if (rateStr.endsWith("%")) {
                rateStr = rateStr.substring(0, rateStr.length() - 1);
                rateStr = rateStr.trim();
                rate = Double.parseDouble(rateStr) / 100.0d;
            } else {
                rate = Double.parseDouble(rateStr);
            }

            Consumer<State> addTrans = state -> {
                if (state.cardDesc.type == cardType) {
                    modificator.accept(state, rate);
                }
            };
            if (transformer == null) {
                transformer = addTrans;
            } else {
                transformer = transformer.andThen(addTrans);
            }
        }
    }


    @Override
    public CardDesc apply(CardDesc cardDesc) {

        if (transformer == null) {
            return cardDesc;
        }

        final State state = new State(cardDesc);
        transformer.accept(state);

        String name = cardDesc.name;
        int cost = cardDesc.cost;
        int evolution = cardDesc.evolution;
        int level = cardDesc.level;
        int timer = cardDesc.timer;
        CardType type = cardDesc.type;
        ConflictType conflictType = cardDesc.conflictType;
        Integer limit = cardDesc.limit;
        List<AbilityDesc> abilities = cardDesc.abilities;
        List<CardToken> tokens = cardDesc.tokens;

        return new CardDesc(name, cost, evolution, level, timer, state.attack, state.health, type, conflictType, limit,
            abilities, tokens);
    }

    private static class State {
        final CardDesc cardDesc;
        int attack;
        long health;

        State(CardDesc cardDesc) {
            this.cardDesc = cardDesc;
            attack = cardDesc.attack;
            health = cardDesc.health;
        }
    }
}
