package crystark.common.format;

import crystark.common.api.CardDesc;
import crystark.common.api.DeckDesc;
import crystark.common.api.RuneDesc;
import crystark.common.db.model.Deck;
import crystark.ek.battlesim.task.TaskException;
import crystark.tools.Tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

import static crystark.ek.Constants.NL;

public class DeckDescFormat {
	private static final String	ERROR_FORMAT		= "Error on line %d: %s";

	public static final String	DECK_NAME			= "deck name:";
	public static final String	PLAYER_LEVEL		= "player level:";
	public static final String	PLAYER_NAME			= "player name:";
	public static final String	WAIT_TO_PLAY		= "--wait-to-play=";
	public static final String	SKIP_SHUFFLE		= "--skip-shuffle";
	public static final String	ALWAYS_START		= "--always-start";
	public static final String	INFINITE_HP			= "--infinite-hp";
	public static final String	BYPASS_VALIDATION	= "--bypass-validation";
	public static final String	BUFF_HP				= "--buff-hp="; //--buff-hp=forest 200%;mnt 1.5
	public static final String	BUFF_ATT			= "--buff-att="; //--buff-att=forest 200%;mnt 1.5

	public static DeckDesc[] parseFiles(String... paths) {
		DeckDesc[] files = Tools
			.listFiles(paths)
			.map(DeckDescFormat::parseFile)
			.toArray(DeckDesc[]::new);
		if (files.length == 0) {
			throw new TaskException("No file found for paths " + Arrays.toString(paths));
		}
		return files;
	}

	public static DeckDesc parseFile(String filePath) {
		return parseFile(new File(filePath));
	}

	public static DeckDesc parseFile(File file) {
		try {
			return parseString(file.getName(), Tools.readFile(file));
		}
		catch (IOException e) {
			throw new TaskException("File not found: " + file.getPath(), e);
		}
		catch (TaskException e) {
			throw new TaskException("Failed to parse file " + file.getPath() + ": " + e.getMessage(), e);
		}
	}

	public static DeckDesc parseString(String deckName, String deck) {
		try (BufferedReader br = new BufferedReader(new StringReader(deck))) {
			return parseLines(deckName, br.lines());
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	static DeckDesc parseLines(String defaultDeckName, Stream<String> lines) {
		AtomicReference<String> deckName = new AtomicReference<>(defaultDeckName);
		AtomicReference<String> playerName = new AtomicReference<>(defaultDeckName);
		AtomicReference<Integer> playerLevel = new AtomicReference<>();
		AtomicInteger skipShuffle = new AtomicInteger();
		AtomicBoolean alwaysStart = new AtomicBoolean();
		AtomicBoolean infiniteHp = new AtomicBoolean();
		AtomicBoolean bypassValidation = new AtomicBoolean();
		AtomicReference<Integer> waitToPlay = new AtomicReference<>();
		ArrayList<CardDesc> cards = new ArrayList<CardDesc>();
		ArrayList<RuneDesc> runes = new ArrayList<RuneDesc>();

		AtomicInteger lineNumber = new AtomicInteger();
		BuffParser buffParser = new BuffParser();

		lines.forEach(line -> {
			lineNumber.incrementAndGet();

			String trimedLine = line
				.replace('\ufeff' + "", "") // Remove BOM
				.trim();
			String lowerCasedLine = trimedLine.toLowerCase();
			if (trimedLine.startsWith("#") || trimedLine.isEmpty()) {
				return;
			}

			CardDesc cardDesc;
			RuneDesc runeDesc;
			try {
				if (lowerCasedLine.startsWith(PLAYER_LEVEL)) {
					String level = trimedLine.substring(PLAYER_LEVEL.length()).trim();
					try {
						playerLevel.set(Tools.parseInt(level));
					}
					catch (NumberFormatException e) {
						throw new FormatException(level + " is not a valid player level");
					}
				}
				else if (lowerCasedLine.startsWith(PLAYER_NAME)) {
					String name = trimedLine.substring(PLAYER_NAME.length()).trim();
					if (!name.isEmpty()) {
						playerName.set(name);
					}
				}
				else if (lowerCasedLine.startsWith(DECK_NAME)) {
					String name = trimedLine.substring(DECK_NAME.length()).trim();
					if (!name.isEmpty()) {
						deckName.set(name);
					}
				}
				else if (trimedLine.startsWith(SKIP_SHUFFLE)) {
					skipShuffle.set(Integer.MAX_VALUE);
					if (trimedLine.length() > SKIP_SHUFFLE.length()) {
						String num = trimedLine.substring(SKIP_SHUFFLE.length()).trim();
						if (num.charAt(0) != '=') {
							throw new FormatException("Wrong '" + SKIP_SHUFFLE + "' option format. Right example: " + SKIP_SHUFFLE + "=2");
						}
						num = num.substring(1).trim();
						try {
							skipShuffle.set(Tools.parseInt(num));
						}
						catch (NumberFormatException e) {
							throw new FormatException("'" + num + "' is not a valid value for " + SKIP_SHUFFLE);
						}
					}
				}
				else if (trimedLine.equalsIgnoreCase(ALWAYS_START)) {
					alwaysStart.set(true);
				}
				else if (trimedLine.equalsIgnoreCase(INFINITE_HP)) {
					infiniteHp.set(true);
				}
				else if (trimedLine.equalsIgnoreCase(BYPASS_VALIDATION)) {
					bypassValidation.set(true);
				}
				else if (trimedLine.startsWith(WAIT_TO_PLAY)) {
					String num = trimedLine.substring(WAIT_TO_PLAY.length()).trim();
					try {
						waitToPlay.set(Tools.parseInt(num));
					}
					catch (NumberFormatException e) {
						throw new FormatException("'" + num + "' is not a valid value for " + WAIT_TO_PLAY);
					}
				}
				else if (trimedLine.startsWith(BUFF_HP)) {
					String text = trimedLine.substring(BUFF_HP.length());
					buffParser.parseHealth(text);
				}
				else if (trimedLine.startsWith(BUFF_ATT)) {
					String text = trimedLine.substring(BUFF_ATT.length());
					buffParser.parseAttack(text);
				}
				else if ((runeDesc = RuneDescFormat.parseSilent(trimedLine)) != null) {
					runes.add(runeDesc);
				}
				else if ((cardDesc = CardDescFormat.parseSilent(trimedLine)) != null) {
					cards.add(cardDesc);
				}
				else {
					throw new FormatException("'" + trimedLine + "' doesn't match any card name, card description, rune or player property");
				}
			}
			catch (FormatException e) {
				throw new TaskException(String.format(ERROR_FORMAT, lineNumber.get(), e.getMessage()));
			}
		});

		cards.replaceAll(buffParser::apply);
		return new DeckDesc(deckName.get(), playerName.get(), playerLevel.get(), playerLevel.get() == null ? null : Deck.calcHp(playerLevel.get()), cards, runes, skipShuffle.get(), alwaysStart.get(), infiniteHp.get(), waitToPlay.get(), bypassValidation.get());
	}

	public final static String toDeckString(DeckDesc desc, String header) {
		return toDeckString(desc, header, false);
	}

	public final static String toDeckString(DeckDesc desc, String header, boolean skipOptions) {
		StringBuilder sb = new StringBuilder(2048);
		if (header != null) {
			sb.append("# ").append(header).append(NL);
		}

		if (!skipOptions) {
			if (desc.skipShuffle > 0) {
				sb.append(SKIP_SHUFFLE);
				//desc.skipShuffle may has value Integer.MAX_VALUE. Care overflow about.
				if (Integer.MAX_VALUE != desc.skipShuffle) {
					sb.append('=').append(desc.skipShuffle);
				}
				sb.append(NL);
			}
			if (Boolean.TRUE.equals(desc.infiniteHp)) {
				sb.append(INFINITE_HP).append(NL);
			}
			if (Boolean.TRUE.equals(desc.alwaysStart)) {
				sb.append(ALWAYS_START).append(NL);
			}
			if (desc.waitToPlay > 0) {
				sb.append(WAIT_TO_PLAY).append(desc.waitToPlay).append(NL);
			}
			if (Boolean.TRUE.equals(desc.bypassValidation)) {
				sb.append(BYPASS_VALIDATION).append(NL);
			}

			if (desc.deckName != null) {
				sb.append(DECK_NAME + ' ' + desc.deckName).append(NL);
			}

			sb
				.append(PLAYER_NAME + ' ' + desc.playerName).append(NL)
				.append(PLAYER_LEVEL + ' ' + desc.playerLevel).append(NL)
				.append(NL);

			sb.append("# Cards").append(NL);
		}

		for (CardDesc card : desc.cardsDescs) {
			sb.append(CardDescFormat.toFullDescription(card)).append(NL);
		}

		if (!desc.runesDescs.isEmpty()) {
			sb.append(NL);

			if (!skipOptions) {
				sb.append("# Runes").append(NL);
			}

			for (RuneDesc rune : desc.runesDescs) {
				sb.append(RuneDescFormat.toFullDescription(rune)).append(NL);
			}
		}

		return sb.toString();
	}
}
