package crystark.ek;

import com.google.gson.Gson;
import com.squareup.okhttp.ConnectionPool;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import crystark.common.api.Server;
import crystark.common.db.model.ek.EkCompetitor;
import crystark.common.db.model.ek.EkDto;
import crystark.common.db.model.ek.EkLeague;
import crystark.common.db.model.ek.EkUserDeck;
import crystark.common.db.model.ek.EkVersion;
import crystark.ek.battlesim.task.TaskException;
import crystark.tools.Tools;

import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public abstract class DataFetcher {
	protected static final Gson	GSON	= new Gson();

	private final OkHttpClient	client;
	protected String		baseUrl;

	public DataFetcher(Server server) {
		// Default value. Will be overriden on login
		baseUrl = "http://" + server.host();

		client = new OkHttpClient();
		client.setReadTimeout(15, TimeUnit.SECONDS);

		CookieManager cm = new CookieManager();
		cm.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
		client.setCookieHandler(cm);

		ConnectionPool connectionPool = new ConnectionPool(5, 5 * 60 * 1000);
		client.setConnectionPool(connectionPool);
	}

	public abstract void login() throws IOException;

	String getUserInfo() throws IOException {
		login();
		Response r = get(baseUrl + "/user.php?do=GetUserinfo&phpl=EN");

		return checkStatus(r.body().string());
	}

	public String getCardsData() throws IOException {
		login();
		Response r = get(baseUrl + "/card.php?do=GetAllCard&phpl=EN");

		return checkStatus(r.body().string());
	}

	private String checkStatus(String ekDtoString) {
		EkDto dto = GSON.fromJson(ekDtoString, EkDto.class);
		if (dto.status != 1) {
			throw new TaskException("Failed to retrieve data. Please retry later." + (dto.message.isEmpty() ? "" : " Message from server was: " + dto.message));
		}
		return ekDtoString;
	}

	public List<EkCompetitor> getRankData(Integer from, Integer count) throws IOException {
		login();

		RequestBody formBody = new FormEncodingBuilder()
			.add("Amount", count.toString())
			.add("StartRank", from.toString())
			.build();

		Response r = post(baseUrl + "/arena.php?do=GetRankUsers&phpl=EN", formBody);

		return GSON.fromJson(r.body().string(), EkDto.class).data.Competitors;
	}

	public EkUserDeck freeFight(String competitor) throws IOException {
		login();

		RequestBody formBody = new FormEncodingBuilder()
			.add("isManual", "1")
			.add("NoChip", "1")
			.add("Competitor", competitor)
			.build();

		Response r = post(baseUrl + "/arena.php?do=FreeFight&phpl=EN", formBody);

		return GSON.fromJson(checkStatus(r.body().string()), EkDto.class).data.DefendPlayer;
	}

	public String changeNick(String newNick) throws IOException {
		login();

		RequestBody formBody = new FormEncodingBuilder()
			.add("NickName", newNick)
			.build();

		Response r = post(baseUrl + "/user.php?do=ChangeNick&phpl=EN", formBody);

		return r.body().string();
	}

	public String getSkillsData() throws IOException {
		login();
		Response r = get(baseUrl + "/card.php?do=GetAllSkill&phpl=EN");

		return checkStatus(r.body().string());
	}

	public String getRunesData() throws IOException {
		login();
		Response r = get(baseUrl + "/rune.php?do=GetAllRune&phpl=EN");

		return checkStatus(r.body().string());
	}

	public EkLeague getLeagueData() throws IOException {
		login();
		Response r = get(baseUrl + "/league.php?do=getLeagueInfo&phpl=EN");
		final String s = r.body().string();
		return GSON.fromJson(checkStatus(s), EkDto.class).data.LeagueNow;
	}

	public String getMapsData() throws IOException {
		login();
		Response r = get(baseUrl + "/mapstage.php?do=GetMapStageALL&phpl=EN");

		return r.body().string();
	}

	public class EkMapFullDto {
		public Integer			status;
		public String			message;
		public List<Map<?, ?>>	data;
		public EkVersion		version;
	}

	@SuppressWarnings("unchecked")
	public String getOrderedMapsData() throws IOException {
		String data = getMapsData();

		EkMapFullDto maps = GSON.fromJson(data, EkMapFullDto.class);

		maps.data.stream()
			.map(m -> (List<Map<?, ?>>) m.get("MapStageDetails"))
			.forEach(l -> Collections.sort(l, Comparator.<Map<?, ?>, Integer> comparing(t -> Tools.parseInt(t.get("MapStageDetailId").toString()))));

		return GSON.toJson(maps);
	}

	protected Response get(String url) throws IOException {
		Request request = new Request.Builder()
			.url(url)
			.build();

		Response response = client.newCall(request).execute();
		if (!response.isSuccessful())
			throw new DataFetcherException("Failed request " + response, response);

		return response;
	}

	protected Response post(String url, RequestBody formBody) throws IOException {
		Request request = new Request.Builder()
			.url(url)
			.post(formBody)
			.build();

		Response response = client.newCall(request).execute();
		if (!response.isSuccessful())
			throw new DataFetcherException("Failed request " + response, response);

		return response;
	}

	public static class DataFetcherException extends IOException {
		public final Response response;

		public DataFetcherException(String msg, Response response) {
			super(msg);
			this.response = response;
		}
	}
}
