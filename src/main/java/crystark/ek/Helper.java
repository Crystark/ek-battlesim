package crystark.ek;

import crystark.tools.Tools;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Predicate;

public class Helper {
	//private static final Random R = new Random();

	public static boolean getLucky(int chance) {
		return chance >= 100 || ThreadLocalRandom.current().nextInt(100) < chance;
	}

	public static boolean headsOrTails() {
		return ThreadLocalRandom.current().nextBoolean();
	}

	public static int random(int n) {
		if (n == 0)
			return 0;
		return ThreadLocalRandom.current().nextInt(n);
	}

	public static <T> T pick(List<T> list) {
		if (list.isEmpty())
			return null;
		return list.get(ThreadLocalRandom.current().nextInt(list.size()));
	}

	public static <T> List<T> pick(List<T> list, int n) {
		if (list.isEmpty()) {
			return Collections.emptyList();
		}
		else if (list.size() <= n) {
			return list;
		}
		else {
			int[] indexes = Tools.distinctRandoms(n, list.size());
			List<T> l = new ArrayList<>(indexes.length);
			for (int i = 0; i < indexes.length; i++) {
				int idx = indexes[i];
				l.add(list.get(idx));
			}
			return l;
		}
	}

	public static <T> T pick(List<T> list, Predicate<? super T> predicate) {
		if (list.isEmpty()) {
			return null;
		}
		if (list.size() == 1) {
			T c = list.get(0);
			return predicate.test(c) ? c : null;
		}

		List<T> selection = new ArrayList<>(16);
		for (int i = 0; i < list.size(); i++) {
			T t = list.get(i);
			if (predicate.test(t)) {
				selection.add(t);
			}
		}
		return Helper.pick(selection);
	}

	/**
	 * At least n T in list that matches predicate
	 *
	 * @param n
	 * @param list
	 * @param predicate
	 * @return
	 */
	public static <T> boolean atLeast(int n, List<? extends T> list, Predicate<? super T> predicate) {
		for (int i = 0; i < list.size(); i++) {
			T t = list.get(i);
			if (predicate.test(t)) {
				if (--n == 0) {
					return true;
				}
			}
		}
		return false;
	}

	public static <T> int count(List<T> list, Predicate<? super T> predicate) {
		int n = 0;
		for (int i = 0; i < list.size(); i++) {
			T t = list.get(i);
			if (predicate.test(t))
				n++;
		}
		return n;
	}
}
