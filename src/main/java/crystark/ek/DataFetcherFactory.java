package crystark.ek;

import crystark.common.api.Server;

public class DataFetcherFactory {
	public static DataFetcher get(Server server) {
		return get(server, true);
	}

	public static DataFetcher get(Server server, boolean useDefaultLogin) {
		switch (server.game()) {
			/*case ek:
				return new EkDataFetcher(server, useDefaultLogin);*/
			case mr:
				return new MrDataFetcher(server, useDefaultLogin);
			case mrn:
				return new MrnDataFetcher(server, useDefaultLogin);
			case loa:
				return new LoaDataFetcher(server, useDefaultLogin);
			case hf:
				return new HfDataFetcher(server, useDefaultLogin);
			default:
				throw new RuntimeException("Unknown game " + server.game());
		}
	}
}
