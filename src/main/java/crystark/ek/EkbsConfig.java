package crystark.ek;

import crystark.common.api.Game;
import crystark.common.api.NamesConverter;
import crystark.common.api.NamesConverterByFiles;
import crystark.common.api.Server;
import crystark.ek.battlesim.ability.AbilityFactory;
import crystark.ek.battlesim.rune.RuneFactory;

public class EkbsConfig {

	private static final EkbsConfig EKBS_CONFIG = new EkbsConfig();

	private Game currentGame;
	private Server defaultServer;
	private NamesConverter currentNamesConverter;
	private AbilityFactory currentAbilityFactory;
	private RuneFactory currentRuneFactory;

	private EkbsConfig() {
		Game game = Game.valueOf(System.getProperty("game", "mr").toLowerCase());
		switchGame(game);
	}

	public static EkbsConfig current(){
		return EKBS_CONFIG;
	}


	public void switchGame(Game game){
		currentNamesConverter =
			game == Game.mr || game == Game.mrn ? NamesConverter.IDENTITY
				: new NamesConverterByFiles(game);

		defaultServer = Server.apollo;
		for (Server srv : Server.values()) {
			if (srv.game() == game) {
				defaultServer = srv;
				break;
			}
		}

		currentAbilityFactory = new AbilityFactory(currentNamesConverter);
		currentRuneFactory = new RuneFactory(currentNamesConverter);

		currentGame = game;
	}

	public Game game(){
		return currentGame;
	}

	public NamesConverter namesConverter(){
		return currentNamesConverter;
	}

	public Server defaultServer() {
		return defaultServer;
	}

	public AbilityFactory abilityFactory(){
		return currentAbilityFactory;
	}

	public RuneFactory runeFactory() {
		return currentRuneFactory;
	}
}
