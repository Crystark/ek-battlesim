package crystark.ek;

import crystark.common.api.Game;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class Constants {
	public static final String				VERSION				= Constants.class.getPackage().getImplementationVersion();
	public static final Game 				GAME = Game.valueOf(System.getProperty("game", "mr").toLowerCase());
	public static final String				TITLE				= "Crystark's Sim for " + GAME.getFullName() + " (" + (VERSION == null ? "Unknown version" : VERSION) + ")";

	public static boolean					IS_DEBUG			= false;
	public static final String				NL					= System.lineSeparator();
	public static final int					THREADS				= Runtime.getRuntime().availableProcessors() + 1;

	public static final SimpleDateFormat	DATE_FORMAT			= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static final SimpleDateFormat	SERVER_DAY_FORMAT	= new SimpleDateFormat("yyyyMMdd");


	static {
		SERVER_DAY_FORMAT.setTimeZone(TimeZone.getTimeZone("PST"));
	}
}
