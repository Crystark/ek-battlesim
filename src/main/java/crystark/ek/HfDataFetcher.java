package crystark.ek;

import com.google.gson.JsonObject;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import crystark.common.api.Server;
import okio.Buffer;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.zip.DeflaterOutputStream;

public class HfDataFetcher extends DataFetcher {

	private static final String SECRET_KEY = "AAAAGQAAABhAAAAawAAAHMAAkAHMAYQAmACEAKABfACkASAAZgAAAGMAAABuAAAAYQAAA$10|654|18|180|137|823|77|791|77|252|";
	private int authSerial = 1000 + new Random().nextInt(8998);
	private final String login;
	private final String password;

	private boolean loginAsGuest = true;
	private boolean loggedIn     = false;

	HfDataFetcher(Server server, boolean useDefaultLogin) {
		super(server);

		//TODO Use guest login for HF if possible
		useDefaultLogin = true;

		if (useDefaultLogin) {
			final Server.Credentials c = server.user();
			this.login = c.user;
			this.password = c.pass;
		} else {
			//Guest login
			this.login = null;
			this.password = null;
		}
	}

	public HfDataFetcher(Server server, String login, String password) {
		super(server);

		this.login = login;
		this.password = password;
	}

	@Override
	public void login() throws IOException {
		if (!loggedIn) {
			if (login != null || password != null) {
				userLogin(login, password);
			}
			else {
				guestLogin();
			}
			loggedIn = true;
		}
	}

	@Override
	protected Response get(String url) throws IOException {
		return post(url, null);
	}

	@Override
	protected Response post(String url, RequestBody formBody) throws IOException {
		try {
			url = addServiceUrl(url);
			final int idx = url.indexOf('?');

			String urlParam = url.substring(idx + 1);
			String baseUrl = url.substring(0, idx);
			JsonObject jsonData = new JsonObject();
			jsonData.addProperty("mzsg", urlParam);

			if (formBody != null){
				String data = decodeForm(formBody);
				String[] params = data.split("&");
				for (int i = 0; i < params.length; i++) {
					String param = params[i];
					String[] split = param.split("=");
					String name = URLDecoder.decode(split[0], "UTF-8");
					String value = split.length < 1 ? "" :URLDecoder.decode(split[1], "UTF-8");
					jsonData.addProperty(name, value);
				}
			}
			jsonData.addProperty("pvpNewVersion", "1");
			jsonData.addProperty("OpenCardChip", "1");
			final String jsonString = jsonData.toString();
			byte[] bytes = jsonString.getBytes(StandardCharsets.UTF_8);

			ByteArrayOutputStream memoryStream = new ByteArrayOutputStream(bytes.length * 4 / 3 + 2);
			final OutputStream base64Stream = Base64.getEncoder().wrap(memoryStream);
			final DeflaterOutputStream zipStream = new DeflaterOutputStream(base64Stream);

			zipStream.write(bytes);
			zipStream.close();

			String base64String = new String(memoryStream.toByteArray(), StandardCharsets.US_ASCII);
			memoryStream.close();

			StringBuilder sb = new StringBuilder(base64String.length() + 15 + SECRET_KEY.length());
			sb.append(randomString(5));
			int len = ThreadLocalRandom.current().nextInt(1, 8);
			sb.append(len);
			sb.append(randomString(len));
			sb.append(base64String);
			base64String = sb.toString();

			sb.append(SECRET_KEY);
			String md5String = getMd5(sb.toString());
			final RequestBody newFormBody = (new FormEncodingBuilder())
				.add("z", base64String)
				.add("b", md5String)
				.build();

			return postPlain(baseUrl, newFormBody);

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	protected Response getPlain(String url) throws IOException {
		return super.get(url);
	}

	protected Response postPlain(String url, RequestBody formBody) throws IOException {
		return super.post(url, formBody);
	}

	void userLogin(String user, String password) throws IOException {
		if (loginAsGuest) {
			loggedIn = false;
			loginAsGuest = false;
		}
		login(new LoginData().login(user, password));
	}

	void guestLogin() throws IOException {
		login(new LoginData().quickStart());
	}

	// pp/start.do
	// httpService.do
	// login.php
	void login(LoginData ld) throws IOException {
		LoginResponse.ReturnObjs lr = postLoginData(ld);
		baseUrl = lr.GS_IP;
		RequestBody form = new FormEncodingBuilder()
			.add("Udid", ld.callPara.udid)
			.add("time", String.valueOf(lr.timestamp))
			.add("UserName", lr.userName)
			.add("Origin", "ANDROID")
			.add("key", lr.key)
			.add("Password", lr.U_ID)
			.add("Devicetoken", "")
			.build();

		postPlain(baseUrl + "/login.php?do=PassportLogin", form);
	}

	LoginResponse.ReturnObjs postLoginData(LoginData ld) throws IOException {
		String dataInfo = GSON.toJson(ld);
		String postData = dataInfo;

		Response r = postPlain(
			"http://web.pp.ifreeteam.com/pp/httpService.do",
			RequestBody.create(
				MediaType.parse("application/json"),
				postData));
		final String responseString = r.body().string();
		LoginResponse lr = GSON.fromJson(responseString, LoginResponse.class);

		if (lr.returnCode != 0) {
			throw new DataFetcherException("Failed to login: " + lr.returnMsg, r);
		}
		return lr.returnObjs;
	}

	public String getMapsData() throws IOException {
		login();
		Response r = get(baseUrl + "/mapstage.php?do=GetMapStageALL&stageNum=40&phpl=EN");

		return r.body().string();
	}

	protected String addServiceUrl(final String url) throws IOException {
		login();
		final StringBuilder sb = new StringBuilder(128);
		authSerial++;
		sb
			//.append(this.baseUrl)
			.append(url)
			.append("&v=")
			.append(authSerial)
			.append("&phpp=Android")
			//.append("&phpl=EN")
			.append("&pvc=3.0.8")
			.append("&pvb=2018-01-25+14%3a20%3a47")
		;
		return sb.toString();
	}

	private static String randomString(int length) {
		final ThreadLocalRandom random = ThreadLocalRandom.current();
		StringBuilder sb = new StringBuilder(length);

		for(int i = 0 ; i < length ; i++){
			final int k = random.nextInt(48, 122);
			char c = (char) (k & 0xFF);
			sb.append(c);
		}
		return sb.toString();
	}

	private static String decodeForm(RequestBody formBody) {
		try {
			Buffer buffer = new Buffer();
			InputStreamReader charsStream = new InputStreamReader(buffer.inputStream(), StandardCharsets.UTF_8);
			final BufferedReader bufferedReader = new BufferedReader(charsStream);
			formBody.writeTo(buffer);
			return bufferedReader.lines().collect(Collectors.joining());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

	}

	protected String getMd5(final String text) {
		try {
			final byte[] bytesOfMessage = text.getBytes("UTF-8");
			final MessageDigest md = MessageDigest.getInstance("MD5");
			final byte[] thedigest = md.digest(bytesOfMessage);

     		final BigInteger bigInt = new BigInteger(1, thedigest);
			String result = String.format("%1$032x", bigInt);
			return result;

		} catch (NoSuchAlgorithmException | UnsupportedEncodingException ex2) {
			return "";
		}
	}

	public static class LoginData {
		public CallPara callPara = new CallPara();
		public String serviceName;

		public LoginData login(String user, String pass) {
			this.serviceName = "startGameJson";
			this.callPara.udid = user;

			return this;
		}

		public LoginData quickStart() {
			this.serviceName = "startGameJson";

			return this;
		}

		public class CallPara {
			public String gameName = "CARDNEW-ANDROID-EN";
			public String clientType = "flash";
			public String releaseChannel = "";
			public String locale = "en";
			public String udid;
			public String selGsId = "1010559735"; //Mirror Lake
		}
	}

	public class LoginResponse {
		public int        returnCode;
		public String     returnMsg;
		public ReturnObjs returnObjs;

		/*
	"isSetNick": "0",
    "invite": true,
    "gscode": false,
    "minor": false,
    "PlayingTipsUrl": "",
    "cdnurl": "http:\/\/cache.ifreecdn.com\/mkhx\/",
    "encrypt": 0,
    "ip": "ea9.liesofastaroth.com",
    "ipport": 8000
		*/
		/*
		{
  "returnCode": "0",
  "returnMsg": "No error.",
  "returnObjs": {
    "GS_CHAT_PORT": "8000",
    "GS_ID": "1541",
    "GS_CHAT_IP": "38.121.63.130",
    "GS_NAME": "EN-ANDROID-09",
    "initialUName": "login",
    "GS_PORT": "80",
    "friendCode": "null",
    "source": "login",
    "userName": "login",
    "U_ID": "1010557199",
    "uEmailState": "0",
    "LOGIN_TYPE": "[]",
    "GS_DESC": "Daenerys",
    "GS_IP": "http://ea9.liesofastaroth.com/",
    "G_TYPE": "1",
    "key": "e0342abc4b402b093ec7c46e9e368a40",
    "timestamp": "1515748163921"
  }
}
		*/
		public class ReturnObjs {
			public int    GS_CHAT_PORT;
			public String GS_ID;
			public String GS_CHAT_IP;
			public String GS_NAME;
			public String initialUName;
			public int    GS_PORT;
			public String friendCode;
			public String source;
			public String userName;
			public String U_ID;
			public String uEmailState;
			public String LOGIN_TYPE;
			public String GS_DESC;
			public String GS_IP;
			public String G_TYPE;
			public String key;
			public long   timestamp;
		}

	}
}
