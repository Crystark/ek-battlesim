package crystark.ek;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import crystark.common.api.Server;

import java.io.IOException;

public class MrnDataFetcher extends DataFetcher {
	private static final String		PASSWORD		= "EKb5p4ssw0rd";
	public static final String[]	HEADER_UNITY	= new String[] { "X-Unity-Version", "4.6.1p4" };
	public static final String[]	HEADER_UA		= new String[] { "User-Agent", "Dalvik/1.6.0 (Linux; U; Android 4.4.2; Mali-T760 Build/KOT49H)" };

	private final String			login;
	private final String			password;

	private boolean					loginAsGuest	= true;
	private boolean					loggedIn		= false;

	MrnDataFetcher(Server server, boolean useDefaultLogin) {
		super(server);

		if (useDefaultLogin) {
			final Server.Credentials c = server.user();
			this.login = c.user;
			this.password = c.pass;
		}
		else {
			this.login = null;
			this.password = null;
		}
	}

	public MrnDataFetcher(Server server, String login, String password) {
		super(server);

		this.login = login;
		this.password = password;
	}

	@Override
	public void login() throws IOException {
		if (!loggedIn) {
			if (login != null || password != null) {
				userLogin(login, password);
			}
			else {
				guestLogin();
			}
			loggedIn = true;
		}
	}

	void userLogin(String user, String password) throws IOException {
		if (loginAsGuest) {
			loggedIn = false;
			loginAsGuest = false;
		}
		login(new LoginData().login(user, password));
	}

	void guestLogin() throws IOException {
		login(new LoginData().quickStart());
	}

	void login(LoginData ld) throws IOException {
		LoginResponse.ReturnObjs lr = postLoginData(ld);
		baseUrl = lr.GS_IP;
		RequestBody form = new FormEncodingBuilder()
			.add("Udid", ld.callPara.udid)
			.add("time", String.valueOf(lr.timestamp))
			.add("UserName", lr.userName)
			.add("Origin", "ANDROID")
			.add("key", lr.key)
			.add("Password", lr.U_ID)
			.add("Devicetoken", "")
			.build();

		post(baseUrl + "/login.php?do=PassportLogin", form);
	}

	LoginResponse.ReturnObjs postLoginData(LoginData ld) throws IOException {
		Response r = post(
			"http://im.fantasytoyou.com/pp/httpService.do",
			RequestBody.create(
				MediaType.parse("application/json; charset=utf-8"),
				GSON.toJson(ld)));
		LoginResponse lr = GSON.fromJson(r.body().string(), LoginResponse.class);
		if (lr.returnCode != 0) {
			throw new DataFetcherException("Failed to login: " + lr.returnMsg, r);
		}
		return lr.returnObjs;
	}

	public static class LoginData {
		public CallPara	callPara	= new CallPara();
		public String	serviceName;

		public LoginData login(String user, String pass) {
			this.serviceName = "login";
			this.callPara.userName = user;
			this.callPara.userPassword = pass;

			return this;
		}

		public LoginData quickStart() {
			this.serviceName = "startGameJson";

			return this;
		}

		public class CallPara {
			public String	gameName		= "SGZJMR-ANDROID-EN";
			public String	clientType		= "EN";
			public String	releaseChannel	= "EN";
			public String	locale			= "EN";
			public String	udid			= "feccd41224b2a69a52c8647d4f0a9b0c";

			public String	userPassword;
			public String	userName;
		}

	}

	public static class LoginResponse {
		public int			returnCode;
		public String		returnMsg;
		public ReturnObjs	returnObjs;

		public class ReturnObjs {
			public int		GS_CHAT_PORT;
			public String	GS_ID;
			public String	GS_CHAT_IP;
			public String	GS_NAME;
			public String	initialUName;
			public int		GS_PORT;
			public String	friendCode;
			public String	source;
			public String	userName;
			public String	U_ID;
			public String	uEmailState;
			public String	LOGIN_TYPE;
			public String	GS_DESC;
			public String	GS_IP;
			public String	G_TYPE;
			public String	key;
			public long		timestamp;
		}
	}
}
