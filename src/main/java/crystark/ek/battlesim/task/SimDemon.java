package crystark.ek.battlesim.task;

import crystark.common.api.CardDesc;
import crystark.common.api.CardTokenType;
import crystark.common.api.CardType;
import crystark.common.api.DeckDesc;
import crystark.ek.battlesim.battle.Battle;
import crystark.tools.Tools;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @see SimDemonTest
 */
class SimDemon extends DeckSim {

	private static final String	RESULT_TABLE	= "SimDemonData";

	final SimMetric				metricRound;
	final SimMetric				metricMerit;

	final boolean				showDamage;
	final long					baseHp;
	final long					maxDmg;

	public SimDemon(final SimDemonTaskOptions opts) {
		super(opts);
		this.showDamage = opts.showDamage == null ? false : opts.showDamage;

		this.baseHp = getHealth(this.defenders[0]);

		metricRound = new SimMetric((int) expectedTotalCount());
		metricMerit = new SimMetric((int) expectedTotalCount());

		maxDmg = opts.maxDamage;
	}

	@Override
	protected Runnable newRunnableInstance(final long iter, final AtomicInteger metricCounter, final AtomicInteger metricErrors) {
		return () -> {
			for (long i = 0; i < iter; i++) {
				try {
					Battle battle = Battle.run(attackers[0], defenders[0]);
					int r = battle.getRound();
					long m = getMerit(battle);
					if (showDamage) {
						p("Merit: " + m);
					}
					metricCounter.getAndIncrement();
					metricRound.update(r);
					metricMerit.update(m);
				}
				catch (Throwable t) {
					metricErrors.getAndIncrement();
					t.printStackTrace();
					throw t;
				}
			}
		};
	}

	private static long getHealth(DeckDesc dd) {
		long totalHealth = 0;
		for (CardDesc c : dd.cardsDescs) {
			if (c.type.equals(CardType.demon) || c.type.equals(CardType.hydra)) {
				totalHealth += c.health;
				break;
			}
		}
		return totalHealth;
	}

	public long getMerit(Battle battle) {
		long hpLeft = getHealth(battle.defenderSnapshot());
		int meritCards = battle.desc.attacker.countTokens(CardTokenType.diMerit);
		long merits = Math.max(1, baseHp - hpLeft + meritCards * 8000);
		merits = merits > maxDmg ? maxDmg : merits;
		return merits;
	}

	private int cooldownSecs() {
		int cost = attackers[0].getCardsBaseCost();
		return 60 + 2 * cost;
	}

	private int cooldownSecsForReset() {
		return cooldownSecs() - 15;// 15 seconds wait time between resets
	}

	@Override
		Results buildSimResults() {
		int cooldownSecs = cooldownSecs();

		Results results = super.buildSimResults();
		results.title = defenders[0].cardsDescs.get(0).name + " Demon Simulation";

		ResultTable rt = new ResultTable(RESULT_TABLE);
		rt.addLine("Deck cooldown", Tools.formatSecondsToMinutes(cooldownSecs) + " (" + attackers[0].getCardsBaseCost() + " base cost)");
		rt.addLine();
		rt.addLine("Average number of rounds", Math.round(metricRound.getMean() * 100) / 100f);
		rt.addLine("Average merit per fight (MPF)", Math.round(metricMerit.getMean()));
		rt.addLine("Average merit per minute (MPM)", meritPerMinute());
		rt.addLine("Average merit per gem (MPG)", meritPerGem() + " (Gem ASAP " + Tools.formatSecondsToMinutes(cooldownSecsForReset()) + ')');
		rt.addLine("", meritPerGemWait() + " (Gem @ 0" + (cooldownSecsForReset() / 60) + ":00)");
		if (maxDmg != Long.MAX_VALUE) rt.addLine("Merit limit", maxDmg);
		results.tables.put(rt.id, rt);

		ResultChart rsRound = new ResultChart();
		rsRound.title = "Round repartition chart";
		rsRound.hystogramAsMap = metricRound.getHystogramAsMap(10);
		results.charts.put(rsRound.title, rsRound);

		ResultChart rsMpf = new ResultChart();
		rsMpf.title = "MPF repartition chart";
		rsMpf.hystogramAsMap = metricMerit.getHystogramAsMap(10);
		results.charts.put(rsMpf.title, rsMpf);

		return results;
	}

	long meritPerMinute() {
		double cooldownMinutes = cooldownSecs() / 60.0;
		return Math.round(metricMerit.getMean() / cooldownMinutes);
	}

	long meritPerGem() {
		double gemsToReset = 10 * Math.ceil(cooldownSecsForReset() / 60.0);
		return Math.round(metricMerit.getMean() / gemsToReset);
	}

	long meritPerGemWait() {
		double gemsToReset = 10 * Math.floor(cooldownSecsForReset() / 60.0);
		return Math.round(metricMerit.getMean() / gemsToReset);
	}
}
