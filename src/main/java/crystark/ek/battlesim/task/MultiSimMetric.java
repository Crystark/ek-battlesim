package crystark.ek.battlesim.task;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

class MultiSimMetric extends SimMetric {
	List<SimMetric> simMetrics;

	public MultiSimMetric(int simMetrics) {
		super();

		this.simMetrics = new ArrayList<>(simMetrics);
		for (int i = 0; i < simMetrics; i++) {
			this.simMetrics.add(new SimMetric());
		}
	}

	public void update(int index, long i) {
		super.update(i);
		simMetrics.get(index).update(i);
	}

	@Override
	public void update(long i) {
		throw new UnsupportedOperationException("Cannot call update on multisim metric. Use update(index, i)");
	}

	public long getMin(int index) {
		return simMetrics.get(index).getMin();
	}

	public long getMax(int index) {
		return simMetrics.get(index).getMax();
	}

	public long getTotal(int index) {
		return simMetrics.get(index).getTotal();
	}

	public float getMean(int index) {
		return simMetrics.get(index).getMean();
	}

	public float getMeanOrDefault(int index, int def) {
		return simMetrics.get(index).getMeanOrDefault(def);
	}

	public long getCount(int index) {
		return simMetrics.get(index).getCount();
	}

	public Map<Long[], Float> getHystogramAsMap(int index, int nBars) {
		return simMetrics.get(index).getHystogramAsMap(nBars);
	}
}
