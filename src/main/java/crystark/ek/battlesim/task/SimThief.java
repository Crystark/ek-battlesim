package crystark.ek.battlesim.task;

/**
 * @see SimThiefTest
 */
class SimThief extends MultiDefenderSim {

	public SimThief(final TaskOptions opts) {
		super(opts);
	}

	@Override
	protected int getDefaultIter(TaskOptions opts) {
		return 20000;
	}

	@Override
	Results buildSimResults() {
		Results sr = super.buildSimResults();
		sr.title = "Thief Simulation";
		return sr;
	}
}
