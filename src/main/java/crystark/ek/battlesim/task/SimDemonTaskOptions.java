package crystark.ek.battlesim.task;

import crystark.common.api.CardDesc;
import crystark.common.api.CardType;
import crystark.common.api.DeckDesc;
import crystark.common.format.CardDescFormat;
import crystark.common.format.DeckDescFormat;
import crystark.common.transform.DeckDescTransformer;

import java.nio.file.Paths;
import java.util.Collections;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Stream;

public class SimDemonTaskOptions extends SimTaskOptions<SimDemonTaskOptions> {
	public static final DeckDescTransformer DEFAULTS = DeckDescTransformer.create()
		.withInfiniteHpTransformer(t -> true);

	long maxDamage = Long.MAX_VALUE;

	static SimDemonTaskOptions create(DeckDesc deck, String demonDeck) {
		return new SimDemonTaskOptions()
			.attackerDecks(deck)
			.withDemon(demonDeck);
	}

	private SimDemonTaskOptions withDemon(String deck) {
		defenderDecks(getDemonDeck(deck));
		return this;
	}

	private static DeckDesc getDemonDeck(String deck) {
		return Stream
			.<Function<String, DeckDesc>> of(
				s -> {
					CardDesc card = CardDescFormat.parseSilent(s);
					if (card != null) {
						if (CardType.demon.equals(card.type)) {
							return new DeckDesc(card.name, card.name, 100, (int) card.health, Collections.singletonList(card), Collections.emptyList(), 0, true, true);
						}
					}
					return null;
				} ,
				s -> {
					try {
						if (Paths.get(s.trim()).toFile().exists()) {
							return DeckDescFormat.parseFile(s);
						}
					}
					catch (Throwable t) {}
					return null;
				} ,
				s -> DeckDescFormat.parseString(null, s))
			.map(f -> f.apply(deck))
			.filter(Objects::nonNull)
			.map(DEFAULTS::apply)
			.findFirst()
			.orElseThrow(() -> new TaskException("Not a valid demon: " + deck));
	}

	public SimDemonTaskOptions showDamage(boolean showDamage) {
		this.showDamage = showDamage;
		return this;
	}

	public SimDemonTaskOptions maxDamage(long maxDamage) {
		this.maxDamage = maxDamage;
		return this;
	}

	@Override
	protected Task buildTask() {
		return new SimDemon(this);
	}
}
