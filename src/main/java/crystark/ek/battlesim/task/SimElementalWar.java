package crystark.ek.battlesim.task;

import crystark.common.api.CardDesc;
import crystark.common.api.CardType;
import crystark.common.api.DeckDesc;
import crystark.ek.battlesim.battle.Battle;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @see SimElementalWarTest
 */
class SimElementalWar extends DeckSim {

	private static final String	RESULT_TABLE	= "SimElementalWar";
	final SimMetric				metricRound;
	final SimMetric				metricDamage;

	final long					baseHp;

	public SimElementalWar(final TaskOptions opts) {
		super(opts);

		this.baseHp = getHealth(this.defenders[0]);

		metricRound = new SimMetric((int) expectedTotalCount());
		metricDamage = new SimMetric((int) expectedTotalCount());
	}

	@Override
	protected Runnable newRunnableInstance(final long iter, final AtomicInteger metricCounter, final AtomicInteger metricErrors) {
		return () -> {
			for (int i = 0; i < iter; i++) {
				try {
					Battle battle = Battle.run(attackers[0], defenders[0]);
					int r = battle.getRound();
					long dmg = baseHp;
					if (battle.defenderWon()) {
						dmg -= getHealth(battle.defenderSnapshot());
					}
					metricCounter.getAndIncrement();
					metricRound.update(r);
					metricDamage.update(dmg);
				}
				catch (Throwable t) {
					metricErrors.getAndIncrement();
					t.printStackTrace();
					throw t;
				}
			}
		};
	}

	private static long getHealth(DeckDesc dd) {
		long h = 0;
		for (CardDesc c : dd.cardsDescs) {
			if (c.type.equals(CardType.demon) || c.type.equals(CardType.hydra)) {
				h += c.health;
			}
		}
		return h;
	}

	@Override
	protected Results buildSimResults() {
		Integer waitToPlay = attackers[0].waitToPlay;
		int skipShuffle = attackers[0].skipShuffle;

		Results results = super.buildSimResults();
		results.title = "Elemental War Simulation";

		ResultTable rt = new ResultTable(RESULT_TABLE);
		rt.addLine("Average number of rounds", Math.round(metricRound.getMean() * 100) / 100f);
		rt.addLine("Average damage per fight (DPF)", damagePerFight());
		rt.addLine("Using --skip-shuffle", skipShuffle <= 0 ? "No" : skipShuffle == Integer.MAX_VALUE ? "Yes" : skipShuffle);
		rt.addLine("Using --wait-to-play", waitToPlay == null ? "No" : waitToPlay);
		results.tables.put(rt.id, rt);

		ResultChart rsRound = new ResultChart();
		rsRound.title = "Round repartition chart";
		rsRound.hystogramAsMap = metricRound.getHystogramAsMap(10);
		results.charts.put(rsRound.title, rsRound);

		ResultChart rsMpf = new ResultChart();
		rsMpf.title = "DPF repartition chart";
		rsMpf.hystogramAsMap = metricDamage.getHystogramAsMap(10);
		results.charts.put(rsMpf.title, rsMpf);

		return results;
	}

	int damagePerFight() {
		return Math.round(metricDamage.getMean());
	}
}
