package crystark.ek.battlesim.task;

public abstract class BaseTask implements Task {
	private TaskOptions	opts;
	protected long		startedAt;
	protected long		duration;
	protected boolean	printOutput;

	protected BaseTask(TaskOptions opts) {
		this.opts = opts;
		this.printOutput = !Boolean.FALSE.equals(opts.printOutput);
	}

	@Override
	public TaskOptions getOptions() {
		return opts;
	}

	protected final void p() {
		p(null);
	}

	protected final void p(String str) {
		if (printOutput)
			System.out.println("# " + (str == null ? "" : str.replaceAll(System.lineSeparator(), System.lineSeparator() + "# ")));
	}

	protected final void pw(String str) {
		p("/!\\/!\\/!\\  " + str + "  /!\\/!\\/!\\");
	}

	protected final void psep() {
		p("---------------------------------------------------------------------------");
	}

	protected final void pt(String str) {
		p("--- " + str);
	}

	@Override
	public boolean isInterrupted() {
		return false;
	}

	@Override
	public void onStart() {
		startedAt = System.nanoTime();
	}

	@Override
	public void onComplete() {
		duration = System.nanoTime() - startedAt;
	}
}
