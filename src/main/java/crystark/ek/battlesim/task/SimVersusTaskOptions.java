package crystark.ek.battlesim.task;

import crystark.common.api.DeckDesc;

public class SimVersusTaskOptions extends SimTaskOptions<SimVersusTaskOptions> {

	static SimVersusTaskOptions create(DeckDesc deck, DeckDesc[] opponentsDecks) {
		return new SimVersusTaskOptions()
			.attackerDecks(deck)
			.defenderDecks(opponentsDecks);
	}

	@Override
	protected SimVersus buildTask() {
		return new SimVersus(this);
	}
}
