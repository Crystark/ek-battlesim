package crystark.ek.battlesim.task;

import java.util.LinkedHashMap;
import java.util.Map;

public class Results {
	public String					title;
	public long						iterations;
	public long						maxRunsPerIter;
	public long						durationMillis;
	public Map<String, ResultTable>	tables	= new LinkedHashMap<>();
	public Map<String, ResultChart>	charts	= new LinkedHashMap<>();
}
