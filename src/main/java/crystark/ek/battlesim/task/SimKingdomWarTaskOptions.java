package crystark.ek.battlesim.task;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import crystark.common.api.DeckDesc;
import crystark.common.format.DeckDescFormat;
import crystark.tools.Tools;

public class SimKingdomWarTaskOptions extends SimTaskOptions<SimKingdomWarTaskOptions> {
	private static final String		DECK_DIR	= "decks/kw/";
	public static final String[]	DECKS		= { "dst_55", "fd_54", "fd_55", "fd_56", "id_52_rl3", "id_52_rl4", "id_53", "md_72", "mfb_74", "oni_54", "tcw_71" };

	static SimKingdomWarTaskOptions create(DeckDesc deck, Collection<String> guards) {
		return new SimKingdomWarTaskOptions()
			.attackerDecks(deck)
			.defenderDecks(getDecks(guards));
	}

	public SimKingdomWarTaskOptions limit(Integer limit) {
		this.limit = limit;
		return this;
	}

	static DeckDesc[] getDecks(Collection<String> only) {
		Set<String> remaining = new HashSet<>(only);

		List<DeckDesc> decks = new ArrayList<>();
		for (int i = 0; i < DECKS.length; i++) {
			String deck = DECKS[i];
			if (only.isEmpty() || only.contains(deck)) {
				remaining.remove(deck);
				decks.add(getDeck(deck));
			}
		}

		if (!remaining.isEmpty()) {
			throw new TaskException("Unrecognized guard(s): " + remaining);
		}

		return decks.toArray(new DeckDesc[decks.size()]);
	}

	private static DeckDesc getDeck(String deck) {
		return DeckDescFormat.parseString(deck, Tools.readResource(DECK_DIR + deck + ".txt"));
	}

	@Override
	protected Task buildTask() {
		return new SimKingdomWar(this);
	}
}
