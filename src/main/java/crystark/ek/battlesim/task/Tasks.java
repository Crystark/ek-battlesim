package crystark.ek.battlesim.task;

import crystark.common.api.DeckDesc;
import crystark.common.api.Server;

import java.util.Collection;
import java.util.List;

public final class Tasks {
	public static SimDemonTaskOptions prepareDemonInvasionSim(DeckDesc deck, String demonDescription) {
		return SimDemonTaskOptions.create(deck, demonDescription);
	}

	public static SimElementalWarTaskOptions prepareElementalWarSim(DeckDesc deck, String legendaryDeck) {
		return SimElementalWarTaskOptions.create(deck, legendaryDeck);
	}

	public static SimWonderTowerTaskOptions prepareWonderTowerSim(DeckDesc deck, DeckDesc... opponentsDecks) {
		return SimWonderTowerTaskOptions.create(deck, opponentsDecks);
	}

	public static SimThiefTaskOptions prepareThiefSim(DeckDesc deck) {
		return SimThiefTaskOptions.create(deck);
	}

	public static SimHydraTaskOptions prepareHydraSim(DeckDesc deck, List<String> hydras, boolean hydraOnly) {
		// TODO .toLowerCase().trim() on each hydra param
		return SimHydraTaskOptions.create(deck, hydras, hydraOnly);
	}

	public static SimKingdomWarTaskOptions prepareKingdomWarSim(DeckDesc deck, Collection<String> guards) {
		// TODO .toLowerCase().trim() on each guard param
		return SimKingdomWarTaskOptions.create(deck, guards);
	}

	public static SimVersusTaskOptions prepareVersusSim(DeckDesc deck, DeckDesc... opponentsDecks) {
		return SimVersusTaskOptions.create(deck, opponentsDecks);
	}

	public static SimLeagueTaskOptions prepareFieldOfHonorSim(Server server) {
		return SimLeagueTaskOptions.create(server);
	}

	public static ExtractRMDecksTaskOptions prepareArenaExtract(Server server, String outputDir) {
		return ExtractRMDecksTaskOptions.create(server, outputDir);
	}

	public static ExtractLeagueDecksTaskOptions prepareFieldOfHonorExtract(Server server, String outputDir) {
		return ExtractLeagueDecksTaskOptions.create(server, outputDir);
	}

	public static ConvertJsonDecksTaskOptions prepareConvertJsonDecks(String[] strings, String outputDir) {
		return ConvertJsonDecksTaskOptions.create(strings, outputDir);
	}

	public static UpdateLocalDbTaskOptions prepareUpdateLocalDb() {
		return UpdateLocalDbTaskOptions.create();
	}
}
