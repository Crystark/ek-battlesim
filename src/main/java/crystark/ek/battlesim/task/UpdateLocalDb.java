package crystark.ek.battlesim.task;

import crystark.common.api.Server;
import crystark.common.db.Database;
import crystark.ek.DataFetcher;
import crystark.ek.DataFetcherFactory;

import java.io.IOException;

class UpdateLocalDb extends RunnableTask {
	private final Server server;

	public UpdateLocalDb(final TaskOptions opts) {
		super(opts);
		server = opts.server;
	}

	@Override
	public void run() {
		DataFetcher df = DataFetcherFactory.get(server, true);
		try {
			this.updateProgress(10, "Connecting to " + server.name() + " server");
			df.login();
			this.updateProgress(40, "Retrieving server data");
			String cards = df.getCardsData();
			this.updateProgress(65);
			String skills = df.getSkillsData();
			this.updateProgress(75);
			String runes = df.getRunesData();
			this.updateProgress(85);
			String maps = df.getMapsData();
			this.updateProgress(95, "Writing db files to disk");
			Database.writeDb(server.game(), cards, skills, runes, maps);
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
