package crystark.ek.battlesim.task;

import java.util.Objects;

import crystark.common.api.DeckDesc;
import crystark.common.events.Warnings;
import crystark.common.transform.DeckDescTransformer;
import crystark.common.validator.DeckDescValidator;

abstract class DeckSim extends Sim {
	protected final DeckDesc[]	attackers;
	protected final DeckDesc[]	defenders;

	public DeckSim(final TaskOptions opts) {
		super(opts);

		attackers = opts.attackerDecks;
		defenders = opts.defenderDecks;

		String firstDeck = Objects.toString(attackers[0].deckName, "Attacker deck " + 1);

		for (int c = 0; c < opts.attackerDecks.length; c++) {
			int i = c;
			attackers[i] = DeckDescTransformer.create()
				.withPlayerNameTransformer(dd -> dd.playerName == null ? "Attacker " + i : dd.playerName)
				.apply(attackers[i]);
			DeckDesc attacker = attackers[i];
			DeckDescValidator.validate(attacker);

			String name = Objects.toString(attacker.deckName, "Attacker deck " + (i + 1));
			if (attacker.getCardsCost() > attacker.getMaxCost()) {
				Warnings.send("Deck '" + name + "' cost is over your player allowed cost");
			}
			if (!Objects.equals(attacker.playerLevel, attackers[0].playerLevel)) {
				Warnings.send("Deck '" + name + "' and deck '" + firstDeck + "' player levels do not match !");
			}
		}

		for (int c = 0; c < defenders.length; c++) {
			int i = c;
			defenders[i] = DeckDescTransformer.create()
				.withPlayerNameTransformer(dd -> dd.playerName == null ? "Defender " + i : dd.playerName)
				.apply(defenders[i]);
			DeckDesc defender = defenders[i];
			DeckDescValidator.validate(defender);
		}
	}

	@Override
	public void onComplete() {
		for (DeckDesc desc : attackers) {
			printPlayer(desc);
			psep();
		}

		super.onComplete();
	}
}
