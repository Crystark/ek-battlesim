package crystark.ek.battlesim.task;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import com.google.gson.Gson;

import crystark.common.db.Database;
import crystark.common.db.model.Deck;
import crystark.common.db.model.ek.EkDto;
import crystark.common.db.model.ek.EkUserDeck;
import crystark.common.format.DeckDescFormat;
import crystark.ek.Constants;
import crystark.tools.Tools;

class ConvertJsonDecks extends RunnableTask {
	private final String	outputFolder;
	private final String[]	inputFiles;

	public ConvertJsonDecks(final TaskOptions opts) {
		super(opts);

		inputFiles = opts.inputFiles;
		outputFolder = opts.outputDir;

		// Check output folder exists and is valid
		File o = new File(outputFolder);
		if (!o.exists()) {
			throw new TaskException("Output path " + outputFolder + " does not exist. Please create it.");
		}
		if (!o.isDirectory()) {
			throw new TaskException("Output path " + outputFolder + " isn't a directory. Please choose a directory as an output path.");
		}
	}

	@Override
	public void run() {
		Gson GSON = new Gson();
		Database db = Database.get();
		final String baseOutputPath = outputFolder + '/';

		Tools.listFiles(inputFiles)
			.forEach(f -> {
				try {
					String json = Tools.readFile(f);
					EkUserDeck ekUser = GSON.fromJson(json, EkDto.class).data.DefendPlayer;
					Deck d = new Deck(db, ekUser);

					String output = baseOutputPath + f.getName();
					Tools.writeToFile(output, DeckDescFormat.toDeckString(d.toDeckDesc(), Constants.TITLE + " - JSON Deck conversion on " + Constants.DATE_FORMAT.format(new Date())));
				}
				catch (IOException e) {
					throw new RuntimeException(e);
				}
			});

	}
}
