package crystark.ek.battlesim.task;

public class ConvertJsonDecksTaskOptions extends SimTaskOptions<ConvertJsonDecksTaskOptions> {

	static ConvertJsonDecksTaskOptions create(String[] strings, String outputDir) {
		return new ConvertJsonDecksTaskOptions()
			.inputFiles(strings)
			.outputDir(outputDir);
	}

	private ConvertJsonDecksTaskOptions inputFiles(String[] inputFiles) {
		this.inputFiles = inputFiles;
		return this;
	}

	private ConvertJsonDecksTaskOptions outputDir(String outputDir) {
		this.outputDir = outputDir;
		return this;
	}

	@Override
	protected Task buildTask() {
		return new ConvertJsonDecks(this);
	}
}
