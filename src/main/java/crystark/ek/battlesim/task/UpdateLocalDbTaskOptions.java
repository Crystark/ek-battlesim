package crystark.ek.battlesim.task;

import crystark.common.api.Server;
import crystark.ek.EkbsConfig;

public class UpdateLocalDbTaskOptions extends SimTaskOptions<UpdateLocalDbTaskOptions> {

	static UpdateLocalDbTaskOptions create() {
		return new UpdateLocalDbTaskOptions()
			.server(EkbsConfig.current().defaultServer());
	}

	private UpdateLocalDbTaskOptions server(Server server) {
		this.server = server;
		return this;
	}

	@Override
	protected Task buildTask() {
		return new UpdateLocalDb(this);
	}
}
