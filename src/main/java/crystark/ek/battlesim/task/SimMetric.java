package crystark.ek.battlesim.task;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.LongAdder;

class SimMetric {
	final Queue<Long> values;
	final LongAdder  count = new LongAdder();
	final LongAdder  total = new LongAdder();
	final AtomicLong min   = new AtomicLong(Long.MAX_VALUE);
	final AtomicLong max   = new AtomicLong();

	public SimMetric() {
		this(50000);
	}

	public SimMetric(int expectedValue) {
		//values = Collections.synchronizedList(new ArrayList<Long>(expectedValue));
		values = new ConcurrentLinkedQueue();
	}

	public void update(long i) {

		count.increment();
		values.add(i);
		total.add(i);

		long current;
		do {
			current = min.get();
		} while (i < current && !min.compareAndSet(current, i));

		do {
			current = max.get();
		} while (i > current && !max.compareAndSet(current, i));
	}

	private void check() {
		if (count.sum() == 0)
			throw new IllegalStateException("You need to call update at least once");
	}

	public long getMin() {
		check();
		return min.get();
	}

	public long getMax() {
		check();
		return max.get();
	}

	public long getTotal() {
		check();
		return total.longValue();
	}

	public float getMean() {
		check();
		return total.longValue() / (float) count.longValue();
	}

	public float getMeanOrDefault(int def) {
		return count.sum() == 0 ? def : total.longValue() / (float) count.longValue();
	}

	public long getCount() {
		return count.longValue();
	}

	public Map<Long[], Float> getHystogramAsMap(int nBars) {
		Long[] values = this.values.toArray(new Long[this.values.size()]);
		Arrays.sort(values);
		int size = values.length;

		long amplitude = getMax() - getMin() + 1;
		nBars = (int) Math.min(amplitude, nBars);
		nBars = Math.min(size, nBars);
		float range = amplitude / (float) nBars;

		int precision;
		if (range > 100000) {
			precision = 1000;
		}
		else if (range > 10000) {
			precision = 100;
		}
		else if (range > 1000) {
			precision = 10;
		}
		else {
			precision = 1;
		}

		Map<Long[], Float> map = new LinkedHashMap<>(nBars);
		long higherValue = getMin() - 1;
		int higherKey = -1;
		for (int i = 0; i < nBars; i++) {
			long lowerValue = higherValue + 1;
			if (i == nBars - 1) {
				higherValue = getMax();
			}
			else {
				higherValue = precision * Math.round((getMin() + range * (i + 1) - 1) / precision);
			}

			int lowerKey = higherKey + 1;
			if (higherKey < 0) {
				higherKey = Math.abs(higherKey) - 1;
			}
			while (higherKey < size - 1 && values[higherKey + 1] <= higherValue)
				higherKey++;

			map.put(new Long[] { lowerValue, higherValue }, (100 * (higherKey - lowerKey + 1) / (float) size));
		}

		return map;
	}
}
