package crystark.ek.battlesim.task;

import crystark.common.api.Server;
import crystark.common.db.Database;
import crystark.common.db.model.League;
import crystark.common.format.DeckDescFormat;
import crystark.ek.Constants;
import crystark.ek.DataFetcher;
import crystark.ek.DataFetcherFactory;
import crystark.tools.Tools;

import java.io.File;
import java.io.IOException;
import java.util.Date;

class ExtractLeagueDecks extends RunnableTask {
	private final Server	server;
	private final String	outputFolder;

	public ExtractLeagueDecks(final TaskOptions opts) {
		super(opts);

		server = opts.server;
		outputFolder = opts.outputDir;

		// Check output folder exists and is valid
		File o = new File(outputFolder);
		if (!o.exists()) {
			throw new TaskException("Output path " + outputFolder + " does not exist. Please create it.");
		}
		if (!o.isDirectory()) {
			throw new TaskException("Output path " + outputFolder + " isn't a directory. Please choose a directory as an output path.");
		}
	}

	@Override
	public void run() {
		DataFetcher df = DataFetcherFactory.get(server);
		Database db = Database.get();
		League league;

		try {
			this.updateProgress(10, "Connecting to " + server.name() + " server");
			df.login();
			this.updateProgress(50, "Fetching " + server.name() + " FoH decks");
			league = new League(db, df.getLeagueData());
		}
		catch (IOException e) {
			throw new TaskException("An error occured while trying to retrieve FoH data. Please make sure your internet connection is available.", e);
		}

		final String baseOutputPath = (outputFolder + '/' + Tools.normalizeStringForFile(league.condition()).toLowerCase() + '/' + Constants.SERVER_DAY_FORMAT.format(new Date())) + '-' + server.name() + '-';

		this.updateProgress(95, "Writing FoH decks to disk");
		league
			.deckDescsForRound(1)
			.map(deck -> new String[] { deck.playerName, DeckDescFormat.toDeckString(deck, Constants.TITLE + " - FoH season #" + league.season() + " deck extracted on " + Constants.DATE_FORMAT.format(new Date())) })
			.toBlocking()
			.forEach(t -> Tools.writeToFile(baseOutputPath + Tools.normalizeStringForFile(t[0]) + ".txt", t[1]));

	}
}
