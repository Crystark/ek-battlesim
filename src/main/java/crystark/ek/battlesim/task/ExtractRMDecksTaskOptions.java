package crystark.ek.battlesim.task;

import crystark.common.api.Server;

public class ExtractRMDecksTaskOptions extends SimTaskOptions<ExtractRMDecksTaskOptions> {

	static ExtractRMDecksTaskOptions create(Server server, String outputDir) {
		return new ExtractRMDecksTaskOptions()
			.server(server)
			.outputDir(outputDir);
	}

	private ExtractRMDecksTaskOptions server(Server server) {
		this.server = server;
		return this;
	}

	private ExtractRMDecksTaskOptions outputDir(String outputDir) {
		this.outputDir = outputDir;
		return this;
	}

	public ExtractRMDecksTaskOptions from(Integer extractFrom) {
		this.extractFrom = extractFrom;
		return this;
	}

	public ExtractRMDecksTaskOptions count(Integer extractCount) {
		this.extractCount = extractCount;
		return this;
	}

	@Override
	protected Task buildTask() {
		return new ExtractRMDecks(this);
	}
}
