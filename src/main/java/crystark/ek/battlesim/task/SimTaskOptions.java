package crystark.ek.battlesim.task;

@SuppressWarnings("unchecked")
public abstract class SimTaskOptions<T extends SimTaskOptions<T>> extends BaseTaskOptions<T> {
	public T threads(Integer threads) {
		this.threads = threads;
		return (T) this;
	}

	public T iterations(Long iterations) {
		this.iterations = iterations;
		return (T) this;
	}
}
