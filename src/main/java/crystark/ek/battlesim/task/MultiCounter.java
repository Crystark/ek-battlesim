package crystark.ek.battlesim.task;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class MultiCounter {
	AtomicInteger		global	= new AtomicInteger();
	List<AtomicInteger>	counters;

	public MultiCounter(int counters) {
		this.counters = new ArrayList<>(counters);
		for (int i = 0; i < counters; i++) {
			this.counters.add(new AtomicInteger());
		}
	}

	public int incrementAndGet(int index) {
		global.incrementAndGet();
		return counters.get(index).incrementAndGet();
	}

	public int get(int index) {
		return counters.get(index).get();
	}

	public int get() {
		return global.get();
	}

}
