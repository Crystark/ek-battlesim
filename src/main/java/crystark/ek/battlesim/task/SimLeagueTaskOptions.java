package crystark.ek.battlesim.task;

import crystark.common.api.Server;

public class SimLeagueTaskOptions extends SimTaskOptions<SimLeagueTaskOptions> {

	static SimLeagueTaskOptions create(Server server) {
		return new SimLeagueTaskOptions()
			.server(server);
	}

	private SimLeagueTaskOptions server(Server server) {
		this.server = server;
		return this;
	}

	@Override
	protected Task buildTask() {
		return new SimLeague(this);
	}
}
