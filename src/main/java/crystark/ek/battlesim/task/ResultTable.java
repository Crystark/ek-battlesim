package crystark.ek.battlesim.task;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import crystark.tools.Tools;

public class ResultTable {
	public String						id;
	public String						title;
	public ArrayList<String>			headers	= new ArrayList<>();
	public ArrayList<ArrayList<Object>>	lines	= new ArrayList<>();
	public ArrayList<Object>			footer	= new ArrayList<>();

	private List<Integer>				colSizes;

	public ResultTable(String id) {
		this.id = id;
	}

	public List<Integer> getSizes() {
		if (colSizes == null) {
			colSizes = Stream
				.concat(
					Stream.of(headers, footer),
					lines.stream())
				.map(al -> al.stream().map(o -> o.toString().length()).collect(Collectors.toList()))
				.reduce(
					new ArrayList<>(headers.size()),
					(e1, e2) -> {
						ArrayList<Integer> al = new ArrayList<>();
						for (int i = 0; i < Math.max(e1.size(), e2.size()); i++) {
							int s1 = 0;
							int s2 = 0;
							if (i < e1.size()) {
								s1 = e1.get(i);
							}
							if (i < e2.size()) {
								s2 = e2.get(i);
							}
							al.add(s1 < s2 ? s2 : s1);
						}
						return al;
					});
		}
		return colSizes;
	}

	public void addLine(Object... objects) {
		this.lines.add(Tools.newArrayList(objects));
	}

	public void mergeColumns(ResultTable extended) {
		if (extended.headers.isEmpty() != this.headers.isEmpty()) {
			throw new IllegalArgumentException("Headers do not match.");
		}
		if (extended.lines.size() != this.lines.size()) {
			throw new IllegalArgumentException("Number of lines do not match.");
		}
		if (extended.footer.isEmpty() != this.footer.isEmpty()) {
			throw new IllegalArgumentException("Footer do not match.");
		}

		this.headers.addAll(extended.headers);
		for (int i = 0; i < extended.lines.size(); i++) {
			this.lines.get(i).addAll(extended.lines.get(i));
		}
		this.footer.addAll(extended.footer);
	}
}
