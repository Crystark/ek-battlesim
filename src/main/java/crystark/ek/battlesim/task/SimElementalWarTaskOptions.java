package crystark.ek.battlesim.task;

import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

import crystark.common.api.CardType;
import crystark.common.api.DeckDesc;
import crystark.common.db.Database;
import crystark.common.db.model.BaseCard;
import crystark.common.format.DeckDescFormat;
import crystark.common.transform.DeckDescTransformer;
import crystark.tools.Tools;

public class SimElementalWarTaskOptions extends SimTaskOptions<SimElementalWarTaskOptions> {
	public static final DeckDescTransformer DEFAULTS = DeckDescTransformer.create()
		.withInfiniteHpTransformer(t -> true)
		.withBypassValidationTransformer(t -> true);

	static SimElementalWarTaskOptions create(DeckDesc deck, String legendaryDeck) {
		return new SimElementalWarTaskOptions()
			.attackerDecks(deck)
			.defenderDecks(getEwDeck(legendaryDeck));
	}

	@Override
	protected Task buildTask() {
		return new SimElementalWar(this);
	}

	@Override
	protected SimElementalWarTaskOptions defenderDecks(DeckDesc... decks) {
		return super.defenderDecks(Arrays.stream(decks)
			.map(DEFAULTS)
			.toArray(DeckDesc[]::new));
	}

	public static DeckDesc getEwDeck(String deck) {
		return Stream
			.<Function<String, DeckDesc>> of(
				s -> {
					try {
						String path = "decks/ew/";
						return Optional.of(s)
							.map(SimElementalWarTaskOptions::normalizeDeckName)
							.map(n -> Tools.readResource(path + n + ".txt"))
							.map(str -> DeckDescFormat.parseString(s, str))
							.get();
					}
					catch (Throwable t) {}
					return null;
				} ,
				s -> {
					try {
						if (Paths.get(s.trim()).toFile().exists()) {
							return DeckDescFormat.parseFile(s);
						}
					}
					catch (Throwable t) {}
					return null;
				} ,
				s -> DeckDescFormat.parseString(null, s))
			.map(f -> f.apply(deck))
			.filter(Objects::nonNull)
			.map(DEFAULTS::apply)
			.findFirst()
			.orElseThrow(() -> new TaskException("Not a valid EW Boss: " + deck));
	}

	public static List<String> getAvailableBosses() {
		String path = "decks/ew/";
		return Database.get().cards().all()
			.filter(c -> CardType.hydra.equals(c.type()) && !c.name().startsWith("Hydra"))
			.map(BaseCard::name)
			.filter(n -> ClassLoader.getSystemResource(path + normalizeDeckName(n) + ".txt") != null)
			.toSortedList()
			.toBlocking()
			.first();
	}

	static String normalizeDeckName(String name) {
		return name
			.toLowerCase()
			.replace("(", "")
			.replace(")", "")
			.replace("legendary", "")
			.trim()
			.replace(' ', '_');
	}
}
