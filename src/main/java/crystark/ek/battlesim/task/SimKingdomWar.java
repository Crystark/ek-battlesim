package crystark.ek.battlesim.task;

import crystark.common.api.BattleDesc;
import crystark.common.api.DeckDesc;
import crystark.ek.Helper;
import crystark.ek.battlesim.battle.Battle;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @see SimKingdomWarTest
 */
class SimKingdomWar extends DeckSim {
	private static final String	RESULT_TABLE	= "SimKingdomWar";

	final SimMetric				fights;
	final SimMetric 			rounds;
	final AtomicInteger			deathByHero;
	final AtomicInteger			deathByCards;

	final Integer				maxFights;

	public SimKingdomWar(final TaskOptions opts) {
		super(opts);

		this.maxFights = opts.limit == null ? 500 : opts.limit;
		this.fights = new SimMetric(maxFights);
		this.rounds = new SimMetric();
		this.deathByHero = new AtomicInteger();
		this.deathByCards = new AtomicInteger();
	}

	@Override
	protected int getDefaultIter(TaskOptions opts) {
		return 10000;
	}

	private DeckDesc randomDefender() {
		return defenders[Helper.random(defenders.length)];
	}

	@Override
	protected Runnable newRunnableInstance(final long iter, final AtomicInteger metricCounter, final AtomicInteger metricErrors) {
		return () -> {
			for (int i = 0; i < iter; i++) {
				try {
					Battle battle = null;
					BattleDesc desc = new BattleDesc(attackers[0], randomDefender());
					int f;
					for (f = 0; f < maxFights; f++) {
						battle = Battle.run(desc);
						rounds.update(battle.getRound());
						desc = battle.nextIfWon(randomDefender());

						if (desc == null) {
							f++; // Cause number of fights = f + 1 unless we reach the end of the loop
							break;
						}
					}
					fights.update(f);
					if (battle != null) {
						if (battle.attackerHealth() > 0) {
							deathByCards.getAndIncrement();
						}
						else {
							deathByHero.incrementAndGet();
						}

					}
					metricCounter.getAndIncrement(); // only increment once per battle
				}
				catch (Throwable t) {
					metricErrors.getAndIncrement();
					t.printStackTrace();
					throw t;
				}
			}
		};
	}

	static int honorPerWinMin(int atk, int health) {
		int atkAndHealth = atk + health;
		return (int) (10 + 2 * atkAndHealth / 1000f);
	}

	static int toNextHonor(int atk, int health) {
		int honorPerWin = honorPerWinMin(atk, health);
		return (500 * (honorPerWin - 9)) - atk - health;
	}

	@Override
	Results buildSimResults() {
		long count = this.getIterCount();

		Results results = super.buildSimResults();
		results.title = "Kingdom War Simulation";
		results.maxRunsPerIter = 0;

		ResultTable rt = new ResultTable(RESULT_TABLE);

		int cardAtk = attackers[0].getCardsAtk();
		int cardHp = attackers[0].getCardsHp();
		int honorPerWin = honorPerWinMin(cardAtk, cardHp);
		int nextHonor = toNextHonor(cardAtk, cardHp);
		rt.addLine("Average wins per loss", f2s(fights.getMean()));
		rt.addLine("Approximate honor per win", honorPerWin + "-" + (honorPerWin + 5) + " (next in " + nextHonor + ')');
		rt.addLine("Losses from out of HP", f2s(100 * deathByHero.get() / (float) count) + '%');
		rt.addLine("Losses from out of Cards", f2s(100 * deathByCards.get() / (float) count) + '%');
		results.tables.put(rt.id, rt);

		ResultChart rsRound = new ResultChart();
		rsRound.title = "Consecutive fights repartition chart";
		rsRound.hystogramAsMap = fights.getHystogramAsMap(10);
		results.charts.put(rsRound.title, rsRound);

		return results;
	}

}
