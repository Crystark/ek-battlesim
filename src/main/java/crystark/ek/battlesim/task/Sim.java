package crystark.ek.battlesim.task;

import crystark.common.api.CardDesc;
import crystark.common.api.DeckDesc;
import crystark.common.api.RuneDesc;
import crystark.common.events.Warnings;
import crystark.common.format.CardDescFormat;
import crystark.common.format.RuneDescFormat;
import crystark.ek.Constants;
import crystark.tools.Tools;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringJoiner;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

abstract class Sim extends BaseTask {

	private final AtomicInteger metricCounter = new AtomicInteger();
	private final AtomicInteger metricErrors  = new AtomicInteger();
	private final   int  threads;
	private final   int  tasks;
	private final   int  iterPerTask;
	private final   int  iterRemains;
	protected final long iterTotal;

	private Results results;

	protected int getDefaultIter(TaskOptions opts) {
		return 50000;
	}

	public Sim(final TaskOptions opts) {
		super(opts);

		int iter;
		int nThreads;
		if (Constants.IS_DEBUG) {
			nThreads = 1;
			iter = (int) (opts.iterations == null ? 1 : opts.iterations);
		}
		else {
			iter = (int) (opts.iterations == null ? getDefaultIter(opts) : opts.iterations);
			nThreads = opts.threads == null ? Constants.THREADS - 1 : opts.threads;
		}

		// Tasks with much iterations lasts unevenly. Less iterations per task makes threads utilization more evenly.
		// 1 iteration per task is acceptable if task initialization costs nothing, but
		// too large task queue is not good for memory footprint
		iterPerTask = Math.min(10, iter);
		tasks = iter / iterPerTask;
		iterRemains = iter % iterPerTask;
		iterTotal = iter;
		threads = Math.min(nThreads, tasks); // Do not tasks+1, because iterRemains will be processed by first freed thread.
	}

	protected abstract Runnable newRunnableInstance(long iter, AtomicInteger metricCounter, AtomicInteger metricErrors);

	Results buildSimResults() {
		Results results = new Results();
		results.iterations = this.getIterCount();
		results.durationMillis = this.getDurationMillis();

		return results;
	}

	public final Results getSimResults() {
		if (results == null) {
			results = buildSimResults();
		}
		return results;
	}

	final void printSimOutputHeader() {
		Results sr = getSimResults();

		if (sr.title != null) {
			pt(sr.title);
		}
		pt("Results of simulation (" + (sr.maxRunsPerIter > 1 ? "max " + sr.maxRunsPerIter + " x " : "") + sr.iterations + " fights, " + sr.durationMillis + "ms):");
	}

	final void printSimOutput() {
		Results sr = getSimResults();
		for (Entry<String, ResultTable> entry : sr.tables.entrySet()) {
			ResultTable rt = entry.getValue();
			List<Integer> sizes = rt.getSizes();
			StringJoiner joiner = new StringJoiner("s | %-", "%-", "s");
			for (Integer integer : sizes) {
				joiner.add(integer.toString());
			}
			String pattern = joiner.toString();

			// Start printing
			p();
			if (rt.title != null) {
				pt(rt.title);
			}
			if (!rt.headers.isEmpty()) {
				String header = String.format(pattern, rt.headers.toArray());
				p(header);
				p(header.replaceAll(".", "-"));
			}
			for (ArrayList<Object> line : rt.lines) {
				if (line.isEmpty()) {
					p();
				}
				else {
					p(String.format(pattern, line.toArray()));
				}
			}
			if (!rt.footer.isEmpty()) {
				String global = String.format(pattern, rt.footer.toArray());
				p(global.replaceAll(".", "-"));
				p(global);
			}
		}

		for (Entry<String, ResultChart> entry : sr.charts.entrySet()) {
			ResultChart rs = entry.getValue();
			p();
			if (rs.title != null) {
				pt(rs.title);
			}
			printBarChart(rs.hystogramAsMap);
		}
	}

	@Override
	public String getCurrentAction() {
		return null;
	}

	@Override
	public void onComplete() {
		super.onComplete();
		if (checkErrorForOutput()) {
			psep();
			printSimOutputHeader();
			psep();
			printSimOutput();
			psep();
			Warnings.getAndReset().forEach(this::pw);
		}
	}

	protected long expectedTotalCount() {
		return iterTotal;
	}

	@Override
	public int getParallelism() {
		return threads;
	}

	@Override
	public float getProgress() {
		return metricCounter.get() / (float) expectedTotalCount();
	}

	@Override
	public boolean isInterrupted() {
		return metricErrors.get() >= tasks;
	}

	@Override
	public void run(ExecutorService exec) {
		List<Callable<Object>> callables = new ArrayList<>(tasks + 1);
		int inc = iterRemains / threads;
		int remains = iterRemains % threads;
		for (int i = 0; i < threads; i++) {
			callables.add(Executors.callable(this.newRunnableInstance(iterPerTask + inc, metricCounter, metricErrors)));
		}
		for (int i = 0; i < tasks - threads; i++) {
			callables.add(Executors.callable(this.newRunnableInstance(iterPerTask, metricCounter, metricErrors)));
		}
		if (remains > 0) {
			callables.add(Executors.callable(this.newRunnableInstance(remains, metricCounter, metricErrors)));
		}
		try {
			exec.invokeAll(callables);
		}
		catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

	void printPlayer(DeckDesc desc) {
		String playerName = desc.playerName;
		int playerLvl = desc.playerLevel;
		int playerHp = desc.health;
		List<CardDesc> cards = desc.cardsDescs;
		List<RuneDesc> runes = desc.runesDescs;

		psep();
		pt(playerName + " (level " + playerLvl + ", " + playerHp + " HP, " + desc.getMaxCost() + " max cost)");
		psep();
		p("Deck:  (" + desc.cardsDescs.size() + "/10, " + desc.getCardsCost() + " cost)");
		cards.stream()
			.map(CardDescFormat::toFullDescription)
			.map(s -> '\t' + s)
			.forEach(this::p);
		p("Runes:");
		runes.stream()
			.map(RuneDescFormat::toFullDescription)
			.map(s -> '\t' + s)
			.forEach(this::p);
		p();
	}

	void printBarChart(Map<Long[], Float> bars) {
		String pattern = "%8s - %-8s | %5s%% ";
		for (Entry<Long[], Float> e : bars.entrySet()) {
			Long[] range = e.getKey();
			Float value = e.getValue();
			int barLength = (int) Math.floor(value / 2);
			p(String.format(pattern, range[0], range[1], f2s(value, 1)) + String.join("", Collections.nCopies(barLength, "=")));
		}
	}

	protected boolean checkErrorForOutput() {
		if (getErrorCount() > 0) {
			pw("One or more error occured.");
			if (getIterCount() == 0) {
				pw("Unable to print results.");
				return false;
			}
		}
		return true;
	}

	protected final long getIterCount() {
		return this.metricCounter.get();
	}

	protected final long getErrorCount() {
		return this.metricErrors.get();
	}

	protected final long getDurationMillis() {
		return Math.round(this.duration / 1e6);
	}

	static float ratio(long a, long b) {
		return b == 0 ? 0 : (10000 * a / b) / 100f;
	}

	static String ratioS(long a, long b) {
		return f2s(ratio(a, b)) + '%';
	}

	static String f2s(Float f) {
		return f2s(f, 1);
	}

	static String f2s(Float f, int scale) {
		return Tools.float2String(f, scale);
	}

}
