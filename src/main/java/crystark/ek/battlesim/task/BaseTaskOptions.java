package crystark.ek.battlesim.task;

import java.util.function.BiConsumer;

import crystark.common.api.DeckDesc;

@SuppressWarnings("unchecked")
public abstract class BaseTaskOptions<T extends BaseTaskOptions<T>> extends TaskOptions {

	protected T attackerDecks(DeckDesc... decks) {
		this.attackerDecks = decks;
		return (T) this;
	}

	protected T defenderDecks(DeckDesc... decks) {
		this.defenderDecks = decks;
		return (T) this;
	}

	public T progressListener(BiConsumer<Float, String> progressListener) {
		this.progressListener = progressListener;
		return (T) this;
	}

	public T printOutput(Boolean printOutput) {
		this.printOutput = printOutput;
		return (T) this;
	}
}
