package crystark.ek.battlesim.task;

import java.util.function.BiConsumer;

import crystark.common.api.DeckDesc;
import crystark.common.api.Server;

public abstract class TaskOptions {
	// TaskRunner
	//Boolean			progressBar;		// = true;
	BiConsumer<Float, String>	progressListener;
	// BaseTask
	Boolean						printOutput;		//	= true;
	// Sim
	Integer						threads;			//	= Constants.THREADS;
	Long						iterations;
	Integer						limit;

	DeckDesc[]					attackerDecks;
	DeckDesc[]					defenderDecks;
	Server						server;
	String[]					inputFiles;
	String						outputDir;
	Integer						deckSwitchAtHp;

	Boolean						showDamage;
	Integer						extractFrom;
	Integer						extractCount;

	Boolean						hydraOnly;

	protected abstract Task buildTask();

	public Task run() {
		return new TaskRunner(buildTask()).run();
	}
}
