package crystark.ek.battlesim.task;

import crystark.common.api.BattleDesc;
import crystark.common.api.DeckDesc;
import crystark.ek.battlesim.battle.Battle;
import crystark.tools.Tools;

/**
 * @see SimHydraTest
 */
class SimHydra extends MultiDefenderSim {
	private static final String	ISOLATE_TABLE	= "SimHydraIsolate";
	final MultiCounter[]		phase2;
	final MultiSimMetric		p2Switch;
	final boolean				isHydraOnly;
	final Integer				deckSwitchAtHp;

	public SimHydra(final TaskOptions opts) {
		super(opts);

		this.isHydraOnly = opts.hydraOnly;
		this.deckSwitchAtHp = opts.deckSwitchAtHp;
		this.phase2 = new MultiCounter[maxFightsPerBattle];
		this.p2Switch = new MultiSimMetric(defenders.length);

		if (!this.isHydraOnly) {
			for (int f = 0; f < maxFightsPerBattle; f++) {
				phase2[f] = new MultiCounter(defenders.length);
			}
		}
	}

	@Override
	protected int getDefaultIter(TaskOptions opts) {
		return 10000;
	}

	protected void countPhase2(int def, int fight) {
		phase2[fight].incrementAndGet(def);
	}

	@Override
	protected void countFights(int def, int fight, Battle battle) {
		super.countFights(def, fight, battle);

		if (!this.isHydraOnly && shouldSwitchDeck(battle.defenderSnapshot())) {
			countPhase2(def, fight);
		}
	}

	public DeckDesc isolatedHydraDeck() {
		return hasIsolatedHydraDeck() ? attackers[1] : null;
	}

	public boolean hasIsolatedHydraDeck() {
		return attackers.length > 1;
	}

	protected boolean shouldSwitchDeck(DeckDesc defender) {
		if (this.deckSwitchAtHp != null) {
			return defender.getCardsHp() <= this.deckSwitchAtHp;
		}
		return defender.cardsDescs.size() < 2;
	}

	@Override
	protected BattleDesc getNextBattle(int def, int f, Battle previousBattle) {
		DeckDesc nextDefender = previousBattle.defenderSnapshot();
		DeckDesc nextAttacker = previousBattle.desc.attacker;

		// Check if attacker should switches deck
		if (!shouldSwitchDeck(previousBattle.desc.defender)) { // if it switched deck on previous battle then no need to switch again
			if (shouldSwitchDeck(nextDefender)) {
				// Mark this fight as beeing the one which must triger a switch
				this.p2Switch.update(def, f + 1);// +1 cause iteration starts at 0

				// Do the switch only if the player has an alt deck
				DeckDesc next = isolatedHydraDeck();
				if (next != null) {
					nextAttacker = next;
				}
			}
		}

		if (!previousBattle.defenderWon()) {
			return null;
		}

		return new BattleDesc(nextAttacker, nextDefender);
	}

	@Override
	Results buildSimResults() {
		int maxFights = (int) this.fights.getMax();

		Results results = super.buildSimResults();
		results.title = "Hydra Simulation";

		// Alter main table title
		ResultTable mainTable = results.tables.get(MultiDefenderSim.RESULT_TABLE);
		if (hasIsolatedHydraDeck()) {
			mainTable.title = "Hydra kill with deck switch once " + (deckSwitchAtHp == null ? "hydra is isolated" : "hydra's deck under " + deckSwitchAtHp + " HP");
		}
		else {
			mainTable.title = "One-deck " + (this.isHydraOnly ? "isolated " : "") + "hydra kill";
		}

		// Add new table output
		if (!this.isHydraOnly) {
			ResultTable rt = new ResultTable(ISOLATE_TABLE);
			if (deckSwitchAtHp == null) {
				rt.title = "Battles to isolate Hydra";
			}
			else {
				rt.title = "Battles to get hydra under " + deckSwitchAtHp + " HP";
			}

			if (maxFights > 1) {
				rt.headers.add("");
				rt.headers.add("~ bat.");
				for (int i = 1; i <= maxFights; i++) {
					rt.headers.add((i < 10 ? "bat. " : "bt. ") + i);
				}
			}

			for (int d = 0; d < defenders.length; d++) {
				rt.addLine(phase2Data(maxFights, d));
			}

			if (defenders.length > 1) {
				rt.footer = Tools.newArrayList(globalPhase2Data("Global", maxFights));
			}
			results.tables.put(rt.id, rt);
		}

		return results;
	}

	private Object[] phase2Data(int maxFights, int def) {
		Object[] args = new Object[2 + maxFights];
		int n = 0;
		args[n++] = PREFIX_DEFENDER + defenders[def].playerName;
		args[n++] = capedAvgString(p2Switch.getMeanOrDefault(def, maxFightsPerBattle), maxFightsPerBattle);
		for (int f = 0; f < maxFights; f++) {
			args[n++] = phase2RatioString(def, f);
		}
		return args;
	}

	private Object[] globalPhase2Data(String title, int maxFights) {
		Object[] args = new Object[2 + maxFights];
		int n = 0;
		args[n++] = title;
		args[n++] = capedAvgString(p2Switch.getMeanOrDefault(maxFightsPerBattle), maxFightsPerBattle);
		for (int f = 0; f < maxFights; f++) {
			args[n++] = phase2GlobalRatioString(f);
		}
		return args;
	}

	String phase2GlobalRatioString(int fight) {
		return phase2GlobalRatio(fight) + "%";
	}

	float phase2GlobalRatio(int fight) {
		return ratio(phase2[fight].get(), battles[fight].get());
	}

	String phase2RatioString(int def, int fight) {
		return fights.getMax(def) > fight ? phase2Ratio(def, fight) + "%" : "-";
	}

	float phase2Ratio(int def, int fight) {
		return ratio(phase2[fight].get(def), battles[fight].get(def));
	}
}
