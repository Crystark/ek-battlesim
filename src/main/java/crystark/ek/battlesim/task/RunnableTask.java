package crystark.ek.battlesim.task;

import crystark.common.events.Warnings;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class RunnableTask extends BaseTask implements Runnable {
	private final AtomicInteger	done	= new AtomicInteger();
	private String				currentAction;

	protected RunnableTask(TaskOptions opts) {
		super(opts);
	}

	@Override
	public void run(ExecutorService exec) {
		Future<?> r = exec.submit(this);
		try {
			r.get();
			updateProgress(100, null);
		}
		catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
		catch (ExecutionException e) {
			Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException) ? (RuntimeException) cause : new RuntimeException(cause);
		}
	}

	@Override
	public int getParallelism() {
		return 1;
	}

	protected void updateProgress(Number progress, String nextAction) {
		updateProgress(progress);
		this.currentAction = nextAction;
	}

	protected void updateProgress(Number progress) {
		done.set(progress.intValue());
	}

	@Override
	public float getProgress() {
		return done.get() / 100f;
	}

	@Override
	public String getCurrentAction() {
		return currentAction;
	}

	@Override
	public void onComplete() {
		super.onComplete();
		Warnings.getAndReset().forEach(this::pw);
	}
}
