package crystark.ek.battlesim.task;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.squareup.okhttp.Response;

import crystark.common.api.DeckDesc;
import crystark.common.api.Server;
import crystark.common.db.Database;
import crystark.common.db.model.Deck;
import crystark.common.db.model.ek.EkCompetitor;
import crystark.common.db.model.ek.EkUserDeck;
import crystark.common.events.Warnings;
import crystark.common.format.DeckDescFormat;
import crystark.ek.Constants;
import crystark.ek.DataFetcher;
import crystark.ek.DataFetcher.DataFetcherException;
import crystark.ek.DataFetcherFactory;
import crystark.tools.RxTransformers;
import crystark.tools.Tools;
import rx.Observable;

class ExtractRMDecks extends RunnableTask {
	private final Server	server;
	private final int		from;
	private final int		qte;
	private final String	outputFolder;

	public ExtractRMDecks(final TaskOptions opts) {
		super(opts);

		server = opts.server;
		from = opts.extractFrom == null ? 1 : opts.extractFrom;
		qte = opts.extractCount == null ? 100 : Math.min(100, opts.extractCount);
		outputFolder = opts.outputDir;

		// Check output folder exists and is valid
		File o = new File(outputFolder);
		if (!o.exists()) {
			throw new TaskException("Output path " + outputFolder + " does not exist. Please create it.");
		}
		if (!o.isDirectory()) {
			throw new TaskException("Output path " + outputFolder + " isn't a directory. Please choose a directory as an output path.");
		}
	}

	@Override
	public void run() {
		DataFetcher df = DataFetcherFactory.get(server, true);
		Database db = Database.get();
		List<EkCompetitor> competitors;

		float weight = 100f / (qte + 1);
		try {
			this.updateProgress(0, "Retrieving " + server.name() + " data.");
			competitors = df.getRankData(from, qte);
		}
		catch (IOException e) {
			throw new TaskException("An error occured while trying to retrieve RM data. Please make sure your internet connection is available.", e);
		}

		this.updateProgress(weight, "Fetching " + server.name() + " RM decks from rank " + from + " to rank " + (from + qte - 1));

		Observable
			.from(competitors)
			.compose(RxTransformers.pace(5, TimeUnit.SECONDS))
			.flatMap(c -> {
				try {
					EkUserDeck u = df.freeFight(c.Uid);
					return Observable.just(new CompetitorAndUser(c, u));
				}
				catch (IOException | TaskException t) {
					String msg = "Failed to fetch deck of " + c.NickName + " at rank " + c.Rank;
					if (t instanceof DataFetcherException) {
						Response rs = ((DataFetcherException) t).response;
						msg += " because of " + rs.code() + " - " + rs.message();
					}
					Warnings.send(msg);
					return Observable.empty();
				}
			})
			.toBlocking()
			.forEach(cau -> {
				EkCompetitor c = cau.competitor;
				EkUserDeck u = cau.user;

				DeckDesc d = new Deck(db, u).toDeckDesc();
				String fileName = String.format("%s/%06d-%s.txt", outputFolder, c.Rank, Tools.normalizeStringForFile(d.playerName));
				Tools.writeToFile(fileName, DeckDescFormat.toDeckString(d, Constants.TITLE + " - RM deck extracted on " + Constants.DATE_FORMAT.format(new Date())));
				this.updateProgress(weight * (c.Rank - from + 1));
			});
	}

	private static class CompetitorAndUser {
		public final EkCompetitor	competitor;
		public final EkUserDeck		user;

		public CompetitorAndUser(EkCompetitor c, EkUserDeck u) {
			this.competitor = c;
			this.user = u;
		}

	}
}
