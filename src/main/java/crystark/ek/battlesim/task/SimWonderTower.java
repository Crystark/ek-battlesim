package crystark.ek.battlesim.task;

/**
 *
 */
class SimWonderTower extends MultiDefenderSim {

	public SimWonderTower(final TaskOptions opts) {
		super(opts);

	}

	@Override
	protected int getDefaultIter(TaskOptions opts) {
		return 20000;
	}

	@Override
	Results buildSimResults() {
		Results sr = super.buildSimResults();
		sr.title = "Tower of Wonders simulation";
		return sr;
	}

}
