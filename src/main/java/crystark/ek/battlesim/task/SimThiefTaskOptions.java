package crystark.ek.battlesim.task;

import crystark.common.api.DeckDesc;
import crystark.common.format.DeckDescFormat;
import crystark.tools.Tools;

public class SimThiefTaskOptions extends SimTaskOptions<SimThiefTaskOptions> {
	static final String	THIEF_100					= Tools.readResource("decks/thief.100.txt");
	static final String	THIEF_LEGENDARY_100_41563	= Tools.readResource("decks/thief_legendary.100.41563.txt");
	static final String	THIEF_LEGENDARY_100_52898	= Tools.readResource("decks/thief_legendary.100.52898.txt");
	static final String	THIEF_LEGENDARY_100_75576	= Tools.readResource("decks/thief_legendary.100.75576.txt");

	public static SimThiefTaskOptions create(DeckDesc deck) {
		return new SimThiefTaskOptions()
			.attackerDecks(deck)
			.defenderDecks(getThieves());
	}

	static DeckDesc[] getThieves() {
		return new DeckDesc[] {
			DeckDescFormat.parseString("Thief 100", THIEF_100),
			DeckDescFormat.parseString("L. Thief 100 41563", THIEF_LEGENDARY_100_41563),
			DeckDescFormat.parseString("L. Thief 100 52898", THIEF_LEGENDARY_100_52898),
			DeckDescFormat.parseString("L. Thief 100 75576", THIEF_LEGENDARY_100_75576)
		};
	}

	@Override
	protected Task buildTask() {
		return new SimThief(this);
	}
}
