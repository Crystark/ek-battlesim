package crystark.ek.battlesim.task;

import crystark.common.api.DeckDesc;
import crystark.common.format.DeckDescFormat;
import crystark.common.transform.DeckDescTransformer;

import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Stream;

public class SimWonderTowerTaskOptions extends SimTaskOptions<SimWonderTowerTaskOptions> {
	public static final DeckDescTransformer DEFAULTS = DeckDescTransformer.create()
		.withBypassValidationTransformer(t -> true);

	static SimWonderTowerTaskOptions create(DeckDesc deck, DeckDesc... opponentsDecks) {
		return new SimWonderTowerTaskOptions()
			.attackerDecks(deck)
			//.defenderDecks(getDeck(fileNameOrStrings));
			.defenderDecks(opponentsDecks);
	}

	@Override
	protected Task buildTask() {
		return new SimWonderTower(this);
	}

	@Override
	protected SimWonderTowerTaskOptions defenderDecks(DeckDesc... decks) {
		return super.defenderDecks(Arrays.stream(decks)
			.map(DEFAULTS)
			.toArray(DeckDesc[]::new));
	}

	public static DeckDesc getDeck(String deck) {
		return Stream
			.<Function<String, DeckDesc>> of(
				s -> {
					try {
						if (Paths.get(s.trim()).toFile().exists()) {
							return DeckDescFormat.parseFile(s);
						}
					}
					catch (Throwable t) {}
					return null;
				} ,
				s -> DeckDescFormat.parseString(null, s))
			.map(f -> f.apply(deck))
			.filter(Objects::nonNull)
			.map(DEFAULTS::apply)
			.findFirst()
			.orElseThrow(() -> new TaskException("Not a valid deck: " + deck));
	}

}
