package crystark.ek.battlesim.task;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;

class TaskRunner {
	private final Task	task;
	private boolean		finished	= false;

	TaskRunner(Task task) {
		this.task = task;
	}

	Task run() {
		if (finished) {
			throw new IllegalStateException("Cannot run twice");
		}
		task.onStart();
		BiConsumer<Float, String> progressListener = task.getOptions().progressListener;
		int threads = task.getParallelism() + (progressListener == null ? 0 : 1);
		ExecutorService exec = Executors.newFixedThreadPool(threads);
		try {
			if (progressListener != null) {
				exec.submit(() -> {
					try {
						while (!task.isInterrupted()) {
							float progress = task.getProgress();

							progressListener.accept(progress, task.getCurrentAction());

							if (progress == 1f) {
								break;
							}
							try {
								Thread.sleep(200);
							}
							catch (InterruptedException e) {
								throw new RuntimeException(e);
							}
						}
					}
					catch (Exception e) {
						e.printStackTrace();
					}
				});
			}

			task.run(exec);

		}
		finally {
			exec.shutdown();
		}

		try {
			// Wait for the task to end
			exec.awaitTermination(10L, TimeUnit.MINUTES);
			finished = true;
			task.onComplete();
			return task;
		}
		catch (InterruptedException e) {
			throw new RuntimeException("Failed to finish task in less than ten minutes");
		}
	}
	//	protected final void checkFinished() {
	//		if (!finished) {
	//			throw new IllegalStateException("Task not yet finished or started");
	//		}
	//	}
}
