package crystark.ek.battlesim.task;

import java.util.concurrent.atomic.AtomicInteger;

import crystark.common.api.BattleDesc;
import crystark.common.api.DeckDesc;
import crystark.common.events.Warnings;
import crystark.ek.battlesim.battle.Battle;
import crystark.tools.Tools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

abstract class MultiDefenderSim extends DeckSim {
	private static final   Logger L               = LoggerFactory.getLogger(MultiDefenderSim .class);
	public static final    String RESULT_TABLE    = "MultiDefenderSim";
	protected static final String GLOBAL_LINE     = "Global";
	protected static final String PREFIX_DEFENDER = "vs ";
	final MultiSimMetric			fights;
	final MultiCounter[]			wins;
	final MultiCounter[]			battles;

	final int						maxFightsPerBattle;
	final int						longestName;
	final boolean					isTillDeath;

	protected int maxFightsPerBattle() {
		return 10;
	}

	public MultiDefenderSim(final TaskOptions opts) {
		super(opts);

		int d = defenders.length;

		int l = GLOBAL_LINE.length() - PREFIX_DEFENDER.length();
		for (DeckDesc desc : defenders) {
			int nameLength = desc.playerName.length();
			if (nameLength > l) {
				l = nameLength;
			}
		}
		longestName = l;

		// Prepare metrics
		this.maxFightsPerBattle = maxFightsPerBattle();
		this.isTillDeath = maxFightsPerBattle > 1;

		this.fights = new MultiSimMetric(d);
		this.wins = new MultiCounter[maxFightsPerBattle];
		this.battles = new MultiCounter[maxFightsPerBattle];
		for (int f = 0; f < maxFightsPerBattle; f++) {
			wins[f] = new MultiCounter(d);
			battles[f] = new MultiCounter(d);
		}
	}

	@Override
	protected long expectedTotalCount() {
		return super.expectedTotalCount() * defenders.length;
	}

	protected void countLosses(int def, int fight, Battle battle) {}

	protected void countWins(int def, int fight, Battle battle) {
		wins[fight].incrementAndGet(def);
	}

	protected void countFights(int def, int fight, Battle battle) {
		battles[fight].incrementAndGet(def);
	}

	protected void updateMaxFights(int def, int f) {
		fights.update(def, f);
	}

	protected BattleDesc getNextBattle(int def, int f, Battle previousBattle) {
		if (!previousBattle.defenderWon()) {
			return null;
		}
		return new BattleDesc(previousBattle.desc.attacker, previousBattle.defenderSnapshot());
	}

	@Override
	protected Runnable newRunnableInstance(final long iter, final AtomicInteger metricCounter, final AtomicInteger metricErrors) {
		return () -> {
			for (int i = 0; i < iter; i++) {
				for (int def = 0; def < defenders.length; def++) {
					try {
						BattleDesc desc = new BattleDesc(attackers[0], defenders[def]);
						int f;
						for (f = 0; f < maxFightsPerBattle; f++) {
							Battle battle = Battle.run(desc);
							desc = getNextBattle(def, f, battle);

							countFights(def, f, battle);

							if (desc == null) {
								countWins(def, f, battle);

								// Ajust counters: if won then all next fights are won
								for (int j = f + 1; j < maxFightsPerBattle; j++) {
									battles[j].incrementAndGet(def);
									wins[j].incrementAndGet(def);
								}
								f++;
								break;
							}
							else {
								countLosses(def, f, battle);
							}
						}
						updateMaxFights(def, f);
						metricCounter.getAndIncrement(); // only increment once per battle
					}
					catch (Throwable t) {
						metricErrors.getAndIncrement();
						String s = "An error occured in a battle between " + attackers[0].deckName + " and " + defenders[def].deckName;
						Warnings.send(s);
						L.error(s, t);
						//t.printStackTrace();
						throw t;
					}
				}
			}
		};
	}

	@Override
		Results buildSimResults() {
		int maxFights = (int) fights.getMax();

		Results results = super.buildSimResults();
		results.maxRunsPerIter = maxFights;

		ResultTable rt = new ResultTable(RESULT_TABLE);
		rt.headers.add("");
		if (isTillDeath) {
			rt.headers.add("~ hits");
		}
		if (maxFights == 1) {
			rt.headers.add("% win");
		}
		else {
			for (int i = 1; i <= maxFights; i++) {
				rt.headers.add(i + "-hit");
			}
		}
		for (int d = 0; d < defenders.length; d++) {
			rt.addLine(defenderData(maxFights, d));
		}
		if (defenders.length > 1) {
			rt.footer = Tools.newArrayList(globalData(GLOBAL_LINE, maxFights));
		}
		results.tables.put(rt.id, rt);

		return results;
	}

	private Object[] globalData(String title, int maxFights) {
		Object[] args = new Object[1 + maxFights + (isTillDeath ? 1 : 0)];
		int n = 0;
		args[n++] = title;
		if (isTillDeath) {
			args[n++] = capedAvgString(fights.getMean(), maxFightsPerBattle);
		}
		for (int f = 0; f < maxFights; f++) {
			args[n++] = winRatioGlobalString(f);
		}
		return args;
	}

	private Object[] defenderData(int maxFights, int def) {
		Object[] args = new Object[1 + maxFights + (isTillDeath ? 1 : 0)];
		int n = 0;
		args[n++] = PREFIX_DEFENDER + defenders[def].playerName;
		if (isTillDeath) {
			args[n++] = capedAvgString(fights.getMean(def), maxFightsPerBattle);
		}
		for (int f = 0; f < maxFights; f++) {
			args[n++] = winRatioForDefenderString(def, f);
		}
		return args;
	}

	String winRatioForDefenderString(int def, int fight) {
		return fights.getMax(def) > fight ? winRatioForDefender(def, fight) + "%" : "-";
	}

	float winRatioForDefender(int def, int fight) {
		return ratio(wins[fight].get(def), battles[fight].get(def));
	}

	String winRatioGlobalString(int fight) {
		return winRatioGlobal(fight) + "%";
	}

	float winRatioGlobal(int fight) {
		return ratio(wins[fight].get(), battles[fight].get());
	}

	static String capedAvgString(float mean, int cap) {
		if (mean == cap) {
			return cap + "+";
		}
		else {
			return f2s(mean, 2);
		}
	}
}
