package crystark.ek.battlesim.task;

import crystark.common.api.CardDesc;
import crystark.common.api.DeckDesc;
import crystark.ek.battlesim.battle.Battle;
import crystark.tools.Tools;

/**
 * @see SimVersusTest
 */
class SimVersus extends MultiDefenderSim {
	final MultiCounter[]	winByHero;
	final MultiCounter[]	winByCards;
	final MultiCounter[]	lossByHero;
	final MultiCounter[]	lossByCards;

	public SimVersus(final TaskOptions opts) {
		super(opts);

		int d = defenders.length;

		this.winByHero = new MultiCounter[maxFightsPerBattle];
		this.winByCards = new MultiCounter[maxFightsPerBattle];
		this.lossByHero = new MultiCounter[maxFightsPerBattle];
		this.lossByCards = new MultiCounter[maxFightsPerBattle];
		for (int f = 0; f < maxFightsPerBattle; f++) {
			winByHero[f] = new MultiCounter(d);
			winByCards[f] = new MultiCounter(d);
			lossByHero[f] = new MultiCounter(d);
			lossByCards[f] = new MultiCounter(d);
		}
	}

	@Override
	protected int maxFightsPerBattle() {
		return 1;
	}

	@Override
	protected int getDefaultIter(TaskOptions opts) {
		return Math.max(5000, Math.min(500000 / opts.defenderDecks.length, 50000));
	}

	@Override
	protected void countWins(int def, int fight, Battle battle) {
		super.countWins(def, fight, battle);
		if (battle.defenderHealth() > 0) {
			winByCards[fight].incrementAndGet(def);
		}
		else {
			winByHero[fight].incrementAndGet(def);
		}
	}

	@Override
	protected void countLosses(int def, int fight, crystark.ek.battlesim.battle.Battle battle) {
		super.countLosses(def, fight, battle);
		if (battle.attackerHealth() > 0) {
			lossByCards[fight].incrementAndGet(def);
		}
		else {
			lossByHero[fight].incrementAndGet(def);
		}
	}

	@Override
		Results buildSimResults() {
		Results sr = super.buildSimResults();
		sr.title = "Versus Simulation";

		ResultTable extended = new ResultTable("ext");
		extended.headers = Tools.newArrayList("Init", "Win/H", "Win/C", "Los/H", "Los/C", "lvl", "Tnd", "Mtn", "Fst", "Swp", "Oth");
		for (int d = 0; d < defenders.length; d++) {
			extended.addLine(defenderExtendedData(d));
		}
		if (defenders.length > 1) {
			extended.footer = Tools.newArrayList(globalExtendedData());
		}

		sr.tables.get(MultiDefenderSim.RESULT_TABLE).mergeColumns(extended);

		return sr;
	}

	private Object[] defenderExtendedData(int d) {
		int tundra = 0;
		int mtn = 0;
		int forest = 0;
		int swamp = 0;
		int other = 0;
		for (CardDesc card : defenders[d].cardsDescs) {
			switch (card.type) {
				case forest:
					forest++;
					break;
				case mtn:
					mtn++;
					break;
				case swamp:
					swamp++;
					break;
				case tundra:
					tundra++;
					break;
				default:
					other++;
					break;
			}
		}

		long wh = winByHero[0].get(d);
		long wc = winByCards[0].get(d);
		long w = wh + wc;
		long lh = lossByHero[0].get(d);
		long lc = lossByCards[0].get(d);
		long l = lh + lc;

		int init = Battle.compareInit(attackers[0], defenders[d]);

		return new Object[] { Tools.compareToValue(init, "+" + init, init, "="), ratioS(wh, w), ratioS(wc, w), ratioS(lh, l), ratioS(lc, l), defenders[d].playerLevel, tundra, mtn, forest, swamp, other };
	}

	private Object[] globalExtendedData() {
		int tundra = 0;
		int mtn = 0;
		int forest = 0;
		int swamp = 0;
		int other = 0;
		int init = 0;
		for (DeckDesc def : defenders) {
			for (CardDesc card : def.cardsDescs) {
				switch (card.type) {
					case forest:
						forest++;
						break;
					case mtn:
						mtn++;
						break;
					case swamp:
						swamp++;
						break;
					case tundra:
						tundra++;
						break;
					default:
						other++;
						break;
				}
			}
			if (Battle.compareInit(attackers[0], def) > 0) {
				init++;
			}
		}

		long wh = winByHero[0].get();
		long wc = winByCards[0].get();
		long w = wh + wc;
		long lh = lossByHero[0].get();
		long lc = lossByCards[0].get();
		long l = lh + lc;

		return new Object[] { ratioS(init, defenders.length), ratioS(wh, w), ratioS(wc, w), ratioS(lh, l), ratioS(lc, l), "-", tundra, mtn, forest, swamp, other };
	}
}
