package crystark.ek.battlesim.task;

import crystark.common.api.Server;

public class ExtractLeagueDecksTaskOptions extends SimTaskOptions<ExtractLeagueDecksTaskOptions> {

	static ExtractLeagueDecksTaskOptions create(Server server, String outputDir) {
		return new ExtractLeagueDecksTaskOptions()
			.server(server)
			.outputDir(outputDir);
	}

	private ExtractLeagueDecksTaskOptions server(Server server) {
		this.server = server;
		return this;
	}

	private ExtractLeagueDecksTaskOptions outputDir(String outputDir) {
		this.outputDir = outputDir;
		return this;
	}

	@Override
	protected Task buildTask() {
		return new ExtractLeagueDecks(this);
	}
}
