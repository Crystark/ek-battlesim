package crystark.ek.battlesim.task;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import crystark.common.api.BattleDesc;
import crystark.common.api.Server;
import crystark.common.db.Database;
import crystark.common.db.model.League;
import crystark.ek.DataFetcher;
import crystark.ek.DataFetcherFactory;
import crystark.ek.battlesim.battle.Battle;

/**
 * @see SimLeagueTest
 */
class SimLeague extends Sim {

	private static final String		RESULT_TABLE	= "SimLeague";

	private final List<BattleDesc>	battleDescs;
	private final String[][]		battleNames;
	private final int				battles;
	private AtomicInteger[]			attackerWinsPerBattle;
	private AtomicInteger[]			totalPerBattle;
	private final int				longestName;
	private final BattleDesc		tempBattle;
	private final Server			server;

	public SimLeague(TaskOptions opts) {
		super(opts);

		server = opts.server;
		DataFetcher df = DataFetcherFactory.get(server);
		try {
			p("Downloading " + server.name() + " FoH data...");
			Database db = Database.get();
			League league = new League(db, df.getLeagueData());
			this.battleDescs = league.nextBattles()
				.toList()
				.toBlocking()
				.first();

			this.battles = this.battleDescs.size();

			this.battleNames = new String[battles][2];
			this.attackerWinsPerBattle = new AtomicInteger[battles];
			this.totalPerBattle = new AtomicInteger[battles];

			int l = 0;
			for (int i = 0; i < battles; i++) {
				attackerWinsPerBattle[i] = new AtomicInteger();
				totalPerBattle[i] = new AtomicInteger();

				BattleDesc b = battleDescs.get(i);
				battleNames[i][0] = b.attacker.playerName;
				battleNames[i][1] = b.defender.playerName;
				if (battleNames[i][0].length() > l) {
					l = battleNames[i][0].length();
				}
				if (battleNames[i][1].length() > l) {
					l = battleNames[i][1].length();
				}
			}
			longestName = l;
			tempBattle = this.battleDescs.get(0);
		}
		catch (IOException e) {
			throw new TaskException("An error occured while trying to retrieve FoH data. Please make sure your internet connection is available before running this sim.", e);
		}
	}

	@Override
	protected long expectedTotalCount() {
		return super.expectedTotalCount() * battles;
	}

	@Override
	protected Runnable newRunnableInstance(final long iter, final AtomicInteger metricCounter, final AtomicInteger metricErrors) {
		return () -> {
			for (int i = 0; i < iter; i++) {
				for (int b = 0; b < battles; b++) {
					try {
						Battle battle = Battle.run(battleDescs.get(b));
						totalPerBattle[b].getAndIncrement();
						if (battle.attackerWon()) {
							attackerWinsPerBattle[b].getAndIncrement();
						}
						metricCounter.getAndIncrement();
					}
					catch (Throwable t) {
						metricErrors.getAndIncrement();
						t.printStackTrace();
						throw t;
					}
				}
			}
		};
	}

	@Override
		Results buildSimResults() {
		Results results = super.buildSimResults();
		results.title = "FoH Simulation (Server " + server.name() + ", season " + tempBattle.getMeta("season") + ", round " + tempBattle.getMeta("round") + ')';

		ResultTable rt = new ResultTable(RESULT_TABLE);

		String pattern = "%1$4s%% wins, %5$4s odds for %2$-" + longestName + "s vs %3$-" + longestName + "s (%4$4s%%, %6$4s)";
		for (int b = 0; b < battles; b++) {
			String attackerName = battleNames[b][0];
			String defenderName = battleNames[b][1];
			float winRatio = winRatio(b);
			String attRatio = f2s(winRatio);
			String defRatio = f2s(100 - winRatio);
			String attOdds = f2s((Float) battleDescs.get(b).getMeta("aOdds"), 2);
			String defOdds = f2s((Float) battleDescs.get(b).getMeta("dOdds"), 2);
			if (winRatio >= 50) {
				rt.addLine(String.format(pattern, attRatio, attackerName, defenderName, defRatio, attOdds, defOdds));
			}
			else {
				rt.addLine(String.format(pattern, defRatio, defenderName, attackerName, attRatio, defOdds, attOdds));
			}
		}
		results.tables.put(rt.id, rt);

		return results;
	}

	float winRatio(int battle) {
		return ratio(attackerWinsPerBattle[battle].get(), totalPerBattle[battle].get());
	}

}
