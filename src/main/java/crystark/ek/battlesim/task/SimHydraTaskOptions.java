package crystark.ek.battlesim.task;

import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import crystark.common.api.CardType;
import crystark.common.api.DeckDesc;
import crystark.common.format.DeckDescFormat;
import crystark.common.transform.DeckDescTransformer;
import crystark.ek.EkbsConfig;
import crystark.tools.Tools;

public class SimHydraTaskOptions extends SimTaskOptions<SimHydraTaskOptions> {
	
	private static final DeckDescTransformer	DEFAULTS	= DeckDescTransformer.create()
																.withInfiniteHpTransformer(t -> true)
																.withBypassValidationTransformer(t -> true);
	private static final DeckDescTransformer	KEEP_HYDRA	= DeckDescTransformer.create()
																.withCardsTransformer(dd -> Collections.singletonList(dd.cardsDescs.stream()
																	.filter(c -> c.type.equals(CardType.hydra))
																	.findFirst()
																	.get()));

	static SimHydraTaskOptions create(DeckDesc deck, List<String> hydras, boolean hydraOnly) {

		return new SimHydraTaskOptions()
			.attackerDecks(deck)
			.defenderDecks(getHydras(hydras, hydraOnly))
			.hydraOnly(hydraOnly);
	}

	@Override
	protected SimHydraTaskOptions defenderDecks(DeckDesc... decks) {
		return super.defenderDecks(Arrays.stream(decks)
			.map(DEFAULTS)
			.toArray(DeckDesc[]::new));
	}

	public SimHydraTaskOptions withIsolatedHydraDeck(DeckDesc deck) {
		if (deck != null) {
			attackerDecks(attackerDecks[0], deck);
		}
		return this;
	}

	public TaskOptions withDeckSwitchAtHp(Integer deckSwitchAtHp) {
		this.deckSwitchAtHp = deckSwitchAtHp;
		return this;
	}

	private SimHydraTaskOptions hydraOnly(boolean hydraOnly) {
		this.hydraOnly = hydraOnly;
		return this;
	}

	static DeckDesc[] getHydras(List<String> hydras, boolean hydraOnly) {
		List<DeckDesc> decks = getHydraDecks(hydras, hydraOnly);
		return decks.toArray(new DeckDesc[decks.size()]);
	}

	static List<DeckDesc> getHydraDecks(List<String> hydras, boolean hydraOnly) {
		String pool = (hydras.isEmpty() ? "h" : String.valueOf(hydras.get(0).charAt(0))).toLowerCase();
		String path = "decks/hydra/" + EkbsConfig.current().game() + "/";
		Set<String> remaining = new HashSet<>(hydras);
		Map<String, String> normalized = remaining.stream()
			.collect(Collectors.toMap(SimHydraTaskOptions::normalizeHydra, s -> s));

		final String[] files = Tools.getResourceListing(SimHydraTaskOptions.class, path);
		final List<String> hydraFiles = Arrays.asList(files);
		Collections.sort(hydraFiles);
		List<DeckDesc> decks = hydraFiles.stream()
			.filter(s -> s.startsWith(pool))
			.map(s -> s.substring(0, s.length() - 4)) // Remove .txt
			.filter(hydras.isEmpty() ? n -> true : n -> normalized.keySet().stream()
				.filter(n::startsWith)
				.peek(norm -> remaining.remove(normalized.get(norm)))
				.count() > 0)
			.map(hydraName -> DeckDescFormat.parseString(hydraName, Tools.readResource(path + hydraName + ".txt")))
			.map(hydraOnly ? KEEP_HYDRA::apply : Function.identity())
			.collect(Collectors.toList());

		if (!remaining.isEmpty()) {
			for (String fileName : remaining) {
				if (Paths.get(fileName.trim()).toFile().exists()) {
					decks.add(DeckDescFormat.parseFile(fileName));
					remaining.remove(fileName);
				}
			}
		}
		if (!remaining.isEmpty()) {
			throw new TaskException("Unrecognized hydra(s): " + remaining);
		}

		return decks;
	}

	static String normalizeHydra(String name) {
		return name
			.toLowerCase()
			.replace("(", "")
			.replace(")", "")
			.replace("hydra", "h")
			.replace("lilith", "l")
			.replaceFirst(" v( .+)?$", " 5$1")
			.replaceFirst(" iv( .+)?$", " 4$1")
			.replaceFirst(" iii( .+)?$", " 3$1")
			.replaceFirst(" ii( .+)?$", " 2$1")
			.replaceFirst(" i( .+)?$", " 1$1")
			.replaceFirst("extreme( .+)?$", "5$1")
			.replaceFirst("nightmare( .+)?$", "4$1")
			.replaceFirst("hard( .+)?$", "3$1")
			.replaceFirst("normal( .+)?$", "2$1")
			.replaceFirst("easy( .+)?$", "1$1")
			.replaceFirst("^h ", "h")
			.replaceFirst("^l ", "l")
			.trim()
			.replace(' ', '_');
	}

	@Override
	protected Task buildTask() {
		return new SimHydra(this);
	}
}
