package crystark.ek.battlesim.task;

import java.util.concurrent.ExecutorService;

public interface Task {

	public String getCurrentAction();

	public TaskOptions getOptions();

	public int getParallelism();

	public float getProgress();

	public boolean isInterrupted();

	public void run(ExecutorService exec);

	public void onStart();

	public void onComplete();
}
