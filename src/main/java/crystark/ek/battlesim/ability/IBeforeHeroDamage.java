package crystark.ek.battlesim.ability;

import crystark.ek.battlesim.battle.AbilitiesVisitor;
import crystark.ek.battlesim.battle.Card;

public interface IBeforeHeroDamage extends IAbility {
	/**
	 * Modify damage to hero
	 * @param dmg - current damage to hero
	 * @param sourceCard
	 * @return modified damage to hero
	 */
	int onBeforeHeroDamage(int dmg, Card sourceCard);

	default void accept(AbilitiesVisitor visitor){
		visitor.visit(this);
	}
}
