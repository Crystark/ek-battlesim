package crystark.ek.battlesim.ability;

import crystark.common.api.AbilityDesc;
import crystark.common.api.CardDesc;
import crystark.common.api.NamesConverter;
import crystark.common.db.Database;
import crystark.common.db.model.BaseSkill;
import crystark.common.events.Warnings;
import crystark.ek.Constants;
import crystark.ek.EkbsConfig;
import crystark.ek.battlesim.battle.Card;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class AbilityFactory {
	private static final Logger L = LoggerFactory.getLogger(AbilityFactory.class);

	//private static final ThreadLocal<AbilityFactory>				factory			= ThreadLocal.withInitial(AbilityFactory::new);


	private final Map<List<AbilityDesc>, List<Supplier<IAbility>>> cachePerCard = new ConcurrentHashMap<>(64);
	private final NamesConverter namesConverter;

	private static AbilityFactory current() {
		return EkbsConfig.current().abilityFactory();
	}

	public static Supplier<IAbility> getSupplier(AbilityDesc desc) {
		return () -> current().create(desc);
	}

	public static List<Supplier<IAbility>> getSuppliers(CardDesc desc) {
		return current().getAbilitySuppliers(desc);
	}

	public static Stream<BaseSkill> supportedSkills(boolean keep) {
		return Database.get().skills()
			.allByAbilityName()
			.map(entry -> entry.getValue().get(0))
			.filter(first -> {
				try {
					return (first.isGeneric() || (current().create(first.createAbilityDesc()) != null)) == keep;
				}
				catch (IllegalArgumentException e) {
					return !keep;
				}
			});
	}

	static boolean isBaseAbility(Class<? extends IAbility> clazz) {
		return !Modifier.isAbstract(clazz.getModifiers()) && clazz.getEnclosingClass() == null && !clazz.getPackage().getName().endsWith(".sub");
	}

	private static List<Supplier<IAbility>> makeAbilitySuppliers(List<AbilityDesc> descs) {

		ArrayList<Supplier<IAbility>> suppliers = new ArrayList<>(descs.size());
		for (int i = 0, descsSize = descs.size(); i < descsSize; i++) {
			AbilityDesc ad = descs.get(i);
			if (!ad.isUseless) {
				try {
					suppliers.add(getSupplier(ad));
				}
				catch (IllegalArgumentException e) {
					// TODO warn earlier (deck check ?) to add more info on what card / deck is affected
					String msg = e.getMessage() + " has been ignored.";
					Warnings.send(msg);
					if (Constants.IS_DEBUG)
						L.debug(msg);
				}
			}
		}
		return suppliers;
	}

	public AbilityFactory(NamesConverter namesConverter) {

		this.namesConverter = namesConverter;
	}

	protected IAbility create(AbilityDesc a) {
		try {
			final String mrName = namesConverter.skillFromAlias(a.abilityName);
			Function<AbilityDesc, IAbility> constructor = Abilities.MAPPING.get(mrName);
			if (constructor == null) {
				// UTests uses MR names
				constructor = Abilities.MAPPING.get(a.abilityName);
			}
			if (constructor == null) {
				//throw new IllegalArgumentException("Unimplemented ability '" + a + '\'');
				Warnings.send("Unimplemented ability '" + a + "' has been ignored.");
				constructor = SkillStub::new;
			}
			return constructor.apply(a);
		} catch (IllegalArgumentException e){
			throw new IllegalArgumentException ("Skill '" + a.name + "' creation error.", e);
		} catch (RuntimeException e){
			throw new RuntimeException("Skill '" + a.name + "' creation error.", e);
		}
	}

	private List<Supplier<IAbility>> getAbilitySuppliers(CardDesc desc) {
		return cachePerCard.computeIfAbsent(desc.abilities, AbilityFactory::makeAbilitySuppliers);
	}

	static class SkillStub implements IQuickstrikeAble{
		final String name;
		SkillStub(AbilityDesc b){
			name = b.name;
		}

		@Override public void apply(Card sourceCard) {
		}

		@Override public boolean isQuickstrike() {
			return true;
		}

		@Override public String getName() {
			return "Unimplemented ability '" + name + '\'' ;
		}

		@Override public boolean canBeLaunched(Card owner) {
			return false;
		}
	}
}
