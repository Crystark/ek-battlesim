package crystark.ek.battlesim.ability;

import crystark.common.api.AbilityDesc;
import crystark.common.api.CardType;
import crystark.common.format.AbilityDescFormat;
import crystark.ek.battlesim.battle.Card;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 */
public class AwakingAbility extends Ability<String, String> implements IBeforeTurn {
	public static final String NAME = "Awaking";

	private static Map<String, List<Supplier<IAbility>>> suppliersCache  = new ConcurrentHashMap<>();
	private static Map<Integer, Predicate<Card>>    predicatesCache = new ConcurrentHashMap<>();

	private final String                abilityRef;
	private final ArrayList<IAbility>        abilities;

	private final int count;
	private final Predicate<Card> cardTrigger;

	public AwakingAbility(AbilityDesc desc) {
		super(desc);

		this.abilityRef = getName() + '_' + id;

		final String[] split = getValue().split("_");
		count = Integer.valueOf(split[0]);
		final CardType race = CardType.valueOf(split[1].toLowerCase());
		cardTrigger = c -> c.desc.type == race;

		List<Supplier<IAbility>> suppliers = suppliersCache.computeIfAbsent(
			getValue2(),
			desc2 -> Arrays.stream(desc2.split("&"))
				.map(AbilityDescFormat::parse)
				.map(AbilityFactory::getSupplier)
				.collect(Collectors.toList()));

		this.abilities = new ArrayList<>();
		for (int i = 0, suppliersSize = suppliers.size(); i < suppliersSize; i++) {
			Supplier<IAbility> supplier = suppliers.get(i);
			final IAbility ability = supplier.get();
			if (ability != null) this.abilities.add(ability);
		}
	}

	@Override
	public void apply(Card sourceCard) {
		if (!sourceCard.hasMarker(abilityRef)) {
			if (sourceCard.owner().hasCardsInField(count, cardTrigger)) {
				sourceCard.addMarker(abilityRef);
				for (int i = 0; i < abilities.size(); i++) {
					IAbility iAbility = abilities.get(i);
					sourceCard.addAbility(iAbility);
				}
			}
		}
	}

	@Override public String getName() {
		return NAME;
	}
}
