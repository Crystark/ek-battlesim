package crystark.ek.battlesim.ability;

import crystark.common.api.AbilityDesc;
import crystark.common.api.CardDesc;
import crystark.common.db.Database;
import crystark.common.db.model.BaseCard;
import crystark.common.format.CardDescFormat;
import crystark.ek.Helper;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.SummonedCard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public abstract class SummonAbility extends Ability<String, Integer> implements IPower {
	public static final String				SELF	= "[self]";
	private static final Map<String, List<CardDesc>>	cache	= new ConcurrentHashMap<>(16);
	private final List<CardDesc>			summons;
	private final String					abilityRef;
	private final int count;

	public SummonAbility(AbilityDesc b) {
		super(b);
		final String cardId = getValue();
		if (SELF.equals(cardId)) {
			summons = new ArrayList<>(1);
			summons.add(null);
		}else if (cardId == null || cardId.isEmpty()){
			//Warnings.send("Wrong database data for skill '" + b.name +"'. Summon skill is not specified by cards to summon");
			// summons = new CardDesc[0];
			throw new IllegalArgumentException("Summon ability is not specified by cards to summon");
		} else {
			this.summons = cache.computeIfAbsent(
				cardId,
				desc -> Arrays.stream(desc.split("&"))
					.map(CardDescFormat::parse)
					.collect(Collectors.toList()));
		}

		this.abilityRef = getName() + '_' + id;

		if (getValue2() != null){
			count = getValue2();
		} else {
			count = -1;
		}
	}

	@Override
	public void apply(final Card sourceCard) {
		// Infinite loop is possible if summoned card summons same summoner in the chain
		// The game should not have such chains, but an user can make custom cards with cyclic chains.
		// So let's break any chains, including acyclic because it's dangerous.
		if (sourceCard.isSummoned() || summons.size() == 0){
			//throw new RuntimeException("Summoned card must not summons");
			return;
		}

		if (summons.get(0) == null) { // Summon [self] uninitialized
			//summons[0] = CardDescFormat.parse(sourceCard.desc.name);
			BaseCard card = Database.get().cards().get(sourceCard.desc.name);
			summons.set(0, card.createCardDesc());
		}

		final Player owner = sourceCard.owner();


		List<CardDesc> pack;
		if (count <= 0){
			//Summons if all in `summons` are absent
			pack = summons;
			if (owner.hasCardInField(CardPredicates.summonedBy(sourceCard, abilityRef))) return;
		} else {
			//Summons each round
			List<Card> summoned = owner.streamCardsField()
				.filter(CardPredicates.summonedBy(sourceCard, abilityRef))
				.collect(Collectors.toList());

			final List<CardDesc> cardDescs = new ArrayList<>(summons);
			cardDescs.removeIf(cd -> summoned.stream().anyMatch(c -> cd.name.equals(c.getName())));
			pack = Helper.pick(cardDescs, count);
		}

		for (int i = 0, summonsLength = pack.size(); i < summonsLength; i++) {
			CardDesc cardDesc = pack.get(i);
			new SummonedCard(cardDesc, sourceCard, abilityRef).addToField();
		}
	}

}
