package crystark.ek.battlesim.ability.addons;

import crystark.common.api.AbilityDesc;
import crystark.common.format.AbilityDescFormat;
import crystark.ek.battlesim.ability.AbilityFactory;
import crystark.ek.battlesim.ability.CompositeSkill;
import crystark.ek.battlesim.ability.IAbility;
import crystark.ek.battlesim.ability.SingleValueAbility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class Composite extends SingleValueAbility<String> implements CompositeSkill {
	public static final String NAME = "Composite";

	private static Map<String, List<Supplier<IAbility>>> suppliersCache = new ConcurrentHashMap<>(4);
	private ArrayList<IAbility> abilities;

	@Override
	public String getName() {
		return NAME;
	}

	public Composite(AbilityDesc b) {
		super(b);
		List<Supplier<IAbility>> suppliers = suppliersCache.computeIfAbsent(
			getValue(),
			desc -> Arrays.stream(desc.split("&"))
				.map(AbilityDescFormat::parse)
				.map(AbilityFactory::getSupplier)
				.collect(Collectors.toList()));

		this.abilities = new ArrayList<>(suppliers.size());
		for (int i = 0, suppliersSize = suppliers.size(); i < suppliersSize; i++) {
			Supplier<IAbility> supplier = suppliers.get(i);
			final IAbility ability = supplier.get();
			if (ability != null) this.abilities.add(ability);
		}
	}

	@Override
	public List<IAbility> getAbilities() {
		return abilities;
	}
}
