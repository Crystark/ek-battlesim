package crystark.ek.battlesim.ability.addons;

import crystark.common.api.AbilityDesc;
import crystark.common.format.AbilityDescFormat;
import crystark.ek.battlesim.ability.Ability;
import crystark.ek.battlesim.ability.AbilityCondition;
import crystark.ek.battlesim.ability.AbilityFactory;
import crystark.ek.battlesim.ability.CompositeSkill;
import crystark.ek.battlesim.ability.ConditionalAbilityCompiler;
import crystark.ek.battlesim.ability.IAbility;
import crystark.ek.battlesim.battle.Card;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class Judgement extends Ability<String, String> implements CompositeSkill {

	public static final String                                NAME           = "Judgement";
	private static      Map<String, List<Supplier<IAbility>>> suppliersCache = new ConcurrentHashMap<>(4);

	protected final List<IAbility> abilities;

	public Judgement(AbilityDesc b) {
		super(b);

		final String[] split = getValue2().split("&");
		List<Supplier<IAbility>> suppliers = suppliersCache.computeIfAbsent(
			getValue2(),
			desc -> Arrays.stream(split)
				.map(AbilityDescFormat::parse)
				.map(AbilityFactory::getSupplier)
				.collect(Collectors.toList()));

		this.abilities = new ArrayList<>(suppliers.size());
		for (int i = 0, suppliersSize = suppliers.size(); i < suppliersSize; i++) {
			Supplier<IAbility> supplier = suppliers.get(i);
			final IAbility ability = supplier.get();
			if (ability != null)
				this.abilities.add(ability);
		}

		if (abilities.size() < 1) {
			throw new RuntimeException("Judgement kind skill '" + b.fullName + "' has to has at least one included skill");
		}

		AbilityCondition condition = new AbilityCondition(getValue());
		Predicate<Card> predicate = condition.getPredicate();

		final IAbility abilityIfTrue = abilities.get(0);
		final AbilityDesc abilityDesc = AbilityDescFormat.parse(split[0]);
		final IAbility conditionalAbility = ConditionalAbilityCompiler.createConditionalAbility(abilityIfTrue.getClass(), abilityDesc, predicate);
		abilities.set(0, conditionalAbility);

		if (condition.isExclusive()) {
			final Predicate<Card> conditionNot = predicate.negate();
			for (int i = 1; i < abilities.size(); i++) {
				IAbility ability = abilities.get(i);
				AbilityDesc abilityDescElse = AbilityDescFormat.parse(split[i]);
				IAbility elseAbility = ConditionalAbilityCompiler.createConditionalAbility(ability.getClass(), abilityDescElse, conditionNot);
				abilities.set(i, elseAbility);
			}
		}
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public List<IAbility> getAbilities() {
		return abilities;
	}

}
