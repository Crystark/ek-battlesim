package crystark.ek.battlesim.ability.addons;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IBeforeDamage;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.effect.BonusAbility;
import crystark.ek.battlesim.status.StatusEffect;
import crystark.ek.battlesim.status.StatusEffectType;

public class AmPerseverance extends SingleValueAbility<Integer> implements IBeforeDamage {
	public static final String			NAME	= "AM Perseverance";
	private static final StatusEffect	STATUS	= StatusEffect.builder()
													.setEffect(new BonusAbility(Undamageable::new))
													.setType(StatusEffectType.buff)
													.build();

	public AmPerseverance(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public int onBeforeDamage(Card card, Card damagingCard, int damageTaken, DamageType type) {
		if (damageTaken >= card.getCurrentHp() && !card.hasMarker(NAME)) {
			damageTaken = (int) (card.getCurrentHp() - 1); // Reduce HP to 1
			STATUS.applyTo(card); // Can't be damage until next turn
			card.addMarker(NAME); // This power can't be used anymore
		}
		return damageTaken;
	}

	public static class Undamageable extends SingleValueAbility<Void> implements IBeforeDamage {
		public static final String NAME = "undamageable";

		public Undamageable() {
			super(simpleAbilityDesc(NAME, null));
		}

		@Override
		public String getName() {
			return NAME;
		}

		@Override
		public int onBeforeDamage(Card card, Card damagingCard, int damageTaken, DamageType type) {
			return 0;
		}
	}
}