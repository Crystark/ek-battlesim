package crystark.ek.battlesim.ability.addons;

import crystark.common.api.AbilityDesc;
import crystark.common.format.AbilityDescFormat;
import crystark.ek.battlesim.ability.AbilityFactory;
import crystark.ek.battlesim.ability.DefaultBuffAbility;
import crystark.ek.battlesim.ability.IAbility;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.effect.BonusAbility;

import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * This is intended to be used only by runes which give an ability to cards (Lore, Frost Bite, ...)
 */
public class AbilityProvider extends DefaultBuffAbility<String> implements IOnRuneTurn {
	public static final String	NAME	= "Ability Provider";
	private final BonusAbility	effect;

	public AbilityProvider(AbilityDesc b) {
		this(b, AbilityFactory.getSupplier(AbilityDescFormat.parse(b.value.toString())));
	}

	public AbilityProvider(Supplier<IAbility> ab) {
		this(simpleAbilityDesc(NAME, null), ab);
	}

	private AbilityProvider(AbilityDesc desc, Supplier<IAbility> ab) {
		super(desc);
		this.effect = new BonusAbility(ab);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public BonusAbility getEffect(Card sourceCard) {
		return effect;
	}

	@Override
	public Predicate<Card> getEligibleCardsFilter(Card sourceCard) {
		return t -> true;
	}
}
