package crystark.ek.battlesim.ability.addons;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.ManaCorruptionConsumer;

/**
 * Attack deals 100 Damage to <b>ALL</b> opponent cards, if target card has immunity or reflection to an attack, then an additional 300% damage is inflicted
 *
 * P.S. Multiplier 3 is constant
 */
public class SuperMagicErosion extends SingleValueAbility<Integer> implements IPower, IOnRuneTurn {
	public static final String NAME = "Super Magic erosion";

	@Override
	public String getName() {
		return NAME;
	}

	public SuperMagicErosion(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public SuperMagicErosion(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		superMagicErosion(sourceCard.owner().opponent(), sourceCard);
	}

	public void superMagicErosion(Player targetPlayer, Card sourceCard) {
		int count = targetPlayer.countCardsInField();
		if (count > 0)
			targetPlayer.consumeCardsField(new ManaCorruptionConsumer(getValue(), 3, sourceCard));
	}

	@Override
	public void onRuneTurn(Player player) {
		superMagicErosion(player.opponent(), null);
	}
}
