package crystark.ek.battlesim.ability.addons;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.SummonAbility;

public class Summon extends SummonAbility {
	public static final String NAME = "Summon";

	@Override
	public String getName() {
		return NAME;
	}

	public Summon(AbilityDesc b) {
		super(b);
	}
}
