package crystark.ek.battlesim.ability.addons;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;

public class PsychicMasterHand extends SingleValueAbility<Integer> implements IPower {
	public static final String NAME = "Psychic Master Hand";

	public PsychicMasterHand(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void apply(Card sourceCard) {
		Player owner = sourceCard.owner();
		Card card = owner.pickFirstHighestTimerCardInHand(true); // TODO check if random or not
		if (card != null) {
			card.addToField();
		}
	}
}
