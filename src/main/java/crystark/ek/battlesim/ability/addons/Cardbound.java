package crystark.ek.battlesim.ability.addons;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.CardboundAbility;

public class Cardbound extends CardboundAbility {
	public static final String NAME = "Cardbound";

	@Override
	public String getName() {
		return NAME;
	}

	public Cardbound(AbilityDesc b) {
		super(b);
	}
}
