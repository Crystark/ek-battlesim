package crystark.ek.battlesim.ability;

import java.util.List;

public interface CompositeSkill {
	List<IAbility> getAbilities();
}
