package crystark.ek.battlesim.ability;

import crystark.common.api.AbilityDesc;
import crystark.common.api.LaunchType;
import crystark.common.db.Database;
import crystark.common.db.model.BaseSkill;
import crystark.ek.EkbsConfig;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.task.TaskException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public abstract class Ability<V, V2> implements IAbility {
	protected static final Logger	L				= LoggerFactory.getLogger(Ability.class);
	private static final AtomicLong	IDS				= new AtomicLong();
	protected final long			id				= IDS.incrementAndGet();
	private final AbilityDesc				desc;

	protected static AbilityDesc baseAbilityDesc(String name, Object value) {
		return baseAbilityDesc(name, value, null);
	}

	protected static AbilityDesc baseAbilityDesc(String name, Object value, Object value2) {
		name = EkbsConfig.current().namesConverter().skillToAlias(name);
		final List<BaseSkill> skills = Database.get().skills().getByAbilityName(name);
		if (skills == null) throw new TaskException("Skill '" + name + "' not found in database");
		return skills.get(0).createAbilityDesc(value, value2);
	}

	protected static AbilityDesc simpleAbilityDesc(String name, Object value) {
		return new AbilityDesc(name, name, name, LaunchType.normal, value, null, false, 0);
	}

	public Ability(AbilityDesc desc) {
		this.desc = desc;
	}

	@SuppressWarnings("unchecked")
	public V getValue() {
		return (V) desc.value;
	}

	@SuppressWarnings("unchecked")
	public V2 getValue2() {
		return (V2) desc.value2;
	}

	@Override
	public boolean canBeLaunched(Card owner) {
		return !owner.isSilenced(); // Most of abilities are silenced, so use exclude approach
	}

	public boolean is(LaunchType t) {
		return t.equals(desc.launchType);
	}

	public boolean isPreemptiveStrike() {
		return is(LaunchType.preemptiveStrike);
	}

	public boolean isDeathWhisper() {
		return is(LaunchType.deathWhisper);
	}

	public boolean isQuickstrike() {
		return is(LaunchType.quickstrike);
	}

	public boolean isDesperation() {
		return is(LaunchType.desperation);
	}

	public boolean untyped() {
		return desc.launchType == null;
	}

	@Override
	public String toString() {
		return desc.toString();
	}
}
