package crystark.ek.battlesim.ability;

import crystark.ek.battlesim.battle.AbilitiesVisitor;
import crystark.ek.battlesim.battle.Card;

/**
 * TODO Merge with IAfterAttack if possible
 */
public interface IAfterHit extends IAbility {
	void onAfterHit(Card hittingCard, Card hitCard, int damageDone);

	@Override
	default void accept(AbilitiesVisitor visitor) {
		visitor.visit(this);
	}
}
