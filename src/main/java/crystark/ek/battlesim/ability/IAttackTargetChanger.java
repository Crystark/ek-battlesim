package crystark.ek.battlesim.ability;

import crystark.ek.battlesim.battle.AbilitiesVisitor;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;

public interface IAttackTargetChanger extends IAbility {
	/**
	 *
	 * @param card
	 * @param damagingCard
	 * @param damage
	 * @param type
	 * @return reduced damage value
	 */
	Card selectTarget(Card card, Card damagingCard, int damage, DamageType type);

	@Override
	default void accept(AbilitiesVisitor visitor) {
		visitor.visit(this);
	}
}
