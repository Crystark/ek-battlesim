package crystark.ek.battlesim.ability;

import crystark.ek.battlesim.battle.AbilitiesVisitor;
import crystark.ek.battlesim.battle.Card;

public interface IBeforeTurn extends IAbility {
	void apply(Card sourceCard);

	@Override
	default void accept(AbilitiesVisitor visitor) {
		visitor.visit(this);
	}
}
