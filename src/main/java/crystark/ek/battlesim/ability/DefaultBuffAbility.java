package crystark.ek.battlesim.ability;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.status.StatusEffectApplierHelper.ApplierTarget;
import crystark.ek.battlesim.status.StatusEffectType;

public abstract class DefaultBuffAbility<V> extends DefaultSEAAbility<V> implements IPower {

	public DefaultBuffAbility(AbilityDesc b) {
		super(b, StatusEffectType.buff, ApplierTarget.allies);
	}

	public void apply(Card sourceCard) {
		statusEffectApplierHelper.apply(sourceCard);
	}

}
