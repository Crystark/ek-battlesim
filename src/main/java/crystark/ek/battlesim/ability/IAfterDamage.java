package crystark.ek.battlesim.ability;

import crystark.ek.battlesim.battle.AbilitiesVisitor;
import crystark.ek.battlesim.battle.Card;

public interface IAfterDamage extends IAbility {
	void onAfterDamage(Card counterCard, Card counteredCard, int damageTaken);

	@Override
	default void accept(AbilitiesVisitor visitor) {
		visitor.visit(this);
	}
}
