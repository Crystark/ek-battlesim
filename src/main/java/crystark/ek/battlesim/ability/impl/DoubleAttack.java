package crystark.ek.battlesim.ability.impl;

import java.util.Collections;
import java.util.Iterator;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IAttack;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.CardAttack;

public class DoubleAttack extends SingleValueAbility<Integer> implements IAttack {
	public static final String NAME = "Double Attack";

	@Override
	public String getName() {
		return NAME;
	}

	public DoubleAttack(AbilityDesc b) {
		super(b);
	}

	@Override
	public Iterator<CardAttack> attacksIterator(Card defendingCard, int unreducedDamage, int damage) {
		if (defendingCard.isAlive()) {
			return Collections.singletonList(new CardAttack(defendingCard, unreducedDamage)).iterator();
		}
		return Collections.emptyIterator();
	}
}
