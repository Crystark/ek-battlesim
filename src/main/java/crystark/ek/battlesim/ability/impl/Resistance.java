package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.AbilitiesVisitor;
import crystark.ek.battlesim.battle.Card;

public class Resistance extends SingleValueAbility<Integer> {
	public static final String NAME = "Resistance";

	@Override
	public String getName() {
		return NAME;
	}

	public Resistance() {
		this(baseAbilityDesc(NAME, null));
	}

	public Resistance(AbilityDesc b) {
		super(b);
	}

	@Override
	public boolean canBeLaunched(Card owner) {
		return true;
	}


	@Override
	public void accept(AbilitiesVisitor visitor) {
		visitor.visit(this);
	}
}
