package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.HealConsumer;

public class DianasTouch extends SingleValueAbility<Integer> implements IPower {
	public static final String	NAME	= "Diana's Touch";
	private final float			multiplier;

	@Override
	public String getName() {
		return NAME;
	}

	public DianasTouch(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public DianasTouch(AbilityDesc b) {
		super(b);
		multiplier = getValue() / 100f;
	}

	@Override
	public void apply(Card sourceCard) {
		this.dianasTouch(sourceCard.owner());
	}

	public void dianasTouch(Player player) {
		Card card = player.pickMostDamagedCard();
		if (card != null) {
			HealConsumer.heal(card, (int) (card.getCurrentBaseHp() * multiplier));
		}
	}
}
