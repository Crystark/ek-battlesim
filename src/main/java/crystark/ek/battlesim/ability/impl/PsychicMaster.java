package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;

public class PsychicMaster extends SingleValueAbility<Integer> implements IPower {
	public static final String NAME = "Psychic Master";

	public PsychicMaster(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void apply(Card sourceCard) {
		Player owner = sourceCard.owner();
		Card card = owner.pickLastHighestTimerCardInDeck(true); // TODO check if random or not (reincarnation put cards back on top)
		if (card != null) {
			card.addToField();
		}
	}
}
