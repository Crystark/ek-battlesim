package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.HealConsumer;

public class Healing extends SingleValueAbility<Integer> implements IPower, IOnRuneTurn {
	public static final String NAME = "Healing";

	@Override
	public String getName() {
		return NAME;
	}

	public Healing(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Healing(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		this.healing(sourceCard.owner());
	}

	public void healing(Player targetPlayer) {
		HealConsumer.heal(targetPlayer.pickMostDamagedCard(), getValue());
	}

	@Override
	public void onRuneTurn(Player player) {
		this.healing(player);
	}
}
