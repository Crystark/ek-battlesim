package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.Helper;
import crystark.ek.battlesim.ability.IBeforeDamage;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;

public class Sensitive extends SingleValueAbility<Integer> implements IBeforeDamage {
	public static final String NAME = "Sensitive";

	public Sensitive(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Sensitive(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public int onBeforeDamage(Card card, Card damagingCard, int damageTaken, DamageType type) {
		if (DamageType.focused.equals(type)) {
			if (Helper.getLucky(getValue())) {
				return 0;
			}
		}
		return damageTaken;
	}
}
