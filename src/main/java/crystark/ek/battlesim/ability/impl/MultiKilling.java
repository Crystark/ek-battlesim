package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IAfterAttack;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;

public class MultiKilling extends SingleValueAbility<Integer> implements IAfterAttack {
	public static final String NAME = "Multi-Killing [physical]";

	public MultiKilling(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void onAfterAttack(Card hittingCard, Card hitCard, int damageDone) {
		if (!hitCard.isAlive() && damageDone > 0 && hittingCard.isAlive()) {
			final Card newTarget = hitCard.owner().pickCardInField();
			if (newTarget != null) hittingCard.owner().physicalAttack(hittingCard, newTarget, hittingCard.getCurrentAtk());
		}
	}
}
