package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.DefaultAuraAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.effect.BonusBaseAtk;

import java.util.Objects;
import java.util.function.Predicate;

public class SwampForce extends DefaultAuraAbility {
	public static final String	NAME	= "Swamp Force";
	private final BonusBaseAtk	effect;

	public SwampForce(AbilityDesc b) {
		super(b);
		this.effect = new BonusBaseAtk(getValue());
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public BonusBaseAtk getEffect(Card sourceCard) {
		return effect;
	}

	@Override
	public Predicate<Card> getEligibleCardsFilter(Card sourceCard) {
		return CardPredicates.IS_SWAMP.and((c) -> !Objects.equals(c, sourceCard));
	}
}
