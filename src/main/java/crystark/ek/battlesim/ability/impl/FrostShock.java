package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.Ability;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.FreezeConsumer;

/**
 * Frost Shock >=15 are different skills
 * @see LightingCut
 */
public class FrostShock extends Ability<Integer, Integer> implements IPower {
	public static final String NAME = "Frost Shock";

	@Override
	public String getName() {
		return NAME;
	}

	public FrostShock(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		this.frostShock(sourceCard.owner().opponent(), sourceCard);
	}

	public void frostShock(Player targetPlayer, Card sourceCard) {
		targetPlayer.consumeCardsField(new FreezeConsumer(getValue() + getValue2() * targetPlayer.countCardsInField(), 50, sourceCard));
	}
}