package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.BlindConsumer;

public class Blind extends SingleValueAbility<Integer> implements IPower {
	public static final String NAME = "Blind";

	@Override
	public String getName() {
		return NAME;
	}

	public Blind(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		this.blind(sourceCard.owner().opponent());
	}

	public void blind(Player targetPlayer) {
		BlindConsumer.blind(targetPlayer.pickCardInField(), getValue());
	}
}
