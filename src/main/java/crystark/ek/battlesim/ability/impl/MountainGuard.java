package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.DefaultAuraAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.effect.BonusHp;

import java.util.Objects;
import java.util.function.Predicate;

public class MountainGuard extends DefaultAuraAbility {
	public static final String	NAME	= "Mountain Guard";
	private final BonusHp		effect;

	public MountainGuard(AbilityDesc b) {
		super(b);
		this.effect = new BonusHp(getValue());
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public BonusHp getEffect(Card sourceCard) {
		return effect;
	}

	@Override
	public Predicate<Card> getEligibleCardsFilter(Card sourceCard) {
		return CardPredicates.IS_MOUNTAIN.and((c) -> !Objects.equals(c, sourceCard));
	}
}
