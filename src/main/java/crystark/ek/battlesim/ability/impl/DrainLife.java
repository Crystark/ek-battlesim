package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IAfterDamage;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;

/**
 * When receiving physical attack, reduce the attacker's base HP by 12% and restore same HP to this card.
 * This skill doesn't work against Demon but ignore Immunity.
 */
public class DrainLife extends SingleValueAbility<Integer> implements IAfterDamage {
	public static final String	NAME	= "Drain Life";
	private final float			multiplier;

	public DrainLife(AbilityDesc b) {
		super(b);
		multiplier = getValue() / 100f;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void onAfterDamage(Card counterCard, Card counteredCard, int damageTaken) {
		if (!counteredCard.isBoss()) {
			int health = (int) (counteredCard.getCurrentBaseHp() * multiplier);
			counteredCard.removeBaseHp(health);
			counterCard.heal(health);
		}
	}
}
