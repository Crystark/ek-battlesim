package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.AbilitiesVisitor;

public class Infiltrator extends SingleValueAbility<Integer> {
	public static final String NAME = "Infiltrator";

	@Override
	public String getName() {
		return NAME;
	}

	public Infiltrator() {
		this(baseAbilityDesc(NAME, null));
	}

	public Infiltrator(AbilityDesc b) {
		super(b);
	}

	@Override
	public void accept(AbilitiesVisitor visitor) {
		visitor.visit(this);
	}
}
