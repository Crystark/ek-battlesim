package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IAfterHit;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;

public class Devour extends SingleValueAbility<Integer> implements IAfterHit {
	public static final String	NAME	= "Devour";
	private final float			multiplier;

	public Devour(AbilityDesc b) {
		super(b);
		this.multiplier = getValue() / 100f;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void onAfterHit(Card hittingCard, Card hitCard, int damageDone) {
		if (!hitCard.isAlive()) {
			hittingCard.addBaseAtk((int) (hittingCard.getCurrentBaseAtk() * multiplier));
			hittingCard.addBaseHp((int) (hittingCard.getCurrentBaseHp() * multiplier));
		}
	}
}
