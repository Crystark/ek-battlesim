package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.FreezeConsumer;

public class Blizzard extends SingleValueAbility<Integer> implements IPower, IOnRuneTurn {
	public static final String NAME = "Blizzard";

	@Override
	public String getName() {
		return NAME;
	}

	public Blizzard(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Blizzard(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		this.blizzard(sourceCard.owner().opponent(), sourceCard);
	}

	public void blizzard(Player targetPlayer, Card sourceCard) {
		targetPlayer.consumeCardsField(new FreezeConsumer(getValue(), 30, sourceCard));
	}

	@Override
	public void onRuneTurn(Player player) {
		this.blizzard(player.opponent(), null);
	}
}