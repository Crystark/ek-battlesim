package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.AbilitiesVisitor;

public class SoulImprison extends SingleValueAbility<Void> {
	public static final String NAME = "Soul Imprison";

	@Override
	public String getName() {
		return NAME;
	}

	public SoulImprison() {
		this(baseAbilityDesc(NAME, null));
	}

	public SoulImprison(AbilityDesc b) {
		super(b);
	}

	@Override
	public void accept(AbilitiesVisitor visitor) {
		visitor.visit(this);
	}
}
