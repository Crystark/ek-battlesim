package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IAfterAttack;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;

public class BreakIce extends SingleValueAbility<Integer> implements IAfterAttack {
	public static final String NAME = "Break Ice";

	public BreakIce(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void onAfterAttack(Card hittingCard, Card hitCard, int damageDone) {
		if (damageDone > 0 && !hitCard.isBoss() && hitCard.isAlive() && (hitCard.isFrozen() || hitCard.hasIceShieldIgnoreSilence())) {
			hitCard.owner().destroyCard(hitCard);
		}
	}
}
