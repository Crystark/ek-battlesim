package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.Ability;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.BurnConsumer;
import crystark.ek.battlesim.battle.consumer.CombustionConsumer;
import crystark.ek.battlesim.status.StatusEffect;
import crystark.tools.CustomPredicates;
import crystark.tools.PredicateConsumer;

/**
 * Deal random 1 enemy card 500-1000 damage, the enemy will lose 500 HP every time after its action.
 */
public class DemonFire extends Ability<Integer, String> implements IPower {
	public static final String NAME = "Demon Fire";
	private final StatusEffect statusEffect;
	private final int          minDamage;
	private final int          maxDamage;

	@Override
	public String getName() {
		return NAME;
	}

	public DemonFire(AbilityDesc b) {
		super(b);
		//"AffectValue2": "500_1000_500",
		final String[] split = getValue2().split("_");
		minDamage = Integer.valueOf(split[0]);
		maxDamage = Integer.valueOf(split[1]);
		statusEffect = CombustionConsumer.makeEffect(Integer.valueOf(split[2]));
	}

	@Override
	public void apply(Card sourceCard) {
		this.demonFire(sourceCard.owner().opponent(), sourceCard);
	}

	private void demonFire(Player targetPlayer, Card sourceCard) {
		int count = targetPlayer.countCardsInField();
		if (count > 0)
			targetPlayer.consumeCardsField(
				new PredicateConsumer<>(
					new BurnConsumer(minDamage, maxDamage, sourceCard).andThen(new CombustionConsumer(statusEffect)),
					CustomPredicates.randomNAmongX(getValue(), count)
				)
			);
	}

}