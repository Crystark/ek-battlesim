package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.Ability;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.ManaCorruptionConsumer;
import crystark.tools.CustomPredicates;

public class ManaBreak extends Ability<Integer, Integer> implements IPower {
	public static final String	NAME	= "Mana Break";

	private int					value2;

	@Override
	public String getName() {
		return NAME;
	}

	public ManaBreak(AbilityDesc b) {
		super(b);
		value2 = super.getValue2() / 100;
	}

	@Override
	public Integer getValue2() {
		return value2;
	}

	@Override
	public void apply(Card sourceCard) {
		Player targetPlayer = sourceCard.owner().opponent();
		int count = targetPlayer.countCardsInField();
		if (count > 0)
			targetPlayer.consumeCardsField(new ManaCorruptionConsumer(getValue(), getValue2(), sourceCard).withPredicate(CustomPredicates.randomNAmongX(3, count)));
	}
}
