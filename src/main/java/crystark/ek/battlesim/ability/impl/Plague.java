package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;

import java.util.function.Consumer;

public class Plague extends SingleValueAbility<Integer> implements IPower, IOnRuneTurn {
	public static final String		NAME	= "Plague";
	private final BlightConsumer	consumer;

	public Plague(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Plague(AbilityDesc b) {
		super(b);
		this.consumer = new BlightConsumer(getValue());
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void apply(Card sourceCard) {
		this.plague(sourceCard.owner().opponent());
	}

	public void plague(Player targetPlayer) {
		targetPlayer.consumeCardsField(this.consumer);
	}

	@Override
	public void onRuneTurn(Player player) {
		this.plague(player.opponent());
	}

	static class BlightConsumer implements Consumer<Card> {
		private final Integer value;

		BlightConsumer(int value) {
			this.value = value;
		}

		@Override
		public void accept(Card card) {
			if (!card.hasImmunity()) {
				card.removeBaseAtk(value);
				card.damaged(null, value, DamageType.unavoidable);
			}
		}
	}
}
