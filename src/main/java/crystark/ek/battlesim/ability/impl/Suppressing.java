package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.AbilitiesVisitor;

public class Suppressing extends SingleValueAbility<Void> {
	public static final String NAME = "Suppressing";

	@Override
	public String getName() {
		return NAME;
	}

	public Suppressing() {
		this(baseAbilityDesc(NAME, null));
	}

	public Suppressing(AbilityDesc b) {
		super(b);
	}

	@Override
	public void accept(AbilitiesVisitor visitor) {
		visitor.visit(this);
	}
}
