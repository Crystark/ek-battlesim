package crystark.ek.battlesim.ability.impl;

import java.util.Iterator;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IAttack;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.CardAttack;
import crystark.ek.battlesim.battle.CardInFieldIterator;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.CreateAttackIterator;

public class CleanSweep extends SingleValueAbility<Void> implements IAttack {
	public static final String NAME = "Clean Sweep";

	@Override
	public String getName() {
		return NAME;
	}

	public CleanSweep() {
		this(baseAbilityDesc(NAME, null));
	}

	public CleanSweep(AbilityDesc b) {
		super(b);
	}

	@Override
	public Iterator<CardAttack> attacksIterator(Card defendingCard, int unreducedDamage, int damage) {
		CardInFieldIterator fieldIterator = defendingCard.owner().getFieldIterator(CardPredicates.nextTo(defendingCard));

		return new CreateAttackIterator(fieldIterator, damage);
	}
}
