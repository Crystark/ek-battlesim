package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.CombustionConsumer;
import crystark.ek.battlesim.status.StatusEffect;

/**
 * Burns all the cards on the opponent's battlefield. The enemy burnt will lose 200 HP every time after its action.
 */
public class FireGod extends SingleValueAbility<Integer> implements IPower, IOnRuneTurn {
	public static final String	NAME	= "Fire God";
	private final StatusEffect	statusEffect;

	public FireGod(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public FireGod(AbilityDesc b) {
		super(b);
		statusEffect = CombustionConsumer.makeEffect(getValue());
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void apply(Card sourceCard) {
		this.fireGod(sourceCard.owner().opponent());
	}

	public void fireGod(Player targetPlayer) {
		targetPlayer.consumeCardsField(new CombustionConsumer(statusEffect));
	}

	@Override
	public void onRuneTurn(Player player) {
		this.fireGod(player.opponent());
	}
}
