package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IAfterDamage;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;

public class Craze extends SingleValueAbility<Integer> implements IAfterDamage {
	public static final String NAME = "Craze";

	public Craze(Integer value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Craze(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void onAfterDamage(Card counterCard, Card counteredCard, int damageTaken) {
		counterCard.crazeAddAtk(this.getValue());
	}
}
