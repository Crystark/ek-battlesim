package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.DefaultBeforeAttackAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.effect.BonusAtk;

public class Warpath extends DefaultBeforeAttackAbility {
	public static final String	NAME	= "Warpath";
	private final float			multiplier;

	public Warpath(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Warpath(AbilityDesc b) {
		super(b);
		this.multiplier = getValue() / 100f;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public boolean conditionsAreMet(Player sourcePlayer, Card sourceCard) {
		Card card = sourcePlayer.oppositeCardOnField(sourceCard);
		return (card != null && card.getCurrentHp() > sourceCard.getCurrentHp());
	}

	@Override
	public BonusAtk getEffect(Card sourceCard) {
		return new BonusAtk((int) (multiplier * sourceCard.getCurrentBaseAtk()));
	}
}
