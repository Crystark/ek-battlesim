package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IAfterHit;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;

public class Bloodthirsty extends SingleValueAbility<Integer> implements IAfterHit {
	public static final String NAME = "Bloodthirsty";

	public Bloodthirsty(Integer value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Bloodthirsty(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void onAfterHit(Card hittingCard, Card hitCard, int damageDone) {
		hittingCard.addBaseAtk(getValue());
	}
}
