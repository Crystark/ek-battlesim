package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.DefaultBuffAbility;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.effect.BonusBaseAtk;

import java.util.function.Predicate;

/**
 * Increases the Attack of ALL cards on your battlefield by X
 */
public class GroupMorale extends DefaultBuffAbility<Integer> implements IOnRuneTurn {
	public static final String	NAME	= "Group Morale";
	private final BonusBaseAtk	effect;

	public GroupMorale(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public GroupMorale(AbilityDesc b) {
		super(b);
		this.effect = new BonusBaseAtk(getValue());
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public BonusBaseAtk getEffect(Card sourceCard) {
		return effect;
	}

	@Override
	public Predicate<Card> getEligibleCardsFilter(Card sourceCard) {
		return t -> true;
	}
}
