package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;

public class Exile extends SingleValueAbility<Integer> implements IPower {
	public static final String NAME = "Exile";

	@Override
	public String getName() {
		return NAME;
	}

	public Exile(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		Card card = sourceCard.owner().oppositeCardOnField(sourceCard);
		exile(card);
	}

	public static void exile(Card card) {
		if (card != null && !card.resists()) {
			card.removeFromField();
			card.addRandomlyToDeck();
		}
	}
}
