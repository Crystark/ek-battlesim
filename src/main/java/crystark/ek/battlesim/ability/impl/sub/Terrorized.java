package crystark.ek.battlesim.ability.impl.sub;

import crystark.ek.battlesim.ability.DefaultDeboostAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.effect.MalusAtk;

public class Terrorized extends DefaultDeboostAbility {
	public static final String NAME = "Terrorized";

	public Terrorized() {
		super(simpleAbilityDesc(NAME, null));
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public boolean canBeLaunched(Card owner) {
		return true;
	}

	@Override
	public boolean conditionsAreMet(Player sourcePlayer, Card sourceCard) {
		return sourceCard.getCurrentBaseAtk() > sourceCard.getCurrentMalusAtk();
	}

	@Override
	public MalusAtk getEffect(Card sourceCard) {
		int malusBase = sourceCard.getCurrentBaseAtk() - sourceCard.getCurrentMalusAtk();
		return new MalusAtk(malusBase == 1 ? 1 : malusBase / 2);
	}
}
