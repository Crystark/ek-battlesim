package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IAfterHit;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;

public class ColdBlood extends SingleValueAbility<Integer> implements IAfterHit {
	public static final String NAME = "Cold Blood";

	public ColdBlood(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void onAfterHit(Card hittingCard, Card hitCard, int damageDone) {
		if (hitCard.isAlive())
			hitCard.coldBloodRemoveHp(damageDone);
	}
}
