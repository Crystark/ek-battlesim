package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;

public class Damnation extends SingleValueAbility<Integer> implements IPower, IOnRuneTurn {
	public static final String NAME = "Damnation";

	@Override
	public String getName() {
		return NAME;
	}

	public Damnation(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Damnation(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		this.damnation(sourceCard.owner().opponent());
	}

	public void damnation(Player targetPlayer) {
		targetPlayer.damaged(targetPlayer.countCardsInField() * this.getValue(), true);
	}

	@Override
	public void onRuneTurn(Player player) {
		this.damnation(player.opponent());
	}
}
