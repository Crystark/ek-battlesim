package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.DefaultBuffAbility;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.effect.BonusAbility;
import crystark.ek.battlesim.effect.Effect;

import java.util.function.Predicate;

/**
 * Gives Evasion to all cards on your battlefield.(Grants immunity to Trap, Seal, and Confusion. Also cannot be frozen or stunned)
 */
public class GroupEvasion extends DefaultBuffAbility<Integer> implements IOnRuneTurn {
	public static final String NAME = "Group Evasion";

	public GroupEvasion(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public Effect<? super Card> getEffect(Card sourceCard) {
		return new BonusAbility(Evasion::new);
	}

	@Override
	public Predicate<Card> getEligibleCardsFilter(Card sourceCard) {
		return t -> true;
	}
}
