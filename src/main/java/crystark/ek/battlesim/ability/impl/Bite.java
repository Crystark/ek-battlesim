package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.consumer.BiteConsumer;

public class Bite extends SingleValueAbility<Integer> implements IPower {
	public static final String NAME = "Bite";

	@Override
	public String getName() {
		return NAME;
	}

	public Bite(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		Card bitten = sourceCard.owner().opponent().pickCardInField();
		BiteConsumer.bite(bitten, sourceCard, getValue());
	}
}
