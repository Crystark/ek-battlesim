package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IAfterDamage;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;

public class Counterattack extends SingleValueAbility<Integer> implements IAfterDamage {
	public static final String NAME = "Counterattack";

	public Counterattack(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Counterattack(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void onAfterDamage(Card counterCard, Card counteredCard, int damageTaken) {
		counteredCard.prepareAction().damaged(counterCard, this.getValue(), DamageType.counter);
	}
}
