package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IQuickstrikeAble;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.tools.PredicateConsumer;

import java.util.Objects;
import java.util.function.Consumer;

public class TimeReverse extends SingleValueAbility<Integer> implements IQuickstrikeAble {
	public static final String	NAME		= "Time Reverse";
	static final Consumer<Card>	handExile	= card -> {
												if (!card.isBoss()) {
													card.removeFromHand();
													card.addRandomlyToDeck();
												}
											};
	static final Consumer<Card>	forceExile	= card -> {
												if (!card.isBoss()) {
													card.removeFromField();
													card.addRandomlyToDeck();
												}
											};

	@Override
	public String getName() {
		return NAME;
	}

	public TimeReverse(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		Player player = sourceCard.owner();
		if (player.tryAddMarker(sourceCard.desc.uniqueName + '#' + NAME)) {
			Player opponent = player.opponent();
			Consumer<Card> forceExileExceptSelf = new PredicateConsumer<>(forceExile, (c) -> !Objects.equals(c, sourceCard));
			opponent.consumeCardsField(forceExile);
			player.consumeCardsField(forceExileExceptSelf);
			opponent.consumeCardsHand(handExile);
			player.consumeCardsHand(handExile);
		}
	}
}
