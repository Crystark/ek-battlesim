package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.ManaCorruptionConsumer;

/**
 * Deals 100 damage to a random enemy unit. If the target has Immunity or Magic Reflections, deals 300% damage.
 */
public class ManaCorruption extends SingleValueAbility<Integer> implements IPower, IOnRuneTurn {
	public static final String NAME = "Mana Corruption";

	@Override
	public String getName() {
		return NAME;
	}

	public ManaCorruption(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public ManaCorruption(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		manaCorruption(sourceCard.owner().opponent().pickCardInField(), sourceCard);
	}

	public void manaCorruption(Card card, Card sourceCard) {
		ManaCorruptionConsumer.manaCorrupt(card, sourceCard, getValue(), 3);
	}

	@Override
	public void onRuneTurn(Player player) {
		manaCorruption(player.opponent().pickCardInField(), null);
	}
}
