package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IAfterDamage;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;

public class Spine extends SingleValueAbility<Integer> implements IAfterDamage {
	public static final String	NAME	= "Spine";
	private final float			multiplier;

	public Spine(AbilityDesc b) {
		super(b);
		multiplier = getValue() / 100f;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void onAfterDamage(Card counterCard, Card counteredCard, int damageTaken) {
		counteredCard.prepareAction().damaged(counterCard, (int) (multiplier * damageTaken), DamageType.counter);
	}
}
