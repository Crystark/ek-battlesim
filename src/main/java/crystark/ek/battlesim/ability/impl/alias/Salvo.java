package crystark.ek.battlesim.ability.impl.alias;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.impl.Multshot;

public class Salvo extends Multshot {
	public static final String NAME = "Salvo";

	@Override
	public String getName() {
		return NAME;
	}

	public Salvo(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Salvo(AbilityDesc b) {
		super(b);
	}

	@Override
	public Integer getValue2() {
		return 99;
	}
}
