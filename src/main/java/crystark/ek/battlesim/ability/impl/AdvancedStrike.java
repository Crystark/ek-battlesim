package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;

public class AdvancedStrike extends SingleValueAbility<Integer> implements IPower {
	public static final String NAME = "Advanced Strike";

	public AdvancedStrike(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void apply(Card sourceCard) {
		Card card = sourceCard.owner().pickFirstHighestTimerCardInHand(false);
		if (card != null) {
			card.decreaseTimer(getValue());
		}
	}
}
