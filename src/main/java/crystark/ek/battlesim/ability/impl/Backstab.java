package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.DefaultBeforeAttackAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.effect.BonusAtk;

public class Backstab extends DefaultBeforeAttackAbility {
	public static final String NAME = "Backstab";

	public Backstab(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public boolean conditionsAreMet(Player sourcePlayer, Card sourceCard) {
		return !sourceCard.hasMarker(NAME);
	}

	@Override
	public void onBeforeAttack(Card sourceCard) {
		super.onBeforeAttack(sourceCard);
		sourceCard.addMarker(NAME);
	}

	@Override
	public BonusAtk getEffect(Card sourceCard) {
		return new BonusAtk(getValue());
	}
}
