package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;

public class Charging extends SingleValueAbility<Integer> implements IPower {
	public static final String NAME = "Charging";

	@Override
	public String getName() {
		return NAME;
	}

	public Charging(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		Player player = sourceCard.owner();
		if (player.tryAddMarker(sourceCard.desc.uniqueName + '#' + id)) {
			player.chargeRunes();
		}
	}
}
