package crystark.ek.battlesim.ability.impl.alias;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.impl.Snipe;

public class BloodyJudgement extends Snipe {

	public static final String NAME = "Bloody Judgement";

	public BloodyJudgement(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}
}
