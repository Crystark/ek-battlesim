package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.BurnConsumer;

/**
 * Deals 250-500 damage to ALL of the opponent's cards.
 */
public class Firestorm extends SingleValueAbility<Integer> implements IPower, IOnRuneTurn {
	public static final String NAME = "Firestorm";

	@Override
	public String getName() {
		return NAME;
	}

	public Firestorm(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Firestorm(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		this.firestorm(sourceCard.owner().opponent(), sourceCard);
	}

	public void firestorm(Player targetPlayer, Card sourceCard) {
		targetPlayer.consumeCardsField(new BurnConsumer(getValue(), 2 * getValue(), sourceCard));
	}

	@Override
	public void onRuneTurn(Player player) {
		this.firestorm(player.opponent(), null);
	}
}