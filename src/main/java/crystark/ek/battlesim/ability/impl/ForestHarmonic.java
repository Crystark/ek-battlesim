package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.common.api.CardType;
import crystark.ek.battlesim.ability.Harmonic;

/**
 * Each one Forest card entering the battlefield after this card will add 6% Attack and 6% HP to this card.
 * The summoned cards will not activate this skill.
 */
public class ForestHarmonic extends Harmonic {
	public static final String	NAME	= "Forest Harmonic";

	public ForestHarmonic(AbilityDesc b) {
		super(b, CardType.forest);
	}

	@Override
	public String getName() {
		return NAME;
	}

}
