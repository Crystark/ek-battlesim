package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;

import java.util.List;

/**
 * Transfer N cards from deck to hand. (the skill will not be enabled if no room in hand.)
 * TODO Does room in hand has to be >=N or transfer cards for free spots?
 */
public class Wish extends SingleValueAbility<Integer> implements IPower {
	public static final String NAME = "Wish";

	public Wish(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void apply(Card sourceCard) {
		Player owner = sourceCard.owner();
		int n = Math.min(getValue(), Player.MAX_HAND_SIZE - owner.countCardsInHand());
		List<Card> cards = owner.pickRandomCardsInDeck(n, true);
		for (int i = 0, s = cards.size(); i < s; i++) {
			Card card = cards.get(i);
			card.addToHand();
		}
	}
}
