package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.Ability;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.ConfusionConsumer;
import crystark.tools.CustomPredicates;

public class FieldOfChaos extends Ability<Integer, Integer> implements IPower {
	public static final String		NAME	= "Field of Chaos";
	private final ConfusionConsumer	consumer;

	@Override
	public String getName() {
		return NAME;
	}

	public FieldOfChaos(AbilityDesc b) {
		super(b);
		this.consumer = new ConfusionConsumer(getValue());
	}

	public FieldOfChaos(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	@Override
	public void apply(Card sourceCard) {
		this.fieldOfChaos(sourceCard.owner().opponent());
	}

	void fieldOfChaos(Player opponent) {
		int count = opponent.countCardsInField();
		if (count > 0)
			opponent.consumeCardsField(this.consumer.withPredicate(CustomPredicates.randomNAmongX(getValue2(), count)));
	}
}