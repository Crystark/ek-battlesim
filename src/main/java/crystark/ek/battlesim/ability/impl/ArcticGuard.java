package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.DefaultAuraAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.effect.BonusHp;

import java.util.Objects;
import java.util.function.Predicate;

public class ArcticGuard extends DefaultAuraAbility {
	public static final String NAME = "Arctic Guard";

	public ArcticGuard(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public BonusHp getEffect(Card sourceCard) {
		return new BonusHp(getValue());
	}

	@Override
	public Predicate<Card> getEligibleCardsFilter(Card sourceCard) {
		return CardPredicates.IS_TUNDRA.and((c) -> !Objects.equals(c, sourceCard));
	}
}
