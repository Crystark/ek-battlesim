package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.Ability;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.FreezeConsumer;

/**
 * This card deals N + xK (number of cards on enemy's battlefield) magical damage to the card directly across from it as well as both cards adjactent to that card.
 */
public class ToxicStorm extends Ability<Integer, Integer> implements IPower {
	public static final String NAME = "Toxic Storm";

	public ToxicStorm(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void apply(Card sourceCard) {
		this.toxicStorm(sourceCard.owner().opponent(), sourceCard);
	}

	public void toxicStorm(Player opponent, Card sourceCard) {

		int dmg = getValue() + getValue2() * opponent.countCardsInField();
		int idx = sourceCard.owner().getIndexOnField(sourceCard);

		opponent.consumeCardsField(new FreezeConsumer(dmg, 0, sourceCard).withPredicate(CardPredicates.atIndexes(idx, idx -1 , idx + 1)));
	}
}