package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.common.api.LaunchType;
import crystark.ek.battlesim.ability.IDesperationAble;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.CardAction;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.effect.BonusAbility;
import crystark.ek.battlesim.status.StatusEffect;
import crystark.ek.battlesim.status.StatusEffectType;

/**
 * Place a death mark lasting for 1 turn to the card directly across. If the marked card dies during the turn, deals X damage to its adjacent cards."
 */
public class DeathMarker extends SingleValueAbility<Integer> implements IPower {
	public static final String	NAME	= "Death Marker";

	private final StatusEffect	statusEffect;

	@Override
	public String getName() {
		return NAME;
	}

	public DeathMarker(AbilityDesc b) {
		super(b);
		statusEffect = StatusEffect.builder()
			.setEffect(new BonusAbility(() -> new DeathMark(getValue())))
			.setType(StatusEffectType.debuff)
			.build();
	}

	public DeathMarker(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	@Override
	public void apply(Card sourceCard) {
		this.deathMarker(sourceCard.owner().oppositeCardOnField(sourceCard));
	}

	public void deathMarker(Card targetCard) {
		if (targetCard != null && !targetCard.hasImmunity()) {
			statusEffect.applyTo(targetCard);
		}
	}

	private static class DeathMark extends SingleValueAbility<Integer> implements IDesperationAble {
		public static final String NAME = "death mark";

		public DeathMark(int value) {
			super(new AbilityDesc(NAME, NAME, NAME, LaunchType.desperation, value, null, false, 0));
		}

		@Override
		public String getName() {
			return NAME;
		}

		@Override
		public boolean canBeLaunched(Card owner) {
			return true;
		}

		@Override
		public void apply(Card sourceCard) {
			Player player = sourceCard.owner();
			int idx = player.getIndexOnField(sourceCard);
			Card leftCard = player.atIndexOnField(idx - 1);
			Card rightCard = player.atIndexOnField(idx + 1);

			CardAction caLeft = (leftCard == null || leftCard.hasImmunity()) ? null : leftCard.prepareAction();
			CardAction caRight = (rightCard == null || rightCard.hasImmunity()) ? null : rightCard.prepareAction();

			if (caLeft != null) {
				caLeft.damaged(sourceCard, getValue(), DamageType.unavoidable);
			}
			if (caRight != null) {
				caRight.damaged(sourceCard, getValue(), DamageType.unavoidable);
			}
		}
	}
}
