package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.BurnConsumer;

public class Fireball extends SingleValueAbility<Integer> implements IPower, IOnRuneTurn {
	public static final String NAME = "Fireball";

	@Override
	public String getName() {
		return NAME;
	}

	public Fireball(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Fireball(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		this.fireball(sourceCard.owner().opponent(), sourceCard);
	}

	public void fireball(Player targetPlayer, Card sourceCard) {
		BurnConsumer.burn(targetPlayer.pickCardInField(), sourceCard, getValue(), getValue());
	}

	@Override
	public void onRuneTurn(Player player) {
		this.fireball(player.opponent(), null);
	}
}
