package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.consumer.SilenceConsumer;

public class Silence extends SingleValueAbility<Integer> implements IPower {
	public static final String NAME = "Silence";

	@Override
	public String getName() {
		return NAME;
	}

	public Silence(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		SilenceConsumer.silence(sourceCard.owner().oppositeCardOnField(sourceCard));
	}
}