package crystark.ek.battlesim.ability.impl.alias;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.impl.DemonFire;

/**
 *
 */
public class MultiDemonFire extends DemonFire {
	public static final String NAME = "Multi-Demon Fire";
	public MultiDemonFire(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}
}
