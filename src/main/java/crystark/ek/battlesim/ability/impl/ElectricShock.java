package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.ShockConsumer;

public class ElectricShock extends SingleValueAbility<Integer> implements IPower, IOnRuneTurn {
	public static final String NAME = "Electric Shock";

	@Override
	public String getName() {
		return NAME;
	}

	public ElectricShock(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public ElectricShock(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		this.electricShock(sourceCard.owner().opponent(), sourceCard);
	}

	public void electricShock(Player targetPlayer, Card sourceCard) {
		targetPlayer.consumeCardsField(new ShockConsumer(getValue(), 35, sourceCard));
	}

	@Override
	public void onRuneTurn(Player player) {
		this.electricShock(player.opponent(), null);
	}
}