package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.Constants;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;

public class Teleportation extends SingleValueAbility<Integer> implements IPower {
	public static final String NAME = "Teleportation";

	@Override
	public String getName() {
		return NAME;
	}

	public Teleportation(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		Player opponent = sourceCard.owner().opponent();
		Card card = opponent.pickFirstHighestTimerCardInHand(false);
		if (card != null) {
			if (!card.resists()) {
				card.removeFromHand();
				card.addToCemetery();
			}
			else if (Constants.IS_DEBUG)
				L.debug(card + " resists teleportation! ");
		}
	}
}
