package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.Constants;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;

import java.util.function.Consumer;

public class Banish extends SingleValueAbility<Void> implements IPower {
	public static final String	NAME	= "Banish";

	static final Consumer<Card>	banish	= card -> {
											if (card.isSummoned() && !card.isBoss()) {
												card.removeFromField();
												if (Constants.IS_DEBUG)
													L.debug(card + " has been unsummoned.");
											}
										};

	@Override
	public String getName() {
		return NAME;
	}

	public Banish(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		sourceCard.owner().opponent().consumeCardsField(banish);
	}
}
