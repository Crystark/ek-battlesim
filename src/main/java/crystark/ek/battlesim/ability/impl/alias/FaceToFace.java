package crystark.ek.battlesim.ability.impl.alias;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.impl.SpiritualVoice;

public class FaceToFace extends SpiritualVoice{
	public static final String NAME = "Face-to-face";

	public FaceToFace(AbilityDesc abilityDesc) {
		super(abilityDesc);
	}
}
