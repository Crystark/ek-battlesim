package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.common.api.CardType;
import crystark.ek.battlesim.ability.IBeforeDamage;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;

public class GlacialBarrier extends SingleValueAbility<Integer> implements IBeforeDamage {
	public static final String	NAME	= "Glacial Barrier";
	private final float			multiplier;

	public GlacialBarrier(AbilityDesc b) {
		super(b);
		this.multiplier = (1 - (getValue() / 100f));
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public int onBeforeDamage(Card card, Card damagingCard, int damageTaken, DamageType type) {
		if (damagingCard != null && CardType.mtn.equals(damagingCard.getType()) && DamageType.attack.equals(type)) {
			return (int) (damageTaken * multiplier);
		}
		return damageTaken;
	}
}
