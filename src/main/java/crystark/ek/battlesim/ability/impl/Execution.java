package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IAfterHit;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;

public class Execution extends SingleValueAbility<Integer> implements IAfterHit {
	public static final String	NAME	= "Execution";

	public final float			threshold;

	public Execution(AbilityDesc b) {
		super(b);
		threshold = getValue() / 100f;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void onAfterHit(Card hittingCard, Card hitCard, int damageDone) {
		if (damageDone > 0 && !hitCard.isBoss() && ((float) (hitCard.getCurrentHp() + damageDone) / hitCard.getCurrentBaseHp()) < threshold) {
			hitCard.owner().destroyCard(hitCard);
		}
	}
}
