package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IBeforeDamage;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;

public class SpellReduction extends SingleValueAbility<Integer> implements IBeforeDamage {
	public static final String	NAME	= "Spell Reduction";
	private final float			multiplier;

	public SpellReduction(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public SpellReduction(AbilityDesc b) {
		super(b);
		this.multiplier = getValue() / 100f;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public int onBeforeDamage(Card card, Card damagingCard, int damageTaken, DamageType type) {
		switch (type) {
			case fire:
			case ice:
			case lightning:
			case blood:
				return (int) (damageTaken * multiplier);
			default:
				return damageTaken;
		}
	}
}
