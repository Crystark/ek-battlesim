package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.Ability;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.consumer.ManaCorruptionConsumer;

public class ManaPuncture extends Ability<Integer, Integer> implements IPower {
	public static final String	NAME	= "Mana Puncture";

	private int					value2;

	@Override
	public String getName() {
		return NAME;
	}

	public ManaPuncture(AbilityDesc b) {
		super(b);
		value2 = super.getValue2() / 100;
	}

	@Override
	public Integer getValue2() {
		return value2;
	}

	@Override
	public void apply(Card sourceCard) {
		manaPuncture(sourceCard.owner().opponent().pickCardInField(), sourceCard);
	}

	public void manaPuncture(Card card, Card sourceCard) {
		ManaCorruptionConsumer.manaCorrupt(card, sourceCard, getValue(), getValue2());
	}
}
