package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.Ability;
import crystark.ek.battlesim.ability.IQuickstrikeAble;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
/**
 * Upon entering the battlefield, this card's Attack and HP is increased by 100%, while a random card from enemy side of the battlefield is sent to the cemetery.
 */

public class Erosion extends Ability<Integer, Integer> implements IQuickstrikeAble {
	public static final String	NAME	= "Erosion";
	private final float			multiplier;

	@Override
	public String getName() {
		return NAME;
	}

	public Erosion(AbilityDesc b) {
		super(b);
		this.multiplier = getValue2() / 100f;
	}

	@Override
	public void apply(Card sourceCard) {
		Player opponent = sourceCard.owner().opponent();
		Card cardToSacrifice = opponent.pickCardInField();

		if (cardToSacrifice != null && !cardToSacrifice.resists())  {
			opponent.destroyCard(cardToSacrifice);
			sourceCard.addBaseAtk((int) (sourceCard.getCurrentBaseAtk() * multiplier));
			sourceCard.addBaseHp((int) (sourceCard.getCurrentBaseHp() * multiplier));
		}
	}
}
