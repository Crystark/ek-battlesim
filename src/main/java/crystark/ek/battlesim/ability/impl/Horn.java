package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;

import java.util.List;

/**
 * Summon N cards with the longest waiting time directly into battlefield.
 * TODO Does the skill summons from hand or from deck?
 */
public class Horn extends SingleValueAbility<Integer> implements IPower {
	public static final String NAME = "Horn";

	public Horn(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void apply(Card sourceCard) {
		Player owner = sourceCard.owner();
		List<Card> cards = owner.pickHighestTimerCardsInDeck(getValue2(),true); // TODO check if random or not (reincarnation put cards back on top)
		for (int i = 0; i < cards.size(); i++) {
			Card card = cards.get(i);
			card.addToField();
		}
	}
}
