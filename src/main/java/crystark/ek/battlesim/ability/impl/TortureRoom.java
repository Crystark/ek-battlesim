package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.LacerationConsumer;

public class TortureRoom extends SingleValueAbility<Integer> implements IPower {
	public static final String			NAME	= "Torture Room";
	private final LacerationConsumer	consumer;

	public TortureRoom(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public TortureRoom(AbilityDesc b) {
		super(b);
		this.consumer = LacerationConsumer.INSTANCE;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void apply(Card sourceCard) {
		this.tortureRoom(sourceCard.owner().opponent());
	}

	public void tortureRoom(Player targetPlayer) {
		targetPlayer.consumeCardsField(consumer);
	}
}
