package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.Ability;
import crystark.ek.battlesim.ability.IBeforeDamage;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.effect.Effect;
import crystark.ek.battlesim.status.StatusEffect;
import crystark.ek.battlesim.status.StatusEffectType;

/**
 * Block physical attack once every round.
 * @see DivineShield Is it the same?
 */
public class PhysicalShield extends Ability implements IBeforeDamage {
	public static final String	NAME	= "Physical Shield";
	private static final StatusEffect	STATUS	= StatusEffect.builder()
		.setEffect(new MarkerRemover())
		.setType(StatusEffectType.buff)
		.build();

	public PhysicalShield(){
		super(baseAbilityDesc(NAME, null));
	}

	public PhysicalShield(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public int onBeforeDamage(Card card, Card damagingCard, int damageTaken, DamageType type) {
		if (type == DamageType.attack && !card.hasMarker(MarkerRemover.NAME)) {
			damageTaken = 0;
			card.addMarker(MarkerRemover.NAME); // This skill can't be used anymore on this round
			STATUS.applyTo(card);// Restart on next round start
		}
		return damageTaken;
	}

	static class MarkerRemover extends Effect<Card> {
		public static final String NAME = "Reset PhysicalShield";

		@Override
		public void applyTo(Card target) {

		}

		@Override
		public void removeFrom(Card target) {
			target.removeMarker(NAME);
		}
	}
}