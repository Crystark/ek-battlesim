package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.Ability;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;

public class Mania extends Ability<Integer, Integer> implements IPower {
	public static final String NAME = "Mania";

	@Override
	public String getName() {
		return NAME;
	}

	public Mania(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		final Integer value2 = getValue2();
		if (value2 != null) sourceCard.addBaseAtk(value2); //Self-destruction has value2 == 0 that converts to null in BaseSkill class
		sourceCard.damaged(sourceCard, getValue(), DamageType.unavoidable);
	}
}
