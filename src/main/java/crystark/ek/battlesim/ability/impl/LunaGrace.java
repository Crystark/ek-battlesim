package crystark.ek.battlesim.ability.impl;

import java.util.function.Supplier;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IAbility;
import crystark.ek.battlesim.ability.IBeforeRound;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.effect.BonusAbility;
import crystark.ek.battlesim.status.StatusEffect;
import crystark.ek.battlesim.status.StatusEffectType;

/**
 * At the beginning of (each) round, make itself and the card in front of it with extra effect [Holy Shield].
 * <p>P.S. "In front of" means a card on battle field before the skill owner card on the same battle field side
 */
public class LunaGrace  extends SingleValueAbility<Integer> implements IBeforeRound {
    public static final String NAME = "Luna Grace";
    //private final Supplier<IAbility> supplier;
    private final StatusEffect statusEffect;

    public LunaGrace(AbilityDesc desc) {
        super(desc);

        // AbilityFactory.getSupplier(baseAbilityDesc(PhysicalShield.NAME, null));
        Supplier<IAbility> supplier = ()-> new PhysicalShield();
        BonusAbility bonusAbility = new BonusAbility(supplier);
        statusEffect = StatusEffect.builder()
            .setEffect(bonusAbility)
            .setType(StatusEffectType.buff)
            .build();
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void onBeforeRound(Card sourceCard) {

        //IAbility ability = supplier.get();
        //sourceCard.addAbility(ability);
        statusEffect.applyTo(sourceCard);

        final int idx = sourceCard.owner().getIndexOnField(sourceCard);
        if (idx <= 0) return;
        Card cardBefore = sourceCard.owner().atIndexOnField(idx - 1);
        if (cardBefore == null || !cardBefore.isAlive()) return;

        statusEffect.applyTo(cardBefore);
    }
}
