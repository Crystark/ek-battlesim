package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.BurnConsumer;
import crystark.tools.CustomPredicates;

/**
 * Deals 250-500 fire damage to 3 of the opponent's cards.
 */
public class FireWall extends SingleValueAbility<Integer> implements IPower, IOnRuneTurn {
	public static final String NAME = "Fire Wall";

	@Override
	public String getName() {
		return NAME;
	}

	public FireWall(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public FireWall(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		this.fireWall(sourceCard.owner().opponent(), sourceCard);
	}

	public void fireWall(Player targetPlayer, Card sourceCard) {
		int count = targetPlayer.countCardsInField();
		if (count > 0)
			targetPlayer.consumeCardsField(
				new BurnConsumer(getValue(), 2 * getValue(), sourceCard)
					.withPredicate(CustomPredicates.randomNAmongX(3, count))
			);
	}

	@Override
	public void onRuneTurn(Player player) {
		this.fireWall(player.opponent(), null);
	}
}