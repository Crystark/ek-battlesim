package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IAfterDamage;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.consumer.CombustionConsumer;
import crystark.ek.battlesim.status.StatusEffect;

/**
 * Burns any opponent which attacks and inflicts damage on this card. The enemy burnt will lose 250 HP every time after its action.
 */
public class Combustion extends SingleValueAbility<Integer> implements IAfterDamage {
	public static final String	NAME	= "Combustion";
	private final StatusEffect	statusEffect;

	public Combustion(AbilityDesc b) {
		super(b);
		statusEffect = CombustionConsumer.makeEffect(getValue());
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void onAfterDamage(Card counterCard, Card counteredCard, int damageTaken) {
		CombustionConsumer.combust(counteredCard, statusEffect);
	}
}
