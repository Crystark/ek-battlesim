package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IDesperationAble;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;

public class DyingStrike extends SingleValueAbility<Integer> implements IDesperationAble {
	public static final String NAME = "Dying Strike";
	@Override
	public String getName() {
		return NAME;
	}

	public DyingStrike(AbilityDesc b) {
		super(b);
	}

	public DyingStrike(int value){
		this(baseAbilityDesc(NAME, value));
	}

	@Override
	public void apply(Card sourceCard) {
		Card facingCard = sourceCard.owner().oppositeCardOnField(sourceCard);
		if (facingCard != null) {
			facingCard.damaged(sourceCard, sourceCard.desc.attack * getValue() / 100, DamageType.unavoidable);
		}
	}
}
