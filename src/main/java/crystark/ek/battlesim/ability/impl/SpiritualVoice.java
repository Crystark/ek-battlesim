package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.AbilitiesVisitor;
import crystark.ek.battlesim.battle.Card;

public class SpiritualVoice extends SingleValueAbility<Void> {
	public static final String NAME = "Spiritual Voice";

	@Override
	public String getName() {
		return NAME;
	}

	public SpiritualVoice() {
		this(baseAbilityDesc(NAME, null));
	}

	public SpiritualVoice(AbilityDesc b) {
		super(b);
	}

	@Override
	public boolean canBeLaunched(Card owner) {
		return true;
	}

	@Override
	public void accept(AbilitiesVisitor visitor) {
		visitor.visit(this);
	}
}
