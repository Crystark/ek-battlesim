package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.Ability;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.ManaCorruptionConsumer;

/**
 * Attack deals 100 Damage to <b>ALL</b> opponent cards, if target card has immunity or reflection to an attack, then an additional 300% damage is inflicted
 *
 * P.S. Multiplier is {@link #getValue2()}/100
 */
public class Cataclysm extends Ability<Integer, Integer> implements IPower {
	public static final String	NAME	= "Cataclysm";

	private int					value2;

	@Override
	public String getName() {
		return NAME;
	}

	public Cataclysm(AbilityDesc b) {
		super(b);
		value2 = super.getValue2() / 100;
	}

	@Override
	public Integer getValue2() {
		return value2;
	}

	@Override
	public void apply(Card sourceCard) {
		Player targetPlayer = sourceCard.owner().opponent();
		targetPlayer.consumeCardsField(new ManaCorruptionConsumer(getValue(), getValue2(), sourceCard));
	}
}
