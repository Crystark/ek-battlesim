package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.Constants;
import crystark.ek.battlesim.ability.IBeforeHeroDamage;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;

public class Guard extends SingleValueAbility<Integer> implements IBeforeHeroDamage {
	public static final String NAME = "Guard";

	@Override
	public String getName() {
		return NAME;
	}

	public Guard(AbilityDesc b) {
		super(b);
	}

	@Override
	public boolean canBeLaunched(Card owner) {
		return true;
	}

	@Override
	public int onBeforeHeroDamage(int dmg, Card sourceCard) {
		int cardDmg = (int) Math.min(dmg, sourceCard.getCurrentHp());
		if (Constants.IS_DEBUG)
			L.debug(sourceCard + " intercepts " + cardDmg + " damage");
		sourceCard.damaged(null, cardDmg, DamageType.unavoidable);
		return dmg - cardDmg;
	}
}
