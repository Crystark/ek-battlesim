package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IAfterDamage;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;

public class WickedLeech extends SingleValueAbility<Integer> implements IAfterDamage {
	public static final String	NAME	= "Wicked Leech";
	private float				multiplier;

	public WickedLeech(AbilityDesc b) {
		super(b);
		this.multiplier = getValue() / 100f;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void onAfterDamage(Card counterCard, Card counteredCard, int damageTaken) {
		int atk = (int) (counteredCard.getCurrentBaseAtk() * multiplier);
		counteredCard.wickedLeechRemoveAtk(atk);
		counterCard.addBaseAtk(atk);
	}
}
