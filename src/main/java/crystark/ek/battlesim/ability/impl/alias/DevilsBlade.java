package crystark.ek.battlesim.ability.impl.alias;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.impl.Snipe;

public class DevilsBlade extends Snipe {

	public static final String NAME = "Devil's Blade";

	public DevilsBlade(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}
}
