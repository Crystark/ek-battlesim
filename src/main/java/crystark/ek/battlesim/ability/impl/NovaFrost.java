package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.FreezeConsumer;
import crystark.tools.CustomPredicates;

public class NovaFrost extends SingleValueAbility<Integer> implements IPower, IOnRuneTurn {
	public static final String NAME = "Nova Frost";

	@Override
	public String getName() {
		return NAME;
	}

	public NovaFrost(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public NovaFrost(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		this.novaFrost(sourceCard.owner().opponent(), sourceCard);
	}

	public void novaFrost(Player targetPlayer, Card sourceCard) {
		int count = targetPlayer.countCardsInField();
		if (count > 0)
			targetPlayer.consumeCardsField(new FreezeConsumer(getValue(), 35, sourceCard).withPredicate(CustomPredicates.randomNAmongX(3, count)));
	}

	@Override
	public void onRuneTurn(Player player) {
		this.novaFrost(player.opponent(), null);
	}
}