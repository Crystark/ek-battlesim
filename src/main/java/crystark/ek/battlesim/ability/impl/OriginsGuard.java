package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.DefaultAuraAbility;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.effect.BonusHp;

import java.util.Objects;
import java.util.function.Predicate;

public class OriginsGuard extends DefaultAuraAbility implements IOnRuneTurn {
	public static final String	NAME	= "Origins Guard";
	private final BonusHp		effect;

	public OriginsGuard(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public OriginsGuard(AbilityDesc b) {
		super(b);
		this.effect = new BonusHp(getValue());
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public BonusHp getEffect(Card sourceCard) {
		return effect;
	}

	@Override
	public Predicate<Card> getEligibleCardsFilter(Card sourceCard) {
		return (c) -> !Objects.equals(c, sourceCard);
	}
}
