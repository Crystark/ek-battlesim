package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.AbilityFactory;
import crystark.ek.battlesim.ability.DefaultAuraAbility;
import crystark.ek.battlesim.ability.IAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.effect.BonusAbility;

import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * The card and its adjacent cards physical attack randomly increase 700-2000.
 */
public class PhysicalBuff extends DefaultAuraAbility {
	public static final String	NAME	= "Physical buff";

	public PhysicalBuff(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public BonusAbility getEffect(Card sourceCard) {
		final Supplier<IAbility> abilitySupplier = AbilityFactory.getSupplier(baseAbilityDesc(ViolentStorm.NAME, getValue(), getValue2()));
		BonusAbility ba = new BonusAbility(abilitySupplier);
		return ba;
	}

	@Override
	public Predicate<Card> getEligibleCardsFilter(Card sourceCard) {
		return CardPredicates.nextTo(sourceCard).or(Predicate.isEqual(sourceCard));
	}

	/*@Override
	public Predicate<Card> getEligibleCardsFilter(Card sourceCard) {
		return Predicate.<Card> isEqual(sourceCard).or(CardPredicates.nextTo(sourceCard));
	}*/

}
