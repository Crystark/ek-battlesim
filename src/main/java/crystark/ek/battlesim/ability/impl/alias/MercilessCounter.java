package crystark.ek.battlesim.ability.impl.alias;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.impl.Retaliation;

public class MercilessCounter extends Retaliation {
	public static final String NAME = "Merciless Counter";

	public MercilessCounter(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public MercilessCounter(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}
}
