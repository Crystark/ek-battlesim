package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.Constants;
import crystark.ek.battlesim.ability.IBeforeHeroDamage;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;

/**
 * On the battlefield, it offers a buff to reduce Hero's damage 40%.
 * TODO Does Hero Shield apply after Guard or in order by field?
 */
public class HeroShield extends SingleValueAbility<Integer> implements IBeforeHeroDamage {

	public static final String NAME = "Hero Shield";

	public HeroShield(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public boolean canBeLaunched(Card owner) {
		return true;
	}

	@Override
	public int onBeforeHeroDamage(int dmg, Card sourceCard) {
		int reduced = dmg * getValue() / 100;
		if (Constants.IS_DEBUG)
			L.debug(sourceCard + " reduce " + reduced + " damage");
		return dmg - reduced;
	}
}
