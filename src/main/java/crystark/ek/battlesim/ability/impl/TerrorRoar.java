package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.RoarConsumer;
import crystark.ek.battlesim.status.StatusEffect;

/**
 * Every turn halves a random card's Attack on opponent's battlefield for 1 round. This skill doesn't work against Demons.
 */
public class TerrorRoar extends SingleValueAbility<Void> implements IPower {
	public static final String	NAME	= "Terror Roar";
	private final StatusEffect	statusEffect;

	@Override
	public String getName() {
		return NAME;
	}

	public TerrorRoar(AbilityDesc b) {
		super(b);
		statusEffect = RoarConsumer.makeEffect();
	}

	public TerrorRoar() {
		this(baseAbilityDesc(NAME, null));
	}

	@Override
	public void apply(Card sourceCard) {
		this.terrorRoar(sourceCard.owner().opponent());
	}

	public void terrorRoar(Player targetPlayer) {
		RoarConsumer.roar(targetPlayer.pickCardInField(), statusEffect);
	}
}
