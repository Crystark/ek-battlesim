package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.ShockConsumer;

public class Thunderbolt extends SingleValueAbility<Integer> implements IPower, IOnRuneTurn {
	public static final String NAME = "Thunderbolt";

	@Override
	public String getName() {
		return NAME;
	}

	public Thunderbolt(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Thunderbolt(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		this.thunderbolt(sourceCard.owner().opponent(), sourceCard);
	}

	public void thunderbolt(Player targetPlayer, Card sourceCard) {
		ShockConsumer.shock(targetPlayer.pickCardInField(), sourceCard, getValue(), 50);
	}

	@Override
	public void onRuneTurn(Player player) {
		this.thunderbolt(player.opponent(), null);
	}
}
