package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.Helper;
import crystark.ek.battlesim.ability.IBeforeDamage;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;

public class Dexterity extends SingleValueAbility<Integer> implements IBeforeDamage {
	public static final String NAME = "Dexterity";

	@Override
	public String getName() {
		return NAME;
	}

	public Dexterity(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Dexterity(AbilityDesc b) {
		super(b);
	}

	@Override
	public int onBeforeDamage(Card card, Card damagingCard, int damageTaken, DamageType type) {
		if (DamageType.counter.equals(type) && Helper.getLucky(this.getValue())) {
			return 0;
		}
		return damageTaken;
	}
}
