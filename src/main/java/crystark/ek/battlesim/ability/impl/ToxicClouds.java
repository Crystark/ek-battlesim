package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.PoisonConsumer;
import crystark.ek.battlesim.status.StatusEffect;

public class ToxicClouds extends SingleValueAbility<Integer> implements IPower, IOnRuneTurn {
	public static final String	NAME	= "Toxic Clouds";
	private final StatusEffect	statusEffect;

	public ToxicClouds(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public ToxicClouds(AbilityDesc b) {
		super(b);
		this.statusEffect = PoisonConsumer.makeEffect(getValue());
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void apply(Card sourceCard) {
		this.toxicClouds(sourceCard.owner().opponent(), sourceCard);
	}

	public void toxicClouds(Player targetPlayer, Card sourceCard) {
		targetPlayer.consumeCardsField(new PoisonConsumer(getValue(), sourceCard, statusEffect));
	}

	@Override
	public void onRuneTurn(Player player) {
		this.toxicClouds(player.opponent(), null);
	}
}
