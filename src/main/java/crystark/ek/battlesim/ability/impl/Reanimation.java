package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.AbilitiesVisitor;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;

public class Reanimation extends SingleValueAbility<Integer> implements IPower {
	public static final String NAME = "Reanimation";

	@Override
	public String getName() {
		return NAME;
	}

	public Reanimation(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		Player owner = sourceCard.owner();
		if (!owner.isCemeteryLocked()) {
			Card card = owner.pickCardInCemetery(CardPredicates.CAN_BE_REANIMATED);
			Reanimation.reanimation(card);
		}
	}

	public static void reanimation(Card cardToReanimate) {
		if (cardToReanimate != null) {
			cardToReanimate.removeFromCemetery();
			cardToReanimate.addToField();
		}
	}
	

	@Override
	public void accept(AbilitiesVisitor visitor) {
		visitor.visit(this);
	}
}
