package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IAfterDamage;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;

/**
 Cast Exile to the attacker when this card receives physical damage. This skill doesn't work if the attacker has Resistance.
 */
public class GetOut extends SingleValueAbility<Integer> implements IAfterDamage {
	public static final String NAME = "Get Out";

	public GetOut(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void onAfterDamage(Card counterCard, Card counteredCard, int damageTaken) {
		if (counteredCard != null && counteredCard.isAlive() && !(counteredCard.hasResistance() || counteredCard.isLastChance() || counteredCard.isBoss())) {
			counteredCard.removeFromField();
			counteredCard.addRandomlyToDeck();
		}
	}
}
