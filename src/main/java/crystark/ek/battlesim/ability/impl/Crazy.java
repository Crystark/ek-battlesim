package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.Constants;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Crazy extends SingleValueAbility<Integer> implements IPower {
	private static final Logger	L		= LoggerFactory.getLogger(Crazy.class);
	public static final String	NAME	= "Crazy";

	@Override
	public String getName() {
		return NAME;
	}

	public Crazy(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		crazy(sourceCard.owner().opponent().pickCardInField(), 100);
	}

	public static void crazy(Card card, int damageRate) {
		if (damageRate == 0) damageRate = 100;

		// && !card.hasResistance() ????
		if (card != null && !card.hasImmunity() && !card.isLastChance()) {
			int dmg = card.getCurrentAtk() * damageRate / 100;
			Player owner = card.owner();
			int idx = owner.getIndexOnField(card);
			Card leftCard = owner.atIndexOnField(idx - 1);
			if (leftCard != null) {
				if (Constants.IS_DEBUG)
					L.debug(card + " attacks " + leftCard + ". It's completly crazy!");
				leftCard.damaged(card, dmg, DamageType.unavoidable);
			}
			Card rightCard = owner.atIndexOnField(idx + 1);
			if (rightCard != null) {
				if (Constants.IS_DEBUG)
					L.debug(card + " attacks " + rightCard + ". It's completly crazy!");
				rightCard.damaged(card, dmg, DamageType.unavoidable);
			}
		}
	}
}
