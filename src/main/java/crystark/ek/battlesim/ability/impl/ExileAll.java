package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.tools.PredicateConsumer;

import java.util.Objects;
import java.util.function.Consumer;

/**
 * Desperation: Exile
 * After being eliminated, except this card, all cards in hand and on the battlefield return back to player's deck.
 * Ignores Immunity and Resistance. This skill doesn't work against Demons.
 *
 * Actually the skill doesn't affects cards with Immunity or Resistance in opponent field
 * TODO: Does 'Exile All' skill affects cards with active LastChance and cards in hand with Immunity or Resistance?
 */
public class ExileAll extends SingleValueAbility<Integer> implements IPower {
	public static final String          NAME        = "Exile All";
	private static final Consumer<Card> HAND_EXILE  = card -> {
												if (!card.isBoss() && !card.hasImmunity() && !card.hasResistance()) {
													card.removeFromHand();
													card.addRandomlyToDeck();
												}
											};
	private static final Consumer<Card> FORCE_EXILE = card -> {
												if (!card.isBoss() && !card.hasImmunity() && !card.hasResistance()) {
													card.removeFromField();
													card.addRandomlyToDeck();
												}
											};

	@Override
	public String getName() {
		return NAME;
	}

	public ExileAll(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		Player player = sourceCard.owner();

		Player opponent = player.opponent();
		Consumer<Card> forceExileExceptSelf = new PredicateConsumer<>(FORCE_EXILE, (c) -> !Objects.equals(c, sourceCard));
		opponent.consumeCardsField(FORCE_EXILE);
		player.consumeCardsField(forceExileExceptSelf);
		opponent.consumeCardsHand(HAND_EXILE);
		player.consumeCardsHand(HAND_EXILE);

	}
}
