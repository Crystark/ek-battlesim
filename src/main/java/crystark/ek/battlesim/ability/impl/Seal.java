package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.TrapConsumer;

public class Seal extends SingleValueAbility<Integer> implements IPower {
	public static final String	NAME	= "Seal";
	private final TrapConsumer	consumer;

	@Override
	public String getName() {
		return NAME;
	}

	public Seal(AbilityDesc b) {
		super(b);
		this.consumer = new TrapConsumer(100);
	}

	@Override
	public void apply(Card sourceCard) {
		if (!sourceCard.hasMarker(NAME)) {
			Player opponent = sourceCard.owner().opponent();
			opponent.consumeCardsField(this.consumer);
			sourceCard.addMarker(NAME);
		}
	}

}