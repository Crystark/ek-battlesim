package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IBeforeDamage;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;

public class LifeLink extends SingleValueAbility<Integer> implements IBeforeDamage {
	public static final String NAME = "Life Link";

	@Override
	public String getName() {
		return NAME;
	}

	public LifeLink(AbilityDesc b) {
		super(b);
	}

	@Override
	public int onBeforeDamage(Card card, Card damagingCard, int damageTaken, DamageType type) {
		if (DamageType.attack.equals(type)) {
			Player owner = card.owner();
			int idx = owner.getIndexOnField(card);
			Card leftCard = owner.atIndexOnField(idx - 1);
			Card rightCard = owner.atIndexOnField(idx + 1);

			int split = 1;
			if (leftCard != null)
				split++;
			if (rightCard != null)
				split++;

			if (split == 1) {
				return damageTaken;
			}
			int damage = damageTaken / split;
			int damageOnMain = damage;

			if (leftCard != null) {
				long initialHealth = leftCard.getCurrentHp();
				if (initialHealth < damage) {
					damageOnMain += damage - initialHealth;
				}
				leftCard.damaged(card, damage, DamageType.unavoidable);
			}
			if (rightCard != null && rightCard.isAlive()) {
				long initialHealth = rightCard.getCurrentHp();
				if (initialHealth < damage) {
					damageOnMain += damage - initialHealth;
				}
				rightCard.damaged(card, damage, DamageType.unavoidable);
			}
			return damageOnMain;
		}
		return damageTaken;
	}
}
