package crystark.ek.battlesim.ability.impl.alias;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.impl.MultiCrazy;

public class Waltz extends MultiCrazy {

	public static final String NAME = "Waltz";

	public Waltz(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}
}
