package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IBeforeDamage;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;

/**
 * When the card receive damage, it will receive total damage minus X damage.
 *
 */
public class DemonArmor extends SingleValueAbility<Integer> implements IBeforeDamage {
	public static final String	NAME = "Demon Armor";
	private final int decrease;

	public DemonArmor(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public DemonArmor(AbilityDesc b) {
		super(b);
		this.decrease = getValue();
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public boolean canBeLaunched(Card owner) {
		return true;
	}

	@Override
	public int onBeforeDamage(Card card, Card damagingCard, int damageTaken, DamageType type) {
		//TODO What damage types are decreased?
		if (!DamageType.unavoidable.equals(type)) {
			damageTaken = Math.max(0, damageTaken - decrease);
		}
		return damageTaken;
	}
}
