package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IAfterAttack;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;

public class FatalPoison extends SingleValueAbility<Integer> implements IAfterAttack {
	public static final String NAME = "Fatal Poison";

	public FatalPoison(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void onAfterAttack(Card hittingCard, Card hitCard, int damageDone) {
		if (damageDone > 0 && !hitCard.isBoss() && hitCard.isAlive() && hitCard.isPoisonned()) {
			hitCard.owner().destroyCard(hitCard);
		}
	}
}
