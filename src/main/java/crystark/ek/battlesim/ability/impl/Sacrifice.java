package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IQuickstrikeAble;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;

import java.util.Objects;

public class Sacrifice extends SingleValueAbility<Integer> implements IQuickstrikeAble {
	public static final String	NAME	= "Sacrifice";
	private final float			multiplier;

	@Override
	public String getName() {
		return NAME;
	}

	public Sacrifice(AbilityDesc b) {
		super(b);
		this.multiplier = getValue() / 100f;
	}

	@Override
	public void apply(Card sourceCard) {
		Player owner = sourceCard.owner();
		Card cardToSacrifice = owner.pickCardInField((c) -> !Objects.equals(c, sourceCard));

		if (cardToSacrifice != null && cardToSacrifice.canBeSacrificed()) {
			// Sacrifice kills a card and triggers desperation
			owner.destroyCard(cardToSacrifice);
			sourceCard.addBaseAtk((int) (sourceCard.getCurrentBaseAtk() * multiplier));
			sourceCard.addBaseHp((int) (sourceCard.getCurrentBaseHp() * multiplier));
		}
	}
}
