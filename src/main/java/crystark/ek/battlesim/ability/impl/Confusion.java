package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.consumer.ConfusionConsumer;

public class Confusion extends SingleValueAbility<Integer> implements IPower {
	public static final String NAME = "Confusion";

	@Override
	public String getName() {
		return NAME;
	}

	public Confusion(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		ConfusionConsumer.confuse(sourceCard.owner().opponent().pickCardInField(), getValue());
	}
}
