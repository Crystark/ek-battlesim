package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IAfterTurn;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;

public class Rejuvenation extends SingleValueAbility<Integer> implements IAfterTurn {
	public static final String NAME = "Rejuvenation";

	public Rejuvenation(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Rejuvenation(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void onTurnFinish(Card sourceCard) {
		sourceCard.heal(getValue());
	}
}
