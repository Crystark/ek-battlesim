package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.Constants;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;

/**
 * Randomly transfer 1 card from cemetery to HAND
 */
public class MultiReanimation extends SingleValueAbility<Integer> implements IPower {
	public static final String NAME = "Multi-Reanimation";

	@Override
	public String getName() {
		return NAME;
	}

	public MultiReanimation(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		Player owner = sourceCard.owner();
		//TODO Does 'Suppressing' (Paraselene) skill blocks Multi-Reanimation?
		if (!owner.isCemeteryLocked()) {
			int count = Math.min(getValue(), owner.countCardsInCemetery());
			int countInHand = owner.countCardsInHand();
			for (int i = 0; i < count; i++) {
				Card card = owner.pickCardInCemetery();
				MultiReanimation.resurrect(card, countInHand < Player.MAX_HAND_SIZE);
				countInHand++;
			}
		}
	}

	public static void resurrect(Card cardToResurrect, boolean isToHand) {
		if (cardToResurrect != null) {

			final Player owner = cardToResurrect.owner();

			if (Constants.IS_DEBUG) {
				if (isToHand){
					L.debug(cardToResurrect + " is resurrected");
				} else {
					L.debug(cardToResurrect + " is resurrected to deck." + owner + " hand is full.");
				}
			}

			cardToResurrect.removeFromCemetery();
			if (isToHand) {
				cardToResurrect.addToHand();
			} else {
				cardToResurrect.addOnTopOfDeck();
			}
		}
	}
}
