package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IDesperationAble;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.AbilitiesVisitor;
import crystark.ek.battlesim.battle.Card;

public class LastChance extends SingleValueAbility<Integer> implements IDesperationAble {
	public static final String NAME = "Last Chance";

	public LastChance(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void apply(Card card) {
		if (!card.hasMarker(NAME) && card.getCurrentHp() < 1) {
			card.addMarker(NAME);
			card.markLastChance();
		}
	}

	@Override
	public void accept(AbilitiesVisitor visitor) {
		//Desp only, so do nothing
	}
}
