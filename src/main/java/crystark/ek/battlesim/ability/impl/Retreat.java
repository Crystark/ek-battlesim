package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IAfterDamage;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;

/**
Return directly to your hand when hit by a physical attack. If your hand is full, the card returns to your deck.
 */
public class Retreat extends SingleValueAbility<Integer> implements IAfterDamage {
	public static final String NAME = "Retreat";

	public Retreat(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void onAfterDamage(Card counterCard, Card counteredCard, int damageTaken) {
		if (counterCard.isAlive()) { // Only retreat if alive
			counterCard.removeFromField();
			if (counterCard.owner().countCardsInHand() < Player.MAX_HAND_SIZE) {
				counterCard.addToHand();
			}
			else {
				counterCard.addOnTopOfDeck();
			}
		}
	}
}
