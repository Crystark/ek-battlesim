package crystark.ek.battlesim.ability.impl.alias;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.impl.FieldOfChaos;

public class Confusions extends FieldOfChaos {
	public static final String NAME = "Confusions";

	@Override
	public String getName() {
		return NAME;
	}

	public Confusions(AbilityDesc b) {
		super(b);

	}

}