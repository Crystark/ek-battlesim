package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.AbilitiesVisitor;

public class Evasion extends SingleValueAbility<Void> {
	public static final String NAME = "Evasion";

	@Override
	public String getName() {
		return NAME;
	}

	public Evasion() {
		this(baseAbilityDesc(NAME, null));
	}

	public Evasion(AbilityDesc b) {
		super(b);
	}

	@Override
	public void accept(AbilitiesVisitor visitor) {
		visitor.visit(this);
	}
}
