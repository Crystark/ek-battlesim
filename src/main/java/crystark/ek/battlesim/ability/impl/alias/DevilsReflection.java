package crystark.ek.battlesim.ability.impl.alias;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.impl.Retaliation;

public class DevilsReflection extends Retaliation {

	public static final String NAME = "Devil's Reflection";

	public DevilsReflection(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}
}
