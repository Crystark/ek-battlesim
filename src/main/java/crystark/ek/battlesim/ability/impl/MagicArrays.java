package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.SpellMarkConsumer;

public class MagicArrays extends SingleValueAbility<Integer> implements IPower {
	public static final String NAME = "Magic Arrays";

	public MagicArrays(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public MagicArrays(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void apply(Card sourceCard) {
		this.magicArrays(sourceCard.owner().opponent());
	}

	public void magicArrays(Player targetPlayer) {
		targetPlayer.consumeCardsField(new SpellMarkConsumer(getValue()));
	}
}
