package crystark.ek.battlesim.ability.impl;

import java.util.Iterator;
import java.util.Objects;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IAttack;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.CardAttack;
import crystark.ek.battlesim.battle.CardInFieldIterator;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.CreateAttackIterator;

public class ChainAttack extends SingleValueAbility<Integer> implements IAttack {
	public static final String	NAME	= "Chain Attack";
	private final float			multiplier;

	@Override
	public String getName() {
		return NAME;
	}

	public ChainAttack(AbilityDesc b) {
		super(b);
		this.multiplier = getValue() / 100f;
	}

	@Override
	public Iterator<CardAttack> attacksIterator(Card defendingCard, int unreducedDamage, int damage) {
		int increasedDamage = (int) (damage * multiplier);

		CardInFieldIterator fieldIterator = defendingCard.owner()
			.getFieldIterator(
				CardPredicates
					.isNamed(defendingCard.desc.name)
					.and((c) -> !Objects.equals(c, defendingCard)));

		return new CreateAttackIterator(fieldIterator, increasedDamage);

	}
}
