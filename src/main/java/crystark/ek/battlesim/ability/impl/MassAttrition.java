package crystark.ek.battlesim.ability.impl;

import java.util.function.Consumer;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;

public class MassAttrition extends SingleValueAbility<Integer> implements IPower {
	public static final String		NAME	= "Mass Attrition";
	private MassAttritionConsumer	consumer;

	@Override
	public String getName() {
		return NAME;
	}

	public MassAttrition(AbilityDesc b) {
		super(b);
		this.consumer = new MassAttritionConsumer(getValue());
	}

	@Override
	public void apply(Card sourceCard) {
		sourceCard.owner().opponent().consumeCardsHand(consumer);
	}

	static class MassAttritionConsumer implements Consumer<Card> {
		private final int value;

		MassAttritionConsumer(int value) {
			this.value = value;
		}

		@Override
		public void accept(Card card) {
			card.increaseTimer(value);
		}
	}
}
