package crystark.ek.battlesim.ability.impl;

import java.util.function.Predicate;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.DefaultAuraAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.effect.BonusHp;
import crystark.ek.battlesim.effect.Effect;

public class DivineProtection extends DefaultAuraAbility {
	public static final String	NAME	= "Divine Protection";
	private final BonusHp		effect;

	public DivineProtection(AbilityDesc b) {
		super(b);
		effect = new BonusHp(getValue());
	}

	public DivineProtection(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public Effect<? super Card> getEffect(Card sourceCard) {
		return effect;
	}

	@Override
	public Predicate<Card> getEligibleCardsFilter(Card sourceCard) {
		return Predicate.<Card> isEqual(sourceCard).or(CardPredicates.nextTo(sourceCard));
	}
}
