package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.DefaultBeforeAttackAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.effect.BonusAtk;

public class BloodyBattle extends DefaultBeforeAttackAbility {
	public static final String	NAME	= "Bloody Battle";
	private final float			multiplier;

	public BloodyBattle(AbilityDesc b) {
		super(b);
		this.multiplier = getValue() / 100f;
	}

	public BloodyBattle(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public boolean conditionsAreMet(Player sourcePlayer, Card sourceCard) {
		return sourceCard.getBaseLostHp() > 0;
	}

	@Override
	public BonusAtk getEffect(Card sourceCard) {
		return new BonusAtk((int) (multiplier * sourceCard.getBaseLostHp()));
	}
}
