package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;

import java.util.function.Consumer;

public class Warcry extends SingleValueAbility<Integer> implements IPower, IOnRuneTurn {
	public static final String	NAME	= "Warcry";
	private WarcryConsumer		consumer;

	@Override
	public String getName() {
		return NAME;
	}

	public Warcry(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Warcry(AbilityDesc b) {
		super(b);
		this.consumer = new WarcryConsumer(getValue());
	}

	@Override
	public void apply(Card sourceCard) {
		warcry(sourceCard.owner());
	}

	public void warcry(Player player) {
		player.consumeCardsHand(consumer);
	}

	@Override
	public void onRuneTurn(Player player) {
		warcry(player);
	}

	static class WarcryConsumer implements Consumer<Card> {
		private final int value;

		WarcryConsumer(int value) {
			this.value = value;
		}

		@Override
		public void accept(Card card) {
			card.decreaseTimer(value);
		}
	}
}
