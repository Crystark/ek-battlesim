package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IBeforeDamage;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.AbilitiesVisitor;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;

public class IceShield extends SingleValueAbility<Integer> implements IBeforeDamage {
	public static final String NAME = "Ice Shield";

	@Override
	public String getName() {
		return NAME;
	}

	public IceShield(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public IceShield(AbilityDesc b) {
		super(b);
	}

	@Override
	public int onBeforeDamage(Card card, Card damagingCard, int damageTaken, DamageType type) {
		if (DamageType.attack.equals(type) && damageTaken > this.getValue() && (damagingCard == null || !damagingCard.hasInfiltrator())) {
			return this.getValue();
		}
		return damageTaken;
	}

	@Override
	public void accept(AbilitiesVisitor visitor) {
		visitor.visit(this);
	}
}
