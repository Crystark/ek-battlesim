package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.DefaultBuffAbility;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.effect.BonusHp;
import crystark.ek.battlesim.effect.Effect;

import java.util.function.Predicate;

public class Barricade extends DefaultBuffAbility<Integer> implements IOnRuneTurn {
	public static final String NAME = "Barricade";

	public Barricade(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Barricade(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public Effect<? super Card> getEffect(Card sourceCard) {
		return new BonusHp(getValue());
	}

	@Override
	public Predicate<Card> getEligibleCardsFilter(Card sourceCard) {
		return t -> true;
	}
}
