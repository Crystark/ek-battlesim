package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.common.api.CardType;
import crystark.ek.battlesim.ability.Harmonic;

/**
 * Each one Tundra card entering the battlefield after this card will add 6% Attack and 6% HP to this card.
 * The summoned cards will not activate this skill.
 */
public class TundraHarmonic extends Harmonic {
	public static final String	NAME	= "Tundra Harmonic";

	public TundraHarmonic(AbilityDesc b) {
		super(b, CardType.tundra);
	}

	@Override
	public String getName() {
		return NAME;
	}

}
