package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.Ability;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.ShockConsumer;

/**
 * Deals damage equal to 170 + 85x (with x being the number of cards on the opponent's battlefield) to all the opponent's cards.
 * The cards receiving damage will suffer a 75% chance of not being able to attack in the next round.
 *
 * Thunderstorm >=15 are different skills
 */
public class Thunderstorm extends Ability<Integer, Integer> implements IPower {
	public static final String NAME = "Thunderstorm";

	@Override
	public String getName() {
		return NAME;
	}

	public Thunderstorm(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		this.frostShock(sourceCard.owner().opponent(), sourceCard);
	}

	public void frostShock(Player targetPlayer, Card sourceCard) {
		targetPlayer.consumeCardsField(new ShockConsumer(getValue() + getValue2() * targetPlayer.countCardsInField(), 75, sourceCard));
	}
}