package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.consumer.ResistanceLessConsumer;

public class ResistanceLess extends SingleValueAbility<Integer> implements IPower{
	public static final String NAME = "Resistance-less";

	public ResistanceLess() {
		this(baseAbilityDesc(NAME, null));
	}

	public ResistanceLess(AbilityDesc desc) {
		super(desc);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void apply(Card sourceCard) {
		sourceCard.owner().opponent().consumeCardsField(ResistanceLessConsumer.INSTANCE);
	}
}
