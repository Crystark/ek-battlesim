package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.PoisonConsumer;
import crystark.ek.battlesim.status.StatusEffect;
import crystark.tools.CustomPredicates;

public class Smog extends SingleValueAbility<Integer> implements IPower, IOnRuneTurn {
	public static final String	NAME	= "Smog";
	private final StatusEffect	statusEffect;

	@Override
	public String getName() {
		return NAME;
	}

	public Smog(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Smog(AbilityDesc b) {
		super(b);
		this.statusEffect = PoisonConsumer.makeEffect(getValue());
	}

	@Override
	public void apply(Card sourceCard) {
		this.smog(sourceCard.owner().opponent(), sourceCard);
	}

	public void smog(Player targetPlayer, Card sourceCard) {
		int count = targetPlayer.countCardsInField();
		if (count > 0)
			targetPlayer.consumeCardsField(new PoisonConsumer(getValue(), sourceCard, statusEffect).withPredicate(CustomPredicates.randomNAmongX(3, count)));
	}

	@Override
	public void onRuneTurn(Player player) {
		this.smog(player.opponent(), null);
	}
}