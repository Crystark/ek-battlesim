package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IBeforeDamage;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.AbilitiesVisitor;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;

public class Reflection extends SingleValueAbility<Integer> implements IBeforeDamage {
	public static final String NAME = "Reflection";

	public Reflection(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Reflection(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public int onBeforeDamage(Card card, Card damagingCard, int damageTaken, DamageType type) {
		switch (type) {
			case fire:
			case ice:
			case lightning:
			case blood:
				// Can be null if the damage results from a Rune
				// Can be dead if killed by a previous reflect or acting on behalf of a Death Whisper / Preemptive Strike
				if (damagingCard != null && damagingCard.isAlive())
					damagingCard.damaged(null, this.getValue(), DamageType.unavoidable);
				return 0;
			default:
				return damageTaken;
		}
	}

	@Override
	public void accept(AbilitiesVisitor visitor) {
		visitor.visit(this);
	}
}
