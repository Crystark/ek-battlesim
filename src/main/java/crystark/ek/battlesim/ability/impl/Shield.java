package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.effect.BonusHp;
import crystark.ek.battlesim.status.StatusEffect;
import crystark.ek.battlesim.status.StatusEffectType;

public class Shield extends SingleValueAbility<Integer> implements IOnRuneTurn {
	public static final String	NAME	= "Shield";
	private final StatusEffect	statusEffect;

	@Override
	public String getName() {
		return NAME;
	}

	public Shield(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Shield(AbilityDesc b) {
		super(b);
		this.statusEffect = StatusEffect.builder()
			.setEffect(new BonusHp(getValue()))
			.setType(StatusEffectType.buff)
			.build();
	}

	public void shield(Player targetPlayer) {
		Card card = targetPlayer.pickCardInField();
		if (card != null) {
			this.statusEffect.applyTo(card);
		}
	}

	@Override
	public void onRuneTurn(Player player) {
		this.shield(player);
	}
}
