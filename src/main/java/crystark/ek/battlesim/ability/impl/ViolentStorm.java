package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.Helper;
import crystark.ek.battlesim.ability.DefaultBeforeAttackAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.effect.BonusAtk;

public class ViolentStorm extends DefaultBeforeAttackAbility {
	public static final String NAME = "Violent Storm";

	public ViolentStorm(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public boolean conditionsAreMet(Player sourcePlayer, Card sourceCard) {
		return true;
	}

	@Override
	public BonusAtk getEffect(Card sourceCard) {
		int bonus = Helper.random(getValue2() - getValue() + 1) + getValue();
		return new BonusAtk(bonus);
	}
}
