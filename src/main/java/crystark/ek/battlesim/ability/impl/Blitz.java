package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.DefaultBeforeAttackAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.effect.BonusAtk;

public class Blitz extends DefaultBeforeAttackAbility {
	public static final String	NAME	= "Blitz";
	private final float			multiplier;

	public Blitz(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Blitz(AbilityDesc b) {
		super(b);
		this.multiplier = getValue() / 100f;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public boolean conditionsAreMet(Player sourcePlayer, Card sourceCard) {
		Card card = sourcePlayer.oppositeCardOnField(sourceCard);
		return (card != null && (card.isFrozen() || card.isShocked() || card.isPoisonned() || card.isBurning()));
	}

	@Override
	public BonusAtk getEffect(Card sourceCard) {
		return new BonusAtk((int) (multiplier * sourceCard.getCurrentBaseAtk()));
	}
}
