package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.common.api.CardType;
import crystark.ek.battlesim.ability.DefaultBeforeAttackAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.effect.BonusAtk;

public class SwampPurity extends DefaultBeforeAttackAbility {
	public static final String	NAME	= "Swamp Purity";
	private final float			multiplier;

	public SwampPurity(AbilityDesc b) {
		super(b);
		this.multiplier = getValue() / 100f;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public boolean conditionsAreMet(Player sourcePlayer, Card sourceCard) {
		Card oppositeCard = sourcePlayer.oppositeCardOnField(sourceCard);
		return oppositeCard != null && CardType.swamp.equals(oppositeCard.getType());
	}

	@Override
	public BonusAtk getEffect(Card sourceCard) {
		return new BonusAtk((int) (multiplier * sourceCard.getCurrentBaseAtk()));
	}
}
