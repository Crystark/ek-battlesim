package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.PoisonConsumer;
import crystark.ek.battlesim.status.StatusEffect;

public class Venom extends SingleValueAbility<Integer> implements IPower, IOnRuneTurn {
	public static final String	NAME	= "Venom";
	private final StatusEffect	statusEffect;

	@Override
	public String getName() {
		return NAME;
	}

	public Venom(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Venom(AbilityDesc b) {
		super(b);
		this.statusEffect = PoisonConsumer.makeEffect(getValue());
	}

	@Override
	public void apply(Card sourceCard) {
		this.venom(sourceCard.owner().opponent(), sourceCard);
	}

	public void venom(Player targetPlayer, Card sourceCard) {
		PoisonConsumer.poison(targetPlayer.pickCardInField(), sourceCard, getValue(), statusEffect);
	}

	@Override
	public void onRuneTurn(Player player) {
		this.venom(player.opponent(), null);
	}
}
