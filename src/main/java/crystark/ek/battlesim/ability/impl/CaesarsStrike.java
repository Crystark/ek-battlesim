package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.DefaultBeforeAttackAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.effect.BonusAtk;

public class CaesarsStrike extends DefaultBeforeAttackAbility {
	public static final String	NAME	= "Caesar's Strike";
	private final float			ratio;

	public CaesarsStrike(AbilityDesc b) {
		super(b);
		this.ratio = getValue() / 100f;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public boolean conditionsAreMet(Player sourcePlayer, Card sourceCard) {
		return true;
	}

	@Override
	public BonusAtk getEffect(Card sourceCard) {
		Player owner = sourceCard.owner();

		int idx = owner.getIndexOnField(sourceCard);
		Card leftCard = owner.atIndexOnField(idx - 1);
		Card rightCard = owner.atIndexOnField(idx + 1);

		int bonus = 0;
		if (leftCard != null) {
			bonus += leftCard.desc.attack * ratio;
		}
		if (rightCard != null) {
			bonus += rightCard.desc.attack * ratio;
		}
		return new BonusAtk(bonus);
	}
}
