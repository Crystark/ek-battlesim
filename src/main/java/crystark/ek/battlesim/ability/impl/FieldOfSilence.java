package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.SilenceConsumer;

public class FieldOfSilence extends SingleValueAbility<Integer> implements IPower {
	public static final String		NAME	= "Field of Silence";
	private final SilenceConsumer	consumer;

	public FieldOfSilence() {
		this(baseAbilityDesc(NAME, null));
	}

	public FieldOfSilence(AbilityDesc b) {
		super(b);
		this.consumer = SilenceConsumer.INSTANCE;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void apply(Card sourceCard) {
		this.fieldOfSilence(sourceCard.owner().opponent());
	}

	public void fieldOfSilence(Player targetPlayer) {
		targetPlayer.consumeCardsField(consumer);
	}
}
