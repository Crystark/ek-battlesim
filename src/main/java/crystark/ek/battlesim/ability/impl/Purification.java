package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IBeforeRound;
import crystark.ek.battlesim.ability.IDesperationAble;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.ability.IQuickstrikeAble;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.PurificationConsummer;

// TODO Does Purification is silenced?

public class Purification extends SingleValueAbility<Void> implements IBeforeRound, IOnRuneTurn, IQuickstrikeAble, IDesperationAble {
	public static final String					NAME		= "Purification";
	public static final PurificationConsummer	CONSUMER	= new PurificationConsummer();

	@Override
	public String getName() {
		return NAME;
	}

	public Purification() {
		this(baseAbilityDesc(NAME, null));
	}

	public Purification(AbilityDesc b) {
		super(b);
	}

	@Override
	public void onBeforeRound(Card sourceCard) {
		Purification.purify(sourceCard.owner());
	}

	public static void purify(Player player) {
		player.consumeCardsField(CONSUMER);
	}

	@Override
	public void onRuneTurn(Player player) {
		Purification.purify(player);
	}

	@Override
	//on QS or D
	public void apply(Card sourceCard) {
		Purification.purify(sourceCard.owner());
	}

}
