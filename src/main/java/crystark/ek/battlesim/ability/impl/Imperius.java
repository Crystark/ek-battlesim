package crystark.ek.battlesim.ability.impl;

import java.util.function.Predicate;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;

public class Imperius extends SingleValueAbility<Integer> implements IPower {
	public static final String				NAME		= "Imperius";
	private static final Predicate<Card>	PREDICATE	= c -> !c.isBoss() && !c.hasReanimateIgnoreSilence();

	@Override
	public String getName() {
		return NAME;
	}

	public Imperius(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		Player source = sourceCard.owner().opponent();
		if (!source.isCemeteryLocked()) {
			Card card = source.pickCardInCemetery(PREDICATE);
			Imperius.imperius(card);
		}
	}

	public static void imperius(Card card) {
		if (card != null) {
			card.removeFromCemetery();
			card.setOwner(card.owner().opponent());
			card.addToField();
		}
	}
}
