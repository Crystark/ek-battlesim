package crystark.ek.battlesim.ability.impl.alias;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.impl.Curse;

public class DevilsCurse extends Curse {

	public static final String NAME = "Devil's Curse";

	public DevilsCurse(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}
}
