package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.ShockConsumer;
import crystark.tools.CustomPredicates;

public class ChainLightning extends SingleValueAbility<Integer> implements IPower, IOnRuneTurn {
	public static final String NAME = "Chain Lightning";

	@Override
	public String getName() {
		return NAME;
	}

	public ChainLightning(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public ChainLightning(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		this.chainLightning(sourceCard.owner().opponent(), sourceCard);
	}

	public void chainLightning(Player targetPlayer, Card sourceCard) {
		int count = targetPlayer.countCardsInField();
		if (count > 0)
			targetPlayer.consumeCardsField(new ShockConsumer(getValue(), 40, sourceCard).withPredicate(CustomPredicates.randomNAmongX(3, count)));
	}

	@Override
	public void onRuneTurn(Player player) {
		this.chainLightning(player.opponent(), null);
	}
}