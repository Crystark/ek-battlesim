package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.DefaultAuraAbility;
import crystark.ek.battlesim.ability.IAttackTargetChanger;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.effect.BonusAbility;
import crystark.ek.battlesim.effect.Effect;

import java.util.function.Predicate;

/**
 * Transfer enemy card attack to this card. If there are more than 1 card with the same skill, the damage will be transferred to them one by one.
 * <p>
 * The skill is not silenced. Damage reducer skills on card with Taunt works as well. If catched damage is larger HP then remaining dmg
 * doesn't apply to initially attacked card (it's diferent to Guard)
 * If there is two cards with Taunt and first is attacked then second card doesn't catch the damage. The damage is applied to first Taunt card.
 * Second attack of double attack skill is not intercepted by Taunt.
 * Clean Sweep is intercepted by Taunt
 */
public class Taunt extends DefaultAuraAbility {

	public static final String NAME = "Taunt";
	private Effect<Card> effect;
	public Taunt(AbilityDesc b) {

		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public Effect<? super Card> getEffect(Card sourceCard) {
		effect = new BonusAbility(() -> new TauntAura(sourceCard, id));
		return effect;
	}

	@Override
	public Predicate<Card> getEligibleCardsFilter(Card sourceCard) {
		//WARN! If tauntSourceCard has TauntAura from another card. It may cause indefinite recursion.
		return (c) -> !(c.getAuraAbilities().stream()
			.anyMatch((s) -> s instanceof Taunt));
	}

	public static class TauntAura implements IAttackTargetChanger {

		//private static final String MARKER_ON_TAUNT = "onTaunt";
		private final Card tauntSourceCard;
		private final long id;

		private TauntAura(Card sourceCard, long id) {
			this.tauntSourceCard = sourceCard;
			this.id = id;
		}

		@Override
		public String getName() {
			return "Taunt Aura";
		}

		@Override
		public boolean canBeLaunched(Card owner) {
			return true;
		}


		@Override
		public Card selectTarget(Card card, Card damagingCard, int damage, DamageType type) {

			if (type != DamageType.attack || !tauntSourceCard.isAlive())
				return card;

			//Do not catch attack to card which apply Taunt itself
			//if (card.hasMarker(MARKER_ON_TAUNT)) return damageTaken;

			//WARN! tauntSourceCard may has TauntAura from another card. It may cause indefinite recursion.
			//tauntSourceCard.addMarker(MARKER_ON_TAUNT);
			//damagingCard.attack(tauntSourceCard, damageTaken);
			//tauntSourceCard.removeMarker(MARKER_ON_TAUNT);

			return tauntSourceCard;
		}
	}
}
