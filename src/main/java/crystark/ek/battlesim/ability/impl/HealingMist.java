package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;

public class HealingMist extends SingleValueAbility<Integer> implements IPower {
	public static final String NAME = "Healing Mist";

	@Override
	public String getName() {
		return NAME;
	}

	public HealingMist(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		Player owner = sourceCard.owner();
		int idx = owner.getIndexOnField(sourceCard);
		Card leftCard = owner.atIndexOnField(idx - 1);
		Card rightCard = owner.atIndexOnField(idx + 1);

		sourceCard.heal(getValue());
		if (leftCard != null) {
			leftCard.heal(getValue());
		}
		if (rightCard != null) {
			rightCard.heal(getValue());
		}
	}
}
