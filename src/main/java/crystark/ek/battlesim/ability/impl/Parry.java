package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IBeforeDamage;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;

public class Parry extends SingleValueAbility<Integer> implements IBeforeDamage {
	public static final String NAME = "Parry";

	@Override
	public String getName() {
		return NAME;
	}

	public Parry(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Parry(AbilityDesc b) {
		super(b);
	}

	@Override
	public int onBeforeDamage(Card card, Card damagingCard, int damageTaken, DamageType type) {
		if (DamageType.attack.equals(type)) {
			damageTaken -= this.getValue();
		}
		return Math.max(0, damageTaken);
	}
}
