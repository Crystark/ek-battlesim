package crystark.ek.battlesim.ability.impl.alias;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.impl.Execution;

public class VampireBlood extends Execution {
	public static final String	NAME	= "Vampire Blood";

	public VampireBlood(AbilityDesc b) {
		super(b);
	}
}
