package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.FreezeConsumer;

public class Iceball extends SingleValueAbility<Integer> implements IPower, IOnRuneTurn {
	public static final String NAME = "Iceball";

	@Override
	public String getName() {
		return NAME;
	}

	public Iceball(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Iceball(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		this.iceball(sourceCard.owner().opponent(), sourceCard);
	}

	public void iceball(Player targetPlayer, Card sourceCard) {
		FreezeConsumer.freeze(targetPlayer.pickCardInField(), sourceCard, getValue(), 45);
	}

	@Override
	public void onRuneTurn(Player player) {
		this.iceball(player.opponent(), null);
	}
}
