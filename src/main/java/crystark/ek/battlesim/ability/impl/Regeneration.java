package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.HealConsumer;

public class Regeneration extends SingleValueAbility<Integer> implements IPower, IOnRuneTurn {
	public static final String	NAME	= "Regeneration";
	private final HealConsumer	consumer;

	@Override
	public String getName() {
		return NAME;
	}

	public Regeneration(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Regeneration(AbilityDesc b) {
		super(b);
		this.consumer = new HealConsumer(getValue());
	}

	@Override
	public void apply(Card sourceCard) {
		this.regeneration(sourceCard.owner());
	}

	public void regeneration(Player player) {
		player.consumeCardsField(this.consumer);
	}

	@Override
	public void onRuneTurn(Player player) {
		this.regeneration(player);
	}
}
