package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.ManaCorruptionConsumer;
import crystark.tools.CustomPredicates;

public class ManaBurn extends SingleValueAbility<Integer> implements IPower {
	public static final String NAME = "Mana Burn";

	@Override
	public String getName() {
		return NAME;
	}

	public ManaBurn(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public ManaBurn(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		Player targetPlayer = sourceCard.owner().opponent();
		int count = targetPlayer.countCardsInField();
		if (count > 0)
			targetPlayer.consumeCardsField(new ManaCorruptionConsumer(getValue(), 3, sourceCard).withPredicate(CustomPredicates.randomNAmongX(3, count)));
	}
}
