package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.CardAction;
import crystark.ek.battlesim.battle.consumer.SnipeConsumer;

import java.util.List;

public class Multshot extends SingleValueAbility<Integer> implements IPower {
	public static final String NAME = "Multshot";
	protected int count;

	@Override
	public String getName() {
		return NAME;
	}

	public Multshot(AbilityDesc b) {
		super(b);
		Integer c = getValue2();
		count = c == null || c == 0 ? 1 : c;
	}

	@Override
	public void apply(Card sourceCard) {
		List<CardAction> cardsToHit = sourceCard.owner().opponent().pickLowestHpCards(count);
		if (cardsToHit != null) {
			for (int i = 0, cardsToHitSize = cardsToHit.size(); i < cardsToHitSize; i++) {
				CardAction ca = cardsToHit.get(i);
				SnipeConsumer.snipe(ca, sourceCard, getValue());
			}
		}
	}
}
