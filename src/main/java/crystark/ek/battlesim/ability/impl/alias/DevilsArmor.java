package crystark.ek.battlesim.ability.impl.alias;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.impl.Retaliation;

public class DevilsArmor extends Retaliation {

	public static final String NAME = "Devil's Armor";

	public DevilsArmor(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}
}
