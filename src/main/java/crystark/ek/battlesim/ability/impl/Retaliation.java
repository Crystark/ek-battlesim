package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IAfterDamage;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.CardAction;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;

public class Retaliation extends SingleValueAbility<Integer> implements IAfterDamage {
	public static final String NAME = "Retaliation";

	public Retaliation(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Retaliation(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void onAfterDamage(Card counterCard, Card counteredCard, int damageTaken) {
		Player counteredCardOwner = counteredCard.owner();

		//In case of Clean Seep the counterCard is not in front of counteredCard, so it's wrong to calc relative to counteredCard
		int idx = counterCard.owner().getIndexOnField(counterCard);;

		Card leftCard = counteredCardOwner.atIndexOnField(idx - 1);
		Card rightCard = counteredCardOwner.atIndexOnField(idx + 1);

		CardAction caLeft = leftCard == null ? null : leftCard.prepareAction();
		CardAction caCountered = counteredCard.prepareAction();
		CardAction caRight = rightCard == null ? null : rightCard.prepareAction();

		if (caLeft != null) {
			caLeft.damaged(counterCard, this.getValue(), DamageType.counter);
		}
		caCountered.damaged(counterCard, this.getValue(), DamageType.counter);
		if (caRight != null) {
			caRight.damaged(counterCard, this.getValue(), DamageType.counter);
		}
	}
}
