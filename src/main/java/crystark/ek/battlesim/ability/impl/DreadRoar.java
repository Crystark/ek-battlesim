package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.RoarConsumer;

/**
 * Every turn halves all cards' Attack on opponent's battlefield for 1 round. This skill doesn't work against Demons.
 */
public class DreadRoar extends SingleValueAbility<Integer> implements IPower {
	public static final String	NAME	= "Dread Roar";
	private final RoarConsumer	consumer;

	@Override
	public String getName() {
		return NAME;
	}

	public DreadRoar() {
		this(baseAbilityDesc(NAME, null));
	}

	public DreadRoar(AbilityDesc b) {
		super(b);
		consumer = new RoarConsumer();
	}

	@Override
	public void apply(Card sourceCard) {
		this.dreadRoar(sourceCard.owner().opponent());
	}

	public void dreadRoar(Player targetPlayer) {
		targetPlayer.consumeCardsField(this.consumer);
	}
}
