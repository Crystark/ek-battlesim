package crystark.ek.battlesim.ability.impl.alias;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.impl.LightingCut;

/**
 * Deal 500 damage to 3 random enemy cards.
 * The cards receiving damage will have a 75% chance to get frozen and can't attack or use skills in the next turn.
 * If any of the card has Immunity or Reflection, deal 300% damage.
 */
public class ThunderStrick extends LightingCut {
	public static final String NAME = "Thunder strick";

	public ThunderStrick(AbilityDesc desc) {
		super(desc);
	}
}