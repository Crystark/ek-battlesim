package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.common.api.CardType;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;

import java.util.function.Consumer;

public class WordOfWithering extends SingleValueAbility<Integer> implements IPower {
	public static final String				NAME	= "Word of Withering";
	private final WordOfWitheringConsumer	consumer;

	public WordOfWithering(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public WordOfWithering(AbilityDesc b) {
		super(b);
		this.consumer = new WordOfWitheringConsumer(getValue());
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void apply(Card sourceCard) {
		this.wordOfWithering(sourceCard.owner().opponent());
	}

	public void wordOfWithering(Player targetPlayer) {
		targetPlayer.consumeCardsField(this.consumer);
	}

	static class WordOfWitheringConsumer implements Consumer<Card> {
		private final float multiplier;

		WordOfWitheringConsumer(int value) {
			this.multiplier = value / 100f;
		}

		@Override
		public void accept(Card card) {
			// WordOfWithering skill activates for non immune hydra (ew) bosses.
			if (!card.hasImmunity()  && !CardType.demon.equals(card.getType())) {
				card.removeBaseAtk((int) (card.getCurrentBaseAtk() * multiplier));
				card.removeBaseHp((int) (card.getCurrentBaseHp() * multiplier));
			}
		}
	}
}
