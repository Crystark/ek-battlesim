package crystark.ek.battlesim.ability.impl.alias;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.impl.Multshot;

public class DualSnipe extends Multshot {
	public static final String NAME = "Dual Snipe";

	@Override
	public String getName() {
		return NAME;
	}

	public DualSnipe(AbilityDesc b) {
		super(b);
	}

	@Override
	public Integer getValue2() {
		return 2;
	}
}
