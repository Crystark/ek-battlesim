package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.DefaultBeforeAttackAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.effect.BonusAtk;

public class Vendetta extends DefaultBeforeAttackAbility {
	public static final String NAME = "Vendetta";

	public Vendetta(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public boolean conditionsAreMet(Player sourcePlayer, Card sourceCard) {
		return sourcePlayer.hasCardInCemetery();
	}

	@Override
	public BonusAtk getEffect(Card sourceCard) {
		return new BonusAtk(getValue() * sourceCard.owner().countCardsInCemetery());
	}
}
