package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;

public class Impede extends SingleValueAbility<Integer> implements IPower {
	public static final String NAME = "Impede";

	@Override
	public String getName() {
		return NAME;
	}

	public Impede(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		Card card = sourceCard.owner().opponent().pickFirstLowestTimerCard();
		if (card != null) {
			card.increaseTimer(getValue());
		}
	}
}
