package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IDesperationAble;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.CardAction;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;

/**
 * Before being sent to the cemetery, this card deals 80 damage to the card directly across from it and both cards adjactent to that card.
 * <br/>
 * LoA has "Self-destruction N" and "Self-destruction". "Self-destruction" is different skill.
 */
public class SelfDestruct extends SingleValueAbility<Integer> implements IDesperationAble {
	public static final String NAME = "Self-Destruct";

	@Override
	public String getName() {
		return NAME;
	}

	public SelfDestruct(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public SelfDestruct(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		Player player = sourceCard.owner();
		Player opponent = player.opponent();
		int idx = player.getIndexOnField(sourceCard);
		Card frontCard = opponent.atIndexOnField(idx);
		Card leftCard = opponent.atIndexOnField(idx - 1);
		Card rightCard = opponent.atIndexOnField(idx + 1);

		CardAction caLeft = (leftCard == null || leftCard.hasImmunity()) ? null : leftCard.prepareAction();
		CardAction caFront = (frontCard == null || frontCard.hasImmunity()) ? null : frontCard.prepareAction();
		CardAction caRight = (rightCard == null || rightCard.hasImmunity()) ? null : rightCard.prepareAction();

		if (caLeft != null) {
			caLeft.damaged(sourceCard, getValue(), DamageType.unavoidable);
		}
		if (caFront != null) {
			caFront.damaged(sourceCard, getValue(), DamageType.unavoidable);
		}
		if (caRight != null) {
			caRight.damaged(sourceCard, getValue(), DamageType.unavoidable);
		}
	}
}
