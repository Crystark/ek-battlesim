package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IAfterDamage;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;

public class ShieldOfEarth extends SingleValueAbility<Integer> implements IAfterDamage {
	public static final String NAME = "Shield of Earth";

	@Override
	public String getName() {
		return NAME;
	}

	public ShieldOfEarth(AbilityDesc b) {
		super(b);
	}

	@Override
	public void onAfterDamage(Card counterCard, Card counteredCard, int damageTaken) {
		counteredCard.markStunned();
	}
}