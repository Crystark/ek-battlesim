package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.BlindConsumer;

public class Flash extends SingleValueAbility<Integer> implements IPower {
	public static final String	NAME	= "Flash";
	private final BlindConsumer	consumer;

	public Flash(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Flash(AbilityDesc b) {
		super(b);
		this.consumer = new BlindConsumer(getValue());
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void apply(Card sourceCard) {
		this.flash(sourceCard.owner().opponent(), sourceCard);
	}

	public void flash(Player targetPlayer, Card sourceCard) {
		targetPlayer.consumeCardsField(consumer);
	}
}
