package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;

public class Curse extends SingleValueAbility<Integer> implements IPower {
	public static final String NAME = "Curse";

	@Override
	public String getName() {
		return NAME;
	}

	public Curse(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		sourceCard.owner().opponent().damaged(this.getValue(), true);
	}
}
