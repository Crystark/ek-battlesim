package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.HealConsumer;

public class DianasProtection extends SingleValueAbility<Integer> implements IPower {
	public static final String	NAME	= "Diana's Protection";
	private final HealConsumer	consumer;

	@Override
	public String getName() {
		return NAME;
	}

	public DianasProtection(AbilityDesc b) {
		super(b);
		float multiplier = getValue() / 100f;
		this.consumer = new HealConsumer(c -> (int) (c.getCurrentBaseHp() * multiplier));
	}

	public DianasProtection(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	@Override
	public void apply(Card sourceCard) {
		this.dianasProtection(sourceCard.owner());
	}

	public void dianasProtection(Player player) {
		player.consumeCardsField(this.consumer);
	}
}
