package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;

public class SacredFlame extends SingleValueAbility<Integer> implements IPower {
	public static final String NAME = "Sacred Flame";

	@Override
	public String getName() {
		return NAME;
	}

	public SacredFlame(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		Player opponent = sourceCard.owner().opponent();
		if (!opponent.isCemeteryLocked()) {
			Card deadCard = opponent.pickCardInCemetery();
			if (deadCard != null) {
				deadCard.removeFromCemetery();
			}
		}
	}
}
