package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;

public class Bless extends SingleValueAbility<Integer> implements IPower, IOnRuneTurn {
	public static final String	NAME	= "Bless";
	private final float			multiplier;

	@Override
	public String getName() {
		return NAME;
	}

	public Bless(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Bless(AbilityDesc b) {
		super(b);
		this.multiplier = getValue() / 100f;
	}

	@Override
	public void apply(Card sourceCard) {
		this.orison(sourceCard.owner());
	}

	public void orison(Player targetPlayer) {
		targetPlayer.heal((int) (multiplier * targetPlayer.desc.health));
	}

	@Override
	public void onRuneTurn(Player player) {
		this.orison(player);
	}
}
