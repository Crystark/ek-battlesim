package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.Helper;
import crystark.ek.battlesim.ability.DefaultBeforeAttackAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.effect.BonusAtk;

public class Concentration extends DefaultBeforeAttackAbility {
	public static final String	NAME	= "Concentration";
	private final float			multiplier;

	public Concentration(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Concentration(AbilityDesc b) {
		super(b);
		this.multiplier = getValue() / 100f;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public boolean conditionsAreMet(Player sourcePlayer, Card sourceCard) {
		return Helper.getLucky(50) && sourcePlayer.oppositeCardOnField(sourceCard) != null;
	}

	@Override
	public BonusAtk getEffect(Card sourceCard) {
		return new BonusAtk((int) (multiplier * sourceCard.getCurrentBaseAtk()));
	}
}
