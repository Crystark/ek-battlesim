package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.Ability;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Player;

public class AsurasFlame extends Ability<Integer, Integer> implements IPower {
	public static final String NAME = "Asura's Flame";

	@Override
	public String getName() {
		return NAME;
	}

	public AsurasFlame(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public AsurasFlame(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		this.asurasFlame(sourceCard.owner().opponent(), sourceCard);
	}

	public void asurasFlame(Player targetPlayer, Card sourceCard) {
		int damage = getValue() + getValue2() * targetPlayer.countCardsInField();
		targetPlayer.consumeCardsField(card -> {
			if (card.hasReflectionOrNotImmune()) {
				card.damaged(sourceCard, damage, DamageType.fire);
			}
		});
	}
}