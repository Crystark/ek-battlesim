package crystark.ek.battlesim.ability.impl.alias;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.impl.Rejuvenation;

public class SavingGrace extends Rejuvenation {
	public static final String NAME = "Saving Grace";

	@Override
	public String getName() {
		return NAME;
	}

	public SavingGrace(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public SavingGrace(AbilityDesc b) {
		super(b);
	}
}
