package crystark.ek.battlesim.ability.impl.alias;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.impl.FireGod;

public class Hellfire extends FireGod {
	public static final String NAME = "Hellfire";

	public Hellfire(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Hellfire(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}
}
