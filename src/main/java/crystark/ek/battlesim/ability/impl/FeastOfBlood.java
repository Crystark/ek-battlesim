package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.consumer.BiteConsumer;

public class FeastOfBlood extends SingleValueAbility<Integer> implements IPower {
	public static final String NAME = "Feast of Blood";

	@Override
	public String getName() {
		return NAME;
	}

	public FeastOfBlood(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		sourceCard.owner().opponent().consumeCardsField(new BiteConsumer(getValue(), sourceCard));
	}
}
