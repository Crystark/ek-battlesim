package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.SpellMarkConsumer;

public class SpellMark extends SingleValueAbility<Integer> implements IPower {
	public static final String NAME = "Spell Mark";

	@Override
	public String getName() {
		return NAME;
	}

	public SpellMark(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		this.spellMark(sourceCard.owner().opponent());
	}

	public void spellMark(Player targetPlayer) {
		SpellMarkConsumer.mark(targetPlayer.pickCardInField(), getValue());
	}
}
