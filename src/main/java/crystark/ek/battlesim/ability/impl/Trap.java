package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.TrapConsumer;
import crystark.tools.CustomPredicates;

public class Trap extends SingleValueAbility<Integer> implements IPower {
	public static final String	NAME	= "Trap";
	private final TrapConsumer	consumer;

	@Override
	public String getName() {
		return NAME;
	}

	public Trap(AbilityDesc b) {
		super(b);
		this.consumer = new TrapConsumer(65);
	}

	@Override
	public void apply(Card sourceCard) {
		Player opponent = sourceCard.owner().opponent();
		int count = opponent.countCardsInField();
		if (count > 0)
			opponent.consumeCardsField(this.consumer.withPredicate(CustomPredicates.randomNAmongX(this.getValue(), count)));
	}
}