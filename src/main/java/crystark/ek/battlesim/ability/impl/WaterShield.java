package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.Ability;
import crystark.ek.battlesim.ability.IBeforeDamage;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;

public class WaterShield extends Ability<Integer, Integer> implements IBeforeDamage {
	public static final String NAME = "Water Shield";

	public WaterShield(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public WaterShield(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public int onBeforeDamage(Card card, Card damagingCard, int damageTaken, DamageType type) {
		if (DamageType.attack.equals(type) && damageTaken > getValue()) {
			card.owner().heal(Math.min(getValue2(), damageTaken - getValue()));
			return getValue();
		}
		return damageTaken;
	}
}
