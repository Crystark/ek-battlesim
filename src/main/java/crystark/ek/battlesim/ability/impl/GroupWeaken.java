package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;

import java.util.function.Consumer;

public class GroupWeaken extends SingleValueAbility<Integer> implements IPower, IOnRuneTurn {
	public static final String		NAME	= "Group Weaken";
	private final WeakenConsumer	consumer;

	@Override
	public String getName() {
		return NAME;
	}

	public GroupWeaken(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public GroupWeaken(AbilityDesc b) {
		super(b);
		this.consumer = new WeakenConsumer(getValue());
	}

	@Override
	public void apply(Card sourceCard) {
		this.groupWeaken(sourceCard.owner().opponent());
	}

	public void groupWeaken(Player targetPlayer) {
		targetPlayer.consumeCardsField(this.consumer);
	}

	@Override
	public void onRuneTurn(Player player) {
		this.groupWeaken(player.opponent());
	}

	static class WeakenConsumer implements Consumer<Card> {
		private final Integer value;

		WeakenConsumer(int value) {
			this.value = value;
		}

		@Override
		public void accept(Card card) {
			if (!card.hasImmunity()) {
				card.removeBaseAtk(value);
			}
		}
	}
}
