package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;

public class Reincarnation extends SingleValueAbility<Integer> implements IPower {
	public static final String NAME = "Reincarnation";

	@Override
	public String getName() {
		return NAME;
	}

	public Reincarnation(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		Player owner = sourceCard.owner();
		if (!owner.isCemeteryLocked()) {
			for (int i = 0; i < getValue(); i++) {
				Card card = owner.pickCardInCemetery();
				if (card == null)
					break;
				card.removeFromCemetery();
				card.addOnTopOfDeck();
			}
		}
	}
}
