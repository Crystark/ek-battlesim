package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.SnipeConsumer;

public class Snipe extends SingleValueAbility<Integer> implements IPower, IOnRuneTurn {
	public static final String NAME = "Snipe";

	@Override
	public String getName() {
		return NAME;
	}

	public Snipe(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Snipe(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		this.snipe(sourceCard.owner().opponent(), sourceCard);
	}

	public void snipe(Player opponent, Card sniper) {
		Card cardToHit = opponent.pickLowestHpCard();
		SnipeConsumer.snipe(cardToHit, sniper, getValue());
	}

	@Override
	public void onRuneTurn(Player player) {
		this.snipe(player.opponent(), null);
	}
}
