package crystark.ek.battlesim.ability.impl.alias;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.impl.Taunt;

public class CatTank extends Taunt {

 	public static final String NAME = "Cat Tank";

	public CatTank(AbilityDesc b) {
		super(b);
	}

	@Override public String getName() {
		return NAME;
	}
}
