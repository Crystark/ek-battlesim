package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IAfterHit;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;

public class Puncture extends SingleValueAbility<Integer> implements IAfterHit {
	public static final String	NAME	= "Puncture";
	private final float			multiplier;

	public Puncture(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Puncture(AbilityDesc b) {
		super(b);
		this.multiplier = getValue() / 100f;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void onAfterHit(Card hittingCard, Card hitCard, int damageDone) {
		hitCard.owner().damaged((int) (damageDone * multiplier), true);
	}
}
