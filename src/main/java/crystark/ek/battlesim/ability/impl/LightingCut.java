package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.Ability;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.LightingCutConsumer;
import crystark.tools.CustomPredicates;
import crystark.tools.Tools;

/**
 * Deal 400 damage to 3 random enemy cards. The cards receiving damage will have a 75% chance to get frozen
 * and can't attack or use skills in the next turn.If any of the card has Immunity or Reflection, deal 300% damage.
 * TODO Does 'SpellMark' increase damage?
 * In game tests results:
 * Electric Shock is applying instead of freeze
 * Electric Shock is going together with 400 damage
 * Electric Shock does not shock (disables physical attacks) Immunity or Reflection cards
 * Electric Shock is not reflected.
 * Evil Mantis' Sensitive skill blocks damage, but can be shocked
 *
 * Lighting Cut is close to 'Snipe'
 */

/*
"Type": 26,
"LanchType": 4,
"LanchCondition": 0,
"LanchConditionValue": 0,
"AffectType": 165,
"AffectValue": "3_75",
"AffectValue2": "400_300",
 */
public class LightingCut extends Ability<String, String> implements IPower {
	public static final String NAME = "Lighting Cut";

	final int targetCount;
	final int baseDamage;
	final int chanceToFreeze;
	final int multiplier;

	public LightingCut(AbilityDesc desc) {

		super(desc);

		String[] split = getValue().split("_");
		targetCount = Tools.parseInt(split[0]);
		chanceToFreeze = Tools.parseInt(split[1]);

		split = getValue2().split("_");
		baseDamage = Tools.parseInt(split[0]);
		multiplier = Tools.parseInt(split[1]) / 100;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void apply(Card sourceCard) {
		Player targetPlayer = sourceCard.owner().opponent();
		lightingCut(targetPlayer, sourceCard, targetCount, baseDamage, chanceToFreeze, multiplier);
	}

	public static void lightingCut(Player targetPlayer, Card sourceCard, int targetCount, int baseDamage, int chanceToFreeze, int multiplier)
	{
		int count = targetPlayer.countCardsInField();
		if (count > 0) {
			targetPlayer.consumeCardsField(
				new LightingCutConsumer(baseDamage, multiplier, chanceToFreeze, sourceCard)
					.withPredicate(CustomPredicates.randomNAmongX(targetCount, count))
			);
		}
	}
}
