package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.Constants;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;

public class Destroy extends SingleValueAbility<Integer> implements IPower {
	public static final String NAME = "Destroy";

	@Override
	public String getName() {
		return NAME;
	}

	public Destroy(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		Player opponent = sourceCard.owner().opponent();
		Card card = opponent.pickCardInField();
		if (card != null) {
			if (!card.hasImmunity() && !card.hasResistance()) {
				opponent.destroyCard(card);
			}
			else if (Constants.IS_DEBUG)
				L.debug(card + " resists destroy! ");
		}
	}
}
