package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IBeforeDamage;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;

/**
 * The first physical attack against this card will be nullified.
 * @see PhysicalShield Is it the same?
 */
public class DivineShield extends SingleValueAbility<Integer> implements IBeforeDamage {
	public static final String NAME = "Divine Shield";

	public DivineShield(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public int onBeforeDamage(Card card, Card damagingCard, int damageTaken, DamageType type) {
		if (DamageType.attack.equals(type) && !card.hasMarker(NAME)) {
			card.addMarker(NAME);
			return 0;
		}
		return damageTaken;
	}
}
