package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.AbilityFactory;
import crystark.ek.battlesim.ability.DefaultAuraAbility;
import crystark.ek.battlesim.ability.IAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.effect.BonusAbility;
import crystark.ek.battlesim.effect.Effect;

import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 Armor-less 5 = When dealing damage, the card and its adjacent cards have 70% ignore all defense skills.
 */
public class ArmorLess5 extends DefaultAuraAbility {
	public static final String NAME = "Armor-less_5";

	final BonusAbility bonusAbility;

	public ArmorLess5(AbilityDesc b) {
		super(b);
		final Supplier<IAbility> abilitySupplier = AbilityFactory.getSupplier(baseAbilityDesc(ArmorLess.NAME, 70));
		bonusAbility = new BonusAbility(abilitySupplier);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public Effect<? super Card> getEffect(Card sourceCard) {
		return bonusAbility;
	}

	@Override
	public Predicate<Card> getEligibleCardsFilter(Card sourceCard) {
		return CardPredicates.nextTo(sourceCard).or(Predicate.isEqual(sourceCard));
	}

	/*
 {
        "SkillId": 3116,
        "Name": "Armor-less 5",
        "Type": 49,
        "LanchType": 19,
        "LanchCondition": 0,
        "LanchConditionValue": 0,
        "AffectType": 173,
        "AffectValue": 3154,
        "AffectValue2": 2,
        "SkillCategory": 1,
        "Desc": "“When dealing damage, the card and its adjacent cards have 70% ignore all defense skills."
      },

      	{
        "SkillId": 3154,
        "Name": "Armor-less 6",
        "Type": 47,
        "LanchType": 0,
        "LanchCondition": 0,
        "LanchConditionValue": 0,
        "AffectType": 170,
        "AffectValue": 75,
        "AffectValue2": 0,
        "SkillCategory": 1,
        "Desc": "When dealing physical damage, 75% chance ignore all defense skills."
      },

	 */
}
