package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IDesperationAble;
import crystark.ek.battlesim.ability.IQuickstrikeAble;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;

public class Obstinacy extends SingleValueAbility<Integer> implements IQuickstrikeAble, IDesperationAble {
	public static final String NAME = "Obstinacy";

	@Override
	public String getName() {
		return NAME;
	}

	public Obstinacy(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		sourceCard.owner().damaged(this.getValue(), false);
	}
}
