package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.Helper;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.AbilitiesVisitor;
import crystark.ek.battlesim.battle.Card;

public class Resurrection extends SingleValueAbility<Integer> {
	public static final String NAME = "Resurrection";

	@Override
	public String getName() {
		return NAME;
	}

	public Resurrection(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Resurrection(AbilityDesc b) {
		super(b);
	}

	public boolean check(Card card) {
		return !card.owner().isResurrectionLocked() && Helper.getLucky(this.getValue());
	}

	@Override
	public void accept(AbilitiesVisitor visitor) {
		visitor.visit(this);
	}
}
