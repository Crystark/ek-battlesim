package crystark.ek.battlesim.ability.impl.alias;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.impl.PsychicMaster;

public class Conjurer extends PsychicMaster {
	public static final String NAME = "Conjurer";

	public Conjurer(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Conjurer(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}
}
