package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.Helper;
import crystark.ek.battlesim.ability.IBeforeDamage;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;

import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

public class Dodge extends SingleValueAbility<Integer> implements IBeforeDamage {
	public static final String	NAME	= "Dodge";
	private final PRNG prng;

	public Dodge(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Dodge(AbilityDesc b) {
		super(b);
		this.prng = new PRNG(getValue());
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public int onBeforeDamage(Card card, Card damagingCard, int damageTaken, DamageType type) {
		if (DamageType.attack == type && !damagingCard.hasInfiltrator() && (damagingCard.isBlind() || prng.test())) {
			return 0;
		}
		return damageTaken;
	}
	
	static class PRNG {

		static final NavigableMap<Integer, Double> successAfterFail = new ConcurrentSkipListMap<>();
		static final NavigableMap<Integer, Double> successAfterSuccess = new ConcurrentSkipListMap<>();

		static {
			successAfterFail.put(100, 1.0);
			successAfterFail.put(90, 1.0);
			successAfterFail.put(80, 1.0);
			successAfterFail.put(70, 1.0);
			successAfterFail.put(65, 0.99);
			successAfterFail.put(60, 0.87); // linear estimation
			successAfterFail.put(55, 0.77);
			successAfterFail.put(50, 0.67);
			successAfterFail.put(45, 0.59); // linear estimation
			successAfterFail.put(40, 0.54);
			successAfterFail.put(35, 0.46);
			successAfterFail.put(30, 0.40);
			successAfterFail.put(25, 0.30);
			successAfterFail.put(0, 0.0);

			successAfterSuccess.put(100, 1.0);
			successAfterSuccess.put(90, 0.89); // created for stabilization
			successAfterSuccess.put(80, 0.75); // created for stabilization
			successAfterSuccess.put(70, 0.57);
			successAfterSuccess.put(65, 0.47);
			successAfterSuccess.put(60, 0.42); // ???
			successAfterSuccess.put(55, 0.37);
			successAfterSuccess.put(50, 0.33);
			successAfterSuccess.put(45, 0.28); // ???
			successAfterSuccess.put(40, 0.20);
			successAfterSuccess.put(35, 0.15);
			successAfterSuccess.put(30, 0.1);
			successAfterSuccess.put(25, 0.1);
			successAfterSuccess.put(0, 0.0);
		}

		private final int afterFail;
		private final int afterSuccess;
		private boolean   lastResultSuccess = true;

		PRNG(int chance) {
			double d;
			d = 100d * successAfterFail.computeIfAbsent(chance, PRNG::computeSuccessAfterFail);
			this.afterFail = (int) Math.round(d);
			d = 100d * successAfterSuccess.computeIfAbsent(chance, PRNG::computeSuccessAfterSuccess);
			this.afterSuccess = (int) Math.round(d);
		}

		private static Double computeEntry(Integer chance, NavigableMap<Integer, Double> map) {
			Entry<Integer, Double> ceilingEntry = map.ceilingEntry(chance);
			Entry<Integer, Double> floorEntry = map.floorEntry(chance);
			if (ceilingEntry == null) {
				ceilingEntry = floorEntry;
				floorEntry = map.floorEntry(ceilingEntry.getKey() - 1);
			}
			else if (floorEntry == null) {
				floorEntry = ceilingEntry;
				ceilingEntry = map.ceilingEntry(floorEntry.getKey() + 1);
			}
			double y1 = ceilingEntry.getValue();
			double y2 = floorEntry.getValue();
			double x1 = ceilingEntry.getKey();
			double x2 = floorEntry.getKey();
			return y2 + ((((double) chance - x2) * (y1 - y2)) / (x1 - x2));
		}

		private static Double computeSuccessAfterFail(Integer chance) {
			return computeEntry(chance, successAfterFail);
		}

		private static Double computeSuccessAfterSuccess(Integer chance) {
			return computeEntry(chance, successAfterSuccess);
		}

		public boolean test() {
			int chance = lastResultSuccess ? afterSuccess : afterFail;
			//lastResultSuccess = (Math.random() < chance);
			lastResultSuccess = Helper.getLucky(chance);
			return lastResultSuccess;
		}
	}

	static class PRNG2 {

		private final int chance;

		public PRNG2(int chance){
			this.chance = chance;
		}
		public boolean test() {
			return Helper.getLucky(chance);
		}
	}
}
