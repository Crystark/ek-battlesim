package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.AbilitiesVisitor;

/**
 Armor-less = When dealing physical damage, X% chance ignore all defense skills.
 <p/>
 The skill doesn't ignore
 <li/>- Reflection
 <li/>- Sensitive
 <li/>- Dexterity
 <p/>
 The skill ignore
 <li/>- Life Link
 'Armor-less 5' is different skill
 Skill model doesn't activates IBeforeDamage skills if damage type is DamageType.attack.
 <p/>
 TODO Does LifeLink is ignored?
 */
public class ArmorLess extends SingleValueAbility<Integer>  {
	public static final String NAME = "Armor-less";

	@Override
	public String getName() {
		return NAME;
	}

	public ArmorLess(AbilityDesc b) {
		super(b);
	}

	@Override
	public void accept(AbilitiesVisitor visitor) {
		visitor.visit(this);
	}

	/*
 {
        "SkillId": 3116,
        "Name": "Armor-less 5",
        "Type": 49,
        "LanchType": 19,
        "LanchCondition": 0,
        "LanchConditionValue": 0,
        "AffectType": 173,
        "AffectValue": 3154,
        "AffectValue2": 2,
        "SkillCategory": 1,
        "Desc": "“When dealing damage, the card and its adjacent cards have 70% ignore all defense skills."
      },

      	{
        "SkillId": 3154,
        "Name": "Armor-less 6",
        "Type": 47,
        "LanchType": 0,
        "LanchCondition": 0,
        "LanchConditionValue": 0,
        "AffectType": 170,
        "AffectValue": 75,
        "AffectValue2": 0,
        "SkillCategory": 1,
        "Desc": "When dealing physical damage, 75% chance ignore all defense skills."
      },

	 */
}
