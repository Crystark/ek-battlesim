package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.Ability;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.CardInFieldIterator;
import crystark.ek.battlesim.battle.Player;
import crystark.tools.CustomPredicates;

/**
 * Multi-Crazy 3 = Cause Crazy to 3 random enemy cards every round, making it attack adjacent cards.This skill will be denied by Immunity.
 * Multi-Crazy 5 = Cause Crazy to 3 random enemy cards every round, making it attack adjacent cards with 100% damage.This skill will be denied by Immunity and Resistance.
 * Multi-Crazy 4 = QuikStrike. Upon entering the battlefield, cause Crazy to all enemy cards every round, making it attack adjacent cards with 70% attack power.This skill will be denied by Immunity and Resistance.
 *
 * TODO Does 'Multi-Crazy' takes pure random N cards with repeats or unique N cards?
 * TODO Does 'Multi-Crazy' is blocked by Resistance?
 */
public class MultiCrazy extends Ability<Integer, Integer> implements IPower {

	public static final String NAME = "Multi-Crazy";

	@Override
	public String getName() {
		return NAME;
	}

	public MultiCrazy(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		CardInFieldIterator fieldIterator;
		int cardsToCraze = getValue() == null ? 0 : getValue();

		final Player opponent = sourceCard.owner().opponent();
		if (cardsToCraze == 0) {
			fieldIterator = opponent.getFieldIterator((c) -> true);
		} else {
			final int cardsInFieldCount = opponent.countCardsInField();
			if (cardsToCraze >= cardsInFieldCount){
				fieldIterator = opponent.getFieldIterator((c) -> true);
			} else {
				fieldIterator = opponent.getFieldIterator(CustomPredicates.randomNAmongX(cardsToCraze, cardsInFieldCount));
			}
		}
		int dmgRate = getValue2() == null ? 0 : getValue2();
		while (fieldIterator.hasNext()) {
			final Card card = fieldIterator.next();
			if (card!= null && card.isAlive()) Crazy.crazy(card, dmgRate); //Do not take cards killed by previous 'Crazy'
		}
	}
	/*
	"SkillId": 3101,
        "Name": "Multi-Crazy 3",
        "Type": 64,
        "LanchType": 4,
        "LanchCondition": 0,
        "LanchConditionValue": 0,
        "AffectType": 157,
        "AffectValue": 3,
        "AffectValue2": 0,
        "SkillCategory": 2,
		Cause Crazy to 3 random enemy cards every round, making it attack adjacent cards.This skill will be denied by Immunity.

         {
        "SkillId": 3121,
        "Name": "Multi-Crazy 5",
        "Type": 44,
        "LanchType": 4,
        "LanchCondition": 0,
        "LanchConditionValue": 0,
        "AffectType": 157,
        "AffectValue": 3,
        "AffectValue2": 100,
        "SkillCategory": 2,
        "Desc": "Cause Crazy to 3 random enemy cards every round, making it attack adjacent cards with 100% damage.This skill will be denied by Immunity and Resistance."
      },

      {
        "SkillId": 3113,
        "Name": "Multi-Crazy 4",
        "Type": 52,
        "LanchType": 10,
        "LanchCondition": 0,
        "LanchConditionValue": 0,
        "AffectType": 157,
        "AffectValue": 0,
        "AffectValue2": 70,
        "SkillCategory": 2,
        "Desc": "Upon entering the battlefield, cause Crazy to all enemy cards every round, making it attack adjacent cards with 70% attack power.This skill will be denied by Immunity and Resistance."
      },
	 */
}
