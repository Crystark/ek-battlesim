package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.Ability;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.ShockConsumer;

/**
 * Deal 150 + 60x (number of cards on opponent's battlefield) to all opponent's cards. Deal the same damage to enemy hero, too.
 * Thunderstorm <15 are different skills
 */
public class Thundershower extends Ability<Integer, String> implements IPower {
	public static final String NAME = "Thundershower";
	private final int baseDmg;
	private final int mult;

	@Override
	public String getName() {
		return NAME;
	}

	public Thundershower(AbilityDesc b) {
		super(b);
		final String[] split = getValue2().split("_");
		baseDmg = Integer.valueOf(split[0]);
		mult  = Integer.valueOf(split[1]);
	}

	@Override
	public void apply(Card sourceCard) {
		this.frostShock(sourceCard.owner().opponent(), sourceCard);
	}

	public void frostShock(Player targetPlayer, Card sourceCard) {
		int damage = baseDmg + mult * targetPlayer.countCardsInField();
		//TODO Does 'Thunderstorm 15' has chance to disable physical attack like 'Thunderstorm 10'?
		targetPlayer.consumeCardsField(new ShockConsumer(damage, 0, sourceCard));
		//TODO Does 'Thunderstorm 15' triggers Guard?
		targetPlayer.damaged(damage, true);
	}
}