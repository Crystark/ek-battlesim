package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.DefaultAuraAbility;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.effect.BonusBaseAtk;

import java.util.Objects;
import java.util.function.Predicate;

public class PowerSource extends DefaultAuraAbility implements IOnRuneTurn {
	public static final String	NAME	= "Power Source";
	private final BonusBaseAtk	effect;

	public PowerSource(AbilityDesc b) {
		super(b);
		this.effect = new BonusBaseAtk(getValue());
	}

	public PowerSource(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public BonusBaseAtk getEffect(Card sourceCard) {
		return effect;
	}

	@Override
	public Predicate<Card> getEligibleCardsFilter(Card sourceCard) {
		return (c) -> !Objects.equals(c, sourceCard);
	}
}
