package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IBeforeDamage;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;

public class MagicShield extends SingleValueAbility<Integer> implements IBeforeDamage {
	public static final String NAME = "Magic Shield";

	public MagicShield(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public MagicShield(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public int onBeforeDamage(Card card, Card damagingCard, int damageTaken, DamageType type) {
		switch (type) {
			case fire:
			case ice:
			case lightning:
			case blood:
				return Math.min(getValue(), damageTaken);
			default:
				return damageTaken;
		}
	}
}
