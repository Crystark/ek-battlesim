package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.Ability;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.EarthquakeConsummer;

public class Earthquake extends Ability<Integer, Integer> implements IPower {
	public static final String NAME = "Earthquake";

	@Override
	public String getName() {
		return NAME;
	}

	public Earthquake(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		this.earthquake(sourceCard.owner().opponent(), sourceCard);
	}

	public void earthquake(Player targetPlayer, Card sourceCard) {
		targetPlayer.consumeCardsField(new EarthquakeConsummer(getValue(), getValue2(), sourceCard));
	}
}