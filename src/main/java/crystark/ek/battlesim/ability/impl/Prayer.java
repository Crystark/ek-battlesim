package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;

public class Prayer extends SingleValueAbility<Integer> implements IPower, IOnRuneTurn {
	public static final String NAME = "Prayer";

	@Override
	public String getName() {
		return NAME;
	}

	public Prayer(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	public Prayer(AbilityDesc b) {
		super(b);
	}

	@Override
	public void apply(Card sourceCard) {
		this.prayer(sourceCard.owner());
	}

	public void prayer(Player targetPlayer) {
		targetPlayer.heal(this.getValue());
	}

	@Override
	public void onRuneTurn(Player player) {
		this.prayer(player);
	}
}
