package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IAfterHit;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;

public class Weaken extends SingleValueAbility<Integer> implements IAfterHit {
	public static final String NAME = "Weaken";

	public Weaken(AbilityDesc b) {
		super(b);
	}

	public Weaken(int value) {
		this(baseAbilityDesc(NAME, value));
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void onAfterHit(Card hittingCard, Card hitCard, int damageDone) {
		if (!hitCard.hasImmunity()) {
			hitCard.removeBaseAtk(getValue());
		}
	}
}
