package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.common.api.CardType;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.battle.consumer.ChangeTypeConsumer;

public class Naturalize extends SingleValueAbility<Integer> implements IPower, IOnRuneTurn {
	public static final String			NAME	= "Naturalize";
	private final ChangeTypeConsumer	consumer;

	@Override
	public String getName() {
		return NAME;
	}

	public Naturalize(AbilityDesc b) {
		super(b);
		this.consumer = new ChangeTypeConsumer(CardType.forest);
	}

	@Override
	public void apply(Card sourceCard) {
		this.naturalize(sourceCard.owner().opponent());
	}

	public void naturalize(Player targetPlayer) {
		targetPlayer.consumeCardsField(consumer);
	}

	@Override
	public void onRuneTurn(Player player) {
		this.naturalize(player.opponent());
	}
}
