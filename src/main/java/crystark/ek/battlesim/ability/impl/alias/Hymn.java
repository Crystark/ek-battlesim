package crystark.ek.battlesim.ability.impl.alias;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.impl.DianasTouch;

public class Hymn extends DianasTouch {

	public static final String	NAME	= "Hymn";

	public Hymn(int value) {
		super(value);
	}

	public Hymn(AbilityDesc b) {
		super(b);
	}
}
