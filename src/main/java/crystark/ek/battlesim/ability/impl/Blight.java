package crystark.ek.battlesim.ability.impl;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IAfterHit;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;

public class Blight extends SingleValueAbility<Integer> implements IAfterHit {
	public static final String NAME = "Blight";

	public Blight(AbilityDesc b) {
		super(b);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void onAfterHit(Card hittingCard, Card hitCard, int damageDone) {
		if (!hitCard.hasImmunity()) {
			hitCard.removeBaseAtk(getValue());
			hitCard.damaged(null, getValue(), DamageType.unavoidable);
		}
	}
}
