package crystark.ek.battlesim.ability.impl;

import java.util.function.Predicate;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.ability.IBeforeRound;
import crystark.ek.battlesim.ability.SingleValueAbility;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;

public class Cleanse extends SingleValueAbility<Integer> implements IBeforeRound {
	public static final String NAME = "Cleanse";

	@Override
	public String getName() {
		return NAME;
	}

	public Cleanse(AbilityDesc b) {
		super(b);
	}

	@Override
	public void onBeforeRound(Card sourceCard) {
		Player player = sourceCard.owner();
		int count = player.countCardsInField();
		if (count > 1)
			player.consumeCardsField(Purification.CONSUMER.withPredicate(Predicate.<Card> isEqual(sourceCard).or(CardPredicates.nextTo(sourceCard))));
	}
}
