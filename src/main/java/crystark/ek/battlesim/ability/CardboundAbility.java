package crystark.ek.battlesim.ability;

import crystark.common.api.AbilityDesc;
import crystark.common.format.AbilityDescFormat;
import crystark.ek.battlesim.battle.Card;
import crystark.tools.CustomPredicates;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * V: Ability descriptions seperated by &
 * V2: Cards to trigger bonus ability seperated by &
 */
public abstract class CardboundAbility extends Ability<String, String> implements IBeforeTurn {
	private static Map<String, List<Supplier<IAbility>>> suppliersCache  = new ConcurrentHashMap<>();
	private static Map<String, List<Predicate<Card>>>    predicatesCache = new ConcurrentHashMap<>();

	private final String                abilityRef;
	private       List<IAbility>        abilities;
	private       List<Predicate<Card>> cardTriggers;

	public CardboundAbility(AbilityDesc desc) {
		super(desc);

		this.abilityRef = getName() + '_' + id;

		List<Supplier<IAbility>> suppliers = suppliersCache.computeIfAbsent(
			getValue(),
			desc2 -> Arrays.stream(desc2.split("&"))
				.map(AbilityDescFormat::parse)
				.map(AbilityFactory::getSupplier)
				.collect(Collectors.toList()));

		this.abilities = new ArrayList<>();
		for (int i = 0, suppliersSize = suppliers.size(); i < suppliersSize; i++) {
			Supplier<IAbility> supplier = suppliers.get(i);
			final IAbility ability = supplier.get();
			if (ability != null) this.abilities.add(ability);
		}

		cardTriggers = predicatesCache.computeIfAbsent(
			getValue2().toLowerCase(),
			desc2 -> Arrays.stream(desc2.split("&"))
				.<Predicate<Card>> map(card -> c -> c.desc.name.equalsIgnoreCase(card.trim()))
				.collect(Collectors.toList()));
	}

	@Override
	public void apply(Card sourceCard) {
		if (!sourceCard.hasMarker(abilityRef)) {
			if (sourceCard.owner().hasCardInField(CustomPredicates.allOf(cardTriggers))) {
				sourceCard.addMarker(abilityRef);
				for (IAbility iAbility : abilities) {
					sourceCard.addAbility(iAbility);
				}
			}
		}
	}

}
