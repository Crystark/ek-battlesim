package crystark.ek.battlesim.ability;

import java.util.function.Predicate;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.effect.Effect;
import crystark.ek.battlesim.status.StatusEffect;
import crystark.ek.battlesim.status.StatusEffectType;

public interface IStatusEffectApplier<V> {
	V getValue();

	StatusEffectType getStatusType();

	Effect<? super Card> getEffect(Card sourceCard);

	Predicate<Card> getEligibleCardsFilter(Card sourceCard);

	void applyTo(Card source, Card target, StatusEffect status);
}
