package crystark.ek.battlesim.ability;

import crystark.common.api.AbilityDesc;
import crystark.common.api.CardType;
import crystark.ek.battlesim.battle.Card;

/**
 * Each one Swamp card entering the battlefield after this card will add 6% Attack and 6% HP to this card.
 * The summoned cards will not activate this skill.
  */
public abstract class Harmonic extends Ability<Integer, Integer> implements IAura {
	private final float    hpMultiplier;
	private final float    attMultiplier;
	private final CardType type;

	public Harmonic(AbilityDesc b, CardType type) {
		super(b);
		this.type = type;
		attMultiplier = getValue() / 100f;
		hpMultiplier = getValue2() / 100f;
	}

	@Override
	public void onEnterField(Card sourceCard) {

	}

	@Override
	public boolean onReceiveAura(Card targetCard, Card sourceCard) {
		if (type != targetCard.getType() || targetCard.isSummoned() || !this.canBeLaunched(sourceCard)) return false;

		sourceCard.addBaseAtk((int) ((float) sourceCard.desc.attack * attMultiplier));
		sourceCard.addBaseHp((int) ((float) sourceCard.desc.health * hpMultiplier));
		return true;
	}
}
