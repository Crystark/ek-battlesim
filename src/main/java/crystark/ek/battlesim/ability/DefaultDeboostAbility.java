package crystark.ek.battlesim.ability;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.status.StatusEffectApplierHelper.ApplierTarget;
import crystark.ek.battlesim.status.StatusEffectType;

import java.util.function.Predicate;

public abstract class DefaultDeboostAbility extends DefaultSEAAbility<Integer> implements IBeforeAttackDeboost, IConditionalStatusApplier {

	public DefaultDeboostAbility(AbilityDesc b) {
		super(b, StatusEffectType.boost, ApplierTarget.self);
	}

	@Override
	public Predicate<Card> getEligibleCardsFilter(Card sourceCard) {
		return Predicate.isEqual(sourceCard);
	}

	public void onBeforeAttack(Card sourceCard) {
		statusEffectApplierHelper.apply(sourceCard);
	}

}
