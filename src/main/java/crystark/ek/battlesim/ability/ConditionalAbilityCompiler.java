package crystark.ek.battlesim.ability;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.battle.Card;
import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtConstructor;
import javassist.CtField;
import javassist.CtMethod;
import javassist.CtNewConstructor;
import javassist.CtNewMethod;
import javassist.CtPrimitiveType;
import javassist.Modifier;
import javassist.NotFoundException;
import javassist.bytecode.Descriptor;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.function.Predicate;

/**
 *
 */
public final class ConditionalAbilityCompiler {
	private static final String GENERATED_SUFFIX = "$Judg";

	private ConditionalAbilityCompiler(){};

	/**
	 * Make class with same interfaces as in superClass. It impact to the battle phase that triggers the skill.
	 */
	public static IAbility createConditionalAbility(Class superClass, AbilityDesc abilityDesc, Predicate<Card> condition) {

		String newName = superClass.getName() + GENERATED_SUFFIX;
		Class clazz = createSkillClass(superClass, newName);
		try {
			final Constructor constructor = clazz.getConstructor(AbilityDesc.class, Predicate.class);
			final Object o = constructor.newInstance(abilityDesc, condition);
			return (IAbility) o;
		}
		catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Compile subclass of <code>superClass</code> that override canBeLaunched method. Let superClass is of MyAbility class, then compiled class is
	 * <pre>{@code
	 * public class MyAbility$Judg extends MyAbility {
	 *   private final Predicate condition$Judg;
	 *
	 *   public MyAbility$Judg(AbilityDesc desc, Predicate p){
	 *      super(desc);
	 *      condition$Judg = p;
	 *   }
	 *   @Override
	 *   public boolean canBeLaunched(Card card) {
	 *   	return super.canBeLaunched(card) && condition$Judg.test(card);
	 *   }
	 * }}</pre>
	 *
	 * @param superClass
	 * @return
	 */
	private static Class createSkillClass(Class superClass, String newName) {
		ClassPool pool = ClassPool.getDefault();
		CtClass cc = pool.getOrNull(newName);
		try {
			if (cc != null) {
				return Class.forName(newName);
			}
		} catch (ClassNotFoundException e){

		}

		try {
			synchronized (superClass) {
				cc = pool.getOrNull(newName);
				if (cc != null) {
					return Class.forName(newName);
				}
				CtClass superCc = pool.get(superClass.getName());
				cc = pool.makeClass(newName, superCc);

				CtClass predicateClass = pool.get(Predicate.class.getName());
				CtField conditionField = new CtField(predicateClass, "condition" + GENERATED_SUFFIX, cc);
				conditionField.setModifiers(Modifier.PRIVATE + Modifier.FINAL);
				cc.addField(conditionField);

				final CtConstructor constructor = CtNewConstructor.defaultConstructor(cc);
				CtClass abilityDescClass = pool.get(AbilityDesc.class.getName());
				constructor.addParameter(abilityDescClass);
				constructor.addParameter(predicateClass);
				constructor.setBody("{super($1); condition" + GENERATED_SUFFIX + " = $2;}");
				cc.addConstructor(constructor);

				CtClass cardClass = pool.get(Card.class.getName());
				final CtMethod canBeLaunchedSuperMethod = superCc.getMethod("canBeLaunched", Descriptor.ofMethod(CtPrimitiveType.booleanType, new CtClass[] { cardClass }));
				final CtMethod canBeLaunchedMethod = CtNewMethod.delegator(canBeLaunchedSuperMethod, cc);
				canBeLaunchedMethod.insertAfter("{return $_ && condition" + GENERATED_SUFFIX + ".test($1);}");
				cc.addMethod(canBeLaunchedMethod);

				return cc.toClass();
			}
		}
		catch (CannotCompileException | NotFoundException | ClassNotFoundException e) {
			throw new RuntimeException(e);
		}

	}
}
