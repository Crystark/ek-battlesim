package crystark.ek.battlesim.ability;

import crystark.ek.battlesim.battle.AbilitiesVisitor;
import crystark.ek.battlesim.battle.Card;

/**
 * TODO Merge with {@link IBeforeAttack} if possible
 */
public interface IBeforeAttackDeboost extends IAbility {
	void onBeforeAttack(Card sourceCard);

	@Override
	default void accept(AbilitiesVisitor visitor) {
		visitor.visit(this);
	}
}
