package crystark.ek.battlesim.ability;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;

public interface IConditionalStatusApplier {
	boolean conditionsAreMet(Player sourcePlayer, Card sourceCard);
}
