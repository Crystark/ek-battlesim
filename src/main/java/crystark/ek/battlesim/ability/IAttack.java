package crystark.ek.battlesim.ability;

import java.util.Iterator;

import crystark.ek.battlesim.battle.AbilitiesVisitor;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.CardAttack;

public interface IAttack extends IAbility {
	Iterator<CardAttack> attacksIterator(Card defendingCard, int unreducedDamage, int damage);

	@Override
	default void accept(AbilitiesVisitor visitor) {
		visitor.visit(this);
	}
}
