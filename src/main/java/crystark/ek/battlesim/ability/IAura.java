package crystark.ek.battlesim.ability;

import crystark.ek.battlesim.battle.AbilitiesVisitor;
import crystark.ek.battlesim.battle.Card;

public interface IAura extends IAbility {

	void onEnterField(Card sourceCard);

	boolean onReceiveAura(Card targetCard, Card sourceCard);

	@Override
	default void accept(AbilitiesVisitor visitor) {
		visitor.visit(this);
	}
}
