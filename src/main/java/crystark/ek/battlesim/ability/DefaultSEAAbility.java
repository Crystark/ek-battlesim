package crystark.ek.battlesim.ability;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.status.StatusEffect;
import crystark.ek.battlesim.status.StatusEffectApplierHelper;
import crystark.ek.battlesim.status.StatusEffectApplierHelper.ApplierTarget;
import crystark.ek.battlesim.status.StatusEffectType;

public abstract class DefaultSEAAbility<V> extends SingleValueAbility<V> implements IStatusEffectApplier<V> {

	protected final StatusEffectApplierHelper<V> statusEffectApplierHelper;
	protected final StatusEffectType             statusType;

	public DefaultSEAAbility(AbilityDesc b, StatusEffectType statusType, ApplierTarget target) {
		super(b);
		this.statusEffectApplierHelper = new StatusEffectApplierHelper<>(this, target);
		this.statusType = statusType;
	}

	@Override
	public StatusEffectType getStatusType() {
		return this.statusType;
	}

	public void onRuneTurn(Player player) {
		statusEffectApplierHelper.apply(player);
	}


	@Override
	public void applyTo(Card source, Card target, StatusEffect status) {
		status.applyTo(target);
	}
}
