package crystark.ek.battlesim.ability;

import crystark.ek.battlesim.battle.Card;

//Skill's AffectValue point to skill to activate
public interface IDesperationAble extends IAbility {

	public void apply(Card sourceCard);

	public boolean isDesperation();

}
