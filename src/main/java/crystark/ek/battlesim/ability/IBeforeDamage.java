package crystark.ek.battlesim.ability;

import crystark.ek.battlesim.battle.AbilitiesVisitor;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;

public interface IBeforeDamage extends IAbility {
	/**
	 *
	 * @param card
	 * @param damagingCard
	 * @param damageTaken
	 * @param type
	 * @return reduced damage value
	 */
	int onBeforeDamage(Card card, Card damagingCard, int damageTaken, DamageType type);

	@Override
	default void accept(AbilitiesVisitor visitor) {
		visitor.visit(this);
	}
}
