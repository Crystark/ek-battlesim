package crystark.ek.battlesim.ability;

import crystark.common.api.AbilityDesc;

public abstract class SingleValueAbility<V> extends Ability<V, V> {
	public SingleValueAbility(AbilityDesc desc) {
		super(desc);
	}
}
