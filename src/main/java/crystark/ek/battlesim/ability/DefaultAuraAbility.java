package crystark.ek.battlesim.ability;

import crystark.common.api.AbilityDesc;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.status.StatusEffectApplierHelper.ApplierTarget;
import crystark.ek.battlesim.status.StatusEffectType;

public abstract class DefaultAuraAbility extends DefaultSEAAbility<Integer> implements IAura {
	public DefaultAuraAbility(AbilityDesc b) {
		super(b, StatusEffectType.aura, ApplierTarget.allies);
	}

	public void onEnterField(Card sourceCard) {
		statusEffectApplierHelper.apply(sourceCard);
	}

	public boolean onReceiveAura(Card targetCard, Card sourceCard) {
		return statusEffectApplierHelper.applyTo(targetCard, sourceCard);
	}
}
