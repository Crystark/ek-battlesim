package crystark.ek.battlesim.ability;

import crystark.ek.battlesim.battle.Player;

public interface IOnRuneTurn extends IAbility {
	void onRuneTurn(Player player);
}
