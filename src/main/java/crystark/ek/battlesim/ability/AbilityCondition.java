package crystark.ek.battlesim.ability;

import java.util.function.Predicate;

import crystark.ek.battlesim.battle.Card;

/**
 *
 */
public class AbilityCondition {
	final Predicate<Card> condition;
	final Condition condId;
	final AbilityTargetSide side;
	final String param;

	public AbilityCondition(String value) {
		String[] conditionArr = value.split("_");
		if (conditionArr.length < 3)
			throw new RuntimeException("Malformed skill launch condition format: '" + value + '\'');

		side = AbilityTargetSide.valueOf(Integer.valueOf(conditionArr[0]));
		condId = Condition.valueOf(Integer.valueOf(conditionArr[1]));
		param = conditionArr[2];
		condition = createConditionPredicate(side, condId, param);
	}

	public boolean isExclusive(){
		return condId.isExclusive();
	}

	public String getDescription(){

		StringBuilder sb = new StringBuilder();
		sb.append(side.getDescription());
		sb.append(' ');
		sb.append(String.format(condId.getDescriptionFormat(), param));
		return sb.toString();
	}

	private static Predicate<Card> createConditionPredicate(AbilityTargetSide side, AbilityCondition.Condition cond, final String paramStr) {
		int param = Integer.valueOf(paramStr);
		switch (cond) {
			case CardsOnBattlefield:
				switch (side) {
					case Both:
						return (Card c) -> {
							int oppCount = c.owner().opponent().countCardsInField();
							return oppCount >= param || oppCount + c.owner().countCardsInField() >= param;
						};
					case Owner:
						return (Card c) -> c.owner().countCardsInField() >= param;
					case Opponent:
						return (Card c) -> c.owner().opponent().countCardsInField() >= param;
				}
				break;
			case CardsInHand:
				switch (side) {
					case Both:
						return (Card c) -> {
							int oppCount = c.owner().opponent().countCardsInHand();
							return oppCount >= param || oppCount + c.owner().countCardsInHand() >= param;
						};
					case Owner:
						return (Card c) -> c.owner().countCardsInHand() >= param;
					case Opponent:
						return (Card c) -> c.owner().opponent().countCardsInHand() >= param;
				}
				break;
			case HeroHpOver:
				switch (side) {
					case Both:
						//TODO Clarify hp of ANY or BOTH heroes are matter. But it never being used in game likely.
						return (Card c) -> !(c.owner().hpLessThanPercent(param) || c.owner().opponent().hpLessThanPercent(param));
					case Owner:
						return (Card c) -> !c.owner().hpLessThanPercent(param);
					case Opponent:
						return (Card c) -> !c.owner().opponent().hpLessThanPercent(param);
				}
				break;
			case ResistCount:
				switch (side) {
					case Both:
						return (Card c) -> {
							int oppCount = c.owner().opponent().countCardsInField(Card::hasResistance);
							return oppCount >= param || oppCount + c.owner().countCardsInField(Card::hasResistance) >= param;
						};
					case Owner:
						return (Card c) -> c.owner().hasCardsInField(param, Card::hasResistance);
					case Opponent:
						return (Card c) -> c.owner().opponent().hasCardsInField(param, Card::hasResistance);
				}
				break;
		}
		throw new IllegalArgumentException("Unknown Judgement condition " + side + ' ' + cond + ' ' + param);
	}

	public Predicate<Card> getPredicate() {
		return condition;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		AbilityCondition that = (AbilityCondition) o;

		if (condId != that.condId)
			return false;
		if (side != that.side)
			return false;
		return param.equals(that.param);
	}

	@Override
	public int hashCode() {
		int result = condId.hashCode();
		result = 31 * result + side.hashCode();
		result = 31 * result + param.hashCode();
		return result;
	}

	/*
			1 - cards on battlefield >=
			3 - cards in hand count >=
			5 - hero's HP over
			6 - number of the cards with skill Resistance
			8 - If specified cards are on the battlefield all together

			'TODO Recheck again. Resistance Judgement' always activates second skill
			'HP Judgement' doesn't activates second skill if first was activated
			*/
	private enum Condition {

		CardsOnBattlefield(1, "have %s or more cards on battlefield", true),
		CardsInHand(3, "have %s or more cards in hand", true),
		HeroHpOver(5, "hero HP is greater %s%%", true),
		ResistCount(6, "have %s or more cards with 'Resistance' on battlefield", false);

		private final int     id;
		private final String  descriptionFormat;
		private final boolean isExclusive;

		Condition(int id, String descriptionFormat, boolean isExclusive) {
			this.id = id;
			this.descriptionFormat = descriptionFormat;
			this.isExclusive = isExclusive;
		}

		public static Condition valueOf(int id) {
			for (Condition item : values()) {
				if (item.id == id)
					return item;
			}
			throw new IllegalArgumentException("Unknown skill condition id=" + id);
		}

		public String getDescriptionFormat() {
			return descriptionFormat;
		}

		public boolean isExclusive() {
			return isExclusive;
		}
	}

	/**
	 *
	 */
	private enum AbilityTargetSide {
		Both(1000, "Card owner and opponent both"),
		Owner(1001, "Card owner"),
		Opponent(1002, "Opponent");

		private final int id;
		private final String description;

		AbilityTargetSide(int id, String description) {
			this.id = id;
			this.description = description;
		}

		public static AbilityTargetSide valueOf(int id) {
			for (AbilityTargetSide targetSide : AbilityTargetSide.values()) {
				if (targetSide.id == id)
					return targetSide;
			}
			throw new IllegalArgumentException("Unknown Judgement condition target id=" + id);
		}

		public String getDescription() {
			return description;
		}
	}
}
