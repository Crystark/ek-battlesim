package crystark.ek.battlesim.ability;

import crystark.ek.battlesim.battle.Card;

public interface IDeathWhisperAble extends IAbility {

	public void apply(Card sourceCard);

	public boolean isDeathWhisper();

}
