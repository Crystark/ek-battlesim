package crystark.ek.battlesim.ability;

import crystark.ek.battlesim.battle.AbilitiesVisitor;

public interface IPower extends IQuickstrikeAble, IDesperationAble, IPreemptiveStrikeAble, IDeathWhisperAble, IAbility {

	@Override
	default void accept(AbilitiesVisitor visitor) {
		visitor.visit(this);
	}
}
