package crystark.ek.battlesim.ability;

import crystark.ek.battlesim.battle.AbilitiesVisitor;
import crystark.ek.battlesim.battle.Card;

public interface IAbility {
	String getName();

	boolean canBeLaunched(Card owner);

	default void accept(AbilitiesVisitor visitor){
		throw new IllegalArgumentException("Unclassified ability " + this.getName());
	}
}
