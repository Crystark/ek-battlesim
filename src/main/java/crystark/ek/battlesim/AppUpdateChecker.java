package crystark.ek.battlesim;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;

import crystark.common.events.Warnings;
import crystark.ek.Constants;
import rx.Observable;
import rx.exceptions.OnErrorThrowable;
import rx.schedulers.Schedulers;

public class AppUpdateChecker {
	static final Pattern		P	= Pattern.compile("latest version \\(([0-9]+\\.[0-9]+\\.[0-9A-z]+) ");
	static final OkHttpClient	C	= new OkHttpClient();
	final CountDownLatch		cdl	= new CountDownLatch(1);
	final Observable<String>	request;

	AppUpdateChecker() {
		request = Observable
			.just("https://bitbucket.org/api/1.0/repositories/Crystark/ek-battlesim/wiki/Home")
			.subscribeOn(Schedulers.io())
			.map(url -> new Request.Builder()
				.url(url)
				.build())
			.map(rq -> {
				try {
					return C.newCall(rq).execute().body().string();
				}
				catch (IOException e) {
					throw OnErrorThrowable.from(e);
				}
			})
			.map(AppUpdateChecker::extractVersion)
			.cache();

		request
			.subscribe(
				v -> {
					if (v != null && !v.equals(Constants.VERSION)) {
						Warnings.send("You're not using the latest sim version. You can get it at https://bitbucket.org/Crystark/ek-battlesim/wiki/Home");
					}
					cdl.countDown();
				} ,
				t -> cdl.countDown());
	}

	public static AppUpdateChecker run() {
		return new AppUpdateChecker();
	}

	static String extractVersion(String str) {
		Matcher matcher = P.matcher(str);
		if (matcher.find()) {
			return matcher.group(1);
		}
		return null;
	}
}
