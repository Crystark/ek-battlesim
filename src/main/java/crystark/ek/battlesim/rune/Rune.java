package crystark.ek.battlesim.rune;

import crystark.common.api.RuneDesc;
import crystark.ek.Constants;
import crystark.ek.battlesim.ability.IOnRuneTurn;
import crystark.ek.battlesim.battle.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Rune {
	private static final Logger	L			= LoggerFactory.getLogger(Rune.class);
	protected final RuneDesc    desc;
	protected final IOnRuneTurn ability;

	protected int				usedTimes	= 0;

	//	public Rune(RuneDesc desc, String abilityName) {
	//		this(desc, AbilityDescFormat.parse(abilityName + ":" + desc.value));
	//	}

	//	public Rune(RuneDesc desc, AbilityDesc ab) {
	//		this(desc, getRuneAble(ab));
	//	}

	public Rune(RuneDesc desc, IOnRuneTurn ability) {
		this.desc = desc;
		this.ability = ability;
	}

	protected abstract boolean conditionsAreMet(int round, Player player);

	public final boolean use(int round, Player player) {
		if (!this.allUsed() && this.conditionsAreMet(round, player)) {
			this.usedTimes++;
			if (Constants.IS_DEBUG)
				L.debug("Rune " + desc.name + " is activating " + usedTimes + " time of " + desc.times);
			this.ability.onRuneTurn(player);
			return true;
		}
		return false;
	}

	protected boolean allUsed() {
		return this.usedTimes >= desc.times;
	}

	public void charge() {
		this.usedTimes--;
	}

	int remaining() {
		return desc.times - usedTimes;
	}

	//	protected static IOnRuneTurn getRuneAble(AbilityDesc desc) {
	//		return (IOnRuneTurn) AbilityFactory.get(desc);
	//	}

	@Override
	public String toString() {
		return desc.toString();
	}
}
