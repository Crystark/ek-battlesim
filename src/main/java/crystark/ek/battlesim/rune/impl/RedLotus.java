package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Healing;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class RedLotus extends Rune {
	public static final String NAME = "red lotus";

	public RedLotus(RuneDesc desc) {
		super(desc, new Healing((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.opponent().hasCardsInField(2, CardPredicates.IS_FOREST); // Verified
	}
}
