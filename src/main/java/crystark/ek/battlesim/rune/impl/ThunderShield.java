package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Retaliation;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.AbilityProvidingRune;

public class ThunderShield extends AbilityProvidingRune {
	public static final String NAME = "thunder shield";

	public ThunderShield(RuneDesc desc) {
		super(desc, () -> new Retaliation((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hasCardsInField(2, CardPredicates.IS_FOREST);
	}
}
