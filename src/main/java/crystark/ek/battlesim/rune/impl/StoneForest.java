package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Reflection;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.AbilityProvidingRune;

public class StoneForest extends AbilityProvidingRune {
	public static final String NAME = "stone forest";

	public StoneForest(RuneDesc desc) {
		super(desc, () -> new Reflection((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hasCardInField() && player.hasCardsInCemetery(2, CardPredicates.IS_SWAMP); // Verified
	}
}
