package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Craze;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.AbilityProvidingRune;

public class Tsunami extends AbilityProvidingRune {
	public static final String NAME = "tsunami";

	public Tsunami(RuneDesc desc) {
		super(desc, () -> new Craze((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hpLessThanPercent(50) && player.hasCardInField(); // Verified
	}
}
