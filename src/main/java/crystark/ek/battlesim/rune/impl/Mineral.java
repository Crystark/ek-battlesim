package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Shield;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class Mineral extends Rune {
	public static final String NAME = "mineral";

	public Mineral(RuneDesc desc) {
		super(desc, new Shield((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.opponent().hasCardsInHand(2) && player.hasCardInField(); // Verified
	}
}
