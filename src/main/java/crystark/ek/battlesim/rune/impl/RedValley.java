package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Bloodsucker;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.AbilityProvidingRune;

public class RedValley extends AbilityProvidingRune {
	public static final String NAME = "red valley";

	public RedValley(RuneDesc desc) {
		super(desc, () -> new Bloodsucker((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hasCardsInField(2, CardPredicates.IS_SWAMP);
	}
}
