package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Damnation;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class BurningSoul extends Rune {
	public static final String NAME = "burning soul";

	public BurningSoul(RuneDesc desc) {
		super(desc, new Damnation((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hasCardsInField(2, CardPredicates.IS_MOUNTAIN); // Verified
	}
}
