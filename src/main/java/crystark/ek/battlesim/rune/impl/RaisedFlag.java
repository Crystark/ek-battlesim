package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Blitz;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.AbilityProvidingRune;

public class RaisedFlag extends AbilityProvidingRune {
	public static final String NAME = "raised flag";

	public RaisedFlag(RuneDesc desc) {
		super(desc, () -> new Blitz((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hasCardsInField(2, CardPredicates.IS_FOREST);
	}
}
