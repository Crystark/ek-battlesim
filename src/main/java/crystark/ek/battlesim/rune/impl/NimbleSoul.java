package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Dodge;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.AbilityProvidingRune;

public class NimbleSoul extends AbilityProvidingRune {
	public static final String NAME = "nimble soul";

	public NimbleSoul(RuneDesc desc) {
		super(desc, () -> new Dodge((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hasCardInField() && player.hasCardsInCemetery(3, CardPredicates.IS_FOREST); // Verified
	}
}
