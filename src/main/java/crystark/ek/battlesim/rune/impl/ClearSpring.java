package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Regeneration;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class ClearSpring extends Rune {
	public static final String NAME = "clear spring";

	public ClearSpring(RuneDesc desc) {
		super(desc, new Regeneration((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hasCardsInField(2, CardPredicates.IS_TUNDRA); // Verified
	}
}
