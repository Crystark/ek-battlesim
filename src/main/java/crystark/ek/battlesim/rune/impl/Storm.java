package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.ManaCorruption;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class Storm extends Rune {
	public static final String NAME = "storm";

	public Storm(RuneDesc desc) {
		super(desc, new ManaCorruption((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hasCardsInField(2, CardPredicates.IS_TUNDRA);
	}
}
