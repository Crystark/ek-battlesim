package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.NovaFrost;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class Avalanche extends Rune {
	public static final String NAME = "avalanche";

	public Avalanche(RuneDesc desc) {
		super(desc, new NovaFrost((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.opponent().hasCardsInField(2, CardPredicates.IS_MOUNTAIN);
	}
}
