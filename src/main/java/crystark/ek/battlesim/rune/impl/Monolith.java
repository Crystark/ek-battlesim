package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Resistance;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.AbilityProvidingRune;

public class Monolith extends AbilityProvidingRune {
	public static final String NAME = "monolith";

	public Monolith(RuneDesc desc) {
		super(desc, Resistance::new);
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hasCardInField() && player.hasCardsInField(2, CardPredicates.IS_MOUNTAIN);
	}
}
