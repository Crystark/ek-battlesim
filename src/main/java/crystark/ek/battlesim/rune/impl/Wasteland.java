package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Venom;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class Wasteland extends Rune {
	public static final String NAME = "wasteland";

	public Wasteland(RuneDesc desc) {
		super(desc, new Venom((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hpLessThanPercent(60) && player.opponent().hasCardInField();
	}
}
