package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.MagicShield;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.AbilityProvidingRune;

public class IceWall extends AbilityProvidingRune {
	public static final String NAME = "ice wall";

	public IceWall(RuneDesc desc) {
		super(desc, () -> new MagicShield((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.opponent().hasCardsInField(2, CardPredicates.IS_MOUNTAIN) && player.hasCardInField();
	}
}
