package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Concentration;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.AbilityProvidingRune;

public class FrostBite extends AbilityProvidingRune {
	public static final String NAME = "frost bite";

	public FrostBite(RuneDesc desc) {
		super(desc, () -> new Concentration((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hasCardInField() && player.hasCardsInCemetery(4, CardPredicates.IS_TUNDRA);
	}
}
