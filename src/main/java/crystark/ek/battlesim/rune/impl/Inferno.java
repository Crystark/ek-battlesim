package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Firestorm;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class Inferno extends Rune {
	public static final String NAME = "inferno";

	public Inferno(RuneDesc desc) {
		super(desc, new Firestorm((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.opponent().hasCardInField() && player.hasCardsInCemetery(2, CardPredicates.IS_MOUNTAIN);
	}
}
