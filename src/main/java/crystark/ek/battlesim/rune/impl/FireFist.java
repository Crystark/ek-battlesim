package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Fireball;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class FireFist extends Rune {
	public static final String NAME = "fire fist";

	public FireFist(RuneDesc desc) {
		super(desc, new Fireball((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.opponent().hasCardsInField(3);
	}
}
