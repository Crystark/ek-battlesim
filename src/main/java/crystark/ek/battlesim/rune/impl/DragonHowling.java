package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Bless;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class DragonHowling extends Rune {
	public static final String NAME = "dragon howling";

	public DragonHowling(RuneDesc desc) {
		super(desc, new Bless((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hpLessThanPercent(60);
	}
}
