package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.FireWall;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class HeatWave extends Rune {
	public static final String NAME = "heat wave";

	public HeatWave(RuneDesc desc) {
		super(desc, new FireWall((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hpLessThanPercent(60) && player.opponent().hasCardInField();
	}
}
