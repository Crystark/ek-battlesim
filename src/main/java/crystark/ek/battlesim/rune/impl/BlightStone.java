package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Warcry;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class BlightStone extends Rune {
	public static final String NAME = "blight stone";

	public BlightStone(RuneDesc desc) {
		super(desc, new Warcry((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.opponent().countCardsInField() > player.countCardsInField() + 1; // Verified
	}
}
