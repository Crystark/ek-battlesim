package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.FireWall;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class Eruption extends Rune {
	public static final String NAME = "eruption";

	public Eruption(RuneDesc desc) {
		super(desc, new FireWall((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hpLessThanPercent(50) && player.opponent().hasCardInField();
	}
}
