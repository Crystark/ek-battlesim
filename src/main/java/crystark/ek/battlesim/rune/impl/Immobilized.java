package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.WaterShield;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.AbilityProvidingRune;

public class Immobilized extends AbilityProvidingRune {
	public static final String NAME = "immobilized";

	public Immobilized(RuneDesc desc) {
		super(desc, () -> new WaterShield((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hpLessThanPercent(60);
	}
}
