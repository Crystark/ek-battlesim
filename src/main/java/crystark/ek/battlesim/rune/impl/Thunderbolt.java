package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class Thunderbolt extends Rune {
	public static final String NAME = "thunderbolt";

	public Thunderbolt(RuneDesc desc) {
		super(desc, new crystark.ek.battlesim.ability.impl.Thunderbolt((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		Player opponent = player.opponent();
		return opponent.hasCardInField() && opponent.hasCardsInCemetery(2, CardPredicates.IS_SWAMP);
	}
}
