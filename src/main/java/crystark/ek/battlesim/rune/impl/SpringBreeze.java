package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Barricade;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class SpringBreeze extends Rune {
	public static final String NAME = "spring breeze";

	public SpringBreeze(RuneDesc desc) {
		super(desc, new Barricade((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hasCardInField() && player.hasCardsInHand(2, CardPredicates.IS_FOREST); // Verified
	}
}
