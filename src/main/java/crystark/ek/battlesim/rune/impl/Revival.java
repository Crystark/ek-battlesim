package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.GroupMorale;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class Revival extends Rune {
	public static final String NAME = "revival";

	public Revival(RuneDesc desc) {
		super(desc, new GroupMorale((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hasCardInField() && player.hasCardsInCemetery(2, CardPredicates.IS_FOREST); // Verified
	}
}
