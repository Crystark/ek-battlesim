package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Infiltrator;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.AbilityProvidingRune;

public class Eagle extends AbilityProvidingRune {
	public static final String NAME = "eagle";

	public Eagle(RuneDesc desc) {
		super(desc, Infiltrator::new);
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hasCardsInField(3, CardPredicates.IS_TUNDRA);
	}
}
