package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Venom;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class Arsenopyrite extends Rune {
	public static final String NAME = "arsenopyrite";

	public Arsenopyrite(RuneDesc desc) {
		super(desc, new Venom((Integer) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return round > 12 && player.opponent().hasCardInField();
	}
}
