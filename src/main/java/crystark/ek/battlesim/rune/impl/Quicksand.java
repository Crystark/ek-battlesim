package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Plague;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class Quicksand extends Rune {
	public static final String NAME = "quicksand";

	public Quicksand(RuneDesc desc) {
		super(desc, new Plague((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.opponent().hasCardsInField(2, CardPredicates.IS_TUNDRA);
	}
}
