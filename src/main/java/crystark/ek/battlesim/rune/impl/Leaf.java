package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Snipe;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class Leaf extends Rune {
	public static final String NAME = "leaf";

	public Leaf(RuneDesc desc) {
		super(desc, new Snipe((Integer) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return round > 14 && player.opponent().hasCardInField();
	}
}
