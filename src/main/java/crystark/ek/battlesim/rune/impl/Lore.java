package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Warpath;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.AbilityProvidingRune;

public class Lore extends AbilityProvidingRune {
	public static final String NAME = "lore";

	public Lore(RuneDesc desc) {
		super(desc, () -> new Warpath((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hasCardInField() && player.hasCardsInCemetery(3, CardPredicates.IS_MOUNTAIN);
	}
}
