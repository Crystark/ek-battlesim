package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Bloodthirsty;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.AbilityProvidingRune;

public class Transparency extends AbilityProvidingRune {
	public static final String NAME = "transparency";

	public Transparency(RuneDesc desc) {
		super(desc, () -> new Bloodthirsty((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hasCardInField() && player.opponent().hasCardsInField(2, CardPredicates.IS_SWAMP);
	}
}
