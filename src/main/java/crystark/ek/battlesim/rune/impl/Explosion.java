package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.SelfDestruct;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.AbilityProvidingRune;

public class Explosion extends AbilityProvidingRune {
	public static final String NAME = "explosion";

	public Explosion(RuneDesc desc) {
		super(desc, () -> new SelfDestruct((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hasCardInField() && player.opponent().hasCardsInField(2, CardPredicates.IS_FOREST);
	}
}
