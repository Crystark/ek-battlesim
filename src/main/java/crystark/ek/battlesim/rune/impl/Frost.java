package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Iceball;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class Frost extends Rune {
	public static final String NAME = "frost";

	public Frost(RuneDesc desc) {
		super(desc, new Iceball((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hasCardInCemetery() && player.opponent().hasCardInField();
	}
}
