package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.ChainLightning;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class Tornado extends Rune {
	public static final String NAME = "tornado";

	public Tornado(RuneDesc desc) {
		super(desc, new ChainLightning((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hpLessThanPercent(60) && player.opponent().hasCardInField();
	}
}
