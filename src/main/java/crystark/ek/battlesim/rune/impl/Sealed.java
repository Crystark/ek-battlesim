package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.FieldOfSilence;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.AbilityProvidingRune;

public class Sealed extends AbilityProvidingRune {
	public static final String NAME = "sealed";

	public Sealed(RuneDesc desc) {
		super(desc, FieldOfSilence::new);
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hasCardInField() && player.hasCardsInCemetery(3, CardPredicates.IS_MOUNTAIN);
	}
}
