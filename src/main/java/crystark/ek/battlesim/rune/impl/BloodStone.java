package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Rejuvenation;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.AbilityProvidingRune;

public class BloodStone extends AbilityProvidingRune {
	public static final String NAME = "blood stone";

	public BloodStone(RuneDesc desc) {
		super(desc, () -> new Rejuvenation((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hasCardsInField(2, CardPredicates.IS_MOUNTAIN);
	}
}
