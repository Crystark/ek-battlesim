package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Evasion;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.AbilityProvidingRune;

public class GhostStep extends AbilityProvidingRune {
	public static final String NAME = "ghost step";

	public GhostStep(RuneDesc desc) {
		super(desc, Evasion::new);
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hasCardsInField(3, CardPredicates.IS_MOUNTAIN);
	}
}
