package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.IceShield;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.AbilityProvidingRune;

public class ArcticFreeze extends AbilityProvidingRune {
	public static final String NAME = "arctic freeze";

	public ArcticFreeze(RuneDesc desc) {
		super(desc, () -> new IceShield((Integer) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hasCardInField() && player.hasCardsInCemetery(3, CardPredicates.IS_TUNDRA);
	}
}
