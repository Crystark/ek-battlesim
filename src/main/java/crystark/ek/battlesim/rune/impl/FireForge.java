package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Counterattack;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.AbilityProvidingRune;

public class FireForge extends AbilityProvidingRune {
	public static final String NAME = "fire forge";

	public FireForge(RuneDesc desc) {
		super(desc, () -> new Counterattack((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hasCardInField() && player.hasCardsInCemetery(2, CardPredicates.IS_MOUNTAIN);
	}
}
