package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Purification;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class DivinePlea extends Rune {
	public static final String NAME = "divine plea";

	public DivinePlea(RuneDesc desc) {
		super(desc, new Purification());
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hasCardsInField(3, CardPredicates.IS_FOREST); // Verified
	}
}
