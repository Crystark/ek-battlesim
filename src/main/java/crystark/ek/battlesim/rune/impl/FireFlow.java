package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Fireball;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class FireFlow extends Rune {
	public static final String NAME = "fire flow";

	public FireFlow(RuneDesc desc) {
		super(desc, new Fireball((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return round > 12 && player.opponent().hasCardInField();
	}
}
