package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.NovaFrost;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class Coldwave extends Rune {
	public static final String NAME = "coldwave";

	public Coldwave(RuneDesc desc) {
		super(desc, new NovaFrost((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.countCardsInDeck() < 2 && player.opponent().hasCardInField();
	}
}
