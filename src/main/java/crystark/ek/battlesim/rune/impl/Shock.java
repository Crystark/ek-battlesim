package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Thunderbolt;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class Shock extends Rune {
	public static final String NAME = "shock";

	public Shock(RuneDesc desc) {
		super(desc, new Thunderbolt((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		Player opponent = player.opponent();
		return opponent.hasCardInField() && opponent.hasCardsInCemetery(3);
	}
}
