package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Iceball;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class Icicle extends Rune {
	public static final String NAME = "icicle";

	public Icicle(RuneDesc desc) {
		super(desc, new Iceball((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return round > 14 && player.opponent().hasCardInField();
	}
}
