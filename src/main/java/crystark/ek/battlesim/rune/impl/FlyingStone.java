package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Snipe;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.AbilityProvidingRune;

public class FlyingStone extends AbilityProvidingRune {
	public static final String NAME = "flying stone";

	public FlyingStone(RuneDesc desc) {
		super(desc, () -> new Snipe((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hasCardInField() && player.hasCardsInCemetery(3, CardPredicates.IS_SWAMP);
	}
}
