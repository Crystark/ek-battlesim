package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Parry;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.AbilityProvidingRune;

public class Stonewall extends AbilityProvidingRune {
	public static final String NAME = "stonewall";

	public Stonewall(RuneDesc desc) {
		super(desc, () -> new Parry((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hasCardsInField(2, CardPredicates.IS_SWAMP);
	}
}
