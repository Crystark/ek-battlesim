package crystark.ek.battlesim.rune.impl;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Prayer;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class HolyWell extends Rune {
	public static final String NAME = "holy well";

	public HolyWell(RuneDesc desc) {
		super(desc, new Prayer((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.opponent().hasCardsInCemetery(2, CardPredicates.IS_MOUNTAIN); // Verified
	}
}
