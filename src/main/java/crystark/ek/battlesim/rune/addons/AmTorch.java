package crystark.ek.battlesim.rune.addons;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.FireWall;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

/**
 *
 */
public class AmTorch extends Rune {
	public static final String NAME = "torch";

	public AmTorch(RuneDesc desc) {
		super(desc, new FireWall((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hasCardsInField(2, CardPredicates.IS_FOREST);
	}
}