package crystark.ek.battlesim.rune.addons;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.DyingStrike;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.AbilityProvidingRune;

/**
 *
 */
public class AmTheLastStand  extends AbilityProvidingRune {
	public static final String NAME = "the last stand";
	public AmTheLastStand(RuneDesc desc) {
		super(desc, () -> new DyingStrike((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.opponent().countCardsInField() > player.countCardsInField() + 1;
	}
}
