package crystark.ek.battlesim.rune.addons;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.addons.SuperMagicErosion;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class AmStorm extends Rune {
	public static final String NAME = "am storm";

	public AmStorm(RuneDesc desc) {
		super(desc, new SuperMagicErosion((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hasCardsInField(2, CardPredicates.IS_TUNDRA);
	}
}
