package crystark.ek.battlesim.rune.addons;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Snipe;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class AmEnvol extends Rune {
	public static final String NAME = "am envol";

	public AmEnvol(RuneDesc desc) {
		super(desc, new Snipe((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.opponent().hasCardInField() && player.hasCardsInCemetery(2, CardPredicates.IS_FOREST);
	}
}
