package crystark.ek.battlesim.rune.addons;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Rejuvenation;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.AbilityProvidingRune;

public class AmRenaissance extends AbilityProvidingRune {
	public static final String NAME = "am renaissance";

	public AmRenaissance(RuneDesc desc) {
		super(desc, () -> new Rejuvenation((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.opponent().hasCardsInCemetery(2, CardPredicates.IS_SWAMP);
	}
}
