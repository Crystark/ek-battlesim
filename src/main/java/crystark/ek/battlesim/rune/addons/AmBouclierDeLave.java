package crystark.ek.battlesim.rune.addons;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.MagicShield;
import crystark.ek.battlesim.battle.CardPredicates;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.AbilityProvidingRune;

public class AmBouclierDeLave extends AbilityProvidingRune {
	public static final String NAME = "am bouclier de lave";

	public AmBouclierDeLave(RuneDesc desc) {
		super(desc, () -> new MagicShield((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hasCardsInField(2, CardPredicates.IS_MOUNTAIN);
	}
}
