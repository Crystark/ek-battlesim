package crystark.ek.battlesim.rune.addons;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Craze;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.AbilityProvidingRune;

public class AmTsunami extends AbilityProvidingRune {
	public static final String NAME = "am tsunami";

	public AmTsunami(RuneDesc desc) {
		super(desc, () -> new Craze((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hpLessThanPercent(40) && player.hasCardInField();
	}
}
