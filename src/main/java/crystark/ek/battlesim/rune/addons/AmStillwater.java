package crystark.ek.battlesim.rune.addons;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.WaterShield;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.AbilityProvidingRune;

public class AmStillwater extends AbilityProvidingRune {
	public static final String NAME = "stillwater";

	public AmStillwater(RuneDesc desc) {
		super(desc, () -> new WaterShield((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hpLessThanPercent(40);
	}
}
