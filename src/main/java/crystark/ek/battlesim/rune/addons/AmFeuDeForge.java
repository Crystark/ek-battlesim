package crystark.ek.battlesim.rune.addons;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.GroupMorale;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class AmFeuDeForge extends Rune {
	public static final String NAME = "am feu de forge";

	public AmFeuDeForge(RuneDesc desc) {
		super(desc, new GroupMorale((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.hpLessThanPercent(50) && player.hasCardInField();
	}
}
