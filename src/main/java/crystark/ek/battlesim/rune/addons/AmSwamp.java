package crystark.ek.battlesim.rune.addons;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.impl.Smog;
import crystark.ek.battlesim.battle.Player;
import crystark.ek.battlesim.rune.Rune;

public class AmSwamp extends Rune {
	public static final String NAME = "swamp";

	public AmSwamp(RuneDesc desc) {
		super(desc, new Smog((int) desc.value));
	}

	@Override
	protected boolean conditionsAreMet(int round, Player player) {
		return player.opponent().hasCardsInField(2);
	}
}
