package crystark.ek.battlesim.rune;

import java.util.function.Supplier;

import crystark.common.api.RuneDesc;
import crystark.ek.battlesim.ability.IAbility;
import crystark.ek.battlesim.ability.addons.AbilityProvider;

public abstract class AbilityProvidingRune extends Rune {
	protected AbilityProvidingRune(RuneDesc desc, Supplier<IAbility> ab) {
		super(desc, new AbilityProvider(ab));
	}
}
