package crystark.ek.battlesim.rune;

import crystark.common.api.NamesConverter;
import crystark.common.api.RuneDesc;
import crystark.common.db.Database;
import crystark.common.db.model.BaseRune;
import crystark.ek.EkbsConfig;
import crystark.ek.battlesim.task.TaskException;
import rx.Observable;

import java.io.PrintStream;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class RuneFactory {


	private final NamesConverter namesConverter;

	public static Rune get(String name, int times) {
		return get(name, times, null);
	}

	public static Rune get(String name, int times, Object value) {
		return get(new RuneDesc(name, times, value));
	}

	public static Rune get(RuneDesc r) {
		return EkbsConfig.current().runeFactory().create(r);
	}

	public static boolean exists(RuneDesc r) {
		return EkbsConfig.current().runeFactory().exists(r.name);
	}

	public static List<RuneDesc> getRuneDesc(String... names) {
		return StreamSupport.stream(Arrays.spliterator(names), false)
			.map(name -> Database.get().runes().get(name).createRuneDesc())
			.collect(Collectors.toList());
	}

	static boolean isBaseRune(Class<? extends Rune> clazz) {
		return !Modifier.isAbstract(clazz.getModifiers()) && clazz.getEnclosingClass() == null;
	}

	public static Observable<BaseRune> supportedRunes(boolean keep) {
		return Database.get().runes()
			.all()
			.filter(r -> {
				try {
					return (!r.isBase() || (RuneFactory.get(r.createRuneDesc()) != null)) == keep;
				}
				catch (TaskException e) {
					return !keep;
				}
			});
	}

	public static void printNames(PrintStream ps) {
		ArrayList<String> list = EkbsConfig.current().runeFactory().enlistSupportedRunes();
		Collections.sort(list);

		ps.println("# Supported runes");
		ps.println("#");
		ps.println();
		for (String string : list) {
			ps.println(string);
		}
	}

	public RuneFactory(NamesConverter namesConverter){

		this.namesConverter = namesConverter;
	}

	protected boolean exists(String name) {
		name = namesConverter.runeFromAlias(name);
		return Runes.MAPPING.containsKey(name.toLowerCase());
	}

	protected Rune create(RuneDesc r){
		String name = namesConverter.runeFromAlias(r.name);
		Function<RuneDesc, Rune> factory = Runes.MAPPING.get(name.toLowerCase());
		if (factory == null) {
			throw new TaskException("Unknown rune " + r);
		}
		return factory.apply(r);
	}

	protected ArrayList<String> enlistSupportedRunes(){
		final ArrayList<String> list = new ArrayList<>(Runes.MAPPING.keySet().size());
		/*for (String mrName : Runes.MAPPING.keySet()) {
			final String inGame = namesConverter.runeToAlias(mrName);
			list.add(inGame);
		}*/

		for (String mrName : Runes.MAPPING.keySet()) {
			final String inGame = namesConverter.runeToAlias(mrName);
			list.add(inGame);
		}
		return list;
	}
}
