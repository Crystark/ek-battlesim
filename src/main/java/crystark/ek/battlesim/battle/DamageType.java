package crystark.ek.battlesim.battle;

public enum DamageType {
	attack, counter, fire, ice, lightning, blood, unavoidable, poison, burn, focused
}
