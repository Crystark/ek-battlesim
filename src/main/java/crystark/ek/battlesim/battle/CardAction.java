package crystark.ek.battlesim.battle;

import crystark.ek.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CardAction implements Damageable {
	private static final Logger	L	= LoggerFactory.getLogger(CardAction.class);

	public final Card			card;
	int							iter;

	public CardAction(Card card) {
		this.card = card;
		this.iter = card.iterOnField();
	}

	private boolean check() {
		return card.isAlive() && card.isIterOnField(this.iter);
	}

	@Override
	public int damaged(Card damagingCard, int damage, DamageType type) {
		if (check()) {
			return card.damaged(damagingCard, damage, type);
		}
		else {
			if (Constants.IS_DEBUG) {
				L.debug(card + " not damaged cause already removed from field.");
			}
			return 0;
		}
	}
}
