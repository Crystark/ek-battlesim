package crystark.ek.battlesim.battle;

public interface HasAtk {

	void addMalusAtk(int modifier);

	void removeMalusAtk(int modifier);

	void addBonusAtk(int modifier);

	void removeBonusAtk(int modifier);

	int getCurrentBaseAtk();

	void modifyAtk(int modifier);

	int getCurrentAtk();
}
