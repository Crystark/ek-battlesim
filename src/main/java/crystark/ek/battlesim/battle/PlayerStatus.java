package crystark.ek.battlesim.battle;

import java.util.ArrayList;
import java.util.List;

public class PlayerStatus {
	private List<Card>	hasSoulImprison	= new ArrayList<>(8);
	private List<Card>	hasSuppressing	= new ArrayList<>(8);

	void cardAddedToField(Card card) {
		if (card.hasSoulImprison()) {
			this.hasSoulImprison.add(card);
		}
		if (card.hasSuppressing()) {
			this.hasSuppressing.add(card);
		}
	}

	void cardRemovedFromField(Card card) {
		this.hasSoulImprison.remove(card);
		this.hasSuppressing.remove(card);
	}

	boolean hasSoulImprison() {
		if (!this.hasSoulImprison.isEmpty()) {
			for (Card card : hasSoulImprison) {
				if (card.hasSoulImprison())
					return true;
			}
		}
		return false;
	}

	boolean hasSuppressing() {
		if (!this.hasSuppressing.isEmpty()) {
			for (Card card : hasSuppressing) {
				if (card.hasSuppressing())
					return true;
			}
		}
		return false;
	}
}