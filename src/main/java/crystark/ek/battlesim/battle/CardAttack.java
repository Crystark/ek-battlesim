package crystark.ek.battlesim.battle;

public class CardAttack {
	public final Card		cardToAttack;
	public final Integer	damage;

	public CardAttack(Card cardToAttack, Integer damage) {
		this.cardToAttack = cardToAttack;
		this.damage = damage;
	}
}
