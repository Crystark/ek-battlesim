package crystark.ek.battlesim.battle;

import crystark.common.api.BattleDesc;
import crystark.common.api.DeckDesc;
import crystark.ek.Constants;
import crystark.ek.Helper;
import crystark.tools.Tools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Battle {
	private static final Logger	L			= LoggerFactory.getLogger(Battle.class);
	public static final int		MAX_ROUND	= 500;

	public final BattleDesc		desc;

	private Player				attacker;
	private Player				defender;
	private int					round		= 0;

	private DeckDesc			attackerSnapshot;
	private DeckDesc			defenderSnapshot;

	protected Battle(BattleDesc desc) {
		this.desc = desc;
	}

	public static Battle run(BattleDesc battleDesc) {
		Battle battle = new Battle(battleDesc);
		battle.fight();
		return battle;
	}

	public static Battle run(DeckDesc attacker, DeckDesc defender) {
		return run(new BattleDesc(attacker, defender));
	}

	public BattleDesc nextIfWon(DeckDesc nextOpponent) {
		if (!this.attackerWon()) {
			return null;
		}
		return new BattleDesc(attackerSnapshot(), nextOpponent);
	}

	static int getExhaustionDamage(int round) {
		return 50 + 30 * (round - 51);
	}

	public boolean attackerWon() {
		return this.attacker.isAlive();
	}

	public boolean defenderWon() {
		return this.defender.isAlive();
	}

	public int getRound() {
		return round;
	}

	public DeckDesc attackerSnapshot() {
		if (attackerSnapshot == null) {
			attackerSnapshot = attacker.snapshotHpAndCards();
		}
		return attackerSnapshot;
	}

	public DeckDesc defenderSnapshot() {
		if (defenderSnapshot == null) {
			defenderSnapshot = defender.snapshotHpAndCards();
		}
		return defenderSnapshot;
	}

	private void fight() {
		this.attacker = Player.from(desc.attacker);
		this.defender = Player.from(desc.defender);
		Player.salute(this.attacker, this.defender);

		this.round = 0;
		boolean isAttackerRound = Tools.compareToValue(compareInit(this.attacker.desc, this.defender.desc), true, false, Helper.headsOrTails());

		if (Constants.IS_DEBUG)
			L.debug((isAttackerRound ? attacker : defender) + " starts !");

		while (this.attacker.isAlive() && this.defender.isAlive() && this.round < MAX_ROUND) {
			round++;

			try {
				if (Constants.IS_DEBUG) {
					L.debug("--- Round " + round + " started. " + (isAttackerRound ? attacker : defender) + "'s turn ---");
					L.debug(defender + " cemetery: " + defender.printCemetery());
					L.debug(defender + " deck    : " + defender.printDeck());
					L.debug(defender + " hand    : " + defender.printHand());
					L.debug(defender + " field   : " + defender.printField());
					L.debug(attacker + " field   : " + attacker.printField());
					L.debug(attacker + " hand    : " + attacker.printHand());
					L.debug(attacker + " deck    : " + attacker.printDeck());
					L.debug(attacker + " cemetery: " + attacker.printCemetery());
				}

				this.attacker.newRound();
				this.defender.newRound();

				Player p = isAttackerRound ? this.attacker : this.defender;
				Player o = isAttackerRound ? this.defender : this.attacker;

				// At round 51, the players start taking unavoidable damage.
				if (round > 50 && !p.desc.infiniteHp) {
					if (Constants.IS_DEBUG)
						L.debug("Round " + round + " ! " + p + " suffers from battle exhaustion!");
					p.modifyHp(-getExhaustionDamage(round));

					if (!p.isAlive())
						break;
				}

				// When the turn starts, rune buffs are removed
				p.removeBuffs();
				p.playCardsFromDeck();
				p.playCardsFromHand();

				// Clean dead cards
				o.cleanup();
				p.cleanup();

				// Check here because of obstinacy.
				if (!p.isAlive())
					break;

				p.handleRunes(this.round);
				// Clean dead cards due to runes
				o.cleanup();
				p.cleanup();

				p.preemptiveStrikes();
				// Clean dead cards due to preemptive strikes
				o.cleanup();
				p.cleanup();

				p.playCardsFromField();
				o.cleanup();
				p.cleanup();

				p.deathWhispers();
				// Clean dead cards due to death whisper
				o.cleanup();
				p.cleanup();

				isAttackerRound = !isAttackerRound;
			}
			catch (Throwable t) {
				L.error("Error @ round " + round + ", " + (isAttackerRound ? attacker : defender) + "'s turn ---");
				L.error(defender + " cemetery: " + defender.printCemetery());
				L.error(defender + " deck    : " + defender.printDeck());
				L.error(defender + " hand    : " + defender.printHand());
				L.error(defender + " field   : " + defender.printField());
				L.error(attacker + " field   : " + attacker.printField());
				L.error(attacker + " hand    : " + attacker.printHand());
				L.error(attacker + " deck    : " + attacker.printDeck());
				L.error(attacker + " cemetery: " + attacker.printCemetery());
				throw new BattleException("Error at round " + round, t);
			}
		}
		if (Constants.IS_DEBUG) {
			Player looser = this.attacker.isAlive() ? defender : attacker;
			L.debug(looser + " lost from " + (looser.hasRemainingHp() ? "out of cards." : "out of HP."));
		}
	}

	public int attackerHealth() {
		return attacker.getCurrentHp();
	}

	public int defenderHealth() {
		return defender.getCurrentHp();
	}

	public static int compareInit(DeckDesc p1, DeckDesc p2) {
		if (p1.alwaysStart != p2.alwaysStart) {
			return p1.alwaysStart ? Integer.MAX_VALUE : Integer.MIN_VALUE;
		}

		return p1.getTotalPower() - p2.getTotalPower();
	}
}
