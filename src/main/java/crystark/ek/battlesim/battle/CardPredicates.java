package crystark.ek.battlesim.battle;

import crystark.common.api.CardType;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public enum CardPredicates implements Predicate<Card> {
	NOT_READY(((Predicate<Card>) Card::isReady).negate()),
	ON_FIELD(Card::isOnField),
	HAS_IMMUNITY(Card::hasImmunity),
	IS_DAMAGED(Card::isDamaged),
	IS_PURIFIABLE(Card::isPurifiable),
	CAN_BE_REANIMATED(Card::canBeReanimated),
	IS_MOUNTAIN(c -> CardType.mtn.equals(c.getType())),
	IS_TUNDRA(c -> CardType.tundra.equals(c.getType())),
	IS_SWAMP(c -> CardType.swamp.equals(c.getType())),
	IS_FOREST(c -> CardType.forest.equals(c.getType()));

	private final Predicate<Card> delegate;

	private CardPredicates(Predicate<Card> delegate) {
		this.delegate = delegate;
	}

	@Override
	public boolean test(Card card) {
		return delegate.test(card);
	}

	public static Predicate<Card> nextTo(final Card sourceCard) {
		final int idx = sourceCard.owner().getIndexOnField(sourceCard);
		return atIndexes(idx - 1, idx + 1);
	}

	public static Predicate<Card> atIndexes(Integer firstIdx, Integer... otherIndices) {
		if (otherIndices.length == 1) {
			// Most often used indices count
			return card -> {
				Player owner = card.owner();
				return card.equals(owner.atIndexOnField(firstIdx)) || card.equals(owner.atIndexOnField(otherIndices[0]));
			};
		} else if (otherIndices.length == 0) {
			return card -> card.equals(card.owner().atIndexOnField(firstIdx));
		} else {
			final List<Integer> indicesList = Arrays.asList(otherIndices);
			return card -> {
				int index = card.owner().getIndexOnField(card);
				if (index == -1) {
					throw new IndexOutOfBoundsException();
				}

				return indicesList.contains(index);
			};
		}
	}

	public static Predicate<Card> summonedBy(Card sourceCard, String abilityRef) {
		return card -> card.isSummoned() && ((SummonedCard) card).isSummonedBy(sourceCard, abilityRef);
	}

	public static Predicate<Card> isNamed(String name) {
		return card -> card.desc.name.equalsIgnoreCase(name);
	}
}
