package crystark.ek.battlesim.battle;

import java.util.ArrayList;
import java.util.Collection;

import crystark.ek.battlesim.ability.IAbility;
import crystark.ek.battlesim.ability.IAfterAttack;
import crystark.ek.battlesim.ability.IAfterDamage;
import crystark.ek.battlesim.ability.IAfterHit;
import crystark.ek.battlesim.ability.IAfterTurn;
import crystark.ek.battlesim.ability.IAttack;
import crystark.ek.battlesim.ability.IAttackTargetChanger;
import crystark.ek.battlesim.ability.IAura;
import crystark.ek.battlesim.ability.IBeforeAttack;
import crystark.ek.battlesim.ability.IBeforeAttackDeboost;
import crystark.ek.battlesim.ability.IBeforeDamage;
import crystark.ek.battlesim.ability.IBeforeHeroDamage;
import crystark.ek.battlesim.ability.IBeforeRound;
import crystark.ek.battlesim.ability.IBeforeTurn;
import crystark.ek.battlesim.ability.IDeathWhisperAble;
import crystark.ek.battlesim.ability.IDesperationAble;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.IPreemptiveStrikeAble;
import crystark.ek.battlesim.ability.IQuickstrikeAble;
import crystark.ek.battlesim.ability.impl.ArmorLess;
import crystark.ek.battlesim.ability.impl.Evasion;
import crystark.ek.battlesim.ability.impl.IceShield;
import crystark.ek.battlesim.ability.impl.Immunity;
import crystark.ek.battlesim.ability.impl.Infiltrator;
import crystark.ek.battlesim.ability.impl.Reanimation;
import crystark.ek.battlesim.ability.impl.Reflection;
import crystark.ek.battlesim.ability.impl.Resistance;
import crystark.ek.battlesim.ability.impl.Resurrection;
import crystark.ek.battlesim.ability.impl.SoulImprison;
import crystark.ek.battlesim.ability.impl.SpiritualVoice;
import crystark.ek.battlesim.ability.impl.Suppressing;

/**
 * Template class to encapsulate card's ability attributes
 */
public class AbilitiesSupport {

	private final AbilitiesVisitor ABILITIES_ADD_VISITOR    = new AbilitiesAddVisitor();
	private final AbilitiesVisitor ABILITIES_REMOVE_VISITOR = new AbilitiesRemoveVisitor();

	protected final FTArrayList<IAbility>         allAbilities         = new FTArrayList<>();
	protected final FTArrayList<IQuickstrikeAble> quickstrikeAbilities = new FTArrayList<>(4);
	protected final FTArrayList<IDesperationAble> desperationAbilities = new FTArrayList<>(4);
	protected final FTArrayList<IPreemptiveStrikeAble> preemptiveStrikeAbilities = new FTArrayList<>(2);
	protected final FTArrayList<IDeathWhisperAble>     deathWhisperAbilities     = new FTArrayList<>(2);

	protected final FTArrayList<IBeforeTurn>          onTurnStartAbilities   = new FTArrayList<>(4);
	protected final FTArrayList<IPower>               powerAbilities         = new FTArrayList<>(4);
	protected final FTArrayList<IBeforeDamage>        damageReducerAbilities = new FTArrayList<>(4);
	protected final FTArrayList<IAttack>              attackAbilities        = new FTArrayList<>(4);
	protected final FTArrayList<IAfterHit>            onAfterHitAbilities    = new FTArrayList<>(4);
	protected final FTArrayList<IAfterAttack>         afterAttackAbilities   = new FTArrayList<>(4);
	protected final FTArrayList<IAfterDamage>         counterAbilities       = new FTArrayList<>(4);
	protected final FTArrayList<IAfterTurn>           onFinishedAbilities    = new FTArrayList<>(4);
	protected final FTArrayList<IAura>                auraAbilities          = new FTArrayList<>(4);
	protected final FTArrayList<IBeforeAttackDeboost> deboostAbilities = new FTArrayList<>(4);
	protected final FTArrayList<IBeforeAttack>        boostAbilities   = new FTArrayList<>(4);
	protected final FTArrayList<Resurrection>         resurrections    = new FTArrayList<>(4);
	protected final FTArrayList<IBeforeRound>         beforeRoundAbilities = new FTArrayList<>(4);
	protected final FTArrayList<Immunity>             immunityList     = new FTArrayList<>(4);
	protected final FTArrayList<Evasion>              evasionList      = new FTArrayList<>(4);
	protected final FTArrayList<IBeforeHeroDamage>    guardList        = new FTArrayList<>(4);
	protected final FTArrayList<Reflection>           reflectionList   = new FTArrayList<>(4);
	protected final FTArrayList<Resistance>           resistanceList   = new FTArrayList<>(4);
	protected final FTArrayList<Infiltrator>          infiltratorList  = new FTArrayList<>(4);
	protected final FTArrayList<ArmorLess>            armorLessList    = new FTArrayList<>(4);
	protected final FTArrayList<IceShield>            iceShieldList    = new FTArrayList<>(4);
	protected final FTArrayList<Reanimation>          reanimateList    = new FTArrayList<>(4);
	protected final FTArrayList<SoulImprison>   soulImprisonList   = new FTArrayList<>(4);
	protected final FTArrayList<SpiritualVoice> spiritualVoiceList = new FTArrayList<>(4);
	protected final FTArrayList<Suppressing>    suppressingList    = new FTArrayList<>(4);
	protected final FTArrayList<IAttackTargetChanger>    attackTargetChangerList    = new FTArrayList<>(4);

	private int baseAbilitiesCount   = 0;
	private int baseQuickstrikeCount = 0;
	private int baseDesperationCount = 0;
	private int basePreemptiveStrikeCount = 0;
	private int baseDeathWhisperCount     = 0;

	private int baseOnTurnStartCount    = 0;
	private int basePowerCount          = 0;
	private int baseDamagereducerCount  = 0;
	private int baseAttackCount         = 0;
	private int baseOnHitCount          = 0;
	private int baseAfterHitCount       = 0;
	private int baseCounterCount        = 0;
	private int baseOnFinishedCount     = 0;
	private int baseAuraCount           = 0;
	private int baseDeboostCount        = 0;
	private int baseBoostCount          = 0;
	private int baseImmunityCount       = 0;
	private int baseEvasionCount        = 0;
	private int baseGuardCount          = 0;
	private int baseReflectionCount     = 0;
	private int baseResistanceCount     = 0;
	private int baseInfiltratorCount    = 0;
	private int baseArmorLessCount    	= 0;
	private int baseIceShieldCount      = 0;
	private int baseReanimateCount      = 0;
	private int baseSoulImprisonCount   = 0;
	private int baseSpiritualVoiceCount = 0;
	private int baseSuppressingCount    = 0;
	private int baseResurrectionsCount  = 0;
	private int basePurificationsCount  = 0;
	private int baseAttackTargetChangerCount  = 0;


	protected AbilitiesVisitor getAbilitiesAddVisitor() {
		return ABILITIES_ADD_VISITOR;
	}

	protected AbilitiesVisitor getAbilitiesRemoveVisitor() {
		return ABILITIES_REMOVE_VISITOR;
	}

	protected void memoAbilitiesState() {

		this.baseAbilitiesCount = this.allAbilities.size();
		this.baseQuickstrikeCount = this.quickstrikeAbilities.size();
		this.baseDesperationCount = this.desperationAbilities.size();

//		TODO Should PS and DW are resetted?
//		this.basePreemptiveStrikeCount	= this.preemptiveStrikeAbilities.size();
//		this.baseDeathWhisperCount = this.deathWhisperAbilities.size();

		this.baseOnTurnStartCount = this.onTurnStartAbilities.size();
		this.basePowerCount = this.powerAbilities.size();
		this.baseDamagereducerCount = this.damageReducerAbilities.size();
		this.baseAttackCount = this.attackAbilities.size();
		this.baseOnHitCount = this.onAfterHitAbilities.size();
		this.baseAfterHitCount = this.afterAttackAbilities.size();
		this.baseCounterCount = this.counterAbilities.size();
		this.baseOnFinishedCount = this.onFinishedAbilities.size();
		this.baseAuraCount = this.auraAbilities.size();
		this.baseBoostCount = this.boostAbilities.size();
		this.baseDeboostCount = this.deboostAbilities.size();

		this.baseResurrectionsCount = this.resurrections.size();
		this.basePurificationsCount = this.beforeRoundAbilities.size();
		this.baseImmunityCount = this.immunityList.size();
		this.baseEvasionCount = this.evasionList.size();
		this.baseGuardCount = this.guardList.size();
		this.baseReflectionCount = this.reflectionList.size();
		this.baseResistanceCount = this.resistanceList.size();
		this.baseInfiltratorCount = this.infiltratorList.size();
		this.baseArmorLessCount = this.armorLessList.size();
		this.baseIceShieldCount = this.iceShieldList.size();
		this.baseReanimateCount = this.reanimateList.size();
		this.baseSoulImprisonCount = this.soulImprisonList.size();
		this.baseSpiritualVoiceCount = this.spiritualVoiceList.size();
		this.baseSuppressingCount = this.suppressingList.size();
		this.baseAttackTargetChangerCount = this.attackTargetChangerList.size();
	}

	protected void resetAbilities() {

		truncate(this.baseAbilitiesCount, this.allAbilities);
		truncate(this.baseQuickstrikeCount, this.quickstrikeAbilities);
		truncate(this.baseDesperationCount, this.desperationAbilities);
//		TODO Should PS and DW are resetted?
//		truncate(this.basePreemptiveStrikeCount, this.preemptiveStrikeAbilities);
//		truncate(this.baseDeathWhisperCount, this.deathWhisperAbilities);

		truncate(this.baseOnTurnStartCount, this.onTurnStartAbilities);
		truncate(this.basePowerCount, this.powerAbilities);
		truncate(this.baseDamagereducerCount, this.damageReducerAbilities);
		truncate(this.baseAttackCount, this.attackAbilities);
		truncate(this.baseOnHitCount, this.onAfterHitAbilities);
		truncate(this.baseAfterHitCount, this.afterAttackAbilities);
		truncate(this.baseCounterCount, this.counterAbilities);
		truncate(this.baseOnFinishedCount, this.onFinishedAbilities);
		truncate(this.baseAuraCount, this.auraAbilities);
		truncate(this.baseBoostCount, this.boostAbilities);
		truncate(this.baseDeboostCount, this.deboostAbilities);

		truncate(this.baseResurrectionsCount, this.resurrections);
		truncate(this.basePurificationsCount, this.beforeRoundAbilities);
		truncate(this.baseImmunityCount, this.immunityList);
		truncate(this.baseEvasionCount, this.evasionList);
		truncate(this.baseGuardCount, this.guardList);
		truncate(this.baseReflectionCount, this.reflectionList);
		truncate(this.baseResistanceCount, this.resistanceList);
		truncate(this.baseInfiltratorCount, this.infiltratorList);
		truncate(this.baseArmorLessCount, this.armorLessList);
		truncate(this.baseIceShieldCount, this.iceShieldList);
		truncate(this.baseReanimateCount, this.reanimateList);
		truncate(this.baseSoulImprisonCount, this.soulImprisonList);
		truncate(this.baseSpiritualVoiceCount, this.spiritualVoiceList);
		truncate(this.baseSuppressingCount, this.suppressingList);
		truncate(this.baseAttackTargetChangerCount, this.attackTargetChangerList);
	}

	private static void truncate(int size, FTArrayList<?> list) {
		/*while (list.size() > size) {
			list.remove(list.size() - 1);
		}*/
		list.truncateToSize(size);
	}

	//Fast Truncated ArrayList
	public static final class FTArrayList<E> extends ArrayList<E>
	{
		public FTArrayList(int initialCapacity) {
			super(initialCapacity);
		}

		public FTArrayList() {
			super();
		}

		public FTArrayList(Collection<? extends E> c) {
			super(c);
		}

		public void truncateToSize(int newSize){
			if (newSize < size()) super.removeRange(newSize, size());
		}
	}

	/**
	 * Add an ability into it's own list
	 */
	private final class AbilitiesAddVisitor implements AbilitiesVisitor {

		@Override
		public void visit(Reanimation a) {
			reanimateList.add(a);
			if (!(a.isQuickstrike() || a.isDesperation() || a.isDeathWhisper() || a.isPreemptiveStrike()) ){
				powerAbilities.add(a);
			}
		}

		@Override
		public void visit(IPower a) {
			powerAbilities.add(a);
		}

		@Override
		public void visit(Reflection a) {
			reflectionList.add(a);
			damageReducerAbilities.add(a);
		}

		@Override
		public void visit(IceShield a) {
			iceShieldList.add(a);
			damageReducerAbilities.add(a);
		}

		@Override
		public void visit(IBeforeDamage a) {
			//Taunt catch unredused damage
			damageReducerAbilities.add(0, a);
		}

		@Override
		public void visit(IAttack a) {
			attackAbilities.add(a);
		}

		@Override
		public void visit(IBeforeTurn a) {
			onTurnStartAbilities.add(a);
		}

		@Override
		public void visit(IAfterHit a) {
			onAfterHitAbilities.add(a);
		}

		@Override
		public void visit(IAfterAttack a) {
			afterAttackAbilities.add(a);
		}

		@Override
		public void visit(IAfterTurn a) {
			onFinishedAbilities.add(a);
		}

		@Override
		public void visit(IAfterDamage a) {
			counterAbilities.add(a);
		}

		@Override
		public void visit(IAura a) {
			auraAbilities.add(a);
		}

		@Override
		public void visit(IBeforeAttack a) {
			boostAbilities.add(a);
		}

		@Override
		public void visit(IBeforeAttackDeboost a) {
			deboostAbilities.add(a);
		}

		@Override
		public void visit(Resurrection a) {
			resurrections.add(a);
		}

		@Override
		public void visit(Immunity a) {
			immunityList.add(a);
		}

		@Override
		public void visit(IBeforeHeroDamage a) {
			guardList.add(a);
		}

		@Override
		public void visit(Resistance a) {
			resistanceList.add(a);
		}

		@Override
		public void visit(Infiltrator a) {
			infiltratorList.add(a);
		}

		@Override
		public void visit(Evasion a) {
			evasionList.add(a);
		}

		@Override
		public void visit(SoulImprison a) {
			soulImprisonList.add(a);
		}

		@Override
		public void visit(SpiritualVoice a) {
			spiritualVoiceList.add(a);
		}

		@Override
		public void visit(Suppressing a) {
			suppressingList.add(a);
		}

		@Override
		public void visit(ArmorLess a) {
			armorLessList.add(a);
		}

		@Override
		public void visit(IAttackTargetChanger a) {
			attackTargetChangerList.add(a);
		}

		@Override
		public void visit(IBeforeRound a) {
			beforeRoundAbilities.add(a);
		}
	}

	/**
	 * Remove an ability from it's own list
	 */
	private final class AbilitiesRemoveVisitor implements AbilitiesVisitor {

		// After QS and D as those are manage through the card but before IPower
		@Override
		public void visit(Reanimation a) { // can be QS, Desp or Power
			reanimateList.remove(a);
			if (!(a.isQuickstrike() || a.isDesperation() || a.isDeathWhisper() || a.isPreemptiveStrike()) ) {
				powerAbilities.remove(a);
			}
		}

		@Override
		public void visit(IPower a) {
			powerAbilities.remove(a);
		}

		@Override
		public void visit(IBeforeDamage a) {
			damageReducerAbilities.remove(a);
		}

		@Override
		public void visit(Reflection a) {
			reflectionList.remove(a);
			damageReducerAbilities.remove(a);
		}

		@Override
		public void visit(IceShield a) {
			iceShieldList.remove(a);
			damageReducerAbilities.remove(a);
		}

		@Override
		public void visit(IAttack a) {
			attackAbilities.remove(a);
		}

		@Override
		public void visit(IBeforeTurn a) {
			onTurnStartAbilities.remove(a);
		}

		@Override
		public void visit(IAfterHit a) {
			onAfterHitAbilities.remove(a);
		}

		@Override
		public void visit(IAfterAttack a) {
			afterAttackAbilities.remove(a);
		}

		@Override
		public void visit(IAfterTurn a) {
			onFinishedAbilities.remove(a);
		}

		@Override
		public void visit(IAfterDamage a) {
			counterAbilities.remove(a);
		}

		@Override
		public void visit(IAura a) {
			auraAbilities.remove(a);
		}

		@Override
		public void visit(IBeforeAttack a) {
			boostAbilities.remove(a);
		}

		@Override
		public void visit(IBeforeAttackDeboost a) {
			deboostAbilities.remove(a);
		}

		@Override
		public void visit(Immunity a) {
			immunityList.remove(a);
		}

		@Override
		public void visit(IBeforeHeroDamage a) {
			guardList.remove(a);
		}

		@Override
		public void visit(Resistance a) {
			resistanceList.remove(a);
		}

		@Override
		public void visit(Resurrection a) {
			resurrections.remove(a);
		}

		@Override
		public void visit(Infiltrator a) {
			infiltratorList.remove(a);
		}

		@Override
		public void visit(Evasion a) {
			evasionList.remove(a);
		}

		@Override
		public void visit(SoulImprison a) {
			soulImprisonList.remove(a);
		}

		@Override
		public void visit(SpiritualVoice a) {
			spiritualVoiceList.remove(a);
		}

		@Override
		public void visit(Suppressing a) {
			suppressingList.remove(a);
		}

		@Override
		public void visit(ArmorLess a) {
			armorLessList.remove(a);
		}

		@Override
		public void visit(IAttackTargetChanger a) {
			attackTargetChangerList.remove(a);
		}

		@Override
		public void visit(IBeforeRound a) {
			beforeRoundAbilities.remove(a);
		}
	}
}
