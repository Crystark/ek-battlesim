package crystark.ek.battlesim.battle;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.Predicate;

public class CardInFieldIterator implements Iterator<Card> {

	private final List<Card>		cards;
	private final Predicate<Card>	predicate;

	boolean							nextComputed	= false;
	Card							next			= null;
	int								cursor			= -1;

	CardInFieldIterator(List<Card> cards, Predicate<Card> predicate) {
		this.cards = cards;
		this.predicate = predicate;
	}

	private Card computeNext() {
		if (!nextComputed) {
			next = null;
			while (++cursor < cards.size()) {
				Card card = cards.get(cursor);
				if (CardPredicates.ON_FIELD.and(predicate).test(card)) {
					next = card;
					break;
				}
			}
			nextComputed = true;
		}
		return next;
	}

	@Override
	public boolean hasNext() {
		return computeNext() != null;
	}

	@Override
	public Card next() {
		computeNext();
		if (next != null) {
			nextComputed = false;
			return next;
		}
		throw new NoSuchElementException();
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

}