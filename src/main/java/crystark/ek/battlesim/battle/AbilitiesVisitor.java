package crystark.ek.battlesim.battle;

import crystark.ek.battlesim.ability.IAfterAttack;
import crystark.ek.battlesim.ability.IAfterDamage;
import crystark.ek.battlesim.ability.IAfterHit;
import crystark.ek.battlesim.ability.IAfterTurn;
import crystark.ek.battlesim.ability.IAttack;
import crystark.ek.battlesim.ability.IAttackTargetChanger;
import crystark.ek.battlesim.ability.IAura;
import crystark.ek.battlesim.ability.IBeforeAttack;
import crystark.ek.battlesim.ability.IBeforeAttackDeboost;
import crystark.ek.battlesim.ability.IBeforeDamage;
import crystark.ek.battlesim.ability.IBeforeHeroDamage;
import crystark.ek.battlesim.ability.IBeforeRound;
import crystark.ek.battlesim.ability.IBeforeTurn;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.impl.ArmorLess;
import crystark.ek.battlesim.ability.impl.Evasion;
import crystark.ek.battlesim.ability.impl.IceShield;
import crystark.ek.battlesim.ability.impl.Immunity;
import crystark.ek.battlesim.ability.impl.Infiltrator;
import crystark.ek.battlesim.ability.impl.Reanimation;
import crystark.ek.battlesim.ability.impl.Reflection;
import crystark.ek.battlesim.ability.impl.Resistance;
import crystark.ek.battlesim.ability.impl.Resurrection;
import crystark.ek.battlesim.ability.impl.SoulImprison;
import crystark.ek.battlesim.ability.impl.SpiritualVoice;
import crystark.ek.battlesim.ability.impl.Suppressing;

/**
 *
 */
public interface AbilitiesVisitor {
	void visit(Reanimation a);

	void visit(IPower a);

	void visit(Reflection a);

	void visit(IceShield a);

	void visit(IBeforeDamage a);

	void visit(IAttack a);

	void visit(IBeforeTurn a);

	void visit(IAfterHit a);

	void visit(IAfterAttack a);

	void visit(IAfterTurn a);

	void visit(IAfterDamage a);

	void visit(IAura a);

	void visit(IBeforeAttack a);

	void visit(IBeforeAttackDeboost a);

	void visit(Resurrection a);

	void visit(Immunity a);

	void visit(IBeforeHeroDamage a);

	void visit(Resistance a);

	void visit(Infiltrator a);

	void visit(Evasion a);

	void visit(SoulImprison a);

	void visit(SpiritualVoice a);

	void visit(Suppressing a);

	void visit(ArmorLess a);

	void visit(IAttackTargetChanger a);

	void visit(IBeforeRound a);
}
