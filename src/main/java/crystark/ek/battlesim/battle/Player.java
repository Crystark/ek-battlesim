package crystark.ek.battlesim.battle;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import crystark.common.api.CardDesc;
import crystark.common.api.CardType;
import crystark.common.api.ConflictType;
import crystark.common.api.DeckDesc;
import crystark.common.api.RuneDesc;
import crystark.common.format.CardDescFormat;
import crystark.common.format.RuneDescFormat;
import crystark.ek.Constants;
import crystark.ek.Helper;
import crystark.ek.battlesim.ability.IAttackTargetChanger;
import crystark.ek.battlesim.rune.Rune;
import crystark.ek.battlesim.rune.RuneFactory;
import crystark.tools.CustomPredicates;

import static crystark.ek.Constants.NL;

public class Player {
	private static final Logger        L              = LoggerFactory.getLogger(Player.class);
	public static final  int           MAX_HAND_SIZE  = 5;
	private static final Collection<Card> SINGLETON_NULL = Collections.singleton(null);

	public static final  Card             DEAD_CARD           = new Card(new CardDesc("dead card", 0, 0, 0, 0, 0, 0L, CardType.special, ConflictType.none, 0, Collections.emptyList(), Collections.emptyList()), null);
	private static final Collection<Card> SINGLETON_DEAD_CARD = Collections.singleton(DEAD_CARD);

	public final DeckDesc desc;

	protected Player opponent;
	protected int    currentHp;

	protected final ArrayList<Card> deck     = new ArrayList<>(10);
	protected final ArrayList<Card> hand     = new ArrayList<>(10);
	protected final ArrayList<Card> field    = new ArrayList<>(10);
	protected final ArrayList<Card> cemetery = new ArrayList<>(10);
	protected final ArrayList<Rune> runes;

	protected final ArrayList<Card> beforeRoundCards = new ArrayList<>(10);
	protected final ArrayList<Card> guardCards            = new ArrayList<>(10);
	protected final ArrayList<Card> preemptiveStrikeCards = new ArrayList<>(5);
	protected final ArrayList<Card> deathWhispersCards    = new ArrayList<>(10);

	private       boolean      playedCardFromHand = false;
	private final PlayerStatus status             = new PlayerStatus();
	private final Set<Object>  markers            = new HashSet<>();

	protected Player(DeckDesc desc) {
		this.desc = desc;

		this.runes = new ArrayList<>(desc.runesDescs.size());

		for (CardDesc card : desc.cardsDescs) {
			this.deck.add(new Card(card, this));
		}
		if (desc.skipShuffle > 0 ) {
			shuffleLast(this.deck, desc.skipShuffle);
			// First card drawn is last in this.deck list to avoid array copy,
			// so that card input first is the first card drawn.
			Collections.reverse(this.deck);
		}
		else {
			Collections.shuffle(this.deck);
		}

		for (RuneDesc rune : desc.runesDescs) {
			this.runes.add(RuneFactory.get(rune));
		}

		this.currentHp = desc.health;

	}

	/** First card to drow has to be first in {@code cards}
	 *
 	 * @param cards First card to drow has to be first in the List
	 * @param fromIdx
	 */
	private void shuffleLast(List<Card> cards, int fromIdx){
		// Care about overflow, fromIdx may has Integer.MAX_VALUE value
		if (fromIdx >= cards.size() - 1) return;
		List<Card> shuffleCards = cards.subList(fromIdx, cards.size());
		Collections.shuffle(shuffleCards);
	}

	public static Player from(DeckDesc desc) {
		return new Player(desc);
	}

	public static void salute(Player p1, Player p2) {
		p1.opponent = p2;
		p2.opponent = p1;
	}

	public boolean tryAddMarker(Object marker) {
		return markers.add(marker);
	}

	public DeckDesc snapshotHpAndCards() {
		ArrayList<CardDesc> cards = new ArrayList<>();
		for (Card card : this.deck) {
			cards.add(card.desc);
		}
		for (Card card : this.hand) {
			cards.add(card.desc);
		}
		for (Card card : this.field) {
			if (!card.isSummoned()) {
				cards.add(card.snapshot());
			}
		}
		return new DeckDesc(desc.deckName, desc.playerName, desc.playerLevel, this.currentHp, cards, desc.runesDescs, desc.skipShuffle, desc.alwaysStart, desc.infiniteHp, desc.waitToPlay, desc.bypassValidation);
	}

	public Player opponent() {
		return opponent;
	}

	public void newRound() {
		ArrayList<Card> hand1 = this.hand;
		for (int i = 0, hand1Size = hand1.size(); i < hand1Size; i++) {
			Card card = hand1.get(i);
			card.decreaseTimer();
		}
	}

	public Card drawCardFromDeck() {
		if (!this.deck.isEmpty()) {
			return this.deck.remove(this.deck.size() - 1); // pick card from the top
		}
		return null;
	}

	public void playCardsFromDeck() {
		Card card;
		if (this.countCardsInHand() < MAX_HAND_SIZE && (card = drawCardFromDeck()) != null) {
			this.addCardToHand(card);
		}
	}

	public void playCardsFromHand() {
		// Note: auto mode does not work like manual:
		// - auto activates cards one by one when they are set on the field
		// - manual puts all the cards on the field then activates qs/auras
		// How would desperation on the other side be managed in both case if a card dies during the QS ?
		// We'll use the auto mode here

		if (playedCardFromHand || this.countCardsInHand(Card::isReady) >= desc.waitToPlay) {
			playedCardFromHand = true;
			// using for to avoid concurrent modification if sacrifce then rez
			for (int i = 0, s = this.hand.size(); i < s; i++) {
				Card card = this.hand.get(i);
				if (card != null && card.isReady()) {
					removeCardFromHand(i, false);
					i--;
					s--;

					this.addCardToField(card); // TODO qs_blizzard + desperation: reanimate + qs_teleport can alter hand here
				}
			}
		}
		else {
			if (Constants.IS_DEBUG)
				L.info("Waiting to play " + desc.waitToPlay + " cards at once.");
		}
	}

	public void preemptiveStrikes() {
		for (int i = 0; i < this.preemptiveStrikeCards.size(); i++) {
			Card card = this.preemptiveStrikeCards.get(i);
			card.applyPreemptiveStrikes();
		}
	}

	public void deathWhispers() {
		for (int i = 0; i < this.deathWhispersCards.size(); i++) {
			Card card = this.deathWhispersCards.get(i);
			card.applyDeathWhispers();
		}
	}

	public void playCardsFromField() {
		// Apply IBeforeRound (Purification, Luna Grace) if any
		applyBeforeRound();

		// using for to avoid concurrent modification. the field should not be modified while beeing processed (elements can be added)
		for (int i = 0, s = this.field.size(); i < s; i++) { //reanimated cards should not have a turn using this

			Card card = this.field.get(i);
			if (Constants.IS_DEBUG)
				L.debug(card + " starts it's turn.");

			try {
				if (!card.isAlive()) {
					if (Constants.IS_DEBUG) {
						L.debug(card + " skipped (dead)");
						//L.debug(card + " ends it's turn.");
					}
					continue;
				}

				card.onTurnStart();

				if (!card.canUseSkill()) {
					if (Constants.IS_DEBUG)
						L.debug(card + " cannot use it's skills.");
				}
				else {
					card.applyPowerAbilities();

					// Power abilities can kill the card (reflection)
					if (!card.isAlive()) {
						if (Constants.IS_DEBUG)
							L.debug(card + " died on its turn");
						continue;
					}
				}


				// Power abilities can freeze the card (D_Blizzard)
				if (!card.canAttack()) {
					if (Constants.IS_DEBUG)
						L.debug(card + " cannot attack.");
				}
				else {
					card.applyBoosts();
					int damage = card.getCurrentAtk();
					if (damage != 0) {
						Card defendingCard = this.oppositeCardOnField(card);
						if (defendingCard != null) {

                            physicalAttack(card, defendingCard, damage);
                            
						} else {
							if (Constants.IS_DEBUG)
								L.debug(card + " attacks opponent player");
							this.opponent.damaged(damage, true);
						}
					}
					card.removeBoosts();

					if (!card.isAlive()) {
						if (Constants.IS_DEBUG)
							L.debug(card + " died on its turn");
						continue;
					}
				}

				if (card.isConfused()) {
					if (Constants.IS_DEBUG)
						L.debug(card + " attacks " + this + " out of confusion ! ");
					this.damaged(card.getCurrentAtk(), true);

					// A guard card if been damaged by confusion may exile this card (Reverse Alchemist, D_Exile All)
					if (!card.isAlive()) {
						if (Constants.IS_DEBUG)
							L.debug(card + " died on its turn");
						continue;
					}
				}
				

				//Reget canUseSkill for case the card has been frozen by D_Blizzard 6
				if (card.canUseSkill()) {
					//Rejuvenation is silenced
					card.applyAfterTurnAbilities();
				}
				else if (Constants.IS_DEBUG)
					L.debug(card + " cannot use it's skills.");

				card.applyRecurringStatus();

				if (card.isAlive()) {
					card.removeSilence();
					card.removeStatusAilments();
					card.removeDebuffs();
				}

				if (Constants.IS_DEBUG)
					L.debug(card + " ends it's turn.");
			}
			catch (Throwable t) {
				throw new BattleException("Error during " + card + "'s turn", t);
			}
		}
	}

    public void physicalAttack(Card attackingCard, Card defendingCard, int damage) {
        Card newDefendingCard = defendingCard;
        List<IAttackTargetChanger> attackTargetChangerList = defendingCard.attackTargetChangerList;
        for (int i1 = 0, size = attackTargetChangerList.size(); i1 < size; i1++) {
            IAttackTargetChanger changer = attackTargetChangerList.get(i1);
            newDefendingCard = changer.selectTarget(defendingCard, attackingCard, damage, DamageType.attack);
            if (newDefendingCard != defendingCard) {
                if (Constants.IS_DEBUG) {
                    L.debug(newDefendingCard + " intercept attack to " + defendingCard + " by " + changer.getName() + " skill");
                }
                break;
            }
        }

        if (Constants.IS_DEBUG)
            L.debug(attackingCard + " attacks " + newDefendingCard + " for " + damage + " damage");

        //Taunt intercepts first attack only of Double Attack skill
        // TODO Does Clean Sweep affects Taunt card?
        int damageDone = attackingCard.attack(newDefendingCard, damage);

        if (attackingCard.isAlive() && attackingCard.canUseSkill()) {
            attackingCard.applyAttackAbilities(defendingCard, damage, damageDone);
        }
    }

    void applyBeforeRound() {
		for (int i = 0, size = beforeRoundCards.size(); i < size; i++) {
			Card card = beforeRoundCards.get(i);
			card.onBeforeRound();
		}
	}

	int applyGuard(int dmg) {
		final int size = guardCards.size();
		if (size == 0) {
			return dmg;
		}

		//guardCards may reduce if guard card killed
		final List<Card> guards = new ArrayList(guardCards);
		for (int i = 0; i < size && dmg > 0; i++) {
			Card card = guards.get(i);
			dmg = card.guard(dmg);
		}
		return dmg;
	}

	public void cleanup() {
		// Remove teleport-dead cards
		this.hand.removeAll(SINGLETON_NULL);
		// Remove dead cards from field
		boolean isRemoved = this.field.removeAll(SINGLETON_DEAD_CARD);
		if (Constants.IS_DEBUG && isRemoved)
			L.debug("Dead cards are removed from field");
	}

	public void destroyCard(Card card) {
		Player.sendCardToCemetery(card, true);
	}

	static void sendCardToCemetery(Card card, boolean triggerDesperation) {
		if (!card.isBeingKilled()) { // To avoid cycling on deaths
			card.markBeingKilled(true);

			if (Constants.IS_DEBUG)
				L.debug(card + " killed");

			// Note: desperation applied before card is removed from field. This can lead to a chain destruction of all desperation:destroy cards or not if one card is "destroyed twice"
			if (triggerDesperation)
				card.applyDesperations();

			if (!card.isAlive()) { // Desperation can short-circuit death (Last Chance)
				card.removeFromField();

				if (card.isSummoned()) {
					if (Constants.IS_DEBUG)
						L.debug(card + " has been unsummoned.");
				}
				else if (card.isGoingExtinct()) {
					if (Constants.IS_DEBUG)
						L.debug(card + " has gone extinct.");
				}
				else if (!triggerDesperation || !card.resurrects()) {
					card.addToCemetery();
				}
			}
		}
		else {
			if (Constants.IS_DEBUG)
				L.debug(card + " is already dying.");
		}
	}

	void addCardToCemetery(Card card) {
		card.markDirty();
		card.restoreType();
		card.removeSilence();

		if (Constants.IS_DEBUG)
			L.debug(card + " added to cemetery.");

		this.cemetery.add(card);

		if (card.hasDeathWhisper()) {
			this.deathWhispersCards.add(card);
		}
	}

	void removeCardFromCemetery(Card card) {
		if (Constants.IS_DEBUG)
			L.debug(card + " removed from cemetery.");

		if (card.hasDeathWhisper()) {
			this.deathWhispersCards.remove(card);
		}
		this.cemetery.remove(card);
	}

	void addCardRandomlyToDeck(Card card) {
		addCardToDeck(card, Helper.random(this.deck.size() + 1));
	}

	void addCardOnTopOfDeck(Card card) {
		addCardToDeck(card, this.deck.size());
	}

	void addCardToDeck(Card card, int position) {
		if (card.isSummoned()) {
			if (Constants.IS_DEBUG)
				L.debug(card + " has been unsummoned.");
		}
		else {
			this.deck.add(position, card);
			if (Constants.IS_DEBUG)
				L.debug(card + " added to deck.");
		}
	}

	void addCardToField(Card card) {

		card.reset();
		card.markDirty();
		card.markOnField(true);

		if (Constants.IS_DEBUG && this.field.contains(card)) {
			throw new RuntimeException("Card " + card.desc.uniqueName + " already on field.");
		}

		this.field.add(card);
		status.cardAddedToField(card);

		if (Constants.IS_DEBUG)
			L.debug(card + " added to field");

		// Apply auras
		card.applyAuras();
		// Receive auras
		ArrayList<Card> field1 = this.field;
		//Skip last card. It doesn't gives aura to itself.
		for (int i = 0, field1Size = field1.size() - 1; i < field1Size; i++) {
			Card fieldCard = field1.get(i);
			fieldCard.applyAurasTo(card);
		}

		// Quick Strikes
		card.applyQuickStrikes();

		// Add card to cards having purification
		if (card.hasBeforeRound()) {
			beforeRoundCards.add(card);
		}
		if (card.hasGuard()) {
			guardCards.add(card);
		}
	}

	void removeCardFromField(Card card) {
		status.cardRemovedFromField(card);
		card.markOnField(false); // marked as not beeing on the field anymore (in case it rez, it would be alive but not on field)
		if (card.canApplyAuras()) {
			ArrayList<Card> field1 = this.field;
			for (int i = 0, field1Size = field1.size(); i < field1Size; i++) {
				Card fieldCard = field1.get(i);
				if (fieldCard.isAlive()) {
					fieldCard.removeAurasWithSource(card);
				}
			}
		}
		int idx = this.field.indexOf(card);
		this.field.set(idx, DEAD_CARD); // replaced by a dead card to keep the "empty" spot
		beforeRoundCards.remove(card);
		guardCards.remove(card);
	}

	/**
	 * Damage directed to player but reduced by guarding cards
	 *
	 * @param triggerGuardDesperation
	 */
	public void damaged(int dmg, boolean triggerGuardDesperation) {
		// Find cards with Guard to absorb damage.
		dmg = applyGuard(dmg);

		// Any damage left over is applied to the player's hero.
		this.modifyHp(-dmg);
	}

	public void heal(int h) {
		if (!desc.infiniteHp && this.currentHp > 0) {
			this.modifyHp(Math.min(h, desc.health - this.currentHp));
		}
	}

	/**
	 * Check if the player is still alive
	 */
	public boolean isAlive() {
		return this.hasRemainingHp() && (!this.hand.isEmpty() || !this.deck.isEmpty() || !this.field.isEmpty());
	}

	public boolean hasRemainingHp() {
		return (desc.infiniteHp || this.currentHp > 0);
	}

	public void handleRunes(int round) {
		ArrayList<Rune> runes1 = this.runes;
		for (int i = 0, runes1Size = runes1.size(); i < runes1Size; i++) {
			Rune rune = runes1.get(i);
			rune.use(round, this);
		}
	}

	public void chargeRunes() {
		ArrayList<Rune> runes1 = this.runes;
		for (int i = 0, runes1Size = runes1.size(); i < runes1Size; i++) {
			Rune rune = runes1.get(i);
			rune.charge();
		}
	}

	/**
	 * Removes the buffs that are supposed to be removed on the start of the turn of the player
	 */
	public void removeBuffs() {
		ArrayList<Card> field1 = this.field;
		for (int i = 0, field1Size = field1.size(); i < field1Size; i++) {
			Card fieldCard = field1.get(i);
			fieldCard.removeBuffs();
		}
	}

	public int getCurrentHp() {
		return this.currentHp;
	}

	public void modifyHp(int modifier) {
		if (!desc.infiniteHp && modifier != 0) {
			if (Constants.IS_DEBUG)
				L.debug(this + " " + (modifier > 0 ? "gained" : "lost") + ' ' + Math.abs(modifier) + " hp");
			this.currentHp += modifier;
			if (this.currentHp < 0)
				this.currentHp = 0;
		}
	}

	public static void modifyBaseHp(int modifier) {
		throw new UnsupportedOperationException("Can't modify player's base HP");
	}

	public boolean hpLessThanPercent(int percent) {
		return this.currentHp < (int) (desc.health * percent / 100f);
	}

	// --- Deck accessors

	public int countCardsInDeck() {
		return this.deck.size();
	}

	public Card pickLastHighestTimerCardInDeck(boolean remove) {
		return pickFirstHighestTimerCard(this.deck, remove);
	}

	public List<Card> pickHighestTimerCardsInDeck(int n, boolean remove) {
		return pickHighestTimerCards(this.deck, n, remove);
	}
	public List<Card> pickRandomCardsInDeck(int n, boolean remove) {
		final List<Card> result = Helper.pick(this.deck, n);
		if (remove) {
			this.deck.removeAll(result);
		}
		return result;
	}

	// --- Field accessors

	public CardInFieldIterator getFieldIterator(Predicate<Card> predicate) {
		return new CardInFieldIterator(this.field, predicate);
	}

	public void consumeCardsField(Consumer<Card> consumer) {
		for (int i = 0, s = this.field.size(); i < s; i++) {
			Card card = this.field.get(i);
			if (card.isAlive()) {
				consumer.accept(card);
			}
		}
	}

	public Stream<Card> streamCardsField() {
		return this.field.stream().filter(Card::isAlive);
	}

	public Card pickCardInField() {
		return Helper.pick(this.field, c -> c.isAlive());
	}

	public Card pickCardInField(Predicate<Card> predicate) {
		return Helper.pick(this.field, predicate.and(c -> c.isAlive()));
	}

	/**
	 * Compare based on the card health. If health is equal, order is randomized.
	 */
	static final Comparator<Card> HEALTH_COMPARE = (o1, o2) -> {
		int compared = Long.compare(o1.getCurrentHp(), o2.getCurrentHp());
		return compared == 0 ? (Helper.headsOrTails() ? -1 : 1) : compared;
	};

	public List<CardAction> pickLowestHpCards(int i) {
		if (this.field.isEmpty()) {
			return null;
		}

		List<CardAction> result = this.field.stream()
			.filter(Card::isOnField)
			.sorted(HEALTH_COMPARE)
			.limit(i)
			.map(CardAction::new)
			.collect(Collectors.toList());

		return result.isEmpty() ? null : result;
	}

	public Card pickLowestHpCard() {
		if (this.field.isEmpty()) {
			return null;
		}
		if (this.field.size() == 1) {
			Card card = this.field.get(0);
			return card.isAlive() ? card : null;
		}

		List<Card> result = new ArrayList<>();
		long lowestHp = Long.MAX_VALUE;
		ArrayList<Card> field1 = this.field;
		for (int i = 0, field1Size = field1.size(); i < field1Size; i++) {
			Card card = field1.get(i);
			if (card.isAlive()) {
				long hp = card.getCurrentHp();
				if (hp <= lowestHp) {
					if (hp < lowestHp) {
						lowestHp = hp;
						result.clear();
					}
					result.add(card);
				}
			}
		}
		return Helper.pick(result);
	}

	public Card pickMostDamagedCard() {
		if (this.field.isEmpty()) {
			return null;
		}
		if (this.field.size() == 1) {
			Card card = this.field.get(0);
			return card.isAlive() ? card : null;
		}

		List<Card> result = new ArrayList<>(4);
		int highestDamage = 1;
		ArrayList<Card> field1 = this.field;
		for (int i = 0, field1Size = field1.size(); i < field1Size; i++) {
			Card card = field1.get(i);
			if (card.isAlive()) {
				int lostHp = card.getLostHp();
				if (lostHp >= highestDamage) {
					if (lostHp > highestDamage) {
						highestDamage = lostHp;
						result.clear();
					}
					result.add(card);
				}
			}
		}
		return result.size() == 1 ? result.get(0) : Helper.pick(result);
	}

	public int countCardsInField() {
		return Helper.count(this.field, CardPredicates.ON_FIELD);
	}

	public int countCardsInField(Predicate<? super Card> predicate) {
		return Helper.count(this.field, CardPredicates.ON_FIELD.and(predicate));
	}

	public boolean hasCardInField() {
		return this.hasCardsInField(1);
	}

	public boolean hasCardsInField(int n) {
		return Helper.atLeast(n, this.field, CardPredicates.ON_FIELD);
	}

	public boolean hasCardInField(Predicate<? super Card> predicate) {
		return hasCardsInField(1, predicate);
	}

	public boolean hasCardsInField(int n, Predicate<? super Card> predicate) {
		return Helper.atLeast(n, this.field, CardPredicates.ON_FIELD.and(predicate));
	}

	public int getIndexOnField(Card sourceCard) {
		return this.field.indexOf(sourceCard);
	}

	public Card atIndexOnField(int index) {
		if (0 <= index && index < this.field.size()) {
			Card card = this.field.get(index);
			if (card.isAlive())
				return card;
		}
		return null;
	}

	public Card oppositeCardOnField(Card sourceCard) {
		int index = this.field.indexOf(sourceCard);
		return opponent.atIndexOnField(index);
	}

	// -- Cemetery accessors

	public Card pickCardInCemetery() {
		return Helper.pick(this.cemetery);
	}

	public Card pickCardInCemetery(Predicate<? super Card> predicate) {
		return Helper.pick(this.cemetery, predicate);
	}

	public int countCardsInCemetery() {
		return this.cemetery.size();
	}

	public int countCardsInCemetery(Predicate<? super Card> predicate) {
		return Helper.count(this.cemetery, predicate);
	}

	public boolean hasCardInCemetery() {
		return !this.cemetery.isEmpty();
	}

	public boolean hasCardsInCemetery(int n) {
		return this.countCardsInCemetery() >= n;
	}

	public boolean hasCardsInCemetery(int n, Predicate<? super Card> predicate) {
		return Helper.atLeast(n, this.cemetery, predicate);
	}

	// -- Hand accessor

	void addCardToHand(Card card) {
		if (this.countCardsInHand() < MAX_HAND_SIZE) {
			this.hand.add(card.reset());

			if (Constants.IS_DEBUG)
				L.debug(card + " added to hand");

			if (card.hasPreemptiveStrike()) {
				preemptiveStrikeCards.add(card);
			}
		}
		else {
			throw new IllegalStateException("Hand is full, you should not add any card there");
		}
	}

	public void removeCardFromHand(Card card) {
		removeCardFromHand(this.hand.indexOf(card), true);
	}

	private Card removeCardFromHand(int index, boolean setPlaceHolder) {
		Card card;
		if (setPlaceHolder) {
			card = this.hand.set(index, null); // replace by null to avoid issues on playCardsFromHand
		}
		else {
			card = this.hand.remove(index);
		}
		if (card.hasPreemptiveStrike()) {
			preemptiveStrikeCards.remove(card);
		}
		if (Constants.IS_DEBUG)
			L.debug(card + " removed from hand.");

		return card;
	}

	public void consumeCardsHand(Consumer<? super Card> consumer) {
		for (int i = 0, s = this.hand.size(); i < s; i++) {
			Card card = this.hand.get(i);
			if (card != null) {
				consumer.accept(card);
			}
		}
	}

	public int countCardsInHand() {
		return Helper.count(this.hand, CustomPredicates.notNull());
	}

	public int countCardsInHand(Predicate<? super Card> predicate) {
		return Helper.count(this.hand, CustomPredicates.<Card>notNull().and(predicate));
	}

	public boolean hasCardsInHand(int n) {
		return this.countCardsInHand() >= n;
	}

	public boolean isCemeteryLocked() {
		if (status.hasSoulImprison() || opponent.status.hasSoulImprison()) {
			if (Constants.IS_DEBUG) {
				L.info(this + " cemetery is locked!");
			}
			return true;
		}
		return false;
	}

	public boolean isResurrectionLocked() {
		if (status.hasSuppressing() || opponent.status.hasSuppressing()) {
			if (Constants.IS_DEBUG) {
				L.info(this + " resurrection suppressed !");
			}
			return true;
		}
		return false;
	}

	public boolean hasCardsInHand(int n, Predicate<? super Card> predicate) {
		return Helper.atLeast(n, this.hand, CustomPredicates.<Card>notNull().and(predicate));
	}

	public Card pickFirstHighestTimerCardInHand(boolean remove) {
		return pickFirstHighestTimerCard(this.hand, remove);
	}

	public Card pickFirstLowestTimerCard() {
		if (this.hand.isEmpty()) {
			return null;
		}
		if (this.hand.size() == 1) {
			return this.hand.get(0);
		}

		int lowest = Integer.MAX_VALUE;
		Card lowestCard = null;
		ArrayList<Card> hand1 = this.hand;
		for (int i = 0, hand1Size = hand1.size(); i < hand1Size; i++) {
			Card card = hand1.get(i);
			if (card != null) {
				int t = card.getCurrentTimer();
				if (t < lowest) {
					lowest = t;
					lowestCard = card;
				}
			}
		}

		return lowestCard;
	}

	public String printField() {
		return this.field.toString();
	}

	public String printHand() {
		return this.hand.toString();
	}

	public String printDeck() {
		return this.deck.toString();
	}

	public String printCemetery() {
		return this.cemetery.toString();
	}

	public String toDeckFileString() {
		StringBuilder sb = new StringBuilder(1536);
		sb.append("# ").append(Constants.TITLE).append(" - Generated RM deck file on ").append(Constants.DATE_FORMAT.format(new Date())).append(NL)
			.append("Player name: ").append(desc.playerName).append(NL)
			.append("Player level: ").append(desc.playerLevel).append(NL)
			.append(NL)
			.append("# Cards").append(NL);

		for (CardDesc desc : desc.cardsDescs) {
			sb.append(CardDescFormat.toFullDescription(desc)).append(NL);
		}

		sb
			.append(NL)
			.append("# Runes").append(NL);

		for (RuneDesc desc : desc.runesDescs) {
			sb.append(RuneDescFormat.toFullDescription(desc)).append(NL);
		}

		return sb.toString();
	}

	private static List<Card> pickHighestTimerCards(List<Card> cards, int n, boolean remove) {
		if (cards.isEmpty() || n == 0) {
			return Collections.emptyList();
		}

		List<Card> result;
		if (cards.size() <= n) {
			result = new ArrayList<>(cards);
		}
		else {
			result = cards.stream()
				.sorted(Comparator.comparingInt(Card::getCurrentTimer).reversed())
				.limit(n)
				.collect(Collectors.toList());
		}

		if (remove) {
			cards.removeAll(result);
		}
		return result;
	}

	private static Card pickFirstHighestTimerCard(List<Card> cards, boolean remove) {
		if (cards.isEmpty()) {
			return null;
		}
		if (cards.size() == 1) {
			return remove ? cards.remove(0) : cards.get(0);
		}

		int highest = -1;
		int highestCard = -1;
		for (int i = 0; i < cards.size(); i++) {
			Card card = cards.get(i);
			if (card != null) {
				int t = card.getCurrentTimer();
				if (t > highest) {
					highest = t;
					highestCard = i;
				}
			}
		}

		return highestCard == -1 ? null : (remove ? cards.remove(highestCard) : cards.get(highestCard));
	}

	@Override
	public String toString() {
		return desc.playerName;
	}
}
