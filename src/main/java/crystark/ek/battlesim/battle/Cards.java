package crystark.ek.battlesim.battle;

import java.util.ArrayList;
import java.util.List;

import crystark.common.api.CardDesc;
import crystark.common.api.CardType;
import crystark.common.db.Database;
import crystark.common.db.model.BaseCard;
import crystark.common.format.CardDescFormat;
import rx.Observable;

public class Cards {
	public static List<Card> mostDamagedCards(List<Card> cards) {
		if (cards.size() < 2) {
			return cards;
		}
		List<Card> result = new ArrayList<>(cards.size());
		int highestDamage = 1;
		for (int i = 0, cardsSize = cards.size(); i < cardsSize; i++) {
			Card card = cards.get(i);
			if (card.isAlive()) {
				int damage = card.getLostHp();
				if (damage >= highestDamage) {
					if (damage > highestDamage) {
						highestDamage = damage;
						result.clear();
					}
					result.add(card);
				}
			}
		}

		return result;
	}

	public static Card firstHighestTimer(List<Card> cards) {
		if (cards.size() < 2) {
			return cards.isEmpty() ? null : cards.get(0);
		}
		int highest = -1;
		Card highestCard = null;
		for (Card card : cards) {
			int t = card.getCurrentTimer();
			if (t > highest) {
				highest = t;
				highestCard = card;
			}
		}

		return highestCard;
	}

	public static CardDesc getCardDesc(String description) {
		return CardDescFormat.parse(description);
	}

	public static Card from(String description, Player owner) {
		return from(description, owner, false);
	}

	public static Card from(String description, Player owner, boolean addToCardDesc) {
		CardDesc desc = CardDescFormat.parse(description);
		if (addToCardDesc) {
			owner.desc.cardsDescs.add(desc);
		}
		return new Card(desc, owner);
	}

	public static Observable<BaseCard> getDemons() {
		return Database.get().cards().all()
			.filter(c -> CardType.demon.equals(c.type()));
	}

	public static Observable<String> getDemonsNames() {
		return getDemons().map(BaseCard::name);
	}

	public static List<String> getDemonsNameList() {
		return getDemonsNames()
			.toSortedList()
			.toBlocking()
			.first();
	}
}
