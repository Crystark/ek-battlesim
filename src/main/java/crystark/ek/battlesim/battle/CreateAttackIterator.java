package crystark.ek.battlesim.battle;

import java.util.Iterator;

public class CreateAttackIterator implements Iterator<CardAttack> {

	private final Iterator<Card>	cards;
	private final int				damage;

	public CreateAttackIterator(Iterator<Card> cards, int damage) {
		this.cards = cards;
		this.damage = damage;
	}

	@Override
	public boolean hasNext() {
		return cards.hasNext();
	}

	@Override
	public CardAttack next() {
		Card card = cards.next();
		return new CardAttack(card, damage);
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
}
