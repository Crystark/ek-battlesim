package crystark.ek.battlesim.battle;

public class BattleException extends RuntimeException {

	public BattleException(String string, Throwable t) {
		super(string, t);
	}

}
