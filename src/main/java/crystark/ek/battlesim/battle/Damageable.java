package crystark.ek.battlesim.battle;

public interface Damageable {
	public int damaged(Card damagingCard, int damage, DamageType type);
}
