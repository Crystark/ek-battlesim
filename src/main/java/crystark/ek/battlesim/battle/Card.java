package crystark.ek.battlesim.battle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.function.Supplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import crystark.common.api.CardDesc;
import crystark.common.api.CardType;
import crystark.ek.Constants;
import crystark.ek.Helper;
import crystark.ek.battlesim.ability.AbilityFactory;
import crystark.ek.battlesim.ability.CompositeSkill;
import crystark.ek.battlesim.ability.IAbility;
import crystark.ek.battlesim.ability.IAfterAttack;
import crystark.ek.battlesim.ability.IAfterDamage;
import crystark.ek.battlesim.ability.IAfterHit;
import crystark.ek.battlesim.ability.IAfterTurn;
import crystark.ek.battlesim.ability.IAttack;
import crystark.ek.battlesim.ability.IAura;
import crystark.ek.battlesim.ability.IBeforeAttack;
import crystark.ek.battlesim.ability.IBeforeAttackDeboost;
import crystark.ek.battlesim.ability.IBeforeDamage;
import crystark.ek.battlesim.ability.IBeforeHeroDamage;
import crystark.ek.battlesim.ability.IBeforeRound;
import crystark.ek.battlesim.ability.IBeforeTurn;
import crystark.ek.battlesim.ability.IDeathWhisperAble;
import crystark.ek.battlesim.ability.IDesperationAble;
import crystark.ek.battlesim.ability.IPower;
import crystark.ek.battlesim.ability.IPreemptiveStrikeAble;
import crystark.ek.battlesim.ability.IQuickstrikeAble;
import crystark.ek.battlesim.ability.impl.ArmorLess;
import crystark.ek.battlesim.ability.impl.Reanimation;
import crystark.ek.battlesim.ability.impl.Resurrection;
import crystark.ek.battlesim.status.Burning;
import crystark.ek.battlesim.status.Poison;
import crystark.ek.battlesim.status.StatusEffect;
import crystark.ek.battlesim.status.StatusEffectType;

/**
 * @see CardTest
 */
public class Card extends AbilitiesSupport implements HasAtk, Damageable {
	private static final Logger L = LoggerFactory.getLogger(Card.class);

	public final  CardDesc desc;
	private final Player   baseOwner;
	private       Player   currentOwner;

	protected int      timer;
	protected long     currentBaseHp;
	protected long     currentBaseHpWithoutBonus;
	protected long     currentHp;
	protected int      currentBonusHp;
	protected int      currentBaseAtk;
	protected int      currentBaseAtkWithoutBonus;
	protected int      currentAtk;
	protected int      currentBonusAtk;
	protected int      currentMalusAtk;
	protected int      currentCraze;
	protected CardType currentType;

	//TODO Is not used?
	protected final HashMap<Long, IAbility> bonusAbilities = new HashMap<>();

	protected final ArrayList<StatusEffect> auras   = new ArrayList<>();
	protected final ArrayList<StatusEffect> buffs   = new ArrayList<>();
	protected final ArrayList<StatusEffect> debuffs = new ArrayList<>();
	protected final ArrayList<StatusEffect> boosts  = new ArrayList<>();
	protected final ArrayList<StatusEffect> hexes   = new ArrayList<>();

	protected final ArrayList<StatusEffect> recurringStatus = new ArrayList<>();
	private final   List<String>            markers         = new ArrayList<>();

	private boolean dirty = false;

	private boolean isLacerated    = false;
	private boolean isShocked      = false;
	private boolean isStunned      = false;
	private boolean isFrozen       = false;
	private boolean isTrapped      = false;
	private boolean isConfused     = false;
	private boolean isGoingExtinct = false;
	private boolean isSilenced     = false;
	private int     isBlind        = 0;
	private int     isSpellMarked  = 0;
	boolean isLastChance = false;

	private boolean beingKilled = false;
	private boolean onField     = false;
	private int     iterOnField = 0;

	public Card(CardDesc cardDesc, Player owner) {
		this.desc = cardDesc;
		this.baseOwner = owner;
		this.currentOwner = owner;

		List<Supplier<IAbility>> suppliers = AbilityFactory.getSuppliers(desc);
		for (int i = 0, suppliersSize = suppliers.size(); i < suppliersSize; i++) {
			Supplier<IAbility> s = suppliers.get(i);
			IAbility ability = s.get();
			if (ability != null) {
				this.addAbility(ability);
			}
		}
		memoAbilitiesState();
		this.init();
	}

	public CardDesc snapshot() {
		long health = Math.min(this.currentHp, desc.health);
		return new CardDesc(desc.uniqueName, desc.name, desc.cost, desc.evolution, desc.level, desc.timer, desc.attack, health, desc.type, desc.conflictType, desc.limit, desc.abilities, desc.tokens);
	}

	public void decreaseTimer() {
		this.decreaseTimer(1);
	}

	public void decreaseTimer(int value) {
		int prev = this.timer;
		this.timer -= Math.min(value, this.timer);
		if (Constants.IS_DEBUG && prev != this.timer)
			L.debug(this + " wait time changed from  " + prev + " to " + this.timer);
	}

	public void increaseTimer(int value) {
		int prev = this.timer;
		this.timer += value;
		if (Constants.IS_DEBUG && prev != this.timer)
			L.debug(this + " wait time changed from  " + prev + " to " + this.timer);
	}

	public boolean isReady() {
		return this.timer == 0;
	}

	public void markDirty() {
		this.dirty = true;
	}

	public Card reset() {
		if (dirty) {
			this.init();
			this.resetAbilities();
			this.dirty = false;
		}
		return this;
	}

	private void init() {
		this.timer = desc.timer;
		this.currentHp = desc.health;
		this.currentBaseHp = desc.health;
		this.currentBaseHpWithoutBonus = desc.health;
		this.currentBonusHp = 0;
		this.currentAtk = desc.attack;
		this.currentBaseAtk = desc.attack;
		this.currentBaseAtkWithoutBonus = desc.attack;
		this.currentBonusAtk = 0;
		this.currentMalusAtk = 0;
		this.currentCraze = 0;
		this.currentType = desc.type;

		this.auras.clear();
		this.buffs.clear();
		this.debuffs.clear();
		this.boosts.clear();
		this.hexes.clear();

		this.recurringStatus.clear();
		this.markers.clear();

		this.isLacerated = false;
		this.isShocked = false;
		this.isStunned = false;
		this.isGoingExtinct = false;
		this.isFrozen = false;
		this.isTrapped = false;
		this.isConfused = false;
		this.isBlind = 0;
		this.isLastChance = false;
		this.beingKilled = false;
		this.onField = false;
	}

	private boolean existsNonSilent(List<? extends IAbility> list) {
		return !(isSilenced || list.isEmpty());
		/*for (int i = 0, listSize = list.size(); i < listSize; i++) {
			IAbility a = list.get(i);
			if (a.canBeLaunched(this))
				return true;
		}
		return false;*/
	}

	//Can't be silenced
	public boolean hasGuard() {
		return !guardList.isEmpty();
	}

	public boolean hasImmunityIgnoreSilence() {
		return !immunityList.isEmpty();
	}

	public boolean hasImmunity() {
		return existsNonSilent(immunityList);
	}

	public boolean hasEvasion() {
		return existsNonSilent(evasionList);
	}

	public boolean hasReflectionIgnoreSilence() {
		return !reflectionList.isEmpty();
	}

	public boolean hasReflection() {
		return existsNonSilent(reflectionList);
	}

	/**
	 * This was created to easily handle cases where reflection would be added to an immune card
	 */
	public boolean hasReflectionOrNotImmune() {
		return hasReflection() || !hasImmunity();
	}

	//Can't be silenced
	public boolean hasResistance() {
		return !resistanceList.isEmpty();
	}

	public boolean hasIceShieldIgnoreSilence() {
		return !iceShieldList.isEmpty();
	}

	public boolean hasInfiltrator() {
		return existsNonSilent(infiltratorList);
	}

	private boolean hasArmorLess() {
		return existsNonSilent(armorLessList);
	}

	public boolean hasBeforeRound() {
		return existsNonSilent(beforeRoundAbilities);
	}

	public boolean hasReanimateIgnoreSilence() {
		return !reanimateList.isEmpty();
	}

	public boolean hasSoulImprison() {
		return existsNonSilent(soulImprisonList);
	}

	public boolean hasSpiritualVoice() {
		return !spiritualVoiceList.isEmpty(); // Was baseSpiritualVoiceCount > 0;
	}

	public boolean hasSuppressing() {
		return existsNonSilent(suppressingList);
	}

	public boolean hasPreemptiveStrike() {
		return !preemptiveStrikeAbilities.isEmpty();
	}

	public boolean hasDeathWhisper() {
		return !deathWhisperAbilities.isEmpty();
	}

	public boolean isSummoned() {
		return false;
	}

	public boolean isGoingExtinct() {
		return isGoingExtinct;
	}

	public boolean isConfused() {
		return isConfused;
	}

	public boolean isBlind() {
		return isBlind > 0;
	}

	public boolean isSpellMarked() {
		return isSpellMarked > 0;
	}

	public boolean isLacerated() {
		return isLacerated;
	}

	public boolean isSilenced() {
		return isSilenced;
	}

	public boolean isTrapped() {
		return isTrapped;
	}

	public boolean isFrozen() {
		return isFrozen;
	}

	public boolean isStunned() {
		return isStunned;
	}

	public boolean isShocked() {
		return isShocked;
	}

	public boolean isLastChance() {
		return isLastChance;
	}

	public boolean isPoisonned() {
		ArrayList<StatusEffect> recurringStatus1 = this.recurringStatus;
		for (int i = 0, recurringStatus1Size = recurringStatus1.size(); i < recurringStatus1Size; i++) {
			StatusEffect se = recurringStatus1.get(i);
			if (se instanceof Poison)
				return true;
		}
		return false;
	}

	public boolean isBurning() {
		ArrayList<StatusEffect> recurringStatus1 = this.recurringStatus;
		for (int i = 0, recurringStatus1Size = recurringStatus1.size(); i < recurringStatus1Size; i++) {
			StatusEffect se = recurringStatus1.get(i);
			if (se instanceof Burning)
				return true;
		}
		return false;
	}

	/**
	 * Check if the card is still alive
	 */
	public boolean isAlive() {
		return this.currentHp > 0 && this.onField && !this.beingKilled;
	}

	//	private boolean canAct() {
	//		return this.hasTurn() && !this.isFrozen && !this.isTrapped && !this.isStunned && !this.isConfused;
	//	}

	public boolean canAttack() {
		return !this.isFrozen && !this.isTrapped && !this.isStunned && !this.isConfused && !this.isShocked;
	}

	public boolean canUseSkill() {
		return !this.isFrozen && !this.isTrapped && !this.isStunned && !this.isConfused && !isSilenced;
	}

	public boolean canBeReanimated() {
		return !this.hasReanimateIgnoreSilence() && !desc.abilities.isEmpty(); // Cards with no abilities don't create reanimated
	}

	public boolean canBeSacrificed() {
		return !this.hasImmunity() && !isLastChance;
	}

	//----------------------
	// Special ATK changes
	//----------------------

	public void wickedLeechRemoveAtk(int modifier) {
		this.modifyBaseAtk(-modifier); // Removes base atk
	}

	public void crazeAddAtk(int modifier) {
		this.currentCraze += modifier;
		this.modifyBaseAtk(modifier); // Adds base atk
	}

	//-------------
	// BONUS ATK
	//-------------

	/**
	 * Adds atk originating from auras
	 */
	@Override
	public void addBonusAtk(int modifier) {
		this.currentBonusAtk += modifier;
		this.modifyBaseAtk(modifier);
	}

	/**
	 * Removes atk originating from auras
	 */
	@Override
	public void removeBonusAtk(int modifier) {
		if (modifier > this.currentBonusAtk) {
			throw new IllegalStateException("You asked to remove " + modifier + " bonus atk but current bonus is only of " + currentBonusAtk);
		}
		this.currentBonusAtk -= modifier;

		// We take MIN(currentBaseAtkWithoutBonus + currentBonusAtk + 2 * currentCraze, currentBaseAtk)
		int attackWithBonus = currentBaseAtkWithoutBonus + currentBonusAtk + 2 * currentCraze;
		if (this.currentBaseAtk > attackWithBonus) {
			this.modifyBaseAtk(attackWithBonus - this.currentBaseAtk);
		}
	}

	//-------------
	// MALUS ATK
	//-------------

	public int getCurrentMalusAtk() {
		return this.currentMalusAtk;
	}

	@Override
	public void addMalusAtk(int modifier) {
		this.currentMalusAtk += modifier;
		this.modifyAtk(-modifier);
	}

	@Override
	public void removeMalusAtk(int modifier) {
		if (modifier > this.currentMalusAtk) {
			throw new IllegalStateException("You asked to remove " + modifier + " malus atk but current malus is only of " + currentMalusAtk);
		}
		this.currentMalusAtk -= modifier;
		this.modifyAtk(modifier);
	}

	//--------------
	// BASE ATK
	//--------------

	public void addBaseAtk(int modifier) {
		this.currentBaseAtkWithoutBonus += modifier;
		modifyBaseAtk(modifier);
	}

	public void removeBaseAtk(int modifier) {
		this.currentBaseAtkWithoutBonus = Math.max(0, this.currentBaseAtkWithoutBonus - modifier);
		modifyBaseAtk(-modifier);
	}

	private void modifyBaseAtk(int modifier) {
		this.currentBaseAtk = Math.max(0, this.currentBaseAtk + modifier);

		if (Constants.IS_DEBUG)
			L.debug(this + " base atk modified by " + modifier + ". New base atk: " + currentBaseAtk);

		this.modifyAtk(modifier);
	}

	@Override
	public int getCurrentBaseAtk() {
		return this.currentBaseAtk;
	}

	//--------------
	// ATK
	//--------------
	@Override
	public void modifyAtk(int modifier) {
		this.currentAtk = Math.max(0, this.currentAtk + modifier);

		if (Constants.IS_DEBUG)
			L.debug(this + " atk modified by " + modifier + ". New atk: " + currentAtk);
	}

	@Override
	public int getCurrentAtk() {
		return this.currentAtk;
	}

	//----------------------
	// Special HP changes
	//----------------------

	public void coldBloodRemoveHp(int modifier) {
		this.modifyBaseHp(-modifier); // Removes base hp
	}

	//-------------
	// BONUS HP
	//-------------

	/**
	 * Adds hp originating from auras
	 */
	public void addBonusHp(int modifier) {
		this.currentBonusHp += modifier;
		this.modifyBaseHp(modifier);
	}

	/**
	 * Removes hp originating from auras
	 */
	public void removeBonusHp(int modifier) {
		if (modifier > this.currentBonusHp) {
			throw new IllegalStateException("You asked to remove " + modifier + " bonus hp but current bonus is only of " + currentBonusHp);
		}
		this.currentBonusHp -= modifier;

		// We take MIN(currentBaseAtkWithoutBonus + currentBonusAtk + 2 * currentCraze, currentBaseAtk)
		long healthWithBonus = currentBaseHpWithoutBonus + currentBonusHp;
		if (this.currentBaseHp > healthWithBonus) {
			this.modifyBaseHp((int) (healthWithBonus - this.currentBaseHp));
		}
	}

	//--------------
	// BASE HP
	//--------------

	public void addBaseHp(int modifier) {
		this.currentBaseHpWithoutBonus += modifier;
		modifyBaseHp(modifier);
	}

	public void removeBaseHp(int modifier) {
		this.currentBaseHpWithoutBonus = Math.max(0, this.currentBaseHpWithoutBonus - modifier);
		modifyBaseHp(-modifier);
	}

	private void modifyBaseHp(int modifier) {
		this.currentBaseHp += modifier;
		int hpModifier = 0;
		if (modifier < 0) {
			if (this.currentBaseHp < 0)
				this.currentBaseHp = 0;
			// If base hp are removed then current hp should only be removed if overflowing
			if (this.currentHp > this.currentBaseHp) {
				hpModifier = (int) (this.currentBaseHp - this.currentHp);
			}
		}
		else {
			// Always add bonus base hp to current hp
			hpModifier = modifier;
		}
		if (Constants.IS_DEBUG)
			L.debug(this + " base hp modified by " + modifier + ". New base hp: " + currentBaseHp);

		if (hpModifier != 0)
			this.modifyHp(hpModifier, true);
	}

	public long getCurrentBaseHp() {
		return this.currentBaseHp;
	}

	//--------------
	// HP
	//--------------

	private void modifyHp(int modifier, boolean sendToCemeteryIfKilled) {
		if (modifier != 0) {
			this.currentHp = Math.max(0, this.currentHp + modifier);
			if (Constants.IS_DEBUG)
				L.debug(this + " hp modified by " + modifier + ". New hp: " + currentHp);
			if (this.currentHp == 0) {
				if (sendToCemeteryIfKilled) {
					Player.sendCardToCemetery(this, true);
				}
				else {
					this.markBeingKilled(true);
				}
			}
		}
	}

	public long getCurrentHp() {
		return this.currentHp;
	}

	public void heal(int h) {
		if (!this.isLacerated)
			this.regen(h);
	}

	public void regen(int h) {
		this.modifyHp(Math.min(h, this.getLostHp()), true);
	}

	public int getLostHp() {
		return (int) (this.currentBaseHp - this.currentHp);
	}

	public int getBaseLostHp() {
		return (int) Math.max(desc.health - this.currentHp, 0);
	}

	public boolean isDamaged() {
		return this.currentBaseHp > this.currentHp;
	}

	private boolean checkBlindness() {
		return !isBlind() || this.hasInfiltrator() || !Helper.getLucky(isBlind);
	}

	/**
	 * Does one attack and manages hitAbilities and counterAbilities
	 */
	public int attack(final Card defendingCard, int damage) {
		try {
			if (this.checkBlindness()) {
				long hpBefore = defendingCard.currentHp;
				int reducedDamage = defendingCard.damaged(this, damage, DamageType.attack, false); // attack power after beeing reduced
				boolean stillAlive = defendingCard.isAlive();
				int realDamage = (int) (hpBefore - defendingCard.currentHp); // real hp removed
				if (realDamage > 0 || defendingCard.isLastChance) { // If last chance reduced damage to 0, skills still activate
					this.applyAfterHitAbilities(defendingCard, realDamage); // BloodSucker, BloodThirsty, Execution, Puncture
					defendingCard.applyAfterDamageAbilities(this, reducedDamage); // Counterattack, ShieldOfEarth, Wicked Leech
					this.applyAfterAttackAbilities(defendingCard, realDamage); //Melting, BreakIce, MultyKilling
				}
				if (!stillAlive && defendingCard.onField) { // applies death only given by initial damage
					defendingCard.markBeingKilled(false);
					Player.sendCardToCemetery(defendingCard, true);
				}
				return reducedDamage;

			}
			return 0;
		}
		catch (Throwable t) {
			throw new BattleException("Error on " + this + " attacks '" + defendingCard, t);
		}
	}

	public CardAction prepareAction() {
		return new CardAction(this);
	}

	@Override
	public int damaged(Card damagingCard, int damage, DamageType type) {
		return damaged(damagingCard, damage, type, true);
	}

	/**
	 * Applies damage to the card after possibly reducing it
	 *
	 * @return the real damage done
	 */
	private int damaged(Card damagingCard, int damage, DamageType type, boolean sendToCemeteryIfKilled) {
		if (damage < 0) {
			throw new IllegalArgumentException("Damage should never be negative. Got " + damage);
		}
		if (isLastChance) {
			if (Constants.IS_DEBUG)
				L.debug(this + " can't be damaged due to last chance !");
			return 0;
		}

		int reducedDamage = this.applySpellMark(damage, type);

		boolean isDefenceEnabled = true;
		if (DamageType.attack == type && damagingCard != null) {
			for (int i = 0, armorLessListSize = damagingCard.armorLessList.size(); i < armorLessListSize; i++) {
				ArmorLess armorLess = damagingCard.armorLessList.get(i);
				if (armorLess.canBeLaunched(damagingCard) && damagingCard.canUseSkill() && Helper.getLucky(armorLess.getValue())){
					isDefenceEnabled = false;
					break;
				}
			}
		}

		if (isDefenceEnabled) {
			FTArrayList<IBeforeDamage> abilities = this.damageReducerAbilities;
			//damageReducerAbilities size may decrease if card that gives IBeforeDamage aura will die (Taunt skill)
			for (int i = 0; i < abilities.size(); i++) {
				IBeforeDamage damageReducer = abilities.get(i);
				if (damageReducer.canBeLaunched(this)) {
					int oldDamage = reducedDamage;
					reducedDamage = damageReducer.onBeforeDamage(this, damagingCard, reducedDamage, type);
					if (oldDamage != reducedDamage) {
						if (Constants.IS_DEBUG)
							L.debug(this + "'s " + damageReducer.getName() + " reduced damage to " + reducedDamage);
						if (reducedDamage == 0) {
							break;
						}
					}
				}
			}
		}

		if (Constants.IS_DEBUG)
			L.debug(this + " takes " + reducedDamage + ' ' + type.name() + " damage");

		this.modifyHp(-reducedDamage, sendToCemeteryIfKilled);

		return reducedDamage;
	}


	private int applySpellMark(int damage, DamageType type) {
		if (isSpellMarked()) {
			switch (type) {
				case fire:
				case ice:
				case lightning:
				case blood:
					return (int) (damage * (1 + isSpellMarked / 100f));
				default:
					break;
			}
		}
		return damage;
	}

	/**
	 * Applies attack abilities until all attacks are done or the attacking card is dead
	 */
	public void applyAttackAbilities(Card defendingCard, int unreducedDamage, int damage) {
		FTArrayList<IAttack> attackAbilities1 = this.attackAbilities;
		for (int i = 0, attackAbilities1Size = attackAbilities1.size(); i < attackAbilities1Size; i++) {
			IAttack attack = attackAbilities1.get(i);
			if (attack.canBeLaunched(this)) {
				Iterator<CardAttack> attacks = attack.attacksIterator(defendingCard, unreducedDamage, damage);
				while (attacks.hasNext()) {
					CardAttack cardAttack = attacks.next();
					if (cardAttack.cardToAttack.isAlive()) {
						if (Constants.IS_DEBUG) {
							L.debug(this + " uses attack " + attack.getName() + " on " + cardAttack.cardToAttack);
						}
						int attackDamage = cardAttack.damage == null ? this.currentAtk : cardAttack.damage;
						this.attack(cardAttack.cardToAttack, attackDamage);
						if (!this.isAlive()) // TODO check if iter on field still the same
							return;
					}
				}
			}
		}
	}

	public void applyBeforeTurnAbilities() {
		FTArrayList<IBeforeTurn> onTurnStartAbilities1 = this.onTurnStartAbilities;
		for (int i = 0, onTurnStartAbilities1Size = onTurnStartAbilities1.size(); i < onTurnStartAbilities1Size; i++) {
			IBeforeTurn onTurnStart = onTurnStartAbilities1.get(i);
			if (onTurnStart.canBeLaunched(this)) {
				if (Constants.IS_DEBUG)
					L.debug(this + " tries to use onTurnStart " + onTurnStart.getName());
				onTurnStart.apply(this);
			}
		}
	}

	public void applyPowerAbilities() {
		FTArrayList<IPower> powerAbilities1 = this.powerAbilities;
		for (int i = 0, powerAbilities1Size = powerAbilities1.size(); i < powerAbilities1Size; i++) {
			IPower power = powerAbilities1.get(i);
			if (power.canBeLaunched(this)) {
				if (Constants.IS_DEBUG)
					L.debug(this + " tries to use power " + power.getName());
				power.apply(this);
				if (!this.isAlive())
					break;
			}
		}
	}


	public void applyPreemptiveStrikes() {
		FTArrayList<IPreemptiveStrikeAble> preemptiveStrikeAbilities1 = this.preemptiveStrikeAbilities;
		for (int i = 0, preemptiveStrikeAbilities1Size = preemptiveStrikeAbilities1.size(); i < preemptiveStrikeAbilities1Size; i++) {
			IPreemptiveStrikeAble preemptiveStrike = preemptiveStrikeAbilities1.get(i);
			if (preemptiveStrike.canBeLaunched(this)) {
				if (Constants.IS_DEBUG)
					L.debug(this + " tries to use preemptive strike " + preemptiveStrike.getName());
				preemptiveStrike.apply(this); // null given as a source card as the card is not on the field
			}
		}
	}

	public void applyDeathWhispers() {
		FTArrayList<IDeathWhisperAble> deathWhisperAbilities1 = this.deathWhisperAbilities;
		for (int i = 0, deathWhisperAbilities1Size = deathWhisperAbilities1.size(); i < deathWhisperAbilities1Size; i++) {
			IDeathWhisperAble deathWhisper = deathWhisperAbilities1.get(i);
			if (deathWhisper.canBeLaunched(this)) {
				if (Constants.IS_DEBUG)
					L.debug(this + " tries to use death whisper " + deathWhisper.getName());
				deathWhisper.apply(this); // null given as a source card as the card is not on the field
			}
		}
	}

	public void applyQuickStrikes() {
		if (isSummoned()) return;
		FTArrayList<IQuickstrikeAble> quickstrikeAbilities1 = this.quickstrikeAbilities;
		for (int i = 0, quickstrikeAbilities1Size = quickstrikeAbilities1.size(); i < quickstrikeAbilities1Size; i++) {
			IQuickstrikeAble quickstrike = quickstrikeAbilities1.get(i);
			if (quickstrike.canBeLaunched(this)) {
				if (Constants.IS_DEBUG)
					L.debug(this + " tries to use quickstrike " + quickstrike.getName());
				quickstrike.apply(this);
				if (!this.isAlive())
					break;
			}
		}
	}

	public void applyDesperations() {
		if (isSummoned()) return;
		FTArrayList<IDesperationAble> desperationAbilities1 = this.desperationAbilities;
		for (int i = 0, desperationAbilities1Size = desperationAbilities1.size(); i < desperationAbilities1Size; i++) {
			IDesperationAble desperation = desperationAbilities1.get(i);
			if (desperation.canBeLaunched(this)) {
				if (Constants.IS_DEBUG)
					L.debug(this + " tries to use desperation " + desperation.getName());
				desperation.apply(this);
				// if a desperation brought the card back to life, stop activating desperations
				if (this.isAlive())
					break;
			}
		}
	}

	public void applyAfterHitAbilities(Card hitCard, int damageDone) {
		FTArrayList<IAfterHit> abilities = this.onAfterHitAbilities;
		for (int i = 0; i < abilities.size(); i++) {
			IAfterHit onhit = abilities.get(i);
			if (onhit.canBeLaunched(this)) {
				if (Constants.IS_DEBUG)
					L.debug(this + " tries to use onhit " + onhit.getName());
				onhit.onAfterHit(this, hitCard, damageDone);
				if (!this.isAlive())
					break;
			}
		}
	}

	public void applyAfterAttackAbilities(Card hitCard, int damageDone) {
		for (int i = 0; i < afterAttackAbilities.size(); i++) {
			IAfterAttack afterHit = afterAttackAbilities.get(i);
			if (afterHit.canBeLaunched(this)) {
				if (Constants.IS_DEBUG)
					L.debug(this + " tries to use afterHit " + afterHit.getName());
				afterHit.onAfterAttack(this, hitCard, damageDone);
				if (!this.isAlive())
					break;
			}
		}
	}

	public void applyAfterDamageAbilities(Card attackingCard, int damageTaken) {
		int originalIter = iterOnField;
		try {
			/*if (Constants.IS_DEBUG)
				L.debug(this + " countering with " + this.counterAbilities);*/
			FTArrayList<IAfterDamage> counters = this.counterAbilities;
			for (int i = 0; i < counters.size(); i++) {
				IAfterDamage counter = counters.get(i);
				if (counter.canBeLaunched(this)) {
					if (Constants.IS_DEBUG)
						L.debug(this + " tries to use counter " + counter.getName());
					counter.onAfterDamage(this, attackingCard, damageTaken);
					if (originalIter != iterOnField) {
						// card was killed by the desperation of a card the counter killed
						if (Constants.IS_DEBUG)
							L.debug(this + " counters are aborted because the card isn't on the field anymore.");
						break;
					}
				}
			}
		}
		catch (Exception e) {
			// TODO try afterHit
			// pb is it's not on the field anymore when counters trigger so originalIter == iterOnField
			System.out.println(this);
			System.out.println(this.onField);
			System.out.println(attackingCard);
			System.out.println(damageTaken);
			System.out.println(this.counterAbilities);
			throw e;
		}
	}

	public void applyAfterTurnAbilities() {
		FTArrayList<IAfterTurn> onFinishedAbilities1 = this.onFinishedAbilities;
		for (int i = 0, onFinishedAbilities1Size = onFinishedAbilities1.size(); i < onFinishedAbilities1Size; i++) {
			IAfterTurn onfinished = onFinishedAbilities1.get(i);
			if (onfinished.canBeLaunched(this)) {
				if (Constants.IS_DEBUG)
					L.debug(this + " tries to use onfinished " + onfinished.getName());
				onfinished.onTurnFinish(this);
			}
		}
	}

	public void applyAurasTo(Card card) {
		FTArrayList<IAura> auras = this.auraAbilities;
		for (int i = 0, size = auras.size(); i < size; i++) {
			IAura aura = auras.get(i);
			if (aura.canBeLaunched(this) && aura.onReceiveAura(card, this) && Constants.IS_DEBUG) {
				L.debug(card + " received aura " + aura.getName() + " from " + this);
			}
		}
	}

	public void applyAuras() {
		FTArrayList<IAura> auraAbilities1 = this.auraAbilities;
		for (int i = 0, auraAbilities1Size = auraAbilities1.size(); i < auraAbilities1Size; i++) {
			IAura aura = auraAbilities1.get(i);
			if (aura.canBeLaunched(this)) {
				if (Constants.IS_DEBUG)
					L.debug(this + " tries to use aura " + aura.getName());
				aura.onEnterField(this);
			}
		}
	}

	public void applyBoosts() {
		FTArrayList<IBeforeAttackDeboost> deboostAbilities1 = this.deboostAbilities;
		for (int i = 0, deboostAbilities1Size = deboostAbilities1.size(); i < deboostAbilities1Size; i++) {
			IBeforeAttackDeboost deboost = deboostAbilities1.get(i);
			if (Constants.IS_DEBUG)
				L.debug(this + " deboosted by " + deboost.getName());
			deboost.onBeforeAttack(this);
		}
		FTArrayList<IBeforeAttack> boostAbilities1 = this.boostAbilities;
		for (int i = 0, boostAbilities1Size = boostAbilities1.size(); i < boostAbilities1Size; i++) {
			IBeforeAttack boost = boostAbilities1.get(i);
			if (boost.canBeLaunched(this)) {
				if (Constants.IS_DEBUG)
					L.debug(this + " tries to use boost " + boost.getName());
				boost.onBeforeAttack(this);
			}
		}
	}

	public void addAbility(Long key, IAbility a) {
		this.bonusAbilities.put(key, a);
		this.addAbility(a);
	}

	public void addAbility(IAbility a) {

		if (a instanceof CompositeSkill) {
			List<IAbility> abilities = ((CompositeSkill) a).getAbilities();
			for (int i = 0, abilitiesSize = abilities.size(); i < abilitiesSize; i++) {
				IAbility ability = abilities.get(i);
				this.addAbility(ability);
			}
		}
		else {

			this.allAbilities.add(a);

			// Can be QS, Desp, PS, DW or IPower. "Reanimation" affects condition that card is disallowed to be reanimated
			if (a instanceof Reanimation){
				a.accept(getAbilitiesAddVisitor());
			}

			if (a instanceof IQuickstrikeAble && ((IQuickstrikeAble) a).isQuickstrike()) {
				this.quickstrikeAbilities.add((IQuickstrikeAble) a);
			}
			else if (a instanceof IDesperationAble && ((IDesperationAble) a).isDesperation()) {
				this.desperationAbilities.add((IDesperationAble) a);
			}
			else if (a instanceof IPreemptiveStrikeAble && ((IPreemptiveStrikeAble) a).isPreemptiveStrike()) {
				this.preemptiveStrikeAbilities.add((IPreemptiveStrikeAble) a);
			}
			else if (a instanceof IDeathWhisperAble && ((IDeathWhisperAble) a).isDeathWhisper()) {
				this.deathWhisperAbilities.add((IDeathWhisperAble) a);
			}
			else if (!(a instanceof Reanimation)){
				a.accept(getAbilitiesAddVisitor());
			}
		}
	}

	public void removeAbility(Long key) {
		this.removeAbility(this.bonusAbilities.remove(key));
	}

	public void removeAbility(IAbility a) {
		if (a instanceof CompositeSkill) {
			List<IAbility> abilities = ((CompositeSkill) a).getAbilities();
			for (int i = 0, abilitiesSize = abilities.size(); i < abilitiesSize; i++) {
				IAbility ability = abilities.get(i);
				this.removeAbility(ability);
			}
		}
		else {

			this.allAbilities.remove(a);

			// Can be QS, Desp, PS, DW or IPower. "Reanimation" affects condition that card is disallowed to be reanimated
			if (a instanceof Reanimation){
				a.accept(getAbilitiesRemoveVisitor());
			}

			if (a instanceof IQuickstrikeAble && ((IQuickstrikeAble) a).isQuickstrike()) {
				this.quickstrikeAbilities.remove(a);
			}
			else if (a instanceof IDesperationAble && ((IDesperationAble) a).isDesperation()) {
				this.desperationAbilities.remove(a);
			}
			else if (a instanceof IPreemptiveStrikeAble && ((IPreemptiveStrikeAble) a).isPreemptiveStrike()) {
				this.preemptiveStrikeAbilities.remove(a);
			}
			else if (a instanceof IDeathWhisperAble && ((IDeathWhisperAble) a).isDeathWhisper()) {
				this.deathWhisperAbilities.remove(a);
			}
			else if (!(a instanceof Reanimation)){
				a.accept(getAbilitiesRemoveVisitor());
			}
		}
	}

	//------------------------
	// StatusEffect Management
	//------------------------

	/**
	 * Remove all the auras from this card that were originating from source
	 */
	public void removeAurasWithSource(Card source) {
		for (int i = 0; i < this.auras.size(); i++) {
			StatusEffect status = this.auras.get(i);
			if (status.isSource(source)) {
				status.removeFrom(this);
				i--;
			}
		}
	}

	/**
	 * Remove all the buffs from this card
	 */
	public void removeBuffs() {
		removeStatusByType(StatusEffectType.buff);
	}

	public boolean hasStatusAilments() {
		return this.isShocked || this.isFrozen || this.isTrapped || this.isStunned || this.isConfused;
	}

	public void removeStatusAilments() {
		if (Constants.IS_DEBUG) {
			if (this.isShocked) {
				L.debug(this + " won't be shocked anymore.");
			}

			if (this.isFrozen) {
					L.debug(this + " won't be frozen anymore.");
			}
			if (this.isTrapped) {
					L.debug(this + " won't be trapped anymore.");
			}
			if (this.isStunned) {
					L.debug(this + " won't be stunned anymore.");
			}
			if (this.isConfused) {
					L.debug(this + " won't be confused anymore.");
			}

			if (this.isBlind()) {
					L.debug(this + " won't be blind anymore.");
			}

			if (this.isSpellMarked()) {
					L.debug(this + " won't be spell marked anymore.");
			}
		}
		this.isShocked = false;
		this.isFrozen = false;
		this.isTrapped = false;
		this.isStunned = false;
		this.isConfused = false;
		this.isBlind = 0;
		this.isSpellMarked = 0;
	}

	public boolean hasDebuffs() {
		return !this.debuffs.isEmpty() || this.isBlind() || this.isSpellMarked();
	}

	/**
	 * Remove all the debuffs from this card
	 */
	public void removeDebuffs() {
		removeStatusByType(StatusEffectType.debuff);
	}

	public boolean hasHexes() {
		return this.isLacerated || !this.hexes.isEmpty();
	}

	/**
	 * Remove all the hexes from this card
	 */
	private void removeHexes() {
		removeStatusByType(StatusEffectType.hex);
		if (this.isLacerated) {
			this.isLacerated = false;
			if (Constants.IS_DEBUG)
				L.debug(this + " won't be lacerated anymore.");
		}
	}

	public boolean isPurifiable() {
		return this.hasStatusAilments() || this.hasDebuffs() || this.hasHexes();
	}

	public void purify() {
		removeStatusAilments();
		removeDebuffs();
		removeHexes();
	}

	public void onTurnStart() {
		restoreType();
		this.isGoingExtinct = false;
		this.isLastChance = false;

		this.applyBeforeTurnAbilities();
	}

	/**
	 * Remove all the boosts from this card
	 */
	public void removeBoosts() {
		removeStatusByType(StatusEffectType.boost);
	}

	private void removeStatusByType(StatusEffectType type) {
		ArrayList<StatusEffect> targetList = pickStatusList(type);
		while (!targetList.isEmpty()) {
			targetList.get(targetList.size() - 1).removeFrom(this);
		}
	}

	public boolean addActiveStatus(StatusEffect status) {
		ArrayList<StatusEffect> targetList = pickStatusList(status.getType());

		boolean modified;
		if (targetList.contains(status)) {
			modified = false;
		}
		else {
			modified = targetList.add(status);
		}
		if (modified) {
			if (status.isRecurring()) {
				this.recurringStatus.add(status);
			}
			if (Constants.IS_DEBUG)
				L.debug(this + " affected by " + status.getType().name() + ' ' + status);

		}
		return modified;
	}

	public boolean removeActiveStatus(StatusEffect status) {
		boolean modified = pickStatusList(status.getType()).remove(status);
		if (modified) {
			if (status.isRecurring()) {
				this.recurringStatus.remove(status);
			}
			if (Constants.IS_DEBUG)
				L.debug(this + " isn't affected by " + status.getType().name() + ' ' + status + " anymore");

		}
		return modified;
	}

	ArrayList<StatusEffect> pickStatusList(StatusEffectType type) {
		switch (type) {
			case aura:
				return this.auras;
			case boost:
				return this.boosts;
			case buff:
				return this.buffs;
			case debuff:
				return this.debuffs;
			case hex:
				return this.hexes;
			default:
				throw new IllegalArgumentException("Unhandled type " + type.name());
		}
	}

	public boolean hasStatus(StatusEffect status) {
		return pickStatusList(status.getType()).contains(status);
	}

	public void markLacerated() {
		this.isLacerated = true;
		if (Constants.IS_DEBUG)
			L.debug(this + " is lacerated !");
	}

	public void markTrapped() {
		if (!this.hasImmunity() && !this.hasEvasion() && !isLastChance) {
			this.isTrapped = true;
			if (Constants.IS_DEBUG)
				L.debug(this + " is trapped !");
		}
	}

	public void markConfused() {
		if (!this.hasImmunity() && !this.hasEvasion() && !isLastChance) {
			this.isConfused = true;
			if (Constants.IS_DEBUG)
				L.debug(this + " is confused !");
		}
	}

	public void markFrozen() {
		if (!this.hasImmunity() && !this.hasEvasion() && !isLastChance) {
			this.isFrozen = true;
			if (Constants.IS_DEBUG)
				L.debug(this + " is frozen !");
		}
	}

	public void markShocked() {
		if (!this.hasImmunity() && !this.hasEvasion() && !isLastChance) {
			this.isShocked = true;
			if (Constants.IS_DEBUG)
				L.debug(this + " is shocked !");
		}
	}

	public void markStunned() {
		if (!isBoss() && !this.hasEvasion() && !isLastChance) {
			this.isStunned = true;
			if (Constants.IS_DEBUG)
				L.debug(this + " is stunned !");
		}
	}

	public void markBlind(int missChance) {
		if (!this.hasImmunity() && !isLastChance) {
			this.isBlind = missChance;
			if (Constants.IS_DEBUG)
				L.debug(this + " is " + missChance + "% blind !");
		}
	}

	public void markSpellMarked(int damagePercentInc) {
		if (!this.hasImmunity() && !isLastChance) {
			this.isSpellMarked = damagePercentInc;
			if (Constants.IS_DEBUG)
				L.debug(this + " is spell marked and will take " + damagePercentInc + "% more damage from spells !");
		}
	}

	public void markGoingExtinct() {
		this.isGoingExtinct = true;
		if (Constants.IS_DEBUG)
			L.debug(this + " is going exctinct !");
	}

	public void markLastChance() {
		this.currentHp = 1;
		if (this.currentBaseHp == 0) {
			this.currentBaseHp = 1;
		}
		this.markBeingKilled(false);
		this.isLastChance = true;
		if (Constants.IS_DEBUG)
			L.debug(this + " got a last chance !");
	}

	public void markSilenced() {
		if (!hasSpiritualVoice() && !isLastChance && !isBoss()) {

			/*for (int i = 0, allAbilitiesSize = allAbilities.size(); i < allAbilitiesSize; i++) {
				IAbility iAbility = allAbilities.get(i);
				iAbility.setSilenceMarker(silenceMarker);
			}*/
			//this.silenceMarker = new Ref<>(true);
			isSilenced = true;
			if (Constants.IS_DEBUG)
				L.debug(this + " is silenced !");
		}
	}

	public void removeSilence() {
		if (Constants.IS_DEBUG && isSilenced) {
			L.debug(this + " is not silenced anymore !");
		}
		isSilenced = false;
	}

	public void markBeingKilled(boolean b) {
		this.beingKilled = b;
	}

	public boolean isBeingKilled() {
		return this.beingKilled;
	}

	public boolean isBoss() {
		return CardType.demon.equals(this.currentType) || CardType.hydra.equals(this.currentType);
	}

	public CardType getType() {
		return this.currentType;
	}

	public void changeType(CardType type) {
		if (!isBoss()) {
			CardType beforeType = this.currentType;
			this.currentType = type;
			if (Constants.IS_DEBUG)
				L.debug(this + " changed type from " + beforeType + " to " + this.currentType + " !");
		}
	}

	public void restoreType() {
		if (!this.currentType.equals(desc.type)) {
			this.currentType = desc.type;
			if (Constants.IS_DEBUG)
				L.debug(this + "'s type is back to " + desc.type);
		}
	}

	public int iterOnField() {
		return this.iterOnField;
	}

	public boolean isIterOnField(int iterOnfield) {
		return iterOnfield == this.iterOnField;
	}

	public void markOnField(boolean onField) {
		this.onField = onField;
		if (!onField) { // when gets out of field, increment iter
			iterOnField++;
		}
	}

	public boolean isOnField() {
		return this.onField;
	}

	public int getCurrentTimer() {
		return timer;
	}

	public boolean resists() {
		return this.hasImmunity() || this.hasResistance() || isLastChance;
	}

	public boolean resurrects() {
		FTArrayList<Resurrection> resurrections1 = this.resurrections;
		for (int i = 0, resurrections1Size = resurrections1.size(); i < resurrections1Size; i++) {
			Resurrection r = resurrections1.get(i);
			if (r.canBeLaunched(this) && r.check(this)) {
				if (Constants.IS_DEBUG)
					L.debug(this + " resurrects!");
				if (owner().countCardsInHand() < Player.MAX_HAND_SIZE) {
					owner().addCardToHand(this);
				}
				else {
					if (Constants.IS_DEBUG)
						L.debug(owner() + " hand is full.");
					owner().addCardOnTopOfDeck(this);
				}
				return true;
			}
		}
		return false;
	}

	public void onBeforeRound() {
		for (int i = 0, s = beforeRoundAbilities.size(); i < s; i++) {
			IBeforeRound p = beforeRoundAbilities.get(i);
			if (p.canBeLaunched(this)) {
				if (Constants.IS_DEBUG)
					L.debug(this + " tries to use " + p.getName());
				p.onBeforeRound(this);
			}
		}
	}

	int guard(int dmg) {
		for (int i = 0; i < guardList.size(); i++) {
			IBeforeHeroDamage guardSkill = this.guardList.get(i);
			if (isAlive() && guardSkill.canBeLaunched(this)) {
				dmg = guardSkill.onBeforeHeroDamage(dmg, this);
				if (dmg == 0) break;
			}
		}
		return dmg;
	}

	public void applyRecurringStatus() {
		ArrayList<StatusEffect> recurringStatus1 = this.recurringStatus;
		for (int i = 0, recurringStatus1Size = recurringStatus1.size(); i < recurringStatus1Size; i++) {
			StatusEffect status = recurringStatus1.get(i);
			if (Constants.IS_DEBUG)
				L.debug(this + " is affected by " + status);
			status.applyEffectTo(this);
			if (!this.isAlive())
				break;
		}
	}

	public boolean canApplyAuras() {
		return !this.auraAbilities.isEmpty();
	}

	public boolean hasMarker(String name) {
		return this.markers.contains(name);
	}

	public void addMarker(String name) {
		this.markers.add(name);
	}

	public void removeMarker(String name) {
		this.markers.remove(name);
	}

	public Card removeFromField() {
		owner().removeCardFromField(this);
		this.restoreOwner();
		return this;
	}

	public Card addRandomlyToDeck() {
		owner().addCardRandomlyToDeck(this);
		return this;
	}

	public Card addOnTopOfDeck() {
		owner().addCardOnTopOfDeck(this);
		return this;
	}

	public Card removeFromHand() {
		owner().removeCardFromHand(this);
		return this;
	}

	public Card addToHand() {
		owner().addCardToHand(this);
		return this;
	}

	public Card addToField() {
		owner().addCardToField(this);
		return this;
	}

	public Card addToCemetery() {
		owner().addCardToCemetery(this);
		return this;
	}

	public Card removeFromCemetery() {
		owner().removeCardFromCemetery(this);
		return this;
	}

	public Player owner() {
		return currentOwner;
	}

	public void setOwner(Player owner) {
		if (Constants.IS_DEBUG && !currentOwner.equals(owner)) {
			L.debug(this + " now serves " + owner + '!');
		}
		currentOwner = owner;
	}

	public void restoreOwner() {
		setOwner(baseOwner);
	}

	@Override
	public String toString() {
		return desc.uniqueName;//+ "[" + this.getCurrentAtk() + "][" + this.getCurrentHp() + "]";
	}

	public List<IPower> getPowerAbilities() {
		return powerAbilities;
	}

	public List<IBeforeDamage> getDamageReducerAbilities() {
		return damageReducerAbilities;
	}

	public List<IAttack> getAttackAbilities() {
		return attackAbilities;
	}

	public List<IAfterHit> getOnHitAbilities() {
		return onAfterHitAbilities;
	}

	public List<IAfterDamage> getCounterAbilities() {
		return counterAbilities;
	}

	public List<IAfterTurn> getOnFinishedAbilities() {
		return onFinishedAbilities;
	}

	public List<IAura> getAuraAbilities() {
		return auraAbilities;
	}

	public List<IBeforeAttackDeboost> getDeboostAbilities() {
		return deboostAbilities;
	}

	public List<IQuickstrikeAble> getQuickstrikeAbilities() {
		return quickstrikeAbilities;
	}

	public List<IDesperationAble> getDesperationAbilities() {
		return desperationAbilities;
	}

	public List<IBeforeAttack> getBoostAbilities() {
		return boostAbilities;
	}

	public String getName(){
		return desc.name;
	}
}
