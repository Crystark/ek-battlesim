package crystark.ek.battlesim.battle;

import crystark.common.api.CardDesc;

public class SummonedCard extends Card {
	protected final Card	summoner;
	protected final String	abilityRef;

	public SummonedCard(CardDesc cardDesc, Card summoner, String abilityRef) {
		super(cardDesc, summoner.owner());
		this.summoner = summoner;
		this.abilityRef = abilityRef;
	}

	public boolean isSummonedBy(Card card, String abilityRef) {
		return this.summoner.equals(card) && this.abilityRef.equals(abilityRef);
	}

	@Override
	public boolean isSummoned() {
		return true;
	}

	public void applyQuickStrikes() {}
	public void applyDesperations() {}

	@Override
	public String toString() {
		return "S:" + super.toString();
	}
}
