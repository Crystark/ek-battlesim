package crystark.ek.battlesim.battle.consumer;

import crystark.ek.battlesim.battle.Card;
import crystark.tools.AbstractConsumer;

public class ResistanceLessConsumer extends AbstractConsumer<Card> {
	public static final ResistanceLessConsumer INSTANCE = new ResistanceLessConsumer();

	public ResistanceLessConsumer() {

	}

	@Override
	public void accept(Card card) {
		exile(card);
	}

	public static void exile(Card card) {
		if (card != null && card.hasResistance() && !card.hasImmunity() && !card.isLastChance()) {
			card.removeFromField();
			card.addRandomlyToDeck();
		}
	}
}

