package crystark.ek.battlesim.battle.consumer;

import crystark.ek.battlesim.battle.Card;
import crystark.tools.AbstractConsumer;

public class LacerationConsumer extends AbstractConsumer<Card> {
	public static final LacerationConsumer INSTANCE = new LacerationConsumer();

	@Override
	public void accept(Card card) {
		lacerate(card);
	}

	public static void lacerate(Card card) {
		card.markLacerated();
	}
}
