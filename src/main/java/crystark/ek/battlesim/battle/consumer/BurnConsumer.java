package crystark.ek.battlesim.battle.consumer;

import crystark.ek.Helper;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.tools.AbstractConsumer;

public class BurnConsumer extends AbstractConsumer<Card> {
	private final Integer minDamage;
	private final Card    sourceCard;
	private final int     delta;

	public BurnConsumer(int minDamage, int maxDamage, Card sourceCard) {
		this.minDamage = minDamage;
		this.sourceCard = sourceCard;
		this.delta = maxDamage - minDamage + 1;
	}

	@Override
	public void accept(Card card) {
		burn(card, sourceCard, minDamage, delta);
	}

	public static void burn(Card card, Card sourceCard, int minDamage, int delta) {
		if (card != null && card.hasReflectionOrNotImmune()) {
			card.damaged(sourceCard, minDamage + Helper.random(delta), DamageType.fire);
		}
	}
}
