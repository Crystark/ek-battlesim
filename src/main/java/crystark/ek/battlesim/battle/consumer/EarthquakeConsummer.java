package crystark.ek.battlesim.battle.consumer;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.tools.AbstractConsumer;

public class EarthquakeConsummer extends AbstractConsumer<Card> {
	private final int	damage;
	private final int	resistDamage;
	private final Card	sourceCard;

	public EarthquakeConsummer(int damage, int resistDamage, Card sourceCard) {
		this.damage = damage;
		this.resistDamage = resistDamage;
		this.sourceCard = sourceCard;
	}

	@Override
	public void accept(Card card) {
		earthquake(card, sourceCard, damage, resistDamage);
	}

	public static void earthquake(Card card, Card sourceCard, int damage, int resistDamage) {
		if (card != null) {
			card.damaged(sourceCard, card.hasResistance() ? resistDamage : damage, DamageType.unavoidable);
		}
	}
}
