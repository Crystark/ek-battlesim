package crystark.ek.battlesim.battle.consumer;

import java.util.function.Consumer;

import crystark.common.api.CardType;
import crystark.ek.battlesim.battle.Card;

public class ChangeTypeConsumer implements Consumer<Card> {
	private CardType type;

	public ChangeTypeConsumer(CardType type) {
		this.type = type;
	}

	@Override
	public void accept(Card card) {
		card.changeType(type);
	}
}
