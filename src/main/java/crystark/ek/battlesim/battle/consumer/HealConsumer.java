package crystark.ek.battlesim.battle.consumer;

import crystark.ek.battlesim.battle.Card;

import java.util.function.Consumer;
import java.util.function.Function;

public class HealConsumer implements Consumer<Card> {
	private final Function<Card, Integer> valueFunction;

	public HealConsumer(Integer value) {
		this(c -> value);
	}

	public HealConsumer(Function<Card, Integer> valueFunction) {
		this.valueFunction = valueFunction;
	}

	@Override
	public void accept(Card card) {
		heal(card, valueFunction.apply(card));
	}

	public static void heal(Card card, int value) {
		if (card != null) {
			card.heal(value);
		}
	}
}
