package crystark.ek.battlesim.battle.consumer;

import java.util.function.Consumer;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;

public class BiteConsumer implements Consumer<Card> {
	private final Integer	value;
	private final Card		sourceCard;

	public BiteConsumer(Integer value, Card sourceCard) {
		this.value = value;
		this.sourceCard = sourceCard;
	}

	@Override
	public void accept(Card card) {
		bite(card, sourceCard, value);
	}

	public static void bite(Card card, Card sourceCard, int damage) {
		if (card != null && card.hasReflectionOrNotImmune()) {
			int damageDone = card.damaged(sourceCard, damage, DamageType.blood);
			if (damageDone > 0 && sourceCard.isAlive()) { // card can be dead from reflection
				sourceCard.regen(damageDone); // ignores Laceration
			}
		}
	}
}
