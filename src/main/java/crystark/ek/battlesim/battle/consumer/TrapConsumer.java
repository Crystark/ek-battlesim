package crystark.ek.battlesim.battle.consumer;

import crystark.ek.Helper;
import crystark.ek.battlesim.battle.Card;
import crystark.tools.AbstractConsumer;

public class TrapConsumer extends AbstractConsumer<Card> {
	private Integer chance;

	public TrapConsumer(Integer chance) {
		this.chance = chance;
	}

	@Override
	public void accept(Card card) {
		if (Helper.getLucky(chance)) {
			card.markTrapped();
		}
	}
}
