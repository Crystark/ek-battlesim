package crystark.ek.battlesim.battle.consumer;

import crystark.ek.battlesim.battle.Card;
import crystark.tools.AbstractConsumer;

public class BlindConsumer extends AbstractConsumer<Card> {
	private final Integer value;

	public BlindConsumer(int value) {
		this.value = value;
	}

	@Override
	public void accept(Card card) {
		blind(card, value);
	}

	public static void blind(Card blindedCard, int value) {
		if (blindedCard != null) blindedCard.markBlind(value);
	}
}
