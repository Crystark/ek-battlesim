package crystark.ek.battlesim.battle.consumer;

import java.util.function.Consumer;

import crystark.ek.battlesim.battle.Card;

public class SpellMarkConsumer implements Consumer<Card> {
	private final int percentDmgInc;

	public SpellMarkConsumer(int percentDmgInc) {
		this.percentDmgInc = percentDmgInc;
	}

	@Override
	public void accept(Card card) {
		mark(card, percentDmgInc);
	}

	public static void mark(Card card, int percentDmgInc) {
		if (card != null) {
			card.markSpellMarked(percentDmgInc);
		}
	}
}
