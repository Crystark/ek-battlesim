package crystark.ek.battlesim.battle.consumer;

import crystark.ek.battlesim.battle.Card;
import crystark.tools.AbstractConsumer;

public class SilenceConsumer extends AbstractConsumer<Card> {
	public static final SilenceConsumer INSTANCE = new SilenceConsumer();

	@Override
	public void accept(Card card) {
		silence(card);
	}

	public static void silence(Card card) {
		if (card != null) {
			card.markSilenced();
		}
	}
}
