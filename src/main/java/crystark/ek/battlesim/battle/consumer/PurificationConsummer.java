package crystark.ek.battlesim.battle.consumer;

import crystark.ek.battlesim.battle.Card;
import crystark.tools.AbstractConsumer;

public class PurificationConsummer extends AbstractConsumer<Card> {
	@Override
	public void accept(Card card) {
		purify(card);
	}

	public static void purify(Card card) {
		if (card != null) {
			card.purify();
		}
	}
}
