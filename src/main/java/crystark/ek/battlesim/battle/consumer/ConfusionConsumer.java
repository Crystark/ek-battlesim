package crystark.ek.battlesim.battle.consumer;

import crystark.ek.Helper;
import crystark.ek.battlesim.battle.Card;
import crystark.tools.AbstractConsumer;

public class ConfusionConsumer extends AbstractConsumer<Card> {
	private Integer chance;

	public ConfusionConsumer(Integer chance) {
		this.chance = chance;
	}

	@Override
	public void accept(Card card) {
		confuse(card, chance);
	}

	public static void confuse(Card card, Integer chance) {
		if (card != null && Helper.getLucky(chance)) {
			card.markConfused();
		}
	}
}
