package crystark.ek.battlesim.battle.consumer;

import crystark.ek.Helper;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.tools.AbstractConsumer;

/**
 * Deal 400 damage to 3 random enemy cards. The cards receiving damage will have a 75% chance to get frozen
 * and can't attack or use skills in the next turn.If any of the card has Immunity or Reflection, deal 300% damage.
 */
public class LightingCutConsumer extends AbstractConsumer<Card> {
	private final int  damage;
	private final int  multiplier;
	private final int  chance;
	private final Card sourceCard;

	public LightingCutConsumer(int damage, int multiplier, int chance, Card sourceCard) {
		this.damage = damage;
		this.chance = chance;
		this.sourceCard = sourceCard;
		this.multiplier = multiplier;
	}

	@Override
	public void accept(Card card) {
		lightingCut(card, sourceCard, damage, chance, multiplier);
	}

	public static void lightingCut(Card card, Card sourceCard, int damage, int chance, int multiplier) {
		if (card != null) {
			if (card.hasImmunityIgnoreSilence() || card.hasReflectionIgnoreSilence()) {
				damage *= multiplier;
			} else if (Helper.getLucky(chance) && !card.hasReflection()) {					
				card.markShocked();
			}
			int damageDone = card.damaged(sourceCard, damage, DamageType.focused);

		}
	}
}
