package crystark.ek.battlesim.battle.consumer;

import java.util.function.Consumer;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.status.Burning;
import crystark.ek.battlesim.status.StatusEffect;

public class CombustionConsumer implements Consumer<Card> {
	private final StatusEffect statusEffect;

	public CombustionConsumer(StatusEffect statusEffect) {
		this.statusEffect = statusEffect;
	}

	@Override
	public void accept(Card card) {
		combust(card, statusEffect);
	}

	public static StatusEffect makeEffect(Integer damage) {
		return new Burning(damage);
	}

	public static void combust(Card card, StatusEffect statusEffect) {
		if (card != null && !card.hasImmunity()) {
			statusEffect.applyTo(card);
		}
	}
}
