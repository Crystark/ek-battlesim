package crystark.ek.battlesim.battle.consumer;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.status.Poison;
import crystark.ek.battlesim.status.StatusEffect;
import crystark.tools.AbstractConsumer;

public class PoisonConsumer extends AbstractConsumer<Card> {
	private final Integer		damage;
	private final Card			sourceCard;
	private final StatusEffect	statusEffect;

	public PoisonConsumer(Integer damage, Card sourceCard, StatusEffect statusEffect) {
		this.damage = damage;
		this.sourceCard = sourceCard;
		this.statusEffect = statusEffect;
	}

	@Override
	public void accept(Card card) {
		poison(card, sourceCard, damage, statusEffect);
	}

	public static StatusEffect makeEffect(Integer damage) {
		return new Poison(damage);
	}

	public static void poison(Card card, Card sourceCard, int damage, StatusEffect statusEffect) {
		if (card != null && !card.hasImmunity()) {
			card.damaged(sourceCard, damage, DamageType.poison);
			if (card.isAlive()) {
				statusEffect.applyTo(card);
			}
		}
	}
}
