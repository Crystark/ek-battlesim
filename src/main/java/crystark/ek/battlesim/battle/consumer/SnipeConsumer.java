package crystark.ek.battlesim.battle.consumer;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.battle.Damageable;
import crystark.tools.AbstractConsumer;

public class SnipeConsumer extends AbstractConsumer<Card> {
	private final Integer	damage;
	private final Card		sourceCard;

	public SnipeConsumer(Integer damage, Card sourceCard) {
		this.damage = damage;
		this.sourceCard = sourceCard;
	}

	@Override
	public void accept(Card card) {
		snipe(card, sourceCard, damage);
	}

	public static void snipe(Damageable card, Card sourceCard, int damage) {
		if (card != null) {
			card.damaged(sourceCard, damage, DamageType.focused);
		}
	}
}
