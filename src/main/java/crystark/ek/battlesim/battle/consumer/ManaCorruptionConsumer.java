package crystark.ek.battlesim.battle.consumer;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.tools.AbstractConsumer;

public class ManaCorruptionConsumer extends AbstractConsumer<Card> {
	private final int	damage;
	private final int	multiplier;
	private final Card	sourceCard;

	public ManaCorruptionConsumer(int damage, int multiplier, Card sourceCard) {
		this.damage = damage;
		this.sourceCard = sourceCard;
		this.multiplier = multiplier;
	}

	@Override
	public void accept(Card card) {
		manaCorrupt(card, sourceCard, damage, multiplier);
	}

	public static void manaCorrupt(Card card, Card sourceCard, int damage, int multiplier) {
		if (card != null) {
			if (card.hasImmunityIgnoreSilence() || card.hasReflectionIgnoreSilence()) {
				damage *= multiplier;
			}
			card.damaged(sourceCard, damage, DamageType.focused);
		}
	}
}
