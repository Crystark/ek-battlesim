package crystark.ek.battlesim.battle.consumer;

import java.util.function.Consumer;

import crystark.ek.battlesim.ability.impl.sub.Terrorized;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.effect.BonusAbility;
import crystark.ek.battlesim.status.StatusEffect;
import crystark.ek.battlesim.status.StatusEffectType;

public class RoarConsumer implements Consumer<Card> {
	private final StatusEffect statusEffect;

	public RoarConsumer() {
		this(makeEffect());
	}

	public RoarConsumer(StatusEffect statusEffect) {
		this.statusEffect = statusEffect;
	}

	@Override
	public void accept(Card card) {
		roar(card, statusEffect);
	}

	public static StatusEffect makeEffect() {
		return StatusEffect.builder()
			.setEffect(new BonusAbility(Terrorized::new))
			.setType(StatusEffectType.debuff)
			.build();
	}

	public static void roar(Card card, StatusEffect statusEffect) {
		if (card != null && !card.isBoss() && !card.isLastChance()) {
			statusEffect.applyTo(card);
		}
	}
}
