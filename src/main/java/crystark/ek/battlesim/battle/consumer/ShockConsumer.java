package crystark.ek.battlesim.battle.consumer;

import crystark.ek.Helper;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;
import crystark.tools.AbstractConsumer;

public class ShockConsumer extends AbstractConsumer<Card> {
	private final Integer	damage;
	private final Integer	chance;
	private final Card		sourceCard;

	public ShockConsumer(Integer damage, Integer chance, Card sourceCard) {
		this.damage = damage;
		this.chance = chance;
		this.sourceCard = sourceCard;
	}

	@Override
	public void accept(Card card) {
		shock(card, sourceCard, damage, chance);
	}

	public static void shock(Card card, Card sourceCard, int damage, int chance) {
		if (card != null && card.hasReflectionOrNotImmune()) {
			int damageDone = card.damaged(sourceCard, damage, DamageType.lightning);
			if (damageDone > 0 && Helper.getLucky(chance)) {
				card.markShocked();
			}
		}
	}
}
