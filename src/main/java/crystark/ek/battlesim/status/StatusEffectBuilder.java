package crystark.ek.battlesim.status;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.effect.Effect;

public class StatusEffectBuilder<E extends Effect<? super Card>> extends StatusEffectOrBuilder {

	public StatusEffectBuilder<E> setType(StatusEffectType type) {
		this.type = type;
		return this;
	}

	public StatusEffectBuilder<E> setEffect(Effect<? super Card> effect) {
		this.effect = effect;
		return this;
	}

	public StatusEffectBuilder<E> setSource(Object source) {
		this.source = source;
		return this;
	}

	public StatusEffect build() {
		return new StatusEffect(this);
	}

	public Effect<? super Card> getEffect() {
		return effect;
	}

}
