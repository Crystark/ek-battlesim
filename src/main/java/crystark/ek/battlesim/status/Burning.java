package crystark.ek.battlesim.status;

import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.effect.Damage;

public class Burning extends StatusEffect {
	public Burning(int i) {
		super(StatusEffect
			.builder()
			.setEffect(new Damage(i, DamageType.burn, null))
			.setSource(null)
			.setType(StatusEffectType.hex));
	}

	protected Burning(StatusEffectBuilder<?> b) {
		super(b);
	}
}
