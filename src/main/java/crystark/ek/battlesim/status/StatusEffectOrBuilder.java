package crystark.ek.battlesim.status;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.effect.Effect;

public abstract class StatusEffectOrBuilder {
	protected StatusEffectType		type;
	protected Effect<? super Card>	effect;
	protected Object				source;

	protected StatusEffectOrBuilder() {}

	protected StatusEffectOrBuilder(StatusEffectOrBuilder b) {
		this.type = b.type;
		this.effect = b.effect;
		this.source = b.source;
	}

	public StatusEffectType getType() {
		return type;
	}

}
