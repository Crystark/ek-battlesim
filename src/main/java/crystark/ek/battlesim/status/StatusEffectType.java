package crystark.ek.battlesim.status;

public enum StatusEffectType {
	/**
	 * Status which is dependent on an other card beeing present
	 */
	aura,
	/**
	 * Status that's applied only for the duration of one attack
	 */
	boost,
	/**
	 * Status that is removed on the start of the next turn of the owner of the card it's applied to
	 */
	buff,
	/**
	 * Status that is removed after the next turn of what it's apllied to
	 */
	debuff,
	/**
	 * Status that holds on forever
	 */
	hex
}
