package crystark.ek.battlesim.status;

import crystark.ek.battlesim.ability.IConditionalStatusApplier;
import crystark.ek.battlesim.ability.IStatusEffectApplier;
import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.Player;

import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Ability that applies a status effect
 */
public class StatusEffectApplierHelper<V> {
	public enum ApplierTarget {
		allies, enemies, self
	}

	protected final IStatusEffectApplier<V> sea;
	protected final ApplierTarget           target;

	public StatusEffectApplierHelper(IStatusEffectApplier<V> sea, ApplierTarget target) {
		this.sea = sea;
		this.target = target;
	}

	private StatusEffect getStatus(Card sourceCard) {
		return StatusEffect.builder()
			.setEffect(sea.getEffect(sourceCard))
			.setType(sea.getStatusType())
			.setSource(sourceCard)
			.build();
	}

	private boolean conditionsAreMet(Player sourcePlayer, Card sourceCard) {
		if (sea instanceof IConditionalStatusApplier) {
			return ((IConditionalStatusApplier) sea).conditionsAreMet(sourcePlayer, sourceCard);
		}
		return true;
	}

	protected void applyWithoutCondition(Player sourcePlayer, Card sourceCard) {
		StatusEffect status = this.getStatus(sourceCard);

		switch (target) {
			case allies:
				sourcePlayer.consumeCardsField(new StatusApplierConsumer(sea, sourceCard, status, sea.getEligibleCardsFilter(sourceCard)));
				break;
			case enemies:
				sourcePlayer.opponent().consumeCardsField(new StatusApplierConsumer(sea, sourceCard, status, sea.getEligibleCardsFilter(sourceCard)));
				break;
			case self:
				sea.applyTo(sourceCard, sourceCard, status);
				break;
			default:
				throw new RuntimeException("Unmanaged target " + target);
		}
	}

	public void apply(Card sourceCard) {
		this.apply(sourceCard.owner(), sourceCard);
	}

	public void apply(Player sourcePlayer) {
		this.apply(sourcePlayer, null);
	}

	protected void apply(Player sourcePlayer, Card sourceCard) {
		if (this.conditionsAreMet(sourcePlayer, sourceCard)) {
			this.applyWithoutCondition(sourcePlayer, sourceCard);
		}
	}

	/**
	 * Direct apply to specific card
	 *
	 * @return
	 */
	public boolean applyTo(Card targetCard, Card sourceCard) {
		Predicate<Card> predicate = sea.getEligibleCardsFilter(sourceCard);
		if (this.conditionsAreMet(sourceCard.owner(), sourceCard) && predicate.test(targetCard)) {
			StatusEffect status = this.getStatus(sourceCard);
			sea.applyTo(sourceCard, targetCard, status);
			return true;
		}
		return false;
	}

	static class StatusApplierConsumer implements Consumer<Card> {
		private final IStatusEffectApplier<?>	sea;
		private final Card						sourceCard;
		private final StatusEffect				status;
		private final Predicate<Card>			predicate;

		StatusApplierConsumer(IStatusEffectApplier<?> sea, Card sourceCard, StatusEffect status, Predicate<Card> predicate) {
			this.sea = sea;
			this.sourceCard = sourceCard;
			this.status = status;
			this.predicate = predicate;
		}

		@Override
		public void accept(Card card) {
			if (predicate.test(card)) {
				sea.applyTo(sourceCard, card, status);
			}
		}
	}
}
