package crystark.ek.battlesim.status;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.effect.Effect;
import crystark.ek.battlesim.effect.RecurringEffect;

public class StatusEffect extends StatusEffectOrBuilder {
	private static final Logger	L	= LoggerFactory.getLogger(StatusEffect.class);

	private String				stringValue;

	protected StatusEffect(StatusEffectBuilder<?> b) {
		super(b);
	}

	public static <E extends Effect<? super Card>> StatusEffectBuilder<E> builder() {
		return new StatusEffectBuilder<>();
	}

	public boolean isRecurring() {
		return this.effect instanceof RecurringEffect;
	}

	public boolean isSource(Object source) {
		return Objects.equals(this.source, source);
	}

	public void applyTo(Card target) {
		// Apply effect only if status not already present
		if (target.addActiveStatus(this)) {
			effect.isAddedTo(target);
		}
	}

	public void applyEffectTo(Card target) {
		effect.applyTo(target);
	}

	public void removeFrom(Card target) {
		// Remove effect only if status was present (this is a security and should not happen)
		if (target.removeActiveStatus(this)) {
			effect.isRemovedFrom(target);
		}
		else {
			L.warn("StatusEffect#removeFrom did not find " + this + " on target card " + target);
		}
	}

	public boolean is(StatusEffectType t) {
		return t.equals(this.type);
	}

	@Override
	public String toString() {
		if (stringValue == null) {
			stringValue = this.getClass().getSimpleName() + '[' + effect + ']';
		}
		return stringValue;
	}

}
