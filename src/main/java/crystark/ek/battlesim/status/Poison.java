package crystark.ek.battlesim.status;

import crystark.ek.battlesim.battle.DamageType;
import crystark.ek.battlesim.effect.Damage;

public class Poison extends StatusEffect {
	public Poison(int i) {
		super(StatusEffect
			.builder()
			.setEffect(new Damage(i, DamageType.poison, null))
			.setSource(null)
			.setType(StatusEffectType.debuff));
	}

	protected Poison(StatusEffectBuilder<?> b) {
		super(b);
	}
}
