package crystark.ek.battlesim.effect;

import crystark.ek.battlesim.ability.IAbility;
import crystark.ek.battlesim.battle.Card;

import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Supplier;

public class BonusAbility extends Effect<Card> {
	private static final AtomicLong		IDS	= new AtomicLong();
	private final Supplier<IAbility>	abilitySupplier;
	protected final long				id	= IDS.incrementAndGet();

	public BonusAbility(Supplier<IAbility> abilitySupplier) {
		this.abilitySupplier = abilitySupplier;
	}

	@Override
	public void applyTo(Card target) {
		final IAbility ability = abilitySupplier.get();
		if (ability != null) target.addAbility(id, ability);
	}

	@Override
	public void removeFrom(Card target) {
		target.removeAbility(id);
	}

	@Override
	public String toString() {
		if (stringValue == null) {
			stringValue = super.toString() + '[' + abilitySupplier.get() + ']';
		}
		return stringValue;
	}
}
