package crystark.ek.battlesim.effect;

import crystark.ek.battlesim.battle.Card;

public class BonusHp extends Effect<Card> {
	final int value;

	public BonusHp(int value) {
		this.value = value;
	}

	@Override
	public void applyTo(Card target) {
		target.addBonusHp(value);
	}

	@Override
	public void removeFrom(Card target) {
		target.removeBonusHp(value);
	}
}
