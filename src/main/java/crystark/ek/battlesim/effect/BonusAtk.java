package crystark.ek.battlesim.effect;

import crystark.ek.battlesim.battle.HasAtk;

public class BonusAtk extends Effect<HasAtk> {
	final int value;

	public BonusAtk(int value) {
		this.value = value;
	}

	@Override
	public void applyTo(HasAtk target) {
		target.modifyAtk(value);
	}

	@Override
	public void removeFrom(HasAtk target) {
		target.modifyAtk(-value);
	}
}
