package crystark.ek.battlesim.effect;

import crystark.ek.battlesim.battle.HasAtk;

public class MalusAtk extends Effect<HasAtk> {
	final int value;

	public MalusAtk(int value) {
		if (value < 1) {
			throw new IllegalArgumentException("Requires a value > 0. Got " + value);
		}
		this.value = value;
	}

	@Override
	public void applyTo(HasAtk target) {
		target.addMalusAtk(value);
	}

	@Override
	public void removeFrom(HasAtk target) {
		target.removeMalusAtk(value);
	}
}
