package crystark.ek.battlesim.effect;

public abstract class Effect<T> {
	protected String stringValue;

	public abstract void applyTo(T target);

	public abstract void removeFrom(T target);

	public void isAddedTo(T target) {
		this.applyTo(target);
	}

	public void isRemovedFrom(T target) {
		this.removeFrom(target);
	}

	@Override
	public String toString() {
		if (stringValue == null) {
			stringValue = this.getClass().getSimpleName();
		}
		return stringValue;
	}
}
