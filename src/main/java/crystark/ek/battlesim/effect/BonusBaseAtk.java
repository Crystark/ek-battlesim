package crystark.ek.battlesim.effect;

import crystark.ek.battlesim.battle.HasAtk;

public class BonusBaseAtk extends Effect<HasAtk> {
	final int value;

	public BonusBaseAtk(int value) {
		this.value = value;
	}

	@Override
	public void applyTo(HasAtk target) {
		target.addBonusAtk(value);
	}

	@Override
	public void removeFrom(HasAtk target) {
		target.removeBonusAtk(value);
	}
}
