package crystark.ek.battlesim.effect;

import crystark.ek.battlesim.battle.Card;
import crystark.ek.battlesim.battle.DamageType;

public class Damage extends Effect<Card> implements RecurringEffect {
	final Integer		value;
	final DamageType	damageType;
	final Card			sourceCard;

	public Damage(Integer value, DamageType damageType, Card sourceCard) {
		if (value < 1) {
			throw new IllegalArgumentException("Damage must be > 0 but was " + value);
		}
		this.value = value;
		this.damageType = damageType;
		this.sourceCard = sourceCard;
	}

	@Override
	public void isAddedTo(Card target) {
		// nothing done when applied
	}

	@Override
	public void applyTo(Card target) {
		target.damaged(sourceCard, value, damageType);
	}

	@Override
	public void removeFrom(Card target) {
		// can't remove damage
	}
}
