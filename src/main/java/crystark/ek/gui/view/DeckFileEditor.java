package crystark.ek.gui.view;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

import crystark.ek.gui.GuiUtils;
import crystark.tools.Tools;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyBooleanPropertyBase;
import javafx.scene.control.ButtonType;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.stage.FileChooser;

public class DeckFileEditor extends DeckEditor {
	private DivergedProperty	diverged			= new DivergedProperty();
	private String				latestState;
	private Long				lastModified;
	private File				file;

	private AtomicBoolean		suspendOutsideCheck	= new AtomicBoolean();

	public DeckFileEditor() {
		super();
		init();
	}

	private void init() {
		// Add save shortcut once it is linked to a scene
		sceneProperty().addListener((obs, old, scene) -> {
			if (scene != null) {
				scene.getAccelerators().put(new KeyCodeCombination(KeyCode.S, KeyCodeCombination.CONTROL_DOWN), this::saveFile);
			}
		});

		richChanges()
			.filter(ch -> !ch.getInserted().equals(ch.getRemoved()))
			.subscribe(e -> updateDivergedProperty());
	}

	public File file() {
		return file;
	}

	private Long lastModified() {
		return lastModified;
	}

	public ReadOnlyBooleanProperty divergedProperty() {
		return diverged;
	}

	private void updateDivergedProperty() {
		if (file() == null) {
			diverged.store(false);
		}
		else {
			diverged.store(!getText().equals(latestState));
		}
		diverged.notifyListeners();
	}

	private boolean outsideModification() {
		return file() != null && file().lastModified() > lastModified();
	}

	private void updateState(String text) {
		latestState = text;
		lastModified = file() == null ? null : file().lastModified();
		updateDivergedProperty();
	}

	public boolean newFile() {
		return clearAll() && saveFile();
	}

	boolean saveFile() {
		boolean save = true;
		if (outsideModification()) {
			ButtonType confirm = GuiUtils.dialogConfirm("This deck has been modified by another program. Continue saving ?", getScene().getWindow(), "Load changed file");
			if (!confirm.equals(ButtonType.OK)) {
				save = false;
				if (!confirm.equals(ButtonType.CANCEL)) {
					loadFile(file());
				}
			}
		}

		if (save) {
			if (file() == null) {
				FileChooser fileChooser = new FileChooser();
				fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
				fileChooser.setInitialFileName("new_deck.txt");
				file = fileChooser.showSaveDialog(getScene().getWindow());
				if (file() == null) {
					return false;
				}
			}

			Tools.writeToFile(file(), getText());
			updateState(getText());
		}

		return save;
	}

	public boolean loadFile(File file) {
		try {
			if (clearAll()) {
				this.file = file;
				replaceText(Tools.readFile(file));
				getUndoManager().forgetHistory();
				getUndoManager().mark();
				selectRange(0, 0);

				updateState(getText());
				return true;
			}
			return false;
		}
		catch (IOException e) {
			throw new RuntimeException("Failed to read file", e); // TODO
		}
	}

	public boolean clearAll() {
		boolean clear = true;
		if (suspendOutsideCheck.compareAndSet(false, true)) {
			if (diverged.get()) {
				ButtonType confirm = GuiUtils.dialogYesNoCancel("The current deck isn't saved. Do you want to save it ?", getScene().getWindow());
				if (confirm.equals(ButtonType.CANCEL)) {
					clear = false;
				}
				else if (confirm.equals(ButtonType.YES)) {
					clear = saveFile();
				}
			}
			suspendOutsideCheck.set(false);
		}
		if (clear) {
			resetWith(null);
			file = null;
			updateState(getText());
		}
		return clear;
	}

	final class DivergedProperty extends ReadOnlyBooleanPropertyBase {
		private boolean	value;
		private boolean	valid				= true;
		private boolean	needsChangeEvent	= false;

		public void store(final boolean value) {
			if (value != this.value) {
				this.value = value;
				markInvalid();
			}
		}

		public void notifyListeners() {
			if (needsChangeEvent) {
				fireValueChangedEvent();
				needsChangeEvent = false;
			}
		}

		private void markInvalid() {
			if (valid) {
				valid = false;
				needsChangeEvent = true;
			}
		}

		@Override
		public boolean get() {
			valid = true;
			return value;
		}

		@Override
		public Object getBean() {
			return DeckFileEditor.this;
		}

		@Override
		public String getName() {
			return "diverged";
		}
	}
}
