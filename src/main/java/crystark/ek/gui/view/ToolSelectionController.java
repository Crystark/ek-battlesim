package crystark.ek.gui.view;

import java.io.IOException;
import java.util.HashMap;

import crystark.ek.gui.MainApp;
import crystark.ek.gui.ToolController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;

public class ToolSelectionController {
	public static final double				TAB_CONTENT_HEIGHT	= 100.0;
	public static final double				TAB_WIDTH			= 124.0;			// Also contains borders/paddings

	private final HashMap<String, Integer>	tabs				= new HashMap<>();
	private MainApp							mainApp;

	@FXML
	private TabPane							tabPane;

	@FXML
	private void initialize() {
		tabPane.getSelectionModel().selectedItemProperty().addListener((obs, old, n3w) -> {
			if (old != null) { // initial selection is set when first tab is added
				setLastTab(n3w.getId());
			}
		});
	}

	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
	}

	public void setLastTab(String tabName) {
		this.mainApp.getStorage().lastTab = tabName;
	}

	public String getLastTab() {
		return this.mainApp.getStorage().lastTab;
	}

	public void createToolTab(String fxmlName) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(ToolSelectionController.class.getResource(fxmlName + ".fxml"));
			Node node = loader.load();
			ToolController c = loader.getController();
			c.setMainApp(mainApp);
			Tab tab = this.addTab(fxmlName, c.getTabLabel(), node);

			c.getDefaultButton().defaultButtonProperty().bind(tab.selectedProperty());
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	private Tab addTab(String id, String tabName, Node contentNode) {
		Tab tab = new Tab();
		tab.setId(id);

		Label l = new Label(tabName);
		l.setRotate(90);
		StackPane stp = new StackPane(new Group(l));
		tab.setGraphic(stp);
		if (contentNode != null) {
			AnchorPane.setTopAnchor(contentNode, 5.0);
			AnchorPane.setBottomAnchor(contentNode, 5.0);
			AnchorPane.setLeftAnchor(contentNode, 5.0);
			AnchorPane.setRightAnchor(contentNode, 5.0);

			AnchorPane pane = new AnchorPane(contentNode);
			pane.setMaxHeight(TAB_CONTENT_HEIGHT - 1);
			tab.setContent(pane);
		}

		int index = tabPane.getTabs().size();
		tabPane.getTabs().add(index, tab);
		if (id.equals(getLastTab())) {
			tabPane.getSelectionModel().select(index);
		}

		tabs.put(tabName, index);
		return tab;
	}
}
