package crystark.ek.gui.view;

import javafx.concurrent.Task;
import javafx.scene.control.IndexRange;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;
import org.fxmisc.richtext.model.StyleSpans;
import org.fxmisc.richtext.model.StyleSpansBuilder;

import java.io.BufferedReader;
import java.io.StringReader;
import java.time.Duration;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static crystark.common.format.DeckDescFormat.*;

public class DeckEditor extends CodeArea {

	private static final Pattern							SPECIAL_REGEX_CHARS	= Pattern.compile("[{}()\\[\\].+*?\\-^$\\\\|]");
	private static final List<String>						DECK_OPTS			= Arrays.asList(WAIT_TO_PLAY, SKIP_SHUFFLE, ALWAYS_START, INFINITE_HP);
	private static final List<String>						DECK_PROP			= Arrays.asList(DECK_NAME, PLAYER_LEVEL, PLAYER_NAME);
	private static final String								DECK_OPTS_PATTERN	= keywordsToRegexp(DECK_OPTS);
	private static final String								DECK_PROP_PATTERN	= keywordsToRegexp(DECK_PROP);
	private static final String								SPEC				= "(?<![\\\\]),|\\+|:|&";
	private static final String								NUMBER				= "[0-9]";
	private static final String								SEP					= "^ *##[^\n]*";
	private static final String								COMMENT				= "^ *#[^\n]*";
	private static final Collector<CharSequence, ?, String>	LINE_JOINER			= Collectors.joining("\n");
	private static final Pattern							COMMENT_PATTERN		= Pattern.compile("^ *#(?!#) ?([^\n]*)");
	private static final Pattern							SYNTAX_HL_PATTERN	= Pattern.compile(
																					"(?<OPTS>" + DECK_OPTS_PATTERN + ")"
																						+ "|(?<PROP>" + DECK_PROP_PATTERN + ")"
																						+ "|(?<SPEC>" + SPEC + ")"
																						+ "|(?<NUMBER>" + NUMBER + ")"
																						+ "|(?<SEP>" + SEP + ")"
																						+ "|(?<COMMENT>" + COMMENT + ")",
																					Pattern.MULTILINE);
	private static final List<String>						GROUPS				= Arrays.asList("OPTS", "PROP", "SPEC", "NUMBER", "SEP", "COMMENT");
	private static final Executor							EXECUTOR			= Executors.newCachedThreadPool();

	private static final String keywordsToRegexp(List<String> keywords) {
		return "^(" +
			String.join("|", keywords.stream()
				.map(s -> SPECIAL_REGEX_CHARS.matcher(s).replaceAll("\\\\$0"))
				.collect(Collectors.toList()))
			+ ")";
	}

	public DeckEditor() {
		super();
		init();
	}

	public void resetWith(String text) {
		if (text == null) {
			clear();
		}
		else {
			appendText(text);
		}
		getUndoManager().forgetHistory();
		getUndoManager().mark();
		selectRange(0, 0);
	}

	private final void init() {
		getStylesheets().add(DeckEditor.class.getResource("deck-editor.css").toExternalForm());
		setParagraphGraphicFactory(LineNumberFactory.get(this));

		// Compute highlighting o nchange
		richChanges()
			.filter(ch -> !ch.getInserted().equals(ch.getRemoved()))
			.successionEnds(Duration.ofMillis(1))
			.supplyTask(this::computeHighlightingAsync)
			.awaitLatest(richChanges())
			.filterMap(t -> {
				if (t.isSuccess()) {
					return Optional.of(t.get());
				}
				else {
					t.getFailure().printStackTrace();
					return Optional.empty();
				}
			})
			.subscribe(this::applyHighlighting);

		// Add shortcuts when the scene is set.
		sceneProperty().addListener((obs, old, scene) -> {
			if (scene != null) {
				// CTRL + Q > (un)comment line(s)
				scene.getAccelerators().put(new KeyCodeCombination(KeyCode.Q, KeyCodeCombination.CONTROL_DOWN), () -> {
					IndexRange range = getSelection();
					IndexRange linesRange = getSelectionLines(range);
					String selectedText = getText(linesRange.getStart(), linesRange.getEnd());
					int originalSize = selectedText.length();
					if (originalSize != 0) {
						try (BufferedReader reader = new BufferedReader(new StringReader(selectedText))) {
							AtomicReference<Integer> firstLineDiffSize = new AtomicReference<>();
							String replacement = reader.lines().map(l -> {
								String newline;
								if (l.trim().isEmpty()) {
									newline = "";
								}
								else {
									Matcher m = COMMENT_PATTERN.matcher(l);
									if (m.matches()) {
										newline = m.group(1);
									}
									else if (l.trim().startsWith("#")) {
										newline = l;
									}
									else {
										newline = "# " + l;
									}
								}
								int diff = newline.length() - l.length();
								firstLineDiffSize.compareAndSet(null, diff);
								return newline;
							}).collect(LINE_JOINER);
							int diffSize = replacement.length() - originalSize;

							getUndoManager().preventMerge();
							replaceText(linesRange.getStart(), linesRange.getEnd(), replacement);
							getUndoManager().preventMerge();

							selectRange(
								Math.max(range.getStart() + firstLineDiffSize.get(), linesRange.getStart()),
								range.getEnd() + diffSize);
						}
						catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				// CTRL + D > duplicate line(s)
				scene.getAccelerators().put(new KeyCodeCombination(KeyCode.D, KeyCodeCombination.CONTROL_DOWN), () -> {
					IndexRange range = getSelection();
					IndexRange linesRange = getSelectionLines(range);
					String selectedText = getText(linesRange.getStart(), linesRange.getEnd());

					getUndoManager().preventMerge();
					insertText(linesRange.getEnd(), "\n" + selectedText);
					getUndoManager().preventMerge();

					selectRange(range.getStart(), range.getEnd());
				});
			}
		});
	}

	private IndexRange getSelectionLines(IndexRange range) {
		int firstLineStart = getText().lastIndexOf("\n", range.getStart() - 1) + 1;
		int lastLineEnd = getText().indexOf("\n", range.getEnd());
		if (lastLineEnd == -1) {
			lastLineEnd = getLength();
		}
		return IndexRange.normalize(firstLineStart, lastLineEnd);
	}

	private Task<StyleSpans<Collection<String>>> computeHighlightingAsync() {
		String text = getText();
		Task<StyleSpans<Collection<String>>> task = new Task<StyleSpans<Collection<String>>>() {
			@Override
			protected StyleSpans<Collection<String>> call() throws Exception {
				return computeHighlighting(text);
			}
		};
		EXECUTOR.execute(task);
		return task;
	}

	private void applyHighlighting(StyleSpans<Collection<String>> highlighting) {
		setStyleSpans(0, highlighting);
	}

	private static StyleSpans<Collection<String>> computeHighlighting(String text) {
		Matcher matcher = SYNTAX_HL_PATTERN.matcher(text.toLowerCase());
		int lastKwEnd = 0;
		StyleSpansBuilder<Collection<String>> spansBuilder = new StyleSpansBuilder<>();
		while (matcher.find()) {
			String styleClass = GROUPS.stream()
				.filter(g -> matcher.group(g) != null)
				.limit(1)
				.map(String::toLowerCase)
				.findFirst()
				.orElse(null);

			/* never happens */ assert styleClass != null;
			spansBuilder.add(Collections.emptyList(), matcher.start() - lastKwEnd);
			spansBuilder.add(Collections.singleton(styleClass), matcher.end() - matcher.start());
			lastKwEnd = matcher.end();
		}
		spansBuilder.add(Collections.emptyList(), text.length() - lastKwEnd);
		return spansBuilder.create();
	}
}
