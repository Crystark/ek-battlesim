package crystark.ek.gui.view;

import crystark.ek.gui.MainApp;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class RootLayoutController {

	@FXML
	ImageView		donateImage;

	private MainApp	mainApp;

	@FXML
	public void initialize() {
		donateImage.setImage(new Image(ClassLoader.getSystemResourceAsStream("images/btn-donate.png")));
	}

	@FXML
	public void handleDonate() {
		openLink("https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=crystark%40gmail%2ecom&lc=US&item_name=Crystark&item_number=ek%2dbattlesim&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donate_SM%2egif%3aNonHosted");
	}

	@FXML
	public void handleWikiHome() {
		openLink("https://bitbucket.org/Crystark/ek-battlesim/wiki/Home");
	}

	@FXML
	public void handleWikiGettingStarted() {
		openLink("https://bitbucket.org/Crystark/ek-battlesim/wiki/Getting%20Started");
	}

	@FXML
	public void handleWikiAdvancedDeckBuilding() {
		openLink("https://bitbucket.org/Crystark/ek-battlesim/wiki/Advanced%20Deck%20building");
	}

	@FXML
	public void handleWikiGameLore() {
		openLink("https://bitbucket.org/Crystark/ek-battlesim/wiki/Game%20Lore");
	}

	@FXML
	public void handleWikiTipsTricks() {
		openLink("https://bitbucket.org/Crystark/ek-battlesim/wiki/Tips%20and%20Tricks");
	}

	@FXML
	public void handleWikiDI() {
		openLink("https://bitbucket.org/Crystark/ek-battlesim/wiki/Demon%20Invasion%20Simulation");
	}

	@FXML
	public void handleWikiEW() {
		openLink("https://bitbucket.org/Crystark/ek-battlesim/wiki/Elemental%20War%20Simulation");
	}

	@FXML
	public void handleWikiThief() {
		openLink("https://bitbucket.org/Crystark/ek-battlesim/wiki/Thief%20Simulation");
	}

	@FXML
	public void handleWikiHydra() {
		openLink("https://bitbucket.org/Crystark/ek-battlesim/wiki/Hydra%20Simulation");
	}

	@FXML
	public void handleWikiKW() {
		openLink("https://bitbucket.org/Crystark/ek-battlesim/wiki/Kingdom%20War%20Simulation");
	}

	@FXML
	public void handleWikiArena() {
		openLink("https://bitbucket.org/Crystark/ek-battlesim/wiki/Arena%20&%20Versus%20Simulation");
	}

	@FXML
	public void handleWikiFoH() {
		openLink("https://bitbucket.org/Crystark/ek-battlesim/wiki/Field%20of%20Honor%20Simulation");
	}

	@FXML
	public void handleReportIssue() {
		openLink("https://bitbucket.org/Crystark/ek-battlesim/issues/new");
	}

	@FXML
	public void handleExit() {
		Platform.exit();
	}

	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
	}

	private void openLink(String url) {
		mainApp.getHostServices().showDocument(url);
	}
}
