package crystark.ek.gui.view;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import javafx.application.Platform;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;

public class OutputController {
	private static final int	MAX_CONSOLE_OUTPUT	= 200000;

	@FXML
	private TextArea			consoleOutput;
	@FXML
	private ProgressBar			progressBar;
	@FXML
	private Label				currentTaskLabel;

	@FXML
	private void initialize() {
		Console out = new Console();
		System.setOut(new PrintStream(out, true));
		System.setErr(new PrintStream(out, true));
	}

	@FXML
	public void handleClear() {
		consoleOutput.clear();
	}

	public void bindProgress(ReadOnlyDoubleProperty progressProperty) {
		progressBar.progressProperty().bind(progressProperty);
		progressProperty.addListener((obs, old, nw) -> currentTaskLabel.setText((int) Math.floor(nw.doubleValue() * 100) + "%"));
	}

	private void updateConsole(String text) {
		Platform.runLater(() -> {
			consoleOutput.appendText(text);
			int length = consoleOutput.getText().length();
			if (length > MAX_CONSOLE_OUTPUT) {
				consoleOutput.deleteText(0, length - MAX_CONSOLE_OUTPUT);
				int nextLine = consoleOutput.getText().indexOf("\n");
				consoleOutput.deleteText(0, nextLine + 1);
			}
			consoleOutput.setScrollTop(Double.MAX_VALUE);
		});
	}

	class Console extends OutputStream {
		@Override
		public void write(int b) throws IOException {
			updateConsole(String.valueOf((char) b));
		}

		@Override
		public void write(byte[] b, int off, int len) throws IOException {
			updateConsole(new String(b, off, len));
		}

		@Override
		public void write(byte[] b) throws IOException {
			write(b, 0, b.length);
		}
	}
}
