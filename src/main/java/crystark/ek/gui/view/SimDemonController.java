package crystark.ek.gui.view;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import crystark.common.api.DeckDesc;
import crystark.common.format.CardDescFormat;
import crystark.common.format.DeckDescFormat;
import crystark.common.validator.DeckDescValidator;
import crystark.ek.battlesim.battle.Cards;
import crystark.ek.battlesim.task.SimDemonTaskOptions;
import crystark.ek.battlesim.task.TaskException;
import crystark.ek.battlesim.task.Tasks;
import crystark.ek.gui.MainApp;
import crystark.ek.gui.ToolController;
import crystark.ek.gui.model.SimDemonData;
import crystark.tools.Tools;
import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;

public class SimDemonController implements ToolController {

	private static final String			NEW_DEMON		= "New...";
	private static final String			DEFAULT_DEMON	= "Deck name: My Demon\nName, 99, 4, 1800, 12000000, demon, Retaliation:200, Devil's Curse:1000, Laceration, Immunity";

	@FXML
	private ComboBox<String>			demonSelection;
	@FXML
	private TextField					customDemon;
	@FXML
	private TextField					iterations;
	@FXML
	private Button						deleteDemonButton;
	@FXML
	private Button						selectDeckButton;
	@FXML
	private Button						editDeckButton;
	@FXML
	private Button						simButton;

	private MainApp						mainApp;
	private ObservableList<String>		demonsList;
	private SimpleObjectProperty<File>	selectedFile	= new SimpleObjectProperty<>();

	public SimDemonData getStorage() {
		return this.mainApp.getStorage().simDemonData;
	}

	@Override
	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
	}

	@Override
	public String getTabLabel() {
		return "Demon Sim";
	}

	@Override
	public Button getDefaultButton() {
		return simButton;
	}

	@FXML
	private void initialize() {
		demonsList = FXCollections.observableArrayList(Cards.getDemonsNameList());
		demonsList.add(NEW_DEMON);
		// Partial fix for Issue #189
		demonSelection.setVisibleRowCount(9); // So that there's always a scroll

		demonSelection.setItems(demonsList);

		// Make the button not take up space when hidden
		deleteDemonButton.managedProperty().bind(deleteDemonButton.visibleProperty());
		editDeckButton.visibleProperty().bind(selectedFile.isNotNull());
		editDeckButton.managedProperty().bind(editDeckButton.visibleProperty());

		iterations.addEventFilter(KeyEvent.KEY_TYPED, ke -> {
			char ar[] = ke.getCharacter().toCharArray();
			if (ar.length > 0) {
				char ch = ar[ar.length - 1];
				if (!(ch >= '0' && ch <= '9')) {
					ke.consume();
				}
			}
		});

		Platform.runLater(this::loadStorage);
	}

	@FXML
	private void handleDeckSelection() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
		File file = fileChooser.showOpenDialog(mainApp.getPrimaryStage());

		if (file != null) {
			selectDeck(file);
		}
	}

	@FXML
	private void handleNewDeck() {
		DeckFileEditor deckFileEditor = mainApp.openDeckEditor(null);
		if (deckFileEditor != null) {
			selectDeck(deckFileEditor.file());
		}
	}

	@FXML
	private void handleEditDeck() {
		mainApp.openDeckEditor(selectedFile.get());
	}

	@FXML
	private void handleDemonSelection() {
		String demon = getSelectedDemon();
		if (NEW_DEMON.equals(demon)) {
			Optional<String> desc = this.mainApp.openModalDeckEditor(
				"New demon deck",
				SimDemonTaskOptions.DEFAULTS
					.withPlayerNameTransformer(dd -> dd.deckName),
				dd -> {
					if (dd.deckName == null) {
						throw new TaskException("Missing deck name. Please add it using 'Deck name: TheNameYouWant'");
					}
					if (demonsList.contains(dd.deckName)) {
						throw new TaskException("A demon deck with that name already exists. Choose an other name or delete it first.");
					}
				},
				DEFAULT_DEMON);

			if (desc.isPresent()) {
				// If OK add the demon
				String newDemon = this.addDemon(desc.get());
				// and select it
				Platform.runLater(() -> demonSelection.getSelectionModel().select(newDemon));
			}
			else {
				// Else clear the selection
				Platform.runLater(demonSelection.getSelectionModel()::clearSelection);
			}
		}
		else {
			// Load custom demon description
			String desc = getStorage().customDemons.get(demon);
			if (desc == null) {
				desc = demon == null ? "" : CardDescFormat.toFullDescription(demon, 10);
				deleteDemonButton.setVisible(false);
			}
			else {
				try (BufferedReader br = new BufferedReader(new StringReader(desc))) {
					desc = br.lines().filter(s -> CardDescFormat.parseSilent(s) != null).findFirst().orElse(desc);
				}
				catch (IOException e) {
					e.printStackTrace();
				}
				deleteDemonButton.setVisible(true);
			}
			customDemon.setText(desc);

			// Mark last selected demon
			String associatedDeck = getStorage().associatedDecks.get(demon);
			if (associatedDeck != null) {
				File file = new File(associatedDeck);
				if (file.exists()) {
					selectDeck(file);
				}
			}
		}
	}

	@FXML
	private void handleView() {
		String demon = getSelectedDemon();
		if (demon != null) {
			// Load custom demon description
			String desc = getStorage().customDemons.get(demon);
			if (desc == null) {
				desc = CardDescFormat.toFullDescription(demon, 10);
			}
			if (desc != null) {
				System.out.println("--- View ---");
				System.out.println(desc);
				System.out.println("------------");
			}
		}
	}

	@FXML
	private void handleDelete() {
		String selected = getSelectedDemon();
		demonSelection.getSelectionModel().clearSelection();
		removeDemon(selected);
	}

	@FXML
	private void handleSim() {
		if (isInputValid()) {
			updateStorage();
			mainApp.runTask(Tasks
				.prepareDemonInvasionSim(getDeckDesc(), getDemonDescription())
				.iterations(getIter().longValue()));
		}
	}

	private void loadStorage() {
		SimDemonData storage = this.getStorage();

		demonsList.addAll(storage.customDemons.keySet());

		if (demonsList.contains(storage.lastDemon)) {
			demonSelection.getSelectionModel().select(storage.lastDemon);
		}

		iterations.setText(storage.getDefaultIterations());
	}

	private void updateStorage() {
		String filePath = selectedFile.get().getPath();
		String currentDir = System.getProperty("user.dir");
		if (filePath.startsWith(currentDir)) {
			filePath = "." + filePath.substring(currentDir.length());
		}

		SimDemonData storage = this.getStorage();
		String selected = getSelectedDemon();
		this.getStorage().lastDemon = selected;
		storage.associatedDecks.put(selected, filePath);
		storage.setDefaultIterations(iterations.getText());
	}

	private void selectDeck(File file) {
		selectedFile.set(file);
		selectDeckButton.setText(file.getName());
		if (mainApp.isDeckEditorOpen()) {
			handleEditDeck();
			mainApp.getPrimaryStage().requestFocus();
		}
	}

	private String addDemon(String description) {
		DeckDesc deckDesc = DeckDescFormat.parseString(null, description);
		getStorage().customDemons.put(deckDesc.deckName, description);
		demonsList.add(deckDesc.deckName);
		return deckDesc.deckName;
	}

	private void removeDemon(String name) {
		SimDemonData storage = getStorage();

		storage.associatedDecks.remove(name);
		String previous = storage.customDemons.remove(name);
		if (previous != null) {
			demonsList.remove(name);
		}
	}

	private Integer getIter() {
		String text = iterations.getText();
		int iter = 0;
		if (!text.isEmpty()) {
			iter = Tools.parseInt(text);
		}
		return iter > 0 ? iter : SimDemonData.DEFAULT_ITER;
	}

	private DeckDesc getDeckDesc() {
		return DeckDescFormat.parseFile(selectedFile.get());
	}

	private String getSelectedDemon() {
		return demonSelection.getSelectionModel().getSelectedItem();
	}

	private String getDemonDescription() {
		String selected = getSelectedDemon();
		String desc = getStorage().customDemons.get(selected);

		return desc == null ? selected : desc;
	}

	private boolean isInputValid() {
		List<String> errorMessage = new ArrayList<>();

		String selected = getDemonDescription();

		if (selected == null || NEW_DEMON.equals(selected)) {
			errorMessage.add("You must select a demon.");
		}
		if (selectedFile == null) {
			errorMessage.add("You must select a deck file.");
		}
		else {
			try {
				DeckDescValidator.validate(getDeckDesc());
			}
			catch (TaskException e) {
				errorMessage.add(e.getMessage());
			}
		}

		if (errorMessage.size() == 0) {
			return true;
		}
		else {
			// Show the error message.
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(mainApp.getPrimaryStage());
			alert.setTitle("Invalid Fields");
			alert.setHeaderText("Please correct invalid fields");
			alert.setContentText(String.join("\n", errorMessage));

			alert.showAndWait();

			return false;
		}
	}

}
