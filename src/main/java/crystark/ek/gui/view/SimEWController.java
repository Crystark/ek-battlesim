package crystark.ek.gui.view;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import crystark.common.api.DeckDesc;
import crystark.common.format.CardDescFormat;
import crystark.common.format.DeckDescFormat;
import crystark.common.validator.DeckDescValidator;
import crystark.ek.battlesim.task.SimElementalWarTaskOptions;
import crystark.ek.battlesim.task.TaskException;
import crystark.ek.battlesim.task.Tasks;
import crystark.ek.gui.MainApp;
import crystark.ek.gui.ToolController;
import crystark.ek.gui.model.SimEWData;
import crystark.tools.Tools;
import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;

public class SimEWController implements ToolController {

	private static final String			NEW_BOSS		= "New...";
	private static final String			DEFAULT_BOSS	= "Deck name: My Boss\nThe Boss, 22, 6, 1650, 230150, hydra, Devil's Curse:1000, Damnation:200, Evasion, Puncture:150, Resistance";

	@FXML
	private ComboBox<String>			bossSelection;
	@FXML
	private TextField					customBoss;
	@FXML
	private TextField					iterations;
	@FXML
	private Button						deleteBossButton;
	@FXML
	private Button						selectDeckButton;
	@FXML
	private Button						editDeckButton;
	@FXML
	private Button						simButton;

	private MainApp						mainApp;
	private ObservableList<String>		bossesList;
	private SimpleObjectProperty<File>	selectedFile	= new SimpleObjectProperty<>();

	public SimEWData getStorage() {
		return this.mainApp.getStorage().simEWData;
	}

	@Override
	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
	}

	@Override
	public String getTabLabel() {
		return "Elemental War Sim";
	}

	@Override
	public Button getDefaultButton() {
		return simButton;
	}

	@FXML
	private void initialize() {
		bossesList = FXCollections.observableArrayList(SimElementalWarTaskOptions.getAvailableBosses());
		bossesList.add(NEW_BOSS);
		bossSelection.setItems(bossesList);

		// Make the button not take up space when hidden
		deleteBossButton.managedProperty().bind(deleteBossButton.visibleProperty());
		editDeckButton.visibleProperty().bind(selectedFile.isNotNull());
		editDeckButton.managedProperty().bind(editDeckButton.visibleProperty());

		iterations.addEventFilter(KeyEvent.KEY_TYPED, ke -> {
			char ar[] = ke.getCharacter().toCharArray();
			if (ar.length > 0) {
				char ch = ar[ar.length - 1];
				if (!(ch >= '0' && ch <= '9')) {
					ke.consume();
				}
			}
		});

		Platform.runLater(this::loadStorage);
	}

	@FXML
	private void handleDeckSelection() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
		File file = fileChooser.showOpenDialog(mainApp.getPrimaryStage());

		if (file != null) {
			selectDeck(file);
		}
	}

	@FXML
	private void handleNewDeck() {
		DeckFileEditor deckFileEditor = mainApp.openDeckEditor(null);
		if (deckFileEditor != null) {
			selectDeck(deckFileEditor.file());
		}
	}

	@FXML
	private void handleEditDeck() {
		mainApp.openDeckEditor(selectedFile.get());
	}

	@FXML
	private void handleBossSelection() {
		String boss = getSelectedBoss();
		if (NEW_BOSS.equals(boss)) {
			Optional<String> desc = this.mainApp.openModalDeckEditor(
				"New EW deck",
				SimElementalWarTaskOptions.DEFAULTS
					.withPlayerNameTransformer(dd -> dd.deckName),
				dd -> {
					if (dd.deckName == null) {
						throw new TaskException("Missing deck name. Please add it using 'Deck name: TheNameYouWant'");
					}
					if (bossesList.contains(dd.deckName)) {
						throw new TaskException("A boss deck with that name already exists. Choose an other name or delete it first.");
					}
				},
				DEFAULT_BOSS);

			if (desc.isPresent()) {
				// If OK add the boss
				String newBoss = this.addBoss(desc.get());
				// and select it
				Platform.runLater(() -> bossSelection.getSelectionModel().select(newBoss));
			}
			else {
				// Else clear the selection
				Platform.runLater(bossSelection.getSelectionModel()::clearSelection);
			}
		}
		else {
			// Load custom boss description
			String desc = getStorage().customBosses.get(boss);
			if (desc == null) {
				desc = boss == null ? "" : CardDescFormat.toFullDescription(boss, 15);
				deleteBossButton.setVisible(false);
			}
			else {
				try (BufferedReader br = new BufferedReader(new StringReader(desc))) {
					desc = br.lines().filter(s -> CardDescFormat.parseSilent(s) != null).findFirst().orElse(desc);
				}
				catch (IOException e) {
					e.printStackTrace();
				}
				deleteBossButton.setVisible(true);
			}
			customBoss.setText(desc);

			// Mark last selected boss
			String associatedDeck = getStorage().associatedDecks.get(boss);
			if (associatedDeck != null) {
				File file = new File(associatedDeck);
				if (file.exists()) {
					selectDeck(file);
				}
			}
		}
	}

	@FXML
	private void handleView() {
		String boss = getSelectedBoss();
		if (boss != null) {
			// Load custom boss description
			String desc = getStorage().customBosses.get(boss);
			if (desc == null) {
				desc = DeckDescFormat.toDeckString(SimElementalWarTaskOptions.getEwDeck(boss), null, true);
			}
			if (desc != null) {
				System.out.println("--- View ---");
				System.out.println(desc);
				System.out.println("------------");
			}
		}
	}

	@FXML
	private void handleDelete() {
		String selected = getSelectedBoss();
		bossSelection.getSelectionModel().clearSelection();
		removeBoss(selected);
	}

	@FXML
	private void handleSim() {
		if (isInputValid()) {
			updateStorage();
			mainApp.runTask(Tasks
				.prepareElementalWarSim(getDeckDesc(), getBossDescription())
				.iterations(getIter().longValue()));
		}
	}

	private void loadStorage() {
		SimEWData storage = this.getStorage();

		bossesList.addAll(storage.customBosses.keySet());

		if (bossesList.contains(storage.lastBoss)) {
			bossSelection.getSelectionModel().select(storage.lastBoss);
		}

		iterations.setText(storage.getDefaultIterations());
	}

	private void updateStorage() {
		String filePath = selectedFile.get().getPath();
		String currentDir = System.getProperty("user.dir");
		if (filePath.startsWith(currentDir)) {
			filePath = "." + filePath.substring(currentDir.length());
		}

		SimEWData storage = this.getStorage();
		String selected = getSelectedBoss();
		this.getStorage().lastBoss = selected;
		storage.associatedDecks.put(selected, filePath);
		storage.setDefaultIterations(iterations.getText());
	}

	private void selectDeck(File file) {
		selectedFile.set(file);
		selectDeckButton.setText(file.getName());
		if (mainApp.isDeckEditorOpen()) {
			handleEditDeck();
			mainApp.getPrimaryStage().requestFocus();
		}
	}

	private String addBoss(String description) {
		DeckDesc deckDesc = DeckDescFormat.parseString(null, description);
		getStorage().customBosses.put(deckDesc.deckName, description);
		bossesList.add(deckDesc.deckName);
		return deckDesc.deckName;
	}

	private void removeBoss(String name) {
		SimEWData storage = getStorage();

		storage.associatedDecks.remove(name);
		String previous = storage.customBosses.remove(name);
		if (previous != null) {
			bossesList.remove(name);
		}
	}

	private Integer getIter() {
		String text = iterations.getText();
		int iter = 0;
		if (!text.isEmpty()) {
			iter = Tools.parseInt(text);
		}
		return iter > 0 ? iter : SimEWData.DEFAULT_ITER;
	}

	private DeckDesc getDeckDesc() {
		return DeckDescFormat.parseFile(selectedFile.get());
	}

	private String getSelectedBoss() {
		return bossSelection.getSelectionModel().getSelectedItem();
	}

	private String getBossDescription() {
		String selected = getSelectedBoss();
		String desc = getStorage().customBosses.get(selected);

		return desc == null ? selected : desc;
	}

	private boolean isInputValid() {
		List<String> errorMessage = new ArrayList<>();

		String selected = getBossDescription();

		if (selected == null || NEW_BOSS.equals(selected)) {
			errorMessage.add("You must select a boss.");
		}
		if (selectedFile == null) {
			errorMessage.add("You must select a deck file.");
		}
		else {
			try {
				DeckDescValidator.validate(getDeckDesc());
			}
			catch (TaskException e) {
				errorMessage.add(e.getMessage());
			}
		}

		if (errorMessage.size() == 0) {
			return true;
		}
		else {
			// Show the error message.
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(mainApp.getPrimaryStage());
			alert.setTitle("Invalid Fields");
			alert.setHeaderText("Please correct invalid fields");
			alert.setContentText(String.join("\n", errorMessage));

			alert.showAndWait();

			return false;
		}
	}

}
