package crystark.ek.gui;

import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.fxmisc.flowless.VirtualizedScrollPane;

import crystark.common.api.DeckDesc;
import crystark.common.format.DeckDescFormat;
import crystark.common.transform.DeckDescTransformer;
import crystark.common.validator.DeckDescValidator;
import crystark.common.validator.IValidator;
import crystark.ek.Constants;
import crystark.ek.battlesim.task.BaseTaskOptions;
import crystark.ek.battlesim.task.TaskException;
import crystark.ek.gui.view.DeckEditor;
import crystark.ek.gui.view.DeckFileEditor;
import crystark.ek.gui.view.OutputController;
import crystark.ek.gui.view.RootLayoutController;
import crystark.ek.gui.view.ToolSelectionController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class MainApp extends Application {

	private GuiStorage			storage;
	private Stage				primaryStage;
	private AnchorPane			mainAnchor;
	private OutputController	outputController;
	private AnchorPane			toolSelectionPane;
	private ExecutorService		executor;
	private DeckFileEditor		deckFileEditor;

	public static void main(String[] args) {
		launch(args);
	}

	public GuiStorage getStorage() {
		return storage;
	}

	@Override
	public void start(Stage primaryStage) {
		this.executor = Executors.newCachedThreadPool();
		this.primaryStage = primaryStage;
		primaryStage.setTitle(Constants.TITLE + " - GUI (Beta)");
		primaryStage.getIcons().add(new Image(ClassLoader.getSystemResourceAsStream("images/icon.jpg")));
		primaryStage.setOnCloseRequest(this::onClose);

		primaryStage.focusedProperty().addListener((obs, old, n3w) -> {
			if (n3w && deckFileEditor != null) {
				GuiUtils.toFrontOrdered((Stage) deckFileEditor.getScene().getWindow(), primaryStage);
			}
		});

		GuiUtils.loadStagePosition(primaryStage, MainApp.class);

		storage = loadGuiStorage();

		initRootLayout();
	}

	private GuiStorage loadGuiStorage() {
		Alert alert;
		try {
			return GuiStorage.load();
		}
		catch (Throwable t) {
			if (t instanceof IOException) {
				alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Creating GUI DB");
				alert.setHeaderText("No GUI DB found. An empty GUI DB will be created.");
			}
			else {
				alert = new Alert(AlertType.WARNING);
				alert.setTitle("Failed to load GUI DB");
				alert.setHeaderText("Failed to load GUI DB. A new empty GUI DB will be created.");
			}
		}

		Platform.runLater(() -> {
			alert.initOwner(primaryStage);
			alert.showAndWait();
		});

		return new GuiStorage();
	}

	public void onClose(WindowEvent e) {
		if (deckFileEditor != null) {
			deckFileEditor.getScene().getWindow().getOnCloseRequest().handle(e);
		}

		if (!e.isConsumed()) {
			// Close all other windows if main window is closed
			Platform.exit();
		}
	}

	@Override
	public void stop() {
		storage.save();

		GuiUtils.saveStagePosition(primaryStage, MainApp.class);
	}

	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public void initRootLayout() {
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
			BorderPane borderPane = (BorderPane) loader.load();

			// Show the scene containing the root layout.
			Scene scene = new Scene(borderPane);
			primaryStage.setScene(scene);
			primaryStage.minHeightProperty().bind(borderPane.minHeightProperty());
			primaryStage.minWidthProperty().bind(borderPane.minWidthProperty());

			borderPane.setCenter(mainAnchor = new AnchorPane());

			// Give the controller access to the main app.
			RootLayoutController controller = loader.getController();
			controller.setMainApp(this);

			showToolSelection();
			showOutput();

			primaryStage.show();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void showToolSelection() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/ToolSelection.fxml"));
			toolSelectionPane = (AnchorPane) loader.load();

			AnchorPane.setTopAnchor(toolSelectionPane, 0.0);
			AnchorPane.setBottomAnchor(toolSelectionPane, 0.0);
			AnchorPane.setLeftAnchor(toolSelectionPane, 0.0);
			AnchorPane.setRightAnchor(toolSelectionPane, 0.0);
			mainAnchor.getChildren().add(toolSelectionPane);

			ToolSelectionController controller = loader.getController();
			controller.setMainApp(this);
			controller.createToolTab("SimDemon");
			controller.createToolTab("SimEW");
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void showOutput() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/Output.fxml"));
			AnchorPane pane = (AnchorPane) loader.load();

			AnchorPane.setTopAnchor(pane, ToolSelectionController.TAB_CONTENT_HEIGHT);
			AnchorPane.setBottomAnchor(pane, 0.0);
			AnchorPane.setLeftAnchor(pane, ToolSelectionController.TAB_WIDTH);
			AnchorPane.setRightAnchor(pane, 0.0);
			mainAnchor.getChildren().add(pane);

			outputController = loader.getController();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Optional<String> openModalDeckEditor(String title, DeckDescTransformer transformer, IValidator<DeckDesc> additionalValidator, String content) {
		DeckEditor deckEditor = new DeckEditor();
		deckEditor.resetWith(content);

		Dialog<String> dialog = new Dialog<>();
		dialog.getDialogPane().setContent(new StackPane(new VirtualizedScrollPane<>(deckEditor)));
		dialog.getDialogPane().getButtonTypes().setAll(ButtonType.OK, ButtonType.CANCEL);

		dialog.initModality(Modality.WINDOW_MODAL);
		dialog.initOwner(primaryStage);
		dialog.setTitle(title);
		dialog.setResizable(true);

		dialog.getDialogPane().setPrefWidth(760);
		dialog.getDialogPane().setPrefHeight(300);
		((Stage) dialog.getDialogPane().getScene().getWindow()).setMinWidth(760);
		((Stage) dialog.getDialogPane().getScene().getWindow()).setMinHeight(300);

		Button okButton = (Button) dialog.getDialogPane().lookupButton(ButtonType.OK);
		okButton.addEventFilter(ActionEvent.ACTION, ae -> {
			try {
				DeckDesc dd = DeckDescFormat.parseString(null, deckEditor.getText());
				if (transformer != null) {
					dd = transformer.apply(dd);
				}

				if (additionalValidator != null) {
					additionalValidator.apply(dd);
				}

				DeckDescValidator.validate(dd);
			}
			catch (TaskException e) {
				GuiUtils.alertSimple("Deck doesn't pass validation", e.getMessage(), primaryStage);
				ae.consume();
			}
		});

		Platform.runLater(() -> deckEditor.requestFocus());

		dialog.setResultConverter(dialogButton -> {
			if (dialogButton.getButtonData().isDefaultButton()) {
				return deckEditor.getText();
			}
			return null;
		});

		Optional<String> result = dialog.showAndWait();
		return result;
	}

	public DeckFileEditor openDeckEditor(File file) {
		Stage stage;
		if (deckFileEditor == null) {
			deckFileEditor = new DeckFileEditor();

			stage = new Stage();
			stage.setScene(new Scene(new StackPane(new VirtualizedScrollPane<>(deckFileEditor))));
			stage.setMinWidth(600);
			stage.setMinHeight(300);
			stage.setWidth(760);
			stage.setHeight(500);

			deckFileEditor.divergedProperty().addListener((obs, old, n3w) -> {
				File f = deckFileEditor.file();
				if (f == null) {
					stage.setTitle("New Deck");
				}
				else {
					stage.setTitle((n3w ? "*" : "") + f.getName());
				}
			});

			deckFileEditor.focusedProperty().addListener((obs, old, n3w) -> {
				if (n3w) {
					GuiUtils.toFrontOrdered(primaryStage, (Stage) deckFileEditor.getScene().getWindow());
				}
			});

			stage.setOnCloseRequest(e -> {
				if (deckFileEditor.clearAll()) {
					GuiUtils.saveStagePosition(stage, DeckFileEditor.class);
				}
				else {
					e.consume();
				}
			});
			stage.setOnShown(e -> GuiUtils.loadStagePosition(stage, DeckFileEditor.class));
		}
		else {
			stage = (Stage) deckFileEditor.getScene().getWindow();
		}

		if (file == null ? deckFileEditor.newFile() : deckFileEditor.loadFile(file)) {
			stage.show();
			stage.requestFocus();
			return deckFileEditor;
		}

		return null;
	}

	public boolean isDeckEditorOpen() {
		return deckFileEditor != null && deckFileEditor.getScene().getWindow().isShowing();
	}

	public void backgroundTask(Runnable task) {
		executor.execute(task);
	}

	public Task<Void> runTask(BaseTaskOptions<?> taskOptions) {
		toolSelectionPane.setDisable(true);

		Task<Void> task = new Task<Void>() {
			@Override
			public Void call() {
				taskOptions
					.progressListener((n, m) -> updateProgress(n, 1))
					.run();
				return null;
			}
		};

		EventHandler<WorkerStateEvent> enable = t -> {
			toolSelectionPane.setDisable(false);
			toolSelectionPane.requestFocus();
		};
		task.addEventHandler(WorkerStateEvent.WORKER_STATE_SUCCEEDED, enable);
		task.addEventHandler(WorkerStateEvent.WORKER_STATE_FAILED, enable);

		outputController.bindProgress(task.progressProperty());

		executor.execute(task);
		return task;
	}

}
