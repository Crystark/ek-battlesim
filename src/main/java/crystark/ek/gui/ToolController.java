package crystark.ek.gui;

import javafx.scene.control.Button;

public interface ToolController {
	public void setMainApp(MainApp mainApp);

	public String getTabLabel();

	public Button getDefaultButton();
}
