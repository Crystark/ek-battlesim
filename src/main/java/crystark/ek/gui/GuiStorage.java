package crystark.ek.gui;

import com.google.gson.Gson;
import crystark.ek.gui.model.SimDemonData;
import crystark.ek.gui.model.SimEWData;
import crystark.tools.Tools;

import java.io.File;
import java.io.IOException;

public class GuiStorage {
	private static final String	PATH			= "db/gui_0001.json";
	private static final Gson	GSON			= new Gson();

	public final SimDemonData	simDemonData	= new SimDemonData();
	public final SimEWData		simEWData		= new SimEWData();
	public String				lastTab;

	public static GuiStorage load() throws IOException {
		String content = Tools.readFile(Tools.getStartDir() + File.separator + PATH);
		return GSON.fromJson(content, GuiStorage.class);
	}

	public void save() {
		Tools.writeToFile(Tools.getStartDir() + File.separator + PATH, GSON.toJson(this));
	}
}
