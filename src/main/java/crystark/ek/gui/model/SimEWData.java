package crystark.ek.gui.model;

import java.util.HashMap;
import java.util.prefs.Preferences;

public class SimEWData {
	private static final Preferences		PREFS			= Preferences.userNodeForPackage(SimEWData.class);

	public static final Integer				DEFAULT_ITER	= 50000;

	public final HashMap<String, String>	customBosses	= new HashMap<>();									// name => desc
	public final HashMap<String, String>	associatedDecks	= new HashMap<>();									// name => path
	public String							lastBoss;

	public String getDefaultIterations() {
		return PREFS.get("iter", DEFAULT_ITER.toString());
	}

	public void setDefaultIterations(String iter) {
		PREFS.put("iter", iter);
	}
}
