package crystark.ek.gui.model;

import java.util.HashMap;
import java.util.prefs.Preferences;

public class SimDemonData {
	private static final Preferences		PREFS			= Preferences.userNodeForPackage(SimDemonData.class);

	public static final Integer				DEFAULT_ITER	= 50000;

	public final HashMap<String, String>	customDemons	= new HashMap<>();										// name => desc
	public final HashMap<String, String>	associatedDecks	= new HashMap<>();										// name => path
	public String							lastDemon;

	public String getDefaultIterations() {
		return PREFS.get("iter", DEFAULT_ITER.toString());
	}

	public void setDefaultIterations(String iter) {
		PREFS.put("iter", iter);
	}
}
