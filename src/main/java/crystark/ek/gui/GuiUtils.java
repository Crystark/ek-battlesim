package crystark.ek.gui;

import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.Window;

public class GuiUtils {

	public static void alertSimple(String title, String message, Stage stage) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setGraphic(null);
		alert.setHeaderText(null);
		alert.initOwner(stage);
		alert.setTitle(title);
		alert.setContentText(message);

		alert.showAndWait();
	}

	public static boolean dialogConfirm(String text, Window owner) {
		return dialogConfirm(text, owner, new String[0]).equals(ButtonType.OK);
	}

	public static ButtonType dialogConfirm(String text, Window owner, String... buttons) {
		List<ButtonType> bt = new ArrayList<>();
		bt.add(ButtonType.OK);
		bt.add(ButtonType.CANCEL);

		for (String buttonType : buttons) {
			bt.add(new ButtonType(buttonType));
		}
		return dialogConfirm(text, owner, bt.toArray(new ButtonType[bt.size()]));
	}

	public static ButtonType dialogYesNoCancel(String text, Window owner) {
		return dialogConfirm(text, owner, ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
	}

	public static ButtonType dialogConfirm(String text, Window owner, ButtonType... buttons) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.initOwner(owner);
		alert.setTitle(null);
		alert.setHeaderText(null);
		alert.setContentText(text);

		alert.getButtonTypes().setAll(buttons);

		alert.setX(owner.getX() + owner.getWidth() / 2 - alert.getWidth() / 2);
		alert.setY(owner.getY() + owner.getHeight() / 2 - alert.getHeight() / 2);

		return alert.showAndWait().get();
	}

	public static void loadStagePosition(Stage stage, Class<?> storageLevel) {
		Preferences userPrefs = Preferences.userNodeForPackage(storageLevel);
		String prefix = storageLevel.getSimpleName().toLowerCase() + ".";

		Bounds allScreenBounds = computeAllScreenBounds();
		double x = Math.max(userPrefs.getDouble(prefix + "stage.x", stage.getX()), allScreenBounds.getMinX());
		double y = Math.max(userPrefs.getDouble(prefix + "stage.y", stage.getY()), allScreenBounds.getMinY());

		stage.setX(x);
		stage.setY(y);

		if (userPrefs.getBoolean(prefix + "stage.maximized", false)) {
			stage.setMaximized(true);
		}
		else {
			double w = userPrefs.getDouble(prefix + "stage.width", stage.getWidth());
			double h = userPrefs.getDouble(prefix + "stage.height", stage.getHeight());

			if (x + w <= allScreenBounds.getMaxX()) {
				stage.setWidth(w);
			}
			if (y + h <= allScreenBounds.getMaxY()) {
				stage.setHeight(h);
			}
		}
	}

	public static void saveStagePosition(Stage stage, Class<?> storageLevel) {
		Preferences userPrefs = Preferences.userNodeForPackage(storageLevel);
		String prefix = storageLevel.getSimpleName().toLowerCase() + ".";

		userPrefs.putDouble(prefix + "stage.x", stage.getX());
		userPrefs.putDouble(prefix + "stage.y", stage.getY());

		if (stage.isMaximized()) {
			userPrefs.putBoolean(prefix + "stage.maximized", true);
			userPrefs.remove(prefix + "stage.width");
			userPrefs.remove(prefix + "stage.height");
		}
		else {
			userPrefs.remove(prefix + "stage.maximized");
			userPrefs.putDouble(prefix + "stage.width", stage.getWidth());
			userPrefs.putDouble(prefix + "stage.height", stage.getHeight());
		}
	}

	private static Bounds computeAllScreenBounds() {
		double minX = Double.POSITIVE_INFINITY;
		double minY = Double.POSITIVE_INFINITY;
		double maxX = Double.NEGATIVE_INFINITY;
		double maxY = Double.NEGATIVE_INFINITY;
		for (Screen screen : Screen.getScreens()) {
			Rectangle2D screenBounds = screen.getBounds();
			if (screenBounds.getMinX() < minX) {
				minX = screenBounds.getMinX();
			}
			if (screenBounds.getMinY() < minY) {
				minY = screenBounds.getMinY();
			}
			if (screenBounds.getMaxX() > maxX) {
				maxX = screenBounds.getMaxX();
			}
			if (screenBounds.getMaxY() > maxY) {
				maxY = screenBounds.getMaxY();
			}
		}
		return new BoundingBox(minX, minY, maxX - minX, maxY - minY);
	}

	public static void toFrontOrdered(Stage... stagesBackToFront) {
		for (Stage stage : stagesBackToFront) {
			stage.setAlwaysOnTop(true);
			stage.setAlwaysOnTop(false);
		}
	}
}
