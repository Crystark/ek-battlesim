package crystark.ek.cli;

import java.util.Objects;
import java.util.function.BiConsumer;

public class ConsoleProgressListener implements BiConsumer<Float, String> {
	static final String	BASE			= "\r[";
	static final int	WIDTH			= 50;
	int					maxActionLength	= 0;
	int					previousPercent	= -1;
	String				previousAction;
	StringBuilder		sb				= new StringBuilder(WIDTH + 20).append(BASE);

	@Override
	public void accept(Float progess, String currentAction) {
		int percent = (int) (progess * 100);
		if (previousPercent != percent || !Objects.equals(previousAction, currentAction)) {
			sb.setLength(BASE.length());
			previousPercent = percent;
			previousAction = currentAction;
			int i = 0;
			int eqLenght = (int) (progess * WIDTH);
			for (; i <= eqLenght; i++) {
				sb.append("=");
			}
			i--;
			sb.append(">");
			for (; i < WIDTH; i++) {
				sb.append(" ");
			}
			sb.append("] ").append(percent).append("%");

			if (progess == 1f) {
				sb.append(String.format("%-" + (maxActionLength + 5) + "s\n", ""));
			}
			else {
				int length = 0;
				if (currentAction != null) {
					sb.append(" ").append(currentAction).append(" ...");
					length = currentAction.length();
				}
				if (length < maxActionLength) {
					sb.append(String.format("%-" + (maxActionLength - length) + "s", ""));
				}
				else {
					maxActionLength = length;
				}
			}
			System.out.print(sb.toString());
		}
	}
}
