package crystark.ek.cli;

import crystark.common.api.DeckDesc;
import crystark.common.api.Server;
import crystark.common.format.DeckDescFormat;
import crystark.ek.battlesim.battle.Cards;
import crystark.ek.battlesim.task.BaseTaskOptions;
import crystark.ek.battlesim.task.SimKingdomWarTaskOptions;
import crystark.ek.battlesim.task.SimTaskOptions;
import crystark.ek.battlesim.task.TaskOptions;
import crystark.ek.battlesim.task.Tasks;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static crystark.ek.Constants.NL;
import static crystark.ek.Constants.THREADS;

public class BootOptions {

	static final Option				OPT_HELP				= new Option("h", "help", false, "Print this help");

	@SuppressWarnings("static-access")
	static final Option				OPT_THREADS				= OptionBuilder
																.withLongOpt("threads")
																.hasArg()
																.withArgName("#")
																.withDescription("Sets number of threads (default to " + THREADS + ").")
																.withType(Number.class)
																.create("th");

	@SuppressWarnings("static-access")
	static final Option				OPT_NO_PROGRESS_BAR		= OptionBuilder
																.withLongOpt("no-progress-bar")
																.withDescription("Deactivates the progress bar. Usually usefull when outputing to a file.")
																.create("npb");

	@SuppressWarnings("static-access")
	static final Option				OPT_ITER				= OptionBuilder
																.withLongOpt("iter")
																.hasArg()
																.withArgName("#")
																.withDescription(
																	"Sets number of iterations per defender (default 50000 unless otherwise specified)."
																		+ "Each simulation will run this number of fights for each defender.")
																.withType(Number.class)
																.create("i");

	@SuppressWarnings("static-access")
	static final Option				OPT_DECK				= OptionBuilder
																.withLongOpt("deck")
																.hasArg()
																.withArgName("file")
																.withDescription("Reads the deck from the given filename")
																.create("dk");

	@SuppressWarnings("static-access")
	static final Option				OPT_DECK_ALT			= OptionBuilder
																.withLongOpt("deck-alt")
																.hasArg()
																.withArgName("file")
																.withDescription("Alternate deck used in some sims")
																.create("dka");

	@SuppressWarnings("static-access")
	static final Option				OPT_DECK_SWITCH_AT_HP	= OptionBuilder
																.withLongOpt("deck-switch-at-hp")
																.hasArg()
																.withArgName("#")
																.withType(Number.class)
																.withDescription(
																	"Overrides the default behavior of switching decks with -dka." + NL
																		+ "The switch will occur when the whole opponent's deck HP goes under the provided theshold.")
																.create("dsh");

	@SuppressWarnings("static-access")
	static final Option				OPT_WAIT				= OptionBuilder
																.withDescription("Waits for the user to press enter to start the actual sim.")
																.create("wait");

	@SuppressWarnings("static-access")
	static final Option				OPT_DEBUG				= OptionBuilder
																.withDescription(
																	"Sets the debug mode which will print alot of information on what's going on." + NL
																		+ "Fixes the number of threads to 1 and the max iter to 10 (you can manually set a lower value)." + NL
																		+ "It is advised to redirect the output to a file ending the command with '> output.txt'.")
																.create("debug");

	@SuppressWarnings("static-access")
	static final Option				OPT_DEMON				= OptionBuilder
																.withLongOpt("demon")
																.hasArg()
																.withArgName("name")
																.withDescription(
																	"The demon to fight surrounded by double quotes (e.g. \"Dark titan\")." + NL
																		+ "You can also use a full card description if you want to sim against a custom demon." + NL
																		+ "Full decks are also allowed as long as they contain a demon to sim a demon with mobs." + NL
																		+ "Valid names are:" + NL
																		+ "\"" + String.join("\"" + NL + "\"", Cards.getDemonsNameList()) + "\"")
																.create("dm");

	@SuppressWarnings("static-access")
	static final Option				OPT_DEMON_SHOW_DAMAGE	= OptionBuilder
																.withLongOpt("demon-show-damage")
																.withDescription(
																	"Allows to print the damage done for each DI sim iteration.")
																.create("dmsd");

	@SuppressWarnings("static-access")
	static final Option				OPT_DEMON_MAX_DAMAGE	= OptionBuilder
																.withLongOpt("demon-max-damage")
																.hasArg()
																.withDescription("Limits damage to demon (HF/LoA)")
																.create("dmmd");

	@SuppressWarnings("static-access")
	static final Option				OPT_ELEMENTAL_WAR		= OptionBuilder
																.withLongOpt("elemental-war")
																.hasArg()
																.withArgName("file")
																.withDescription("The EW deck against which to fight. You can provide a complete deck or reference an existing one with it's boss name.")
																.create("ew");
	@SuppressWarnings("static-access")
	static final Option 			OPT_WONDER_TOWER 		= OptionBuilder
																.withLongOpt("tower-of-wonder")
																.hasArg()
																.withArgName("file")
																.withDescription("Reads the boss deck from the given filename.")
																.create("tow");

	@SuppressWarnings("static-access")
	static final Option				OPT_INPUT				= OptionBuilder
																.withLongOpt("input")
																.hasArg()
																.withArgName("file")
																.withDescription(
																	"Used with conversion options. Input files or directory of files you want converted.")
																.create("in");

	@SuppressWarnings("static-access")
	static final Option				OPT_VERSUS				= OptionBuilder
																.withLongOpt("versus")
																.hasArg()
																.withArgName("file")
																.withDescription(
																	"Another deck file against which to fight." + NL
																		+ "You can repeat this option multiple times to stack multiple fights." + NL
																		+ "You can reference a folder to sim against all the files in it.")
																.create("vs");

	@SuppressWarnings("static-access")
	static final Option				OPT_THIEF				= OptionBuilder
																.withDescription(
																	"Sim a battle against level 100 thieves." + NL
																		+ "Default iter will be set down to 20000 unless overrided.")
																.create("thief");

	@SuppressWarnings("static-access")
	static final Option				OPT_HYDRA				= OptionBuilder
																.withLongOpt("hydra")
																.hasOptionalArg()
																.withArgName("name")
																.withDescription(
																	"Sim battles against all hydras." + NL
																		+ "Default iter will be set down to 10000 unless overrided." + NL
																		+ "You can provide an argument surrounded by double quotes to this option to filter hydras." + NL
																		+ "You can repeat this option multiple times to sim against multiple hydras." + NL
																		+ "Some valid options: \"h1\" \"h3\" \"h3 curse\" \"Hydra II (Weaken)\" \"H5\"..." + NL)
																.create("hy");

	@SuppressWarnings("static-access")
	static final Option				OPT_HYDRA_ONLY			= OptionBuilder
																.withLongOpt("hydra-only")
																.withDescription(
																	"Restrict the hydra battles to facing the hydra only." + NL
																		+ "The hydra will have no cards with it and will be full life.")
																.create("hyo");
	@SuppressWarnings("static-access")
	static final Option				OPT_KINGDOM_WAR			= OptionBuilder
																.withLongOpt("kingdom-war")
																.hasOptionalArg()
																.withArgName("guard")
																.withDescription(
																	"Simulate KW multiple consecutive battles against all known guard decks." + NL
																		+ "Default iter will be set down to 10000 unless overrided." + NL
																		+ "You can provide a specific kw guard as an argument to this option surrounded by double quotes (e.g. \"fd_56\")." + NL
																		+ "You can repeat this option multiple times to increase the pool of decks the sim will pick from." + NL
																		+ "Valid guards are:" + NL
																		+ "\"" + String.join("\"" + NL + "\"", SimKingdomWarTaskOptions.DECKS) + "\"")
																.create("kw");

	@SuppressWarnings("static-access")
	static final Option				OPT_LIMIT				= OptionBuilder
																.withLongOpt("limit")
																.hasArg()
																.withArgName("#")
																.withType(Number.class)
																.withDescription(
																	"Used with the --" + OPT_KINGDOM_WAR.getLongOpt() + " option. Max number of consecutive fights (default 1000).")
																.create("l");

	@SuppressWarnings("static-access")
	static final Option				OPT_FOH					= OptionBuilder
																.hasArg()
																.withArgName("server")
																.withDescription(
																	"Auto-load and run current FoH round for the specified server." + NL
																		+ "Requires an internet connection !" + NL
																		+ "Valid servers are:" + NL
																		+ "\"" + String.join("\"" + NL + "\"", Server.names()) + "\"")
																.create("foh");

	@SuppressWarnings("static-access")
	static final Option				OPT_EXTRACT_FOH			= OptionBuilder
																.withLongOpt("extract-foh")
																.hasArg()
																.withArgName("server")
																.withDescription(
																	"Download FoH matches from the provided server." + NL
																		+ "Requires an internet connection !" + NL
																		+ "This task will create a subfolder named with this season's condition.")
																.create("efoh");

	@SuppressWarnings("static-access")
	static final Option				OPT_EXTRACT_RM			= OptionBuilder
																.withLongOpt("extract-rm")
																.hasArg()
																.withArgName("server")
																.withDescription(
																	"Download ranked matches from the provided server." + NL
																		+ "Requires an internet connection !" + NL
																		+ "This task is limited to downloading 1 deck every 5 seconds to limit potential abuse.")
																.create("erm");

	@SuppressWarnings("static-access")
	static final Option				OPT_EXTRACT_RM_FROM		= OptionBuilder
																.withLongOpt("erm-from")
																.hasArg()
																.withArgName("#")
																.withType(Number.class)
																.withDescription(
																	"Used with the --" + OPT_EXTRACT_RM.getLongOpt() + " option. Rank from which to start downloading RM decks (default 1).")
																.create("ermf");

	@SuppressWarnings("static-access")
	static final Option				OPT_EXTRACT_RM_COUNT	= OptionBuilder
																.withLongOpt("erm-count")
																.hasArg()
																.withArgName("#")
																.withType(Number.class)
																.withDescription(
																	"Used with the --" + OPT_EXTRACT_RM.getLongOpt() + " option. Number of RM decks to download (default 100, max 100).")
																.create("ermc");

	@SuppressWarnings("static-access")
	static final Option				OPT_EXTRACT_OUTPUT		= OptionBuilder
																.withLongOpt("extract-output")
																.hasArg()
																.withArgName("path")
																.withDescription(
																	"Used with exctracting options. Directory to which you want to output the retrieved decks.")
																.create("out");

	@SuppressWarnings("static-access")
	static final Option				OPT_UPDATE_LOCAL_DB		= OptionBuilder
																.withLongOpt("update-local-db")
																.withDescription(
																	"Load a local version of the database to override the bundled database." + NL
																		+ "/!\\ This could cause the sim to break. /!\\" + NL
																		+ "If it does just delete the 'db' folder to fallback to the bundled database.")
																.create("uldb");

	static final Options			OPTIONS					= new Options()
																.addOption(OPT_NO_PROGRESS_BAR)
																.addOption(OPT_HELP)
																.addOption(OPT_THREADS)
																.addOption(OPT_ITER)
																.addOption(OPT_DECK_ALT)
																.addOption(OPT_DECK)
																.addOption(OPT_DECK_SWITCH_AT_HP)
																.addOption(OPT_DEBUG)
																.addOption(OPT_DEMON)
																.addOption(OPT_DEMON_SHOW_DAMAGE)
																.addOption(OPT_DEMON_MAX_DAMAGE)
																.addOption(OPT_ELEMENTAL_WAR)
																.addOption(OPT_WONDER_TOWER)
																.addOption(OPT_FOH)
																.addOption(OPT_VERSUS)
																.addOption(OPT_HYDRA)
																.addOption(OPT_HYDRA_ONLY)
																.addOption(OPT_KINGDOM_WAR)
																.addOption(OPT_LIMIT)
																.addOption(OPT_THIEF)
																.addOption(OPT_EXTRACT_FOH)
																.addOption(OPT_EXTRACT_RM)
																.addOption(OPT_EXTRACT_RM_FROM)
																.addOption(OPT_EXTRACT_RM_COUNT)
																.addOption(OPT_EXTRACT_OUTPUT)
																.addOption(OPT_UPDATE_LOCAL_DB)
																.addOption(OPT_INPUT)
																.addOption(OPT_WAIT);

	private static final Option[]	ONE_AND_ONLY_ONE		= { OPT_INPUT, OPT_DEMON, OPT_ELEMENTAL_WAR, OPT_WONDER_TOWER, OPT_VERSUS, OPT_THIEF, OPT_HYDRA, OPT_KINGDOM_WAR, OPT_FOH, OPT_EXTRACT_RM, OPT_EXTRACT_FOH, OPT_UPDATE_LOCAL_DB };
	private static final Option[]	REQUIRES_DECK			= { OPT_DEMON, OPT_ELEMENTAL_WAR, OPT_WONDER_TOWER, OPT_VERSUS, OPT_THIEF, OPT_HYDRA, OPT_KINGDOM_WAR };
	private static final Option[]	REQUIRES_EXTRACT_OUTPUT	= { OPT_EXTRACT_RM, OPT_EXTRACT_FOH, OPT_INPUT };

	private static final int		WIDTH					= 100;
	private static final String		SEP;

	static {
		StringBuilder s = new StringBuilder();
		for (int i = 0; i < WIDTH; i++) {
			s.append("-");
		}
		SEP = s.toString();
	}

	private CommandLine cmd;

	BootOptions(String[] args) {
		try {
			cmd = new BasicParser().parse(OPTIONS, args);
		}
		catch (ParseException e) {
			throw new BootException(e.getMessage(), e);
		}
		if (cmd.hasOption(OPT_HELP.getOpt())) {
			throw new BootException("");
		}
		else {
			boolean foundOne = false;
			for (Option opt : ONE_AND_ONLY_ONE) {
				if (cmd.hasOption(opt.getOpt())) {
					if (foundOne) {
						throw new BootException("You must specify only one of the following options: " + optsToString(ONE_AND_ONLY_ONE));
					}
					foundOne = true;
				}
			}
			if (!foundOne) {
				throw new BootException("You must specify one of the following options: " + optsToString(ONE_AND_ONLY_ONE));
			}

			checkRequirement(cmd, OPT_DECK, REQUIRES_DECK);
			checkRequirement(cmd, OPT_EXTRACT_OUTPUT, REQUIRES_EXTRACT_OUTPUT);
		}
	}

	boolean isDebug() {
		return cmd.hasOption(OPT_DEBUG.getOpt());
	}

	boolean isWait() {
		return cmd.hasOption(OPT_WAIT.getOpt());
	}

	boolean showProgessBar() {
		return !cmd.hasOption(OPT_NO_PROGRESS_BAR.getOpt());
	}

	private Long getIters() {
		if (cmd.hasOption(OPT_ITER.getOpt())) {
			return parseLong(OPT_ITER.getOpt());
		}
		return null;
	}

	private Integer getThreads() {
		if (cmd.hasOption(OPT_THREADS.getOpt())) {
			return parseInteger(OPT_THREADS.getOpt());
		}
		return null;
	}

	private String getDemon() {
		if (cmd.hasOption(OPT_DEMON.getOpt())) {
			return cmd.getOptionValue(OPT_DEMON.getOpt());
		}
		return null;
	}

	private boolean showDemonDamage() {
		return cmd.hasOption(OPT_DEMON_SHOW_DAMAGE.getOpt());
	}

	private long maxDemonDamage() {
		final String value = cmd.getOptionValue(OPT_DEMON_MAX_DAMAGE.getOpt());
		long maxDmg = value == null ? Long.MAX_VALUE : Long.parseLong(value);
		return maxDmg;
	}

	private String getEWDeck() {
		if (cmd.hasOption(OPT_ELEMENTAL_WAR.getOpt())) {
			return cmd.getOptionValue(OPT_ELEMENTAL_WAR.getOpt());
		}
		return null;
	}
	private String[] getWonderTowerDeck() {
		if (cmd.hasOption(OPT_WONDER_TOWER.getOpt())) {
			return cmd.getOptionValues(OPT_WONDER_TOWER.getOpt());
		}
		return null;
	}

	private String[] getInputFiles() {
		if (cmd.hasOption(OPT_INPUT.getOpt())) {
			return cmd.getOptionValues(OPT_INPUT.getOpt());
		}
		return null;
	}

	String[] getVersusFiles() {
		if (cmd.hasOption(OPT_VERSUS.getOpt())) {
			return cmd.getOptionValues(OPT_VERSUS.getOpt());
		}
		return null;
	}

	private DeckDesc getDeck() {
		if (cmd.hasOption(OPT_DECK.getOpt())) {
			return DeckDescFormat.parseFile(cmd.getOptionValue(OPT_DECK.getOpt()));
		}
		return null;
	}

	private DeckDesc getDeckAlt() {
		if (cmd.hasOption(OPT_DECK_ALT.getOpt())) {
			return DeckDescFormat.parseFile(cmd.getOptionValue(OPT_DECK_ALT.getOpt()));
		}
		return null;
	}

	private Integer deckSwitchAtHp() {
		if (cmd.hasOption(OPT_DECK_SWITCH_AT_HP.getOpt())) {
			return parseInteger(OPT_DECK_SWITCH_AT_HP.getOpt());
		}
		return null;
	}

	private boolean isThief() {
		return cmd.hasOption(OPT_THIEF.getOpt());
	}

	private static Server toServer(String server) {
		server = server.replace("-", "_");
		return Server.valueOf(server.toLowerCase());
	}

	private Server getFoHServer() {
		if (cmd.hasOption(OPT_FOH.getOpt())) {
			return toServer(cmd.getOptionValue(OPT_FOH.getOpt()));
		}
		return null;
	}

	String[] getHydras() {
		if (cmd.hasOption(OPT_HYDRA.getOpt())) {
			String[] hydras = cmd.getOptionValues(OPT_HYDRA.getOpt());
			return hydras == null ? new String[0] : hydras;
		}
		return null;
	}

	private boolean isHydraOnly() {
		return cmd.hasOption(OPT_HYDRA_ONLY.getOpt());
	}

	String[] getKWGuards() {
		if (cmd.hasOption(OPT_KINGDOM_WAR.getOpt())) {
			String[] kwDecks = cmd.getOptionValues(OPT_KINGDOM_WAR.getOpt());
			return kwDecks == null ? new String[0] : kwDecks;
		}
		return null;
	}

	private Integer getLimit() {
		if (cmd.hasOption(OPT_LIMIT.getOpt())) {
			return parseInteger(OPT_LIMIT.getOpt());
		}
		return null;
	}

	private String getExtractOutput() {
		if (cmd.hasOption(OPT_EXTRACT_OUTPUT.getOpt())) {
			return cmd.getOptionValue(OPT_EXTRACT_OUTPUT.getOpt());
		}
		return null;
	}

	private Server getExtractRMServer() {
		if (cmd.hasOption(OPT_EXTRACT_RM.getOpt())) {
			return toServer(cmd.getOptionValue(OPT_EXTRACT_RM.getOpt()));
		}
		return null;
	}

	private Integer getExtractRMFrom() {
		if (cmd.hasOption(OPT_EXTRACT_RM_FROM.getOpt())) {
			return parseInteger(OPT_EXTRACT_RM_FROM.getOpt());
		}
		return null;
	}

	private Integer getExtractRMCount() {
		if (cmd.hasOption(OPT_EXTRACT_RM_COUNT.getOpt())) {
			return parseInteger(OPT_EXTRACT_RM_COUNT.getOpt());
		}
		return null;
	}

	private Server getExtractFoHServer() {
		if (cmd.hasOption(OPT_EXTRACT_FOH.getOpt())) {
			return toServer(cmd.getOptionValue(OPT_EXTRACT_FOH.getOpt()));
		}
		return null;
	}

	private boolean isUpdateLocalDbServer() {
		return cmd.hasOption(OPT_UPDATE_LOCAL_DB.getOpt());
	}

	private Long parseLong(String opt) {
		try {
			return (Long) cmd.getParsedOptionValue(opt);
		}
		catch (ParseException e) {
			throw new BootException(e.getMessage(), e);
		}
	}

	private Integer parseInteger(String opt) {
		return parseLong(opt).intValue();
	}

	TaskOptions preparedTask() {
		TaskOptions options = null;

		if (getDemon() != null) {
			options = Tasks
				.prepareDemonInvasionSim(
					getDeck(),
					getDemon())
				.showDamage(showDemonDamage())
				.maxDamage(maxDemonDamage());
		}
		else if (getEWDeck() != null) {
			options = Tasks
				.prepareElementalWarSim(getDeck(), getEWDeck());
		}
		else if (getWonderTowerDeck() != null) {
			options = Tasks
				.prepareWonderTowerSim(getDeck(), DeckDescFormat.parseFiles(getWonderTowerDeck()));
		}
		else if (getVersusFiles() != null) {
			options = Tasks
				.prepareVersusSim(
					getDeck(),
					DeckDescFormat.parseFiles(getVersusFiles()));
		}
		else if (isThief()) {
			options = Tasks.prepareThiefSim(getDeck());
		}
		else if (getHydras() != null) {
			options = Tasks
				.prepareHydraSim(
					getDeck(),
					Arrays.asList(getHydras()),
					isHydraOnly())
				.withIsolatedHydraDeck(getDeckAlt())
				.withDeckSwitchAtHp(deckSwitchAtHp());
		}
		else if (getKWGuards() != null) {
			options = Tasks
				.prepareKingdomWarSim(
					getDeck(),
					Arrays.asList(getKWGuards()))
				.limit(getLimit());
		}
		else if (getFoHServer() != null) {
			options = Tasks.prepareFieldOfHonorSim(getFoHServer());
		}
		else if (getExtractRMServer() != null) {
			options = Tasks
				.prepareArenaExtract(
					getExtractRMServer(),
					getExtractOutput())
				.from(getExtractRMFrom())
				.count(getExtractRMCount());
		}
		else if (getExtractFoHServer() != null) {
			options = Tasks
				.prepareFieldOfHonorExtract(
					getExtractFoHServer(),
					getExtractOutput());
		}
		else if (getInputFiles() != null) {
			options = Tasks
				.prepareConvertJsonDecks(
					getInputFiles(),
					getExtractOutput());
		}
		else if (isUpdateLocalDbServer()) {
			options = Tasks.prepareUpdateLocalDb();
		}

		if (options instanceof BaseTaskOptions && !isDebug() && showProgessBar()) {
			((BaseTaskOptions<?>) options)
				.progressListener(new ConsoleProgressListener());
		}

		if (options instanceof SimTaskOptions) {
			((SimTaskOptions<?>) options)
				.threads(getThreads())
				.iterations(getIters());
		}

		return options;
	}

	static void checkRequirement(CommandLine cmd, Option requiredOption, Option[] requiredBy) {
		for (Option opt : requiredBy) {
			if (cmd.hasOption(opt.getOpt()) && !cmd.hasOption(requiredOption.getOpt())) {
				throw new BootException("Option '" + opt.getOpt() + "' requires you to define option '" + requiredOption.getOpt() + "' too.");
			}
		}
	}

	static List<String> optsToString(Option... opts) {
		return Stream.of(opts)
			.map(Option::getOpt)
			.collect(Collectors.toList());
	}

	static void printHelp(String header) {
		String usage = "ek-battlesim [options] [> output.txt]";
		HelpFormatter formatter = new HelpFormatter();
		formatter.setWidth(WIDTH);
		formatter.printHelp(usage, header.isEmpty() ? header : NL + SEP + NL + header + NL + SEP, OPTIONS, "");
	}

}
