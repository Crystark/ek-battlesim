package crystark.ek.cli;

import crystark.common.format.FormatException;
import crystark.ek.Constants;
import crystark.ek.battlesim.AppUpdateChecker;
import crystark.ek.battlesim.task.TaskException;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Scanner;

public class Boot {
	private static final Logger L = LoggerFactory.getLogger(Boot.class);

	public static void main(String[] args) {
		System.out.println(Constants.TITLE);
		System.out.println();
		BootOptions bo;
		try {
			bo = new BootOptions(args);
			if (bo.isDebug()) {
				LogManager.getRootLogger().setLevel(Level.DEBUG);
				Constants.IS_DEBUG = true;
			}

			try {
				if (bo.isWait()) {
					System.out.println("Press enter to start the sim.");
					final Scanner scanner = new Scanner(System.in);
					scanner.hasNextLine();
//					System.console().readLine();
				}
				AppUpdateChecker.run();
				bo.preparedTask().run();
			}
			catch (TaskException | FormatException t) {
				System.out.println(t.getMessage());
				L.debug("", t);
				System.exit(1);
			}
			catch (Throwable t) {
				L.error("Something went wrong.", t);
				System.exit(1);
			}
		}
		catch (BootException t) {
			BootOptions.printHelp(t.getMessage());
			System.exit(1);
		}
	}
}
