package crystark.ek.cli;

public class BootException extends RuntimeException {
	public BootException(String message) {
		super(message);
	}

	public BootException(String message, Throwable cause) {
		super(message, cause);
	}
}
