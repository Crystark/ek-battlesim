package crystark.ek;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import crystark.common.api.Server;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.UUID;

public class LoaDataFetcher extends DataFetcher {
	private static final String   PASSWORD     = "EKb5p4ssw0rd";
	public static final  String[] HEADER_UNITY = new String[] { "X-Unity-Version", "4.6.1p4" };
	public static final  String[] HEADER_UA    = new String[] { "User-Agent", "Dalvik/1.6.0 (Linux; U; Android 4.4.2; Mali-T760 Build/KOT49H)" };

	private final String login;
	private final String password;

	private boolean loginAsGuest = true;
	private boolean loggedIn     = false;

	LoaDataFetcher(Server server, boolean useDefaultLogin) {
		super(server);

		//TODO Use guest login for LoA if possible
		useDefaultLogin = true;

		if (useDefaultLogin) {
			final Server.Credentials c = server.user();
			this.login = c.user;
			this.password = c.pass;
		}
		else {
			this.login = null;
			this.password = null;
		}
	}

	public LoaDataFetcher(Server server, String login, String password) {
		super(server);

		this.login = login;
		this.password = password;
	}

	@Override
	public void login() throws IOException {
		if (!loggedIn) {
			if (login != null || password != null) {
				userLogin(login, password);
			}
			else {
				guestLogin();
			}
			loggedIn = true;
		}
	}

	void userLogin(String user, String password) throws IOException {
		if (loginAsGuest) {
			loggedIn = false;
			loginAsGuest = false;
		}
		login(new LoginData().login(user, password));
	}

	void guestLogin() throws IOException {
		login(new LoginData().quickStart());
	}

	// pp/start.do
	// httpService.do
	// login.php
	void login(LoginData ld) throws IOException {
		LoginResponse.ReturnObjs lr = postLoginData(ld);
		baseUrl = lr.GS_IP;
		RequestBody form = new FormEncodingBuilder()
			.add("Udid", ld.callPara.udid)
			.add("time", String.valueOf(lr.timestamp))
			.add("UserName", lr.userName)
			.add("Origin", "ANDROID")
			.add("key", lr.key)
			.add("Password", lr.U_ID)
			.add("Devicetoken", "")
			.build();

		post(baseUrl + "/login.php?do=PassportLogin", form);
	}

	LoginResponse.ReturnObjs postLoginData(LoginData ld) throws IOException {
		get("http://pp.fantasytoyou.com/pp/start.do?udid=" + ld.callPara.udid + "&idfa=&locale=" + ld.callPara.locale
			+ "&gameName=" + ld.callPara.gameName + "&client=" + ld.callPara.clientType);

		String dataInfo = GSON.toJson(ld.callPara);
		String encodedDataInfo = Base64.getEncoder().encodeToString(dataInfo.getBytes(StandardCharsets.UTF_8));
		String postData = "{\"serviceName\":\"checkUserActivedBase64Json\",\"callPara\":\"" +
			encodedDataInfo + "\"}";
		Response r = post(
			"http://pp.fantasytoyou.com/pp/httpService.do",
			RequestBody.create(
				MediaType.parse("application/x-www-form-urlencoded; charset=UTF-8"),
				postData));
		final String s = r.body().string();
		LoginResponse lr = GSON.fromJson(s, LoginResponse.class);
		if (lr.returnCode != 0) {
			throw new DataFetcherException("Failed to login: " + lr.returnMsg, r);
		}
		return lr.returnObjs;
	}

	public String getMapsData() throws IOException {
		login();
		Response r = get(baseUrl + "/mapstage.php?do=GetMapStageALL&stageNum=40&platformtype=1&phpl=EN");

		return r.body().string();
	}

	public static class LoginData {
		public CallPara callPara = new CallPara();
		public String serviceName;

		public LoginData login(String user, String pass) {
			this.serviceName = "checkUserActivedBase64Json";
			this.callPara.userName = user;
			this.callPara.userPassword = pass;

			return this;
		}

		public LoginData quickStart() {
			this.serviceName = "startGameJson";

			return this;
		}

		/*{
		  "userName": "LoGiN",
		  "userPassword": "pwd",
		  "gameName": "CARD-ANDROID-EN",
		  "udid": "C7:57:11:B3:13:57",
		  "idfa": "",
		  "clientType": "flash",
		  "releaseChannel": "",
		  "locale": "en"
		}*/
		public class CallPara {
			public String gameName       = "CARD-ANDROID-EN";
			public String clientType     = "flash";
			public String releaseChannel = "";
			public String locale         = "en";
			public String udid           = UUID.randomUUID().toString().replace("-","").toLowerCase();//"01:23:45:67:89:0a";

			public String userPassword;
			public String userName;
		}

	}

	public class LoginResponse {
		public int        returnCode;
		public String     returnMsg;
		public ReturnObjs returnObjs;

		/*
	"isSetNick": "0",
    "invite": true,
    "gscode": false,
    "minor": false,
    "PlayingTipsUrl": "",
    "cdnurl": "http:\/\/cache.ifreecdn.com\/mkhx\/",
    "encrypt": 0,
    "ip": "ea9.liesofastaroth.com",
    "ipport": 8000
		*/
		/*
		{
  "returnCode": "0",
  "returnMsg": "No error.",
  "returnObjs": {
    "GS_CHAT_PORT": "8000",
    "GS_ID": "1541",
    "GS_CHAT_IP": "38.121.63.130",
    "GS_NAME": "EN-ANDROID-09",
    "initialUName": "login",
    "GS_PORT": "80",
    "friendCode": "null",
    "source": "login",
    "userName": "login",
    "U_ID": "1010557199",
    "uEmailState": "0",
    "LOGIN_TYPE": "[]",
    "GS_DESC": "Daenerys",
    "GS_IP": "http://ea9.liesofastaroth.com/",
    "G_TYPE": "1",
    "key": "e0342abc4b402b093ec7c46e9e368a40",
    "timestamp": "1515748163921"
  }
}
		*/
		public class ReturnObjs {
			public int    GS_CHAT_PORT;
			public String GS_ID;
			public String GS_CHAT_IP;
			public String GS_NAME;
			public String initialUName;
			public int    GS_PORT;
			public String friendCode;
			public String source;
			public String userName;
			public String U_ID;
			public String uEmailState;
			public String LOGIN_TYPE;
			public String GS_DESC;
			public String GS_IP;
			public String G_TYPE;
			public String key;
			public long   timestamp;
		}

	}
}
