package crystark.ek;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import crystark.common.api.Server;
import crystark.ek.EkDataFetcher.MPassport.Data.UserInfo;
import crystark.ek.battlesim.task.TaskException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.Map;

public class EkDataFetcher extends DataFetcher {
	private static final String	PASSWORD		= "EKb5p4ssw0rd";
	private static final String	QUERY			= "gameid=51";		//&deviceid=f0aa8d37a68dc09d331cdc39b1e216cb&platform=android&sdkvcode=2.2.2&androidos=19";

	private final String		email;
	private final String		password;

	private Response			loginPage;
	private boolean				loginAsGuest	= true;
	private boolean				loggedIn		= false;

	EkDataFetcher(Server server, boolean useDefaultLogin) {
		super(server);

		if (useDefaultLogin) {
			Server.Credentials c = server.user();
			this.email = defaultEmail(c.user);
			this.password = c.pass;
		}
		else {
			this.email = null;
			this.password = null;
		}
	}

	@Override
	public void login() throws IOException {
		if (!loggedIn) {
			if (email != null || password != null) {
				userLogin(email, password);
			}
			else {
				guestLogin();
			}
			loggedIn = true;
		}
	}

	void userLogin(String email, String password) throws IOException {
		if (loginAsGuest) {
			loggedIn = false;
			loginAsGuest = false;
		}
		login(getUserConnectionData(email, password));
	}

	void guestLogin() throws IOException {
		login(getGuestConnectionData());
	}

	void login(ConnectionData cd) throws IOException {
		UserInfo ui = getUserInfo(cd);

		RequestBody formBody = new FormEncodingBuilder()
			.add("plat", "pwe")
			.add("uin", cd.uin)
			.add("nickName", cd.uin)
			.add("Devicetoken", cd.Devicetoken)
			.add("userType", "2")

			.add("MUid", ui.MUid)
			.add("ppsign", ui.ppsign)
			.add("sign", ui.sign)
			.add("nick", ui.nick)
			.add("time", ui.time.toString())
			.add("Udid", "00-C0-7B-EF-00-F5")
			.add("Origin", "IOS_PW")
			.build();

		post(baseUrl + "/login.php?do=mpLogin", formBody);
	}

	UserInfo getUserInfo(ConnectionData cd) throws IOException {
		RequestBody formBody = new FormEncodingBuilder()
			.add("plat", "pwe")
			.add("uin", cd.uin)
			.add("nickName", cd.uin)
			.add("Devicetoken", cd.Devicetoken)
			.add("userType", "2")
			.build();

		Response r = post("http://master.ek.ifreeteam.com/mpassport.php?do=plogin", formBody);
		return GSON.fromJson(r.body().string(), MPassport.class).data.uinfo;
	}

	ConnectionData getUserConnectionData(String email, String password) throws IOException {
		Response loginPage = getLoginPage();
		FormEncodingBuilder b = new FormEncodingBuilder();

		Elements inputs = Jsoup.parse(loginPage.body().string()).select("#arc-signin-form input");
		for (Element input : inputs) {
			String inputName = input.attr("name");
			switch (input.attr("type")) {
				case "email":
					b.add(inputName, email);
					break;
				case "password":
					b.add(inputName, password);
					break;
				default:
					b.add(inputName, "");
					break;
			}
		}

		Response response = post("http://mobile.arcgames.com/user/login/?" + QUERY, b.build());
		Map<?, ?> data = GSON.fromJson(response.body().string(), Map.class);

		if (!Boolean.TRUE.equals(data.get("result")))
			throw new TaskException("Failed to login. " + data.get("msg"));

		String[] cnxdata = data.get("loginstatus").toString().split(":");

		ConnectionData cd = new ConnectionData();
		cd.uin = cnxdata[1];
		cd.Devicetoken = cnxdata[3];

		return cd;
	}

	ConnectionData getGuestConnectionData() throws IOException {
		Response response = get("http://mobile.arcgames.com/user/playasguest?" + QUERY);
		String[] data = GSON.fromJson(response.body().string(), Map.class).get("loginstatus").toString().split(":");

		ConnectionData cd = new ConnectionData();
		cd.uin = data[1];
		cd.Devicetoken = data[3];

		return cd;
	}

	Response getLoginPage() throws IOException {
		if (loginPage == null) {
			loginPage = get("http://mobile.arcgames.com/user/login?" + QUERY);
		}
		return loginPage;
	}

	private static String defaultEmail(String user) {
		return "ekbs-" + user + "@gmail.com";
	}

	static class ConnectionData {
		String	uin;
		String	Devicetoken;
	}

	static class MPassport {
		Data data;

		static class Data {
			UserInfo uinfo;

			static class UserInfo {
				String	uin;
				String	nick;
				String	MUid;
				Long	time;
				String	sign;
				String	ppsign;
			}
		}
	}
}
