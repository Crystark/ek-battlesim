#DeckId 10
#Player info
Player name: Giant Mud Larva (Legendary)
Player level: 120

#My cards
LG:Giant Mud Larva (Legendary), 15
LG:Orc Teacher, 15, Resistance
LG:Moor Ripper, 15, Resistance
LG:Behemoth, 15, Toxic Clouds 10
LG:Abhorrent Gargoyle, 15, Evasion
LG:Serpent Shamanka, 15, Toxic Clouds 10
LG:Everglade Mermaid, 15, Resistance
LG:Empyrean Priestess, 15, Resistance
LG:Armored Sumatran, 15, Evasion
LG:Lady Venom, 15, Blizzard 10

#My runes
