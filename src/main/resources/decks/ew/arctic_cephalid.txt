#DeckId 1
#Player info
Player name: Arctic Cephalid (Legendary)
Player level: 120

#My cards
LG:Arctic Cephalid (Legendary), 15
LG:Ice Knight, 15, Resistance
LG:Ice Knight, 15, Resistance
LG:Sea Piercer, 15, Blizzard 8
LG:Sea Piercer, 15, Blizzard 8
LG:Steel Dragon, 15, Resistance
LG:Steel Dragon, 15, Resistance
LG:Tundra Rider, 15, Resistance
LG:Tundra Rider, 15, Resistance
LG:Crystal Emperor, 15, Resistance

#My runes
