#DeckId 3
#Player info
Player name: Zombie Dragon (Legendary)
Player level: 120

#My cards
LG:Zombie Dragon (Legendary), 15
LG:Behemoth, 15, Retaliation 8
LG:Marsh Devourer, 15, Resistance
LG:Moor Ripper, 15, Resistance
LG:Abhorrent Gargoyle, 15, Toxic Clouds 8
LG:Giant Mud Larva, 15, Resistance
LG:Evil Empress, 15, Resistance
LG:Serpent Shamanka, 15, Mana Corruption 10
LG:Typhon, 15, Resistance
LG:Giant Crustacean, 15, Resistance

#My runes
