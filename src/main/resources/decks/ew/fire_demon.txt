#DeckId 1
#Player info
Player name: Fire Demon (Legendary)
Player level: 120

#My cards
LG:Fire Demon (Legendary), 15
LG:Dire Snapping Turtle, 15, Resistance
LG:Dire Snapping Turtle, 15, Resistance
LG:Azula, 15, Resistance
LG:Azula, 15, Resistance
LG:Fire Berserker, 15, Resistance
LG:Fire Berserker, 15, Resistance
LG:Winged Exile, 15, Resistance
LG:Winged Exile, 15, Resistance
LG:Queen of the Dead, 15, Trap 2

#My runes
