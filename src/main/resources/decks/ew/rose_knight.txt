#DeckId 10
#Player info
Player name: Rose Knight (Legendary)
Player level: 120

#My cards
LG:Rose Knight (Legendary), 15
LG:Moss Dragon, 15, Electric Shock 10
LG:Winged Tiger, 15, Resistance
LG:Rose Knight, 15, Resistance
LG:Prince of the Forest, 15, Resistance
LG:Exiled Angel, 15, Resistance
LG:Shadow Assassin, 15, Resistance
LG:Steel Dragon, 15, Resistance
LG:Jack Frost, 15, Resistance
LG:Winter Samurai, 15, Resistance

#My runes
