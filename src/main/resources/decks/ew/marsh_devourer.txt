#DeckId 5
#Player info
Player name: Marsh Devourer (Legendary)
Player level: 120

#My cards
LG:Marsh Devourer (Legendary), 15
LG:Marsh Devourer, 15, Resistance
LG:Moor Ripper, 15, Resistance
LG:Abhorrent Gargoyle, 15, Toxic Clouds 8
LG:Giant Mud Larva, 15, Resistance
LG:Arctic Cephalid, 15, Resistance
LG:Crystal Emperor, 15, Resistance
LG:Ice Knight, 15, Resistance
LG:Marsh Devourer, 15, Resistance
LG:Marsh Devourer, 15, Resistance

#My runes
