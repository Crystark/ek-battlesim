#DeckId 5
#Player info
Player name: Venus Soultrap (Legendary)
Player level: 120

#My cards
LG:Venus Soultrap (Legendary), 15
LG:Giant Crustacean, 15, Resistance
LG:Orc Teacher, 15, Resistance
LG:Typhon, 15, Resistance
LG:Giant Mud Larva, 15, Resistance
LG:Exiled Angel, 15, Resistance
LG:Ice Sprite, 15, Resistance
LG:Empyrean Priestess, 15, Resistance
LG:Tundra Rider, 15, Resistance
LG:Crystal Emperor, 15, Resistance

#My runes
