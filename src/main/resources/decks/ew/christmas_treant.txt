#DeckId 10
#Player info
Player name: Christmas Treant (Legendary)
Player level: 120

#My cards
LG:Christmas Treant (Legendary), 15
LG:Aranyani, 15, Resistance
LG:Night Elf Ranger, 15, Resistance
LG:Winged Tiger, 15, Resistance
LG:Empyrean Priestess, 15, Immunity
LG:Warrior of the Forest, 15, Resistance
LG:Taiga General, 15, Resistance
LG:Steel Dragon, 15, Resistance
LG:Typhon, 15, Resistance
LG:Armored Turkey, 15, Resistance

#My runes
