# Deck options
--always-start
--infinite-hp

# Player info
Player name: Leg. Thief lvl 100 (41563)

# Total HP: 41563
# HP   = floor(HP * 3.19)
# ATK *= 4.9

# Level 10
Behemoth,              14, 6, 2646,  5614,      swamp,  Electric Shock:175,    Retaliation:160,       Immunity
Ice Dragon,            15, 6, 3087,  5391,      tundra, Dodge:45,              Clean Sweep,           Immunity
Fire Demon,            14, 6, 3283,  5454,      mtn,    Concentration:120,     Laceration,            Immunity
Water Elemental,       12, 4, 2082,  3796,      forest, Iceball:80,            Regeneration:150,      Blizzard:120
Water Elemental,       12, 4, 2082,  3796,      forest, Iceball:80,            Regeneration:150,      Blizzard:120
Kitsune,               12, 4, 2254,  4338,      forest, Fireball:100,          Swamp Purity:120,      Firestorm:175
# Level 9
Demonic Imp,           13, 4, 2371,  3141,      mtn,    Resistance,            Concentration:100
Demonic Imp,           13, 4, 2371,  3141,      mtn,    Resistance,            Concentration:100
# Level 8
Swamp Golem,           11, 6, 2141,  4006,      swamp,  Counterattack:240,     Arctic Pollution:120
# Level 5
Polar Bearborn,        12, 4, 2009,  2886,      tundra, Concentration:80,      Backstab:200