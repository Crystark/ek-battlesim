# Player info
Player name: Dire Snapping Turtle 55
Player level: 55

# Level 10
Fire Demon
Demonic Imp
Demonic Imp
Draconian Shaman
Dire Snapping Turtle
Dire Snapping Turtle
Taiga Cleric

# Level 8
Dire Snapping Turtle,  15, 4, 573,  1620,      mtn,    Clean Sweep,           Seal
Draconian Shaman,      12, 4, 427,  876,       mtn,    Sacrifice:60,          Curse:320
Fire Demon,            14, 6, 606,  1638,      mtn,    Concentration:120,     Laceration
 
Inferno
Explosion
Fire Forge
Blood Stone