# EK Battlesim - v0.6.12-SNAPSHOT - Generated deck file on 2015-10-14 13:37:40
deck name: Hydra V (Guard)
player name: Hydra V (Guard)

# Cards
Prince of the Forest, 17, 4, 1240, 7650, forest, Jungle Barrier:55, Forest Force:200, Exile
Dire Snapping Turtle, 15, 4, 1250, 8500, mtn, Clean Sweep, Seal, Bloodsucker:80
Fire Demon, 14, 6, 1340, 8550, mtn, Concentration:120, Laceration, Immunity
Fire Demon, 14, 6, 1340, 8550, mtn, Concentration:120, Laceration, Immunity
Arbiter of War, 16, 6, 1160, 7750, forest, Damnation:140, Magic Shield:80, Sacred Flame
Moss Dragon, 15, 6, 1310, 8550, forest, Concentration:120, Parry:160, Immunity
Moss Dragon, 15, 6, 1310, 8550, forest, Concentration:120, Parry:160, Immunity
Moss Dragon, 15, 6, 1310, 8550, forest, Concentration:120, Parry:160, Immunity
Moss Dragon, 15, 6, 1310, 8550, forest, Concentration:120, Parry:160, Immunity
Hydra V (Guard), 99, 4, 2000, 250000, hydra, Immunity, Sacred Flame, Infiltrator, Origins Guard:320

# Runes
Thunder Shield:200
Lightning:250
Nimble Soul:65
Lore:150
