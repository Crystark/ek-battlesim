# EK Battlesim - v0.6.12-SNAPSHOT - Generated deck file on 2015-10-14 13:37:41
deck name: Hydra V (Retaliate)
player name: Hydra V (Retaliate)

# Cards
Moss Dragon, 15, 6, 1310, 8550, forest, Concentration:120, Parry:160, Immunity
Moss Dragon, 15, 6, 1310, 8550, forest, Concentration:120, Parry:160, Immunity
Moss Dragon, 15, 6, 1310, 8550, forest, Concentration:120, Parry:160, Immunity
Moss Dragon, 15, 6, 1310, 8550, forest, Concentration:120, Parry:160, Immunity
Behemoth, 14, 6, 1080, 8800, swamp, Electric Shock:175, Retaliation:160, Immunity
Behemoth, 14, 6, 1080, 8800, swamp, Electric Shock:175, Retaliation:160, Immunity
Behemoth, 14, 6, 1080, 8800, swamp, Electric Shock:175, Retaliation:160, Immunity
Giant Mud Larva, 16, 4, 1320, 8250, swamp, Regeneration:175, QS_Destroy, Swamp Guard:300
Soul Thief, 14, 3, 1010, 5250, swamp, Reflection:150, Parry:120, Wicked Leech:21
Hydra V (Retaliate), 99, 4, 2000, 250000, hydra, Immunity, Sacred Flame, Infiltrator, Retaliation:200

# Runes
Thunder Shield:200
Red Valley:90
Death Zone:200
Stonewall:180
