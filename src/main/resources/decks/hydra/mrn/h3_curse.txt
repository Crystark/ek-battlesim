# EK Battlesim - v0.6.12-SNAPSHOT - Generated deck file on 2015-10-14 13:37:40
deck name: Hydra III (Curse)
player name: Hydra III (Curse)

# Cards
Taiga General, 15, 4, 726, 5250, tundra, Magic Shield:80, Exile, Regeneration:175
Taiga General, 15, 4, 726, 5250, tundra, Magic Shield:80, Exile, Regeneration:175
Lava Destroyer, 20, 1, 738, 4440, mtn, Guard, Resurrection:60, D_Reanimation
Lava Destroyer, 20, 1, 738, 4440, mtn, Guard, Resurrection:60, D_Reanimation
Draconian Elder, 14, 6, 612, 3300, mtn, Curse:240, Trap:2, QS_Teleportation
Dharmanian, 15, 4, 816, 5160, tundra, Prayer:320, Northern Force:175, QS_Teleportation
Fire Berserker, 17, 4, 804, 5265, mtn, Concentration:120, Fire God:160, Evasion
Fire Demon, 14, 6, 804, 5130, mtn, Concentration:120, Laceration, Immunity
Terra Cotta Warrior, 14, 6, 732, 3930, tundra, Resistance, Mountain Glacier:105, Reflection:150
Hydra III (Curse), 99, 4, 1200, 90000, hydra, Immunity, Dual Snipe:250, Bloodthirsty:80, Curse:400

# Runes
Burning Soul:200
Charred:140
Arctic Freeze:100
Clear Spring:225
