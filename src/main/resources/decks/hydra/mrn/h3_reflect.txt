# EK Battlesim - v0.6.12-SNAPSHOT - Generated deck file on 2015-10-14 13:37:40
deck name: Hydra III (Reflect)
player name: Hydra III (Reflect)

# Cards
Fire Demon, 14, 6, 804, 5130, mtn, Concentration:120, Laceration, Immunity
Moss Dragon, 15, 6, 786, 5130, forest, Concentration:120, Parry:160, Immunity
Arctic Cephalid, 16, 4, 876, 4500, tundra, Reflection:210, Dodge:60, Bloodthirsty:70
Dragon Knight, 18, 6, 804, 4830, mtn, Dodge:50, Reincarnation:1, Resistance
Vampire Bat Demon, 13, 4, 648, 3000, swamp, Warpath:90, Resurrection:55, Bloodsucker:60
Lord of the Mire, 12, 4, 624, 3840, swamp, Smog:120, Parry:120, Blitz:75
Demonic Imp, 13, 4, 612, 3000, mtn, Resistance, Concentration:100, Resurrection:60
Cerato, 13, 6, 594, 3660, swamp, Puncture:75, Craze:50, Ice Shield:120
Taurus Guard, 12, 4, 666, 3150, swamp, Concentration:100, Parry:140, Swamp Guard:250
Hydra III (Reflect), 99, 4, 1200, 90000, hydra, Immunity, Dual Snipe:250, Bloodthirsty:80, Reflection:240

# Runes
Stonewall:180
Stone Forest:270
Death Zone:200
Flying Stone:270
