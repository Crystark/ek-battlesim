# EK Battlesim - v0.6.12-SNAPSHOT - Generated deck file on 2015-10-14 13:37:40
deck name: Hydra IV (Teleport)
player name: Hydra IV (Teleport)

# Cards
Dire Snapping Turtle, 15, 4, 937, 6800, mtn, Clean Sweep, Seal, Bloodsucker:80
Fire Demon, 14, 6, 1005, 6840, mtn, Concentration:120, Laceration, Immunity
Dragon Knight, 18, 6, 1005, 6440, mtn, Dodge:50, Reincarnation:1, Resistance
Armored Sumatran, 16, 4, 810, 7320, forest, Resistance, Craze:80, Retaliation:120
Thunder Dragon, 17, 3, 945, 6080, forest, Electric Shock:150, Mana Corruption:160, Resurrection:65
Night Elf Ranger, 15, 4, 937, 6960, forest, Snipe:210, Mania:160:160, Infiltrator
Demonic Imp, 13, 4, 765, 4000, mtn, Resistance, Concentration:100, Resurrection:60
Red Banded Dragon, 16, 4, 975, 6280, mtn, Bloodsucker:60, Concentration:120, Reflection:180
Taiga Archon, 16, 4, 840, 7240, tundra, Arctic Guard:400, Glacial Barrier:55, Resistance
Hydra IV (Teleport), 99, 4, 1500, 160000, hydra, Immunity, Sacred Flame, Infiltrator, QS_Teleportation

# Runes
Charred:140
Burning Soul:200
Lore:150
Thunder Shield:200
