# EK Battlesim - v0.6.12-SNAPSHOT - Generated deck file on 2015-10-14 13:37:40
deck name: Hydra I
player name: Hydra I

# Cards
Thorn Queen, 11, 4, 400, 885, swamp, Smog:40, Arctic Pollution:120, Trap:2
Thorn Queen, 11, 4, 400, 885, swamp, Smog:40, Arctic Pollution:120, Trap:2
Boreal Wolf, 10, 4, 430, 1030, tundra, QS_Trap:1, Parry:100, Combustion:175
Ash Sprite, 11, 4, 400, 800, mtn, D_Curse:200, QS_Blizzard:80, Resurrection:55
Butterfly Fairy, 11, 4, 415, 870, forest, Forest Guard:200, Forest Guard:250, Forest Guard:300
Mermaid Princess, 14, 4, 430, 1030, tundra, Blizzard:100, Confusion:65, Dodge:45
Red Lotus Sprite, 9, 4, 410, 960, mtn, Dodge:30, Forest Fire:105, Trap:1
Draconian Elder, 14, 6, 510, 1100, mtn, Curse:240, Trap:2, QS_Teleportation
Orc Shaman, 9, 4, 425, 780, swamp, QS_Curse:240, Fireball:125, Regeneration:150
Hydra I, 99, 4, 1000, 10000, hydra, Resistance, Dual Snipe:250, Bloodthirsty:80

# Runes
Thunderbolt:175
Icicle:140
Fire Flow:175
