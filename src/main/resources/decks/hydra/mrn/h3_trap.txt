# EK Battlesim - v0.6.12-SNAPSHOT - Generated deck file on 2015-10-14 13:37:40
deck name: Hydra III (Trap)
player name: Hydra III (Trap)

# Cards
Azula, 15, 4, 732, 4500, mtn, Curse:240, Seal, Resurrection:65
Dire Snapping Turtle, 15, 4, 750, 5100, mtn, Clean Sweep, Seal, Bloodsucker:80
Thunder Dragon, 17, 3, 756, 4560, forest, Electric Shock:150, Mana Corruption:160, Resurrection:65
Armored Sumatran, 16, 4, 648, 5490, forest, Resistance, Craze:80, Retaliation:120
Terra Cotta Warrior, 14, 6, 732, 3930, tundra, Resistance, Mountain Glacier:105, Reflection:150
Demonic Imp, 13, 4, 612, 3000, mtn, Resistance, Concentration:100, Resurrection:60
Lord of the Mire, 12, 4, 624, 3840, swamp, Smog:120, Parry:120, Blitz:75
Arctic Drake, 13, 4, 648, 3660, tundra, Concentration:80, Resistance, Arctic Guard:300
Water Elemental, 12, 4, 510, 3570, forest, Iceball:80, Regeneration:150, Blizzard:120
Hydra III (Trap), 99, 4, 1200, 90000, hydra, Immunity, Dual Snipe:250, Bloodthirsty:80, Trap:3

# Runes
Avalanche:200
Explosion:320
Quicksand:45
Transparency:90
