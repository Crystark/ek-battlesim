# EK Battlesim - v0.6.12-SNAPSHOT - Generated deck file on 2015-10-14 13:37:40
deck name: Hydra II (Reflect)
player name: Hydra II (Reflect)

# Cards
Arctic Drake, 13, 4, 540, 2440, tundra, Concentration:80, Resistance, Arctic Guard:300
Cerato, 13, 6, 495, 2440, swamp, Puncture:75, Craze:50, Ice Shield:120
Polar Bearborn, 12, 4, 560, 1990, tundra, Concentration:80, Backstab:200, Dodge:60
Snow Crusader, 13, 4, 460, 2220, tundra, Bloodthirsty:50, Reflection:150, Blitz:90
Fire Gorilla, 17, 4, 635, 3120, mtn, Magic Shield:80, Warpath:105, Mountain Force:200
Headless Horseman, 9, 2, 455, 1340, mtn, Dodge:35, Backstab:120, Concentration:140
Undead Felldrake, 13, 6, 480, 2860, swamp, Concentration:100, Craze:60, Rejuvenation:210
Alanine, 13, 4, 460, 2040, forest, Concentration:100, Swamp Purity:105, Dodge:45
Gas Spore, 9, 4, 485, 1640, swamp, QS_Group Weaken:30, Bloodthirsty:40, Concentration:140
Hydra II (Reflect), 99, 4, 1000, 40000, hydra, Resistance, Sacred Flame, Bloodthirsty:80, Reflection:150

# Runes
Stone Forest:240
Thunderbolt:200
Icicle:160
Fire Flow:200
