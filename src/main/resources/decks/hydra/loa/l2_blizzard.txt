### Lilith Normal - 62530HP
deck name: Lilith II (Blizzard)
player name: Lilith II (Blizzard)

Spring Breeze
ThunderShield
Ray Prison
Raised Flag		

Normal Lilith, 15, Blizzard:100
Unicorn King,			14,	4,	490,	1220,	Forest,			Vulnerability 6,			Lightning Chain 7,		Rejuvenation:240
Red Dragon,				14,	6,	545,	1510,	Mtn,			Fire God 3,				Rejuvenation:180,		Reflection:150
Elf Wizard,				 8,	2,	305,	900,	Forest,			Fireball 3,				Regeneration:100,		Firewall 6
Unicorn,				10,	4,	320,	1055,	Forest,			Thunderbolt 2,			QS_Electric Shock 5,	Dodge 6		
Bone Dragon,			11,	6,	590,	1250,	Mtn,			Nova frost 5,			Pollution 8,			Hells Power 5
Lampade,				11,	4,	410,	1360,	Forest,			Fireball 4,				Vulnerability:120,		Firestorm 7
Phoenix,				15,	4,	660,	1560,	Forest,			Firestorm 8,			Rejuvenation:210,		Resurrection 8		
Unicorn King,			14,	4,	490,	1220,	Forest,			Vulnerability 6,			Lightning Chain 7,		Rejuvenation:240
Naiad,					12,	4,	425,	1190,	Forest,			Iceball:80,				Regeneration:150,		Blizzard:120	

