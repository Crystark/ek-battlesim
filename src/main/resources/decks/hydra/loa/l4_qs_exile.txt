### Lilith Nightmare - 212200HP
deck name: Lilith IV (QS Exile)
player name: Lilith IV (QS Exile)


ThunderShield
Lore
Burning Soul
Fire Offering


Nightmare Lilith, 15, QS_Exile
Durahan,				13,	4,	612,	3000,	Mtn,			Resistance,				Concentration:100,		Resurrection:60	
Ghost eye,				14,	4,	885,	4280,	Mtn,			Backstab 4,				Revenge 5,				Warpath 6
Lich Lord,				16,	4,	1005,	6520,	Mtn,			Resistance,				Reflection:8,			Warpath:90	
Leviathan,				14,	6,	1035,	2385,	Mtn,			Concentration 6,		Laceration,				Immunity	
Luna Ballista,			14,	4,	675,	4560,	Forest,			Vulnerability 7,			Reflection 3,			Dual Snipe 7
magical Archer,			14,	4,	660,	5480,	Tundra,      	Evasion,				Dual Snipe 7,			Magic erosion 5
Gaia,					16,	4,	968,	6480,	Forest,			Holy Protection 7,	Dodge 6,				Trap 2,					Resistance
Soul reaper,			15,	4,	938,	6800,	Mtn,			Clean Sweep,			Seal,					Bloodsucker 8,			Resurrection 5
Senka,					17,	2,	780,	4560,	Mtn,			Backstab 8,				Snipe 10,				Resurrection 8,			Seal

