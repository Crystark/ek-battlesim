### Lilith Hard - 127755HP
deck name: Lilith III (Reflection)
player name: Lilith III (Reflection)


Stone Forest
Stonewall
Death zone
Flying Stone


Hard Lilith, 15, Reflection:240
Durahan,				14,	4,	640,	1075,	Mtn,			Resistance,				Concentration 5,		Resurrection:60,			Warpath:75	
Seismic Turtle,			14,	6,	620,	1460,	swamp,  		Resistance,				Assassination:120,	Clean Sweep
Leviathan,				14,	6,	690,	1590,	Mtn,			Concentration:120,		Laceration,				Immunity		
Chief Minotaur,			12,	4,	555,	1050,	swamp,  		Concentration:100,		Parry 7,				Wilderness Guard:250
Lich Lord,				16,	4,	670,	1630,	Mtn,			Resistance,				Reflection 8,			Warpath:90		
Mammoth,				12,	6,	495,	1220,	swamp,  		Puncture:75,			Craze:50,				Ice Shield:120		
BearWarrior,			16,	4,	665,	1200,	swamp,  		Craze:50,				Warpath:120,			Bloodsucker:80		
Fafnir,					14,	6,	655,	1710,	Forest,			Concentration:120,		Parry:160,				Immunity		
Ares,					18,	4,	950,	1650,	Tundra,      	Reflection:210,			Dodge:60,				Bloodthirsty:70,			Resistance	

