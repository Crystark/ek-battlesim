### Lilith Normal - 61830HP
deck name: Lilith II (Group weakening)
player name: Lilith II (Group weakening)

Stone Forest
ThunderShield
Lore
Spring Breeze



Normal Lilith, 15,	Group weakening:40
Durahan,				14,	4,	510,	2000,	Mtn,			Resistance,				Concentration 5,		Resurrection:60,		Warpath:75	
Minotaur,				 8,	6,	370,	2100,	swamp,  		Concentration 2,		QS_Group weakening 6,		Wilderness Guard 4
Zen Master,				14,	4,	370,	2740,	Tundra,			Resistance,				Reflection:120,			Regeneration:125
Turtle Cub,				12, 4,	445,	2520,	Swamp,			Retaliation 5,			Exorcism 4,			Rejuvenation 6
Turtle Cub,				12, 4,	445,	2520,	Swamp,			Retaliation 5,			Exorcism 4,			Rejuvenation 6
Pleiade,				13, 4,  400,	2520,	Forest,			Thunderbolt 4,			Resurrection 6,			Dodge 8
Time Traveler,			11,	4,	340,	1320,	Tundra,      	Dodge 3,				QS_Exile,				Group weakening 8
Demon King,				12,	4,	405,	1450,	Mtn,			Combustion 3,			Laceration,				Group weakening 8
Mummy,					10,	4,	480,	895,	Mtn,			Pollution 3,			Plague 5,				Hell Guard 4

