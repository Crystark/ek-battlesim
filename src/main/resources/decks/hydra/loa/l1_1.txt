### Lilith Easy - 18520HP
deck name: Lilith I (1)
player name: Lilith I (1)

Stone Forest
ThunderShield
Lore
Spring Breeze
	

Easy Lilith
Black widow,			10,	4,	400,	885,	swamp,  		Smog 2,					Assassination 7,				Trap 2
Black widow,			10,	4,	400,	885,	swamp,  		Smog 2,					Assassination 7,				Trap 2
Chariot,				10,	4,	430,	1030,	Tundra,      	QS_Trap 1,				Parry 5,						Combustion 7
Mermaid,				13,	4,	425,	1060,	swamp,  		Blizzard 5,				Confusion 7,					Dodge 5
Dream Cleric,			11,	4,	415,	1020,	Forest,			Healing 5,				Regeneration 4,					Confusion 6
Succubus,				 9,	4,	410,	960,	Mtn,			Dodge 2,				Pollution 6,					Trap 1
Medusa,					12,	6,	510,	1100,	Mtn,			Curse 6,				Trap 2,							QS_Teleportation
Spectre orca,			11,	4,	400,	800,	Mtn,			D_Curse 5,				QS_Blizzard 4,					Resurrection 5
Ogre Warlock,			 9,	4,	425,	780,	swamp,  		QS_Curse 6,				Fireball 5,						Regeneration 6
