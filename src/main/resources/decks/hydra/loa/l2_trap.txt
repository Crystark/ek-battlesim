### Lilith Normal - 61130HP
deck name: Lilith II (Trap)
player name: Lilith II (Trap)

Flood
Explosion
Arsenopyrite
Meteor

Normal Lilith,15, Trap 2
Winged Princess,		12,	4,	435,	1260,	Tundra,			Confusion:65,			Kingdom Guard:300,		Reflection:180,
Viking Hunter,			13,	4,	485,	1090,	swamp,  		Backstab:200,			Wilderness Shield:40,		Trap 2
Naiad,					12,	4,	425,	1190,	Forest,			Iceball:80,				Regeneration:150,		Blizzard:120		
Saint hindin,			11,	4,	475,	1040,	Forest,			Healing:100,			Forest Guard:250,		Trap 2		
Black Widow,			10,	4,	400,	885,	swamp,  		Smog:40,				Assassination:120,	Trap 2
Succubus,				 9,	4,	410,	960,	Mtn,			Dodge:30,				Pollution:105,		Trap 1
Dryad,					12,	6,	455,	2680,	Forest,			Trap 1,					Parry:160,				Rejuvenation:210		
Forest Cupid,			14,	6,	450,	1400,	Forest,			Mania:160:160,			confusion:55,			Forests Power:125
Titan,					14,	6,	660,	1400,	Tundra,      	Resistance,				Halo:105,	Reflection:180

