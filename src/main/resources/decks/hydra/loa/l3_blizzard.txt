### Lilith Hard - 130110HP
deck name: Lilith III (Blizzard)
player name: Lilith III (Blizzard)

Nimble soul
ThunderShield
spring Breeze
Avalanche


Hard Lilith, 15, Blizzard:160
Titan,					14,	6,	792,	4200,	Tundra,     	Resistance,				Halo:105,	Reflection:180
Jorm,					16,	6,	768,	5175,	swamp,  		Electric Shock 7,		Retaliation 8,			Immunity,				Rejuvenation:240
Lampade,				11,	4,	492,	4080,	Forest,			Fireball:100,			Vulnerability:120,		Firestorm:175
Mermaid,				13,	4,	510,	3180,	swamp,  		Blizzard:30,			Confusion:65,			Dodge:45	
Naiad,					12,	4,	510,	3570,	Forest,			Iceball:80,				Regeneration:150,		Blizzard:120	
Tantor Lord,			13,	4,	618,	3720,	swamp,  		Firestorm:150,			Bloodsucker:60,			Wilderness Power:175
Moirai,					16,	4,	672,	5430,	swamp,  		Wilderness Guard:400,		Wilderness Shield:50,		Resistance
Phoenix,				15,	4,	792,	4680,	Forest,			Firestorm:200,			Rejuvenation:210,		Resurrection:70	
Artemis,				17,	4,	1020,	6075,	Forest,			Magic Shield:60,		Healing:250,			QS_Teleportation,		Resistance

