
player name: Extreme [350875]. Magic Lance

# Cards
Extreme Lilith, 99, 4, 2000, 250000, hydra, Immunity, Holy Flame, Infiltrator, Magic Lance:700
Fafnir, 17, 4, 1610, 9125, forest, Concentration:120, Parry:160, Immunity, Retaliation:160
Fafnir, 17, 4, 1610, 9125, forest, Concentration:120, Parry:160, Immunity, Infiltrator
Fafnir, 14, 4, 1310, 8550, forest, Concentration:120, Parry:160, Immunity
Fafnir, 17, 4, 1610, 9125, forest, Concentration:120, Parry:160, Immunity, Regeneration:125
Jormungand, 16, 4, 1280, 8875, swamp, Electric Shock:175, Retaliation:160, Immunity, Concentration:120
Jormungand, 16, 4, 1280, 8875, swamp, Electric Shock:175, Retaliation:160, Immunity, Warpath:90
Jormungand, 14, 4, 1080, 7750, swamp, Electric Shock:175, Retaliation:160, Immunity
WingedSnake, 19, 3, 2180, 8075, swamp, Chain Attack:175, Warpath:105, Resurrection:75, Wilderness Guard:350
Moirai, 16, 4, 1120, 9050, swamp, Wilderness Guard:400, Wilderness Shield:50, Resistance
Golden Fafnir, 19, 4, 1910, 10550, forest, Composite:Clean Sweep&Infiltrator, Resilience:140, Immunity, Shelter of Earth
Virgo, 20, 4, 1740, 11775, forest, Mental Frenzy, Resistance, Sura Fire Attack:160:70, Speedup All:1

# Runes
Thundershield:200
Red Valley:90
Death Zone:200
Mysterious Stone:2
