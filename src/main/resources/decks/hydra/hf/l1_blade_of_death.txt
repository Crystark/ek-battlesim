
player name: Easy. Blade of Death

# Cards
Easy Lilith, 99, 4, 1000, 10000, hydra, Resistance, Dual Snipe:250, Bloodthirsty:80, Blade of Death:90
Black Widow, 10, 4, 400, 885, swamp, Smog:40, Assassination:120, Trap:2
Black Widow, 10, 4, 400, 885, swamp, Smog:40, Assassination:120, Trap:2
Chariot, 10, 4, 430, 1030, tundra, QS_Trap:1, Parry:100, Combustion:175
Spectre Orca, 11, 4, 400, 800, mtn, Curse:150, QS_Blizzard:80, Resurrection:55
Dream Cleric, 11, 4, 415, 1020, forest, Healing:125, Regeneration:100, Confusion:60
Mermaid, 13, 4, 425, 1060, swamp, Blizzard:100, Confusion:65, Dodge:45
Succubus, 9, 4, 410, 960, mtn, Dodge:30, Pollution:105, Trap:1
Medusa, 12, 6, 510, 1100, mtn, Curse:180, Trap:2, QS_Teleportation
OgreWarlock, 9, 4, 425, 780, swamp, QS_Curse:180, Fireball:125, Regeneration:150
Feathered Serpent, 16, 3, 690, 2090, swamp, Magic erosion:180, Dodge:60, Immunity


# Runes
Thunderbolt:175
Icicle:140
Fire Flow:175
