
player name: Hard [143685]. Hellfire

# Cards
Hard Lilith, 99, 4, 1200, 90000, hydra, Immunity, Dual Snipe:250, Bloodthirsty:80, Hellfire:3:500_1000_500
Phoenix, 15, 2, 792, 4680, forest, Firestorm:200, Rejuvenation:210, Resurrection:70
Jormungand, 16, 4, 768, 5325, swamp, Electric Shock:175, Retaliation:160, Immunity, Rejuvenation:240
Naiad, 12, 4, 510, 3570, forest, Iceball:80, Regeneration:150, Blizzard:120
Lampade, 11, 4, 492, 4080, forest, Fireball:100, Vulnerability:120, Firestorm:175
TantorLord, 13, 4, 618, 3720, swamp, Firestorm:150, Bloodsucker:60, Wilderness Power:175
Mermaid, 13, 4, 510, 3180, swamp, Blizzard:100, Confusion:65, Dodge:45
Titan, 14, 6, 792, 4200, tundra, Resistance, Halo:105, Reflection:180
Artemis, 17, 4, 1020, 6075, forest, Magic Shield:60, Healing:250, QS_Teleportation, Resistance
Moirai, 16, 4, 672, 5430, swamp, Wilderness Guard:400, Wilderness Shield:50, Resistance
Saber, 20, 4, 1296, 6930, mtn, Composite:Physical parry&Skill parry, Infiltrator, Clean Sweep, Confusion:70
Queen of Thorns, 19, 4, 1080, 6495, forest, Retaliation:140, Perseverance, Warpath:105, Super Craze:240

# Runes
Thundershield:200
Nimble Soul:65
Spring Breeze:240
Avalanche:200
