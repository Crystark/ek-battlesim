# EK Battlesim - v0.6.12-SNAPSHOT - Generated deck file on 2015-10-14 13:37:40
deck name: Hydra II (Curse)
player name: Hydra II (Curse)

# Cards
Lava Destroyer, 20, 1, 615, 2960, mtn, Guard, Resurrection:60, D_Reanimation
Lava Destroyer, 20, 1, 615, 2960, mtn, Guard, Resurrection:60, D_Reanimation
Draconian Elder, 14, 6, 510, 2200, mtn, Curse:240, Trap:2, QS_Teleportation
Fire Berserker, 17, 4, 670, 3510, mtn, Concentration:120, Fire God:160, Evasion
Omniscient Dragon, 18, 5, 490, 5800, forest, Guard, Resistance, Toxic Clouds:120
Draconian Elder, 14, 6, 510, 2200, mtn, Curse:240, Trap:2, QS_Teleportation
Fire-Cursed Treant, 10, 4, 360, 1760, mtn, QS_Curse:320, Mountain Guard:250, Curse:240
Fire-Cursed Treant, 10, 4, 360, 1760, mtn, QS_Curse:320, Mountain Guard:250, Curse:240
Mermaid Princess, 14, 4, 430, 2060, tundra, Blizzard:100, Confusion:65, Dodge:45
Hydra II (Curse), 99, 4, 1000, 40000, hydra, Resistance, Sacred Flame, Bloodthirsty:80, Curse:320

# Runes
Burning Soul:180
Charred:120
Inferno:225
Lore:135
