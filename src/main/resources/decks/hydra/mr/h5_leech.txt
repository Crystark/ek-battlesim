# EK Battlesim - v0.6.12-SNAPSHOT - Generated deck file on 2015-10-14 13:37:41
deck name: Hydra V (Leech)
player name: Hydra V (Leech)

# Cards
Crystal Emperor, 16, 4, 1220, 7450, tundra, Origins Guard:280, Ice Shield:150, Power Source:140
Ice Dragon, 15, 6, 1260, 8450, tundra, Dodge:45, Clean Sweep, Immunity
Ice Dragon, 15, 6, 1260, 8450, tundra, Dodge:45, Clean Sweep, Immunity
Taiga General, 15, 4, 1210, 8750, tundra, Magic Shield:80, Exile, Regeneration:175
Taiga General, 15, 4, 1210, 8750, tundra, Magic Shield:80, Exile, Regeneration:175
Moss Dragon, 15, 6, 1310, 8550, forest, Concentration:120, Parry:160, Immunity
Moss Dragon, 15, 6, 1310, 8550, forest, Concentration:120, Parry:160, Immunity
Moss Dragon, 15, 6, 1310, 8550, forest, Concentration:120, Parry:160, Immunity
Fire Kirin, 14, 4, 1240, 7800, forest, Firestorm:200, Rejuvenation:210, Resurrection:70
Hydra V (Leech), 99, 4, 2000, 250000, hydra, Immunity, Sacred Flame, Infiltrator, Wicked Leech:24

# Runes
Thunder Shield:200
Clear Spring:225
Arctic Freeze:100
Nimble Soul:65
