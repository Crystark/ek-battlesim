# EK Battlesim - v0.6.12-SNAPSHOT - Generated deck file on 2015-10-14 13:37:40
deck name: Hydra I
player name: Hydra I

# Cards
Headless Horseman, 9, 2, 455, 670, mtn, Dodge:35, Backstab:120, Concentration:140
Gas Spore, 9, 4, 485, 820, swamp, QS_Group Weaken:30, Bloodthirsty:40, Concentration:140
Soul Eater, 10, 2, 425, 890, swamp, Parry:40, Craze:50, Rejuvenation:150
Four-winged Firehawk, 11, 4, 430, 900, mtn, Backstab:80, Reflection:60, Rejuvenation:150
Marsh Devourer, 15, 4, 700, 1600, swamp, Blitz:105, Confusion:50, Mania:160:160
Harpy, 9, 4, 375, 900, forest, Thunderbolt:75, Forest Force:100, Chain Lightning:125
White Flying Fox, 9, 4, 380, 870, tundra, Concentration:40, Iceball:100, Retaliation:100
Mystic Monk, 10, 4, 370, 900, tundra, Weaken:30, Reflection:60, Mountain Glacier:90
Foxtail Nymph, 12, 2, 495, 975, forest, Parry:100, Concentration:100, Evasion
Hydra I, 99, 4, 1000, 10000, hydra, Resistance, Dual Snipe:250, Bloodthirsty:80

# Runes
Red Valley:70
Stone Forest:210
Stonewall:140
