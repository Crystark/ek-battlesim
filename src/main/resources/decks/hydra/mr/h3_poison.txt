# EK Battlesim - v0.6.12-SNAPSHOT - Generated deck file on 2015-10-14 13:37:40
deck name: Hydra III (Poison)
player name: Hydra III (Poison)

# Cards
Zombie Dragon, 14, 6, 768, 4830, swamp, Plague:40, Venom:160, Immunity
Zombie Dragon, 14, 6, 768, 4830, swamp, Plague:40, Venom:160, Immunity
Snow Ogre, 11, 4, 408, 3960, tundra, Dodge:35, QS_Exile, Group Weaken:80
Snow Ogre, 11, 4, 408, 3960, tundra, Dodge:35, QS_Exile, Group Weaken:80
Soul Thief, 14, 3, 606, 3150, swamp, Reflection:150, Parry:120, Wicked Leech:21
Dire Snapping Turtle, 15, 4, 750, 5100, mtn, Clean Sweep, Seal, Bloodsucker:80
Latite Golem, 12, 4, 486, 4350, mtn, Combustion:125, Laceration, Group Weaken:80
Frostree-Folk, 13, 6, 600, 3240, tundra, Mountain Glacier:105, Rejuvenation:150, Ice Shield:160
Abhorrent Gargoyle, 15, 6, 744, 5160, swamp, Clean Sweep, Bloodthirsty:60, Resistance
Hydra III (Poison), 99, 4, 1200, 90000, hydra, Immunity, Dual Snipe:250, Bloodthirsty:80, Smog:160

# Runes
Red Valley:90
Stonewall:180
Death Zone:200
Stone Forest:270
