# EK Battlesim - v0.6.12-SNAPSHOT - Generated deck file on 2015-10-14 13:37:40
deck name: Hydra III (Freeze)
player name: Hydra III (Freeze)

# Cards
Fire Kirin, 14, 4, 744, 4680, forest, Firestorm:200, Rejuvenation:210, Resurrection:70
Behemoth, 14, 6, 648, 5280, swamp, Electric Shock:175, Retaliation:160, Immunity
Water Elemental, 12, 4, 510, 3570, forest, Iceball:80, Regeneration:150, Blizzard:120
Kitsune, 12, 4, 552, 4080, forest, Fireball:100, Swamp Purity:120, Firestorm:175
Manscorpion, 13, 4, 618, 3720, swamp, Firestorm:150, Bloodsucker:60, Swamp Force:175
Mermaid Princess, 14, 4, 516, 3090, tundra, Blizzard:100, Confusion:65, Dodge:45
Terra Cotta Warrior, 14, 6, 732, 3930, tundra, Resistance, Mountain Glacier:105, Reflection:150
Aranyani, 14, 4, 816, 5130, forest, Magic Shield:80, Healing:250, QS_Teleportation
Soul Thief, 14, 3, 606, 3150, swamp, Reflection:150, Parry:120, Wicked Leech:21
Hydra III (Freeze), 99, 4, 1200, 90000, hydra, Immunity, Dual Snipe:250, Bloodthirsty:80, Blizzard:160

# Runes
Thunder Shield:200
Nimble Soul:65
Spring Breeze:240
Avalanche:200
