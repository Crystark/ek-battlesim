# EK Battlesim - v0.6.12-SNAPSHOT - Generated deck file on 2015-10-14 13:37:40
deck name: Hydra I
player name: Hydra I

# Cards
Fallen Angel, 11, 2, 400, 750, mtn, Bite:60, Forest Fire:90, Blizzard:100
Snow Lotus Sprite, 8, 4, 290, 950, tundra, Nova Frost:40, QS_Blizzard:80, Weaken:40
Troglodyte, 9, 4, 410, 960, forest, Healing:150, Swamp Purity:105, Chain Lightning:150
Crystal Golem, 11, 4, 385, 950, tundra, QS_Electric Shock:50, Resurrection:50, Chain Lightning:125
Frost Troll Mage, 8, 4, 375, 770, tundra, Fire Wall:50, QS_Firestorm:75, Chain Lightning:150
Kitsune, 12, 4, 460, 1360, forest, Fireball:100, Swamp Purity:120, Firestorm:175
Water Elemental, 12, 4, 425, 1190, forest, Iceball:80, Regeneration:150, Blizzard:120
Mischievous Pixie, 8, 2, 305, 900, forest, Fireball:75, Regeneration:100, Fire Wall:150
Fire Golem, 9, 4, 340, 870, mtn, Nova Frost:40, Curse:200, Group Weaken:40
Hydra I, 99, 4, 1000, 10000, hydra, Resistance, Dual Snipe:250, Bloodthirsty:80

# Runes
Coldwave:60
Permafrost:160
Thunderbolt:175
